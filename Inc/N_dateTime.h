#ifndef _N_DATETIME_H_INCLUDED
#define	_N_DATETIME_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif
    
#define EXPIRE                  2 //MRF_NEW19
/** structured date and time in uint32 type */
typedef struct 
{
    uint32 date;                   /** date */
    uint32 time;                   /** time */
}dateTimeST;


uint32 dateTimeDiffSecondes(dateTimeST* beginDateTime, dateTimeST* endDateTime);

uint32 getDayNumInYear(dateTimeST* dateTime);

void gregorianToJalali(uint32* jalaliDate, uint32 gregorianDate);

void jalaliToGregorian(uint32* gregorianDate, uint32 jalaliDate);

uint8 setSystemDateTime(filesST* files, dateTimeST* dateTime, int dateTimeBoth, int jalali);

int compareDates(dateTimeST* first, dateTimeST* second, uint8 Time);



/**
 * convert date in uint32 format to date time .
 * @param   inputDate input Date.
 * @param   dateTime date time .
 * @see     getYear()  
 * @see     getMonth()
 * @see     getDay()
 */
void dateToSystemDateTime(uint32 inputDate, int32* year, int32* month, int32* day);

uint32 systemDateTimeToDate(void);

int getYear(uint32 inputDate);

int getMonth(uint32 inputDate);

int getDay(uint32 inputDate);

uint32 setDate(int year, int month, int day);

void DateToStr(uint32 inputDate, uint8* outputDate);

uint32 strToDate(uint8* inputDate);

void timeToSystemDateTime(uint32 inputTime, uint32* hour, uint32* minute, uint32* second);

uint32 systemDateTimeToTime(void);

int getHour(uint32 inputTime);

int getMinute(uint32 inputTime);

int getSecond(uint32 inputTime);

uint32 setTime(int hour, int minute, int second);

void timeToStr(uint32 inputTime, uint8* outputTime);

uint32 strToTime(uint8* inputTime);

dateTimeST getDateTime(void);

uint32 setNextDate(uint32 date, int incYear, int incMonth, int incDay);

dateTimeST setNextTime(dateTimeST* dateTime, int incHour, int incMinute, int incSecond);

void secondToStandardTime(uint32 input, int* day, int* hour, int* minute, int* second);

int compareDateTime(dateTimeST* first, dateTimeST* second, uint8 Time); 

void getGMTDateTimeStrYYMMDDhhmm(dateTimeST* dateTime, uint8* buffer);

void getDateStrYYMMDD(dateTimeST* dateTime, uint8* buffer);

void getDateStrYYMM(dateTimeST* dateTime, uint8* buffer);

void getDateStrMMDD(dateTimeST* dateTime, uint8* buffer);

void getDateTimeStrYYMMDDhhmmss(dateTimeST* dateTime, uint8* buffer);

uint8 randomTime(int startHour, int endHour, int* hour, int* minute);

void setUserDateAndTime(void);//HNO

uint8 calculateNextDate(void);

uint8 checkDate(void);

void setTimeForPrint(dateTimeST* dateTime, uint8* time);


#ifdef	__cplusplus
}
#endif

#endif	/* _time_H */

