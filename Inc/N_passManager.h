#ifndef N_PASSMANAGER_H_INCLUDED
#define N_PASSMANAGER_H_INCLUDED


#ifdef	__cplusplus
extern "C" {
#endif

    
/**Maximum length of password can be enter */
#define 	MAX_PASS_LEN                4
    
/**Supervisor menu of Password*/
#define    	SUPERVISOR_PASS             "6402" // :) that is funny ;)
    
/**Merchant menu of Password*/
#define    	MERCHANT_PASS               "1111"
    
/**User Manager of Password*/
#define    	USER_MANAGER_PASS           "1111" 
    
/**Password for reset supervisor menu*/
#define     RESET_SUPERVISOR_PASS       "6453"
    
/**Log menu of Password*/
#define    	LOG_PASS                    "5698" //MRF_NEW19
    
#define    	PRINT_PASS                  "2552" //MRF_NEW20   
//HNO_ADD
/** for manage OS password*/
#define    	DOWN_PASS                   "6906" //for ingenico
    
/**Number of time can enter password*/
#define     PASS_RETRY                  3

/**State of password*/
enum passStateEN
{
    NO_PASS,            /*!< Password Not Entered*/
    CORRECT_PASS,       /*!< Password Entered is correct*/
    INCORRECT_PASS      /*!< Password Entered is incorrect*/
};

uint8 getPass(uint8* message, uint8* enteredPass, int maxlen, int passType);

uint8 writePassword(uint8* fileName, uint8* password);

uint8 readPassword(uint8* fileName, uint8* defaultPass, uint8* password, uint32 passLen, uint8 setDefault);

uint8 changePassword(uint8* fileName, uint8* message, uint32 passLen);

uint8 checkMerchantMenuPassword(void);

uint8 checkSupervisorMenuPassword(void);

uint8 checkUserManagerMenuPassword(void);

uint8 changeSupervisorMenuPassword(void);

uint8 changeUserManagerMenuPassword(void);

uint8 changeMerchantMenuPassword(void);

uint8 resetMerchantMenuPassword(uint8 showMsg);

uint8 resetMerchantMenuPasswordMenu(void); //mgh_add_970528

uint8 checkLogMenuPassword(void);

uint8 ActiveSupervisorMenu(void);

uint8 resetUserMenuPassword(void);//ABS:ADD

uint8 checkDownMenuPassword(void); 

uint8 PasswordGenerator(void);

uint8 printSupervisorPass(void);
#ifdef	__cplusplus
}
#endif

#endif // PASSMANAGER_H_INCLUDED
