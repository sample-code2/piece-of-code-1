#ifndef _N_utility_H
#define	_N_utility_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    
/** set uint32 type date */
#define SN_DATE(yy, mm, dd)  ((((uint32)yy) << 16) + (((uint32)mm) << 8) + ((uint32)dd))
/** set uint32 type time */
#define SN_TIME(hh, mm, ss)  ((((uint32)hh) << 16) + (((uint32)mm) << 8) + ((uint32)ss))
/** returns the minimum value */
#define MINIMUM(a,b) (((a)<(b)) ? (a) : (b))
/** returns the maximum value */
#define MAXIMUM(a,b) (((a)>(b)) ? (a) : (b))
/** Hex code of NUUL in ASCII*/
#define ASCII_NULL 0x00

        

void ipToString(uint8* IP, uint8* strIP);

void makeValueWithCommaStr(uint8* input, uint8* output);

void makeValueWithCommaInt(uint32 value, uint8* valueWithCommaStr);

void maskCardId(uint8* cardIdStr, uint8* result);

int zeroPadStrToInt(uint8* inputStr, int size);

void asciiToBCD(uint8* input, uint8* output, int inputLen, uint8 padElement, uint8 padDir);

uint8 hexCharToBin(uint8 input);

uint8 binCharToHex(uint8 input);

void binStrToHex(uint8* binStr, uint8* hexStr, int binLen);

uint32 StrToUInt64(uint8* inputStr, int size);

uint8 isBitSet(int index, uint8 input);

uint8 setBits(uint8 bit0, uint8 bit1, uint8 bit2, uint8 bit3, uint8 bit4, uint8 bit5, uint8 bit6, uint8 bit7);

void longToBCD(uint32 src, uint8* dst, int32 len);

void comportHexShow(uint8* ucDataToPrint, uint16 iLen);

int wordWrapFarsi(uint8* str, uint8 separatedStr[10][250], uint8 maxInLine,int length);

int wordWrapEnglish(uint8 str[250], uint8 separatedStr[10][250], uint8 maxInLine);

void removePadLeft(uint8* buffer, uint8 pad);

void padLeft(uint8* buffer, const uint8 finalLength);

int32 strToInt(uint8* inputStr);

void hex2bin(uint8* hexStr, uint8* binStr, int hexLen);

void removePadRight(uint8* buffer, uint8 pad);

void removePad(uint8* buffer, uint8 pad);

uint16 convertCp1256ToIransystem(uint8* strBuff);

int power(int x, int y);

void sumStringNumbers(uint8* X, uint8* Y, uint8* result);

int compareStringNumbers(uint8* X, uint8* Y);

void SubStringNumbers(uint8* X, uint8* Y, uint8* result);

void strToUpper(uint8* str);

void strToLower(uint8* str);

uint16 convertCp1256ToUnicde(uint8* OutBuffer, uint8* inBuffer);

int16 convertCp1256ToLCD(uint8* strBuff);

void maskMobileNumbers(uint8* mobileNo, uint8* result);

#ifndef INGENICO
    unsigned int strlcpy(char *dst, const char *src, unsigned int size);

    unsigned int strlcat(char *dst, const char *src, unsigned int size);
#endif

void reversPartString(uint8* string);

uint16 convertIransystemToUnicde(uint8* OutBuffer, uint8* inBuffer);

void stringToIP(uint8* input, uint8* output);

#ifdef	__cplusplus
}
#endif

#endif	/* _utility_H */

