#ifndef N_TRANSACTIONS_H_INCLUDED
#define N_TRANSACTIONS_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif
    
#define UNSUCCESS_TRANS_NOT_SENT                  	101
#define UNSUCCESS_TRANS_NOT_RECEIVED              	102
#define UNSUCCESS_TRANS_RESPONSE_ERROR            	103
#define UNSUCCESS_TRANS_PROCESS_RESPONSE_ERROR    	104
#define TRANS_SUCCEED                               105

// TRANSACTIONS TYPES
#define ACCEPTED                                    0
#define TRANS_BUY                                   0
#define TRANS_BUYCHARGE                             1
#define TRANS_BALANCE                               2
#define TRANS_SETTLEMENT                            3
#define TRANS_REVERSAL                              4
#define TRANS_INITIALIZE                            5
#define TRANS_LOGON                                 6
#define TRANS_BILLPAY                               7
#define TRANS_FILE_MANAGEMENT                       12 
#define TRANS_FUNDS_TRANSFER_INQUERY                13 
#define TRANS_FUNDS_TRANSFER                        14 
#define TRANS_LOANPAY_INQUERY                       15 
#define TRANS_LOANPAY                               16 
#define TRANS_BILL_CARD                             17 
#define TRANS_BATCH                                 18 
#define TRANS_END_OF_DAY                            19 
#define	TRANS_ROLL_REQUEST                          20
#define	TRANS_ROLL_CONFIRM                          21
#define TRANS_CREDIT_BALANCE                        22
#define TRANS_CREDIT_BUY                            23
#define	TRANS_PAY_OFF                               24
#define TRANS_ACTIVE_TEST_BALANCE                   25
#define TRANS_ETC_INQUERY                           26   //MRF_ETC
#define TRANS_ETC                                   27   //MRF_ETC
#define TRANS_ETC_TRACKING                          28   //MRF_ETC 
#define TRANS_TOPUP                                 29   //MRF_TOPUP
#define TRANS_LOAN_TRACKING							30	//HNO_LOAN
#define TRANS_ROLL_TRACKING							31	//HNO_ADD_ROLL
#define TRANS_CHARITY								32	//HNO_CHARITY
#define	MAX_IBAN_INFO								6
#define MAX_TRANSACTION_COUNT                       50 
#define MAX_CHARGE_TRANS                            100	//HNO_CHARGE

#if defined(ICT250) || defined(IWL220) //HNO
#define MAX_ETC_TRANS                               100
#define MAX_LOAN_TRANS                              100
#define MAX_Roll_TRANS                              100
#else
#define MAX_ETC_TRANS                               500 //MRF_ETC
#define MAX_LOAN_TRANS                              500 //MRF_LOAN
#define MAX_Roll_TRANS                              100 //MRF_IDENT2
#endif
    
//*************************************INDEX OF OPERATORS AMOUNT******************************
#define MTN_10000                                   0
#define MTN_20000                                   1
#define MTN_50000                                   2
#define MTN_100000                                  3
#define MTN_200000                                  4
#define MTN_500000                                  16
    
#define MCI_10000                                   5  
#define MCI_20000                                   6 
#define MCI_50000                                   7 
#define MCI_100000                                  8 
#define MCI_200000                                  9 
#define MCI_500000                                  17

#define RTL_20000                                   10  
#define RTL_50000                                   11 
#define RTL_100000                                  12 
#define RTL_200000                                  13 
#define RTL_500000                                  14 
    
#define OPERATOR_COUNT                              15

#define MTN_OTHER                                  18	//++ABS_980728	
#define MCI_OTHER                                  19	//++ABS_980728	
#define RTL_OTHER                                  20	//++ABS_980728	


uint8 NotAvalableService(void);

void deleteFiles(argumentListST* args);

void incAndSaveStan(filesST* files, merchantSpecST* merchantSpec);

uint8 setTimeWithSwitch(uint8 logonFlag, filesST* files, messageSpecST* messageSpec);

uint8 readSTANTransFromFile(uint8* fileName, void* transactionInfo, uint16 structLen, uint16 maxRecord, 
							  	uint32 STAN, int* transactionNum, uint32* requestedNum);
    
uint8 readReversalTrans(filesST* files, messageSpecST* messageSpec);

uint8 createSaveReversal(filesST* files, messageSpecST* messageSpec);

uint8 updateReversalInfo(filesST* files, messageSpecST* messageSpec, uint8 isPOSStan);

uint8 reversalTrans(filesST* files);
//HNO_PCPOS
uint8 reversalTransPCPOS(filesST* files); 

uint8 preTransInitialize(filesST* files);

uint8 initializeTrans(argumentListST* args);

uint16 readSettlementSpec(filesST* files, settlementSpecST* settlementSpec);

uint8 writeSettlementSpec(filesST* files, settlementSpecST* settlement);

uint16 addTransToSettlementFile(filesST* files, messageSpecST* messageSpec);

uint8 supervisorTrans(filesST* files,messageSpecST* messageSpec,  cardSpecST cardSpec, uint8* keyMenu);

uint8 logonTrans(filesST* files);

uint8 settlement(argumentListST* args);

uint16 settlementTrans(filesST* files);

uint8 buyTrans(argumentListST* args);

uint8 buyPcPOSTrans(filesST* files, messageSpecST* messageSpec);

uint8 balanceTrans(argumentListST* args);

uint8 billPayTrans(argumentListST* args);

uint8 chargeBuyTrans(argumentListST* args);

void buyMultiChargeTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int* multiTypeCharge, uint8 singleCharge ); //ABS_NEW1

void buyChargeTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int* multiTypeCharge);

uint8 buyMultiChargeOfflineTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int index, int* multiTypeCharge);

//void buyMultiChargeTrans(filesST* files, messageSpecST* messageSpec,
//		buyChargeSpecST* buyCharge, int index, int* multiTypeCharge);

void resetBuyChargeInfo(void);

void getChargeType(uint8* buff, uint8 type, uint8* amount);

uint8 getChargeTypeCount(void);

uint8 buyMultiChargeMCI10000(argumentListST* args);

uint8 buyMultiChargeMCI20000(argumentListST* args);

uint8 buyMultiChargeMCI50000(argumentListST* args);

uint8 buyMultiChargeMCI100000(argumentListST* args);

uint8 buyMultiChargeMCI200000(argumentListST* args);
//HNO_IDENT2
uint8 buyMultiChargeMCI500000(argumentListST* args);

uint8 buyMultiChargeIrancell10000(argumentListST* args);

uint8 buyMultiChargeIrancell20000(argumentListST* args);

uint8 buyMultiChargeIrancell50000(argumentListST* args);

uint8 buyMultiChargeIrancell100000(argumentListST* args);

uint8 buyMultiChargeIrancell200000(argumentListST* args);
//HNO_IDENT2
uint8 buyMultiChargeIrancell500000(argumentListST* args);

uint8 buyMultiChargeRightel20000(argumentListST* args);

uint8 buyMultiChargeRightel50000(argumentListST* args);

uint8 buyMultiChargeRightel100000(argumentListST* args);

uint8 buyMultiChargeRightel200000(argumentListST* args);

uint8 buyMultiChargeRightel500000(argumentListST* args);

uint8 loanPayTrans(argumentListST* args);

uint16 LoanPayInquery(filesST* files, cardSpecST* cardInfo, loanPayST* loanpay);

void LoanPayFinancialTransaction(filesST* files, cardSpecST* cardInfo, loanPayST* loanPay);

uint16 LoanPayTrackingTrans(argumentListST* args);

uint8 ETCTrans(argumentListST* args);

uint16 ETCInquery(filesST* files, cardSpecST* cardInfo, ETCST* ETC);

void ETCFinancialTransaction(filesST* files, cardSpecST* cardInfo, ETCST* ETC);//HNO_CHANGE

uint16 ETCTrackingTrans(argumentListST* args);

//uint8 transactionChecking(messageSpecST* messageSpec, uint16 checkResStatus, uint16 parseResStatus, filesST* files);

//uint8 buyTopupChargeTrans(argumentListST* args);

uint8 buyTopupChargeTrans(filesST* files, messageSpecST* messageSpec, uint8* keyMenu, uint8 variable);

void setTopupFields(argumentListST* args);

uint8 updateResponse(filesST* files, messageSpecST* messageSpec);

uint8 goToManager(void); //HNO

uint8 rollRequestTrans(argumentListST* args);//HNO

uint16 rollRequestTrackingTrans(argumentListST* args);//HNO_ADD_ROLL

//HNO_CHARITY
uint8 behzistiCharity(argumentListST* args);

uint8 ashrafolAnbiaCharity(argumentListST* args);

uint8 bonyadeMaskaneEnghelabCharity(argumentListST* args);

uint8 hemmateJavananCharity(argumentListST* args);

uint8 komitehEmdadCharity(argumentListST* args);

uint8 kahrizakCharity(argumentListST* args);

uint8 mahakCharity(argumentListST* args);

//HNO_CHARITY
uint8 charityTrans(argumentListST* args, uint8* instituteCode);

//HNO_CHARGE
uint32 createID(uint32 timeStr);

void getCharityInstituteName(uint8* instituteName, uint8* instituteCode);



#ifdef	__cplusplus
}
#endif

#endif // TRANSACTION_H_INCLUDED
