/* 
 * File:   N_log.h
 * Author: maryam.ghods
 *
 * Created on October 12, 2011, 4:15 PM
 */

#ifndef _N_LOG_H
#define	_N_LOG_H

#ifdef	__cplusplus
extern "C" {
#endif

/** file of log type password */
#define     LOG_TYPE_PASSWORDS_FILE                 "ltpw0100"
 
    
uint8 getLogType(void);

void setLogType(void);

void initLogType(void);

void showLog(uint8 posType, uint8* file, uint32 line, uint8 level, uint8* title, uint8* message, ...);

void setLogPort(void);

uint8 writeLogPort(void);

uint8 getLogPort(void);


#ifdef	__cplusplus
}
#endif

#endif	/* _N_LOG_H */

