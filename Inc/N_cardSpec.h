#ifndef _N_CARDSPEC_H
#define	_N_CARDSPEC_H

#ifdef	__cplusplus
extern "C" {
#endif

    
//mgh #define	INSTALLATION_CONFIRM_CARD_FILE     		"install100"
/** max card track size */
#define MAX_TRACK_SIZE                          128
/** max card track2 length */
#define MAX_TRACK2_BANK_LEN                     40
/** min card track2 length */
#define MIN_TRACK2_BANK_LEN                     25
/** maximum count of BIN number */
#define MAX_BIN_COUNT                           100
    

    
#define SUPERVISOR_BIN                          "435701"
 
/** BIN number specification */    
typedef struct
{
    uint8  binBS[12];
    uint8  binES[12];
    uint8  binPF;
    uint8  binLT;
    uint8  binLN;
    uint8  binLX;
    uint8  binIA;
    uint16 binSC;
    uint8  binLC;
    uint8  binPM;
    uint8  binLM[16];
}BINSpecST;

/** card tracks specification */
typedef struct 
{
    uint16 track1Len;                 /*!< card track 1 lenght */
    uint16 track2Len;                 /*!< card track 2 lenght */
    uint16 track3Len;                 /*!< card track 3 lenght */
    uint8  track1[MAX_TRACK_SIZE];    /*!< card track 1 data */
    uint8  track2[MAX_TRACK_SIZE];    /*!< card track 2 data */
    uint8  track3[MAX_TRACK_SIZE];    /*!< card track 3 data */
}cardTracksST;

/** for card tracks information */
typedef struct 
{
    uint8 track1[80];                  /*!< card track 1 data */
    uint8 track2[41];                  /*!< card track 2 data */
}tracksInfoST;   

/** card informations */
typedef struct 
{
    uint8  track2[MAX_TRACK_SIZE];      /*!< card track 2 data */
    uint16 track2Len;                   /*!< card track 2 lenght */
    int8   PAN[19 + 1];                 /*!< card number */
    int8   PANLen;                      /*!< card number lenght */
    uint8  PIN[4 + 1];                  /*!< PIN */
    int8   expDate[4 + 1];              /*!< expiry date */
    uint8  customerName[27];            /*!< customer name */
    uint8  personnelID[4 + 1];
}cardSpecST;


uint8 readTrackData(filesST* files, cardTracksST* cardSpec, cardSpecST* cardInfo, uint8 magnetCard);

uint8 processCardData(filesST* files, cardTracksST* cardTracks, cardSpecST* cardSpec);

uint16 writeCardInfo(cardSpecST* cardInfo);

uint16 readCardInfo(cardSpecST* cardInfo);

uint16 checkBINMaskan(filesST* files, cardSpecST* cardInfo);

#ifdef	__cplusplus
}
#endif

#endif	/* _cardSpec_H */

