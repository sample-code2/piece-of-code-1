
#ifndef _N_PRINTER_H
#define	_N_PRINTER_H

#ifdef	__cplusplus
extern "C" {
#endif

/** farsi font for printer */    
#define FARSI                   1
/** english font for printer */ 
#define ENGLISH              	2

	//HNO_ADD
#define FARSI                   1
#define ENGLISH                 2
#define FARSI_DIGIT             3
/**
 * check printer access.
 * @param   showPrinterError show printer error.
 * @param   checkAgain check printer access again.
 * @return  TRUE or FALSE.
 */
uint8 PrinterAccess(uint8 showMsg);

uint8 printStar(void);

uint8 printDash(void);

uint8 printScrolableLines(uint8* title, uint8 lines[][32], uint8 lineCount);

uint8 printReceiptDate(uint32 date, uint8* message);

uint8 printReceiptDateTime(dateTimeST* dateTime, uint8 isNormal);

uint8 printOneStringFarsi(uint8* string, int size, int align);

uint8 printOneStringFarsiOld(uint8* string, int size, int align) ;

uint8 printOneStringEnglish(uint8* string, int size, int align);

uint8 printTwoStringNumericFarsi(uint8* firstStr, uint8* secondStr, int size);

uint8 printTwoStringFarsi(uint8* firstStr, uint8* secondStr, int size);

uint8 printTwoString(uint8* firstStr, uint8* secondStr, int size, uint8 en);

uint8 printThreeStringFarsi(uint8* firstStr, uint8* secondStr, uint8* thirdStr, int size, int align, uint8 colon);

uint16 printLogo(filesST* files, int type);

uint8 printDash2(void);
 
uint8 printDash3(void);
//+HNO_980512
uint8 printReceiptDateTimeWithString(dateTimeST* dateTime, uint8* string3) ;


#ifdef	__cplusplus
}
#endif

#endif	/* _N_PRINTER_H */

