#ifndef N_MESSAGING_H_INCLUDED
#define N_MESSAGING_H_INCLUDED

#include "N_dateTime.h"



#ifdef	__cplusplus
extern "C" {
#endif


#define UNSUCCESS_INITIALIZE					 -4                     
#define UNSUCCESS_LOGON                          -3
#define UNSUCCESS_REVERSAL                       -2  
#define UNSUCCESS_SETTLEMENT                     -1    

#define PARSE_NO_ERROR                           0
#define PARSE_ERROR_INVALID_MAC                  201
#define PARSE_ERROR_INVALID_BITMAP               202
//#define PARSE_ERROR_INVALID_AMOUNT               203  --MRF_NEW3
#define PARSE_ERROR_INVALID_RESPONSE             204
//#define PARSE_ERROR_INTERNAL                     205 --MRF_NEW3
#define PARSE_REPEAT_BUY_CHARGE_TRANS            206

#define CHECK_RESPONSE_ERROR                     2
// DEFINED PROCESS CODES
#define PROCESS_CODE_BUY                   "000000" 
#define PROCESS_CODE_BUY_CHARGE            "880000" 
#define PROCESS_CODE_BALANCE               "310000" 
#define PROCESS_CODE_SETTLEMENT            "920000"
#define PROCESS_CODE_INITIALIZE            "920000" 
#define PROCESS_CODE_LOGON                 "920000"
#define PROCESS_CODE_BILLPAY               "500000"
#define PROCESS_CODE_BILL_REMAIN           "390000"  
#define PROCESS_CODE_SHORT                 "380000"
#define PROCESS_CODE_REFUND                "200000"
#define PROCESS_CODE_CASH                  "010000"
#define PROCESS_CODE_FUNDS_TRANSFER        "400000"  
#define PROCESS_CODE_FUNDS_TRANSFER_1      "330000"  
#define PROCESS_CODE_ROLL                  "950000"
#define PROCESS_CODE_LOAN_PAY_INQUERY      "370000"
#define PROCESS_CODE_LOAN_PAY_TRANS        "860000"
#define PROCESS_CODE_ETC_TRANS             "840000"   //MRF_ETC
#define PROCESS_CODE_ETC_TRACKING          "840001"   //MRF_ETC
#define PROCESS_CODE_BUY_CHARGE_TOPUP      "890000"   //MRF_TOPUP
#define PROCESS_CODE_LOAN_TRACKING         "860001"		//HNO_LOAN
#define PROCESS_CODE_ROLL_REQUEST          "920001"		//HNO_ADD_ROLL
#define PROCESS_CODE_ROLL_TRACKING         "920002"		//HNO_ADD_ROLL_TRACKING
#define PROCESS_CODE_CHARITY               "200000"		//HNO_CHARITY
    
    
#define MAX_IRAN_CODE_COUNT					20
    
    

// for maskan project 
//HNO_LOAN
typedef struct
{
    uint32          STAN;
    int8            destinationCardPAN[16 + 1];
    uint8           destinationCardHolderName[20];
    uint8           destinationCardHolderFamily[20];
    uint8           payAmount[12 + 1]; //loan amount for pay
    uint8           realAmount[12 + 1]; //real loan amount
    dateTimeST      dateTime;
    uint8           ReferenceNumber[12 + 1];
    uint8           PAN[19 + 1];
    uint8           status[250]; //MRF_NEW8// status[999]; MRF: IN PROTOCOL IS 999 FOR MEMORY CRASH CHANGED
    uint8           error[99];//should be 999
}loanPayST;

typedef struct
{
	dateTimeST      dateTime;
    uint8           ReferenceNumber[12 + 1];
    uint8           serial[20 + 1];
    uint8   		PIN[16 + 1];
    uint8           amount[12 + 1]; //real loan amount
    int     		type;
}chargeInfoST;//HNO_CHARGE

typedef struct
{
    uint32          	id;
    uint8   			terminalID[8 + 1];
    int8   				PAN[19 + 1];
    int					chargeCount;
    chargeInfoST		chargeInfo[100];//MRF_NEW10 99
}chargeST;//HNO_CHARGE


// for maskan project   
//MRF_ETC
typedef struct  
{
    uint32          STAN;
    int8            serialETC[13 + 1];
    uint8           cardHolderName[20];
    uint8           cardHolderFamily[20];
    uint8           cardHolderID[10 + 1];
    uint8           amount[12 + 1]; 
    dateTimeST      dateTime;
    uint8           ReferenceNumber[12 + 1];
    uint8           PAN[19 + 1];
    uint8           status[99];// status[999]; MRF: IN PROTOCOL IS 999 FOR MEMORY CRASH CHANGED
    uint8           error[99];//shuld be 999
}ETCST;

//HNO_ADD_ROLL
typedef struct
{
    uint32          STAN;
    uint8           rollCount[3];
}rollRequestST;

//HNO_CHARITY
typedef struct
{
	dateTimeST      dateTime;          /** transaction date and time */
	uint8 			InstituteCode[4];// MRF_CHANGE InstituteCode[99];
	uint8 			PAN[19 + 1];           /** card number */
    uint8 			amount[12 + 1];            /** transaction value */
    uint8 			retrievalReferenceNumber[12 + 1];

}charityST;

typedef struct
{
    int     type;        // Irancell,Rightel,Hamrahe Aval
    //int   productType; // 1000, 2000, ...
    int     productTypeCount;
    uint8   operatorId[3 + 1];
    uint8   amount[12 + 1];
    uint8   realAmount[99 + 1];
    uint8   serial[20 + 1];
    uint8   PIN[32 + 1];
    uint8   usingDescription[250 + 1];//usingDescription[999 + 1];MRF: IN PROTOCOL IS 999 FOR MEMORY CRASH CHANGED
    uint32  id; //HNO_IDENT3
    
}buyChargeSpecST;

typedef struct 
{
    uint8   PAN[19 + 1];
    uint8   transType;
    uint32  STAN;
    uint8   retrievalReferenceNumber[12 + 1];
    uint8   transAmount[12 + 1]; 
    uint8   creditAmount[12 + 1]; 
    uint8   creditReversalAmount[12 + 1];
    uint8   debitAmount[12 + 1]; 
    uint8   debitReversalAmount[12 + 1]; 
    uint8   netReconciliationAmount[12 + 1]; 
    //MRF
    dateTimeST          dateTime;
    
}settlementSpecST;


/** structure of bill specification */
typedef struct
{
    int16   type;                   /*!< type of bill */
    uint8   billID[13 + 1];         /*!<  bill ID */
    uint8   paymentID[13 + 1];      /*!<  payment ID */
    uint8   billAmount[12 + 1];     /*!<  amount of bill */
}billSpecST;


typedef struct 
{
    uint8       pinKey[16 + 1];
    uint8       macKey[16 + 1];
    uint8       voucherKey[16];
    uint8       masterKey[16 + 1];
    uint8       supervisorKey[8];
    uint8       merchantKey[8];//HNO_DEBUG3
}terminalKeyST;

typedef struct 
{
    uint8       accountBalance[12 + 1];
    uint8       withdrawalAmount[12 + 1]; // mablaghe ghabele bardasht
}balanceSpecST;

typedef struct{
   uint8         macVersion[3 + 1];
   uint8         pinVersion[3 + 1];   
}versionKeyST;

typedef struct{
    uint8       masterKCV[8 + 1];
    uint8       macKCV[8 + 1];
    uint8       pinKCV[8 + 1];
    uint8       voucherKCV[8 + 1];
}kcvST;

typedef struct{
    uint8   personnelID[4 + 1];
    uint8   lastName[30];
    uint8   codeMeli[10 + 1];
    uint8   expDate[5 + 1];
    uint8   PAN[19 + 1];
    uint8   activity[100];
}supervisorSpec;

//MRF_TOPUP
typedef struct
{
    int     type;               //Normal, Amazing 
    int     productTypeCount;
    int     operatorType;       //MTN,MCI,RIGHTEL
    uint8   amount[12 + 1]; 
    uint8   mobileNo[10 +1];
}topupST;

//HNO_PCPOS
//HNO_IDENT_PCPOS
typedef struct{
//    uint8 advertise[100];
    uint8 advertise1[200];
    uint8 advertise2[200];
    uint8 context[30];        
}informationST;

typedef struct 
{ 
    
    
    uint8               fileSeq;
    uint8               fileVersionFlags[14 + 1];
    uint8               reversTryCount;     // for use in reversal tries.
    uint16              functionCode;
//////////////////////////////////////////////
    loanPayST           loanPay;       
//    versionsST          flgVersion; //HNo_COMMENT
    uint16              responseCode;
    uint16              responseStatus; //mgh_93: save state of response after check and parse message
    generalConnInfoST   generalConnInfo;
    terminalKeyST       terminalKey;
    uint8               transType;
    uint8               amount[12 + 1]; //changed
    uint8               preAmount[12 + 1];//MRF_NEW17
    uint8               reward[350];
    uint8               rollAmount;
    billSpecST          billSpec;
    uint8               approvalCode[6 + 1];
    uint8               retrievalReferenceNumber[12 + 1];
    balanceSpecST       balanceSpec;
    dateTimeST          dateTime;
    dateTimeST          SWdateTime;
    settlementSpecST    settlementSpec;
    buyChargeSpecST     buyChargeSpec; 
    chargeST			charge;//HNO_CHARGE
//    buyChargeSpecST     buyChargeOfflineSpec[99]; 
//    buyChargeSpecST     boughtChargeOffline[15][99];
    //-- scheduleST          schedule;
    versionKeyST        versionKey;
    merchantSpecST      merchantSpec;
    cardSpecST          cardSpec;  
//    uint8               topUpChargeMobileNo[20];
    kcvST               kcv;
    uint8               voucherKeyDecryptor[16 + 1];
    supervisorSpec      supervisor;
    ETCST               ETC;
    topupST             topup; //MRF_TOPUP
    informationST       information;//HNO
    uint8               IMSI[30 + 1]; // MRF_NEW3: Based On the Protocol Should be 999 but we have not Capacity
    rollRequestST		rollRequest;//HNO_ADD_ROLL
    charityST			charity;
    uint8               depositID[13 + 1];//#MRF_970813
    uint8               tipAmount[12 + 1];
    uint8               pcPos;//+HNO_970924
	uint8               taxNumber[22 + 1];//+ABS_980927
	uint8               shebaField[999 + 1];//+ABS_981002
}messageSpecST;

typedef struct 
{ 
    uint8               fileSeq;
    uint8               fileVersionFlags[14 + 1];
    uint8               reversTryCount;     // for use in reversal tries.
    uint16              functionCode;
    loanPayST           loanPay;       
//    versionsST          flgVersion; //HNo_COMMENT
    uint16              responseCode;
    uint16              responseStatus; //mgh_93: save state of response after check and parse message
    generalConnInfoST   generalConnInfo;
    terminalKeyST       terminalKey;
    uint8               transType;
    uint8               amount[12 + 1]; //changed
    uint8               preAmount[12 + 1];//MRF_NEW17
    uint8               reward[350];
    uint8               rollAmount;
    billSpecST          billSpec;
    uint8               approvalCode[6 + 1];
    uint8               retrievalReferenceNumber[12 + 1];
    balanceSpecST       balanceSpec;
    dateTimeST          dateTime;
    dateTimeST          SWdateTime; //MRF_NEW2
    settlementSpecST    settlementSpec;
    buyChargeSpecST     buyChargeSpec; 
    chargeST			charge;//HNO_CHARGE
//    buyChargeSpecST     buyChargeOfflineSpec[99]; 
//    buyChargeSpecST     boughtChargeOffline[15][99];
    //-- scheduleST          schedule;
    versionKeyST        versionKey;
    merchantSpecST      merchantSpec;
    cardSpecST          cardSpec;  
//    uint8               topUpChargeMobileNo[20];
    kcvST               kcv;
    uint8               voucherKeyDecryptor[16 + 1];
    supervisorSpec      supervisor;
    ETCST               ETC; //MRF_ETC
    topupST             topup; //MRF_TOPUP
    informationST       information;//HNO
    uint8               IMSI[99 + 1]; // MRF_NEW3: Based On the Protocol Should be 999 but we have not Capacity
    rollRequestST		rollRequest;//HNO_ADD_ROLL
    charityST			charity;
    uint8               depositID[11 + 1]; //MRF_NEW16
    uint8               tipAmount[12 + 1];//MRF_NEW17
    
}messageSpecSTOld;

uint8 initMessageSpec(filesST* files, uint8 transType, messageSpecST* messageSpec);

uint8 saveMessageSpec(filesST* files, messageSpecST* messageSpec);

uint8 initVersionKeySpec(filesST* files, versionKeyST* versionKey);

uint8 readVersionKeySpec(filesST* files, versionKeyST* versionKeySpec);

uint8 writeVersionKeySpec(filesST* files, versionKeyST versionKeySpec);

uint16 setMac(uint8* key, uint8* buffer, uint8 headerLen, uint16 packedSize);

int checkResponseValidity87(messageSpecST* messageSpec, int type, uint8* response, uint16 responseLen, 
                          uint8* request, uint16 requestLen);

int createSettlementMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseSettlementResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int createReversalMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseField63ForBuyChargeOfflineResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int createInitializeMessage87(uint8* buffer, messageSpecST* messageSpec);

int parseInitializeResponse87(uint8* response, int16 responseLen, messageSpecST* messageSpec);

int createBuyMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseBuyResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int createBalanceMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseBalanceResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int createBillPayMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseBillPayResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int createBuyChargeMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

//int createBuyChargeOfflineMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseBuyChargeResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int parseField63ForBuyChargeResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

void resetVoucherData(void);

int createloanPayTransMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int createloanPayInqueryMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int createLoanPayTrackingMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseLoanPayTransResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int parseLoanPayInqueryResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int parseLoanPayTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

uint8 readLoanInfo(filesST* files, messageSpecST* messageSpec);

uint8 saveLoanInfo(uint8* files, loanPayST loanSpec);

int createETCTransMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int createETCInqueryMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int createETCTrackingMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseETCTransResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int parseETCInqueryResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

int parseETCTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

uint8 readETCInfo(filesST* files, messageSpecST* messageSpec);

uint8 saveETCInfo(uint8* fileName, ETCST ETCSpec);//ABS:CHANGE:961205

int createTopupMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

int parseBuyChargeTopupResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

uint8 saveRollInfo(uint8* files, rollRequestST rollReuest);

int createRollRequestMessage87(uint8* buffer, messageSpecST* messageSpec);

int createRollRequestMessage87(uint8* buffer, messageSpecST* messageSpec);

int createRollTrackingMessage87(uint8* buffer, messageSpecST* messageSpec);//HNO_ADD_ROLL_TRACKING

int parseRollTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);//HNO_ADD_ROLL_TRACKING

//HNO_CHARGE
uint8 savechargeInfo(chargeST charge, int count);

//HNO_CHARITY
int createCharityMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec);

//HNO_CHARITY
int parseCharityResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec);

#ifdef	__cplusplus
}
#endif

#endif // MESSAGING_H_INCLUDED
