#ifndef _N_COMMON_H
#define	_N_COMMON_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "N_typedefs.h"
    
#define N_FONT_FARSI                           0xA001
	
#define ALIGN_LEFT                              0
#define ALIGN_CENTER                            1 //IDENT
#define ALIGN_RIGHT                             2

#define  SMALL_STRING                           1
#define  NORMAL_STRING                          2
#define  BIG_STRING                             3
#define  SMALL_INVERT_STRING                    4
#define  NORMAL_INVERT_STRING                   5
#define  BIG_INVERT_STRING                      6

#define FARSI                                   1
#define ENGLISH                                 2

// EVENT TYPES
#define NO_EVENTS                               0
#define SCHEDULED_ACT_EVENTS                    1
#define MAGNETIC_CARD_READER_EVENTS             2
#define MIFARE_CARD_EVENTS                      3
#define KBD_EVENTS                              4
#define TMS_EVENTS                              5
#define TOUCH_EVENTS                            6
#define SMART_CARD_READER_EVENTS                7
#define ACTIVE_MERCHANT_EVENTS                  8
#define LAN_CABEL_EVENTS                        9  //+MRF_970820
#define DUMMY_EVENTS                            10
    
#define FILE_NAME_LENTGH                        32

/** Null pointer or value */
#ifndef NULL   // PDJ-TG
#define NULL                                    0
#endif
   
/**Boolean values */
#ifndef FALSE   // PDJ-TG
#define FALSE                                   0
#endif
 
/**Boolean values */
#ifndef TRUE   // PDJ-TG
#define TRUE                                    1
#endif

#define SUCCESS                                 0
#define FAILURE                                10000//0x1000000

//Index of Services
#define SERVICE_ACTIVE_TEST_INDEX               0
#define SERVICE_LOGON_INDEX                     1
#define SERVICE_SETTLEMENT_INDEX                2
#define SERVICE_REVERSAL_INDEX                  3
#define SERVICE_TMS_INDEX                       4 
#define SERVICE_PERIODIC_TMS_INDEX              5 
#define SERVICE_TMS_UPDATE_INDEX                6  
#define SERVICE_TMS_CONFIG_INDEX                7 
  
//Timeouts
#define ERR_TIMEOUT                             10//mgh_94 5
#define WARN_TIMEOUT                            5
#define INFO_TIMEOUT                            5
#define DISPLAY_TIMEOUT                         30
#define GET_INPUT_TIMEOUT                       30
#define GET_CONFIRM_TIMEOUT                     15

#define SHOW_DC_MSG                             1    
#define NOT_SHOW_DC_MSG                         2
#define NO_DISPLAY                              2    
#define SERVERS_COUNT                           10    
    
#define	NO_CARD									3	//HNO_ADD
/**  Operators ID*/
#define MCI                                 	"919"
#define IRANCELL                            	"936"
#define RIGHTEL                             	"921"
	
// --MRF IS DUPLICATE uint8 printerLogoFile[FILE_NAME_LENTGH];
    
/** Type of oprators for Charges.*/	
enum OperatorTypeEN
{
    MULTI_MCI,
    MULTI_IRANCELL,
    MULTI_RIGHTEL,
    MULTI_TALIYA,
};

//MRF_TOPUP
enum chargeTypeEN
{
    NORMAL,
    AMAZING,
};

/**Level of logs.*/
enum logLevelTypeEN
{
    RELEASE,                /*!< Release Mode */
    TRACE,                  /*!< Trace Mode */
    DEBUG,                  /*!< Debug Mode */
    FATAL,                  /*!< Fatal Mode */
    TEST                    //MRF_954
};

//MRF_NEW14
/**PORT of logs.*/
enum logPortEN
{
    USB,
    COMPORT1,//HNO_IDENT11
    COMPORT2,
    NO_PORT //ABS:COMMENT
    
};

/**State of Aligns used for convertor.*/
enum alignStateEN
{
   LEFT,            /*!< Align Left*/
   RIGHT            /*!< Align Right*/
 };
        
/**State of languages used for TTF convertor.*/ 
enum languageEN
{
    LA_NONE,            /*!< No Language*/
    LA_FA,              /*!< Is Farsi Character*/
    LA_EN,              /*!< Is English character*/
    LA_NUM,             /*!< Is Number*/
    LA_EMPTY              /*!< Is empty*/
};


enum trasPrintEN
{
   NO_REPRINT        = 0,
   REPRINT           = 1,
   CHARGE_REPRINT    = 2
};

/**There are many files that used in different sections.*/
typedef struct 
{
    uint8 id;                                                /*!< File of ID*/
    uint8 macKeyFile[FILE_NAME_LENTGH];                      /*!< MAC key File*/
//  --MRF_IDENT  uint8 homa[FILE_NAME_LENTGH];                            /*!< Homa File*/
    uint8 masterKeyFile[FILE_NAME_LENTGH];                   /*!< Master key file*/
    uint8 pinKeyFile[FILE_NAME_LENTGH];                      /*!< Pin key file*/
    uint8 prefixFile[FILE_NAME_LENTGH];                      /*!< Prefix file*/
    uint8 merchantSpecFile[FILE_NAME_LENTGH];                /*!< Merchant specification file*/
    uint8 versionFile[FILE_NAME_LENTGH];                     /*!< Version of file*/ 
    uint8 binFile[FILE_NAME_LENTGH];                         /*!< Merchant specification file*/
    uint8 scheduleFile[FILE_NAME_LENTGH];                    /*!< Scheduale of file*/
// --MRF_IDENT  uint8 existUnSuccessSettelmentFile[FILE_NAME_LENTGH];    /*!< Exist UnSuccess Settelment File*/
    uint8 reversalTransFile[FILE_NAME_LENTGH];               /*!< Reversal Transaction File*/
//  --MRF_IDENT  uint8 transactionBatchFile[FILE_NAME_LENTGH];            /*!< Transaction Batch File*/
    uint8 settelmentInfoFile[FILE_NAME_LENTGH];              /*!< Settelment Information File*/
    uint8 reversalReceiptFile[FILE_NAME_LENTGH];             /*!< Reversal Receipt File*/
    uint8 generalConnInfoFile[FILE_NAME_LENTGH];             /*!< General Connection Information File*/
    uint8 configXml[FILE_NAME_LENTGH];                       /*!< Config XML File*/
//  --MRF_IDENT  uint8 swVersionNumberFile[FILE_NAME_LENTGH];             /*!< Switch Version Number File*/
    uint8 settingFile[FILE_NAME_LENTGH];                     /*!< Setting File*/
    uint8 customerReceiptFile[FILE_NAME_LENTGH];             /*!< Customer Receipt File*/
//    uint8 posParam[FILE_NAME_LENTGH];                        /*!< POS Parameters File*/
//    uint8 pppUserPass[FILE_NAME_LENTGH];                     /*!< ppp User and Password File*/
    uint8 connFiles[6][FILE_NAME_LENTGH];                    /*!< Connection Files*/
    uint8 logonInfoFile[FILE_NAME_LENTGH];                   /*!< Logon Information File*/
    uint8 displayLogoFile[FILE_NAME_LENTGH];                 /*!< Display Logo File*/
    uint8 printerLogoFile[FILE_NAME_LENTGH];                 /*!< Printer Logo File*/
    uint8 generalKey[FILE_NAME_LENTGH];                      /*!< General Key File*/
    uint8 versionKeyFile[FILE_NAME_LENTGH];                  /*!< version mac & pin File*/
    uint8 kcvFile[FILE_NAME_LENTGH];
    uint8 voucherKeyFile[FILE_NAME_LENTGH];
    uint8 ETCInfo[FILE_NAME_LENTGH]; //MRF_ETC
    uint8 ETCTemp[FILE_NAME_LENTGH]; //MRF_ETC
    uint8 loanInfo[FILE_NAME_LENTGH]; //MRF_LOAN
    uint8 loanTemp[FILE_NAME_LENTGH]; //MRF_LOAN
    uint8 rollTemp[FILE_NAME_LENTGH]; //HNO_ADD_ROLL
    uint8 chargeInfo[FILE_NAME_LENTGH];//HNO_CHARGE
    uint8 pcPosFiles[FILE_NAME_LENTGH];//HNO_TCP
}filesST;

/**Determine Service Type.*/
typedef struct 
{
    uint32 serviceType;
}transactionFuncitonParamST;


/**Each function maybe have one or several argument and is unclear number of argument this structure help us have variable argument. */
struct argumentST
{
    void*   argumentName;           /*!< Argument Name */
    struct  argumentST*   next;     /*!< Next argument can be exist */
};
typedef	struct argumentST  argumentListST;


/**Maybe use several server and each server have attributes such as Files,Schedules,visiblity.*/
typedef struct serverSpecST
{
    filesST     files;                  /*!< Files are on server */
    uint8       schedules[32];          /*!< Schaedules of server*/
    uint8       isMenuVisible;          /*!< Visiblity */
    struct      serverSpecST* next;     /*!< Next Server */
}serverSpecST;


/**Brands of POS terminals.*/
enum POSSpecifierEN
{
    JUST_CASTLES = 1,           /*!< Castles Brand */
    JUST_INGENICO = 2,          /*!< Ingenico Brand */
    JUST_VERIFONE = 4,          /*!< Verifone Brand */
    ALL_POS = 8                 /*!< All of Brands */
};


#ifdef	__cplusplus
}
#endif

#endif	/* _COMMON_H */

