#ifndef N_SCHEDULING_H_INCLUDED
#define N_SCHEDULING_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif


/** number of scheduled command number */
#define SCHEDULES_NUMBER                            8 //mgh 6
/** reversal of scheduled command number */
#define SCHEDULES_REVERSAL                          0
/** settlement of scheduled command number */
#define SCHEDULES_SETTLEMENT                        1
//mgh #define SCHEDULES_ACTIVE_TEST                       2
/** log on scheduled command number */
#define SCHEDULES_LOGON                             3
/** TMS scheduled command number */
#define SCHEDULES_TMS                               4
/** TMS periodic scheduled command number */
#define SCHEDULES_PERIODIC_TMS                      5
/** TMS update scheduled command number */
#define SCHEDULES_TMS_UPDATE                        6  
/** TMS scheduled command number */
#define SCHEDULES_TMS_CONFIG                        7    
/** reversal time schedules */   
#define SCHEDULES_REVERSAL_TIME                     30
/** settlement time scheduled */
#define SCHEDULES_SETTLEMENT_TIME                   30
/** log on time scheduled */
#define SCHEDULES_LOGON_TIME                        30

//mgh #define SCHEDULES_ACTIVE_TEST_TIME                  (5 * 24 * 60 * 60)    
//mgh #define SCHEDULES_ACTIVE_TEST_NEXT_TIME             30    
    
    
/** period of TMS time schedules */
#define SCHEDULES_PERIODIC_TMS_DATE                1//3 days later //MRF_TMS
    
#define SCHEDULES_PERIODIC_TMS_TIME                1440//1 days later   1440 is minute 
    
/** schedules of next time that TMS should be run  */   
#define SCHEDULES_TMS_RUN_NEXT_TIME                 30 
    
/** schedules of next time that TMS should be update  */ 
#define SCHEDULES_TMS_UPDATE_NEXT_TIME              30 
    
/** schedules of next time that TMS should be configuration  */ 
#define SCHEDULES_TMS_CONFIG_NEXT_TIME              30   
    
/** maximum time of schedule to permit  */    
#define MAX_SCHEDULE_TIME                           (5 * 30 * 24 * 60)//(2 * 24 * 60)    


/** Schedule command */
typedef struct
{
    uint8       set;                  /*!<  is schedule set or not? */
    dateTimeST  datetime;             /*!<  schedule date and time */
    dateTimeST  lastDatetime;         /*!<  schedule last date and time */
    dateTimeST  orginDatetime;        /*!<  schedule origin date and time */
    uint8       scheduleCount;        /*!<  count of schedule */
}scheduleST;

/** next schedule command */
typedef struct
{
    uint8      group;                   /*!<   schedule group */
    uint8      existance;               /*!<  is next schedule existance or not? */
    int8       type;                    /*!<  type of schedule */
    dateTimeST dateTime;                /*!<  date time of next schedule */
}nextScheduleST;


int16 readSchedules(filesST* files);

scheduleST getSchedule(uint8 scheduleIndex);

void doSchedules(serverSpecST* serverSpec, nextScheduleST* currentSchedule);

// void setActiveTestCommandAfterUnsuccessSend(filesST* files);

void setPeriodicTMSSchedule(filesST* files, uint8 day);

void setTMSRunSchedule(filesST* files);

void setTMSUpdateSchedule(filesST* files);

void setTMSConfigSchedule(filesST* files);

void resetPeriodicTMSSchedule(filesST* files);

void resetTMSRunSchedule(filesST* files);

void resetTMSUpdateSchedule(filesST* files);

void resetTMSConfigSchedule(filesST* files);

void resetLogonSchedule(filesST* files);

void setLogonScheduleAfterInitSchedule(filesST* files);

void setLogonScheduleAfterUnsuccessLogon(filesST* files);

void setLogonScheduleForTomorrow(filesST* files);

void setReversalScheduleAfterInitSchedule(filesST* files);

void setReversalScheduleAfterUnsuccessReversal(filesST* files);

void resetReversalSchedule(filesST* files);

void setSettlementScheduleAfterUnsuccessSettlement(filesST* files);

void setSettlementScheduleAfterInitSchedule(filesST* files);

void resetSettlementSchedule(filesST* files);

//void readActiveTestPeriod(filesST* files, uint32* activeTestPeriodSec, uint8* testActivity);
//
//void setActiveTestCommandAfterTransaction(filesST* files);
//
//void setActiveTestCommandAfterUnsuccessSend(filesST* files);

void initSchedules(filesST* files);

void nextSchedule(serverSpecST* serverSpec, nextScheduleST* schedule);

uint8 isScheduleSet(filesST* files, int scheduleType);

// void resetActiveTestSchedule(filesST* files);

void setTMSScheduleAfterUnsuccessTMS(filesST* files);

#ifdef	__cplusplus
}
#endif

#endif // SCHEDULING_H_INCLUDED

