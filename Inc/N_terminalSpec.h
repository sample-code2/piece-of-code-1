#ifndef N_TERMINALSPEC_H_INCLUDED
#define N_TERMINALSPEC_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif

#define     TERMINAL_SPEC_FILE                      "terminf0100"
#define     POS_SERVICE_FILE                        "service0100" //MRF_NEW12
#define     POS_TIME_FILE                           "ptime0100" //MRF_NEW19
//#define     NEXT_DATE_FILE                          "nxdt0100" //--MRF_NEW19 970624
    
#define     TTF_FONT                                "1" //MRF_NEW15
#define     FNT_FONT                                "2" //MRF_NEW15     
    
/** terminal specification*/   
typedef struct 
{
    uint8  LANCapability;                /*!< LAN capability have or not */
    uint8  supportTTFFont;               /*!< Terminal support TTF Font or Not */
    uint8  supportTTFFontPrint;
    uint8  graphicCapability;            /*!< Terminal support Image or Not */
    uint8  GPRSCapability;
    uint8  modemCapability;              /*!< modem capability have or not */
    uint8  TCPModemCapability;
    uint8  contactlessMode;              /*!< show contactless mode */
    uint8  contactlessCapability;
    uint8  portableCapability;           /*!< portable capability have or not */
    uint8  TMSCapability;
    uint8  pcPosCapability;     //HNO_IDENT2
    uint8  deviceModel[20]; //MRF_NEW16
	uint8  OTPCapability;//ABS
//    uint8  WifiCapability;      //MRF_NEW3
//    uint8  bluetoothCapability; //MRF_NEW3
//    uint8  touchCapability;              /*!< touch capability have or not */
    
}terminalSpecST;

//MRF_NEW12
typedef struct 
{
    uint8  loanPayService;
    uint8  ETCService;
    uint8  PcPOSService;
    uint8  shiftService; //MRF_SHIFT
    uint8  depositID;   //MRF_NEW16
    uint8  pinPad;      //MRF_NEW16
    uint8  discount;     //MRF_NEW17
    uint8  tip;         //MRF_NEW17
    uint8  merchantReceipt;//MRF_NEW19
    uint8  confirmationAmount;//MRF_NEW19
	uint8  TMSService;
}serviceSpecST; 

uint8 terminalInitiation(serverSpecST* serverSpec);

uint8 initTerminalSpec(uint8 flag);

void getVersion (uint8* verNumber);

void getAppName(uint8* appName);

uint8 initUserSetting(void);

void userSetting(void);

uint8 checkPrerequisitDisableShift(filesST* files, uint8 check);

void setupServices(argumentListST* args);

uint8 saveAppVersion(void);

uint8 readLastAppVersion(uint8* verNumber);

uint8 checkAppVersion(void);

void setupMerchantServices(void);

#ifdef	__cplusplus
}
#endif

#endif // TERMINALSPEC_H_INCLUDED
