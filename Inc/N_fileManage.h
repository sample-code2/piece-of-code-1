
#ifndef _N_FILEMANAGE_H
#define	_N_FILEMANAGE_H

#ifdef	__cplusplus
extern "C" {
#endif

int16 fileReadTo(uint32 fileId, int8* buffer, int8 endChar);


int16 fileEmpty(uint8* fileName);


int16 updateFileInfo(uint8* fileName, void* stInfo, uint32 structLen);


int16 appendFileInfo(uint8* fileName, void* stInfo, uint32 structLen);


int16 appendFixedFileInfo(uint8* fileName, void* stInfo, uint32 structLen, int16 maxRecord);


int16 appendOpenedFile(uint32 fileId, void* stInfo, uint32 structLen);


int16 readFixedFileLastRecord(uint8* fileName, void* stInfo, uint32 structLen,
                                 int16 maxRecord);

int16 updateFileLastRecord(uint8* fileName, void* stInfo, int16 structLen);


int16 updateFixedFileLastRecord(uint8* fileName,void* stInfo, uint32 structLen,
                                   uint32 maxRecord);

int16 deleteFileLastRecord(int8* fileName, uint32 structLen);


int16 fileReadLine(uint32 fileId, void* buff, uint32* readLen);


int16 readFileInfo(uint8* fileName, void* data, uint32* len);


int16 readFixedFileInfo(uint32 fileId, void* stInfo, uint32 structLen, 
                           uint16 recordNum, uint32 maxRecord);

int16 readFixedFileInfoArray(uint8* fileName, /*MRF_ETC uint8**/void* buffer, uint32 structLen, int startRecord,
                                uint16 neededNum, uint32 maxRecord);


int16 unpackFileInfo(uint8* fileName, int8* data, int8* outDataBuff[56],
                        uint16 lineNum);


int searchFirstOccurDateInFixedFile(uint32 fileId, uint32 structLen, 
                                    uint32 dateOffset, uint32 startDate, 
                                    uint32 endDate, uint32 maxRecord, 
                                    uint32 low, uint32 high);

int searchLastOccurDateInFixedFile(uint32 fileId, uint32 structLen, 
                                   uint32 dateOffset, uint32 startDate, 
                                   uint32 endDate, uint32 maxRecord, 
                                   uint32 low, uint32 high);

int searchDateRangeInFixedFile(uint8* fileName, uint32 structLen, uint32 dateOffset, uint32 startDate,
                               uint32 endDate, uint32 maxRecord, int* startRecordNum, int* endRecordNum);


int searchFirstOccurDateTimeInFixedFile(uint32 fileId, uint32 structLen, 
                                    uint32 dateOffset, dateTimeST* startDateTime, dateTimeST* endDateTime, 
                                    uint32 maxRecord, 
                                    uint32 low, uint32 high);


int searchLastOccurDateTimeInFixedFile(uint32 fileId, uint32 structLen, 
                                   uint32 dateOffset, dateTimeST* startDateTime, dateTimeST* endDateTime,
                                   uint32 maxRecord, uint32 low, uint32 high);

int searchDateTimeRangeInFixedFile(uint8* fileName, uint32 structLen, uint32 dateOffset, /*uint32 startDate,
                                uint32 endDate, uint32 startTime, uint32 endTime,*/
								dateTimeST* startDateTime, dateTimeST* endDateTime, uint32 maxRecord, 
                                int* startRecordNum, int* endRecordNum);

int searchSTANInFixedFile(uint8* fileName, uint32 structLen, uint32 dateOffset, uint32 STAN,
						uint32 maxRecord, int* recordNum);

int searchFirstOccurSTANInFixedFile(uint32 fileId, uint32 structLen, uint32 STAN, 
        uint32 dateOffset, uint32 maxRecord, uint32 low, uint32 high);

int searchLastOccurSTANInFixedFile(uint32 fileId, uint32 structLen, 
                                   uint32 dateOffset, uint32 STAN,
                                   uint32 maxRecord, uint32 low, uint32 high);
#ifdef	__cplusplus
}
#endif

#endif	/* _N_FILEMANAGE_H */

