#ifndef _N_terminalReport_H
#define	_N_terminalReport_H

#ifdef	__cplusplus
extern "C" {
#endif

uint8 hardwareReport(void);

uint8 systemErrorList(argumentListST* args);

void addSystemErrorReport(int32 errorCode, int errorType);

void settingReport(filesST* files);

#ifdef	__cplusplus
}
#endif

#endif	/* _terminalReport_H */

