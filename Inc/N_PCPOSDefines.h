
//#ifdef PC_POS//MRF_NEW5
/* 
 * File:   ET_PCPOSDefines.h
 * Author: maryam.ghods
 *
 * Created on December 27, 2011, 3:44 PM
 */

#ifndef _ET_PCPOSDEFINES_H
#define	_ET_PCPOSDEFINES_H

#ifdef	__cplusplus
extern "C" {
#endif

//#ifndef INGENICO
#define EOS                     0x00        /* End of string */

#define NUL                     0x00        /* Null character */
#define SOH                     0x01        /* Start of hearder */
#define STX                     0x02        /* Start of text */
#define ETX                     0x03        /* End of text */
#define EOT                     0x04        /* End of transmission */
#define ENQ                     0x05        /* Inquiry */
#define ACK                     0x06        /* Acknowledge */
#define BEL                     0x07        /* Bell */
#define BS                      0x08        /* Back space */
#define HT                      0x09        /* Horizontal tab */
#define LF                      0x0A        /* Line feed */
#define VT                      0x0B        /* Vertical tabulation */
#define FF                      0x0C        /* Form feed */
#define CR                      0x0D        /* Carriage return */
#define SO                      0x0E        /* Shift out */
#define SI                      0x0F        /* Shift in */
#define DLE                     0x10        /* Data link escape */
#define DC1                     0x11        /* Device control 1 */
#define DC2                     0x12        /* Device control 2 */
#define DC3                     0x13        /* Device control 3 */
#define DC4                     0x14        /* Device control 4 */
#define NAK                     0x15        /* Negative acknowledge */
#define SYN                     0x16        /* Synchronus idle */
#define ETB                     0x17        /* End of block */
#define CAN                     0x18        /* Cancel */
#define EM                      0x19        /* End of medium */
#define SUB                     0x1A        /* Substitute */
#define ESC                     0x1B        /* Escape */
#define FS                      0x1C        /* File seperator */
#define GS                      0x1D        /* Group seperator */
#define RS                      0x1E        /* Record seperator */
#define US                      0x1F        /* Unit seperator */  
#define COM_TIMEOUT       	 	0xFF
#endif
    
#ifdef	__cplusplus
}
#endif

//#endif	/* _ET_PCPOSDEFINES_H */

//#endif
