#ifndef _N_DISPLAY_H
#define	_N_DISPLAY_H

#ifdef	__cplusplus
extern "C" {
#endif

//MRF
#ifdef CASTLES_V5S
    /**Number of character can be show on one line */
    #define DISPLAY_CHARACTER_COUNT	24
#elif defined(IWL220) || defined(ICT250)//HNO_ADD
	/**Number of character can be show on one line */
	#define DISPLAY_CHARACTER_COUNT	 28
#else
    /**Number of character can be show on one line */
    #define DISPLAY_CHARACTER_COUNT	16    
#endif
//HNO_ADD
#define DISPLAY_CHARACTER_COUNT_CHECK 		TRUE

/**Features of Edit for show on display*/
enum editFeatureEN
{
    SHOW_EDIT = 1,              /*!< just Show "Edit" */
    SHOW_EDIT_RIGHT_ARROW,      /*!< Show "Edit" and ">>" */
    SHOW_EDIT_LEFT_ARROW,       /*!< Show "Edit" and "<<" */
    SHOW_ARROWS,                /*!< show "<<" and ">>" */
    SHOW_RIGHT_ARROW,           /*!< Just show ">>" */
    SHOW_LEFT_ARROW,            /*!< Just show "<<" */
    SHOW_ALL,                    /*!< Show "Edit" and ">>' and "<<" */
    SHOW_NONE               //MRF_TMS
};

/**Determine type of messages*/
enum messageTypeEN
{
    MSG_ERROR = 1,          /*!< Is error message*/
    MSG_WARNING,            /*!< Is warning message*/
    MSG_INFO,               /*!< Is info message*/
    MSG_CONFIRMATION        /*!< Is confirmation message*/
};

/**For clean each line of display used*/
enum clearDisplayEN
{
    NO_LINES    = 0,        /*!< None line*/
    LINE_1      = 1,        /*!< Line 1*/
    LINE_2      = 2,        /*!< Line 2*/
    LINE_3      = 4,        /*!< Line 3*/
    LINE_4      = 8,        /*!< Line 4*/
    ALL_LINES   = 16,       /*!< All Lines*/
};


void displayString(uint8* string, uint8 line);

void displayInvertString(uint8* string, uint8 line);

void displayPic(uint8 picType);

void displayMainPage(uint8 forcedDisplayLogo);

void displayDateTimeEnglish(dateTimeST* dateTime);

void displayDateTimeFarsi(dateTimeST* dateTime);

uint8 displayScrollableWithHeader(uint8* title, uint8 lines[][32], uint16 linesNum, uint32 timeout, uint8 confirmable , uint8 header);

uint8 displayScrollable(uint8* title, uint8 lines[][32], uint16 linesNum, uint32 timeout, uint8 confirmable);

uint8 displayMessageBox(uint8* msg, uint8 type);

uint8 displayMessage(uint8* msg, uint8 line, uint8 clearLines);

uint8 displayBalance(uint8* availableAmount, uint8* legerAmount);

void justifyOneStringFarsi(uint8* s1, uint8* outStr, uint8 size, uint8 align);

void justifyTwoStringFarsi(uint8* s1, uint8* s2, uint8* outStr, uint8 size);

void displayStringFarsi(uint8* string, uint8 line, int size, int align);

void displayStringEnglish(uint8* string, uint8 line, int size, int align);

void displayAndEditValue(uint8* title, uint8* value, int isIP, uint8 editLineFeature);

void displayInvertStringEnglish(uint8* string, uint8 line, int size, int align); 

void displayInvertStringFarsi(uint8* string, uint8 line, int size, int align); 

void displayDateTimeFarsiF4(dateTimeST* dateTime);

uint8 displayBillPaymentInfo(billSpecST* billSpec, uint8* billCount);

uint8 displayLoanPay( cardSpecST* cardSpec, loanPayST* loanPay);

uint8 displayETCInfo(ETCST* ETC , uint8 checked);

void LEDBlink(uint8 loop);

#ifdef	__cplusplus
}
#endif

#endif	/* _lcd_H */

