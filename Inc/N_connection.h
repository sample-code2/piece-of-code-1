
#ifndef _N_connection_H
#define	_N_connection_H                

#ifdef	__cplusplus
extern "C" {
#endif
/** to not display connection error */   
#define NOT_DISPLAY_CONN_ERROR           0           
/** to display connection error */
#define DISPLAY_CONN_ERROR               1           
/** not show value of amount */   
#define NOT_SHOW_VALUE_AMOUNT            1000000000  
/** receive data from the Ethernet port result */   
#define RX_READY_FAIL                    1            
/** receive data from the port is fail */    
#define RX_DATA_FAIL                     2       
/** receive data from the port success */    
#define RX_DATA_SUCCESS                  3           
/** the length of invalid received data*/    
#define INVALID_RECEIVED_DATA_LEN        4            
/** when connection failed*/
#define CONNECTION_FAILED                0            
/** when connection success*/    
#define CONNECTION_SUCCESS               1          
/** when connection canceled by user */    
#define CONNECTION_USER_CANCEL           2           
/** when connection retry */    
#define CONNECTION_RETRY                 3   
    

/**
 * information of HDLC connection
 */	
typedef struct 
{
    //uint8  enabled;
    uint8  phone[17];                /*!< phone number to have connection */
    uint8  prefix[7];                /*!< prefix of phone number */
    uint8  useToneDialing;           /*!< use tone dialing */
    uint32 retries;                  /*!< count of retries to dial connection */
    uint32 communicationTimeout;     /*!< time out of communication */
}HDLCConnInfoST;

/**
 * configuration of PPP connection
 */	
typedef struct 
{
    uint8   user[17];               /*!< user identity */
    uint8   password[17];           /*!< user password */
}PPPConfigST;

/**
 * configuration of PPP connection
 */
typedef struct 
{
    uint8           ipAddress[5];       /*!< IP address */
    uint32          tcpPort;            /*!< tcp port */
    HDLCConnInfoST  HDLCConnInfo;       /*!< connection information of HDLC */
    PPPConfigST     PPPConfig;          /*!< configuration of PPP */
}PPPConnInfoST;

/**
 * information of LAN connection
 */
typedef struct 
{ //MRF_NEW : CHANGE 
    //uint8  enabled;
    uint8  ipAddress[15];                
    uint32 tcpPort;                     /*!< tcp port */
    uint8  localIpAddress[15];           /*!< local IP address */
    uint8  gateway[15];                  /*!< gateway */
    uint8  networkMask[15];              /*!< network mask */
    uint8  dhcp;  
    uint8  DNSCapability;//+HNO_980312
    uint8  DNS1[15];   //+HNO_980314
    uint8  DNS2[15];  //+HNO_980314              
    uint8  URL[50];//+HNO_980314
   //uint8  autoConn;
}LANConnInfoST;
 
/**
 * information of GPRS connection
 */   
typedef struct 
{
    uint8 connTypePcPos[8];
    uint32 pcposPort; 
    LANConnInfoST lanPcPos;
}PcPosConnInfoST;
 
/**
 * information of GPRS connection
 */   
typedef struct 
{
    uint8  ipAddress[15];            /*!< IP address */
    uint32 tcpPort;                 /*!< tcp Port */
    uint8  apn[20];                 /*!< access point name */
    uint8  pin[8 + 1];                  /*!< pin number */
    uint32* socket; //MRF_GPRS
    uint8  DNSCapability;//+HNO_980423
    uint8  URL[50];//+HNO_980423
}GPRSConnInfoST;

/**
 * general connection information
 */
typedef struct 
{
    uint8  connectionCount;         /*!< count of connections */
    uint8  connectionsType[7];      /*!< type of connection */
    uint32 POSNii;                  /*!< Nii of POS */
    uint32 SWINii;                  /*!< Nii of switch */
}generalConnInfoST;

void setGeneralConnInfo(uint8 serverID, generalConnInfoST* generalConnInfo);

generalConnInfoST getGeneralConnInfo(uint8 serverID);

uint8 disconnectAndClose(int argumentCount, ...);

uint8 disconnectServer(void);

uint8 initHDLCConn(filesST* files, uint8 connectionCount);

uint8 initPPPConn(filesST* files, uint8 connectionCount);

uint8 initLanConn(filesST* files, uint8 connectionCount);

uint8 initGPRSConn(filesST* files, uint8 connectionCount);

void incompleteIpToStr(uint8* ip, int ipIndex, int lenInIndex, uint8* ipStr);

uint8 editConnectionNumber(generalConnInfoST* generalConnInfo, filesST *files);

void editConnectionType(uint8 connectionCount, uint8* connectionType, filesST* files);

void setupGeneralConfig(argumentListST* args);

uint8 setupConnectionType(argumentListST* args);

void setupHDLCConnection(uint8 connectionNumber, filesST* files);

void setupPPPConnection(uint8 connectionNumber, filesST* files);

void setupLANConnection(uint8 connectionNumber, filesST* files);

void setupGPRSConnection(uint8 connectionNumber, filesST* files);

void setupConnection(argumentListST* args);

void preDial(filesST* files, uint8 connNumber);

uint8 doConnect(filesST* files);

uint8 updatePPPConfig(filesST* files, uint8* user, uint8* password, int connectionNumber);

uint8 connSendBuff(uint8* buff, uint16 packedSize);

uint8 connReceiveBuff(uint8* buff, uint16* packedSize);

void showPINSimCard(uint8* pin);

uint8 setupPcPosConnection(argumentListST* args);

uint8 setPcPosConfig(PcPosConnInfoST pcPosConnInfo);       //HNO_TCP

uint8 initPcPosConn(filesST* files, uint8 connectionCount);

//+HNO_980311
uint8 initDNSConn(filesST* files, uint8 connectionCount);

#ifdef	__cplusplus
}
#endif

#endif	/* _connection_H */

