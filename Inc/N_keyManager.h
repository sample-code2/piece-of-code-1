#ifndef _N_KEYMANAGER_H
#define	_N_KEYMANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif


uint8 loadSecurityKey(filesST* files, uint8* fileName, uint8* keyForDecryption, uint8* decryptedKey, int32 keyLen);

int verifyMac(uint8* key, uint8* buffer, uint32 len, uint8 hex);

void createPin87(messageSpecST* messageSpec, uint8* outputHex, uint8* output);

uint8 parityAdjustment(char* keyValue);

void createPinAndEncryptedKey93(messageSpecST* messageSpec, uint8* output, uint8* encryptedKey);

void createMac(uint8* key, uint8* input, uint32 len, uint8* outputHex, uint8* output);
    
uint8 generateRandomKey(uint8* randomKLey);


#ifdef	__cplusplus
}
#endif

#endif	/* _N_KEYMANAGER_H */

