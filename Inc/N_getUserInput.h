#ifndef N_GETUSERINPUT_H_INCLUDED
#define N_GETUSERINPUT_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef CASTLES_V5S
#define MAX_LINE_LOG                            6
#else
#define MAX_LINE_LOG                            3  
#endif

#define FIRST_DIGIT                             1
#define SECOND_DIGIT                            2

/** PIN password */
#define PASS_PIN                                 0
/** merchant menu password */
#define PASS_TERM_KEY                            1
/** new password */
#define PASS_NEW_KEY                             2
/** repeat new password */
#define PASS_RE_NEW_KEY                          3
/** reset password */
#define PASS_RESET                               4
/** superviser password */
#define PASS_SUP_KEY                             5
/** merchant menu password, LENGHT = 4 */
#define PASS_TERM_KEY_LEN                        6

///** structure of bill specification */
//typedef struct
//{
//    int16   type;                   /*!< type of bill */
//    uint8   billID[13 + 1];         /*!<  bill ID */
//    uint8   paymentID[13 + 1];      /*!<  payment ID */
//    uint8   billAmount[12 + 1];     /*!<  amount of bill */
//}billSpecST;

/************************ IRANCELL PREFIXS ************************/
#define     MTN_1       901
#define     MTN_2       902
#define     MTN_3       903
#define     MTN_4       930
#define     MTN_5       933
#define     MTN_6       935
#define     MTN_7       936
#define     MTN_8       937
#define     MTN_9       938
#define     MTN_10      939
#define     MTN_11      905
#define     MTN_12      941

/************************ HAMRAHE AVAL PREFIXS ************************/    
#define     MCI_1       910
#define     MCI_2       919
#define     MCI_3       990    
#define		MCI_4		991
#define		MCI_5		992
#define		MCI_6		911
#define		MCI_7		913
#define		MCI_8		914
#define		MCI_9		915
#define		MCI_10		916
#define		MCI_11		917
#define		MCI_12		918
#define		MCI_13		994

	
/************************ RIGHTEL PREFIXS ************************/ //+MRF_971128
#define     RTL_1       920
#define     RTL_2       921   
#define     RTL_3       922 
    
int16 checkBillcode(uint8* billID);

int16 checkBillPaymentcode(uint8* billID, uint8* paymentID);

//uint8 confirmCancelPage(uint8* title, int timeout);

uint8 enterBalanceInfo(uint8* PIN);

uint8 enterBillPaymentInfo(billSpecST* billSpec, uint8* billCount);

uint8 readBarCodeBillPaymentInfo(billSpecST* billSpec, uint8* billCount);

uint8 enterMultiBillPaymentInfo(uint8* PIN, billSpecST* billSpec, uint8* billCount);

uint8 readMultiBarCodeBillPaymentInfo(uint8* PIN, billSpecST* billSpec, uint8* billCount);

uint8 enterBuyChargeInfo(filesST* files, uint8* PIN, uint8* chargeCount, uint8* chargeAmount, uint8 chargeType);

uint8 enterBuyMultiChargeInfo(uint8* PIN, buyChargeSpecST* buyCharge, int* chargeTypeCount, int* multiTypeCharge, uint8 offline);

uint8 enterBuyInfo(filesST* files, messageSpecST* messageSpec, uint8* PIN, uint8 readAmount, uint8* depositID);//#MRF_970806 

uint8 enterRollTransInfo(uint8* neededRollNum, uint8* PIN);

	uint8 enterBuyTopUpChargeInfo(topupST* topup, uint8* PIN, uint8 variable);//ABS

uint8 enterCardBillInfo(uint8* PIN);

uint8 enterLoanPayInfo(cardSpecST* cardSpec, loanPayST* loanPay);//HNO

uint8 getDateDistanceFromUser(uint32* startDate, uint32* endDate);

uint8 getDateTimeDistanceFromUser(dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser); //mgh 120204: added

uint8 getDateFromUser(uint8* title, uint8 nextprev, uint32* enteredDate);

void getInputPageDrawer(uint8* title, int isPass, int withComma, uint8* transactionValue, int transactionRequestNum, uint8* value, uint8 withF4);

void getKey(uint8* key, uint8* keyPressed, uint8* validKeys, uint32 timeout);

uint8 getStringValue(uint8* title, int maxLen, int minLen, uint8* result, int isPass, int zeroBegin, int withComma, uint8* transactionValue,
					 int transactionRequestNum);

uint8 getTimeFromUser(uint8* title, uint8 nextprev, uint32* enteredTime);

//HNO_IDENT9
uint8 selectItemPage(uint8* title, uint8 items[][20],int language, uint8 itemsNum, uint8* selectedItemIndex, uint32 timeout);

uint8 editNumericValue(uint32* inputNumber, uint8* message, uint32 min, 
                       uint32 max, int zeroBegin, int withComma);

uint8 editPort(uint32* inputNumber, uint8* message, uint32 min, 
                       uint32 max, int zeroBegin, int withComma);

uint8 getNumeric(uint8* title, int maxLen, int minLen, uint8* result, int zeroBegin, 
                  int withComma);

uint8 editNumericStringValue(uint8* inputString, uint8 maxLen, uint8 minLen, uint8* message, 
                             int zeroBegin, int withComma);

uint8 editIp(uint8* ip, uint8* message);

uint8 edit2StatusItem(uint8* itemValue, uint8 selectionItems[][20], uint8* message);

uint8 getDateDistanceFromUser(uint32* startDate, uint32* endDate);

uint8 enterETCInfo(cardSpecST* cardSpec, ETCST* ETC);

uint8 enterETCTrackingInfo(uint8* stan , uint8* PIN);

uint8 enterLoanPayTrackingInfo(uint8* stan , uint8* PIN);

uint8 enterRollRequestTrackingInfo(uint8* stan);//HNO_ADD_ROLL

uint8 editStatusItem(uint8* itemValue, uint8 selectionItems[][20], uint8* message, uint8 count);

//HNO_CHARITY
uint8 enterCharityInfo(uint8* amount, uint8* PIN);

uint8 enterTip(uint8* tip);

int cancelFunction(void);

uint8 getStringValue_WithF4(uint8* title, int maxLen, int minLen, uint8* result, int isPass, int zeroBegin, int withComma,
	uint8* transactionValue, int transactionRequestNum);


#ifdef	__cplusplus
}
#endif

#endif // GETUSERINPUT_H_INCLUDED

