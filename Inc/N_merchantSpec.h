#ifndef N_MERCHANTSPEC_H_INCLUDED
#define N_MERCHANTSPEC_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif

#define     IBAN_INFO_FILE                               "iban0100" //+HNO_980920
#define     IBAN_BANK_NAME_FILE                          "ibbkn0100" //+HNO_980927
#define     IBAN_BANK_COUNT_FILE                         "ibbkc0100" //+HNO_980927


	typedef struct
	{
		uint32   IbanID;//+HNO_980916
		uint16   IbanCount;
		uint8    IbanCode[30];//+HNO_980916
	}ibanST;


/** merchant specification */
typedef struct 
{
    uint8   terminalID[8 + 1];  
    uint8   merchantID[15 + 1]; 
    uint8   depositID[13 + 1];//MRF_NEW16
    uint8   discount[5];//MRF_NEW17
    uint8   marketName[100];    
    uint8   marketAddress[100]; 
    uint32  STAN;               
    uint32  recieveSTAN;
    uint32  batchNumber;  
    uint8   headerLine1[24];            
    uint8   headerLine2[24];            
    uint8   footerLine[31];             
    uint8   helpPhone[17];              
    uint8   merchantPhone[13 + 1]; 
    uint8   postalCodeMarket[10 + 1];
	ibanST  ibanInfo[30];

// --MRF_NEW   uint8   settleID[12 + 1];    
// --MRF_NEW   uint8   merchantInfo[24 + 1];
    
/*************************for maskan project*************************/
//    uint8   merchantIdAcronym[25];      
//    uint8   terminalNum[12 + 1];        
//    uint8   terminalType[2 + 1];        
//    uint8   applicationType[2 + 1];     
//    uint8   applicationName[10 + 1];    
//    uint8   MMC[4 + 1];                 
//    uint8   city[13 + 1];               
//    uint8   ANA[6 + 1];
//    uint8   terminalSerial[10 + 1];     
//    uint8   IP[15 + 1];                 
//    uint8   password[4 + 1];            

}merchantSpecST;

// for maskan project
/** version for maskan members project*/
typedef struct
{ 
    int16 macVer;           /*!< mac version */
    int16 keyVer;           /*!< key version */
    int16 emvVer;           /*!< emv version */
    int16 binVer;           /*!< bin version */
    int16 stopVer;          /*!< stop version */
    int16 posVer;           /*!< pos version */
    int16 softVer;          /*!< software version */
    int16 domVer;           /*!< dom version */
}versionsST; 

//MRF_NEW2
typedef struct 
{
    uint8   kbdSound;
    uint32  displayTimeout; //JUST FOR GPRS
    uint8   backLight;      //MRF_NEW3
    uint8   beepSound;  //MRF_NEW20
    uint8   fontName[20];
    uint8   fontType[9];
//    uint8   themeName[30];
}settingSpecST;

uint8 isInitialized(filesST* files);

uint8 readMerchantSpec(filesST* files, merchantSpecST* merchantSpec);

uint8 initMerchantSpec(filesST* files);

uint8 writeMerchantSpec(filesST* files, merchantSpecST merchantSpec);

uint8 changeMerchantPhone(argumentListST* args);

void setUserSetting(settingSpecST);

settingSpecST getUserSetting(void);


#ifdef	__cplusplus
}
#endif

#endif // MERCHANTSPEC_H_INCLUDED
