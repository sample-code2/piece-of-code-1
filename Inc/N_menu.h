#ifndef _N_MENU_H_INCLUDED
#define	_N_MENU_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif

/**Maximum connection number can be exist.*/
#define	  MAX_CONNECTION_NUMBER    6

typedef	struct nodeST	menuST;

/**Menu Items that in initialize class managed.*/
enum  menuItemNameEN 
{
    MENU_CARD_ROOT = 1,       /*!< when swipe card child of this root shown on screen. */
                MENU_CARD_ITEM_BUY,
                MENU_CARD_ITEM_BALANCE,
                MENU_CARD_ITEM_BILL_PAY,  
//                        MENU_CARD_BILL_PAY,
//                                MENU_CARD_BILL_PAY_BARCODE,
//                                MENU_CARD_BILL_PAY_MANUALLY,
                MENU_CARD_ITEM_BUY_CHARGE,
                        MENU_CARD_IRANCELL,
//                                MENU_CARD_IRANCELL_10000,          
                                MENU_CARD_IRANCELL_20000,          
                                MENU_CARD_IRANCELL_50000,          
                                MENU_CARD_IRANCELL_100000,  
                                MENU_CARD_IRANCELL_200000,
                                MENU_CARD_IRANCELL_500000,
                        MENU_CARD_MCI,
                                MENU_CARD_MCI_10000, 
                                MENU_CARD_MCI_20000,               
                                MENU_CARD_MCI_50000,               
                                MENU_CARD_MCI_100000, 
                                MENU_CARD_MCI_200000,
                                MENU_CARD_MCI_500000,
                        MENU_CARD_RIGHTEL,       
                                MENU_CARD_RIGHTEL_20000,            
                                MENU_CARD_RIGHTEL_50000,            
                                MENU_CARD_RIGHTEL_100000, 
                                MENU_CARD_RIGHTEL_200000,
                                MENU_CARD_RIGHTEL_500000,
                MENU_CARD_ITEM_BUY_MULTI_CHARGE,
                        MENU_CARD_MULTI_IRANCELL,
//                                MENU_CARD_MULTI_IRANCELL_10000,          
                                MENU_CARD_MULTI_IRANCELL_20000,          
                                MENU_CARD_MULTI_IRANCELL_50000,          
                                MENU_CARD_MULTI_IRANCELL_100000,  
                                MENU_CARD_MULTI_IRANCELL_200000,
                                MENU_CARD_MULTI_IRANCELL_500000,
                        MENU_CARD_MULTI_MCI,
                                MENU_CARD_MULTI_MCI_10000, 
                                MENU_CARD_MULTI_MCI_20000,               
                                MENU_CARD_MULTI_MCI_50000,               
                                MENU_CARD_MULTI_MCI_100000, 
                                MENU_CARD_MULTI_MCI_200000, 
                                MENU_CARD_MULTI_MCI_500000,
                        MENU_CARD_MULTI_RIGHTEL,       
                                MENU_CARD_MULTI_RIGHTEL_20000,            
                                MENU_CARD_MULTI_RIGHTEL_50000,            
                                MENU_CARD_MULTI_RIGHTEL_100000, 
                                MENU_CARD_MULTI_RIGHTEL_200000,
                                MENU_CARD_MULTI_RIGHTEL_500000,
                MENU_CARD_ITEM_BUY_TOPUP_CHARGE,
      //                  MENU_CARD_ITEM_BUY_TOPUP_IRANCELL,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_10000,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_20000,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_50000,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_100000,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_200000,
      //                           MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_OTHER_AMOUNT,
      //                  MENU_CARD_ITEM_BUY_TOPUP_MCI,
      //                          MENU_CARD_ITEM_BUY_TOPUP_MCI_10000,
      //                          MENU_CARD_ITEM_BUY_TOPUP_MCI_20000,
      //                          MENU_CARD_ITEM_BUY_TOPUP_MCI_50000,
      //                          MENU_CARD_ITEM_BUY_TOPUP_MCI_100000,
      //                          MENU_CARD_ITEM_BUY_TOPUP_MCI_200000,
						//		MENU_CARD_ITEM_BUY_TOPUP_MCI_OTHER_AMOUNT,
						//MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, //+MRF_971128
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_20000,
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_50000,
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_100000,
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_200000,
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_500000,
						//		MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_OTHER_AMOUNT,
                MENU_CARD_ITEM_LOAN_PAYMENT,
                MENU_CARD_ITEM_ETC,
				MENU_CARD_ITEM_CHARITY,
							MENU_CARD_ITEM_BEHZISTI_CHARITY,
                            MENU_CARD_ITEM_MAHAK_CHARITY,
                            MENU_CARD_ITEM_KAHRIZAK_CHARITY,
                            MENU_CARD_ITEM_KOMITEH_EMDAD_CHARITY,
//                            MENU_CARD_ITEM_HEMMATE_JAVANAN_CHARITY,
                            MENU_CARD_ITEM_BONYADE_MASKANE_ENGHELAB_CHARITY,
                            MENU_CARD_ITEM_ASHRAFOLANBIA_CHARITY,
    MENU_KEY_ROOT,   /*!< When presse menu key chile of this root shown on screen.*/
        MENU_ITEM_USER, 
                    MENU_ITEM_BUY,
                    MENU_ITEM_BALANCE,
                    MENU_ITEM_BILL_PAY,  
//                            MENU_BILL_PAY,
//                                    MENU_BILL_PAY_BARCODE,
//                                    MENU_BILL_PAY_MANUALLY,
                            MENU_ITEM_BUY_CHARGE,//MRF_NEW17
                                    MENU_IRANCELL,
                                            MENU_IRANCELL_10000,          
                                            MENU_IRANCELL_20000,          
                                            MENU_IRANCELL_50000,          
                                            MENU_IRANCELL_100000,  
                                            MENU_IRANCELL_200000,
                                            MENU_IRANCELL_500000,
                                    MENU_MCI,
                                            MENU_MCI_10000, 
                                            MENU_MCI_20000,               
                                            MENU_MCI_50000,               
                                            MENU_MCI_100000, 
                                            MENU_MCI_200000,
                                            MENU_MCI_500000,
                                    MENU_RIGHTEL,       
                                            MENU_RIGHTEL_20000,            
                                            MENU_RIGHTEL_50000,            
                                            MENU_RIGHTEL_100000, 
                                            MENU_RIGHTEL_200000,
                                            MENU_RIGHTEL_500000,
                            MENU_ITEM_BUY_MULTI_CHARGE,
                                    MENU_MULTI_IRANCELL,
                                            MENU_MULTI_IRANCELL_10000,          
                                            MENU_MULTI_IRANCELL_20000,          
                                            MENU_MULTI_IRANCELL_50000,          
                                            MENU_MULTI_IRANCELL_100000,  
                                            MENU_MULTI_IRANCELL_200000,
                                            MENU_MULTI_IRANCELL_500000,//HNO_IDENT
                                    MENU_MULTI_MCI,
                                            MENU_MULTI_MCI_10000, 
                                            MENU_MULTI_MCI_20000,               
                                            MENU_MULTI_MCI_50000,               
                                            MENU_MULTI_MCI_100000, 
                                            MENU_MULTI_MCI_200000,
                                            MENU_MULTI_MCI_500000,//HNO_IDENT
                                    MENU_MULTI_RIGHTEL,       
                                            MENU_MULTI_RIGHTEL_20000,            
                                            MENU_MULTI_RIGHTEL_50000,            
                                            MENU_MULTI_RIGHTEL_100000, 
                                            MENU_MULTI_RIGHTEL_200000,
                                            MENU_MULTI_RIGHTEL_500000,
                    MENU_ITEM_BUY_TOPUP_CHARGE,
//                            MENU_ITEM_BUY_TOPUP_IRANCELL,
//                                 MENU_ITEM_BUY_TOPUP_IRANCELL_10000,
//                                 MENU_ITEM_BUY_TOPUP_IRANCELL_20000,
////                                 MENU_ITEM_BUY_TOPUP_IRANCELL_50000,
////                                 MENU_ITEM_BUY_TOPUP_IRANCELL_100000,
////                                 MENU_ITEM_BUY_TOPUP_IRANCELL_200000,
////                                 MENU_ITEM_BUY_TOPUP_IRANCELL_OTHER_AMOUNT,
//                        MENU_ITEM_BUY_TOPUP_MCI,
//                                MENU_ITEM_BUY_TOPUP_MCI_10000,
//                                MENU_ITEM_BUY_TOPUP_MCI_20000,
////                                MENU_ITEM_BUY_TOPUP_MCI_50000,
////                                MENU_ITEM_BUY_TOPUP_MCI_100000,
////                                MENU_ITEM_BUY_TOPUP_MCI_200000,
////                                MENU_ITEM_BUY_TOPUP_MCI_OTHER_AMOUNT,
//                        MENU_ITEM_BUY_TOPUP_RIGHTEL,//+HNO_980329
//                            MENU_ITEM_BUY_TOPUP_RIGHTEL_10000,
                    MENU_ITEM_LOAN_SERVICE, //HNO
                                MENU_ITEM_LOAN_PAYMENT,
                                MENU_ITEM_TRACKING_LOAN,
                    MENU_PC_POS,
                    MENU_ITEM_ETC,
                            MENU_ITEM_BUY_ETC,
                            MENU_ITEM_TRACKING_ETC,
					MENU_ITEM_CHARITY,
						MENU_BEHZISTI_CHARITY,
                        MENU_MAHAK_CHARITY,
                        MENU_KAHRIZAK_CHARITY,
                        MENU_KOMITEH_EMDAD_CHARITY,
//                        MENU_HEMMATE_JAVANAN_CHARITY,
                        MENU_BONYADE_MASKANE_ENGHELAB_CHARITY,
                        MENU_ASHRAFOLANBIA_CHARITY,
            MENU_MERCHANT,
//                    MENU_SPECIAL_SERVICE,
//                        OFFLINE_CHARGE,
//                                MENU_SPECIAL_SERVICE_BUY_OFFLINE_CHARGE,
//                                        MENU_BUY_OFFLINE_IRANCELL,
//                                                        MENU_BUY_OFFLINE_IRANCELL_10000,          
//                                                        MENU_BUY_OFFLINE_IRANCELL_20000,          
//                                                        MENU_BUY_OFFLINE_IRANCELL_50000,          
//                                                        MENU_BUY_OFFLINE_IRANCELL_100000,  
//                                                        MENU_BUY_OFFLINE_IRANCELL_200000,
//                                        MENU_BUY_OFFLINE_MCI,
//                                                        MENU_BUY_OFFLINE_MCI_10000, 
//                                                        MENU_BUY_OFFLINE_MCI_20000,               
//                                                        MENU_BUY_OFFLINE_MCI_50000,               
//                                                        MENU_BUY_OFFLINE_MCI_100000, 
//                                                        MENU_BUY_OFFLINE_MCI_200000, 
//                                        MENU_BUY_OFFLINE_RIGHTEL,       
//                                                        MENU_BUY_OFFLINE_RIGHTEL_20000,            
//                                                        MENU_BUY_OFFLINE_RIGHTEL_50000,            
//                                                        MENU_BUY_OFFLINE_RIGHTEL_100000, 
//                                                        MENU_BUY_OFFLINE_RIGHTEL_200000,
//                                                        MENU_BUY_OFFLINE_RIGHTEL_500000,
//                                MENU_SPECIAL_SERVICE_PRINT_OFFLINE_CHARGE,
//                                        MENU_PRINT_OFFLINE_IRANCELL,
//                                                        MENU_PRINT_OFFLINE_IRANCELL_10000,          
//                                                        MENU_PRINT_OFFLINE_IRANCELL_20000,          
//                                                        MENU_PRINT_OFFLINE_IRANCELL_50000,          
//                                                        MENU_PRINT_OFFLINE_IRANCELL_100000,  
//                                                        MENU_PRINT_OFFLINE_IRANCELL_200000,
//                                        MENU_PRINT_OFFLINE_MCI,
//                                                        MENU_PRINT_OFFLINE_MCI_10000, 
//                                                        MENU_PRINT_OFFLINE_MCI_20000,               
//                                                        MENU_PRINT_OFFLINE_MCI_50000,               
//                                                        MENU_PRINT_OFFLINE_MCI_100000, 
//                                                        MENU_PRINT_OFFLINE_MCI_200000, 
//                                        MENU_PRINT_OFFLINE_RIGHTEL,       
//                                                        MENU_PRINT_OFFLINE_RIGHTEL_20000,            
//                                                        MENU_PRINT_OFFLINE_RIGHTEL_50000,            
//                                                        MENU_PRINT_OFFLINE_RIGHTEL_100000, 
//                                                        MENU_PRINT_OFFLINE_RIGHTEL_200000,
//                                                        MENU_PRINT_OFFLINE_RIGHTEL_500000,
//                                MENU_SPECIAL_SERVICE_BALANCE_OFFLINE_CHARGE,
//                    MENU_MERCHANT_INFO,
//                    MENU_MERCHANT_CONNECT_SUPERVISOR,
//                    MENU_MERCHANT_PAY_OFF,
//                    MENU_MERCHANT_ROLL_REQUEST,
//                    MENU_MERCHANT_ROLL_TRACKING,
//                    MENU_MERCHANT_DECLARATION_FAILURE,
//                    MENU_MERCHANT_CHANGE_DATE_TIME,
                    MENU_MERCHANT_CHANGE_PASS,
                    MENU_MERCHANT_TURN_OFF,
                    MENU_MERCHANT_SETTING,
					MENU_MERCHANT_SERVICE_SETTING,  //MRF_971207
                        MENU_MERCHANT_KBD_SOUND,
                            MENU_MERCHANT_KBD_SOUND_ON,
                            MENU_MERCHANT_KBD_SOUND_OFF,
                        MENU_MERCHANT_KBD_LIGHT,
                            MENU_MERCHANT_KBD_LIGHT_ON,
                            MENU_MERCHANT_KBD_LIGHT_OFF,
                    MENU_MERCHANT_REPORTS,
                            MENU_REPORT_CASH,
                                    MENU_REPORT_CASH_BUY,
                                    MENU_REPORT_CASH_CHARGE,
                                    MENU_REPORT_CASH_ETC,
                                    MENU_REPORT_CASH_LOANPAY,
                                    MENU_REPORT_CASH_TOPUP,
                                    MENU_REPORT_CASH_CHARITY,
                            MENU_REPRINT_TRANSACTION,
                                    MENU_REPRINT_TRANSACTION_BUY,
                                    MENU_REPRINT_TRANSACTION_BILL,
                                    MENU_REPRINT_TRANSACTION_BUY_CHARGE,
                                    MENU_REPRINT_TRANSACTION_LOAN_PAY,
                                    MENU_REPRINT_TRANSACTION_ETC,
                                    MENU_REPRINT_TRANSACTION_TOPUP,
                                    MENU_REPRINT_TRANSACTION_CHARITY,
                            MENU_REPORT_LIST_TRANSACTION,
                                    MENU_REPORT_LIST_TRANSACTION_SUCCESS,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_BUY,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_BILL,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARGE,
													MENU_REPORT_LIST_TRANSACTION_CHARGE_ABSTRACT,
													MENU_REPORT_LIST_TRANSACTION_CHARGE_DETAIL,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_LOAN_PAY,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_ETC,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARITY,
                                            MENU_REPORT_LIST_TRANSACTION_SUCCESS_TOPUP,
                                    MENU_REPORT_LIST_TRANSACTION_UNSUCCESS,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_BUY,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_CHARGE,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_BILL_PAY,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_LOAN_PAY,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_ETC,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_TOPUP,
                                            MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_CHARITY,
                            MENU_REPORT_HARDWARE,
                            MENU_REPORT_INITIALIZE_TRANS,
					MENU_MERCHANT_SHIFT,
							MENU_MERCHANT_SHIFTMANAGE,
									MENU_SHIFTMANAGE_CREATE_USER,
									MENU_SHIFTMANAGE_DELETE_USER,
									MENU_SHIFTMANAGE_REPORTS_USERS_LIST,
									MENU_SHIFTMANAGE_RESET_PASS,
							MENU_USERS_OPEN,
							MENU_USERS_CLOSE,
							MENU_USERS_REPORT,
            MENU_SUPERVISOR,
//            MENU_LAUNCH,
                MENU_SETTING,
                    MENU_BANKING_SETTING,
                    MENU_SETTING_CONNECTION,
                    MENUE_PCPOS_CONNECTION,
                    MENU_POS_CONNECTION,
                        MENU_CONNECTION1,
                            MENU_CONN1_TYPE,
                            MENU_CONN1_PARAM,
                        MENU_CONNECTION2,
                            MENU_CONN2_TYPE,
                            MENU_CONN2_PARAM,
                        MENU_CONNECTION3,
                            MENU_CONN3_TYPE,
                            MENU_CONN3_PARAM,
                        MENU_CONNECTION4,
                            MENU_CONN4_TYPE,
                            MENU_CONN4_PARAM,
                        MENU_CONNECTION5,
                            MENU_CONN5_TYPE,
                            MENU_CONN5_PARAM,
                        MENU_CONNECTION6, 
                            MENU_CONN6_TYPE,
                            MENU_CONN6_PARAM,
                    MENU_SETTING_SERVICES,
                MENU_CONFIGURATION,
                MENU_TMS,
                        MENU_TMS_GET_SETTING,
                        MENU_TMS_UPDATE,
                        MENU_TMS_SETTING,
                            MENU_TMS_TYPE,
                            MENU_TMS_PARAM,
                MENU_SUPERVISOR_CHANGE_PASS,
                MENU_RESET_MERCHANT_PASS,
                MENU_PRINT_KEYS,
            MENU_REPORT,
                    MENU_REPORT_TMS,
                    MENU_TRANSACTION_ERROR_LIST,
                    MENU_SYSTEM_ERROR_LIST,
   		    MENU_REPORT_SUPERVISOR_ACTIVITIES,
                    MENU_SETTING_REPORT,
                    MENU_LOCK_SIM_CARD, //+MRF_970829
        MENU_MANAGEMENT_ROOT,
                MENU_LOG,
                    MENU_LOG_TYPE,
                        MENU_LOG_TRACE,
                        MENU_LOG_DEBUG,
                        MENU_LOG_FATAL,
                        MENU_LOG_RELEASE,
                    MENU_LOG_PORT,
                MENU_PRINT_SUPERVISEOR_PASS,
                MENU_SET_DATE_TIME,
                MENU_SET_SERIAL_NUM,
                    MENU_SET_COMFIRM_SERIAL,
                    MENU_SET_RESET_SERIAL,
                MENU_DELETE_FILSE,
                MENU_PIN_UNLOCK,//+MRF_970830
                
         MENU_ITEM_COUNT
	
};

typedef struct menuItemST
{
    enum menuItemNameEN       parent;       /*!< Parent of menu item*/
    enum menuItemNameEN       child;        /*!< Child of menu item*/
    struct menuItemST*        next;         /*!< Next Node*/
}menuItemST;

void clearPreMenuName(void);

uint8 getPreMenuName(void);

uint8 getMaxLineInScreen(void);

void showMenu(uint8 rootMenu);

uint8 doFunction(void);

menuST* getParent(menuST* currentMenu, uint8 rootMenu);

menuST* getChild(menuST* currentMenu);

menuST* getNextChild(menuST* currentMenu);

void displayMenu(menuST* selectedMenu, uint8 rootMenu);

void traceDown(uint8 loop, uint8 rootMenu);

void traceUp(uint8 loop, uint8 rootMenu);

uint8 openMenu(uint8 rootMenu);

uint8 closeMenu(uint8 rootMenu);

uint8 traceShortKeys(uint8 key, uint8 enterMenu, uint8 rootMenu);

uint8 tracePageDown(uint8 rootMenu);

uint8 tracePageUp(uint8 rootMenu);

void setMenuVisible(menuST* parentmenu, menuST* currentMenu);

void setMenuInvisible(menuST* currentMenu, uint8 rootMenu);

void setMenuItem(enum menuItemNameEN menuIndex, enum menuItemNameEN parentMenuIndex, uint8 language, uint16 timeOut, uint8 align, uint8 color, uint8 visible,
                 uint8 checkPass, uint8 title[20], uint8 size, uint8 index, uint8 (*function)(argumentListST* arguments), 
                 int argumentCount, ...);

menuST* getMenuItem(enum menuItemNameEN menuIndex);

menuST* getPrevious(uint8 rootMenu, menuST* currentMenu);

void checkConnectionNumber(argumentListST* args, enum menuItemNameEN parentMenuIndex, uint8* connectionNumbers); // MGH & ELI 030811 : added

uint8 checkBankConnNumberVisibility(argumentListST* args); 

uint8 checkChargeConnNumberVisibility(argumentListST* args);

uint8 checkBillConnNumberVisibility(argumentListST* args); 

uint8 checkServersMenusVisibility(serverSpecST* serverSpec);

uint8 checkMaskanMenusVisibility(uint8 visible);

void checkMaskanMenus(int8* PAN);

uint8 searchMenuItem(uint8 root, uint8 menuItem);

uint8 freeMenuMemory(void);

uint8 quickSelection(uint8 key, uint8 rootMenu);

menuST* getMenuItemList(uint8 id);

void showMenuTocuh(int32 menuType, uint8 rootMenu);

uint8 doFunctionTest(argumentListST* args);

uint8 doFunctionTest2(void);

#ifdef	__cplusplus
}
#endif

#endif
