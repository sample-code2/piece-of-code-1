#ifndef _N_BINARYTREE_H_INCLUDED
#define	_N_BINARYTREE_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif
	
/**This Structure include information of tree*/
struct infoST
{
    uint8           language;       /*!< Language of menu item*/
    uint16          timeOut;        /*!< time out of menu item*/
    uint8           align;          /*!< align of menu item*/
    uint8           color;          /*!< color of menu item*/
    uint8           visible;        /*!< visiblity of menu item*/
    uint8           checkPass;      /*!< Menu item have password or not*/
    uint8           title[20];      /*!< title of menu item*/
    uint8           size;           /*!< size of menu item*/
    uint8           index;          /*!< index of node*/
    uint8           (*function) (argumentListST* arguments); /*!< function that do somthing*/
   	struct          argumentST* arguments;      /*!< Arguments of Function and dependent to function*/
};//ABS:CHANGE:960802

/** Structure of node information*/
struct	nodeST
{
    struct infoST   data;            /*!< Data of node*/
    struct nodeST*  leftChild;       /*!< Left child of node*/
    struct nodeST*  sibling;         /*!< sibling of node*/
    struct nodeST*  parent;          /*!< parent of node*/
};
typedef	struct nodeST nodeST;


void insertAtEnd(nodeST* parent, nodeST* child);

void insertNode(nodeST* parent, nodeST* child);

void addNode(nodeST* rootnode, nodeST* child);

void deleteNode(nodeST* parent, nodeST* menuNode);

void removeNode(nodeST* rootNode);

nodeST* successor(nodeST* rootNode, nodeST* menuNode);

nodeST* predecessor(nodeST* rootNode, nodeST* menuNode);

void preorderTraversal(nodeST* root);

void inorderTraversal(nodeST* root);

nodeST* searchNode(nodeST* root, nodeST* node);

void postorderTraversal(nodeST* root);

nodeST* getParentNode(nodeST* rootNode, nodeST* currentNode);

nodeST* getPreviousNode(nodeST* rootNode, nodeST* selectedMenu);

nodeST* getSuccessorNode(nodeST* roorNode, nodeST* selectedNode);

nodeST* getNextNode(nodeST* currentNode);

nodeST* getChildNode(nodeST* currentNode);

nodeST* firstSibling(nodeST* rootNode, nodeST* currentNode);

nodeST* lastSibling(nodeST* currentNode);

nodeST* getFirstNode(nodeST* rootNode, nodeST* currentNode);

nodeST* getLastNode(nodeST* currentNode);

nodeST* getLastChild(nodeST* rootNode);

nodeST* getLastParent(nodeST* rootNode);

void resetIndex(void);

void decreaseIndex(nodeST* currentNode);

void increaseIndex(nodeST* currentNode);

#ifdef	__cplusplus
}
#endif

#endif

