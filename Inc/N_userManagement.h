#ifndef N_USERMANAGEMENT_H_INCLUDED
#define N_USERMANAGEMENT_H_INCLUDED

#ifdef	__cplusplus
extern "C" {
#endif
	
#define     USER_MANAGEMENT_FILE		"userm0100"
	
#define		ACTIVATED_USER_FILE			"curUser"

//HNO_SHIFT
struct activateDateST
{
	dateTimeST loginDateTime;
	dateTimeST logOffDateTime;

    struct activateDateST*  next;
    struct activateDateST*  previous;
};
typedef struct activateDateST activateDateST;

//HNO_SHIFT
struct userInfoST
{
    uint8       	userName[15 + 1];
    uint8       	userFileIndex;
    uint8       	pass[4 + 1];
    int         	active;
    int         	permission;
    dateTimeST		createDateTime;
	activateDateST*	logDateTime;//NEW15
};
typedef struct userInfoST userInfoST;

 struct	userNodeST
{
    struct userInfoST   data;
    struct userNodeST*  next;
    struct userNodeST*  previous;
};
typedef	struct userNodeST userNodeST;

enum dataType //MRF_SHIFT
{
    USERNAME = 1,
    PERMISION,
    INDEX,
    ACTIVE,
    CREATE_DT,
    LOGIN_DT,
    LOGOFF_DT
};

uint8 strcatUserID(uint8* fileName);

uint8 addUser(void);

uint8 deleteUser(void);

uint8 activateUser(argumentListST* args);

uint8 deactivateUser(argumentListST* args);//MRF_SHIFT

void getLoginUser(uint8* user); //ABS:ADD

void setLoginUser(uint8* setloginUser);

uint8 userLogin(uint8* passWord);

uint8 printUsersList(void);

void logUsersReport(void);

void setActiveUser(uint8* username);//MRF_SHIFT

void getActiveUser(uint8* user);//ABS:CHANGE

userNodeST* getUserList(void); //ABS:ADD
//void getUserList(userNodeST* list); //ABS:ADD

void setPassUserList(uint8* user, uint8* newpass);//ABS:ADD

uint8 updateUsersListFile(void);//ABS:ADD

uint8 loadUserListFile(void);//ABS:ADD

void setShiftChange(uint8 check);

uint8 getShiftChange(void);

void addLoginDateTime(uint8* user);//ABS:ADD

void addLogOffDateTime(uint8* user);//ABS:ADD

void resetAllVariableShift(void);

uint8 checkShiftServiceAccessibility(void);

uint8 checkAdminAccessibility(void);

userNodeST* searchDataNode(void* data, uint8 Type, uint8* count);

userNodeST*	getUsersList(void);

#ifdef	__cplusplus
}
#endif

#endif // USERMANAGEMENT_H_INCLUDED
