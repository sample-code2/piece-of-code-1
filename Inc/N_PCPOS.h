//#ifdef PC_POS//MRF_NEW5
/* 
 * File:   ET_PCPOS.h
 * Author: maryam.ghods
 *
 * Created on December 12, 2011, 9:43 AM
 */

#ifndef _ET_PCPOS_H
#define	_ET_PCPOS_H

#ifdef	__cplusplus
extern "C" {
#endif

        //HNO_ADD
#define PRINTER_FAIL            5
#define PRF                     500
    
uint8 PCPOS(argumentListST* args);

uint8 sendAckPCPOS(void);

uint8 receiveENQWait(uint8* clearLines,int socket);

uint8 sendDataPCPOS(messageSpecST* messageSpec, uint8 result);

uint8 sendControlBytePCPOS(int16 transactionStatus);

uint8 receiveAckPCPOS(void);

uint8 receiveDataPCPOS(messageSpecST* messageSpec);

uint8 receiveControlBytePCPOS(void);

uint8 startOfOperationPCPOS(void);

void initDataParams(void);

void fillData(uint8 *field, uint8 *text);

void createDataPCPOS(uint8* request, uint16* len);

uint8 transPcPos(filesST* files, messageSpecST messageSpec);//HNO

uint8 setPcPosConnType(void);//HNO_TCP

void getPcPosConnType(uint8* connType); //HNO_TCP

#ifdef	__cplusplus
}
#endif

#endif	/* _ET_PCPOS_H */

//#endif

