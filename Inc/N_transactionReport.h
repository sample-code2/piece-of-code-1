#ifndef _N_transactionReport_H
#define	_N_transactionReport_H

#include "N_messaging.h"
#include "N_common.h"


#ifdef	__cplusplus
extern "C" {
#endif

typedef struct
{
    dateTimeST      dateTime;          /** transaction date and time */
    int             type;              /** charge type */
    uint8           amount[12 + 1]; 
    uint8           realAmount[12 + 1]; 
    uint8           PAN[19 + 1];           /** card number */
    uint8           retrievalReferenceNumber[12 + 1];
    uint8           serial[20 + 1];
//    uint32          id; //MRF_NEW10 JUST FOR TEST		//HNO_IDENT8 comment
    
}chargeLogST;

typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           PAN[19 + 1];           /** card number */
    uint8           amount[12 + 1];            /** transaction value */
    uint8           retrievalReferenceNumber[14 + 1];
    uint8           depositID[11 + 1]; //MRF_NEW17
    uint8           tipAmount[12 + 1]; //MRF_NEW17
}buyTransLogST;

typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           PAN[19 + 1];           /** card number */
    uint8           amount[12 + 1];            /** transaction value */
    int8            destinationCardPAN[19 + 1];
    uint8           destinationCardHolderName[20];
    uint8           destinationCardHolderFamily[20];
    uint8           retrievalReferenceNumber[12 + 1];
}loanPayTransLogST;
//HNO_ADD_ROLL
typedef struct
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           rollCount[3];            /** transaction value */
    uint8           retrievalReferenceNumber[12 + 1];
}rollTransLogST;

typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           PAN[19 + 1];           /** card number */
    uint8   		billID[13 + 1];
    uint8   		paymentID[13 + 1];
    uint8   		billAmount[12 + 1];            /** transaction value */
    uint8           retrievalReferenceNumber[12 + 1];
}billPayTransLogST;

//MRF
typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           PAN[19 + 1];           /** card number */
    uint8           amount[12 + 1];            /** transaction value */
    uint8           retrievalReferenceNumber[12 + 1];   
    uint8           personnelID[6 + 1]; //MRF_NEW8
    uint8           codeMeli[10 + 1];
    uint8           activity[100];
}supervisorTransLogST;

//MRF_ETC
typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           PAN[19 + 1];           /** card number */
    uint8           amount[12 + 1];            /** transaction value */
    uint8           balance[12 + 1];
    int8            serialETC[13 + 1];
    uint8           cardHolderName[20];
    uint8           cardHolderFamily[20];
    uint8           cardHolderID[10 + 1];
    uint8           retrievalReferenceNumber[12 + 1];
}ETCTransLogST;

//MRF_TOPUP
typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    int             type;              /** charge type */
    uint8           amount[12 + 1]; 
    uint8           realAmount[12 + 1]; 
    uint8           PAN[19 + 1];           /** card number */
    uint8           retrievalReferenceNumber[12 + 1];
//    uint8           serial[16 + 1];
    uint8           mobileNo[10 +1];
    int             operatorType;
}TopupTransLogST;

typedef struct 
{
    dateTimeST      dateTime; 
    //uint16          responseStatus;
    uint8           errorMsg[20];  
    uint8           macVersion[3 + 1];
    uint8           pinVersion[3 + 1];
    uint8           transType;
}InitializeLogST;

typedef struct 
{
    merchantSpecST      merchantSpec;
    uint8               approvalCode[6 + 1];
    uint8               retrievalReferenceNumber[12 + 1];
    uint8           	PAN[19 + 1];                		/** card number */
    uint8  				customerName[27];            		/** costomer name */
    uint8           	amount[12 + 1];             /** transaction value */
    uint8           	preAmount[12 + 1]; //MRF_NEW17
    dateTimeST      	dateTime;                   /** transaction date and time */
    uint8           	transType;                  /** transaction type */
    billSpecST      	billSpec;
    buyChargeSpecST     buyChargeSpec;
    loanPayST           loanPay; 
    ETCST               ETC; //MRF_ETC
    topupST             topup; //MRF_TOPUP
    rollRequestST       rollRequest; //HNO_ADD_ROLL
    uint8               PIN[19 + 1];
    charityST           charity; //MRF_NEW16
    uint8           	depositID[11 + 1]; //MRF_NEW17
    uint8           	tipAmount[12 + 1]; //MRF_NEW17
	uint8               taxNumber[22 + 1];//+ABS_990313
}lastCustomerReceiptST;

/** transaction error log */
typedef struct 
{
    dateTimeST      dateTime;          /** transaction date and time */
    uint8           transType;         /** transaction type */
    uint16          errorCode;         /** error code */
}transErrorST;

typedef struct 
{
    dateTimeST      dateTime;                   /** transaction date and time */
    dateTimeST      SWdateTime;                //MRF_NEW2
    uint8           transType;                  /** transaction type */
    uint32          STAN;                       /** transaction stan */
    uint16          responseStatus;
    uint8           amount[12 + 1];             /** transaction value */
    uint8           ChargeRealAmount[12 + 1];   /** charge value */
    uint8           chargeType;
    uint8           PAN[19 + 1];                /** card number */
    uint8           retrievalReferenceNumber[12 + 1];
    billSpecST      billSpec;
    //loanPayST       loanPay;//HNO_IDENT the structures aren't useful
    //ETCST           ETC;    //MRF_ETC
    topupST         topup; //MRF_TOPUP
    charityST		charity;//HNO_DEBUG
    uint8           depositID[11 + 1]; //MRF_NEW17
}reversalTransST;


void resetPrePrintFlags(void);

void addUnsuccessTransLog(messageSpecST* messageSpec);

void makeTransPrintDataLog(filesST* files, messageSpecST* messageSpec);

void structCpy(lastCustomerReceiptST* lastCustomerReceipt, messageSpecST* messageSpec);

void deleteDependantFiles(uint8 index);

uint8 prePrint(int printRecLines, filesST* files);

uint8 printUnsuccessTrans(uint8 transType);

uint8 printReceiptHeader(dateTimeST* dateTime, int printRecLines, filesST* files); 

uint8 printTransactionListCommonPart(uint8* amount, uint8* pan, uint8* referenceNum, uint32 stan, dateTimeST* dateTime, uint8* depositID);

uint8 printTransactionFooter(uint8 reprint, uint8 printTel, uint8 emptyLine, filesST* files);

//uint8 printTransactionJointPart(messageSpecST* messageSpec, int reprint/*-- , uint8 chargeRequestCount--*/) ;

uint8 printTransactionListHeader(filesST* files, uint8* title, uint8* activeUserTitle, uint8* activeUser, 
		 dateTimeST* startDateTime, dateTimeST* endDateTime);

uint8 readLastCustomerReceiptFile(uint8* fileName, lastCustomerReceiptST* lastCustomerReceipt);

uint8 readTransactionListFile(uint8* fileName, void* transaction, uint16 structLen, uint16 maxRecord, 
								dateTimeST* startDateTime, dateTimeST* endDateTime, int* transactionNum, 
								uint32* requestedNum, uint8* activeUserTitle, uint8* activeUser);

void fillTransListHeaderLines(uint8 lines[][32], uint16* index, uint8* title, uint8* activeUserTitle, uint8* activeUser,
		 dateTimeST* startDateTime, dateTimeST* endDateTime);

void addTransactionErrorReport(uint8 transactionType, int errorCode, uint8 knownError);

uint8 transactionsErrorList(argumentListST* args);

uint8 printReversal(filesST* files);

uint8 readReversalInfo(int8* fileName, messageSpecST* messageSpec);

void successInitializeReport(filesST* files, messageSpecST* messageSpec);

void printBalanceReceipt(filesST* files, messageSpecST* messageSpec);

/*********************************BUY REPORT FUNCTIONS*********************************************************/

uint8 printTransactionBuy(filesST* files,messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 transactionBuyList(argumentListST* args);

uint8 reprintBuyTransaction(argumentListST* args);

uint8 unsuccessBuyTransaction(argumentListST* args);

uint8 merchantBoxReport(argumentListST* args);

/*********************************BILL PAY REPORT FUNCTIONS*********************************************************/

uint8 printTransactionBillPay(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint); 

uint8 transactionBillPayList(argumentListST* args);

uint8 reprintBillTransaction(argumentListST* args);

uint8 unsuccessBillPayTransaction(argumentListST* args);

uint8 displayunsuccessBillPayTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

/*********************************CHARGE REPORT FUNCTIONS*********************************************************/

uint8 printTransactionBuyCharge(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 transactionChargeList(argumentListST* args);

uint8 reprintChargeTransaction(argumentListST* args);

uint8 unsuccessChargeTransaction(argumentListST* args);

uint8 merchantChargeReport(argumentListST* args);

uint8 displayTransactionChargeList(chargeLogST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);


uint8 displayUnsuccessChargeTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

void getChargeTypeStr(uint8 chargeType, uint8* chargeTypeStr);

/*********************************SUPERVISOR REPORT FUNCTIONS*********************************************************/

uint8 printTransactionSupervisor(messageSpecST* messageSpec, filesST* files);

uint8 displayTransactionSupervisorList(supervisorTransLogST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

uint8 transactionSupervisorList(argumentListST* args);

/*********************************LOAN REPORT FUNCTIONS*********************************************************/

uint8 printLoanPayTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 transactionLoanPayList(argumentListST* args);

uint8 reprintLoanPayTransaction(argumentListST* args);

uint8 merchantLOANReport(argumentListST* args);

//void addLoanReport(messageSpecST* messageSpec); ING IDENT

//uint8 reprintLoanTransaction(argumentListST* args);	ING IDENT
uint8 displayTransactionLoanPayList(loanPayTransLogST* loanPay, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

uint8 printLoanTrakingTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);
/*********************************ETC REPORT FUNCTIONS*********************************************************/

uint8 printETCTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 printETCTrakingTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 transactionETCList(argumentListST* args);

uint8 reprintETCTransaction(argumentListST* args);

uint8 merchantETCReport(argumentListST* args);

uint8 displayTransactionETCList(ETCTransLogST* ETC, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

/*********************************TOPUP REPORT FUNCTIONS*********************************************************/

uint8 printTransactionBuyChargeTopup(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

uint8 transactionTopupList(argumentListST* args);

uint8 reprintTopupTransaction(argumentListST* args);

uint8 unsuccessTopupTransaction(argumentListST* args);

uint8 merchantTopupReport(argumentListST* args);

uint8 displayTransactionTopupList(TopupTransLogST* topup, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

uint8 displayunsuccessTopupTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

/*********************************ROLL REQUEST FUNCTIONS*********************************************************/
uint8 printRollRequestTrans(filesST* files, messageSpecST* messageSpec);//HNO_ADD_ROLL
void addRollRequestReport(messageSpecST* messageSpec);//HNO_ADD_ROLL

uint8 reprintRollRequestTransaction(argumentListST* args);//HNO_ADD_ROLL

uint8 completeChargeReport(argumentListST* args);//HNO_CHARGE

//HNO_SHIFT
uint8 readAllUsersTransactionListFile(uint8* fileName, void* transaction, uint16 structLen, uint16 maxRecord,
							  	dateTimeST* startDateTime, dateTimeST* endDateTime, int* transactionNum,
								uint32* requestedNum,uint8* activeUserTitle, uint8* activeUser);
    
uint8 deletAllDependentFilesShift(void);

uint8 deletUserFiles(uint8* username);

//HNO_CHARITY
uint8 printTransactionCharity(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint);

//HNO_CHARITY
void addCharityReport(messageSpecST* messageSpec);

//HNO_CHARITY
uint8 displayTransactionCharityList(charityST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);

//HNO_CHARITY
uint8 transactionCharityList(argumentListST* args);

//HNO_CHARITY
uint8 displayunsuccessCharityTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues);
//HNO_CHARITY
uint8 unsuccessCharityTransaction(argumentListST* args);

//HNO_CHARITY
uint8 reprintCharityTransaction(argumentListST* args);

//HNO_CHARITY
uint8 merchantCharityReport(argumentListST* args);

//HNO_INIT
void addInitializeReport(messageSpecST* messageSpec, uint8 response);
//HNO_INIT
uint8 transactionInitializeList(argumentListST* args);


#ifdef	__cplusplus
}
#endif

#endif	/* _transactionReport_H */

