#include <string.h> 
//MRF #include <stdlib.h> 
//MRF #include <stdio.h> 
#include "N_common.h"
#include "N_log.h" 
#include "N_dateTime.h"
#include "N_scheduling.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_utility.h"
#include "N_merchantSpec.h"
#include "N_messaging.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_displayWrapper.h"
#include "N_display.h"
#include "N_getUserInput.h"
#include "N_barCode.h"
#include "N_transactions.h" 
#include "N_transactionReport.h" 
#include "N_printer.h" 
#include "N_fileManageWrapper.h" 
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_initialize.h"
#include "N_TMS.h" 
#include "N_printerWrapper.h"
#include "N_dateTimeWrapper.h"
#include "N_terminalReport.h"
#include "N_cardSpecWrapper.h"
#include "N_connectionWrapper.h"
#include "N_fileManage.h"

int32 getEvents(serverSpecST* serverSpec, uint8* data);
uint8 freeMenuItemStMemory(applicationSpecST* applicationSpec);

/**
* This is main function that call other functions for intitialize and
* peresent app and get ready to use.
* @param   argc [input]: not used
* @param   argv [input]: not used
* @see     initPOSParams().
* @see     loadInitializationData().
* @see     displayMessageBox().
* @see     terminalInitiation().
* @see     getCardHandle().
* @see     enableMSR().
* @see     getEvents().
* @see     disableMSR().
* @see     doSchedules().
* @see     isInitialized().
* @see     readTrackData().
* @see     writeCardInfo().
* @see     resetBuyChargeInfo().
* @see     showMenu().
* @see     resetPrePrintFlags().
* @see     setLogType().
* @see     ActiveSupervisorMenu().
* @return  0.
*/
int main(int argc, char** argv)
{
	applicationSpecST*  applicationSpec = NULL;
	serverSpecST*       server = NULL;
	int32               eventType = 0;
	uint8               data[sizeof(cardTracksST)] = { 0 };
	uint8               magnetCard = FALSE;
	nextScheduleST*     currentSchedule = NULL;
	uint8               ScheduleTemp[sizeof(nextScheduleST)] = { 0 };
	uint8               cardTemp[sizeof(cardTracksST)] = { 0 };
	cardTracksST*       cardSpecP = 0;
	uint8               key = KBD_CANCEL;
	uint8               kbd = KBD_CANCEL;
	LANConnInfoST       LANConnInfo;
	cardSpecST          cardSpec;

	memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
	memset(&cardSpec, 0, sizeof(cardSpecST));
	/*++++++++++++++++++++++++++++++++++++initializing config+++++++++++++++++++++++++++++++++*/
	initPOSParams();
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***********Start App***************");

	if (!loadInitializationData(&applicationSpec))
	{
            displayMessageBox("��ѐ���� ���� ������", MSG_ERROR);
            return TRUE;
	}
        //+HNO_980613
        getServer(applicationSpec->serverSpec);
        
	terminalInitiation(applicationSpec->serverSpec);
	/*+++++++++++++++++++++++++++++++++++++++++Main LOOP+++++++++++++++++++++++++++++++++++++++++*/
	while (TRUE)
	{
		memset(data, 0, sizeof(cardTracksST));
		memset(&cardSpec, 0, sizeof(cardSpecST));
		server = applicationSpec->serverSpec;
		fileRemove(SUPERVISOR_FILE);

		if (getCardHandle() <= 0)
		{
			enableMSR();
		}

		eventType = getEvents(applicationSpec->serverSpec, data);
		disableMSR();
		switch (eventType)
		{
		case SCHEDULED_ACT_EVENTS:
			setMagnetFlag(TRUE);
			memcpy(ScheduleTemp, data, sizeof(nextScheduleST));
			currentSchedule = (nextScheduleST*)ScheduleTemp;
			doSchedules(applicationSpec->serverSpec, currentSchedule);
#ifdef INGENICO5100
			//Ingenico: back to GMA
			if (checkForBackToGMA(currentSchedule->type))
			{
				freeMenuItemStMemory(applicationSpec);
				return 0;
			}
#endif
			break;
		case MIFARE_CARD_EVENTS:
			setMagnetFlag(TRUE);
			break;
		case MAGNETIC_CARD_READER_EVENTS:
#ifndef INGENICO//	HNO_CHANGE
			if (getMagnetFlag() == TRUE)
				break;
#endif
			setMagnetFlag(TRUE);

			if (!isInitialized(&(server->files)))
			{
				displayMessageBox("������ ��� ������ ���� ���.", MSG_ERROR);
				break;
			}
			memset(&cardSpec, 0, sizeof(cardSpecST));
			memcpy(cardTemp, data, sizeof(cardTracksST));
			cardSpecP = (cardTracksST*)cardTemp;
			magnetCard = TRUE;

			if (!readTrackData(&(server->files), cardSpecP, &cardSpec, magnetCard))
				break;

			writeCardInfo(&cardSpec);
			resetBuyChargeInfo();
			showMenu(MENU_CARD_ROOT);
			resetPrePrintFlags();
			break;
		case LAN_CABEL_EVENTS://+MRF_970820
			openLANPort();
			break;
		case KBD_EVENTS:

			setMagnetFlag(TRUE);
			key = data[0];

			if (key == KBD_F1)
			{
#ifdef VX520
				closeModem(TRUE);//ABS:ADD
#endif
				showMenu(MENU_KEY_ROOT);
			}
			else if (key == KBD_F3) //+ABS:990313
			{
				clearDisplay();
				if (checkMerchantMenuPassword())
				{
					kbd = displayMessageBox("��� ���� ���� ���� �ǁ ��Ͽ", MSG_CONFIRMATION);
					if (kbd == KBD_ENTER || kbd == KBD_CONFIRM)
					{
						argumentListST* args = (argumentListST*)malloc(sizeof(argumentListST));
						args->argumentName = &(server->files);
						args->next = NULL;
						reprintBuyTransaction(args);
					}
				}
			}
			else if (key == KBD_MANAGEMENT)
			{
				clearDisplay();
				if (checkLogMenuPassword())
                                    showMenu(MENU_MANAGEMENT_ROOT);
			}
			else if (key == KBD_F4)
			{
				settingReport(&(server->files));
			}
#if defined (ICT250) || defined (IWL220) //HNO
			if (key == KBD_DOWN)//HNO_IDENT8
			{
				if (goToManager())
				{
					disablePrinter();
					disableMSR();
					disableKeyboard();
					return 0;
				}
			}
#endif
			else if (key == KBD_CANCEL)
			{
#ifdef PROGRAMMER
				return 0;
#endif
			}

//                        else if (key == KBD_1)
//                        {
//                            uint8               key                 = 0;
//                            uint8               keyPressed              = 0;
//                            uint8               validKeys[3]        = {KBD_CANCEL};
//                            while(TRUE)
//                            {
//                                CTOS_LCDGShowBMPPic(0,0,"MainPage2.bmp");
//                                getKey(&key, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
//                                if (keyPressed)
//                                    break;
//                            }
//                        }
//                        else if (key == KBD_2)
//                        {                               
//                            uint8               key                 = 0;
//                            uint8               keyPressed              = 0;
//                            uint8               validKeys[3]        = {KBD_CANCEL};
//                            
//                            while(TRUE)
//                            {
//                                CTOS_LCDGShowBMPPic(0,0,"MainPage2.bmp");                                
//                                getKey(&key, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
//                                if (keyPressed)
//                                    break;
//                            }
//                            
//                        }
//                        else if (key == KBD_3)
//                        {
//                            uint8               key                 = 0;
//                            uint8               keyPressed              = 0;
//                            uint8               validKeys[3]        = {KBD_CANCEL};
//                            while(TRUE)
//                            {
//                                CTOS_LCDGShowBMPPic(0,0,"MainPage3.bmp");
//                                getKey(&key, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
//                                if (keyPressed)
//                                    break;
//                            }
//                        }
                    break;

            case SMART_CARD_READER_EVENTS:
                    setMagnetFlag(TRUE);
                    break;
            case ACTIVE_MERCHANT_EVENTS:
                    setMagnetFlag(TRUE);
                    ActiveSupervisorMenu();
                    break;
            default:
                    break;
            }
            eventType = 0;
    }

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	return 0;
}

/**
* This function waiting for swipe card. when swipe card get data cards.
* if exist reversal file print reciept that.
* @param   serverSpec [input]: number of servers
* @param   data [output]: is output include taraks data
* @see     nextSchedule().
* @see     getTicks().
* @see     displayMainPage().
* @see     clearKeyBuffer().
* @see     fileExist().
* @see     PrinterAccess().
* @see     delay().
* @see     turnOnLCDBackLight().
* @see     printReversal().
* @see     readSchedules().
* @see     checkEvents().
* @see     getSchedule().
* @see     turnOffLCDBackLight().
* @return  eventType.
*/
int32 getEvents(serverSpecST* serverSpec, uint8* data)
{
    uint32          baseTicks = 0;            /* start of back light timeout time */
    uint32          nowTicks = 0;            /* for check timeout */
    int32           eventType = 0;            /* event type */
    uint32          baseTicks2 = 0;
    serverSpecST*   servers = NULL;
    int             batteryStatus = 0;
    nextScheduleST  schedule;
    scheduleST      tempSchedule;
    terminalSpecST  terminalCapability = getTerminalCapability();
    serverSpecST*   TMSserver = serverSpec;
    uint32          powerOffTick = 0;//HNO_ADD_IWL
    uint32          baseTicks3 = 0;
    uint8           retValue = 0;
    serviceSpecST   POSService      = getPOSService();//+HNO_980902

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "getEvents", "**********START*************");

	memset(&schedule, 0, sizeof(nextScheduleST));
	memset(&tempSchedule, 0, sizeof(scheduleST));

	nextSchedule(serverSpec, &schedule);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "schedules read");

    baseTicks = getTicks(); 
    baseTicks2 = getTicks(); 
    baseTicks3 = getTicks();
    powerOffTick = getTicks();//+HNO_971011
    displayMainPage(TRUE);
    clearKeyBuffer();
    servers = serverSpec;
	
    while (servers != NULL)
    {
        if (fileExist(servers->files.reversalReceiptFile) == SUCCESS)
        {
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "getEvents", " reversal file exist for %d", servers->files.id);
        	if (PrinterAccess(FALSE))
        	{
	            delay(1000, ALL_POS);
	            turnOnLCDBackLight();
                    turnOnKBDBackLight();
                    printHeaderLogoAndMerchantInfo();//+HNO_980620
                    printReversal(&(servers->files));
        	}
        	displayMainPage(TRUE);
        }
       
        servers = servers->next;
    } // each servers reversal
	
    while (TRUE) 
    {
        //--HNO_980722
//        if (terminalCapability.OTPCapability == TRUE)//+MRF_971124
//        {
//            retValue = checkDate();
//            if (retValue == FALSE)
//                displayMessageBox("��� ������� ����� ���!", MSG_ERROR);
//        }
        eventType = 0;
        eventType = checkEvents(&schedule, data);

		if (eventType != MAGNETIC_CARD_READER_EVENTS)
			setMagnetFlag(FALSE);

		if (eventType != NO_EVENTS)
			break;

#if defined(ICT250) || defined(IWL220) //..HNO_971015
        if (!terminalCapability.GPRSCapability)
        	if (tickDuration(baseTicks) > 20000)
#else
		nowTicks = getTicks();
		if (((nowTicks - baseTicks) >= 10000) && ((nowTicks - baseTicks) < 60000)) //MRF_NEW3 60000
#endif
        {
            if ((terminalCapability.TMSCapability)&& (POSService.TMSService))//..HNO_980902
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "TMSCapability for check schedules ");
                //+HNO_980827
                if(fileExist(TMS_CONFIG_AUTO_FILE) == SUCCESS)
                {
                    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "TMS_CONFIG_AUTO_FILE exist");
                    TMSConfigPeriodicTask(&(TMSserver->files));
                    fileRemove(TMS_CONFIG_AUTO_FILE);//+HNO_980828
                }
     
                checkTMSSchedule(&(TMSserver->files));
            }

			displayMainPage(TRUE);
			baseTicks = getTicks();

			if (getDisplayBackLight() == TRUE)
			{
				turnOffLCDBackLight();
				turnOffKBDBackLight();
			}

			if (getPreDialCheck() == TRUE)
			{
#ifdef VX520			//ABS:CHANGE:961207
				closeModem(TRUE);
#else
				//				disconnectModem(); -MRF_970924 : REMOVE 10S UP CONNECTION
#endif
			}
        }
		
        if ((nowTicks - baseTicks3) >= 900000)
        {
            baseTicks3 = getTicks();

            if ((terminalCapability.TMSCapability) && (POSService.TMSService))//..HNO_980902
                checkTMSContinue(&(TMSserver->files));
        }
            
        if (terminalCapability.GPRSCapability)
        {
            if (fileExist(serverSpec->files.settelmentInfoFile) != SUCCESS) //+MRF_971015
                if (fileExist(serverSpec->files.reversalTransFile) != SUCCESS)
                {
					batteryStatus = getBatteryStatus();
#ifdef IWL220//HNO_ADD
					manualGarbageCollection();
					nowTicks = getTicks();
					if ((nowTicks - powerOffTick) >= (getDisplayTimeout() * 100000))
					{
						showLog(JUST_INGENICO, __FILE__, __LINE__, DEBUG, "", "shutdowwwn");
						powerOffTick = getTicks();
		//            	powerOffTick = 0;
						disablePrinter();
		//            	disableMSR();
						shutdown();
					}

#else
			if (fileExist(serverSpec->files.settelmentInfoFile) != SUCCESS) //+MRF_971015
			if (fileExist(serverSpec->files.reversalTransFile) != SUCCESS)//+MRF_971015
			{
				if ((nowTicks - baseTicks2) >= getDisplayTimeout())
				{
					baseTicks2 = getTicks();
					setGPRSCheck(FALSE);
					GPRSClose();
					setOpenPort(FALSE);

					if (batteryStatus != 3)
						powerMode(SLEEP_MODE);

					displayMainPage(TRUE);
				}
			}
#endif
            }  
        }
    }
    
    return eventType;
}

/**
* If exist menu item or server spec free up memory for theme.
* @param    applicationSpec [input]: Application information
* @return   TRUE.
*/
uint8 freeMenuItemStMemory(applicationSpecST*  applicationSpec)
{
	menuItemST*     tempMenuItem = NULL;
	serverSpecST*   tempserverSpec = NULL;

	while (applicationSpec->menuItems)
	{
		tempMenuItem = applicationSpec->menuItems->next;
		free(applicationSpec->menuItems);
		applicationSpec->menuItems = tempMenuItem;
	}

	while (applicationSpec->serverSpec)
	{
		tempserverSpec = applicationSpec->serverSpec->next;
		free(applicationSpec->serverSpec);
		applicationSpec->serverSpec = tempserverSpec;
	}

	free(applicationSpec);
	return TRUE;

}

void BackControlToGma(uint16 eventId)
{
	showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "back to GMA");

	freeMenuMemory();
	disableKeyboard();
	disablePrinter();
	disableMSR();
	disableSmartCard();
	addGMAEvent(eventId);
}

