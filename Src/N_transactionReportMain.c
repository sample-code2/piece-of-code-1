#include <string.h> 
//MRF #include <stdio.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_dateTime.h"
#include "N_printerWrapper.h" 
#include "N_printer.h"
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_connectionWrapper.h"
//#include "N_connection.h"
#include "N_messaging.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
#include "N_printer.h" 
#include "N_getUserInput.h" 
#include "N_transactionReport.h"
#include "N_transactions.h"
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_displayWrapper.h" 
#include "N_display.h" 
#include "N_initialize.h"
#include "N_log.h"
#include "N_userManagement.h"//HNO_SHIFT

#define LAST_CUSTOMER_BUY_RECIEPT_FILE        		"rpbuy0100"
#define LAST_CUSTOMER_BILL_RECIEPT_FILE        		"rpbill0100"
#define LAST_CUSTOMER_CHARGE_RECIEPT_FILE           "rpchrg0100" 
#define LAST_CUSTOMER_LOAN_PAY_RECIEPT_FILE        	"rploan0100"
#define LAST_CUSTOMER_ETC_RECIEPT_FILE              "rpetc0100"
#define LAST_CUSTOMER_TOPUP_RECIEPT_FILE            "rptpu0100"     //MRF_TOPUP
#define LAST_CUSTOMER_ROLL_REQUEST_RECIEPT_FILE     "rprr0100"      //HNO_ADD_ROLL
#define LAST_CUSTOMER_CHARITY_RECIEPT_FILE     		"rpcht0100"

#define BUY_LOG_FILE             					"bl"            //Buy Log
#define BILL_PAY_LOG_FILE                   		"bpl"			//Bill Pay Log
#define CHARGE_LOG_FILE                         	"chl"			//Charge Log
#define LOAN_PAY_LOG_FILE                   		"lpl"
#define ETC_LOG_FILE                                "etl"           
#define TOPUP_LOG_FILE                              "tpl"           //MRF_TOPUP
#define ROLL_LOG_FILE                               "rpl"           //HNO_ADD_ROLL
#define SUPERVISOR_LOG_FILE                         "spl"
#define CHARITY_LOG_FILE							"cht1"	
#define INITIALIZE_LOG_FILE                   		"inl"   //HNO_INIT

#define REVERSAL_BUY_LOG_FILE                   	"revbuylog"
#define REVERSAL_BILLPAY_LOG_FILE               	"revbillog"
#define REVERSAL_CHARGE_LOG_FILE               		"revchrglog"
#define REVERSAL_LOAN_PAY_FILE               		"revloanlog"
#define REVERSAL_ETC_FILE                           "revetclog"
#define REVERSAL_TOPUP_FILE                         "revtpulog"     //MRF_TOPUP
#define TRANS_ERROR_LOG_FILE        				"dtrerr0100"
#define REVERSAL_CHARITY_LOG_FILE               	"revchartog"

/*********************************LOG NUMBER TRANS*********************************/
#if defined( CASTLES_V5S) || defined (VX520) //ABS:ADD
#define SEPERATED_STR_SIZE							250 //ABS:ADD
#define MAX_BUY_TRANS_LOG_RECORDS                   1000
#define MAX_BILL_PAY_TRANS_LOG_RECORDS              1000
#define MAX_CHARGE_TRANS_LOG_RECORDS                1000
#define MAX_LOAN_PAY_TRANS_LOG_RECORDS              1000
#define MAX_ETC_TRANS_LOG_RECORDS                   1000 
#define MAX_TOPUP_TRANS_LOG_RECORDS                 1000 //MRF_TOPUP
#define MAX_SUPERVISOR_TRANS_LOG_RECORDS            200 
#define MAX_TRANS_ERROR_LOG_RECORDS                 200 
#define MAX_ROLL_REQUEST_TRANS_LOG_RECORDS          10 
#define MAX_CHARITY_TRANS_LOG_RECORD 				100
#define MAX_INITIALIZE_TRANS_LOG_RECORDS            100//HNO_INIT

#define MAX_LINE_SUCCESS_TRANS_DISPLAY              3500
#define MAX_LINE_UNSUCCESS_TRANS_DISPLAY            2000

#define MAX_UNSUCCESS_BUY_TRANS                     500
#define MAX_UNSUCCESS_CHARGE_TRANS                  500
#define MAX_UNSUCCESS_BILLPAY_TRANS                 500
#define MAX_UNSUCCESS_LOANPAY_TRANS                 500
#define MAX_UNSUCCESS_ETC_TRANS                     500 
#define MAX_UNSUCCESS_TOPUP_TRANS                   500 //MRF_TOPUP 
#define MAX_UNSUCCESS_CHARITY_TRANS                 50
#endif

#ifdef INGENICO5100

#define MAX_BUY_TRANS_LOG_RECORDS                   600 // max buy log records
#define MAX_BILL_PAY_TRANS_LOG_RECORDS              100 // max BILLpAY log records
#define MAX_CHARGE_TRANS_LOG_RECORDS                200 // max charge log records
#define MAX_LOAN_PAY_TRANS_LOG_RECORDS              100 // max BILLpAY log records
#define MAX_TRANS_ERROR_LOG_RECORDS                 50/* //mgh 250*/ // max error log records
#define MAX_SUPERVISOR_TRANS_LOG_RECORDS            100
#define MAX_ETC_TRANS_LOG_RECORDS                   100
#define MAX_TOPUP_TRANS_LOG_RECORDS                 100 //MRF_TOPUP

#define MAX_LINE_SUCCESS_TRANS_DISPLAY              350 /*MRF_IDENT 3500*/ //max line to display success transactions
#define MAX_LINE_UNSUCCESS_TRANS_DISPLAY            200 /*MRF_IDENT 2000*/ //max line to display unsuccess transactions

#define MAX_UNSUCCESS_BUY_TRANS                     50
#define MAX_UNSUCCESS_CHARGE_TRANS                  50
#define MAX_UNSUCCESS_BILLPAY_TRANS	                50
#define MAX_UNSUCCESS_LOANPAY_TRANS	                50 
#define MAX_UNSUCCESS_ETC_TRANS                     50
#define MAX_UNSUCCESS_TOPUP_TRANS                   50 //MRF_TOPUP
#endif

#if defined(ICT250) || defined(IWL220)

#define MAX_LINE_SUCCESS_TRANS_DISPLAY              200//3500 //max line to display success transactions
#define MAX_LINE_UNSUCCESS_TRANS_DISPLAY            200 //max line to display unsuccess transactions
#define SEPERATED_STR_SIZE							50 //HNO_IDENT

#define MAX_BUY_TRANS_LOG_RECORDS                   700 // max buy log records //HNO: change 200 to 1000
#define MAX_BILL_PAY_TRANS_LOG_RECORDS              500 // max BILLpAY log records //HNO: change 200 to 1000
#define MAX_INSURANCE_TRANS_LOG_RECORDS             1000 // max INSURANCE log records //HNO: change 200 to 1000
#define MAX_CHARGE_TRANS_LOG_RECORDS                500 // max charge log records //HNO: change 200 to 1000
#define MAX_TRANS_ERROR_LOG_RECORDS                 300/* //mgh 250*/ // max error log records //HNO: change 200 to 1000
#define MAX_LOAN_PAY_TRANS_LOG_RECORDS              300	//HNO_LOAN
#define MAX_TOPUP_TRANS_LOG_RECORDS                 300 //MRF_TOPUP
#define MAX_ETC_TRANS_LOG_RECORDS                   300 //MRF_ETC
#define MAX_SUPERVISOR_TRANS_LOG_RECORDS            100  //MRF
#define MAX_ROLL_REQUEST_TRANS_LOG_RECORDS          100  //MRF
#define MAX_CHARITY_TRANS_LOG_RECORD 				100	//HNO_CHARITY

#define MAX_UNSUCCESS_BUY_TRANS                     50
#define MAX_UNSUCCESS_CHARGE_TRANS                  50
#define MAX_UNSUCCESS_BILLPAY_TRANS                 50
#define MAX_UNSUCCESS_LOANPAY_TRANS                 50
#define MAX_UNSUCCESS_ETC_TRANS                     50 //MRF_ETC
#define MAX_UNSUCCESS_TOPUP_TRANS                   50 //MRF_TOPUP
#define MAX_UNSUCCESS_CHARITY_TRANS                 50	//HNO_CHARITY

#endif

/****************Identifier Code for supervisor****************/
#define TEST_POS                                    1001
#define UPDATE_APP                                  1002
#define UPDATE_GUIDE                                1003
#define VISITOR_CASE                                1004
#define PERIODIC_VISIT                              1005
#define UPDATE_CONTRACT                             1006
#define UPDATE_LABLE                                1007
#define CHANGE_TERMINAL                             1008 
#define REMOVE_POS                                  1009
#define INSTALLATION_TRANS                          1010

//1: mean the header is printed, do not print it again
//0: mean the header should be printed
static uint8 prePrintFlags = 0;//[20] = {0};

void    addBuyReport(messageSpecST* messageSpec);
void    addBillPayReport(messageSpecST* messageSpec);
void    addChargeReport(messageSpecST* messageSpec);
void    addLoanPayReport(messageSpecST* messageSpec);
void    addETCReport(messageSpecST* messageSpec);
void    addTopupReport(messageSpecST* messageSpec);
void    addSupervisiorReport(messageSpecST* messageSpec);

void    getIssuerBankName(uint8* BIN, uint8* Name);
void    getBillPayTypeStr(int16 billType, uint8* billTypeStr);
uint8   printChargeType(uint8 chargeType, uint8* nominalValueWithComma);

void resetPrePrintFlags(void)
{
    prePrintFlags = 0;
}

void makeTransPrintDataLog(filesST* files, messageSpecST* messageSpec)                        
{
    int16                       retValue 					= FAILURE;       /** write in file function return value */
    uint8                       fileName[FILE_NAME_LENTGH]	= {0};
	uint8                       loginUserName[15 + 1]       = {0}; //ABS:ADD
    lastCustomerReceiptST       lastCustomerReceipt;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****Make Trans Print Data Log*****");
    
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (messageSpec->transType == TRANS_BUY)
    {
    	strcpy(fileName, LAST_CUSTOMER_BUY_RECIEPT_FILE);

		if (getPOSService().shiftService == TRUE)//HNO_SHIFT
		{
			getLoginUser(loginUserName);  //ABS:ADD
			if(strcmp("����", loginUserName) != 0)//HNO_SHIFT10
				strcat(fileName, loginUserName);
		}
    }

    else if (messageSpec->transType == TRANS_BILLPAY)
    	strcpy(fileName, LAST_CUSTOMER_BILL_RECIEPT_FILE); 
    else if (messageSpec->transType == TRANS_BUYCHARGE) 
        strcpy(fileName, LAST_CUSTOMER_CHARGE_RECIEPT_FILE);
    else if (messageSpec->transType == TRANS_LOANPAY)
        strcpy(fileName, LAST_CUSTOMER_LOAN_PAY_RECIEPT_FILE); 
     else if (messageSpec->transType == TRANS_ETC)
        strcpy(fileName, LAST_CUSTOMER_ETC_RECIEPT_FILE); 
     else if (messageSpec->transType == TRANS_TOPUP)    //MRF_TOPUP
        strcpy(fileName, LAST_CUSTOMER_TOPUP_RECIEPT_FILE); 
     else if (messageSpec->transType == TRANS_ROLL_REQUEST)//HNO_ADD_ROLL
        strcpy(fileName, LAST_CUSTOMER_ROLL_REQUEST_RECIEPT_FILE);
     else if (messageSpec->transType == TRANS_CHARITY)
        strcpy(fileName, LAST_CUSTOMER_CHARITY_RECIEPT_FILE);
    
    strcpy(lastCustomerReceipt.PAN, messageSpec->cardSpec.PAN);
    strcpy(lastCustomerReceipt.amount, messageSpec->amount);
    strcpy(lastCustomerReceipt.approvalCode, messageSpec->approvalCode);
    strcpy(lastCustomerReceipt.customerName, messageSpec->cardSpec.customerName);
    strcpy(lastCustomerReceipt.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(lastCustomerReceipt.preAmount, messageSpec->preAmount);
    strcpy(lastCustomerReceipt.depositID, messageSpec->depositID);
    strcpy(lastCustomerReceipt.tipAmount, messageSpec->tipAmount);
    
    lastCustomerReceipt.billSpec = messageSpec->billSpec;
    lastCustomerReceipt.dateTime = messageSpec->dateTime;
    lastCustomerReceipt.merchantSpec = messageSpec->merchantSpec;
    lastCustomerReceipt.transType = messageSpec->transType;
    lastCustomerReceipt.buyChargeSpec = messageSpec->buyChargeSpec;    
    lastCustomerReceipt.loanPay = messageSpec->loanPay;
    lastCustomerReceipt.ETC = messageSpec->ETC;
    lastCustomerReceipt.topup = messageSpec->topup; //MRF_TOPUP
    lastCustomerReceipt.rollRequest = messageSpec->rollRequest; //HNO_ADD_ROLL
    lastCustomerReceipt.charity = messageSpec->charity;
    
    //HNO_ADD_LOAN add if
    if (messageSpec->responseStatus != FAILURE)
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: lastCustomerBuyReceiptFile");
	    retValue = updateFileInfo(fileName, &lastCustomerReceipt, sizeof(lastCustomerReceiptST));    
	    if (retValue != SUCCESS)
	        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }

    if (messageSpec->transType != TRANS_BUYCHARGE) //MRF_NEW12
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: customerReceiptFile");
        retValue = updateFileInfo(files->customerReceiptFile, &lastCustomerReceipt, sizeof(lastCustomerReceiptST));    
        if (retValue != SUCCESS)
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM) ;
    }
}

void addBuyReport(messageSpecST* messageSpec)  
{
    int16           retValue						= FAILURE;		/** write in file function return value */
    uint8			fileName[FILE_NAME_LENTGH]		= {0};
    uint8           activeUserName[16]              = {0};//ABS
    buyTransLogST   transaction;
	

    memset(&transaction, 0, sizeof(buyTransLogST));
    
    strcpy(fileName, BUY_LOG_FILE);
    
	if (getPOSService().shiftService)//HNO_SHIFT
	{
		getActiveUser(activeUserName);		//ABS:CHANGE
		if(strcmp("����", activeUserName) != 0)//HNO_SHIFT10
			strcat(fileName, activeUserName);	//ABS:CHANGE
	}
    
    transaction.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->cardSpec.PAN, transaction.PAN);
    strcpy(transaction.amount, messageSpec->amount); 
    strcpy(transaction.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(transaction.depositID, messageSpec->depositID);  //MRF_NEW17
    strcpy(transaction.tipAmount, messageSpec->tipAmount);  //MRF_NEW17
    
    retValue = appendFixedFileInfo(fileName, &transaction, 
                                     sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}
 
void addBillPayReport(messageSpecST* messageSpec)  
{
    int16          		retValue                    = FAILURE;              /** write in file function return value */
    uint8				fileName[FILE_NAME_LENTGH]	= {0};
    billPayTransLogST   transaction;                                        /** billPay transaction detail */
    
    memset(&transaction, 0, sizeof(billPayTransLogST));
    strcpy(fileName, BILL_PAY_LOG_FILE);
    
    transaction.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->cardSpec.PAN, transaction.PAN);
    strcpy(transaction.billAmount, messageSpec->billSpec.billAmount); 
    strcpy(transaction.billID, messageSpec->billSpec.billID);
    strcpy(transaction.paymentID, messageSpec->billSpec.paymentID);
    strcpy(transaction.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    
    retValue = appendFixedFileInfo(fileName, &transaction, 
                                     sizeof(billPayTransLogST), MAX_BILL_PAY_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

uint8 prePrint(int printRecLines, filesST* files)
{
	dateTimeST	dateTime	= {0, 0};
	
	dateTime = getDateTime();
    if (!printReceiptHeader(&dateTime, printRecLines, files))
        return FALSE;
    
    prePrintFlags = 1;//[files->id] = 1;
	return TRUE;
}

uint8 printUnsuccessTrans(uint8 transType)
{
    uint8 transName[12] = {0};
    
    if (!PrinterAccess(TRUE))    
        return FALSE;
    
    getIsoTransactionName(transName, transType);
    
    if (!printOneStringFarsi(transName, PRN_NORM, ALIGN_CENTER))
        return FALSE;
    
    printStar();
    
    if (!printOneStringFarsi("��ǘ�� ����� ���!", PRN_NORM, ALIGN_CENTER))
        return FALSE;
    
    printStar();
    printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}


/**
 * print header of transaction receipt.
 * @param   dateTime transaction date and time.
 * @param   prinrRecLines print recied header lines?
 * @see     printLogo()
 * @see     printReceiptDateTime()
 * @return  TRUE or FALSE.
 */
uint8 printReceiptHeader(dateTimeST* dateTime, int printRecLines, filesST* files) 
{
    uint8           marketName[100]		= {0};       //MRF: CHANGE IT   /** market name */
    uint8           headerLine1[31]		= {0};          /** line1 of header message postal code*/
    uint8           headerLine2[31]		= {0};          /** line2 of header message phone*/
    uint8           headerLine3[300]	= {0};          /** line3 of header message Address MRF & HNO */
    uint8           shift[22]           = {0};
	uint8           activeUserName[16]  = { 0 };//ABS
    merchantSpecST  merchantSpec;

    memset(&merchantSpec, 0, sizeof(merchantSpecST));    
    
    readMerchantSpec(files, &merchantSpec); //TODO: MRF OUTPUT NOT CHECKED
#ifndef VX520  //ABS:ADD:960801
    initPrinter(); 
#endif
    if (!PrinterAccess(TRUE))    
        return FALSE;
    
    /** PRINT LOGO */
#if defined(ICT250) || defined(IWL220)//HNO_ADD
    if (!printLogo(files, BankLogo))
    	return FALSE;
#else
	if (!printLogo(files, ShaparakLogo))
		return FALSE;
#endif

    /** PRINT MARKET NAME */

    strcpy(marketName, merchantSpec.marketName);
    removePad(marketName, ' ');
    
    if (strlen(marketName) > 0) 
    {    
#ifdef VX520   //ABS:ADD:960801
		if (!printOneString_FAEN(marketName, PRN_BIG, ALIGN_CENTER))
			return FALSE;
#else
		if (!printOneStringFarsi(marketName, PRN_BIG, ALIGN_CENTER))
			return FALSE;
#endif
    }
    

    /** PRINT HEADER LINES */
    if (printRecLines) 
    {
        
        /** PRINT CODE POSTI */
        strcpy(headerLine1, merchantSpec.postalCodeMarket); // MRF: SIZE OF VARIABLE NOT EQUAL
        removePad(headerLine1, ' ');
        
       if (strlen(headerLine1) > 0) 
        {
            if (!printTwoStringNumericFarsi(headerLine1, "�ρ���", PRN_NORM))
                return FALSE;
        }
        
        /** PRINT TEL NUMBER */
       strcpy(headerLine2, merchantSpec.merchantPhone); // MRF: SIZE OF VARIABLE NOT EQUAL
       removePad(headerLine2, ' ');

       if (strlen(headerLine2) > 0) 
        {
            if (!printTwoStringNumericFarsi(headerLine2, "����� ����", PRN_NORM))
                return FALSE;
        } 
       
       /**PRINT ADDRESS MARKET*/
       strcpy(headerLine3,merchantSpec.marketAddress);
       removePad(headerLine3,' ');
       if (strlen(headerLine3) > 0)
            if (!printOneStringFarsi(headerLine3,PRN_NORM,ALIGN_CENTER))
                return FALSE;
    }  

    /** PRINT ACTIVE USER */
     if (getPOSService().shiftService)
	 {
		 getActiveUser(activeUserName);
		 if (!strcmp(activeUserName, ""))
         {
#ifdef VX520		//ABS:ADD
			 printTwoStringNumericFarsi("---", "���� ����", PRN_NORM);
#else
			 printTwoString("���� ����:", "---", PRN_NORM, TRUE);
#endif	
         }
         else
         {
            strcpy(shift, "���� ");
			strcat(shift, activeUserName);
            printTwoStringFarsi(shift, "���� ����:", PRN_NORM);
         }
     }
      
    /** PRINT STAR */
    if (!printStar())
        return FALSE; 
    
    return TRUE;
}


/**
 * print footer of transaction receipt.
 * @param   reprint is reprint receipt?
 * @param   printTel print tel?
 * @param   emptyLine print empty lines end of receipt?
 * @return  TRUE or FALSE.
 */
uint8 printTransactionFooter(uint8 reprint, uint8 customer, uint8 emptyLine, filesST* files) 
{
    uint8               telNo[20]       = {0};
    uint8               footerMseg[31]	= {0};
    merchantSpecST      merchantSpec;
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*********printTransactionFooter*********");
    
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    readMerchantSpec(files, &merchantSpec);    
  
    if (!PrinterAccess(TRUE))    
        return FALSE;
    
    if ((!customer && !reprint )&& (fileExist(SUPERVISOR_FILE) != SUCCESS))
    {
        if (!printOneStringFarsi("����� �����:", PRN_NORM, ALIGN_RIGHT))
               return FALSE;
    }

    printStar();//MRF_FOOTER
    
    /**if (merchantSpec.merchantID[0] == '1')
    {
        if (!printOneStringFarsi("�� ��� ����", PRN_SMALL, ALIGN_CENTER)) //MRF_FOOTER
            return FALSE;
    }
    else if (merchantSpec.merchantID[0] == '9')
    {
        if (!printOneStringFarsi("����� ������� �������", PRN_SMALL, ALIGN_CENTER)) //MRF_FOOTER
            return FALSE;
    }*/

    if (strlen((const char*)merchantSpec.helpPhone) > 0) 
    {
        strcpy(telNo, "021-");
        
        removePad(merchantSpec.helpPhone, ' ');
        strcat(telNo, merchantSpec.helpPhone);

        selectSmallPrinterFontFarsi();
#ifdef CASTLES_V5S //MRF_TTF
        reversPartString(telNo);
#endif
        if (fileExist(SUPERVISOR_FILE) != SUCCESS)
        {
            if (merchantSpec.merchantID[0] == '1')
            {
#ifdef VX520
					if (!printTwoStringNumericFarsi(telNo, "�� ��� ����", PRN_SMALL)) //ABS:CHANGE:960808
#else
					 if (!printTwoStringFarsi(telNo,"�� ��� ����", PRN_SMALL))
#endif
                {
                    selectNormalPrinterFontFarsi();
                    return FALSE;
                }
            }
            else if (merchantSpec.merchantID[0] == '9')
            {
                if (!printTwoStringFarsi(telNo,"����� ������� �������", PRN_SMALL))
                {
                    selectNormalPrinterFontFarsi();
                    return FALSE;
                }
                    
            }
            
           /**if (!printTwoStringNumericFarsi(telNo, "����� ����� �������", PRN_SMALL))
            {
                selectNormalPrinterFontFarsi();
                return FALSE;
            }*/
        }
        selectNormalPrinterFontFarsi();
    }   

    if (!reprint && strlen(merchantSpec.footerLine) > 0) 
    {
        strcpy(footerMseg, merchantSpec.footerLine);
        removePad(footerMseg, ' ');
        
        if (strlen(footerMseg) > 0) 
        {
            if (!printOneStringFarsi(footerMseg, PRN_NORM, ALIGN_CENTER))
                return FALSE;
        }
    }

    if (!emptyLine)
    {
        if (!printStar())
            return FALSE;
    }
    
    printBlankLines(BLANK_LINES_COUNT);
    return TRUE;
}


uint8 printTransactionCommonPart(messageSpecST* messageSpec, uint8 customer, uint8 rePrint, filesST* files)
{
    uint8           cardID[17]              = {0};          /** card ID */
    uint8           stanNo[7]               = {0};          /** stan string */
    uint8           valueWithComma[19 + 1]  = {0};          /** value with comma string */
    uint8           amountPrintBuffer[20]   = {0}; 
    uint8           IssuerName[20]          = {0};
    uint8           reversalCancelled       = FALSE; 
    uint8           supervisor              = FALSE;
    uint8           transType[30]           = {0};
    uint8           title[40]               = {0};
    uint8           tmp[30]                 = {0};
    uint8           printStr[100]           = {0};
    uint8           buffer[50]              = {0};
    uint8           phoneNumber[12]         = {0};
    uint8           output[12]              = {0};
    uint8           reversStr[250]          = {0};
    uint8           chargeID[20]			= {0};
    uint8           chargeTypeStr[20 + 1] 	= {0};
    uint8           chargeStr[20]			= {0};
    uint8           instituteName[50]		= {0};
    terminalSpecST  terminalCapability      = getTerminalCapability();
    serviceSpecST   POSService              = getPOSService();

    if (!PrinterAccess(TRUE))    
        return reversalCancelled;

    if ((fileExist(SUPERVISOR_FILE) == SUCCESS) && //For detect supervisor
            (fileExist(files->reversalReceiptFile) != SUCCESS)) 
        supervisor = TRUE;
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "preAmount: %s", messageSpec->preAmount);
    getIsoTransactionName(transType, messageSpec->transType); 
    
    /** PRINT TITLE */
    if ((rePrint == REPRINT) || (rePrint == CHARGE_REPRINT ))
    {
        //MGH: changed all
        strcpy(tmp,"���� ���� �����");
        sprintf(title,"%s/",transType);        
        strcat(title, tmp);
        if (!printOneStringFarsi(title, PRN_NORM, ALIGN_CENTER))
            return reversalCancelled;
    }
    else if (customer)
    {
        if (messageSpec->transType  == TRANS_BALANCE 
                || messageSpec->transType  == TRANS_ETC_TRACKING
                || messageSpec->transType  == TRANS_LOAN_TRACKING)
        {
            sprintf(title,"%s",transType);
            if (!printOneStringFarsi(title, PRN_NORM, ALIGN_CENTER))
                return reversalCancelled;
        }
        else
        {   
            strcpy(tmp,"���� �����");
            sprintf(title,"%s/",transType);        
            strcat(title, tmp);
            if (!printOneStringFarsi(title, PRN_NORM, ALIGN_CENTER))
                return reversalCancelled;
        }
    }
    else
    {
        if (!supervisor)
        {
            strcpy(tmp,"���� �������");
            sprintf(title,"%s/",transType);        
            strcat(title, tmp);
            if (!printOneStringFarsi(title, PRN_NORM, ALIGN_CENTER))
                return reversalCancelled;
        }
    }  

    
    if(messageSpec->transType == TRANS_CHARITY)
    {
    	getCharityInstituteName(instituteName, messageSpec->charity.InstituteCode);
    	if(!printOneStringFarsi(instituteName, PRN_NORM, ALIGN_CENTER))
    		return reversalCancelled;
    }
    
    /** PRINT TERMINAL ID */
    if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.terminalID, "����� ������", PRN_NORM))
        return reversalCancelled;
    
    /** PRINT MERCHANT ID */
    /**if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.merchantID, "����� �������", PRN_NORM))
        return reversalCancelled;*/
    
    /** PRINT DEPOSIT ID */ 
//    len = strlen(messageSpec->depositID);
    if ((getPOSService().depositID) && (messageSpec->transType == TRANS_BUY)
            && (fileExist(SUPERVISOR_FILE) != SUCCESS)
            &&(messageSpec->responseStatus == SUCCESS))
    {           if (strlen(messageSpec->depositID) > 0)//MRF_NEW20
                {
                    if (!printTwoStringNumericFarsi(messageSpec->depositID, "����� �����", PRN_NORM))
                        return reversalCancelled;
                }
    }
    
    if(messageSpec->transType == TRANS_BUYCHARGE)
    {
        int				count				= 0;
        count = messageSpec->charge.chargeCount;

        /**PRINT CHARGE SERIAL*/
        if ((messageSpec->responseStatus == SUCCESS) || (messageSpec->responseStatus == 8) 
                    || (messageSpec->responseStatus == 16))
        {
            if (!PrinterAccess(TRUE))
                return reversalCancelled;
            
            sprintf(chargeID, "%ld", messageSpec->buyChargeSpec.id);
        	if (!printTwoStringNumericFarsi(chargeID,"����� ��ю",PRN_NORM))
        		return reversalCancelled;

        	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "chargeID: %s", chargeID);
            removePad(messageSpec->buyChargeSpec.serial, ' ');
            if (!printTwoStringNumericFarsi(messageSpec->buyChargeSpec.serial, "����� ��ю", PRN_NORM))
                return reversalCancelled;

                if (rePrint == NO_REPRINT)
                {
                    //Remove reversal and add settlement after serial for security of PIN
                    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
                    fileRemove(files->reversalTransFile);
                    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
                    fileRemove(files->reversalReceiptFile);

                    addTransToSettlementFile(files, messageSpec);
                    makeTransPrintDataLog(files, messageSpec);
                    addChargeReport(messageSpec);
       
                    /** save charge information for print complete report **/
					strcpy(messageSpec->charge.chargeInfo[count].serial, messageSpec->buyChargeSpec.serial);
					strcpy(messageSpec->charge.chargeInfo[count].amount, messageSpec->buyChargeSpec.amount);
					strcpy(messageSpec->charge.chargeInfo[count].PIN, messageSpec->buyChargeSpec.PIN);
					messageSpec->charge.chargeInfo[count].type = messageSpec->buyChargeSpec.type;
					messageSpec->charge.chargeInfo[count].dateTime = messageSpec->dateTime;
					strcpy(messageSpec->charge.chargeInfo[count].ReferenceNumber, messageSpec->retrievalReferenceNumber);
					messageSpec->charge.id = messageSpec->buyChargeSpec.id;
                    savechargeInfo(messageSpec->charge,count);
                    reversalCancelled = TRUE;
                }
                if ((rePrint == CHARGE_REPRINT) || (rePrint == NO_REPRINT))
                {
                    /**PRINT PIN */
                    removePad(messageSpec->buyChargeSpec.PIN, ' '); 

                    /** PRINT CHARGE TYPE */
                    getChargeTypeStr(messageSpec->buyChargeSpec.type, chargeTypeStr);
                    strcpy(chargeStr, "��� ��ю");
                    strcat(chargeTypeStr, ":");
                    strcat(chargeStr, chargeTypeStr);
                    if (!printOneStringFarsi(chargeStr, PRN_NORM, ALIGN_RIGHT))
                        return reversalCancelled;

                    if(!printOneStringFarsi(messageSpec->buyChargeSpec.PIN,PRN_BIG,ALIGN_CENTER))
                        return reversalCancelled;
               
                }
        }
        else
        {
        	getChargeTypeStr(messageSpec->buyChargeSpec.type, chargeTypeStr);
#ifdef VX520
			printTwoStringFarsi(chargeTypeStr, "��� ��ю:", PRN_NORM);//ABS:CHANGE
#else
        	printTwoStringNumericFarsi(chargeTypeStr, "��� ��ю", PRN_NORM);
#endif
        }
    }
    
    if (messageSpec->transType == TRANS_TOPUP && customer == TRUE) //MRF_TOPUP
    {
        /** PRINT MOBILE NUMBER */
        maskMobileNumbers(messageSpec->topup.mobileNo, output);
        
#ifdef CASTLES_V5S //MRF_TTF
        phoneNumber[0] = '0';
        strcat(phoneNumber, output);
        reversPartString(phoneNumber);
#else
        phoneNumber[0] = '0';
        strcat(phoneNumber, output);
#endif
        if (!printTwoStringNumericFarsi(phoneNumber, "����� ���� �����", PRN_NORM))
            return reversalCancelled;
    }
    
    
    if ((messageSpec->transType == TRANS_LOANPAY) || (messageSpec->transType == TRANS_LOAN_TRACKING))
    {
        uint8           dstCardID[19 + 1]       = {0};
 
        /** PRINT DESTINATION CARD NUMBER */
            maskCardId(messageSpec->loanPay.destinationCardPAN, dstCardID);
#ifdef CASTLES_V5S //MRF_TTF
        reversPartString(dstCardID);
#endif
#ifdef VX520
		if (!printTwoStringNumericFarsi(dstCardID, "����� ���� ���", PRN_NORM))
			return reversalCancelled;
#else
            if (!printTwoStringFarsi(dstCardID, "����� ���� ���:", PRN_NORM))
                return reversalCancelled;
#endif           
            /** PRINT DESTINATION NAME */
            memset(printStr, 0, sizeof(printStr));
			sprintf(printStr, "%s %s", messageSpec->loanPay.destinationCardHolderName, messageSpec->loanPay.destinationCardHolderFamily);
#ifdef VX520
			if (!printTwoStringFarsi(printStr, "�� ���:", PRN_NORM))    //ABS:CHANGE:961213
                return reversalCancelled;
#else
			if (!printTwoStringFarsi(printStr, "�� ���:", PRN_NORM))     //ABS:CHANGE:961213
				return reversalCancelled;
#endif
        messageSpec->merchantSpec.recieveSTAN = messageSpec->loanPay.STAN;
    }
    
    if (messageSpec->transType == TRANS_BILLPAY)
    {
        uint8	shGabzValue[14]         = {0};
        uint8	shPardakhtValue[14] 	= {0};
        uint8	billTypeStr[15 + 1]     = {0};

        /** PRINT SHENASE GHABZ */
        memcpy(shGabzValue, messageSpec->billSpec.billID, 13);
        shGabzValue[13] = 0;
        removePadLeft(shGabzValue,'0'); 
        if (!printTwoStringNumericFarsi(shGabzValue, "����� ���", PRN_NORM))
            return reversalCancelled;

        /** PRINT SHENASE PARDAKHT */
        memcpy(shPardakhtValue, messageSpec->billSpec.paymentID, 13);
        shPardakhtValue[13] = 0;
        removePadLeft(shPardakhtValue, '0');  
        if (!printTwoStringNumericFarsi(shPardakhtValue, "����� ������", PRN_NORM))
            return reversalCancelled;

        getBillPayTypeStr(messageSpec->billSpec.type, billTypeStr);
        if (!printTwoStringFarsi(billTypeStr, "��� ���:", PRN_NORM))
            return reversalCancelled;
    }
    
    if (messageSpec->transType == TRANS_ETC || messageSpec->transType == TRANS_ETC_TRACKING)
    {
        {   /** PRINT SERIAL ETC */
            if (!printTwoStringNumericFarsi(messageSpec->ETC.serialETC, "����� �э��", PRN_NORM)) 
                return reversalCancelled;

            /** PRINT DESTINATION NAME */
            sprintf(printStr, "%s %s", messageSpec->ETC.cardHolderName, messageSpec->ETC.cardHolderFamily);
            if (!printTwoStringFarsi(printStr, "�� ���:", PRN_NORM))
                return reversalCancelled;
        }
        messageSpec->merchantSpec.recieveSTAN = messageSpec->ETC.STAN;
    }
    
    if ((!supervisor) || (rePrint))
    {
        /** PRINT CARD NUMBER */
        if (messageSpec->transType == TRANS_ETC_TRACKING)
        	maskCardId(messageSpec->ETC.PAN, cardID);
        else if(messageSpec->transType == TRANS_LOAN_TRACKING)
        	maskCardId(messageSpec->loanPay.PAN, cardID);
        else
        	maskCardId(messageSpec->cardSpec.PAN, cardID);
        
        removePad(cardID, ' ');
#ifdef CASTLES_V5S  //MRF_TTF
        strcpy(reversStr, cardID);
        reversPartString(reversStr); 
        if (!printTwoStringNumericFarsi(reversStr, "����� ����", PRN_NORM))
            return reversalCancelled;
#else
     if (!printTwoStringNumericFarsi(cardID, "����� ����", PRN_NORM))
            return reversalCancelled;  
#endif

        /** PRINT ISSUER BANK */
        if (customer)
        {
            uint8 BIN[7] = {0};

            strncpy(BIN, cardID, 6);
            BIN[6] = 0;
            getIssuerBankName(BIN, IssuerName);
            if ((strlen(IssuerName)+ 17) > getPrinterCharacterCount())
            {
                if (!printOneStringFarsi("��� ���� �����:", PRN_NORM, ALIGN_RIGHT))
                    return reversalCancelled;
                if (!printOneStringFarsi(IssuerName, PRN_NORM, ALIGN_RIGHT))
                    return reversalCancelled;
            }         
            else if (!printTwoStringFarsi(IssuerName, "��� ���� �����:", PRN_NORM))
                return reversalCancelled;      
        }
        
    /** PRINT PAZIRANDE BANK */
    if(!customer)
        if (!printTwoStringFarsi("�Ә�", "��� �������:", PRN_NORM))
            return reversalCancelled;

    /** PRINT STAN */
    sprintf(stanNo, "%ld", messageSpec->merchantSpec.recieveSTAN);
    if (!printTwoStringNumericFarsi(stanNo, "����� �����", PRN_NORM))
        return reversalCancelled;
    }
    
    /** PRINT DATE AND TIME */
    if (!printReceiptDateTime(&(messageSpec->dateTime), TRUE)) 
        return reversalCancelled;
        
    /** PRINT REFERENCE NUMBER */
    if ((messageSpec->responseStatus == SUCCESS) || (messageSpec->responseStatus == 8)
            || (messageSpec->responseStatus == 16))
    {
        removePad(messageSpec->retrievalReferenceNumber, ' ');

        if (!printTwoStringNumericFarsi(messageSpec->retrievalReferenceNumber, "����� ����", PRN_NORM))
                return reversalCancelled;        
    }
    
    /** PRINT TIP AMOUNT *///+HNO_970924
    if ((getPOSService().tip) && (messageSpec->transType == TRANS_BUY) && ((!messageSpec->pcPos))
            && (fileExist(SUPERVISOR_FILE) != SUCCESS))
    {
        if (strlen(messageSpec->tipAmount) != 0 && (messageSpec->tipAmount[0] != '0'))//MRF_NEW20
        {
            makeValueWithCommaStr(messageSpec->tipAmount, valueWithComma);
            if (!printThreeStringFarsi(" ����", valueWithComma , "���� �����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return reversalCancelled;
        }
    }
    
    /** PRINT CHARGE INFORMATION */ 
    if (messageSpec->transType == TRANS_BUYCHARGE) 
        makeValueWithCommaStr(messageSpec->buyChargeSpec.amount, valueWithComma);

    if ((messageSpec->transType == TRANS_ETC_TRACKING) || messageSpec->transType == TRANS_LOAN_TRACKING)
        return TRUE;
    
    if ((!supervisor)|| (rePrint))
    {   
        /** PRINT TRANS STATUS */
        if ((messageSpec->responseStatus != SUCCESS) && (messageSpec->responseStatus != 8)
                && (messageSpec->responseStatus != 16))
        {
            uint8 errorMessage[50] = {0};
            uint8 errorCode[10]    = {0};
            int   shpState         = 0; 
            
            sprintf(errorCode, "%2d", messageSpec->responseStatus);
            shpState = getShaparakMessages(errorMessage, messageSpec->responseStatus);

            if ((rePrint == NO_REPRINT) && (shpState == WARNING_TRANS || shpState == NO_RECEIVE || shpState == DUPLICATE_TRANS))
            {
                if (customer) 
                {
                    if ((messageSpec->transType != TRANS_ETC) && (messageSpec->transType != TRANS_TOPUP)
                    		&& (messageSpec->transType != TRANS_LOANPAY))
                    {
					  if (!printTwoStringNumericFarsi(errorCode, "������ ������", PRN_NORM_INVERSE))
						  return reversalCancelled;
                    }
                    else if (messageSpec->transType == TRANS_ETC || messageSpec->transType == TRANS_TOPUP
                    		|| messageSpec->transType == TRANS_LOANPAY) //MRF_TOPUP
                    {
                        if (!printTwoStringNumericFarsi(errorCode, "������ ������", PRN_NORM_INVERSE))
                            return reversalCancelled;     
                    }
                }
                else if (!printOneStringFarsi("������ ������(�����...)", PRN_NORM, ALIGN_CENTER))
                        return reversalCancelled;    
            }  
#if defined(CASTLES_V5S)|| defined(VX520)//ABS:ADD
            else if (!printTwoStringNumericFarsi(errorCode, errorMessage, PRN_NORM_INVERSE))
            {
                    return reversalCancelled; 
            }
#else
            else if (!printTwoStringFarsi(errorCode, errorMessage, PRN_NORM))
            {
                    return reversalCancelled; 
            }
#endif

            if ((messageSpec->transType == TRANS_BUY || messageSpec->transType == TRANS_BILLPAY || 
                    messageSpec->transType == TRANS_BUYCHARGE || messageSpec->transType == TRANS_CHARITY) && 
                    (shpState == INVALID_TRANS || shpState == NO_RECEIVE))
            {
#ifdef CASTLES_V5S //MRF_TTF
                if (!printOneStringFarsi("�� ���� ��� ��� �� ���� ��� ���� �И�� �� 72 ���� �� ���� ��� ���� ���� ����� �� �� ��� ��� ���� ��� ����� ���� �� ����� ���� 43570-021 ���� ���� ������.", PRN_NORM, ALIGN_CENTER))
                    return reversalCancelled;
#else
#if defined(ICT250) || defined(IWL220)//HNO_ADD
            	if (!printOneStringFarsi("�� ���� ������ ������ ��� ���� �И�� �� 72",PRN_NORM, ALIGN_CENTER))
					return reversalCancelled;
				if (!printOneStringFarsi("���� �� ���� ������� ����� �� �� ���", PRN_NORM,ALIGN_CENTER))
					return reversalCancelled;
				if (!printOneStringFarsi("������� ��� ����� ���� �� ����� ����", PRN_NORM,ALIGN_CENTER))
					return reversalCancelled;
				if (!printOneStringFarsi("021-43570���� ���� �������", PRN_NORM,ALIGN_CENTER))
					return reversalCancelled;
#else
                if (!printOneStringFarsi("�� ���� ��� ��� �� ���� ��� ���� �И�� �� 72 ���� �� ���� ��� ���� ���� ����� �� �� ��� ��� ���� ��� ����� ���� �� ����� ���� 021-43570 ���� ���� ������.", PRN_NORM, ALIGN_CENTER))
                    return reversalCancelled;
#endif
#endif
            }
            else if (((messageSpec->transType == TRANS_ETC)|| (messageSpec->transType == TRANS_LOANPAY)) &&
                    (shpState == INVALID_TRANS || shpState == NO_RECEIVE))
             {
				#if defined(ICT250) || defined(IWL220)
                    if (!printOneStringFarsi("����� ���� ������ �� �� ���� ��ǘ��", PRN_NORM, ALIGN_CENTER))
                        return FALSE;
                    if (!printOneStringFarsi("����� ����� ������", PRN_NORM, ALIGN_CENTER))
                        return FALSE;
				#else
                    if (!printOneStringFarsi("����� ���� ������ �� �� ���� ��ǘ�� ������ ����� ������.", PRN_NORM, ALIGN_CENTER))
                        return FALSE; 
#endif
             }
            else if ((messageSpec->transType == TRANS_TOPUP) && //MRF_TOPUP
                    (shpState == INVALID_TRANS || shpState == NO_RECEIVE))
             {
				#if defined(ICT250) || defined(IWL220)
				 if (!printOneStringFarsi("�� ���� ��� ��ю ���� ����� ����� ���� ������.", PRN_NORM, ALIGN_CENTER))
					return FALSE;
				 if (!printOneStringFarsi("�� �� ���� ǁ����� ����� ���� ��� ����� ������.", PRN_NORM, ALIGN_CENTER))
					return FALSE;
				#else
                 if (!printOneStringFarsi("�� ���� ��� ��ю ���� ����� ����� ���� ������ �� �� ���� ǁ����� ����� ���� ��� ����� ������.", PRN_NORM, ALIGN_CENTER))
                    return FALSE; 
				#endif
             }
        }  
        else if (!printOneStringFarsi("������ ����", PRN_BIG, ALIGN_CENTER))
                return reversalCancelled;
        
        /** PRINT AMOUNT */
        if ((messageSpec->responseStatus == SUCCESS) || (messageSpec->responseStatus == 8) 
                || (messageSpec->responseStatus == 16))
        {
            if (!PrinterAccess(TRUE))
                return reversalCancelled;
            
            if (messageSpec->transType == TRANS_BALANCE)
            {
                 /** PRINT "MANDEH HESAB" */
                if (messageSpec->balanceSpec.accountBalance[0] != 0) 
                {
                    removePadLeft(messageSpec->balanceSpec.accountBalance, '0');
                    if (strlen((const char*)messageSpec->balanceSpec.accountBalance) == 0)
                    {
                        valueWithComma[0] = '0';
                        valueWithComma[1] = 0;
                    }
                    else 
                        makeValueWithCommaStr(messageSpec->balanceSpec.accountBalance, valueWithComma);

                    if (!printOneStringFarsi("����� ������:", PRN_NORM, ALIGN_RIGHT))
                        return reversalCancelled;
                }
            }   
            else 
            {
                if (messageSpec->transType == TRANS_BUYCHARGE)
                    makeValueWithCommaStr(messageSpec->buyChargeSpec.amount, valueWithComma);   
                else if (messageSpec->transType == TRANS_ETC)
                    makeValueWithCommaStr(messageSpec->ETC.amount, valueWithComma); 
                else if (messageSpec->transType == TRANS_LOANPAY)
                    makeValueWithCommaStr(messageSpec->loanPay.payAmount, valueWithComma);
                else 
                    makeValueWithCommaStr(messageSpec->amount, valueWithComma);   
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "preAmount: %s", messageSpec->preAmount);
                //+HNO_970924
                if ((POSService.discount) && (!messageSpec->pcPos) && (messageSpec->transType == TRANS_BUY))
                {
                    makeValueWithCommaStr(messageSpec->preAmount, valueWithComma);
                   if (!printThreeStringFarsi(" ����", valueWithComma , "����", PRN_NORM, ALIGN_RIGHT, TRUE))
                        return reversalCancelled; 
                   
                   if (!printOneStringFarsi("���� �� �����:", PRN_NORM, ALIGN_RIGHT))
                        return reversalCancelled;
                }
                else 
                {
                    if (!printOneStringFarsi("����:", PRN_NORM, ALIGN_RIGHT))
                        return reversalCancelled;
                }
            }
            
            if ((POSService.discount) && (messageSpec->transType == TRANS_BUY))//MRF_NEW17
            {
                makeValueWithCommaStr(messageSpec->amount, valueWithComma);
                sprintf(amountPrintBuffer, "%s ����", valueWithComma);
                if (!printOneStringFarsi(amountPrintBuffer, PRN_BIG, ALIGN_CENTER))
                    return reversalCancelled;
            }
            else
            {
                sprintf(amountPrintBuffer, "%s ����", valueWithComma);
                removePadLeft(amountPrintBuffer,'0');//MRF_NEW20
                if (!printOneStringFarsi(amountPrintBuffer, PRN_BIG, ALIGN_CENTER))
                    return reversalCancelled;
            }
        }
    }
    
    if ((terminalCapability.pcPosCapability) && (POSService.PcPOSService))
    {
        if((customer) && (rePrint == NO_REPRINT))
        {
            if(strlen(messageSpec->information.advertise1) > 0)
                printOneStringFarsi(messageSpec->information.advertise1, PRN_NORM, ALIGN_CENTER);
        }
        if((!customer))
        {
            if(strlen(messageSpec->information.advertise2) > 0)
                printOneStringFarsi(messageSpec->information.advertise2, PRN_NORM, ALIGN_CENTER);
        }
    }

    return TRUE;
}


/**
 * print balance transaction receipt.
 * @param   PAN card number.
 * @param   available mablaghe ghabele bardasht.
 * @param   leger mande hesab.
 * @see     printReceiptHeader()
 * @see     printTransactionFooter()
 */
void printBalanceReceipt(filesST* files, messageSpecST* messageSpec) 
{
    uint8		valueWithComma[50]		= {0};          /** string format of value with comma */
    uint8		cardID[17]          	= {0};          /** card ID */
    uint8   	amountPrintBuffer[20]   = {0};
    dateTimeST	nowDateTime				= {0, 0};
    
    memset(&nowDateTime , 0,sizeof(dateTimeST));
    
    if (!PrinterAccess(TRUE))    
        return;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);    
    
    /** PRINT HEADER */
    if (!prePrintFlags)
    {
		if (!printReceiptHeader(&nowDateTime, TRUE, files)) 
			return;
    }
    
    prePrintFlags = 0;
       
    printTransactionCommonPart(messageSpec, TRUE, FALSE, files);
    printOneStringFarsi("������ ������ ����� ������1,206����",PRN_NORM,ALIGN_CENTER);
        
    /** PRINT FOOTER */    
    if (!printTransactionFooter(FALSE, TRUE, TRUE, files)) 
        return;    
}


/**
 * print buy transaction receipt.
 * @param   transactionInfo transaction information.
 * @param   customer is costomer receipt?
 * @see     printReceiptHeader()
 * @see     printTransactionJointPart()
 * @see     printTransactionFooter()
 * @return  TRUE or FALSE.
 */
uint8 printTransactionBuy(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)                           
{
    uint8	reversalCancelled           = FALSE;
    int8    i                           = 0;
    
    if (!PrinterAccess(TRUE))    
        return reversalCancelled;    

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);  
    
    /** PRINT HEADER */
    if (!prePrintFlags)
    	if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files)) 
    		return reversalCancelled;  
    
    prePrintFlags = 0;
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "preAmount: %s", messageSpec->preAmount);
    
    /** PRINT COMMON PART */
    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files)) 
        return reversalCancelled; 
        
    if ((customer && !rePrint) && ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8) 
            || (messageSpec->responseStatus == 16) ))
    {   
        /** CANCEL REVERSAL & ... */
        reversalCancelled = TRUE;
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        
        addTransToSettlementFile(files, messageSpec);
        makeTransPrintDataLog(files, messageSpec);
    	addBuyReport(messageSpec);
    }
    
    if (strlen(messageSpec->reward) > 0 &&  customer)
    {
        printStar();
#ifdef INGENICO	//HNO_IDENT
    	printOneStringFarsi("��� ����� ��� ���� ������� ����", PRN_BIG, ALIGN_CENTER);
    	printOneStringFarsi(" �� ���� ����.��� ������ ����� ��", PRN_BIG, ALIGN_CENTER);
    	printOneStringFarsi("����� ����� ������� ���� Ȑ����:)", PRN_BIG, ALIGN_CENTER);
#else
        printOneStringFarsi(messageSpec->reward, PRN_BIG, ALIGN_CENTER);
#endif
        printStar();
    }
    
    if(!customer)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);
    }
    
    /** PRINT TRANSACTION FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files)) 
        return reversalCancelled; // this true because print is complete

    if (strlen(messageSpec->reward) > 0 &&  customer)
        LEDBlink(2);


    if ((!reversalCancelled) && (!rePrint))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);        
    }
    
    return reversalCancelled;
}

void getBillPayTypeStr(int16 billType, uint8* billTypeStr)
{
	switch (billType)
    {
        case 1:
        	strcpy(billTypeStr, "��");
            break;
        case 2:
        	strcpy(billTypeStr, "���");
            break;
        case 3:
        	strcpy(billTypeStr, "���");
            break;
        case 4:
        	strcpy(billTypeStr, "����");
            break;
        case 5:
        	strcpy(billTypeStr, "���� �����");
            break;
        case 6:
        	strcpy(billTypeStr, "�������");
            break;
        case 8:
        	strcpy(billTypeStr, "������");
            break;
        case 9:
        	strcpy(billTypeStr, "����� ��������/����ϐ�");
            break;
    }
}


void printBillTypeStr(int16 billType)
{
	uint8	billTypeStr[15 + 1]		= {0};
	
    getBillPayTypeStr(billType, billTypeStr);
    printOneStringFarsi(billTypeStr, PRN_NORM, ALIGN_CENTER);
}


/**
 * print bill pay transaction receipt.
 * @param   dateTime transaction date and time.
 * @param   PAN card number.
 * @param   value amount.
 * @param   Stan stan, transaction number.
 * @param   approvalCode approval code.
 * @param   shGhbzPrdkht shenaseh ghabz & shenaseh pardakht.
 * @see     printReceiptHeader()
 * @see     printTransactionFooter()
 * @return  TRUE or FALSE.
 */
uint8 printTransactionBillPay(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint) 
{
    uint8	reversalCancelled		= FALSE;		/** is reversal canceled? */
    uint8	valueWithComma[20]		= {0};
    uint8   amountPrintBuffer[20]   = {0};

    if (!PrinterAccess(TRUE))    
        return reversalCancelled;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
            return reversalCancelled;
 
    prePrintFlags = 0;
//MGH    printBillTypeStr(messageSpec->billSpec.type);

    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
        return reversalCancelled;

    if ((customer && !rePrint) && ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8) 
            || (messageSpec->responseStatus == 16)))
    { 
        /** CANCEL REVERSAL & ... */
        reversalCancelled = TRUE;
        
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);
        
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        
        addTransToSettlementFile(files, messageSpec);           
        makeTransPrintDataLog(files, messageSpec);
    	addBillPayReport(messageSpec);
    }
    
    if(!customer)
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);
    }
    
    /** PRINT FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files)) 
        return reversalCancelled;
    
   if ((!reversalCancelled) && (!rePrint))
    {
	   showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);        
    }
    
    return reversalCancelled;
}


void getChargeTypeStr(uint8 chargeType, uint8* chargeTypeStr)
{
    switch (chargeType) 
    {
        case MULTI_MCI:
        	strcpy(chargeTypeStr, "����� ���");
            break;
        case MULTI_IRANCELL:
        	strcpy(chargeTypeStr, "�������");
            break; 
        case MULTI_RIGHTEL:
        	strcpy(chargeTypeStr, "�����");
            break;
        default:
            break;
    }	
}


uint8 printChargeType(uint8 chargeType, uint8* nominalValueWithComma)
{
	uint8	chargeTypeStr[15 + 1] = {0};
	
	getChargeTypeStr(chargeType, chargeTypeStr);
    
    if (!printTwoStringFarsi(chargeTypeStr,"��� ��ю:", PRN_NORM))
        return FALSE;
	return TRUE;
}


/**
 * add buy charge report to file.
 * @param   dateTime transaction date & time.    
 * @param   chargeType charge type.  
 * @param   value amount.  
 */
void addChargeReport(messageSpecST* messageSpec) 
{
    int16       retValue                    = FAILURE;                  /** file function return value */
    uint8       fileName[FILE_NAME_LENTGH]	= {0};
    chargeLogST charge;                                                 /** charge detail, for save in file */
  
    memset(&charge, 0, sizeof(chargeLogST)); 
    
    strcpy(fileName, CHARGE_LOG_FILE);
    
    charge.dateTime = messageSpec->dateTime;
    charge.type = messageSpec->buyChargeSpec.type;
    strcpy(charge.amount, messageSpec->buyChargeSpec.amount);
    strcpy(charge.realAmount, messageSpec->buyChargeSpec.realAmount);
    maskCardId(messageSpec->cardSpec.PAN, charge.PAN);
    strcpy(charge.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(charge.serial, messageSpec->buyChargeSpec.serial);
 
   retValue = appendFixedFileInfo(fileName, &charge , sizeof(chargeLogST), MAX_CHARGE_TRANS_LOG_RECORDS);    
   if (retValue != SUCCESS)
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM); 
}

//HNO
void addLoanPayReport(messageSpecST* messageSpec)  
{
    int16          		retValue                    = FAILURE;          /** write in file function return value */
    uint8				fileName[FILE_NAME_LENTGH]	= {0};
    loanPayTransLogST   loanPay;                                        /** billPay transaction detail */
    
    memset(&loanPay, 0, sizeof(loanPayTransLogST));
    strcpy(fileName, LOAN_PAY_LOG_FILE);
    
    loanPay.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->loanPay.destinationCardPAN, loanPay.destinationCardPAN);
    strcpy(loanPay.amount, messageSpec->loanPay.payAmount); 
    maskCardId(messageSpec->cardSpec.PAN, loanPay.PAN);
    strcpy(loanPay.destinationCardHolderName, messageSpec->loanPay.destinationCardHolderName);
    strcpy(loanPay.destinationCardHolderFamily, messageSpec->loanPay.destinationCardHolderFamily);
    strcpy(loanPay.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
   
    retValue = appendFixedFileInfo(fileName, &loanPay, 
                                     sizeof(loanPayTransLogST), MAX_LOAN_PAY_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
}


uint8 printTransactionBuyCharge(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)
{
    uint8       reversalCancelled           = FALSE;            /** is reversal canceled? */
    uint8*      reversedStr[50]             = {0};
    
    if (!PrinterAccess(TRUE))    
        return reversalCancelled;
    
    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);  

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files)) 
            return reversalCancelled;
    
    prePrintFlags = 0;
    
    /** PRINT COMMON PART */
    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files)) 
        return reversalCancelled;
    
     if ((customer && !rePrint) && ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8) 
            || (messageSpec->responseStatus == 16)))
     {
        /** CANCEL REVERSAL */
        reversalCancelled = TRUE;
     }
    
    if ((customer) && (!rePrint) && (messageSpec->responseStatus == SUCCESS) )
    {
        /**PRINT USING DISCRIPTION */ 
#if defined(ICT250) || defined(IWL220)//HNO_ADD because # doesn't print with one string
		if (!printTwoStringFarsi(messageSpec->buyChargeSpec.usingDescription,"���� �������",PRN_BIG))
			return reversalCancelled;
#else
        if(!printOneStringFarsi("���� �������:",PRN_NORM,ALIGN_RIGHT))
            return reversalCancelled;
        
        //MRF_TTF
        if (messageSpec->buyChargeSpec.type == MULTI_MCI)
        {
#ifndef VX520
            if(!printOneStringFarsi("#����� ��� ��ю#*140*",PRN_NORM, ALIGN_CENTER))
                return reversalCancelled; 
#else
			if(!printOneStringFarsi("#����� ��� ��ю*140*#", PRN_NORM, ALIGN_CENTER))//ABS:ADD:960822
				return reversalCancelled;
#endif
        }
        else
        {
            if(!printOneStringFarsi(messageSpec->buyChargeSpec.usingDescription,PRN_NORM, ALIGN_CENTER))
                return reversalCancelled;    
        }
#endif
    }
        
    if(!customer)
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);
    }
    
    /** PRINT TRANSACTION FOOTER */
    if (!printTransactionFooter(FALSE, TRUE, TRUE, files)) 
        return reversalCancelled; // this true because print is complete
        
    if ((!reversalCancelled) && (!rePrint))
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);        
    }
    
    return reversalCancelled;
}


/**
 * print reversal receipt for some failed transaction.
 * @param   showPrinterError show printer error?
 * @see     printReceiptHeader()
 * @see     printTransactionFooter()
 * @return  TRUE or FALSE.
 */
uint8 printReversal(filesST* files) 
{
    reversalTransST         reversalInfo;			
    merchantSpecST          merchantSpec;
    messageSpecST           messageSpec;
    dateTimeST              dateTime;
    lastCustomerReceiptST	lastCustomerReceipt;
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****printReversal Start****");
  
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&reversalInfo, 0, sizeof(reversalTransST));
    memset(&dateTime, 0, sizeof(dateTimeST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));
    
    if (!PrinterAccess(TRUE))  
        return FALSE;  
    
    if (!readMerchantSpec(files, &(messageSpec.merchantSpec)))
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);   
        return TRUE;
    }
     
    if (!readReversalInfo(files->reversalReceiptFile, &messageSpec))
    {
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);   
        return TRUE;
    }
    
    if (messageSpec.responseStatus == SUCCESS)
        messageSpec.responseStatus = 95; //mgh: for managing reversal printer
    
    switch (messageSpec.transType)
    {
        case TRANS_BUY:
            if (!printTransactionBuy(files, &messageSpec, TRUE, NO_REPRINT))
                return FALSE;
            break;
        case TRANS_BUYCHARGE:
            if (!printTransactionBuyCharge(files, &messageSpec, TRUE, NO_REPRINT))
                return FALSE;
            break;
        case TRANS_BILLPAY:
            if (!printTransactionBillPay(files ,&messageSpec, TRUE, NO_REPRINT))
                return FALSE;
            break;
        case TRANS_TOPUP: //MRF_TOPUP
            if (!printTransactionBuyChargeTopup(files, &messageSpec, TRUE, NO_REPRINT))
                return FALSE;
            break;
        case TRANS_CHARITY:
            if (!printTransactionCharity(files, &messageSpec, TRUE, NO_REPRINT))
                return FALSE;
            break;
        default:
            break;
    }
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
    fileRemove(files->reversalReceiptFile);
    
    return TRUE;
}

void addUnsuccessTransLog(messageSpecST* messageSpec)
{
    
    int16               retValue2                 = FAILURE;
    int16               maxRecord                 = 0;
    int8                logName[FILE_NAME_LENTGH] = {0};
    uint8               activeUserName[16]        = { 0 };//ABS
    reversalTransST     reversalInfo;
	

    memset(&reversalInfo, 0, sizeof(reversalTransST));
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "****addUnsuccessTransLog****");
    
    //mgh: change for mask saved in file strcpy(reversalInfo.PAN, messageSpec->cardSpec.PAN);
	maskCardId(messageSpec->cardSpec.PAN, reversalInfo.PAN); //ABS
    strcpy(reversalInfo.amount, messageSpec->amount);
//    strcpy(reversalInfo.loanPay.amount, messageSpec->loanPay.amount);
//    strcpy(reversalInfo.loanPay.destinationCardPAN, messageSpec->loanPay.destinationCardPAN);
    reversalInfo.STAN       = messageSpec->merchantSpec.recieveSTAN ;
    reversalInfo.billSpec   = messageSpec->billSpec;
    reversalInfo.chargeType = messageSpec->buyChargeSpec.type;
    reversalInfo.transType  = messageSpec->transType;
    reversalInfo.SWdateTime = messageSpec->SWdateTime;
    reversalInfo.dateTime   = messageSpec->dateTime;
    reversalInfo.charity	= messageSpec->charity;
    
    switch (reversalInfo.transType) 
    {
        case TRANS_BUY:
            strcpy(logName, REVERSAL_BUY_LOG_FILE);

			if (getPOSService().shiftService)
			{
				getActiveUser(activeUserName);
				strcat(logName, activeUserName);
			}
            maxRecord = MAX_UNSUCCESS_BUY_TRANS;
            break;
        case TRANS_BUYCHARGE:
            strcpy(logName, REVERSAL_CHARGE_LOG_FILE);
            maxRecord = MAX_UNSUCCESS_CHARGE_TRANS;
            break;
        case TRANS_BILLPAY:
            strcpy(logName, REVERSAL_BILLPAY_LOG_FILE);
            maxRecord = MAX_UNSUCCESS_BILLPAY_TRANS;
            break;
        case TRANS_LOANPAY:
            strcpy(logName, REVERSAL_LOAN_PAY_FILE);
            maxRecord = MAX_UNSUCCESS_LOANPAY_TRANS;
            break;
        case TRANS_ETC:
            strcpy(logName, REVERSAL_ETC_FILE);
            maxRecord = MAX_UNSUCCESS_LOANPAY_TRANS;
            break;
        case TRANS_CHARITY:
        	strcpy(logName,REVERSAL_CHARITY_LOG_FILE);
        	maxRecord = MAX_UNSUCCESS_CHARITY_TRANS;
            break;
        default:
            break;
    }

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**APPEND: reversalLogFile");
    retValue2 = appendFixedFileInfo(logName, &reversalInfo, sizeof (reversalTransST), maxRecord);
    if (retValue2 != SUCCESS) 
    {
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "err #8");
        addSystemErrorReport(retValue2, ERR_TYPE_FILE_SYSTEM);
    }
    //mgh: end of adding reversal Log
        
}


void structCpy(lastCustomerReceiptST* lastCustomerReceipt, messageSpecST* messageSpec)
{
    strcpy(messageSpec->cardSpec.PAN, lastCustomerReceipt->PAN);
    strcpy(messageSpec->amount, lastCustomerReceipt->amount);
    strcpy(messageSpec->approvalCode, lastCustomerReceipt->approvalCode);
    strcpy(messageSpec->cardSpec.customerName, lastCustomerReceipt->customerName);
    strcpy(messageSpec->retrievalReferenceNumber, lastCustomerReceipt->retrievalReferenceNumber);
    strcpy(messageSpec->preAmount, lastCustomerReceipt->preAmount);//MRF_NEW17
    strcpy(messageSpec->depositID, lastCustomerReceipt->depositID);//MRF_NEW17
    strcpy(messageSpec->tipAmount, lastCustomerReceipt->tipAmount);//MRF_NEW17
     
    messageSpec->dateTime = lastCustomerReceipt->dateTime;
    messageSpec->merchantSpec = lastCustomerReceipt->merchantSpec;
    messageSpec->transType = lastCustomerReceipt->transType;
    messageSpec->billSpec = lastCustomerReceipt->billSpec;
    messageSpec->buyChargeSpec = lastCustomerReceipt->buyChargeSpec;
    messageSpec->loanPay = lastCustomerReceipt->loanPay;
    messageSpec->topup = lastCustomerReceipt->topup; //MRF_TOPUP
    messageSpec->ETC = lastCustomerReceipt->ETC;
    messageSpec->charity = lastCustomerReceipt->charity;
}


uint8 readLastCustomerReceiptFile(uint8* fileName, lastCustomerReceiptST* lastCustomerReceipt)
{
	uint32      fileLength	= sizeof(lastCustomerReceiptST);    /** lenght of last buy transaction info file */
	int16       retValue	= FAILURE;							/** read file function return value */
	
	retValue = readFileInfo(fileName, lastCustomerReceipt, &fileLength);    
    if (retValue != SUCCESS)
    {
    	if (retValue != ERR_FILE_NOT_FOUND)
    		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    return TRUE;
}


/**
 * reprint last buy transaction receipt.
 * @see     printTransactionReprint()
*/
uint8 reprintBuyTransaction(argumentListST* args)
{    
    filesST*                files						= (filesST*) (args->argumentName);
    uint8                   fileName[FILE_NAME_LENTGH]	= {0};
    uint8					loginUserName[15 + 1] 		= { 0 }; //ABS:ADD
	uint8					selectUser[16]				= {0};
    messageSpecST           messageSpec;
    lastCustomerReceiptST	lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));
    
    strcpy(fileName, LAST_CUSTOMER_BUY_RECIEPT_FILE);

	if (getPOSService().shiftService == TRUE)//HNO_SHIFT
	{
//		getLoginUser(loginUserName);	//ABS:ADD
		if(!displayUsersList(selectUser, FALSE))//HNO_SHIFT10
			return FALSE;
		if(strcmp("����", selectUser) != 0)//HNO_SHIFT10
			strcat(fileName, selectUser);
	}

    if (!readLastCustomerReceiptFile(fileName, &lastCustomerReceipt))
    	return FALSE;
    
    structCpy(&lastCustomerReceipt, &messageSpec);    
    printTransactionBuy(files, &messageSpec, TRUE, REPRINT);

	return TRUE;
}


/**
 * reprint last bill transaction receipt.
 * @see     printTransactionReprint()
*/
uint8 reprintBillTransaction(argumentListST* args)
{    
    filesST*				files	= (filesST*) (args->argumentName);
    messageSpecST			messageSpec;
    lastCustomerReceiptST   lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_BILL_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;
    
    structCpy(&lastCustomerReceipt, &messageSpec);
    printTransactionBillPay(files, &messageSpec, TRUE, REPRINT);
    
	return TRUE;
}

//HNO_LOAN
uint8 reprintLoanPayTransaction(argumentListST* args)
{
    filesST*                    files	= (filesST*) (args->argumentName);
    messageSpecST               messageSpec;									/** last buy transaction information */
    lastCustomerReceiptST       lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_LOAN_PAY_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;

    structCpy(&lastCustomerReceipt, &messageSpec);
    printLoanPayTransaction(files, &messageSpec, TRUE, TRUE);

    return TRUE;
}


/**
 * print successful terminal configuration report.
 */
void successInitializeReport(filesST* files, messageSpecST* messageSpec)//HNO_INIT
{
//    dateTimeST  nowDateTime     = getDateTime();
    dateTimeST  nowDateTime     = messageSpec->dateTime;
    

    if (!PrinterAccess(TRUE))    
        return;
    
    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    //if (printHeader == PRINT_HEADER)
    if (!prePrintFlags/*[files->id]*/)
    {
    	printReceiptHeader(&nowDateTime, FALSE, files);
    }
    
    /** PRINT SUCCESSFUL CONFIG MESSAGE */
    if (!printOneStringFarsi("�������� ����", PRN_NORM, ALIGN_CENTER))
        return;

    prePrintFlags = 0;//[files->id] = 0;
    
    /** PRINT TERMINAL ID */
    if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.terminalID, "������", PRN_NORM))//ABS:delete shomare
        return;

    /** PRINT MERCHANT ID */
    if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.merchantID, "�������", PRN_NORM))//ABS:delete shomare
        return;
    
     /** PRINT DATE AND TIME */ 
    if (!printReceiptDateTime(&nowDateTime, TRUE)) //MRF_NEW3
        return;
    
    /** PRINT DASHES */
    printBlankLines(BLANK_LINES_COUNT);
}

void addTransactionErrorReport(uint8 transactionType, int errorCode, uint8 knownError)  
{
    int16        retValue                   = FAILURE;      /** write in file function return value */
    uint8        fileName[FILE_NAME_LENTGH]	= {0};  //MRF_NEW3
    transErrorST error;                                     /** transaction error detail */

// MGH 281210 : added
    memset(&error, 0, sizeof(transErrorST));
    
    error.dateTime = getDateTime();
    error.transType = transactionType;
    error.errorCode = errorCode;

    strcpy(fileName, TRANS_ERROR_LOG_FILE);
    
//    if (fileExist(fileName) != SUCCESS)
//        return FALSE;
    
    retValue = appendFixedFileInfo(fileName, &error, sizeof(transErrorST), MAX_TRANS_ERROR_LOG_RECORDS);  
    if (retValue != SUCCESS)
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
}


uint8 readBoxReportFile(uint8* fileName, void* transaction, uint16 structLen, uint16 maxRecord, 
						uint32* userDateJalali, int* transactionNum)
{
    uint32          currentDate             = 0;				/** current date */
    dateTimeST      currentDateTime         = {0, 0};           /** current date and time */
    //-- uint32     userDateJalali          = 0;				/** entered date by user (jalali) */
    uint32          userDateGerigorian      = 0;				/** entered date by user (gerigorian) */
    uint8           getDateRes              = 0;
    int             searchTransRes          = -2;               /** search for transaction with specified date in transactions file */
    int             startRecordNum          = 0;				/** start record number of transactions with specified date in file */
    int             endRecordNum            = 0;				/** end record number of transactions with specified date in file */
    //-- int             transactionNum          = 0;			/** charge number with specified date in file */
    int16           retValue                = FAILURE;
	
    if (fileExist(fileName) != SUCCESS) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    currentDate = systemDateTimeToDate();
    currentDateTime = getDateTime();
    gregorianToJalali(userDateJalali, currentDateTime.date);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fileName:");
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", fileName);
    while (TRUE) 
    {
        getDateRes = KBD_TIMEOUT;
        
        getDateRes = getDateFromUser("���� �����:", FALSE, userDateJalali);
        if (getDateRes == KBD_CANCEL || getDateRes == KBD_TIMEOUT) 
            return FALSE;
     
        jalaliToGregorian(&userDateGerigorian , *userDateJalali);

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "currentDate is : %d", currentDate);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "userDateGerigorian is : %d", userDateGerigorian);

        if (currentDate < userDateGerigorian) 
        {
            displayMessageBox("����� ���� ��� ����� ����", MSG_ERROR);
            continue;
        }
        else 
            break;
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "date: %d", userDateGerigorian);
    searchTransRes = searchDateRangeInFixedFile(fileName, structLen, 0, userDateGerigorian, 
		userDateGerigorian, maxRecord, &startRecordNum, &endRecordNum);    
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "searchTransRes is : %d", searchTransRes);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "startRecordNum is : %d", startRecordNum);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "endRecordNum is : %d", endRecordNum);

    if (searchTransRes == -2 || startRecordNum == -2 || endRecordNum == -2) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    if (startRecordNum == -1 || endRecordNum == -1) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    *transactionNum = endRecordNum - startRecordNum + 1;
    
    retValue = readFixedFileInfoArray(fileName,(uint8*) transaction, structLen, startRecordNum, 
                                        *transactionNum, maxRecord);    
	
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "retValue is : %d", retValue);

    if (retValue != SUCCESS) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    return TRUE;
}

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
uint8 merchantBoxReport(argumentListST* args)
{    
    filesST*        files                                       = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    buyTransLogST   transactions[MAX_BUY_TRANS_LOG_RECORDS] 	= {0};
    int				transactionNumber                           = 0;            /** charge number with specified date in file */
    int				counter                                     = 0;            /** loop counter */
    uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
    uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
    uint8			fileName[FILE_NAME_LENTGH]                  = {0};
    uint8			activeUser[15 + 1]                          = {0};
    uint8			title[30]                                   = {0};
    uint8			activeUserTitle[15 + 1]                     = {0};
    uint8           check                                       = FALSE;
    uint8           sumDailyValues[15 + 1]                      = {0};
    uint8			tempSumValues[15 + 1]                       = {0}; 
    uint8			valueWithComma[20]                          = {0}; 
    uint8           transNum[5]                                 = {0};
    uint8			selectUser[16]								= {0};//HNO_SHIFT
    int             sumCountValues                      		= 0;
    uint8           sumAllUsersValues[15 + 1]                   = {0};
    uint8			shift[22] 									= {0};
	uint8			loginUserName[15 + 1] = { 0 }; //ABS:ADD

    strcpy(fileName, BUY_LOG_FILE);
	strcpy(title, "����� ����� ����");

	if(getPOSService().shiftService == TRUE)//HNO_SHIFT
	{
		getLoginUser(loginUserName); //ABS:ADD
		if (strcmp("����", loginUserName) == 0)//HNO_IDENT9
		{
			if(!displayUsersList(selectUser, TRUE))//ABS
				return FALSE;

			if (strcmp("���� �������", selectUser) == 0)//HNO_SHIFT9
			{
				userNodeST*	currentNode				= NULL;
				uint8		user[16]				= {0};

				currentNode = getUserList();
				//getUserList(currentNode);
			    if (!getDateTimeDistanceFromUser(&startDateTime, &endDateTime, activeUserTitle, activeUser))
			        return FALSE;

			    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
			    	return FALSE;

				while(currentNode)
				{
					memset(&fileName, 0, sizeof(fileName));
					memset(&valueWithComma, 0, sizeof(valueWithComma));
					memset(&sumDailyValues, 0, sizeof(sumDailyValues));

					strcpy(fileName, BUY_LOG_FILE);//file name without catting is just for admin

					strcpy(user, currentNode->data.userName);//HNO_SHIFT_NEW
					if(strcmp("����", currentNode->data.userName) != 0)//HNO_SHIFT10
						strcat(fileName, currentNode->data.userName);

					currentNode = currentNode->next;
					showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fileName allusers: %s", fileName);

					if(!readAllUsersTransactionListFile(fileName, transactions, sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS,
																	&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
					{
						transactionNumber = 0;
						requestedNum = 0;
					}

					for (counter = requestedNum - 1; counter >= 0; counter--)
						sumStringNumbers(sumDailyValues, transactions[counter].amount, sumDailyValues);

					sumCountValues = transactionNumber + sumCountValues;//sum count of all users transactions

				    strcpy(shift, "���� ");
				    strcat(shift, user);
				    printTwoStringFarsi(shift, "����� ����:", PRN_NORM);//HNO_SHIFT

				    sprintf(transNum, "%d", transactionNumber);
				    if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
				        return FALSE;

				    makeValueWithCommaStr(sumDailyValues, valueWithComma);
				    if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
				        return FALSE;

				    printDash();

				    sumStringNumbers(sumAllUsersValues, sumDailyValues, sumAllUsersValues);
				}

			    makeValueWithCommaStr(sumAllUsersValues, valueWithComma);

			    /** PRINT TRANSACTIONS NUMBER IN DATES */
			    sprintf(transNum, "%d", sumCountValues);
			    if (!printTwoStringNumericFarsi(transNum, "����� ��", PRN_NORM))
			        return FALSE;

			    if (!printThreeStringFarsi(" ����", valueWithComma , "��� ��", PRN_NORM, ALIGN_RIGHT, TRUE))
			        return FALSE;

			    printStar();
			    printBlankLines(BLANK_LINES_COUNT);//HNO_IDENT

			    return TRUE;
			}
			else
				if(strcmp("����", selectUser) != 0)
					strcat(fileName, selectUser);
		}
		else
		{
			strcat(fileName, loginUserName);
			strcpy(selectUser, loginUserName);
		}
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fileName: %s", fileName);
    if (!readTransactionListFile(fileName, transactions, sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS,
				    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    for (counter = requestedNum - 1; counter >= 0; counter--)
        sumStringNumbers(sumDailyValues, transactions[counter].amount, sumDailyValues);
    
    makeValueWithCommaStr(sumDailyValues, valueWithComma);
    
    /** PRINT TRANSACTIONS NUMBER IN DATES */
    if(getPOSService().shiftService == TRUE)//HNO_SHIFT3
    {
		strcpy(shift, "���� ");
		strcat(shift, selectUser);
		printTwoStringFarsi(shift, "����� ����:", PRN_NORM);//HNO_SHIFT
    }

    sprintf(transNum, "%d", transactionNumber);
    if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
        return FALSE;

    if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
        return FALSE;
    
    printStar();
    printBlankLines(BLANK_LINES_COUNT);//HNO_IDENT
    
    return TRUE;
}
//#else
///*
// * display & print merchant box report.
// * @see     getDateFromUser()
// * @see     searchDateRangeInFixedFile()
// * @see     readFixedFileInfoArray()
// * @see     displayScrollable()
// */
//uint8 merchantBoxReport(argumentListST* args)
//{
//    filesST*		files                   				= (filesST*) (args->argumentName);
//    uint32          userDateJalali          				= 0;				/** entered date by user (jalali) */
//    int             reportYear              				= 0;				/** report year */
//    int             reportMonth             				= 0;				/** report month */
//    int             reportDay               				= 0;				/** report day */
//    int             transactionNumber       				= 0;				/** transactions number with specified date in file */
//    uint8           transValueSum[15 + 1]   				= {0};				/** sum of transactions value */
//    uint8           tempSumValues[15 + 1]   				= {0};
//    int             counter                 				= 0;				/** loop counter */
//    uint8           lines[15][32]           				= {0};				/** display lines */
//    uint8           valueWithComma[20]      				= {0};				/** sum value with comma, string form */
//    buyTransLogST   transactions[MAX_BUY_TRANS_LOG_RECORDS] = {0};
//    uint8           dateValueStr[11]        				= {0};
//    uint8           transNum[10]            				= {0};
//    uint8           numStr[7]               				= {0};
//    uint16          i                                       = 0;
//    uint8           key                                     = KBD_CANCEL;
//    uint8           separatedStr[10][50]    				= {0, 0};
//    uint8           wordWrapIndex           				= 0;
//    uint8           loop                    				= 0;
//    uint8           amountPrintBuffer[20]   				= {0};
//    uint8           fileName[FILE_NAME_LENTGH]				= {0};
//    uint8			transType								= TRANS_BUY;
//    uint8			activeUser[15 + 1]						= {0};
//    uint8			activeUserTitle[15 + 1]					= {0};
//    merchantSpecST	merchantSpec;
//    dateTimeST		dateTime;
//
//    memset(&merchantSpec, 0, sizeof(merchantSpecST));
//    memset(&dateTime, 0, sizeof(dateTimeST));
//    memset(lines, 0, sizeof(lines));
//
//    //deleted clearDisplay();
//    strcpy(fileName, BUY_LOG_FILE);
//    transType = TRANS_BUY;
//    strcpy(activeUserTitle, "����� ����");
//
//    if (!readBoxReportFile(fileName, transactions, sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS,
//    		&userDateJalali, &transactionNumber))
//    	return FALSE;
//
//    reportYear = getYear(userDateJalali);
//    reportMonth = getMonth(userDateJalali);
//    reportDay = getDay(userDateJalali);
//
//    //mgh: adding 2string...
//    for (counter = 0; counter < transactionNumber; counter++)
//    {
//        strcpy(tempSumValues, transValueSum);
//        sumStringNumbers(tempSumValues, transactions[counter].amount, transValueSum);
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "transValueSum= %s", transValueSum);
//    }
//
//    //deleted clearDisplay();
//
//    /** LINE 0 : HEADER */
//    justifyOneStringFarsi("����� ����� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
//
//    /** ACTIVE USER */
//    if (strlen(activeUser) > 0)
//    {
//	    justifyTwoStringFarsi("", activeUserTitle, lines[i++], PRN_NORM);
//	    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, activeUser);
//    }
//
//    /** LINE 1 : DATE */
//    justifyTwoStringFarsi("", "�����", lines[i++], PRN_NORM);
//
//    sprintf(dateValueStr, "%04d/%02d/%02d", reportYear, reportMonth, reportDay);
//    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, dateValueStr);
//
//    /** LINE 2 : TRANSACTION NUMBER */
//    sprintf(transNum, "%d", transactionNumber);
//    justifyTwoStringFarsi(transNum, "�����", lines[i++], PRN_NORM);
//
//    /** LINE 3 , 4 , 5 : TRANSACTIONS VALUE */
//    justifyTwoStringFarsi(" ", "�����", lines[i++], PRN_NORM);
//
//
//    makeValueWithCommaStr(transValueSum, valueWithComma);
//    if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT - strlen(" ����"))
//    {
//        if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT)
//        {
//            loop = wordWrapEnglish(valueWithComma, separatedStr, DISPLAY_CHARACTER_COUNT);
//            for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
//            {
//                sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, separatedStr[wordWrapIndex]);
//            }
//        }
//        else
//        {
//            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma);
//        }
//        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, "����");
//    }
//    else
//    {
//        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");
//    }
//
//    //mgh_93
////    key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
////    if (key != KBD_ENTER && key != KBD_CONFIRM)
////        return FALSE;
//
//    if (!PrinterAccess(TRUE))
//        return FALSE;
//
//    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);
//
//    /** PRINT HEADER */
//    dateTime = getDateTime();
//    if (!printReceiptHeader(&dateTime, TRUE, files))
//        return FALSE;
//
//    /** PRINT HEADER */
//    if (!printOneStringFarsi("����� ����� ����", PRN_NORM, ALIGN_CENTER))
//        return FALSE;
//
//    /** PRINT DATE */
////    sprintf(dateValueStr, "%04d/%02d/%02d", reportYear, reportMonth, reportDay); //--MRF_TTF
////    if (!printTwoStringNumericFarsi(dateValueStr, "�����", PRN_NORM))
////        return FALSE;
//    printReceiptDate(dateTime.date,"�����");
//
//    /** PRINT ACTIVE USER */
//    strcat(activeUserTitle, ":");
//    if (strlen(activeUser) > 0)
//	    if (!printTwoString(activeUser, activeUserTitle, PRN_NORM, FALSE))
//	        return FALSE;
//
//    /** PRINT TERMINAL ID */
//    if (readMerchantSpec(files, &merchantSpec))
//    {
//        if (!printTwoStringNumericFarsi(merchantSpec.terminalID, "����� ������", PRN_NORM))
//            return FALSE;
//    }
//
//    /** PRINT DASHES */
//    if (!printStar())
//        return FALSE;
//
//    /** PRINT TRANSACTION NUMBER */
//    sprintf(numStr, "%d", transactionNumber);
//    if (!printTwoStringNumericFarsi(numStr, "�����", PRN_NORM))
//            return FALSE;
//
//    /** PRINT TRANSACTIONS VALUE */
//    if (strlen(valueWithComma) + strlen(" ����") + strlen("�����:") <= getPrinterCharacterCount())
//    {
//#if defined(ICT250) || defined(IWL220)
//		sprintf(amountPrintBuffer, "%s ����", valueWithComma);
//		if (!printTwoStringFarsi(amountPrintBuffer, "�����", PRN_NORM))
//			return FALSE;
//#else
//         if (!printThreeStringFarsi(" ����", valueWithComma, "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
//            return FALSE;
//#endif
//    }
//    else if (strlen(valueWithComma) + strlen(" ����") <= getPrinterCharacterCount())
//    {
//        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
//            return FALSE;
//
//        sprintf(amountPrintBuffer, "%s ����", valueWithComma);
//        if (!printOneStringFarsi(amountPrintBuffer, PRN_NORM, ALIGN_CENTER))
//            return FALSE;
//    }
//    else
//    {
//        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
//            return FALSE;
//        if (!printOneStringFarsi(valueWithComma, PRN_NORM, ALIGN_CENTER))
//            return FALSE;
//        if (!printOneStringFarsi("����", PRN_NORM, ALIGN_LEFT))
//            return FALSE;
//    }
//
//    /** PRINT EMPTY LINES */
//    printBlankLines(BLANK_LINES_COUNT);
//    //if (!printBlankLines(BLANK_LINES_COUNT))
//    //    return FALSE;
//    return TRUE;
//}
//#endif


uint8 readTransactionListFile(uint8* fileName, void* transaction, uint16 structLen, uint16 maxRecord, 
							  	dateTimeST* startDateTime, dateTimeST* endDateTime, int* transactionNum,
								uint32* requestedNum,uint8* activeUserTitle, uint8* activeUser)
{
    int             searchTransRes          = -2;			/** search for transaction with specified date in transactions file */
    int             startRecordNum          = 0;			/** start record number of transactions with specified date in file */
    int             endRecordNum            = 0;			/** end record number of transactions with specified date in file */
    int				beginIndexInFile		= 0;            /** index of beginning requested transaction in transactions file */
    int16			retValue 				= FAILURE;
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**readTransactionListFile**");
    
    if (fileExist(fileName) != SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "File transaction unsuccess not exist!");
    	displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
    	return FALSE;
    }
    
    if (!(getDateTimeDistanceFromUser(startDateTime, endDateTime, activeUserTitle, activeUser)))
        return FALSE;

    searchTransRes = searchDateTimeRangeInFixedFile(fileName, structLen, 0,
    												startDateTime, endDateTime, maxRecord, 
                                                    &startRecordNum, &endRecordNum);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "start: %d, end: %d", startRecordNum, endRecordNum);
    if (searchTransRes == -2 || startRecordNum == -2 || endRecordNum == -2) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in Start time trans not exist !!");
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    if (startRecordNum == -1 || endRecordNum == -1)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in End time trans not exist !!");
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }

    *transactionNum = endRecordNum - startRecordNum + 1;

    *requestedNum = *transactionNum;
    
    editNumericValue(requestedNum, "����� ��ǘ��:", 0, maxRecord, FALSE, FALSE);

    if (*requestedNum > maxRecord)
    	if (*transactionNum <= maxRecord)
    		*requestedNum = *transactionNum;
    	else
    		*requestedNum = maxRecord;
    
    if (*requestedNum == 0 || *requestedNum > *transactionNum) 
        *requestedNum = *transactionNum;
    
    //show the last transactions
    beginIndexInFile = *transactionNum - *requestedNum + startRecordNum;
    
    retValue = readFixedFileInfoArray(fileName,(uint8*) transaction, structLen, beginIndexInFile, 
                                        *requestedNum, maxRecord);  
    if (retValue != SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in readFixedFileInfoArray trans not exist !!");
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
   
    return TRUE;
}


void fillTransListHeaderLines(uint8 lines[][32], uint16* index, uint8* title, uint8* activeUserTitle, uint8* activeUser,
		dateTimeST* startDateTime, dateTimeST* endDateTime)
{
	uint32			jalaliDate              	= 0;            /** jalali date */
    int				yearFrom                	= 0;            /** start year */
    int				monthFrom               	= 0;            /** start month */
    int				dayFrom                 	= 0;            /** start day */
    int				yearTo                  	= 0;            /** end year */
    int				monthTo                 	= 0;            /** end month */
    int				dayTo                   	= 0;            /** end day */
    int				hourFrom                	= 0;            /** start hour */
    int				minuteFrom               	= 0;            /** start minute */
    int				hourTo                  	= 0;            /** end hour */
    int				minuteTo                 	= 0;            /** end minute */
    uint16			i							= *index;
    uint8			startDateValueStr[11]   	= {0};
    uint8			endDateValueStr[11]     	= {0};
    uint8			startTimeValueStr[11]   	= {0};
    uint8			endTimeValueStr[11]     	= {0};
    uint8           spaceCountStr[50]           = {0};
    terminalSpecST	terminalCapability			= getTerminalCapability();
       
    gregorianToJalali(&jalaliDate, startDateTime->date);
    yearFrom 	= getYear(jalaliDate);
    monthFrom 	= getMonth(jalaliDate);
    dayFrom 	= getDay(jalaliDate);
    hourFrom    = getHour(startDateTime->time);
    minuteFrom 	= getMinute(startDateTime->time);
       
    gregorianToJalali(&jalaliDate , endDateTime->date);
    yearTo 		= getYear(jalaliDate);
    monthTo		= getMonth(jalaliDate);
    dayTo 		= getDay(jalaliDate);
    hourTo      = getHour(endDateTime->time);
    minuteTo 	= getMinute(endDateTime->time);
    
	/** LINE 0 : TITLE */
    if (!terminalCapability.graphicCapability)	//MRF_IDENT
    	justifyOneStringFarsi(title, lines[i++], PRN_NORM, ALIGN_CENTER);
    
    /** LINE 1 : START DATE */
    memset(spaceCountStr,' ', SPACE_COUNT);
    sprintf(startDateValueStr, "%04d/%02d/%02d", yearFrom, monthFrom, dayFrom);
    sprintf(startTimeValueStr, "%02d:%02d", hourFrom, minuteFrom);

#if defined(ICT250) || defined(IWL220)//HNO_ADD
    justifyOneStringFarsi("���� ����:", lines[i++], PRN_NORM, ALIGN_RIGHT);
	sprintf(lines[i++], "%c%c%c%s       %s", PRN_NORM, ALIGN_CENTER, FARSI,
			startDateValueStr, startTimeValueStr);
#else
    justifyTwoStringFarsi("", "���� ����", lines[i++], PRN_NORM);
    sprintf(lines[i++], "%c%c%c%s%s%s", PRN_NORM, ALIGN_CENTER, FARSI, startDateValueStr,spaceCountStr, startTimeValueStr);
#endif
    
    /** LINE 2 : END DATE */
    sprintf(endDateValueStr, "%04d/%02d/%02d", yearTo, monthTo, dayTo);
    sprintf(endTimeValueStr, "%02d:%02d", hourTo, minuteTo);
#if defined(ICT250) || defined(IWL220)//HNO_ADD
    justifyOneStringFarsi("���� �����:", lines[i++], PRN_NORM, ALIGN_RIGHT);
	sprintf(lines[i++], "%c%c%c%s       %s", PRN_NORM, ALIGN_CENTER, FARSI,endDateValueStr, endTimeValueStr);
#else
    justifyTwoStringFarsi("", "���� �����", lines[i++], PRN_NORM);
    sprintf(lines[i++], "%c%c%c%s%s%s", PRN_NORM, ALIGN_CENTER, FARSI, endDateValueStr,spaceCountStr, endTimeValueStr);
#endif
    /** ACTIVE USER */
// mgh_93	if (getActiveUser(activeUser))
//    {
//	    justifyTwoStringFarsi("", activeUserTitle, lines[i++], PRN_NORM);
//	    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, activeUser);
//    }
    
    /** LINE 3 : DASHES */
    justifyOneStringFarsi(DISPLAY_DASH, lines[i++], PRN_NORM, ALIGN_CENTER);
    *index = i;
}


void fillTransListCommonPartLines(uint8 lines[][32], uint16* index, uint8* amount, uint8* pan,
										uint8* referenceNum, uint32 stan, dateTimeST* dateTime, uint8* depositID)
{
	uint16			i							= *index;
	uint8			valueWithComma[20]			= {0};          /** value with comma */
	uint8           separatedStr[10][250]    	= {0, 0}; //ABS:CHANGE:960801
	uint8			maskedPAN[20]				= {0};
	uint8			transDateStr[11]			= {0};
	uint8			timeValueStr[6]         	= {0};
	uint8           loop                    	= 0; 
	uint8           wordWrapIndex           	= 0; 
	uint32			jalaliDate              	= 0;            /** jalali date */
	int				transYear               	= 0;
    int				transMonth              	= 0;
    int				transDay                	= 0;
    int				transHour               	= 0;
    int				transMinute             	= 0;
    uint8           spaceCountStr[50]           = {0};
    uint8           panOut[20]                  = {0};
    uint8           BIN[7]                      = {0};
	
    /** TRANSACTIONS LOG ; LINE 1 & 2: TRANSACTION VALUE */
    justifyTwoStringFarsi("", "����", lines[i++], PRN_NORM);

    makeValueWithCommaStr(amount, valueWithComma); 
    
    //sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");

    if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT - strlen(" ����"))
    {
        if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT)
        {
            loop = wordWrapEnglish(valueWithComma, separatedStr, DISPLAY_CHARACTER_COUNT);
            for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
            {
                sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, separatedStr[wordWrapIndex]);
            }
        }
        else
        {
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma);
        }
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, "����");
    }
    else
    {
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");
    }
    
    strncpy(BIN, pan, 6);
    if (strcmp(BIN,SUPERVISOR_BIN) != 0)
    {
        /** TRANSACTIONS LOG ; LINE 3 & 4: TRANSACTION CARD NUMBER */
        justifyTwoStringFarsi("", "����� ����", lines[i++], PRN_NORM);
        //mgh: ?! maskCardId(pan, panOut); //ABS
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, pan);
    }
    
    /** TRANSACTIONS LOG ; LINE 5 & 6: TRANSACTION REFERENCE NUMBER */
    if (referenceNum)
        if (strlen(referenceNum) > 0)
        {
            justifyTwoStringFarsi("", "����� ����", lines[i++], PRN_NORM);
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, referenceNum);
        }
    
    if (getPOSService().depositID)//MRF_NEW17
        if (strlen(depositID) > 0)
        {
            justifyTwoStringFarsi("", "����� �����", lines[i++], PRN_NORM);
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, depositID);
        }
    
    if (stan > 0)
    {
	    justifyTwoStringFarsi("", "����� �����", lines[i++], PRN_NORM);
	    sprintf(lines[i++], "%c%c%c%ld", PRN_NORM, ALIGN_LEFT, FARSI, stan);
    }

    /** TRANSACTIONS LOG ; LINE 7 : TRANSACTION DATE AND TIME */
    gregorianToJalali(&jalaliDate, dateTime->date);
    transYear = getYear(jalaliDate);
    transMonth = getMonth(jalaliDate);
    transDay = getDay(jalaliDate);
		
    justifyTwoStringFarsi("", "���� ��ǘ��", lines[i++], PRN_NORM);
    
    memset(spaceCountStr,' ', SPACE_COUNT);
    sprintf(transDateStr, "%04d/%02d/%02d", transYear, transMonth, transDay);
    transHour = getHour(dateTime->time);
    transMinute = getMinute(dateTime->time);
    sprintf(timeValueStr, "%02d:%02d", transHour, transMinute);
    sprintf(lines[i++], "%c%c%c%s%s%s", PRN_NORM, ALIGN_CENTER, FARSI, transDateStr,spaceCountStr, timeValueStr);
    
    /** TRANSACTIONS LOG ; LINE 8: DASHES */
    justifyOneStringFarsi(DISPLAY_DASH, lines[i++], PRN_NORM, ALIGN_CENTER);
    //-- sumValues += transactions[counter].amount;
    *index = i;
}


void fillTransListFooterLines(uint8 lines[][32], uint16* index, int transactionNumber, 
									uint8 displayCount, uint8* sumValue) 
{
	uint8			transNum[10]				= {0};          /** transactions number in dates, exist or listed */////ABS:CHANGE:960801
	uint8           separatedStr[10][250]    	= {0, 0}; //ABS:CHANGE:960801
	uint8           loop                    	= 0; 
	uint8           wordWrapIndex           	= 0; 
	uint16			i							= *index;
	uint8			valueWithComma[20]          = {0};          /** value with comma */
	
    makeValueWithCommaStr(sumValue, valueWithComma);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", valueWithComma);
    
	/** LINE (TRANSACTTIONS LOG + 4) : TRANSACTIONS NUMBER IN DATES */
    sprintf(transNum, "%d", transactionNumber);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", transNum);
    justifyTwoStringFarsi(transNum, "�����", lines[i++], PRN_NORM);
    
    /** LINE (TRANSACTTIONS LOG + 5) : REQUESTED TRANSACTIONS */
    sprintf(transNum, "%ld", displayCount);
    justifyTwoStringFarsi(transNum, "���� ���", lines[i++], PRN_NORM);
    
    //HNO: TODO: check it why is not work!!!
    //NOTICE: it's not for project.
//    displayTransactionsValue(lines,&i,valueWithComma);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "i2 = %d", i);
    
   
    /** LINE (TRANSACTIONS LOG + (6 & 7 & 8)) : SUM OF TRANSACTIONS VALUE */
    justifyTwoStringFarsi("", "�����", lines[i++], PRN_NORM);

    if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT - strlen(" ����"))
    {
        if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT)
        {
            loop = wordWrapEnglish(valueWithComma, separatedStr, DISPLAY_CHARACTER_COUNT);
            for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
            {
                sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, separatedStr[wordWrapIndex]);
            }
        }
        else
        {
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma);
        }
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, "����");
    }
    else
    {
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");
    }
    *index = i;
}


uint8 printTransactionListHeader(filesST* files, uint8* title, uint8* activeUserTitle, uint8* activeUser, 
		dateTimeST* startDateTime, dateTimeST* endDateTime)
{
    dateTimeST		dateTime;
    merchantSpecST  merchantSpec;
	
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    memset(&dateTime, 0, sizeof(dateTimeST));
    
    if (!PrinterAccess(TRUE))    
           return FALSE;
    
   displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);
   
    /** PRINT HEADER */
    dateTime = getDateTime();
    if (!printReceiptHeader(&dateTime, TRUE, files)) 
        return FALSE;

    if (cancelFunction())
        return FALSE;

    /** PRINT TITLE */
    if (!printOneStringFarsi(title, PRN_NORM, ALIGN_CENTER))
        return FALSE;
 
    /** PRINT DATE AND TIME */
    if (!printReceiptDateTime(startDateTime, FALSE)) //MRF_NEW3
        return FALSE;
    
    if (cancelFunction())
        return FALSE;
    
    /** PRINT DATE AND TIME */
    if (!printReceiptDateTime(endDateTime, FALSE)) //MRF_NEW3
        return FALSE;
    
    if (cancelFunction())
        return FALSE;
    
    /** PRINT ACTIVE USER */ 
    /*
    strcat(activeUserTitle, ":"); 
    if (strlen(activeUser) > 0)
	    if (!printTwoString(activeUser, activeUserTitle, PRN_NORM))
	        return FALSE;
   
    if (cancelFunction())
    	return FALSE; 
     */
    
    /** PRINT TERMINAL ID */
    if (readMerchantSpec(files, &merchantSpec))
    {
        if (!printTwoStringNumericFarsi(merchantSpec.terminalID, "����� ������", PRN_NORM))
            return FALSE;
    }
    
    /** PRINT DASHES */
    if (!printStar())
        return FALSE;

    if (cancelFunction())
        return FALSE;
    
    return TRUE;
}

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
uint8 printTransactionListCommonPart(uint8* amount, uint8* pan, uint8* referenceNum, uint32 stan, dateTimeST* dateTime, uint8* depositID)
{
    uint8	valueWithComma[40]		= {0};			/** value with comma, string form */
    uint8   timeStr[100]             = {0};
    uint8	stanStr[8 + 1]			= {0};
    
     
    makeValueWithCommaStr(amount, valueWithComma);
    /** PRINT REFERENCE NUMBER */
    if (referenceNum)
    {
        removePad(referenceNum, ' ');
        if (strlen(referenceNum) > 0)
        {
#ifdef VX520  //ABS:ADD
			if (!printTwoStringNumericFarsi(referenceNum, "����� ����", PRN_NORM))
				return FALSE;
#else
			strcat(referenceNum, "|");
			if (!printTwoStringNumericFarsi(referenceNum, "|����� ����", PRN_NORM))
				return FALSE;
#endif
        }
    }
    
     if (getPOSService().depositID)//MRF_NEW17
        if (strlen(depositID) > 0)
        {
#ifdef VX520  //ABS:ADD
			if (!printTwoStringNumericFarsi(depositID, "����� �����", PRN_NORM))
				return FALSE;
#else
            strcat(depositID, "|");
			if (!printTwoStringNumericFarsi(depositID, "|����� �����", PRN_NORM))
				return FALSE;
#endif
        }
    
    if (stan > 0)
    {
#ifdef VX520
		sprintf(stanStr, "%ld", stan);
		if (!printTwoStringNumericFarsi(stanStr, "����� �����", PRN_NORM))
			return FALSE;
#else
		sprintf(stanStr, "%ld|", stan);
		if (!printTwoStringNumericFarsi(stanStr, "|����� �����", PRN_NORM))
			return FALSE;
#endif
    }
    
    /** PRINT AMOUNT & DATE */
    setTimeForPrint(dateTime, timeStr);
#ifdef VX520
	strcat(timeStr, " ����");
	printTwoStringFarsi(timeStr,valueWithComma, PRN_NORM);
#else
	strcat(valueWithComma, "����|");
	printTwoStringFarsi(valueWithComma, timeStr, PRN_NORM);
#endif
    
   
    return TRUE;
}
//#else
//uint8 printTransactionListCommonPart(uint8* amount, uint8* pan, uint8* referenceNum, uint32 stan, dateTimeST* dateTime)
//{
//    uint8	valueWithComma[30]		= {0};			/** value with comma, string form */
//    uint8	cardID[17]          	= {0};                  /** card ID */
//    uint8   amountPrintBuffer[20]   = {0};   //110828   for amount print
//    uint8	stanStr[6 + 1]			= {0};
//    uint8	panOut[20]              = {0};
//    uint8   BIN[7]                  = {0};
//
//    makeValueWithCommaStr(amount, valueWithComma);
//
//    strncpy(BIN, pan, 6);
//    if (strcmp(BIN,SUPERVISOR_BIN) != 0)
//    {
//        /** PRINT CARD NUMBER */
//        maskCardId(pan, panOut);
//#ifdef CASTLES_V5S //MRF_TTF
//        reversPartString(panOut);
//#endif
//        if (!printTwoStringNumericFarsi(panOut, "����� ����", PRN_NORM))
//            return FALSE;
//    }
//
//    /** PRINT REFERENCE NUMBER */
//    if (referenceNum)
//    {
//        removePad(referenceNum, ' ');
//        if (strlen(referenceNum) > 0)
//            if (!printTwoStringNumericFarsi(referenceNum, "����� ����", PRN_NORM))
//                return FALSE;
//    }
//
//    if (stan > 0)
//    {
//    	sprintf(stanStr, "%ld", stan);
//    	 if (!printTwoStringNumericFarsi(stanStr, "����� �����", PRN_NORM))
//	        return FALSE;
//    }
//
//    /** PRINT DATE AND TIME */
////    if (!printTwoStringNumericFarsi("", "���� ��ǘ��", PRN_NORM))
////        return FALSE;
//
//    if (!printReceiptDateTime(dateTime, TRUE))
//        return FALSE;
//
////   //MRF_413  if (!printOneStringFarsi("����:", PRN_NORM, ALIGN_RIGHT))
////            return FALSE;
//
//    /** PRINT AMOUNT */
//    sprintf(amountPrintBuffer, "%s ����", valueWithComma);
////   //MRF_413  if (!printOneStringFarsi(amountPrintBuffer, PRN_NORM, ALIGN_CENTER))
////        return FALSE;
//
//    //MRF_413
//    //HNO_CHANGE
//#if defined(ICT250) || defined(IWL220)
//    if(!printTwoStringNumericFarsi(amountPrintBuffer, "����", PRN_NORM))
//    		return FALSE;
//#else
//    printThreeStringFarsi("����", valueWithComma, "����", PRN_NORM, ALIGN_RIGHT, TRUE);
//#endif
//
//    return TRUE;
//}
//#endif


uint8 printTransactionListFooter(int transactionNumber, uint32 requestedNum, uint8* sumValues)
{
	uint8			transNum[10]				= {0};          /** transactions number in dates, exist or listed */
	uint8           amountPrintBuffer[20]   	= {0}; 
	uint8			valueWithComma[20]			= {0};          /** value with comma */
	
	makeValueWithCommaStr(sumValues, valueWithComma);
    
	/** PRINT TRANSACTIONS NUMBER IN DATES */
    sprintf(transNum, "%d", transactionNumber);
    if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
        return FALSE;
    
    if (cancelFunction())
        return FALSE;
    
    /** PRINT REQUESTED TRANSACTIONS NUMBER */
    sprintf(transNum, "%ld", requestedNum);
    if (!printTwoStringNumericFarsi(transNum, "���� ���", PRN_NORM))
        return FALSE;

    if (cancelFunction())
        return FALSE;
    
    /** PRINT TRANSACTIONS VALUE */
//     if (!printThreeStringFarsi(" ����", valueWithComma , "��� ��", PRN_NORM, ALIGN_RIGHT, TRUE))
//         return FALSE;
    
    if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT)) //MRF_OPTIMIZE :THIS AND UNDER PART SHOUD BE COMMENT
        return FALSE;
    
    sprintf(amountPrintBuffer, "%s ����", valueWithComma); 
    if (!printOneStringFarsi(amountPrintBuffer, PRN_BIG, ALIGN_CENTER))
        return FALSE;

    /** PRINT EMPTY LINES */
   printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}


uint8 displayTransactionBuyList(buyTransLogST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};		//HNO:change 700 to 3500 & 3 to 4 line for footer /** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();

	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Request: %d", requestedNum);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "transactions: %d", transactionNumber);
    
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0 ; counter--) 
    {
    	//mgh: adding two string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if ( i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 8)) //6: header, 4: footer, 8: body 
    	{
	    	fillTransListCommonPartLines(lines, &i, transactions[counter].amount, transactions[counter].PAN,
	    			transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime) , transactions[counter].depositID); //MRF_NEW17
	    	displayedCount++;
	    	strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sum %s", sumValues);
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE,buyTransPic);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}

//#ifdef CASTLES_V5S //IDENT2
//MRF_OPTIMIZE
//HNO_IDENT uncomment
uint8 transactionBuyList(argumentListST* args)
{    
    filesST*        files                                       = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    buyTransLogST   transactions[MAX_BUY_TRANS_LOG_RECORDS] 	= {0};
    int				transactionNumber                           = 0;            /** charge number with specified date in file */
    int				counter                                     = 0;            /** loop counter */
    uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
    uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
    uint8			fileName[FILE_NAME_LENTGH]                  = {0};
    uint8			activeUser[15 + 1]                          = {0};
    uint8			title[30]                                   = {0};
    uint8			activeUserTitle[15 + 1]                     = {0};
    uint8           check                                       = FALSE;
    uint8           sumDailyValues[15 + 1]                      = {0};
    uint8			tempSumValues[15 + 1]                       = {0}; 
    uint8			valueWithComma[20]                          = {0}; 
    uint8   		amountPrintBuffer[20]   					= {0};
    uint8			selectUser[16]								= {0};
    uint8           shift[22]           						= {0};
	uint8           loginUserName[15 + 1] = { 0 }; //ABS:ADD

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*********transactionBuyList********");
    strcpy(fileName, BUY_LOG_FILE);

	if(getPOSService().shiftService == TRUE)//HNO_SHIFT
	{
		getLoginUser(loginUserName);	//ABS:ADD
		if (strcmp("����", loginUserName) == 0)//HNO_IDENT9 //admin can see all users report
		{
			if (!displayUsersList(selectUser, FALSE))//ABS
			return FALSE;
			if(strcmp("����", selectUser) != 0)//HNO_SHIFT10
				strcat(fileName, selectUser);
		}
		else
		{
			strcat(fileName, loginUserName);//ABS:ADD
			strcpy(selectUser, loginUserName);//ABS:ADD
		}
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "fileName: %s", fileName);
    
    strcpy(activeUserTitle, "����� ����");
    strcpy(title, "�����ǘ�� ���� ����");

    if (!readTransactionListFile(fileName, transactions, sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS, 
    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    
    if (!displayTransactionBuyList(transactions, transactionNumber, requestedNum, "���� ����", &startDateTime,
    		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
    	return FALSE;
    
    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    /** PRINT TRANSACTIONS DETAILS */
    if (! printReceiptDate(transactions[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;
    
   if(getPOSService().shiftService == TRUE)//HNO_SHIFT3
   {
	   strcpy(shift, "���� ");
	   strcat(shift, selectUser);
	   printTwoStringFarsi(shift, "����� ����:", PRN_NORM);//HNO_SHIFT
   }
    
    printDash3();
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {   
        if (check != 0)
        {
           if (!printReceiptDate(transactions[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }
        
        if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN, 
                    transactions[counter].retrievalReferenceNumber, 0, 
                &(transactions[counter].dateTime), transactions[counter].depositID))//MRF_NEW17
            return FALSE;

        check = compareDates(&transactions[counter].dateTime, &transactions[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumDailyValues);
        
        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();
        
        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);
            
            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;
            
            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }
        
        if (cancelFunction())
            return FALSE;
    }
    
    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;
    
    return TRUE;
}
//#else
///**
// * display & print buy transaction list report.
// * @see     searchDateRangeInFixedFile()
// * @see     readFixedFileInfoArray()
// * @see     displayScrollable()
// * @see     printLogo()
// * @see     printReceiptDateTime()
// * @see     show_one_log()
// */
//uint8 transactionBuyList(argumentListST* args)
//{
//    filesST*        files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST		startDateTime				= {0, 0};
//    dateTimeST		endDateTime					= {0, 0};
//    int				transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32			requestedNum				= 0;            /** user requested transaction number, integer form */
//    int				counter                 	= 0;            /** loop counter */
//    uint8			sumValues[15 + 1]       	= {0};       /** sum of listed transactions value */
//    buyTransLogST   transactions[MAX_BUY_TRANS_LOG_RECORDS]       	= {0};	//HNO: change 200 to 1000
//    uint8			fileName[FILE_NAME_LENTGH]	= {0};
//    uint8			transType					= TRANS_BUY;
//    //mgh messageSpecST   messageSpec;
//    uint8			activeUser[15 + 1]			= {0};
//    uint8			title[15 + 1]				= {0};
//    uint8			activeUserTitle[15 + 1]		= {0};
//
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
//
//    strcpy(fileName, BUY_LOG_FILE);
//    transType = TRANS_BUY;
//
//    strcpy(activeUserTitle, "����� ����");
//// mgh_93    strcatUserID(fileName);
//
//    if (!readTransactionListFile(fileName, transactions, sizeof(buyTransLogST), MAX_BUY_TRANS_LOG_RECORDS,
//    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, FATAL, "", "transaction Buy List 5");
//
//    strcpy(title, "�����ǘ�� ����");
//
//    if (!displayTransactionBuyList(transactions, transactionNumber, requestedNum, title, &startDateTime,
//    		&endDateTime, activeUserTitle, activeUser, sumValues))
//    	return FALSE;
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "display list");
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//    	if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN,
//        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime)))
//    		return FALSE;
////        if (!printOneBuyLog(&transactions[counter]))
////            return FALSE;
//
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif

uint8 displayTransactionBillPayList(billPayTransLogST* transactions, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};	//HNO:change 700 to 3500 &3 to 4 for header	/** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	uint8			shGabzValue[18]				= {0};
	uint8			shPardakhtValue[14]			= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].billAmount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);

        if (i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 12)) //6: header, 4: footer, 12: body
    	{
	    	/** TRANSACTIONS LOG ; LINE 1: TRANSACTION BILL ID */
	    	memcpy(shGabzValue, transactions[counter].billID, 13);
		    shGabzValue[13] = 0;
		    removePadLeft(shGabzValue,'0');
		    
		    justifyTwoStringFarsi("", "����� ���", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, shGabzValue);
		            
	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION PAYEMNT ID */
	        memcpy(shPardakhtValue, transactions[counter].paymentID, 13);
		    shPardakhtValue[13] = 0;
		    removePadLeft(shPardakhtValue, '0');  
	        
	    	justifyTwoStringFarsi("", "����� ������", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, shPardakhtValue);
	
	        fillTransListCommonPartLines(lines, &i, transactions[counter].billAmount, transactions[counter].PAN,
	        		transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime), depositID); //MRF_NEW17
	        
	        displayedCount++;
	        strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, billTransPic);
    
	if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;
	else
		return TRUE;
}

//HNO
uint8 displayTransactionLoanPayList(loanPayTransLogST* loanPay, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};	//HNO:change 700 to 3500 &3 to 4 for header	/** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	uint8			destinationCard[16]         = {0};
	uint8			nameStr[20]                 = {0};
    uint8			familyStr[20]               = {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8          depositID[11 + 1]           = {0};//MRF_NEW17  //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, loanPay[counter].amount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);

        if (i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 12)) //6: header, 4: footer, 12: body
    	{
	    	/** TRANSACTIONS LOG ; LINE 1: TRANSACTION loan pan */
	    	memcpy(destinationCard, loanPay[counter].destinationCardPAN, 16);
		    destinationCard[16] = 0;
		    removePadLeft(destinationCard,'0');
       
		    justifyTwoStringFarsi("", "����� ���� ���", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, destinationCard);
		            
	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION card holder name */
	        memcpy(nameStr, loanPay[counter].destinationCardHolderName, strlen(loanPay[counter].destinationCardHolderName));
            memcpy(familyStr, loanPay[counter].destinationCardHolderFamily, strlen(loanPay[counter].destinationCardHolderFamily));
            strcat(nameStr," ");
            strcat(nameStr,familyStr);
	        
	    	justifyTwoStringFarsi("", "�� ���", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, nameStr);
            memset(nameStr,0,sizeof(nameStr));
	
	        fillTransListCommonPartLines(lines, &i, loanPay[counter].amount, loanPay[counter].PAN,
	        		loanPay[counter].retrievalReferenceNumber, 0, 
                    &(loanPay[counter].dateTime), depositID); //MRF_NEW17
	        
	        displayedCount++;
	        strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, sucessLoanPay);
    
	if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;
	else
		return TRUE;
}

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
uint8 transactionBillPayList(argumentListST* args)
{    
    filesST*        	files                                             = (filesST*) (args->argumentName);
    dateTimeST			startDateTime                                     = {0, 0};
    dateTimeST			endDateTime                                       = {0, 0};
    billPayTransLogST   transactions[MAX_BILL_PAY_TRANS_LOG_RECORDS]      = {0};
    int					transactionNumber                                 = 0;    /** charge number with specified date in file */
    int					counter                                           = 0;    /** loop counter */
    uint32				requestedNum                                      = 0;    /** user requested transaction number, integer form */
    uint8				shGabzValue[15]                                   = {0};
    uint8				shPardakhtValue[14]                               = {0};
    uint8               sumValues[15 + 1]                                 = {0}; 
    uint8				fileName[FILE_NAME_LENTGH]                        = {0};
    uint8				activeUser[15 + 1]                                = {0};
    uint8				activeUserTitle[15 + 1]                           = {0};
    uint8				title[30]                                         = {0};
    uint8               check                                             = FALSE;
    uint8               sumDailyValues[15 + 1]                            = {0};
    uint8               tempSumValues[15 + 1]                             = {0}; 
    uint8               valueWithComma[20]                                = {0}; 
    uint8               depositID[11 + 1]                                 = {0};//MRF_NEW17 //ABS_NEW1

    strcpy(fileName, BILL_PAY_LOG_FILE);
    
    if (!readTransactionListFile(fileName, transactions, sizeof(billPayTransLogST), MAX_BILL_PAY_TRANS_LOG_RECORDS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
        
    strcpy(title, "�����ǘ�� ���� ���");

    if (!displayTransactionBillPayList(transactions, transactionNumber, requestedNum, "��� ����", &startDateTime,
                		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
        	return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;
    
    if (! printReceiptDate(transactions[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;
    
    printDash3();
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(transactions[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }
        
    	/** PRINT BiLL ID */
        memset(shGabzValue, 0, sizeof(shGabzValue));
	    strncat(shGabzValue, transactions[counter].billID, 13);
#ifdef VX520		
		//strcat(shGabzValue, "|");//ABS:COMMENT
		shGabzValue[14] = 0;
		removePadLeft(shGabzValue, '0');
		if (!printTwoStringNumericFarsi(shGabzValue, "����� ���", PRN_NORM))//ABS:CHANGE
			return FALSE;

		/** PRINT PAYMENT ID */
		memset(shPardakhtValue, 0, sizeof(shPardakhtValue));
		strncat(shPardakhtValue, transactions[counter].paymentID, 13);
		//strcat(shPardakhtValue, "|"); //ABS:COMMENT
		shGabzValue[14] = 0;
		removePadLeft(shPardakhtValue, '0');
		if (!printTwoStringNumericFarsi(shPardakhtValue, "����� ������", PRN_NORM))	//ABS:CHANGE
			return FALSE;
#else
        strcat(shGabzValue, "|");
	    shGabzValue[14] = 0;
	    removePadLeft(shGabzValue,'0');
	    if (!printTwoStringNumericFarsi(shGabzValue, "|����� ���", PRN_NORM))
	        return FALSE;
	    
	    /** PRINT PAYMENT ID */
        memset(shPardakhtValue, 0, sizeof(shPardakhtValue));
	    strncat(shPardakhtValue, transactions[counter].paymentID, 13);
        strcat(shPardakhtValue, "|");
	    shGabzValue[14] = 0;
	    removePadLeft(shPardakhtValue,'0');
	    if (!printTwoStringNumericFarsi(shPardakhtValue, "|����� ������", PRN_NORM))
	        return FALSE;
#endif
	    
    	if (!printTransactionListCommonPart(transactions[counter].billAmount, transactions[counter].PAN, 
    			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;
        
        check = compareDates(&transactions[counter].dateTime, &transactions[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, transactions[counter].billAmount, sumDailyValues);
        
        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();
        
        if (check != 0)
         {
             makeValueWithCommaStr(sumDailyValues, valueWithComma);

             if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                 return FALSE;

             printStar();
             memset(sumDailyValues, 0, sizeof(sumDailyValues));
         }
        
        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}
//#else
///**
// * display & print billPay transaction list report.
// * @see     searchDateRangeInFixedFile()
// * @see     readFixedFileInfoArray()
// * @see     displayScrollable()
// * @see     printLogo()
// * @see     printReceiptDateTime()
// * @see     show_one_log()
// */
//uint8 transactionBillPayList(argumentListST* args)
//{
//    filesST*        	files                   = (filesST*) (args->argumentName);
//    dateTimeST			startDateTime			= {0, 0};
//    dateTimeST			endDateTime				= {0, 0};
//    int					transactionNumber		= 0;            /** charge number with specified date in file */
//    uint32				requestedNum			= 0;            /** user requested transaction number, integer form */
//    int					counter                 = 0;            /** loop counter */
//    uint8				shGabzValue[14] 		= {0};
//    uint8				shPardakhtValue[14]		= {0};
//    uint8               sumValues[15 + 1]       = {0};
//    uint8				fileName[FILE_NAME_LENTGH]	= {0}; //MRF_NEW3
//    uint8				activeUser[15 + 1]		= {0};
//    uint8				activeUserTitle[15 + 1]	= {0};
//    uint8				title[30]			= {0};
//    billPayTransLogST   transactions[MAX_BILL_PAY_TRANS_LOG_RECORDS]  = {0}; //HNO: change 200 to 1000
//
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
//
//    strcpy(fileName, BILL_PAY_LOG_FILE);
//// mgh_93    strcatUserID(fileName);
//
//    if (!readTransactionListFile(fileName, transactions, sizeof(billPayTransLogST), MAX_BILL_PAY_TRANS_LOG_RECORDS,
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "��� ��ǘ�� ���");
//
//    if (!displayTransactionBillPayList(transactions, transactionNumber, requestedNum, title, &startDateTime,
//                		&endDateTime, activeUserTitle, activeUser, sumValues))
//        	return FALSE;
//
//	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//		return FALSE;
//
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//    	/** PRINT BiLL ID */
//	    memcpy(shGabzValue, transactions[counter].billID, 13);
//	    shGabzValue[13] = 0;
//	    removePadLeft(shGabzValue,'0');
//	    if (!printTwoStringNumericFarsi(shGabzValue, "����� ���", PRN_NORM))
//	        return FALSE;
//
//	    /** PRINT PAYMENT ID */
//	    memcpy(shPardakhtValue, transactions[counter].paymentID, 13);
//	    shGabzValue[13] = 0;
//	    removePadLeft(shPardakhtValue,'0');
//	    if (!printTwoStringNumericFarsi(shPardakhtValue, "����� ������", PRN_NORM))
//	        return FALSE;
//
//    	if (!printTransactionListCommonPart(transactions[counter].billAmount, transactions[counter].PAN,
//    			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime)))
//    		return FALSE;
//
////        if (!printOneBillPayLog(&transactions[counter]))
////            return FALSE;
////
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif
//transactionNumber
uint8 displayTransactionErrorList(transErrorST* errors, int errorNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser)
{
	int				counter                                          = 0;            
	int				errorYear                                        = 0;
    int				errorMonth                                       = 0; 
    int				errorDay                                         = 0;
    int				errorHour                                        = 0;
    int				errorMinute                                      = 0; 
    uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]  = {0,0};//MRF_NEW3
	uint16  		i                                                = 0;
    uint32			jalaliDate                                       = 0;  
    uint8			key                                              = KBD_CANCEL;
	uint8			displayedCount                                   = 0;  
    uint8			dateValueStr[18]                                 = {0};
    uint8			timeValueStr[6]                                  = {0};
    uint8			transType[20]                                    = {0};
    uint8			errorCode[11]                                    = {0};
    uint8			errorsNum[10]                                    = {0};
    uint8           spaceCountStr[10]                                = {0};
	terminalSpecST  terminalCapability                               = getTerminalCapability();
    
    memset(lines, 0, sizeof(lines));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**displayTransactionErrorList**");
    
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	
    /** ERRORS LOG */
    for (counter = requestedNum - 1; counter >= 0 && (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 8)); counter--) // change 2 to 4 and 5 line for body to 8
    {
        /** ERRORS LOG ; LINE 1 & 2: ERROR DATE AND TIME */
        gregorianToJalali(&jalaliDate , errors[counter].dateTime.date);
        
        errorYear = getYear(jalaliDate);
        errorMonth = getMonth(jalaliDate);
        errorDay = getDay(jalaliDate);
        errorHour = getHour(errors[counter].dateTime.time);
        errorMinute = getMinute(errors[counter].dateTime.time);
        
        /** ERRORS LOG ; LINE 3: ERROR TRANSACTION TYPE */
        memset(transType, 0, sizeof(transType));
        getIsoTransactionName(transType, errors[counter].transType);
        justifyTwoStringFarsi("", "��� ��ǘ��", lines[i++], PRN_NORM);
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, transType);
        
        /** ERRORS LOG ; LINE 4: ERROR CODE */
        sprintf(errorCode, "%d", errors[counter].errorCode);
        justifyTwoStringFarsi(errorCode, "����", lines[i++], PRN_NORM);
        
        /** ERRORS LOG ; LINE 5 & 6: ERROR MESSAGE & DASHES */
        memset(spaceCountStr,' ', SPACE_COUNT);
        sprintf(dateValueStr, "%04d/%02d/%02d", errorYear, errorMonth, errorDay);
        sprintf(timeValueStr, "%02d:%02d", errorHour, errorMinute);
        sprintf(lines[i++], "%c%c%c%s%s%s", PRN_NORM, ALIGN_CENTER, FARSI, dateValueStr, spaceCountStr, timeValueStr);
        justifyOneStringFarsi(DISPLAY_DASH, lines[i++], PRN_NORM, ALIGN_CENTER);
        displayedCount++;
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
   		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
   		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
   	}
    
    /** LINE (ERRORS LOG + 4) : ERRORS NUMBER IN DATES */
    sprintf(errorsNum, "%d", errorNumber);
    justifyTwoStringFarsi(errorsNum, "�����", lines[i++], PRN_NORM);
    
    /** LINE (ERRORS LOG + 5) : REQUESTED ERRORS */
    sprintf(errorsNum, "%d", displayedCount);
    justifyTwoStringFarsi(errorsNum, "���� ���", lines[i++], PRN_NORM);
    
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,bankingErrors);
    
    if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;
    else
    	return TRUE;
}


/**
 * display & print transactions error report.
 * @param   known known or unknown error?
 * @see     searchDateRangeInFixedFile()
 * @see     readFixedFileInfoArray()
 * @see     displayScrollable()
 * @see     printLogo()
 * @see     printReceiptDateTime()
 */
uint8 transactionsErrorList(argumentListST* args)
{
    filesST*        files                                   = (filesST*) (args->argumentName);
    uint8			fileName[FILE_NAME_LENTGH]              = {0};	//MRF_NEW3	/** transactions error file name */
    dateTimeST		startDateTime                           = {0, 0};
    dateTimeST		endDateTime                             = {0, 0};
    int				errorNumber                             = 0;		/** errors number with specified date in file */
    int				counter                                 = 0;        //mgh int -> uint16			/** loop counter */
    uint32  		requestedNum                            = 0;		/** user requested errors number, integer form */
    uint8			errorsNum[10]                           = {0};		/** errors number in dates, exist or listed */
    uint8			printStr[100]                           = {0};		/** print styring */
    uint8			transType[10]                           = {0};  //Length of array must be 10 : MRF_NEW10
    uint8			errorCode[11]                           = {0};
    uint8			tmp                                     = 0;		/** temp needed argument */
    uint8			activeUser[15 + 1]                      = {0};
    uint8			activeUserTitle[15 + 1]                 = {0};
    uint8			title[30]                               = {0};
    transErrorST    errors[MAX_TRANS_ERROR_LOG_RECORDS]     = {0};
    merchantSpecST  merchantSpec;
    dateTimeST      dateTime;
    
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    memset(&dateTime, 0, sizeof(dateTimeST));
    memset(errors, 0, sizeof(errors)); 

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****transactionsErrorList****");
    strcpy(fileName, TRANS_ERROR_LOG_FILE);
    
    if (!readTransactionListFile(fileName, errors, sizeof(transErrorST), MAX_TRANS_ERROR_LOG_RECORDS, 
            						&startDateTime, &endDateTime, &errorNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    strcpy(title,"������ �����");

    if (!displayTransactionErrorList(errors, errorNumber, requestedNum, title, &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser))
    {
       	return FALSE;
    }
    
    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    {
    	return FALSE;
    }
	
    /** PRINT ERRORS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        /** PRINT ERROR TRANSACTION TYPE */
        memset(transType, 0, sizeof(transType));
        getIsoTransactionName(transType, errors[counter].transType);
        
//        if (!printTwoStringFarsi(transType, "��� ��ǘ��:", PRN_NORM)) --MRF_NEW3
//            return FALSE;

        if (cancelFunction())
            return FALSE;
    
        /** PRINT ERROR CODE */
        memset(errorCode, 0, sizeof(errorCode));
        sprintf(errorCode, "%d", errors[counter].errorCode);
        
//        if (!printTwoStringNumericFarsi(errorCode, "���", PRN_NORM)) --MRF_NEW3
//            return FALSE;
#ifdef VX520
		if (!printTwoStringNumericFarsi(errorCode, transType, PRN_NORM))//ABS:CHANGE
			return FALSE;
#else
		if (!printTwoStringFarsi(errorCode, transType, PRN_NORM))
            return FALSE;
#endif	
        if (cancelFunction())
            return FALSE;
    
        /** PRINT ERROR MESSAGE */
        getIsoErrorMessage(printStr, errors[counter].errorCode, &tmp);
        
        if (!printOneStringFarsi(printStr, PRN_NORM, ALIGN_RIGHT)) //MRF_NEW3
        	return FALSE;
        
        if (cancelFunction())
            return FALSE;
    
        if (!printReceiptDateTime(&errors[counter].dateTime, TRUE))
            return FALSE;
        
        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }

    /** PRINT ERRORS NUMBER IN DATES */
    sprintf(errorsNum, "%ld", errorNumber);
    if (!printTwoStringNumericFarsi(errorsNum, "�����", PRN_NORM))
        return FALSE;

    if (cancelFunction())
        return FALSE;
    
    /** PRINT REQUESTED ERRORS NUMBER */
    sprintf(errorsNum, "%ld", requestedNum);
    if (!printTwoStringNumericFarsi(errorsNum, "���� ���", PRN_NORM))
        return FALSE;

    /** PRINT EMPTY LINES */
    printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}

uint8 displayunsuccessBuyTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]	= {0, 0};
	uint16  		i                       						= 0;
	uint8			tempSumValues[15 + 1]   						= {0};                          /** sum of listed transactions value */
	int				counter                 						= 0;                            /** loop counter */
	uint8			key 											= KBD_CANCEL;
	uint8			displayedCount          						= 0;                            /** display counter */
	uint8			displayedSumValues[15 + 1]						= {0};
	uint8			referenceNum[1]									= {0};
	terminalSpecST  terminalCapability								= getTerminalCapability();
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 8))
    	{
             //MRF_NEW2
            fillTransListCommonPartLines(lines, &i, reversalInfo[counter].amount, reversalInfo[counter].PAN, 
    				referenceNum, reversalInfo[counter].STAN,
                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
                    &(reversalInfo[counter].SWdateTime), reversalInfo[counter].depositID); //MRF_NEW17
                    
    		displayedCount++;
    		strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
	}
 
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);

    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,buyUnsuccessTransPic);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else 
    	return TRUE;
}


uint8 displayunsuccessBillPayTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]		= {0, 0};		//HNO:change 400 to 2000 and 3 to 4 for footer /** 3: fot the last three lines */
	uint16  		i                       							= 0;
	uint8			tempSumValues[15 + 1]   							= {0};       	/** sum of listed transactions value */
	int				counter                 							= 0;            /** loop counter */
	uint8			key 												= KBD_CANCEL;
	uint8			displayedCount          							= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]							= {0};
	uint8			referenceNum[1]										= {0};
    uint8			shGabzValue[18]										= {0};
	uint8			shPardakhtValue[14]									= {0};
	terminalSpecST  terminalCapability									= getTerminalCapability();
    uint8          depositID[11 + 1]                                   = {0};//MRF_NEW17 //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].billSpec.billAmount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 12))//6 for header,4 for footer and 12 for body
    	{
            /** TRANSACTIONS LOG ; LINE 1: TRANSACTION BILL ID */
	    	memcpy(shGabzValue, reversalInfo[counter].billSpec.billID, 13);
		    shGabzValue[13] = 0;
		    removePadLeft(shGabzValue,'0');
		    
		    justifyTwoStringFarsi("", "����� ���", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, shGabzValue);
		            
	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION PAYEMNT ID */
	        memcpy(shPardakhtValue, reversalInfo[counter].billSpec.paymentID, 13);
		    shPardakhtValue[13] = 0;
		    removePadLeft(shPardakhtValue, '0');  
	        
	    	justifyTwoStringFarsi("", "����� ������", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, shPardakhtValue);
            
    		fillTransListCommonPartLines(lines, &i, reversalInfo[counter].billSpec.billAmount, reversalInfo[counter].PAN, 
    				referenceNum, reversalInfo[counter].STAN, 
                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
                    &(reversalInfo[counter].SWdateTime), depositID); //MRF_NEW17
    		displayedCount++;
    		strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
	}
 
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    
    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
	else
		key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,unsuccessBillPic);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else 
    	return TRUE;
}

////HNO
//uint8 displayunsuccessLoanPayTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
//		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
//{
//	uint8               lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]	= {0, 0};		//HNO:change 400 to 2000 and 3 to 4 for footer /** 3: fot the last three lines */
//	uint16              i                       						= 0;
//	uint8               tempSumValues[15 + 1]   						= {0};       	/** sum of listed transactions value */
//	int                 counter                 						= 0;            /** loop counter */
//	uint8               key 											= KBD_CANCEL;
//	uint8               displayedCount          						= 0;            /** display counter */
//	uint8               displayedSumValues[15 + 1]						= {0};
//	uint8               referenceNum[1]									= {0};
//	uint8               destinationCard[16]         					= {0};
//	uint8               nameStr[20]                 					= {0};
//    uint8               familyStr[20]               					= {0};
//	terminalSpecST  	terminalCapability								= getTerminalCapability();
//	
//	memset(lines, 0, sizeof(lines));
//	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
//	    
//    for (counter = requestedNum - 1; counter >= 0; counter--) 
//    {
//        //mgh: adding 2string...
//        strcpy(tempSumValues, sumValues);
//        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumValues);
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
//        
//    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 12))//6 for header,4 for footer and 12 for body
//    	{
//	    	/** TRANSACTIONS LOG ; LINE 1: TRANSACTION loan pan */
//            maskCardId(reversalInfo[counter].loanPay.destinationCardPAN, destinationCard);
//		    justifyTwoStringFarsi("", "����� ���� ���", lines[i++], PRN_NORM);
//	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, destinationCard);
//		            
//	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION card holder name */
//	        memcpy(nameStr, reversalInfo[counter].loanPay.destinationCardHolderName, strlen(reversalInfo[counter].loanPay.destinationCardHolderName));
//            memcpy(familyStr, reversalInfo[counter].loanPay.destinationCardHolderFamily, strlen(reversalInfo[counter].loanPay.destinationCardHolderFamily));
//            strcat(nameStr," ");
//            strcat(nameStr,familyStr);
//	        
//	    	justifyTwoStringFarsi("", "�� ���", lines[i++], PRN_NORM);
//	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, nameStr);
//            memset(nameStr,0,sizeof(nameStr));
//            
//    		fillTransListCommonPartLines(lines, &i, reversalInfo[counter].loanPay.amount, reversalInfo[counter].PAN, 
//    				referenceNum, reversalInfo[counter].STAN, 
                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
//                    &(reversalInfo[counter].SWdateTime), transactions[counter].depositID); //MRF_NEW17
//    		displayedCount++;
//    		strcpy(displayedSumValues, sumValues);
//    	}
//    }
//    
//    if (requestedNum != displayedCount) //displayed transactions are less than requested
//    {
//    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
//		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
//		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
//	}
// 
//    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
//
//    if (!terminalCapability.graphicCapability)
//    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
//    else
//    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,unsuccessLoanPay);
//
//    if (key != KBD_ENTER && key != KBD_CONFIRM)
//        return FALSE;
//    else 
//    	return TRUE;
//}

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
//HNO_IDENT
uint8 unsuccessBuyTransaction(argumentListST* args)
{    
    filesST*        			files                                 = (filesST*) (args->argumentName);
    dateTimeST					startDateTime                         = {0, 0};
    dateTimeST					endDateTime                           = {0, 0};
    reversalTransST				reversalInfo[MAX_UNSUCCESS_BUY_TRANS] = {0};
    int							transactionNumber			= 0;            /** charge number with specified date in file */
    int							counter                 	= 0;            /** loop counter */
    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
    uint8						sumValues[15 + 1]       	= {0};          /** sum of listed transactions value */
    uint8						fileName[FILE_NAME_LENTGH]	= {0};
    uint8						activeUser[15 + 1]			= {0};
    uint8						activeUserTitle[15 + 1]		= {0};
    uint8						title[30]                   = {0};
    uint8						referenceNum[1]				= {0};
    uint8                       check                       = FALSE;
    uint8                       sumDailyValues[15 + 1]      = {0};
    uint8                       tempSumValues[15 + 1]       = {0}; 
    uint8                       valueWithComma[20]          = {0}; 
    uint8						selectUser[16]				= {0};//HNO_SHIFT
    uint8						shift[22]					= {0};//HNO_SHIFT
	uint8						loginUserName[15 + 1]		= { 0 }; //ABS:ADD

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****unsuccessBUY****");
    
    strcpy(fileName, REVERSAL_BUY_LOG_FILE);
    
	if(getPOSService().shiftService == TRUE)//HNO_SHIFT
	{
		getLoginUser(loginUserName);
		if (strcmp("����", loginUserName) == 0)//HNO_IDENT9//admin can see all users report
		{
			if (!displayUsersList(selectUser, FALSE))//ABS
			return FALSE;
			if(strcmp("����", selectUser) != 0)//HNO_SHIFT10
				strcat(fileName, selectUser);
		}
		else
		{
			strcat(fileName, loginUserName);
			strcpy(selectUser, loginUserName);//HNO_SHIFT
		}
	}

    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_BUY_TRANS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    strcpy(title, "�����ǘ�� ������ ����");
    if (!displayunsuccessBuyTransaction(reversalInfo, transactionNumber, requestedNum, "���� ������", &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
           	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    if (!printReceiptDate(reversalInfo[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;

    if(getPOSService().shiftService == TRUE)//HNO_SHIFT3
    {
		strcpy(shift, "���� ");
		strcat(shift, selectUser);
		printTwoStringFarsi(shift, "����� ����:", PRN_NORM);//HNO_SHIFT
    }

    printDash3();
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(reversalInfo[counter].dateTime.date,"�����"))
               return FALSE;

           printDash3();
        }
        
    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum, 
    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime), reversalInfo[counter].depositID))
    		return FALSE;

        check = compareDates(&reversalInfo[counter].dateTime, &reversalInfo[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumDailyValues);
        
        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();
        
        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);

            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }
        
        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}
//#else
///**
// * display & print unsuccess buy transaction list report.
// * @see     searchDateRangeInFixedFile()
// * @see     readFixedFileInfoArray()
// * @see     displayScrollable()
// * @see     printLogo()
// * @see     printReceiptDateTime()
// * @see     show_one_log()
// */
//uint8 unsuccessBuyTransaction(argumentListST* args)
//{
//    filesST*        			files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST					startDateTime				= {0, 0};
//    dateTimeST					endDateTime					= {0, 0};
//    int							transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
//    int							counter                 	= 0;            /** loop counter */
//    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
//    uint8						fileName[FILE_NAME_LENTGH]	= {0};
//    //mgh messageSpecST   		messageSpec;
//    uint8						activeUser[15 + 1]			= {0};
//    uint8						activeUserTitle[15 + 1]		= {0};
//    uint8						title[30]                   = {0};
//    uint8						referenceNum[1]				= {0};
//    reversalTransST				reversalInfo[MAX_UNSUCCESS_BUY_TRANS]  = {0}; //MRF_NEW3
//
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
////    memset(lines, 0, sizeof(lines));
//    memset(reversalInfo, 0, sizeof(reversalInfo));//HNO_ADD
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "unsuccessBUY");
//    strcpy(fileName, REVERSAL_BUY_LOG_FILE);
//
//    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_BUY_TRANS,
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "���� ������");
//    if (!displayunsuccessBuyTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
//                   		&endDateTime, activeUserTitle, activeUser, sumValues))
//           	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum,
//    			reversalInfo[counter].STAN,/*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime)))
//    		return FALSE;
//
////        if (!printOneUnsuccessBuyLog(&reversalInfo[counter]))
////            return FALSE;
//
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
//HNO_IDENT
uint8 unsuccessBillPayTransaction(argumentListST* args)
{    
    filesST*        			files                   	= (filesST*) (args->argumentName);

    dateTimeST					startDateTime                             = {0, 0};
    dateTimeST					endDateTime                               = {0, 0};
    reversalTransST				reversalInfo[MAX_UNSUCCESS_BILLPAY_TRANS] = {0}; //MRF_NEW3
    int							transactionNumber			= 0;                /** charge number with specified date in file */
    int							counter                 	= 0;                /** loop counter */
    uint32						requestedNum				= 0;                /** user requested transaction number, integer form */
    uint8						sumValues[15 + 1]       	= {0};              /** sum of listed transactions value */
    uint8						fileName[FILE_NAME_LENTGH]	= {0};
    uint8						activeUser[15 + 1]			= {0};
    uint8						activeUserTitle[15 + 1]		= {0};
    uint8						title[30]                   = {0};
    uint8						referenceNum[1]				= {0};
    uint8                       check                       = FALSE;
    uint8                       sumDailyValues[15 + 1]      = {0};
    uint8                       tempSumValues[15 + 1]       = {0}; 
    uint8                       valueWithComma[20]          = {0};
    uint8                       depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1


	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****unsuccessBillPayTransaction****");
    strcpy(fileName, REVERSAL_BILLPAY_LOG_FILE);
    
    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_BILLPAY_TRANS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    strcpy(title, "�����ǘ�� ������ ���");
    if (!displayunsuccessBillPayTransaction(reversalInfo, transactionNumber, requestedNum, "��� ������", &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
           	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    if (!printReceiptDate(reversalInfo[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;
    
    printDash3();
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(reversalInfo[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }
        
    	if (!printTransactionListCommonPart(reversalInfo[counter].billSpec.billAmount, reversalInfo[counter].PAN, referenceNum, 
    			reversalInfo[counter].STAN,/*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime), depositID))//MRF_NEW17
    		return FALSE;
        
        check = compareDates(&reversalInfo[counter].dateTime, &reversalInfo[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumDailyValues);
        
        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();
        
        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);

            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }

        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}
//#else
//uint8 unsuccessBillPayTransaction(argumentListST* args)
//{
//    filesST*        			files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST					startDateTime				= {0, 0};
//    dateTimeST					endDateTime					= {0, 0};
//    int							transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
//    int							counter                 	= 0;            /** loop counter */
//    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
//    uint8						fileName[FILE_NAME_LENTGH]	= {0};
//    //mgh messageSpecST   		messageSpec;
//    uint8						activeUser[15 + 1]			= {0};
//    uint8						activeUserTitle[15 + 1]		= {0};
//    uint8						title[30]                   = {0};
//    uint8						referenceNum[1]				= {0};
//    reversalTransST				reversalInfo[MAX_UNSUCCESS_BILLPAY_TRANS] = {0}; //MRF_NEW3
//    
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
////    memset(lines, 0, sizeof(lines));
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "unsuccessbillPay");
//    strcpy(fileName, REVERSAL_BILLPAY_LOG_FILE);
//    
//    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_BILLPAY_TRANS,
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "��� ������");
//    if (!displayunsuccessBillPayTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
//                   		&endDateTime, activeUserTitle, activeUser, sumValues))
//           	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//    
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//    	if (!printTransactionListCommonPart(reversalInfo[counter].billSpec.billAmount, reversalInfo[counter].PAN, referenceNum,
//    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime)))
//    		return FALSE;
//
//
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif

//HNO
//uint8 unsuccessLoanPayTransaction(argumentListST* args)
//{    
//    filesST*        			files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST					startDateTime				= {0, 0};
//    dateTimeST					endDateTime					= {0, 0};
//    int							transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
//    int							counter                 	= 0;            /** loop counter */
//    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
//    uint8						fileName[FILE_NAME_LENTGH]	= {0};
//    //mgh messageSpecST   		messageSpec;
//    uint8						activeUser[15 + 1]			= {0};
//    uint8						activeUserTitle[15 + 1]		= {0};
//    uint8						title[30]                   = {0};
//    uint8						referenceNum[1]				= {0};
//    uint8                       destinationCard[16]         = {0};
//    uint8                       nameStr[20]                 = {0};
//    reversalTransST				reversalInfo[50]   			= {0};
//    
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
////    memset(lines, 0, sizeof(lines));
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "unsuccessloanPay");
//    strcpy(fileName, REVERSAL_LOAN_PAY_FILE);
//    
//    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_LOANPAY_TRANS, 
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "��� ������");
//    if (!displayunsuccessLoanPayTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
//                   		&endDateTime, activeUserTitle, activeUser, sumValues))
//           	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//    
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//        /** PRINT card Pan */
//        maskCardId(reversalInfo[counter].loanPay.destinationCardPAN, destinationCard);
//#ifdef CASTLES_V5S //MRF_TTF
//        reversPartString(destinationCard);
//#endif
//	    if (!printTwoStringNumericFarsi(destinationCard, "����� ���� ���", PRN_NORM))
//	        return FALSE;
//	           
//        if (!printOneStringFarsi("�� ���:", PRN_NORM, ALIGN_RIGHT))
//            return FALSE;
//        
//        sprintf(nameStr, "%s %s", reversalInfo[counter].loanPay.destinationCardHolderName, reversalInfo[counter].loanPay.destinationCardHolderFamily);
//        printOneStringFarsi(nameStr, PRN_NORM, ALIGN_LEFT);
//        
//    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum, 
//    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime)))
//    		return FALSE;
//
//        
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}

//delete all user dependant files, such as transaction list files
void deleteDependantFiles(uint8 index)
{
	uint8 	userIndex[4]               = {0};
	uint8	fileName[FILE_NAME_LENTGH] = {0}; //MRF_NEW3
	
	sprintf(userIndex, "%d", index);
	
	strcpy(fileName, CHARGE_LOG_FILE);
	strcat(fileName, userIndex);
	fileRemove(fileName);
	
	strcpy(fileName, BILL_PAY_LOG_FILE);
	strcat(fileName, userIndex);
	fileRemove(fileName);
	
	strcpy(fileName, BUY_LOG_FILE);
	strcat(fileName, userIndex);
	fileRemove(fileName);
    
    //MRF
    strcpy(fileName, SUPERVISOR_LOG_FILE);
	strcat(fileName, userIndex);
	fileRemove(fileName);
}

void getIssuerBankName(uint8* BINstr, uint8* Name)
{
	int32 BIN; //ABS:ADD:960801
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "BINstr is: %s", BINstr);
    BIN = strToInt(BINstr);//ABS:CHANGE:960801
	
    switch(BIN)
    {
        case 627961:
            strcpy(Name, "���� � ����");
            break;
        case 610433:
            strcpy(Name, "���");
            break;
        case 589463:
            strcpy(Name, "���� ��ѐ���");
            break;
        case 628023:
            strcpy(Name, "�Ә�");
            break;
        case 589210:
        case 604932:
            strcpy(Name, "Ӂ�");
            break;
        case 603770:
            strcpy(Name, "�������");
            break;
        case 603799:
            strcpy(Name, "���");
            break;
        case 627353:
        case 585983:
            strcpy(Name, "�����");
            break;
        case 603769:
            strcpy(Name, "������");
            break;
        case 627648:
            strcpy(Name, "����� ������");
            break;
        case 627760:
            strcpy(Name, "��� ���");
            break;
        case 502908:
            strcpy(Name, "����� �����");
            break;
        case 628157:
            strcpy(Name, "����� ������� �����");
            break;
        case 627488:
            strcpy(Name, "��������");
            break;
        case 622106:
            strcpy(Name, "�������");
            break;
        case 627412:
            strcpy(Name, "������ ����");
            break;
        case 621986:
            strcpy(Name, "�����");
            break;
        case 502229:
            strcpy(Name, "����ѐ��");
            break;
        case 639607:
            strcpy(Name, "������");
            break;
        case 639346:
            strcpy(Name, "����");
            break;
        case 606373:
            strcpy(Name, "��� ������ ��� �����");
            break;
        case 504706:
            strcpy(Name, "���");
            break;
        case 636214:
            strcpy(Name, "�����");
            break;
        case 627381:
            strcpy(Name, "�����");
            break;
        case 505416:
            strcpy(Name, "���Ԑ��");
            break;
        case 636949:
            strcpy(Name, "��������");
            break;
        case 502938:
            strcpy(Name, "��");
            break;
        case 505785:
            strcpy(Name, "����� ����");
            break;
        case 504172:
            strcpy(Name, "�����");
            break;
        case 505809:
            strcpy(Name, "���������");
            break;
        case 585947:
            strcpy(Name, "���������");
            break;
        case 639599:
            strcpy(Name, "������");
            break;
        case 505801:
            strcpy(Name,"����� ���� � ������� ����");
            break;
        case 606256:
            strcpy(Name, "����� ���� � ������� �Ә���");
            break;
        case 10060:
            strcpy(Name, "��� �ј��");
            break;
        case 636795:
            strcpy(Name, "����� ������� ����� ��� �ј��");
            break;
        case 581672:
            strcpy(Name, "�Ș� �������� ������ ����_ �ǁј");
            break;
        case 581874:
            strcpy(Name, "����� �������");
            break;
        case 507677:
            strcpy(Name, "����� ������� ���");//MRF_NEW
            break;
        default:
            strcpy(Name, "������");
            break;
    }
}


uint8 readReversalInfo(int8* fileName, messageSpecST* messageSpec) 
{
    reversalTransST		reversalInfo;
    uint32              sizeOfFile 	= sizeof(reversalTransST); 	/** size of reversal message file */
    int16               retValue	= FAILURE; 					/** read file function return value */
    int16               retValue2	= FAILURE;
    int16               maxRecord   = 0;
    int8                logName[FILE_NAME_LENTGH]     = {0};

    memset(&reversalInfo, 0, sizeof(reversalTransST));

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "****readReversalInfo****");
    retValue = readFileInfo(fileName, &reversalInfo, &sizeOfFile);    
    if (retValue != SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "retval File reversal = %d", retValue);
//mgh_94: TODO
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "err #5");
//        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }

    strcpy(messageSpec->cardSpec.PAN, reversalInfo.PAN);
    strcpy(messageSpec->amount, reversalInfo.amount);
    strcpy(messageSpec->retrievalReferenceNumber, reversalInfo.retrievalReferenceNumber);
    strcpy(messageSpec->buyChargeSpec.realAmount, reversalInfo.ChargeRealAmount);
    messageSpec->merchantSpec.recieveSTAN = reversalInfo.STAN;
    messageSpec->transType                = reversalInfo.transType;
    messageSpec->dateTime                 = reversalInfo.SWdateTime;//MRF_NEW2
    messageSpec->SWdateTime               = reversalInfo.SWdateTime; //MRF_NEW2
    messageSpec->responseStatus           = reversalInfo.responseStatus;
    messageSpec->billSpec                 = reversalInfo.billSpec;
    messageSpec->buyChargeSpec.type       = reversalInfo.chargeType;
//	messageSpec->loanPay = reversalInfo.loanPay;//HNO_COMMENT not usefull
//    messageSpec->ETC = reversalInfo.ETC; //MRF_ETC
    messageSpec->topup                    = reversalInfo.topup; //MRF_TOPUP
    messageSpec->charity                  = reversalInfo.charity; //HNO_DEBUG2

    
/*
     //mgh: start of adding reversal Log
    switch (reversalInfo.transType) {
        case TRANS_BUY:
            strcpy(logName, REVERSAL_BUY_LOG_FILE);
            maxRecord = MAX_UNSUCCESS_BUY_TRANS;
            break;
        case TRANS_BUYCHARGE:
            strcpy(logName, REVERSAL_CHARGE_LOG_FILE);
            maxRecord = MAX_UNSUCCESS_CHARGE_TRANS;
            break;
        case TRANS_BILLPAY:
            strcpy(logName, REVERSAL_BILLPAY_LOG_FILE);
            maxRecord = MAX_UNSUCCESS_BILLPAY_TRANS;
            break;
        case TRANS_LOANPAY:
            strcpy(logName, REVERSAL_LOAN_PAY_FILE);
            maxRecord = MAX_UNSUCCESS_LOANPAY_TRANS;
            break;
        default:
            break;
    }

    showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "**APPEND: reversalLogFile");
    retValue2 = appendFixedFileInfo(logName, &reversalInfo, sizeof (reversalTransST), maxRecord);
    if (retValue2 != SUCCESS) {
        showLog(JUST_CASTLES, __FILE__, __LINE__, FATAL, "", "err #8");
        addSystemErrorReport(retValue2, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    //mgh: end of adding reversal Log
 * */
    return TRUE;
}


//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
//HNO_IDENT uncomment
uint8 transactionChargeList(argumentListST* args)
{    
    filesST*        files                                       = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    chargeLogST     transactions[MAX_CHARGE_TRANS_LOG_RECORDS]  = {0};	
    int				transactionNumber                           = 0;            /** charge number with specified date in file */
    int				counter                                     = 0;            /** loop counter */
    uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
    uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
    uint8			fileName[FILE_NAME_LENTGH]                  = {0};
    uint8			activeUser[15 + 1]                          = {0};
    uint8			title[30]                                   = {0};
    uint8			activeUserTitle[15 + 1]                     = {0};
    uint8           chargeTypeStr[16]                           = {0};
    int             chargeType                                  = 0;
    uint8           check                                       = FALSE;
    uint8           sumDailyValues[15 + 1]                      = {0};
    uint8			tempSumValues[15 + 1]                       = {0}; 
    uint8			valueWithComma[20]                          = {0}; 
    uint8           depositID[11 + 1]                           = {0};//MRF_NEW17 //ABS_NEW1
    
    strcpy(fileName, CHARGE_LOG_FILE);
    strcpy(activeUserTitle, "����� ����");

    
    if (!readTransactionListFile(fileName, transactions, sizeof(chargeLogST), MAX_CHARGE_TRANS_LOG_RECORDS, 
    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "****transactionChargeList****");
    
    /** LINE 0 : HEADER */
    strcpy(title, "�����ǘ�� ���� ��ю");

    if (!displayTransactionChargeList(transactions, transactionNumber, requestedNum, "��ю ����", &startDateTime,
    		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
    	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    if (! printReceiptDate(transactions[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;
    
    printDash3();
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {   
        if (check != 0)
        {
           if (!printReceiptDate(transactions[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }
        
        /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
        chargeType = transactions[counter].type;
        
        getChargeTypeStr(chargeType, chargeTypeStr);
#ifdef VX520
		//strcat(chargeTypeStr, "|");//ABS:COMMENT
		if (!printTwoStringFarsi(chargeTypeStr, "��� ��ю:", PRN_NORM))//ABS:CHANGE
			return FALSE;
#else
        strcat(chargeTypeStr, "|");
        if (!printTwoStringFarsi(chargeTypeStr,"|��� ��ю:", PRN_NORM))
            return FALSE;
#endif
    	if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN, 
        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime), depositID))//MRF)NEW17
    		return FALSE;
        
        check = compareDates(&transactions[counter].dateTime, &transactions[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumDailyValues);
        
        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();
        
        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);

            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }
        
        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;
    
    return TRUE;
}
//#else
////MRF
//uint8 transactionChargeList(argumentListST* args)
//{
//    filesST*        files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST		startDateTime				= {0, 0};
//    dateTimeST		endDateTime					= {0, 0};
//    int				transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32			requestedNum				= 0;            /** user requested transaction number, integer form */
//    int				counter                 	= 0;            /** loop counter */
//    uint8			sumValues[15 + 1]       	= {0};          /** sum of listed transactions value */
//    chargeLogST     transactions[MAX_CHARGE_TRANS_LOG_RECORDS]       	= {0};
//    uint8			fileName[FILE_NAME_LENTGH]	= {0};
//    uint8			transType					= TRANS_BUYCHARGE;
//    //mgh messageSpecST   messageSpec;
//    uint8			activeUser[15 + 1]			= {0};
//    uint8			title[30]                   = {0};
//    uint8			activeUserTitle[15 + 1]		= {0};
//    uint8           chargeTypeStr[16]           = {0};
//    int             chargeType                  = 0;
//
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
//
//    strcpy(fileName, CHARGE_LOG_FILE);
//    transType = TRANS_BUYCHARGE;
//    strcpy(activeUserTitle, "����� ����");
//
//
//    if (!readTransactionListFile(fileName, transactions, sizeof(chargeLogST), MAX_CHARGE_TRANS_LOG_RECORDS,
//    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, FATAL, "", "transaction Charge List");
//
//    /** LINE 0 : HEADER */
//    strcpy(title, "��� ��ǘ�� ��ю");
//
//    if (!displayTransactionChargeList(transactions, transactionNumber, requestedNum, title, &startDateTime,
//    		&endDateTime, activeUserTitle, activeUser, sumValues))
//    	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//
//    /** PRINT TRANSACTIONS DETAILS */
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "counter %d", counter);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "requestedNum %d", requestedNum);
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//        /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
//	    	chargeType = transactions[counter].type;
//            getChargeTypeStr(chargeType, chargeTypeStr);
//		    if (!printTwoStringFarsi(chargeTypeStr,"��� ��ю:", PRN_NORM))
//                return FALSE;
//
//    	if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN,
//        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime)))
//    		return FALSE;
//
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif

uint8 displayTransactionChargeList(chargeLogST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	int				counter                 						= 0;            			/** loop counter */
	int             chargeType                  					= 0;			
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]	= {0, 0};			
	uint16  		i                       						= 0;			
	uint8			tempSumValues[15 + 1]   						= {0};      				/** sum of listed transactions value */
	uint8			key 											= KBD_CANCEL;			
	uint8			displayedCount          						= 0;            			/** display counter */
	uint8			displayedSumValues[15 + 1]						= {0};
    uint8           chargeTypeStr[16]           					= {0};
	terminalSpecST  terminalCapability								= getTerminalCapability();
    uint8           depositID[11 + 1]                               = {0};//MRF_NEW17 //ABS_NEW1
   
	     			                   
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
    
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0 ; counter--) 
    {
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumValues);
        
    	if ( i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 10)) //6: header, 4: footer, 10: body 
    	{
            /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
	    	chargeType = transactions[counter].type;
            getChargeTypeStr(chargeType, chargeTypeStr);
		    justifyTwoStringFarsi("", "��� ��ю", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, chargeTypeStr);
	        
	    	fillTransListCommonPartLines(lines, &i, transactions[counter].amount, transactions[counter].PAN,
	    			transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime), depositID); //MRF_NEW17
	    	displayedCount++;
	    	strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    
    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE, chargeTrans);
    
    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}

//#ifdef CASTLES_V5S //IDENT2
////MRF_OPTIMIZE
//HNO_IDENT
uint8 unsuccessChargeTransaction(argumentListST* args)
{    
    filesST*        			files                   				 = (filesST*) (args->argumentName);
    dateTimeST					startDateTime							 = {0, 0};
    dateTimeST					endDateTime								 = {0, 0};
	reversalTransST				reversalInfo[MAX_UNSUCCESS_CHARGE_TRANS] = {0}; //MRF_NEW3
    int							transactionNumber						 = 0;   /** charge number with specified date in file */
	int                         chargeType                  			 = 0;
    int							counter                 				 = 0;   /** loop counter */
	uint32						requestedNum							 = 0;   /** user requested transaction number, integer form */
    uint8						sumValues[15 + 1]       				 = {0}; /** sum of listed transactions value */
    uint8						fileName[FILE_NAME_LENTGH]				 = {0};
    uint8						activeUser[15 + 1]						 = {0};
    uint8						activeUserTitle[15 + 1]					 = {0};
    uint8						title[30]                                = {0};
    uint8						referenceNum[1]							 = {0};
    uint8                       chargeTypeStr[16]           			 = {0};
    uint8                       depositID[11 + 1]                        = {0};//MRF_NEW17 //ABS_NEW1


	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****uunsuccessChargeTransaction****");
    strcpy(fileName, REVERSAL_CHARGE_LOG_FILE);
    
    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_CHARGE_TRANS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    strcpy(title, "�����ǘ�� ������ ��ю");
    if (!displayUnsuccessChargeTransaction(reversalInfo, transactionNumber, requestedNum, "��ю ������", &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
           	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    if (!printReceiptDate(reversalInfo[requestedNum - 1].dateTime.date,"�����"))//MRF_NEW8
       return FALSE;
    
    printDash3(); //MRF_NEW8
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
        chargeType = reversalInfo[counter].chargeType;
        getChargeTypeStr(chargeType, chargeTypeStr);
#ifdef VX520
		//strcat(chargeTypeStr, "|");//ABS:COMMENT
		if (!printTwoStringFarsi(chargeTypeStr, "��� ��ю:", PRN_NORM))//ABS:CHANGE
			return FALSE;
#else
        strcat(chargeTypeStr, "|");
        if (!printTwoStringFarsi(chargeTypeStr,"|��� ��ю:", PRN_NORM))
            return FALSE;
#endif
    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum, 
    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime), depositID))//MRF_NEW17
    		return FALSE;

        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}
//#else
//uint8 unsuccessChargeTransaction(argumentListST* args)
//{
//    filesST*        			files                   	= (filesST*) (args->argumentName);
//
//    dateTimeST					startDateTime				= {0, 0};
//    dateTimeST					endDateTime					= {0, 0};
//    int							transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
//    int							counter                 	= 0;            /** loop counter */
//    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
//    uint8						fileName[FILE_NAME_LENTGH]	= {0};
//    //mgh messageSpecST   		messageSpec;
//    uint8						activeUser[15 + 1]			= {0};
//    uint8						activeUserTitle[15 + 1]		= {0};
//    uint8						title[30]                   = {0};
//    uint8						referenceNum[1]				= {0};
//    reversalTransST				reversalInfo[MAX_UNSUCCESS_CHARGE_TRANS] = {0}; //MRF_NEW3
//    uint8                       chargeTypeStr[16]           = {0};
//    int                         chargeType                  = 0;
//
//    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));
////    memset(lines, 0, sizeof(lines));
//
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "unsuccessBUY");
//    strcpy(fileName, REVERSAL_CHARGE_LOG_FILE);
//
//    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_CHARGE_TRANS,
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "��ю ������");
//    if (!displayUnsuccessChargeTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
//                   		&endDateTime, activeUserTitle, activeUser, sumValues))
//           	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//        /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
//        chargeType = reversalInfo[counter].chargeType;
//        getChargeTypeStr(chargeType, chargeTypeStr);
//        if (!printTwoStringFarsi(chargeTypeStr,"��� ��ю:", PRN_NORM))
//                return FALSE;
//
//    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum,
//    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime)))
//    		return FALSE;
//
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}
//#endif

//MRF
uint8 displayUnsuccessChargeTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 3][32]			= {0, 0};		//HNO:change 400 to2000 /** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};       	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	uint8			referenceNum[1]				= {0};
    uint8           chargeTypeStr[16]           = {0};
    int             chargeType                  = 0;
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumValues);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 10))
    	{
             /** TRANSACTIONS LOG ; LINE 1: CHARGE TYPE */
	    	chargeType = reversalInfo[counter].chargeType;
            getChargeTypeStr(chargeType, chargeTypeStr);
		    justifyTwoStringFarsi("", "��� ��ю", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, chargeTypeStr);
            
    		fillTransListCommonPartLines(lines, &i, reversalInfo[counter].amount, reversalInfo[counter].PAN, 
    				referenceNum, reversalInfo[counter].STAN,
                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
                    &(reversalInfo[counter].SWdateTime), depositID); //MRF_NEW17
    		displayedCount++;
    		strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
	}
 
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);

    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, unsucessChargeTrans);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else 
    	return TRUE;
}

//MRF
uint8 reprintChargeTransaction(argumentListST* args)
{    
    filesST*				files	= (filesST*) (args->argumentName);
    messageSpecST			messageSpec;									
    lastCustomerReceiptST   lastCustomerReceipt;
    cardTracksST            cardTracks;
    cardSpecST              cardSpec;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));
    
    //MRF
    memset(&cardTracks, 0, sizeof(cardTracksST));
    memset(&cardSpec, 0, sizeof(cardSpecST));

    displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
    if(getCardHandle() <= 0)
        enableMSR(); 

    if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
    { 
        return FALSE;
    }    

    if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
    {
        return FALSE;
    }

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_CHARGE_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lastCustomerReceipt.PAN: %s", lastCustomerReceipt.PAN);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "cardSpec.PAN: %s", cardSpec.PAN);
    if (strcmp(lastCustomerReceipt.PAN, cardSpec.PAN) == 0)
    {
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Is Equal");
        structCpy(&lastCustomerReceipt, &messageSpec);
        printTransactionBuyCharge(files, &messageSpec, TRUE, CHARGE_REPRINT);
    }
    else 
    {
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Is Not Equal");
        structCpy(&lastCustomerReceipt, &messageSpec);
        printTransactionBuyCharge(files, &messageSpec, TRUE, REPRINT);
    }
    
	return TRUE;
}


//HNO
uint8 printLoanPayTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)
{
	if (!PrinterAccess(TRUE))
		return FALSE;

	displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

	/** PRINT HEADER */
	if (!prePrintFlags/*[files->id]*/)
		if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
			return FALSE;

	prePrintFlags = 0;

	if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
		return FALSE;

	if(!customer)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
		fileRemove(files->customerReceiptFile);
	}
	/** PRINT FOOTER */
	if (!printTransactionFooter(rePrint, customer, TRUE, files))
		return FALSE;

	return TRUE;
}


//HNO
uint8 printLoanTrakingTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)
{
    uint8   errorCode[10]           = {0};
    uint8   len                     = 0;
    uint8   status[109]             = {0};
    uint8	valueWithComma[30]		= {0};//HNO_IDENT
    uint8   amountPrintBuffer[20]   = {0};

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**print LOAN TrakingTransaction**");

    if (!PrinterAccess(TRUE))
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
            return FALSE;
    
    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
        return FALSE;
    
    //HNO_IDENT
    makeValueWithCommaStr(messageSpec->loanPay.payAmount, valueWithComma);
#ifndef VX520
    sprintf(amountPrintBuffer, "%s ����", valueWithComma);
    
    if(!printTwoStringNumericFarsi(amountPrintBuffer, "����", PRN_NORM))
		return FALSE;
#else
	if (!printThreeStringFarsi(" ����", valueWithComma, "����", PRN_NORM, ALIGN_RIGHT, TRUE))	//ABS:ADD;;961207
		return FALSE;
#endif
//    printStar();

    if ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8)
            || (messageSpec->responseStatus == 16))
    {
        //MRF_NEW8  CHANGE THIS PART SAME ETC
#ifdef CASTLES_V5S //MRF_TTF
        sprintf(errorCode,"��:%s",messageSpec->loanPay.error);
#else
        sprintf(errorCode,"�� %s:",messageSpec->loanPay.error);
#endif
        
        len = strlen(messageSpec->loanPay.status);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "len of status: %d", len);
//HNO_ADD
#if defined(ICT250) || defined(IWL220)
        if ( len < 43)
        {
           if(!printTwoStringFarsi(errorCode, messageSpec->loanPay.status, PRN_NORM))
                return FALSE;
        }

        else
        {
        	if(!printOneStringFarsi("������ ������ ���� ���� � �� 72 ���� ����� ", PRN_NORM, ALIGN_CENTER))
        		return FALSE;
        	if(!printTwoStringFarsi(errorCode, "�����     ",PRN_NORM))
        		return FALSE;
        }
#else
        if ( len < 30)
        {
            if (!printTwoStringFarsi(errorCode, messageSpec->loanPay.status, PRN_NORM_INVERSE))
                return FALSE;
        }
        else
        {
            strcpy(status, messageSpec->loanPay.status);
            strcat(status, "  ");
            strcat(status, errorCode);
            if (!printOneStringFarsi(status, PRN_NORM, ALIGN_CENTER))
                return FALSE;
        }
            
#endif
    }
    else
    {
        if (!printOneStringFarsi("����� ��ǘ�� ����� ���", PRN_NORM, ALIGN_CENTER))
            return FALSE;
    }

    /** PRINT FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files))
        return FALSE;

    return TRUE;
}

//HNO
uint8 transactionLoanPayList(argumentListST* args)
{    
    filesST*        	files                                        = (filesST*) (args->argumentName);
    dateTimeST			startDateTime                                = {0, 0};
    dateTimeST			endDateTime                                  = {0, 0};
    int					transactionNumber                            = 0;            /** charge number with specified date in file */
    uint32				requestedNum                                 = 0;            /** user requested transaction number, integer form */
    int					counter                                      = 0;            /** loop counter */
    uint8				destinationCard[18]                          = {0};//MRF_NEW8
    uint8				nameStr[20]                                  = {0};
    uint8               sumValues[15 + 1]                            = {0}; 
    uint8				fileName[FILE_NAME_LENTGH]                   = {0}; //MRF_NEW3
    uint8				activeUser[15 + 1]                           = {0};
    uint8				activeUserTitle[15 + 1]                      = {0};
    uint8				title[30]                                    = {0};
    uint8               check                                        = FALSE;
    uint8               sumDailyValues[15 + 1]                       = {0};
    uint8               tempSumValues[15 + 1]                        = {0};
    uint8               valueWithComma[20]                           = {0};
    loanPayTransLogST   loanPay[MAX_LOAN_PAY_TRANS_LOG_RECORDS]      = {0}; //HNO: change 200 to 1000
    uint8               depositID[11 + 1]                            = {0};//MRF_NEW17 //ABS_NEW1

    //mgh memset(&messageSpec, 0, sizeof(messageSpecST));

    strcpy(fileName, LOAN_PAY_LOG_FILE);
// mgh_93    strcatUserID(fileName);
    
    if (!readTransactionListFile(fileName, loanPay, sizeof(loanPayTransLogST), MAX_LOAN_PAY_TRANS_LOG_RECORDS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
        
    strcpy(title, "�����ǘ�� ���� ���"); //MGH: change name to lower size

    if (!displayTransactionLoanPayList(loanPay, transactionNumber, requestedNum, "��� ����", &startDateTime,
                		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
        	return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;
    
    if (!printReceiptDate(loanPay[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;

    printDash3();

    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
         {
            if (!printReceiptDate(loanPay[counter].dateTime.date,"�����"))
                return FALSE;
            printDash3();
         }
        
#ifdef VX520  //ABS:CHANGE:961215

		if (!printTwoStringNumericFarsi(loanPay[counter].destinationCardPAN, "���� ���", PRN_NORM))
			return FALSE;
		sprintf(nameStr, "%s %s", loanPay[counter].destinationCardHolderName, loanPay[counter].destinationCardHolderFamily);
		if (!printTwoStringNumericFarsi(nameStr, "�� ���", PRN_NORM))
			return FALSE;

#else
        #ifdef CASTLES_V5S //MRF_TTF
        reversPartString(loanPay[counter].destinationCardPAN);
        #endif

    	/** PRINT card Pan */
        maskCardId(loanPay[counter].destinationCardPAN, destinationCard);

        strcat(loanPay[counter].destinationCardPAN,"|");
	    if (!printTwoStringNumericFarsi(loanPay[counter].destinationCardPAN, "|���� ���", PRN_NORM)) //MRF_NEW8
	        return FALSE;
        
        sprintf(nameStr, "%s %s", loanPay[counter].destinationCardHolderName, loanPay[counter].destinationCardHolderFamily);
        strcat(nameStr, "|");
	    if (!printTwoStringNumericFarsi(nameStr, "|�� ���", PRN_NORM))
	        return FALSE;
#endif

    	if (!printTransactionListCommonPart(loanPay[counter].amount, loanPay[counter].PAN, 
    			loanPay[counter].retrievalReferenceNumber, 0, &(loanPay[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;

        check = compareDates(&loanPay[counter].dateTime, &loanPay[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, loanPay[counter].amount, sumDailyValues);

        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();

        if (check != 0)
         {
             makeValueWithCommaStr(sumDailyValues, valueWithComma);

//HNO_IDENT
             if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                 return FALSE;

             printStar();
             memset(sumDailyValues, 0, sizeof(sumDailyValues));
         }

        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}

uint8 printTransactionSupervisor(messageSpecST* messageSpec, filesST* files)  
{
    uint8               reversalCancelled           = FALSE;
    uint8               customer                    = FALSE;
    int                 amount                      = 0;
    int16               retValue                    = 0;
    uint32              len                         = sizeof(supervisorSpec);
    uint8               valueWithComma[12 + 1]      = {0};
    uint8               activity[100]               = {0};
    uint8   			amountPrintBuffer[20]       = {0};
    supervisorSpec      supervisor;
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "printTransactionSupervisor", "********START**********");
    
    memset(&supervisor, 0 , sizeof(supervisorSpec));
    
    if (!PrinterAccess(TRUE))    
        return reversalCancelled;    

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);    

    /** PRINT HEADER */
    if (!prePrintFlags)
    	if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files)) 
    		return reversalCancelled;  
    
    prePrintFlags = 0;
    printOneStringFarsi("���� �������", PRN_NORM, ALIGN_CENTER);
    
    amount = atoi(messageSpec->amount);
    showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "messageSpec->amount: %s", messageSpec->amount);
    showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "AMOUNT: %d", amount);
    showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "messageSpec->responseStatus: %d", messageSpec->responseStatus);
    
//    if(messageSpec->responseStatus == 0) MRF_95
//    {
        switch (amount)
        {
            case TEST_POS:
                strcpy(activity,"��� �����");
                break;
            case UPDATE_APP:
                strcpy(activity,"�� �������� ��� �����");
                break;
            case UPDATE_GUIDE:
                strcpy(activity,"�� �������� ������");
                break;
            case VISITOR_CASE:
                strcpy(activity,"������ �����");
                break;
            case PERIODIC_VISIT:
                strcpy(activity,"������ ���� ��");
                break;
            case UPDATE_CONTRACT:
                strcpy(activity,"�� �������� �������");
                break;
            case UPDATE_LABLE:
                strcpy(activity,"�� �������� �э��");
                break;
            case CHANGE_TERMINAL:
                strcpy(activity,"����� ������ ����"); //MRF_NEW2
                break;
            case REMOVE_POS:
                strcpy(activity,"��� ���� ������");
                break;
            case INSTALLATION_TRANS:
                strcpy(activity,"���");
                break;
            default:
                strcpy(activity,"������ �������");
                break;
                    
        }
//    }
    
    retValue = readFileInfo(SUPERVISOR_FILE, &supervisor , &len);
    if (retValue != SUCCESS)
    {   
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    else
    {
//        printTwoStringFarsi(supervisor.firstName, "���:", PRN_NORM);
        removePadLeft(supervisor.personnelID, '0');
        printTwoStringNumericFarsi(supervisor.personnelID, "�ρ������", PRN_NORM);
        printTwoStringFarsi(activity,"�����:", PRN_NORM);
        
//        printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT);
//        printOneStringFarsi(activity, PRN_NORM, ALIGN_LEFT);
    }
    
    strcpy(messageSpec->supervisor.activity, activity);
    strcpy(messageSpec->supervisor.personnelID, supervisor.personnelID);
    strcpy(messageSpec->supervisor.codeMeli, supervisor.codeMeli);
    
    /** PRINT COMMON PART */
    if (!printTransactionCommonPart(messageSpec, FALSE, FALSE, files)) 
        return FALSE;     
    
    if (messageSpec->responseStatus == 0)
        if (!PrinterAccess(TRUE))    
            return FALSE;   
#ifdef CASTLES_V5S
    printOneStringFarsi("������ ����", PRN_BIG, ALIGN_CENTER);
#else
    printOneStringFarsi("������ ����", PRN_NORM_INVERSE, ALIGN_CENTER);
#endif
    makeValueWithCommaStr(messageSpec->amount, valueWithComma);
    printOneStringFarsi("����:", PRN_NORM, ALIGN_RIGHT);
    sprintf(amountPrintBuffer, "%s ����", valueWithComma);//HNO_CHANGE in ICT the amount print'1' , if we use one variable
    printOneStringFarsi(amountPrintBuffer, PRN_BIG, ALIGN_CENTER);
    
    /** PRINT TRANSACTION FOOTER */
    printTransactionFooter(FALSE, FALSE, TRUE, files); 
    addSupervisiorReport(messageSpec);
    
    fileRemove(SUPERVISOR_FILE);
    
    return FALSE;
}

void addSupervisiorReport(messageSpecST* messageSpec)  
{
    int16                   retValue						= FAILURE;		/** write in file function return value */
    uint8                   fileName[FILE_NAME_LENTGH]		= {0};
    supervisorTransLogST    transaction;
    
    memset(&transaction, 0, sizeof(supervisorTransLogST));
    
    strcpy(fileName, SUPERVISOR_LOG_FILE);
    
    transaction.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->cardSpec.PAN, transaction.PAN);
    strcpy(transaction.amount, messageSpec->amount); 
    strcpy(transaction.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(transaction.personnelID, messageSpec->supervisor.personnelID);
    strcpy(transaction.codeMeli, messageSpec->supervisor.codeMeli);
    strcpy(transaction.activity, messageSpec->supervisor.activity);
    
    retValue = appendFixedFileInfo(fileName, &transaction, 
                                     sizeof(supervisorTransLogST), MAX_SUPERVISOR_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}


uint8 displayTransactionSupervisorList(supervisorTransLogST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[700 + 3][32]			= {0, 0};		/** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	     			                   
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);

    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "Request: %d", requestedNum);
    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "transactoins: %d", transactionNumber);
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0 ; counter--) 
    {
    	//mgh: adding two string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if ( i < (100 - 6 - 4 - 8)) //6: header, 4: footer, 8: body 
    	{
            justifyTwoStringFarsi("", "�����", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, transactions[counter].activity);
            removePadLeft(transactions[counter].personnelID, '0');
		    justifyTwoStringFarsi("", "�� �������", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, transactions[counter].personnelID);
		            
//	    	justifyTwoStringFarsi("", "�� ���", lines[i++], PRN_NORM);
//	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, transactions[counter].codeMeli);
            
	    	fillTransListCommonPartLines(lines, &i, transactions[counter].amount, transactions[counter].PAN,
	    			transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime), depositID); //MRF_NEW17
	    	displayedCount++;
	    	strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
     	key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE,supervisorActivitiesPic);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}


/**
 * display & print buy transaction list report.
 * @see     searchDateRangeInFixedFile()
 * @see     readFixedFileInfoArray()
 * @see     displayScrollable()
 * @see     printLogo()
 * @see     printReceiptDateTime()
 * @see     show_one_log()
 */
uint8 transactionSupervisorList(argumentListST* args)
{    
	filesST*                files                   						= (filesST*) (args->argumentName);
						
    dateTimeST              startDateTime									= {0, 0};
    dateTimeST              endDateTime										= {0, 0};
    int                     transactionNumber								= 0;            /** charge number with specified date in file */
    uint32                  requestedNum									= 0;            /** user requested transaction number, integer form */
    int                     counter                 						= 0;            /** loop counter */
    uint8                   sumValues[15 + 1]       						= {0};          /** sum of listed transactions value */
    supervisorTransLogST    transactions[MAX_SUPERVISOR_TRANS_LOG_RECORDS]  = {0};//#MRF_970924
    uint8                   fileName[FILE_NAME_LENTGH]						= {0};
    uint8                   transType										= TRANS_BUY;
    uint8                   activeUser[15 + 1]								= {0};
    uint8                   title[30]                   					= {0};
    uint8                   activeUserTitle[15 + 1]							= {0};
    uint8                   depositID[11 + 1]           					= {0};
    
    
    strcpy(fileName, SUPERVISOR_LOG_FILE);
    strcpy(activeUserTitle, "����� ����");

    if (!readTransactionListFile(fileName, transactions, sizeof(supervisorTransLogST), MAX_SUPERVISOR_TRANS_LOG_RECORDS, 
    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    showLog(JUST_CASTLES, __FILE__, __LINE__, FATAL, "", "transaction Buy List 5");
    
    strcpy(title, "������ �������");//ABS:CHANGE

    if (!displayTransactionSupervisorList(transactions, transactionNumber, requestedNum, title, &startDateTime,
    		&endDateTime, activeUserTitle, activeUser, sumValues))
    	return FALSE;

    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "display list");
    
    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
#ifndef VX520
        strcat(transactions[counter].activity, "|");//MRF_NEW8
	    if (!printTwoStringNumericFarsi(transactions[counter].activity, "|�����", PRN_NORM))//MRF_NEW8
	        return FALSE;
//        printTwoStringFarsi(transactions[counter].activity,"�����:",PRN_NORM); --MRF_NEW8
        removePadLeft(transactions[counter].personnelID, '0');
        strcat(transactions[counter].personnelID,"|"); //MRF_NEW8
        if (!printTwoStringNumericFarsi(transactions[counter].personnelID, "|�� �������", PRN_NORM))//MRF_NEW8
            return FALSE;
//        printTwoStringNumericFarsi(transactions[counter].personnelID,"�� �������",PRN_NORM); --MRF_NEW8
//        printTwoStringNumericFarsi(transactions[counter].codeMeli,"�� ���",PRN_NORM);
#else
		printTwoStringFarsi(transactions[counter].activity, "�����:", PRN_NORM);
		removePadLeft(transactions[counter].personnelID, '0');
		printTwoStringNumericFarsi(transactions[counter].personnelID, "�� �������", PRN_NORM);

#endif
    	if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN, 
        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;
        
        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }
    
    /** PRINT EMPTY LINES */
    printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}

uint8 merchantChargeReport(argumentListST* args)
{    
    filesST*		files                   				= (filesST*) (args->argumentName);
    uint32          userDateJalali          				= 0;				/** entered date by user (jalali) */
    int             reportYear              				= 0;				/** report year */
    int             reportMonth             				= 0;				/** report month */
    int             reportDay               				= 0;				/** report day */
    int             transactionNumber       				= 0;				/** transactions number with specified date in file */
    uint8           transValueSum[15 + 1]   				= {0};				/** sum of transactions value */
    uint8           tempSumValues[15 + 1]   				= {0};
    int             counter                 				= 0;				/** loop counter */
    uint8           lines[15][32]           				= {0};				/** display lines */
    uint8           valueWithComma[20]      				= {0};				/** sum value with comma, string form */
    chargeLogST     transactions[MAX_CHARGE_TRANS_LOG_RECORDS] = {0}; 
    uint8           dateValueStr[11]        				= {0};
    uint8           transNum[10]            				= {0};
    uint8           numStr[7]               				= {0};
    uint16          i                                       = 0;
    uint8           key                                     = KBD_CANCEL;
	uint8           separatedStr[10][SEPERATED_STR_SIZE] = { 0, 0 };//ABS:CHANGE	//HNO_IDENT8  change 250 to 50 because it crash
    uint8           wordWrapIndex           				= 0; 
    uint8           loop                    				= 0; 
    uint8           amountPrintBuffer[20]   				= {0}; 
    uint8           fileName[FILE_NAME_LENTGH]				= {0};
    uint8			transType								= TRANS_BUYCHARGE;
    uint8			activeUser[15 + 1]						= {0};
    uint8			activeUserTitle[15 + 1]					= {0};
    merchantSpecST	merchantSpec;
    dateTimeST		dateTime;
    uint8           chargeType[15 + 1]                      = {0};
    uint8           buffer[12 + 1]                          = {0};
    dateTimeST		startDateTime                           = {0, 0};
    dateTimeST		endDateTime                             = {0, 0};
    uint32			requestedNum                            = 0;
    uint8			title[30]                               = {0};
    
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    memset(&dateTime, 0, sizeof(dateTimeST));
    memset(lines, 0, sizeof(lines));

    strcpy(fileName, CHARGE_LOG_FILE);
    strcpy(activeUserTitle, "����� ����");
    strcpy(title, "����� ����� ��ю");

    if (!readTransactionListFile(fileName, transactions, sizeof(chargeLogST), MAX_CHARGE_TRANS_LOG_RECORDS, 
                        &startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    return FALSE;
   
    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    return FALSE;

    /** ACTIVE USER */
    if (strlen(activeUser) > 0)
    {
	    justifyTwoStringFarsi("", activeUserTitle, lines[i++], PRN_NORM);
	    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, activeUser);
    }

    /** LINE 2 : TRANSACTION NUMBER */
    sprintf(transNum, "%d", transactionNumber);
    justifyTwoStringFarsi(transNum, "�����", lines[i++], PRN_NORM);
     
    if (!PrinterAccess(TRUE))    
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);
    
    //mgh: adding 2string...
    for (counter = 0; counter < transactionNumber; counter++)
    {
        strcpy(tempSumValues, transValueSum);
        sumStringNumbers(tempSumValues, transactions[counter].amount, transValueSum);
        makeValueWithCommaStr(transValueSum, valueWithComma);
        
        if  (transactions[counter].type == MULTI_IRANCELL)
            {
                showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "MULTI_IRANCELL");
                if (strcmp(transactions[counter].amount, "10000") == 0)
                    chargeType[1]++;
                    else if (strcmp(transactions[counter].amount, "20000") == 0)
                        chargeType[2]++;
                        else if (strcmp(transactions[counter].amount, "50000") == 0)
                            chargeType[3]++;
                             else if (strcmp(transactions[counter].amount, "100000") == 0)
                                 chargeType[4]++;
                                else if (strcmp(transactions[counter].amount, "200000") == 0)
                                    chargeType[5]++;
            }
        else if (transactions[counter].type == MULTI_MCI)
            {
                showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "MULTI_MCI");
                 if (strcmp(transactions[counter].amount, "10000") == 0)
                     chargeType[6]++;
                    else if (strcmp(transactions[counter].amount, "20000") == 0)
                        chargeType[7]++;
                        else if (strcmp(transactions[counter].amount, "50000") == 0)
                            chargeType[8]++;
                             else if (strcmp(transactions[counter].amount, "100000") == 0)
                                 chargeType[9]++;
                                else if (strcmp(transactions[counter].amount, "200000") == 0)
                                    chargeType[10]++;
            }
        else if (transactions[counter].type == MULTI_RIGHTEL)       
            {
                showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "MULTI_RIGHTEL");
                if (strcmp(transactions[counter].amount, "20000") == 0)
                    chargeType[11]++;
                    else if (strcmp(transactions[counter].amount, "50000") == 0)
                        chargeType[12]++;
                        else if (strcmp(transactions[counter].amount, "100000") == 0)
                            chargeType[13]++;
                             else if (strcmp(transactions[counter].amount, "200000") == 0)
                                 chargeType[14]++;
                                else if (strcmp(transactions[counter].amount, "500000") == 0)
                                    chargeType[15]++;
            }
    }
    
    /*Irancell*/
    if (!printOneStringFarsi("�������", PRN_NORM, ALIGN_CENTER))
        return FALSE;
//    sprintf(buffer,"%d",chargeType[1]);   //-MRF_970807
//    if (!printTwoStringNumericFarsi(buffer, "10,000 �����", PRN_NORM))
//        return FALSE;
    sprintf(buffer,"%d",chargeType[2]);
    if (!printTwoStringNumericFarsi(buffer, "20,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[3]);
    if (!printTwoStringNumericFarsi(buffer, "50,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[4]);
    if (!printTwoStringNumericFarsi(buffer, "100,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[5]);
    if (!printTwoStringNumericFarsi(buffer, "200,000 �����", PRN_NORM))
        return FALSE;
    
    /*Hamrahe Aval*/
    if (!printOneStringFarsi("����� ���", PRN_NORM, ALIGN_CENTER))
        return FALSE;
    sprintf(buffer,"%d",chargeType[6]);
    if (!printTwoStringNumericFarsi(buffer, "10,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[7]);
    if (!printTwoStringNumericFarsi(buffer, "20,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[8]);
    if (!printTwoStringNumericFarsi(buffer, "50,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[9]);
    if (!printTwoStringNumericFarsi(buffer, "100,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[10]);
    if (!printTwoStringNumericFarsi(buffer, "200,000 �����", PRN_NORM))
        return FALSE;
    
    /*Rightel*/
    if (!printOneStringFarsi("�����", PRN_NORM, ALIGN_CENTER))
        return FALSE;
    sprintf(buffer,"%d",chargeType[11]);
    if (!printTwoStringNumericFarsi(buffer, "20,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[12]);
    if (!printTwoStringNumericFarsi(buffer, "50,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[13]);
    if (!printTwoStringNumericFarsi(buffer, "100,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[14]);
    if (!printTwoStringNumericFarsi(buffer, "200,000 �����", PRN_NORM))
        return FALSE;
    sprintf(buffer,"%d",chargeType[15]);
    if (!printTwoStringNumericFarsi(buffer, "500,000 �����", PRN_NORM))
        return FALSE;
    
    /** PRINT DASHES */
    if (!printStar())
        return FALSE;
    
    /** PRINT TRANSACTION NUMBER */
    sprintf(numStr, "%d", transactionNumber);
    if (!printTwoStringNumericFarsi(numStr, "�����", PRN_NORM))
            return FALSE;
    /** PRINT TRANSACTION TOTAL AMOUNT */
    if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT - strlen(" ����"))
    {
        if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT)
        {
            loop = wordWrapEnglish(valueWithComma, separatedStr, DISPLAY_CHARACTER_COUNT);
            for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
            {
                sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, separatedStr[wordWrapIndex]);
            }
        }
        else
        {
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma);
        }
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, "����");
    }
    else
    {
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");
    }
    
    /** PRINT TRANSACTIONS VALUE */
    if (strlen(valueWithComma) + strlen(" ����") + strlen("�����:") <= getPrinterCharacterCount())
    {
        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "First");    
         if (!printThreeStringFarsi(" ����", valueWithComma, "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
            return FALSE;
    }
    else if (strlen(valueWithComma) + strlen(" ����") <= getPrinterCharacterCount())
    {
        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
            return FALSE;

        sprintf(amountPrintBuffer, "%s ����", valueWithComma);
        if (!printOneStringFarsi(amountPrintBuffer, PRN_NORM, ALIGN_CENTER))
            return FALSE;
    }
    else
    {
        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
            return FALSE;
        if (!printOneStringFarsi(valueWithComma, PRN_NORM, ALIGN_CENTER))
            return FALSE;       
        if (!printOneStringFarsi("����", PRN_NORM, ALIGN_LEFT))
            return FALSE;       
    }

    /** PRINT EMPTY LINES */
    printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}


uint8 merchantLOANReport(argumentListST* args)
{
	filesST*        files                                       = (filesST*) (args->argumentName);
	dateTimeST		startDateTime                               = {0, 0};
	dateTimeST		endDateTime                                 = {0, 0};
	loanPayTransLogST   transactions[MAX_LOAN_PAY_TRANS_LOG_RECORDS] 	= {0};
	int				transactionNumber                           = 0;            /** charge number with specified date in file */
	int				counter                                     = 0;            /** loop counter */
	uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
	uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
	uint8			fileName[FILE_NAME_LENTGH]                  = {0};
	uint8			activeUser[15 + 1]                          = {0};
	uint8			title[30]                                   = {0};
	uint8			activeUserTitle[15 + 1]                     = {0};
	uint8           check                                       = FALSE;
	uint8           sumDailyValues[15 + 1]                      = {0};
	uint8			tempSumValues[15 + 1]                       = {0};
	uint8			valueWithComma[20]                          = {0};
	uint8           transNum[5]                                 = {0};

	strcpy(fileName, LOAN_PAY_LOG_FILE);

	strcpy(title, "����� ����� ���");

	if (!readTransactionListFile(fileName, transactions, sizeof(loanPayTransLogST), MAX_LOAN_PAY_TRANS_LOG_RECORDS,
							&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
		return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;

	for (counter = requestedNum - 1; counter >= 0; counter--)
		sumStringNumbers(sumDailyValues, transactions[counter].amount, sumDailyValues);

	makeValueWithCommaStr(sumDailyValues, valueWithComma);

	/** PRINT TRANSACTIONS NUMBER IN DATES */
	sprintf(transNum, "%d", transactionNumber);
	if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
		return FALSE;

	//HNO_IDENT remove #ifdef
	if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
		return FALSE;

	printStar();
	printBlankLines(BLANK_LINES_COUNT);//HNO_IDENT

	return TRUE;

}

//MRF_ETC
uint8 printETCTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)  
{ 

    if (!PrinterAccess(TRUE))    
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
            return FALSE;
    
    prePrintFlags = 0;

    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
        return FALSE;
   
    if(!customer)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);
    }
    
    /** PRINT FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files)) 
        return FALSE;
    
    return TRUE;
}

//MRF_ETC
void addETCReport(messageSpecST* messageSpec)  
{
    int16          		retValue                    = FAILURE;		/** write in file function return value */
    uint8				fileName[FILE_NAME_LENTGH]	= {0}; //MRF_NEW3
    ETCTransLogST       ETC;
    
    memset(&ETC, 0, sizeof(ETCTransLogST));
    strcpy(fileName, ETC_LOG_FILE);
    
    ETC.dateTime = messageSpec->dateTime;
    strcpy(ETC.amount, messageSpec->ETC.amount); 
    maskCardId(messageSpec->cardSpec.PAN, ETC.PAN);
    strcpy(ETC.cardHolderName, messageSpec->ETC.cardHolderName);
    strcpy(ETC.cardHolderFamily, messageSpec->ETC.cardHolderFamily);
//    strcpy(ETC.balance, messageSpec->ETC.balance); 
//    strcpy(ETC.cardHolderID, messageSpec->ETC.cardHolderID); //HNO_COMMENT
    strcpy(ETC.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(ETC.serialETC, messageSpec->ETC.serialETC);
   
    retValue = appendFixedFileInfo(fileName, &ETC, 
                                     sizeof(ETCTransLogST), MAX_ETC_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

//MRF_ETC
uint8 reprintETCTransaction(argumentListST* args)
{    
    filesST*                        files	= (filesST*) (args->argumentName);
    messageSpecST                   messageSpec;
    lastCustomerReceiptST           lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_ETC_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;
    
    structCpy(&lastCustomerReceipt, &messageSpec);
    printETCTransaction(files, &messageSpec, TRUE, TRUE);
    
    return TRUE;
}

//MRF_ETC
//uint8 unsuccessETCTransaction(argumentListST* args)
//{    
//    filesST*        			files                   	= (filesST*) (args->argumentName);
//    dateTimeST					startDateTime				= {0, 0};
//    dateTimeST					endDateTime					= {0, 0};
//    int							transactionNumber			= 0;            /** charge number with specified date in file */
//    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
//    int							counter                 	= 0;            /** loop counter */
//    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
//    uint8						fileName[FILE_NAME_LENTGH]	= {0};
//    uint8						activeUser[15 + 1]			= {0};
//    uint8						activeUserTitle[15 + 1]		= {0};
//    uint8						title[30]                   = {0};
//    uint8						referenceNum[1]				= {0};
//    uint8                       serialETC[13 + 1]         = {0};
//    uint8                       nameStr[20]                 = {0};
//    reversalTransST				reversalInfo[50]   			= {0};
//    
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "UNSUCCESS ETC");
//    
//    strcpy(fileName, REVERSAL_ETC_FILE);
//    
//    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_ETC_TRANS, 
//        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
//    	return FALSE;
//
//    strcpy(title, "�э�� ����� ������");
//    
//    if (!displayunsuccessETCTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
//                   		&endDateTime, activeUserTitle, activeUser, sumValues))
//           	return FALSE;
//
//    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
//    	return FALSE;
//    
//    /** PRINT TRANSACTIONS DETAILS */
//    for (counter = requestedNum - 1; counter >= 0; counter--)
//    {
//        /** PRINT SERIAL ETC */
//	    if (!printTwoStringNumericFarsi(reversalInfo[counter].ETC.serialETC, "����� �э��", PRN_NORM))
//	        return FALSE;
//	           
////        if (!printOneStringFarsi("�� ���:", PRN_NORM, ALIGN_RIGHT))
////            return FALSE;
//        
//        sprintf(nameStr, "%s %s", reversalInfo[counter].ETC.cardHolderName, reversalInfo[counter].ETC.cardHolderFamily);
//        printTwoStringFarsi(nameStr, "�� ���:", PRN_NORM);
//        
//    	if (!printTransactionListCommonPart(reversalInfo[counter].ETC.amount, reversalInfo[counter].PAN, referenceNum, 
//    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime)))
//    		return FALSE;
//
//        
//        if (cancelFunction())
//            return FALSE;
//
//        if (!printDash())
//            return FALSE;
//    }
//
//    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
//    	return FALSE;
//
//    return TRUE;
//}

//uint8 displayunsuccessETCTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
//		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
//{
//	uint8               lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};		//HNO:change 400 to 2000 and 3 to 4 for footer /** 3: fot the last three lines */
//	uint16              i                       	= 0;
//	uint8               tempSumValues[15 + 1]   	= {0};       	/** sum of listed transactions value */
//	int                 counter                 	= 0;            /** loop counter */
//	uint8               key 						= KBD_CANCEL;
//	uint8               displayedCount          	= 0;            /** display counter */
//	uint8               displayedSumValues[15 + 1]	= {0};
//	uint8               referenceNum[1]				= {0};
//	uint8               serialETC[13 + 1]           = {0};
//	uint8               nameStr[20]                 = {0};
//    uint8               familyStr[20]               = {0};
//	terminalSpecST  terminalCapability			= getTerminalCapability();
//	
//    
//	memset(lines, 0, sizeof(lines));
//	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
//	    
//    for (counter = requestedNum - 1; counter >= 0; counter--) 
//    {
//        //mgh: adding 2string...
//        strcpy(tempSumValues, sumValues);
//        sumStringNumbers(tempSumValues, reversalInfo[counter].ETC.amount, sumValues);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
//        
//    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 12))//6 for header,4 for footer and 12 for body
//    	{
//	    	/** TRANSACTIONS LOG ; LINE 1: TRANSACTION ETC */
//		    justifyTwoStringFarsi("", "����� �э��", lines[i++], PRN_NORM);
//	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, reversalInfo[counter].ETC.serialETC);
//		            
//	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION card holder name */
//	        memcpy(nameStr, reversalInfo[counter].ETC.cardHolderName, strlen(reversalInfo[counter].ETC.cardHolderName));
//            memcpy(familyStr, reversalInfo[counter].ETC.cardHolderFamily, strlen(reversalInfo[counter].ETC.cardHolderFamily));
//            strcat(nameStr," ");
//            strcat(nameStr,familyStr);
//	        
//	    	justifyTwoStringFarsi("", "�� ���", lines[i++], PRN_NORM);
//	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, nameStr);
//            memset(nameStr,0,sizeof(nameStr));
//            
//    		fillTransListCommonPartLines(lines, &i, reversalInfo[counter].loanPay.amount, reversalInfo[counter].PAN, 
//    				referenceNum, reversalInfo[counter].STAN, 
//                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
//                    &(reversalInfo[counter].SWdateTime), transactions[counter].depositID); //MRF_NEW17
//    		displayedCount++;
//    		strcpy(displayedSumValues, sumValues);
//    	}
//    }
//    
//    if (requestedNum != displayedCount) //displayed transactions are less than requested
//    {
//    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
//		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
//		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
//	}
// 
//    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
//
//    if (!terminalCapability.graphicCapability) //MRF_IDENT
//    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
//	else
//		key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,unsuccessETC);
//
//    if (key != KBD_ENTER && key != KBD_CONFIRM)
//        return FALSE;
//    else 
//    	return TRUE;
//}

//MRF_ETC
uint8 transactionETCList(argumentListST* args)
{    
    filesST*        	files                               = (filesST*) (args->argumentName);
    dateTimeST			startDateTime                       = {0, 0};
    dateTimeST			endDateTime                         = {0, 0};
    int					transactionNumber                   = 0;            /** charge number with specified date in file */
    uint32				requestedNum                        = 0;            /** user requested transaction number, integer form */
    int					counter                             = 0;            /** loop counter */
    uint8				serialETC[13 + 1]                   = {0};
    uint8				nameStr[20]                         = {0};
    uint8               sumValues[15 + 1]                   = {0}; 
    uint8				fileName[FILE_NAME_LENTGH]          = {0};          //MEF_NEW3
    uint8				activeUser[15 + 1]                  = {0};
    uint8				activeUserTitle[15 + 1]             = {0};
    uint8				title[30]                           = {0};
    uint8           	sumDailyValues[15 + 1]              = {0};
    uint8				tempSumValues[15 + 1]               = {0};
    uint8				valueWithComma[20]                  = {0};
    uint8           	check                               = FALSE;
    ETCTransLogST       ETC[MAX_ETC_TRANS_LOG_RECORDS]      = {0}; 
    uint8               depositID[11 + 1]                   = {0};//MRF_NEW17 //ABS_NEW1


    strcpy(fileName, ETC_LOG_FILE);
    
    if (!readTransactionListFile(fileName, ETC, sizeof(ETCTransLogST), MAX_ETC_TRANS_LOG_RECORDS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
        
    strcpy(title, "�����ǘ�� ���� �����"); 

    if (!displayTransactionETCList(ETC, transactionNumber, requestedNum, "����� ����", &startDateTime,
                		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
        	return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;
    
    if (! printReceiptDate(ETC[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;

    printDash3();

    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(ETC[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }
        /** PRINT SERIAL ETC */
#ifdef VX520			//ABS:CHANGE
		if (!printTwoStringNumericFarsi(ETC[counter].serialETC, "����� �э��", PRN_NORM))
			return FALSE;
		sprintf(nameStr, "%s %s", ETC[counter].cardHolderName, ETC[counter].cardHolderFamily);
		printTwoStringFarsi(nameStr, "�� ���:", PRN_NORM); //MRF_IDENT2

#else
        strcat(ETC[counter].serialETC, "|");
	    if (!printTwoStringNumericFarsi(ETC[counter].serialETC, "|����� �э��", PRN_NORM))
	        return FALSE;
        
        sprintf(nameStr, "%s %s", ETC[counter].cardHolderName, ETC[counter].cardHolderFamily);
        strcat(nameStr, "|"); //MRF_IDENT2
        printTwoStringFarsi(nameStr, "|�� ���:", PRN_NORM); //MRF_IDENT2
#endif      
    	if (!printTransactionListCommonPart(ETC[counter].amount, ETC[counter].PAN, 
    			ETC[counter].retrievalReferenceNumber, 0, &(ETC[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;
    	
        check = compareDates(&ETC[counter].dateTime, &ETC[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, ETC[counter].amount, sumDailyValues);

        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();

        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);
            //HNO_IDENT remove #ifdef
            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }

        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}

//MRF_ETC
uint8 displayTransactionETCList(ETCTransLogST* ETC, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]	= {0, 0};	
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};                      /** sum of listed transactions value */
	int				counter                 	= 0;                        /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;                        /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	uint8			destinationCard[16]         = {0};
	uint8			nameStr[20]                 = {0};
    uint8			familyStr[20]               = {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        /* Adding 2string */
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, ETC[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);

        if (i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 12)) //6: header, 4: footer, 12: body
    	{
	    	/** TRANSACTIONS LOG ; LINE 1: TRANSACTION ETC */
            /* SERIAL ETC*/
		    justifyTwoStringFarsi("", "����� �э��", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, ETC[counter].serialETC);
		            
	        /** TRANSACTIONS LOG ; LINE 2: TRANSACTION card holder name */
	        memcpy(nameStr, ETC[counter].cardHolderName, strlen(ETC[counter].cardHolderName));
            memcpy(familyStr, ETC[counter].cardHolderFamily, strlen(ETC[counter].cardHolderFamily));
            strcat(nameStr," ");
            strcat(nameStr,familyStr);
	        
	    	justifyTwoStringFarsi("", "�� ���", lines[i++], PRN_NORM);
	        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, nameStr);
            memset(nameStr,0,sizeof(nameStr));
	
	        fillTransListCommonPartLines(lines, &i, ETC[counter].amount, ETC[counter].PAN,
	        		ETC[counter].retrievalReferenceNumber, 0, 
                    &(ETC[counter].dateTime), depositID); //MRF_NEW17
	        
	        displayedCount++;
	        strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, successETC);

    if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;
	else
		return TRUE;
}

//MRF_ETC
uint8 merchantETCReport(argumentListST* args)//HNO_IDENT
{    
    filesST*		files                   				= (filesST*) (args->argumentName);
    int             transactionNumber       				= 0;				/** transactions number with specified date in file */
    int             counter                 				= 0;				/** loop counter */
    uint8           valueWithComma[20]      				= {0};				/** sum value with comma, string form */
    uint8           transNum[10]            				= {0};
    uint8           fileName[FILE_NAME_LENTGH]				= {0};
    uint32			requestedNum                            = 0;
    uint8			activeUser[15 + 1]						= {0};
    uint8			activeUserTitle[15 + 1]					= {0};
    uint8           sumDailyValues[15 + 1]                  = {0};
    uint8			title[30]                               = {0};

    dateTimeST		dateTime;
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    ETCTransLogST   transactions[MAX_ETC_TRANS_LOG_RECORDS] 	= {0};

    memset(&dateTime, 0, sizeof(dateTimeST));

    strcpy(fileName, ETC_LOG_FILE);
    strcpy(activeUserTitle, "����� ����");
    strcpy(title, "����� ����� �����");
    
    if (!readTransactionListFile(fileName, transactions, sizeof(ETCTransLogST), MAX_ETC_TRANS_LOG_RECORDS,
                        &startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    return FALSE;
    
    for (counter = requestedNum - 1; counter >= 0; counter--)
        sumStringNumbers(sumDailyValues, transactions[counter].amount, sumDailyValues);

    makeValueWithCommaStr(sumDailyValues, valueWithComma);

    /** PRINT TRANSACTIONS NUMBER IN DATES */
    sprintf(transNum, "%d", transactionNumber);
    if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
        return FALSE;
//HNO_IDENT remove #ifdef
    if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
        return FALSE;
    
    printStar();
    printBlankLines(BLANK_LINES_COUNT);//HNO_IDENT

    return TRUE;
}

//*************************  Buy Charge Topup  ***************************
//MRF_TOPUP
uint8 printTransactionBuyChargeTopup(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)                           
{
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "printTransactionBuyChargeTopup", "********START**********");
    
    if (!PrinterAccess(TRUE))    
        return FALSE;    

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);  
    
    /** PRINT HEADER */
    if (!prePrintFlags)
    	if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files)) 
    		return FALSE;  
    
    prePrintFlags = 0;
    
    /** PRINT COMMON PART */
    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files)) 
        return FALSE;
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "responseStatus: %d", messageSpec->responseStatus);
    if ((customer && !rePrint) && ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8) 
            || (messageSpec->responseStatus == 16)))
    { 
//   --MRF_NEW     if (!PrinterAccess(TRUE))    
//            return FALSE;   
        
        makeTransPrintDataLog(files, messageSpec);
    	addTopupReport(messageSpec);
    }
    
    if(!customer)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);
    }
    
    /** PRINT TRANSACTION FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files)) 
        return FALSE; // this true because print is complete

    if (!rePrint)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile1");
        fileRemove(files->reversalReceiptFile);        
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "printTransactionBuy", "********END***********");
    return TRUE;
}

//MRF_TOPUP
void addTopupReport(messageSpecST* messageSpec)  
{
    int16               retValue						= FAILURE;		/** write in file function return value */
    uint8               fileName[FILE_NAME_LENTGH]		= {0};
    uint8               oldFileName[FILE_NAME_LENTGH]	= {0};
    int32               shiftNO							= 0;
    uint8               shiftNOStr[8 + 1]				= {0};
    TopupTransLogST     topup;
    
    memset(&topup, 0, sizeof(TopupTransLogST));
    
    strcpy(fileName, TOPUP_LOG_FILE);
    
    topup.dateTime = messageSpec->dateTime;
    strcpy(topup.amount, messageSpec->amount);
    maskCardId(messageSpec->cardSpec.PAN, topup.PAN);
    strcpy(topup.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    topup.type = messageSpec->topup.type;
    topup.operatorType = messageSpec->topup.operatorType;
    strcpy(topup.mobileNo, messageSpec->topup.mobileNo);
    
    retValue = appendFixedFileInfo(fileName, &topup, 
                                     sizeof(TopupTransLogST), MAX_TOPUP_TRANS_LOG_RECORDS);    
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

//MRF_ETC
uint8 reprintTopupTransaction(argumentListST* args)
{    
    filesST*                        files	= (filesST*) (args->argumentName);
    messageSpecST                   messageSpec;
    lastCustomerReceiptST           lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_TOPUP_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;
    
    structCpy(&lastCustomerReceipt, &messageSpec);
    printTransactionBuyChargeTopup(files, &messageSpec, TRUE, TRUE);
    
    return TRUE;
}

//MRF_ETC
uint8 unsuccessTopupTransaction(argumentListST* args)
{    
    filesST*        			files                   	= (filesST*) (args->argumentName);
    dateTimeST					startDateTime				= {0, 0};
    dateTimeST					endDateTime					= {0, 0};
    int							transactionNumber			= 0;            /** charge number with specified date in file */
    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
    int							counter                 	= 0;            /** loop counter */
    uint8						sumValues[15 + 1]       	= {0};     /** sum of listed transactions value */
    uint8						fileName[FILE_NAME_LENTGH]	= {0};
    uint8						activeUser[15 + 1]			= {0};
    uint8						activeUserTitle[15 + 1]		= {0};
    uint8						title[30]                   = {0};
    uint8						referenceNum[1]				= {0};
    uint8                       serialETC[13 + 1]           = {0};
    uint8                       nameStr[20]                 = {0};
    reversalTransST				reversalInfo[50]   			= {0};
    uint8                       depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "UNSUCCESS ETC");
    
    strcpy(fileName, REVERSAL_ETC_FILE);
    
    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_TOPUP_TRANS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    strcpy(title, "�����ǘ�� ������ ��ю ������");
    
    if (!displayunsuccessTopupTransaction(reversalInfo, transactionNumber, requestedNum, title, &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser, sumValues))
           	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
                       
        if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum, 
    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime), depositID))//MRF_NEW17
    		return FALSE;       

        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}

//MRF_ETC
uint8 printETCTrakingTransaction(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)  
{   
    uint8   errorCode[20]       	= {0};
    uint8   len                 	= 0;
    uint8   status[220]          	= {0};
    uint8	valueWithComma[30]		= {0};//HNO_IDENT
    uint8   amountPrintBuffer[20]   = {0};
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**printETCTrakingTransaction**");
    
    if (!PrinterAccess(TRUE))    
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
            return FALSE;

    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
        return FALSE;
    
    //HNO_IDENT
    makeValueWithCommaStr(messageSpec->ETC.amount, valueWithComma);
#ifndef VX520
    sprintf(amountPrintBuffer, "%s ����", valueWithComma);
    if(!printTwoStringNumericFarsi(amountPrintBuffer, "����", PRN_NORM))
		return FALSE;
#else
	if (!printThreeStringFarsi(" ����", valueWithComma, "����", PRN_NORM, ALIGN_RIGHT, TRUE))	//ABS:ADD;;961207
		return FALSE;
#endif
//    printStar();
     
    if ((messageSpec->responseStatus == SUCCESS) 
            || (messageSpec->responseStatus == 8) 
            || (messageSpec->responseStatus == 16))
    {
#ifdef CASTLES_V5S //MRF_TTF
        sprintf(errorCode,"��:%s",messageSpec->ETC.error);
#else
        sprintf(errorCode,"�� %s:",messageSpec->ETC.error);
#endif
        
        len = strlen(messageSpec->ETC.status);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "len of status: %d", len);
//HNO_ADD
#if defined(ICT250) || defined(IWL220)
        if ( len < 43)
        {
           if(!printTwoStringFarsi(errorCode, messageSpec->ETC.status, PRN_NORM))
                return FALSE;
        }

        else
        {
        	if(!printOneStringFarsi("������ ������ ���� ���� � �� 72 ���� ����� ", PRN_NORM, ALIGN_CENTER))
        		return FALSE;
        	if(!printTwoStringFarsi(errorCode, "�����     ",PRN_NORM))
        		return FALSE;
        }
#else
        if ( len < 30)
        {
            if (!printTwoStringFarsi(errorCode, messageSpec->ETC.status, PRN_NORM_INVERSE))
                return FALSE;
        }
        else
        {
            strcpy(status, messageSpec->ETC.status);
            strcat(status, "  ");
            strcat(status, errorCode);
            if (!printOneStringFarsi(status, PRN_NORM, ALIGN_CENTER))
                return FALSE;
        }
#endif
    }
    else
    {
        if (!printOneStringFarsi("����� ��ǘ�� ����� ���", PRN_NORM, ALIGN_CENTER))
            return FALSE;
    }
     
//    printStar();
    
    /** PRINT FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files)) 
        return FALSE;

    return TRUE;
}


uint8 displayunsuccessTopupTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
    uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};		//HNO:change 400 to 2000 and 3 to 4 for footer /** 3: for the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};       	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	uint8			referenceNum[1]				= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);
	    
    for (counter = requestedNum - 1; counter >= 0; counter--) 
    {
        //mgh: adding 2string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 8))
    	{
    		fillTransListCommonPartLines(lines, &i, reversalInfo[counter].amount, reversalInfo[counter].PAN, 
    				referenceNum, reversalInfo[counter].STAN, 
                    /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/
                    &(reversalInfo[counter].SWdateTime), depositID); //MRF_NEW17
    		displayedCount++;
    		strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
	}
 
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);

    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, unsuccessTopup);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else 
    	return TRUE;
}

//MRF_TOPUP
uint8 transactionTopupList(argumentListST* args)
{    
    filesST*        	files                                        = (filesST*) (args->argumentName);
    dateTimeST			startDateTime                                = {0, 0};
    dateTimeST			endDateTime                                  = {0, 0};
    int					transactionNumber                            = 0;            /** charge number with specified date in file */
    uint32				requestedNum                                 = 0;            /** user requested transaction number, integer form */
    int					counter                                      = 0;            /** loop counter */
    uint8				serialETC[13 + 1]                            = {0};
    uint8				nameStr[20]                                  = {0};
    uint8               sumValues[15 + 1]                            = {0}; 
    uint8				fileName[FILE_NAME_LENTGH]                   = {0}; //MRF_NEW3
    uint8				activeUser[15 + 1]                           = {0};
    uint8				activeUserTitle[15 + 1]                      = {0};
    uint8				title[30]                                    = {0};
    TopupTransLogST     transactions[MAX_TOPUP_TRANS_LOG_RECORDS]    = {0}; 
    uint8               depositID[11 + 1]                            = {0};//MRF_NEW17 //ABS_NEW1


    strcpy(fileName, TOPUP_LOG_FILE);
    
    if (!readTransactionListFile(fileName, transactions, sizeof(TopupTransLogST), MAX_TOPUP_TRANS_LOG_RECORDS, 
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
        
    strcpy(title, "��� ��ǘ�� ���� ��ю ������"); 

    if (!displayTransactionTopupList(transactions, transactionNumber, requestedNum, title, &startDateTime,
                		&endDateTime, activeUserTitle, activeUser, sumValues))
        	return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;
    
    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN, 
        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;
        
        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}

//MRF_TOPUP
uint8 displayTransactionTopupList(TopupTransLogST* transactions, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};		//HNO:change 700 to 3500 & 3 to 4 line for footer /** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1
	
	     			                   
	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);

    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "Request: %d", requestedNum);
    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "transactoins: %d", transactionNumber);
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0 ; counter--) 
    {
    	//mgh: adding two string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);
        
    	if ( i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 8)) //6: header, 4: footer, 8: body 
    	{
	    	fillTransListCommonPartLines(lines, &i, transactions[counter].amount, transactions[counter].PAN,
	    			transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime), depositID); //MRF_NEW17
	    	displayedCount++;
	    	strcpy(displayedSumValues, sumValues);
    	}
    }
    
    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }
    
    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sum %s", sumValues);
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);
    
    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE, successTopup);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}

//MRF_TOPUP
uint8 merchantTopupReport(argumentListST* args)
{    
    filesST*		files                   				= (filesST*) (args->argumentName);
    uint32          userDateJalali          				= 0;				/** entered date by user (jalali) */
    int             reportYear              				= 0;				/** report year */
    int             reportMonth             				= 0;				/** report month */
    int             reportDay               				= 0;				/** report day */
    int             transactionNumber       				= 0;				/** transactions number with specified date in file */
    uint8           transValueSum[15 + 1]   				= {0};				/** sum of transactions value */
    uint8           tempSumValues[15 + 1]   				= {0};
    int             counter                 				= 0;				/** loop counter */
    uint8           lines[15][32]           				= {0};				/** display lines */
    uint8           valueWithComma[20]      				= {0};				/** sum value with comma, string form */
    TopupTransLogST transactions[MAX_TOPUP_TRANS_LOG_RECORDS] = {0}; 
    uint8           dateValueStr[11]        				= {0};
    uint8           transNum[10]            				= {0};
    uint8           numStr[7]               				= {0};
    uint16          i                                       = 0;
    uint8           key                                     = KBD_CANCEL;
    uint8           separatedStr[10][250]    				= {0, 0}; //ABS:CHANGE:960802
    uint8           wordWrapIndex           				= 0; 
    uint8           loop                    				= 0; 
    uint8           amountPrintBuffer[20]   				= {0}; 
    uint8           fileName[FILE_NAME_LENTGH]				= {0};
    uint8			transType								= TRANS_TOPUP;
    uint8			activeUser[15 + 1]						= {0};
    uint8			activeUserTitle[15 + 1]					= {0};
    merchantSpecST	merchantSpec;
    dateTimeST		dateTime;

    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    memset(&dateTime, 0, sizeof(dateTimeST));
    memset(lines, 0, sizeof(lines));

    strcpy(fileName, TOPUP_LOG_FILE);
    strcpy(activeUserTitle, "����� ����");
    
    if (!readBoxReportFile(fileName, transactions, sizeof(TopupTransLogST), MAX_TOPUP_TRANS_LOG_RECORDS,
    		&userDateJalali, &transactionNumber))
    	return FALSE;
    
    reportYear = getYear(userDateJalali);
    reportMonth = getMonth(userDateJalali);
    reportDay = getDay(userDateJalali);

   /* Adding 2string...*/
    for (counter = 0; counter < transactionNumber; counter++)
    {
        strcpy(tempSumValues, transValueSum);
        sumStringNumbers(tempSumValues, transactions[counter].amount, transValueSum);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "transValueSum= %s", transValueSum);
    }

    /** LINE 0 : HEADER */
    justifyOneStringFarsi("����� ����� ���� ��ю ���", lines[i++], PRN_NORM, ALIGN_CENTER);
    
    /** ACTIVE USER */
    if (strlen(activeUser) > 0)
    {
	    justifyTwoStringFarsi("", activeUserTitle, lines[i++], PRN_NORM);
	    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, activeUser);
    }
    
    /** LINE 1 : DATE */
    justifyTwoStringFarsi("", "�����", lines[i++], PRN_NORM);

    sprintf(dateValueStr, "%04d/%02d/%02d", reportYear, reportMonth, reportDay);
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, dateValueStr);
    
    /** LINE 2 : TRANSACTION NUMBER */
    sprintf(transNum, "%d", transactionNumber);
    justifyTwoStringFarsi(transNum, "�����", lines[i++], PRN_NORM);
    
    /** LINE 3 , 4 , 5 : TRANSACTIONS VALUE */
    justifyTwoStringFarsi(" ", "�����", lines[i++], PRN_NORM);

    
    makeValueWithCommaStr(transValueSum, valueWithComma);
    if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT - strlen(" ����"))
    {
        if (strlen(valueWithComma) >= DISPLAY_CHARACTER_COUNT)
        {
            loop = wordWrapEnglish(valueWithComma, separatedStr, DISPLAY_CHARACTER_COUNT);
            for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
            {
                sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, separatedStr[wordWrapIndex]);
            }
        }
        else
        {
            sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma);
        }
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, "����");
    }
    else
    {
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, " ����");
    }
    
    if (!PrinterAccess(TRUE))    
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);
    
    /** PRINT HEADER */
    dateTime = getDateTime();
    if (!printReceiptHeader(&dateTime, TRUE, files)) 
        return FALSE;
 
    /** PRINT HEADER */
    if (!printOneStringFarsi("����� ����� ���� ��ю ���", PRN_NORM, ALIGN_CENTER))
        return FALSE;
    
    /** PRINT DATE */
//  --MRF_TTF  sprintf(dateValueStr, "%04d/%02d/%02d", reportYear, reportMonth, reportDay);
//    if (!printTwoStringNumericFarsi(dateValueStr, "�����", PRN_NORM))
//        return FALSE; 
    printReceiptDate(dateTime.date,"�����"); //MRF_TTF
    
    /** PRINT ACTIVE USER */
    strcat(activeUserTitle, ":");
    if (strlen(activeUser) > 0)
	    if (!printTwoString(activeUser, activeUserTitle, PRN_NORM, FALSE))
	        return FALSE;
    
    /** PRINT TERMINAL ID */
    if (readMerchantSpec(files, &merchantSpec))
    {
        if (!printTwoStringNumericFarsi(merchantSpec.terminalID, "����� ������", PRN_NORM))
            return FALSE;
    }

    /** PRINT DASHES */
    if (!printStar())
        return FALSE;
    
    /** PRINT TRANSACTION NUMBER */
    sprintf(numStr, "%d", transactionNumber);
    if (!printTwoStringNumericFarsi(numStr, "�����", PRN_NORM))
            return FALSE;

    /** PRINT TRANSACTIONS VALUE */
    if (strlen(valueWithComma) + strlen(" ����") + strlen("�����:") <= getPrinterCharacterCount())
    {
        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "First");    
         if (!printThreeStringFarsi(" ����", valueWithComma, "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
            return FALSE;
    }
    else if (strlen(valueWithComma) + strlen(" ����") <= getPrinterCharacterCount())
    {
        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
            return FALSE;

        sprintf(amountPrintBuffer, "%s ����", valueWithComma);
        if (!printOneStringFarsi(amountPrintBuffer, PRN_NORM, ALIGN_CENTER))
            return FALSE;
    }
    else
    {
        if (!printOneStringFarsi("�����:", PRN_NORM, ALIGN_RIGHT))
            return FALSE;
        if (!printOneStringFarsi(valueWithComma, PRN_NORM, ALIGN_CENTER))
            return FALSE;       
        if (!printOneStringFarsi("����", PRN_NORM, ALIGN_LEFT))
            return FALSE;       
    }

    /** PRINT EMPTY LINES */
    printBlankLines(BLANK_LINES_COUNT);

    return TRUE;
}

//HNO_LOAN
void addLoanReport(messageSpecST* messageSpec)
{
    int16          		retValue		= FAILURE;		/** write in file function return value */
    uint8				fileName[20]	= {0};
    loanPayTransLogST   loanPay;					/** loan transaction detail */

    memset(&loanPay, 0, sizeof(loanPayTransLogST));
    strcpy(fileName, LOAN_PAY_LOG_FILE);

    loanPay.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->loanPay.destinationCardPAN, loanPay.destinationCardPAN);
    strcpy(loanPay.amount, messageSpec->loanPay.payAmount);
    maskCardId(messageSpec->cardSpec.PAN, loanPay.destinationCardPAN);
    strcpy(loanPay.destinationCardHolderName, messageSpec->loanPay.destinationCardHolderName);
    strcpy(loanPay.destinationCardHolderFamily, messageSpec->loanPay.destinationCardHolderFamily);
    strcpy(loanPay.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(loanPay.PAN, messageSpec->cardSpec.PAN);

    retValue = appendFixedFileInfo(fileName, &loanPay,
                                     sizeof(loanPayTransLogST), MAX_LOAN_PAY_TRANS_LOG_RECORDS);
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

//HNO_ADD_ROLL
uint8 printRollRequestTrans(filesST* files, messageSpecST* messageSpec)
{
	uint8	stanNo[7]               = {0};

    if (!PrinterAccess(TRUE))
        return FALSE;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
    	if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
    		return FALSE;

    prePrintFlags = 0;

    if (!printOneStringFarsi("���� ������� ���", PRN_NORM, ALIGN_CENTER))
        return FALSE;

    /** PRINT TERMINAL ID */
    if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.terminalID, "����� ������", PRN_NORM))
        return FALSE;

    /** PRINT MERCHANT ID */
    if (!printTwoStringNumericFarsi(messageSpec->merchantSpec.merchantID, "����� �������", PRN_NORM))
        return FALSE;

    /** PRINT STAN */
    sprintf(stanNo, "%ld", messageSpec->merchantSpec.recieveSTAN);
    if (!printTwoStringNumericFarsi(stanNo, "����� �����", PRN_NORM))
        return FALSE;

    /** PRINT DATE AND TIME */
    if (!printReceiptDateTime(&(messageSpec->dateTime), TRUE))
        return FALSE;

    /** PRINT REFERENCE NUMBER */
    if ((messageSpec->responseStatus == SUCCESS) || (messageSpec->responseStatus == 8)
            || (messageSpec->responseStatus == 16))
    {
        removePad(messageSpec->retrievalReferenceNumber, ' ');
        if (!printTwoStringNumericFarsi(messageSpec->retrievalReferenceNumber, "����� ����", PRN_NORM))
                return FALSE;
    }

    /** COUNT OF ROLL REQUEST */
    if (!printTwoStringNumericFarsi(messageSpec->rollRequest.rollCount, "����� ��� ��������", PRN_NORM))
		return FALSE;

    /** PRINT TRANS STATUS */
	if ((messageSpec->responseStatus != SUCCESS) && (messageSpec->responseStatus != 8)
		   && (messageSpec->responseStatus != 16))
	{
	   uint8 errorMessage[50] = {0};
	   uint8 errorCode[10]    = {0};
	   int   shpState         = 0;

		#ifdef CASTLES_V5S //MRF_TTF
		sprintf(errorCode, "��:%2d", messageSpec->responseStatus);
		#else
	    sprintf(errorCode, "�� %2d :", messageSpec->responseStatus);
		#endif
	    shpState = getShaparakMessages(errorMessage, messageSpec->responseStatus);

	    if ((shpState == WARNING_TRANS || shpState == NO_RECEIVE || shpState == DUPLICATE_TRANS))
	    {
			if (!printTwoStringFarsi(errorCode, "������ ������", PRN_NORM_INVERSE))
				return FALSE;
			#if defined(ICT250) || defined(IWL220)//HNO_ADD
			if (!printOneStringFarsi("����� ���� ������ �� �� ���� ��ǘ��", PRN_NORM, ALIGN_CENTER))
				return FALSE;
			if (!printOneStringFarsi("����� ����� ������", PRN_NORM, ALIGN_CENTER))
				return FALSE;
			#else
			if (!printOneStringFarsi("����� ���� ������ �� �� ���� ��ǘ�� ������ ����� ������.", PRN_NORM, ALIGN_CENTER))
				return FALSE;
			#endif
	   }
	   else if (!printTwoStringFarsi(errorCode, errorMessage, PRN_NORM_INVERSE))
	   {
			   return FALSE;
	   }
	}
	else if (!printOneStringFarsi("������ ����", PRN_BIG, ALIGN_CENTER))
		   return FALSE;


    /** PRINT TRANSACTION FOOTER */
    printTransactionFooter(FALSE, TRUE, TRUE, files);

    return TRUE;
}

//HNO_ADD_ROLL
void addRollRequestReport(messageSpecST* messageSpec)
{
    int16           retValue						= FAILURE;		/** write in file function return value */
    uint8			fileName[FILE_NAME_LENTGH]		= {0};
    rollTransLogST  transaction;

    memset(&transaction, 0, sizeof(rollTransLogST));

    strcpy(fileName, ROLL_LOG_FILE);

// mgh_93    strcatUserID(fileName);

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "add roll request log");
    transaction.dateTime = messageSpec->dateTime;
    strcpy(transaction.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(transaction.rollCount, messageSpec->rollRequest.rollCount);

    retValue = appendFixedFileInfo(fileName, &transaction,
                                     sizeof(rollTransLogST), MAX_ROLL_REQUEST_TRANS_LOG_RECORDS);
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

//HNO_ADD_ROLL
uint8 reprintRollRequestTransaction(argumentListST* args)
{
    filesST*				files	= (filesST*) (args->argumentName);
    messageSpecST			messageSpec;									/** last buy transaction information */
    lastCustomerReceiptST   lastCustomerReceipt;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    if (!readLastCustomerReceiptFile(LAST_CUSTOMER_ROLL_REQUEST_RECIEPT_FILE, &lastCustomerReceipt))
    	return FALSE;

    structCpy(&lastCustomerReceipt, &messageSpec);
    printRollRequestTrans(files, &messageSpec);

    return TRUE;
}

//HNO_CHARGE
/**
 * This function report latest successful transactions with fundamental informations.
 * @param   args [input]
 * @return  TRUE & FAlSE.
 */
uint8 completeChargeReport(argumentListST* args)
{
    filesST*        		files                       = (filesST*) (args->argumentName);
    uint8           		IDstr[6 + 1]    			= {0};
    uint8					id[7]						= {0};
    uint32					ID							= 0;
    uint32          		requestedNum      			= 0;
    uint8           		chargeTypeStr[10]   		= {0};
    uint8           		chargeStr[20]   			= {0};
    uint8					cardID[17]					= {0};
    uint8					valueWithComma[19 + 1]  	= {0};
    uint8   				amountPrintBuffer[20]   	= {0};
    uint8					countStr[7]					= {0};
    int             		transactionNumber 			= 0;
    int 					i 							= 0;
    int						count						= 0;
    cardTracksST    		cardTracks;
    cardSpecST      		cardSpec;
    chargeST				charge;
    dateTimeST				dateTime;
    

    memset(&charge, 0, sizeof(chargeST));
    memset(&cardTracks, 0, sizeof(cardTracksST));
    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&dateTime, 0, sizeof(dateTimeST));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**completeChargeReport**");

	if (!getStringValue("���� ����� ��ю:", 12, 1, IDstr, FALSE, FALSE, FALSE, "0", 0))
		return FALSE;

	ID = atoi(IDstr);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "size struct: %ld", sizeof(chargeST));
	if (!readSTANTransFromFile(CHARGE_INFO_FILE, &charge, sizeof(chargeST), MAX_CHARGE_TRANS, ID,
			&transactionNumber, &requestedNum))

	{
		displayMessageBox("��� ���� ������� ��ю!",WARNING_TRANS);
		return FALSE;
	}

	displayMessage("���� ���� �� Ș���. . .", DISPLAY_START_LINE + 1, ALL_LINES);
	if(getCardHandle() <= 0)
		enableMSR();

	if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
	{
		return FALSE;
	}

	if (!readTrackData(files, &cardTracks, &cardSpec, FALSE))
	{
		return FALSE;
	}

	if (strcmp(charge.PAN, cardSpec.PAN) == 0)
	{
        clearDisplay(); //MRF_NEW12
        displayMessage("�� ��� �ǁ �����. . .", DISPLAY_START_LINE + 1, ALL_LINES);//MRF_NEW12
        
		/** PRINT HEADER */
		dateTime = getDateTime();
		if (!prePrintFlags)
			if (!printReceiptHeader(&dateTime, TRUE, files))
				return FALSE;

		if (!PrinterAccess(TRUE))
			return FALSE;

        /** PRINT TITLE */
        if (!printOneStringFarsi("����� ���� �� ��ю", PRN_NORM, ALIGN_CENTER))
            return FALSE;

		/** PRINT TERMINAL ID */
		if (!printTwoStringNumericFarsi(charge.terminalID, "����� ������", PRN_NORM))
			return FALSE;

		sprintf(id, "%ld", charge.id);
		if (!printTwoStringNumericFarsi(id,"����� ��ю",PRN_NORM))
			return FALSE;

		sprintf(countStr, "%d", charge.chargeCount);
		if (!printTwoStringNumericFarsi(countStr,"�����",PRN_NORM))
			return FALSE;

		/** PRINT PAN CARD */
		maskCardId(cardSpec.PAN, cardID);
		removePad(cardID, ' ');
        
#ifdef CASTLES_V5S //MRF_NEW10
        reversPartString(cardID);
#endif
        if (!printTwoStringNumericFarsi(cardID, "����� ����", PRN_NORM))
			return FALSE;

		printStar();

		count = charge.chargeCount;
		for(i = 1 ; i <= count ; i++)
		{
			/** PRINT CHARGE TYPE & CHARGE AMOUNT*/
			getChargeTypeStr(charge.chargeInfo[i].type, chargeTypeStr);
			strcpy(chargeStr,  "���� ��ю");
			strcat(chargeStr, chargeTypeStr);//cat charge type to amount charge
            makeValueWithCommaStr(charge.chargeInfo[i].amount, valueWithComma);
            sprintf(amountPrintBuffer, "%s ����", valueWithComma);
			if (!printTwoStringNumericFarsi(amountPrintBuffer, chargeStr, PRN_NORM))
				return FALSE;

			/** PRINT CHARGE SERIAL*/
			removePad(charge.chargeInfo[i].serial, ' ');
			if (!printTwoStringNumericFarsi(charge.chargeInfo[i].serial, "����� ��ю", PRN_NORM))
				return FALSE;

			if (!printTwoStringNumericFarsi(charge.chargeInfo[i].PIN, "��� ��ю", PRN_NORM))
				return FALSE;

	        removePad(charge.chargeInfo[i].ReferenceNumber, ' ');
	        if (!printTwoStringNumericFarsi(charge.chargeInfo[i].ReferenceNumber, "����� ����", PRN_NORM))
	                return FALSE;

	        /** PRINT DATE AND TIME */
	        if (!printReceiptDateTime(&(charge.chargeInfo[i].dateTime), TRUE))
	            return FALSE;

			printDash3();
		}

		if(!printOneStringFarsi("������� ��� ���� �� ���� ������� ���", PRN_NORM, ALIGN_CENTER))
			return FALSE;

		printBlankLines(BLANK_LINES_COUNT);
    }
    else
    	displayMessageBox("���� ����� ����!",WARNING_TRANS);
    
    return TRUE;
}

//HNO_SHIFT
uint8 readAllUsersTransactionListFile(uint8* fileName, void* transaction, uint16 structLen, uint16 maxRecord,
							  	dateTimeST* startDateTime, dateTimeST* endDateTime, int* transactionNum,
								uint32* requestedNum,uint8* activeUserTitle, uint8* activeUser)
{
    int             searchTransRes          = -2;			/** search for transaction with specified date in transactions file */
    int             startRecordNum          = 0;			/** start record number of transactions with specified date in file */
    int             endRecordNum            = 0;			/** end record number of transactions with specified date in file */
    int				beginIndexInFile		= 0;            /** index of beginning requested transaction in transactions file */
    int16			retValue 				= FAILURE;

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**readallusersTransactionListFile**");

    if (fileExist(fileName) != SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "File transaction unsuccess not exist!");
    	return FALSE;
    }

    searchTransRes = searchDateTimeRangeInFixedFile(fileName, structLen, 0,
    		startDateTime, endDateTime, maxRecord, &startRecordNum, &endRecordNum);

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "start: %d, end: %d", startRecordNum, endRecordNum);
    if (searchTransRes == -2 || startRecordNum == -2 || endRecordNum == -2)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in Start time trans not exist !!");
        return FALSE;
    }

    if (startRecordNum == -1 || endRecordNum == -1)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in End time trans not exist !!");
        return FALSE;
    }

    *transactionNum = endRecordNum - startRecordNum + 1;

    *requestedNum = *transactionNum;

    if (*requestedNum > maxRecord)
    	if (*transactionNum <= maxRecord)
    		*requestedNum = *transactionNum;
    	else
    		*requestedNum = maxRecord;

    if (*requestedNum == 0 || *requestedNum > *transactionNum)
        *requestedNum = *transactionNum;

    //show the last transactions
    beginIndexInFile = *transactionNum - *requestedNum + startRecordNum;

    retValue = readFixedFileInfoArray(fileName,(uint8*) transaction, structLen, beginIndexInFile,
                                        *requestedNum, maxRecord);
    if (retValue != SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " in readFixedFileInfoArray trans not exist !!");
        return FALSE;
    }

    return TRUE;
}

//MRF_NEW15
uint8 deletAllDependentFilesShift(void)
{
    int16           retValue                = FAILURE;
    userNodeST*     currentNode             = NULL;
    uint8           file1[FILE_NAME_LENTGH] = {0};
    uint8           file2[FILE_NAME_LENTGH] = {0};
    uint8           file3[FILE_NAME_LENTGH] = {0};
    uint8           username[16 + 1]        = {0};
  
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**deletAllDependentFilesShift");
    
    currentNode = getUserList();
    while (currentNode)
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL , "", "%s", currentNode->data.userName);
        strcpy(file1, LAST_CUSTOMER_BUY_RECIEPT_FILE);
        strcpy(file2, BUY_LOG_FILE);
        strcpy(file3, REVERSAL_BUY_LOG_FILE);

        strcpy(username, currentNode->data.userName);
        if (strcmp(username , "����") == 0)
        {
            retValue = fileRemove(file1);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file1);
            if (retValue != SUCCESS)
                return retValue;

            retValue = fileRemove(file2);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file2);
            if (retValue != SUCCESS)
                return retValue;

            retValue = fileRemove(file3);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file3);
            if (retValue != SUCCESS)
                return retValue;
        }
        else
        {
            retValue = deletUserFiles(username);
            if (retValue != SUCCESS)
                   return retValue;
        }
        currentNode = currentNode->next;
    }
    
    return SUCCESS;
}

uint8 deletUserFiles(uint8* username)
{
    int16           retValue                = FAILURE;
    uint8           file1[FILE_NAME_LENTGH] = {0};
    uint8           file2[FILE_NAME_LENTGH] = {0};
    uint8           file3[FILE_NAME_LENTGH] = {0};
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**deletUserFiles");
    
    strcpy(file1, LAST_CUSTOMER_BUY_RECIEPT_FILE);
    strcpy(file2, BUY_LOG_FILE);
    strcpy(file3, REVERSAL_BUY_LOG_FILE);
            
    strcat(file1, username);
    strcat(file2, username);
    strcat(file3, username);

    retValue = fileRemove(file1);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file1);
    if (retValue != SUCCESS)
            return retValue;

    retValue = fileRemove(file2);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file2);
    if (retValue != SUCCESS)
            return retValue;

    retValue = fileRemove(file3);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**FileRemove:%s", file3);
    if (retValue != SUCCESS)
            return retValue;
    
    return SUCCESS;
}

//HNO_CHARITY
uint8 printTransactionCharity(filesST* files, messageSpecST* messageSpec, uint8 customer, uint8 rePrint)
{
    uint8	reversalCancelled		= FALSE;		/** is reversal canceled? */
    uint8	valueWithComma[20]		= {0};
    uint8   amountPrintBuffer[20]   = {0};   //110828   for amount print

    if (!PrinterAccess(TRUE))
        return reversalCancelled;

    displayMessage("�ǁ ���� ...", DISPLAY_START_LINE + 1, ALL_LINES);

    /** PRINT HEADER */
    if (!prePrintFlags)
        if (!printReceiptHeader(&(messageSpec->dateTime), TRUE, files))
            return reversalCancelled;

    prePrintFlags = 0;

    if (!printTransactionCommonPart(messageSpec, customer, rePrint, files))
        return reversalCancelled;

    if ((customer && !rePrint) && ((messageSpec->responseStatus == SUCCESS)
            || (messageSpec->responseStatus == 8)
            || (messageSpec->responseStatus == 16)))
    {
        /** CANCEL REVERSAL & ... */
        reversalCancelled = TRUE;

        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);

        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);

        addTransToSettlementFile(files, messageSpec);
        makeTransPrintDataLog(files, messageSpec);
    	addCharityReport(messageSpec);
    }

//    if(!customer)
//    {
//        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
//        fileRemove(files->customerReceiptFile);
//    }

    /** PRINT FOOTER */
    if (!printTransactionFooter(rePrint, customer, TRUE, files))
        return reversalCancelled;

   if ((!reversalCancelled) && (!rePrint))
    {
        showLog(JUST_CASTLES, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);
    }

    return reversalCancelled;
}

//HNO_CHARITY
void addCharityReport(messageSpecST* messageSpec)
{
    int16           retValue						= FAILURE;		/** write in file function return value */
    uint8			fileName[FILE_NAME_LENTGH]		= {0};
    charityST   	transaction;

    memset(&transaction, 0, sizeof(charityST));

    strcpy(fileName, CHARITY_LOG_FILE);

    transaction.dateTime = messageSpec->dateTime;
    maskCardId(messageSpec->cardSpec.PAN, transaction.PAN);
    strcpy(transaction.amount, messageSpec->amount);
    strcpy(transaction.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    strcpy(transaction.InstituteCode, messageSpec->charity.InstituteCode);//HNO_DEBUG

    retValue = appendFixedFileInfo(fileName, &transaction,
                                     sizeof(charityST), MAX_CHARITY_TRANS_LOG_RECORD);
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
    }
}

//HNO_CHARITY
uint8 displayTransactionCharityList(charityST* transactions, int transactionNumber, uint32 requestedNum, uint8* title,
		dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_SUCCESS_TRANS_DISPLAY + 4][32]			= {0, 0};		//HNO:change 700 to 3500 & 3 to 4 line for footer /** 3: fot the last three lines */
	uint16  		i                       	= 0;
	uint8			tempSumValues[15 + 1]   	= {0};      	/** sum of listed transactions value */
	int				counter                 	= 0;            /** loop counter */
	uint8			key 						= KBD_CANCEL;
	uint8			displayedCount          	= 0;            /** display counter */
	uint8			displayedSumValues[15 + 1]	= {0};
	terminalSpecST  terminalCapability			= getTerminalCapability();
    uint8           depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1

	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);

    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "Request: %d", requestedNum);
    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "transactions: %d", transactionNumber);
    /** TRANSACTIONS LOG */
    for (counter = requestedNum - 1; counter >= 0 ; counter--)
    {
    	//mgh: adding two string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);

    	if ( i < (MAX_LINE_SUCCESS_TRANS_DISPLAY - 6 - 4 - 8)) //6: header, 4: footer, 8: body
    	{
	    	fillTransListCommonPartLines(lines, &i, transactions[counter].amount, transactions[counter].PAN,
	    			transactions[counter].retrievalReferenceNumber, 0, 
                    &(transactions[counter].dateTime), depositID); //MRF_NEW17
	    	displayedCount++;
	    	strcpy(displayedSumValues, sumValues);
    	}
    }

    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
    	justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
    }

    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sum %s", sumValues);
    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);

    if (!terminalCapability.graphicCapability) //MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE,successCharity);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}

//HNO_CHARITY
uint8 transactionCharityList(argumentListST* args)
{
    filesST*        files                                       = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    charityST   	transactions[MAX_CHARITY_TRANS_LOG_RECORD] 	= {0};
    int				transactionNumber                           = 0;            /** charge number with specified date in file */
    int				counter                                     = 0;            /** loop counter */
    uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
    uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
    uint8			fileName[FILE_NAME_LENTGH]                  = {0};
    uint8			activeUser[15 + 1]                          = {0};
    uint8			title[30]                                   = {0};
    uint8			activeUserTitle[15 + 1]                     = {0};
    uint8           check                                       = FALSE;
    uint8           sumDailyValues[15 + 1]                      = {0};
    uint8			tempSumValues[15 + 1]                       = {0};
    uint8			valueWithComma[20]                          = {0};
    uint8			instituteName[50]							= {0};//HNO_DEBUG
    uint8           depositID[11 + 1]                           = {0};//MRF_NEW17 //ABS_NEW1


    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*********charityList********");
    strcpy(fileName, CHARITY_LOG_FILE);

    if (!readTransactionListFile(fileName, transactions, sizeof(charityST), MAX_CHARITY_TRANS_LOG_RECORD,
    						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

    /** LINE 0 : HEADER */
    strcpy(title, "�����ǘ�� ���� ������");

    if (!displayTransactionCharityList(transactions, transactionNumber, requestedNum, "������ ����", &startDateTime,
    		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
    	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;

    if (! printReceiptDate(transactions[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;

    printDash3();

    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(transactions[counter].dateTime.date,"�����"))
               return FALSE;
           printDash3();
        }

        //HNO_DEBUG
        getCharityInstituteName(instituteName, transactions[counter].InstituteCode);
#ifdef VX520		//ABS:ADD
		if (!printTwoStringFarsi(instituteName, "��� �����:", PRN_NORM))
			return FALSE;
#else
        //MRF_NEW17
        strcat(instituteName, "|");
        if (!printTwoStringFarsi(instituteName, "|��� �����:", PRN_NORM))
            return FALSE;
#endif
    	if (!printTransactionListCommonPart(transactions[counter].amount, transactions[counter].PAN,
        			transactions[counter].retrievalReferenceNumber, 0, &(transactions[counter].dateTime), depositID))//MRF_NEW17
    		return FALSE;

        check = compareDates(&transactions[counter].dateTime, &transactions[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, transactions[counter].amount, sumDailyValues);

        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();

        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);

            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }

        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))//HNO_DEBUG2
    	return FALSE;

    return TRUE;
}

//HNO_CHARITY
uint8 displayunsuccessCharityTransaction(reversalTransST* reversalInfo, int transactionNumber, uint32 requestedNum,
		uint8* title, dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, uint8* activeUser, uint8* sumValues)
{
	uint8			lines[MAX_LINE_UNSUCCESS_TRANS_DISPLAY + 4][32]	= {0, 0};
	uint16  		i                       						= 0;
	uint8			tempSumValues[15 + 1]   						= {0};                          /** sum of listed transactions value */
	int				counter                 						= 0;                            /** loop counter */
	uint8			key 											= KBD_CANCEL;
	uint8			displayedCount          						= 0;                            /** display counter */
	uint8			displayedSumValues[15 + 1]						= {0};
	uint8			referenceNum[1]									= {0};
	terminalSpecST  terminalCapability								= getTerminalCapability();
    uint8           depositID[11 + 1]                               = {0};//MRF_NEW17 //ABS_NEW1                                       = 0;//MRF_NEW17

	memset(lines, 0, sizeof(lines));
	fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, startDateTime, endDateTime);

    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        //mgh: adding 2string...
        strcpy(tempSumValues, sumValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumValues);
        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "sumValues= %s", sumValues);

    	if (i < (MAX_LINE_UNSUCCESS_TRANS_DISPLAY - 6 - 4 - 8))
    	{
             //MRF_NEW2
            fillTransListCommonPartLines(lines, &i, reversalInfo[counter].amount, reversalInfo[counter].PAN,
    				referenceNum, reversalInfo[counter].STAN,
                    &(reversalInfo[counter].SWdateTime), depositID); //MRF_NEW17

    		displayedCount++;
    		strcpy(displayedSumValues, sumValues);
    	}
    }

    if (requestedNum != displayedCount) //displayed transactions are less than requested
    {
    	justifyOneStringFarsi("���� ������ ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("��ǘ�� �� ����", lines[i++], PRN_NORM, ALIGN_CENTER);
		justifyOneStringFarsi("�� �ǁ ������.", lines[i++], PRN_NORM, ALIGN_CENTER);
	}

    fillTransListFooterLines(lines, &i, transactionNumber, displayedCount, displayedSumValues);

    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE,unsuccessCharity);

    if (key != KBD_ENTER && key != KBD_CONFIRM)
        return FALSE;
    else
    	return TRUE;
}
//HNO_CHARITY
uint8 unsuccessCharityTransaction(argumentListST* args)
{
    filesST*        			files                                 = (filesST*) (args->argumentName);
    dateTimeST					startDateTime                         = {0, 0};
    dateTimeST					endDateTime                           = {0, 0};
    reversalTransST				reversalInfo[MAX_UNSUCCESS_CHARITY_TRANS] = {0};
    int							transactionNumber			= 0;            /** charge number with specified date in file */
    int							counter                 	= 0;            /** loop counter */
    uint32						requestedNum				= 0;            /** user requested transaction number, integer form */
    uint8						sumValues[15 + 1]       	= {0};          /** sum of listed transactions value */
    uint8						fileName[FILE_NAME_LENTGH]	= {0};
    uint8						activeUser[15 + 1]			= {0};
    uint8						activeUserTitle[15 + 1]		= {0};
    uint8						title[30]                   = {0};
    uint8						referenceNum[1]				= {0};
    uint8                       check                       = FALSE;
    uint8                       sumDailyValues[15 + 1]      = {0};
    uint8                       tempSumValues[15 + 1]       = {0};
    uint8                       valueWithComma[20]          = {0};
	uint8						instituteName[50]			= {0};//HNO_DEBUG
    uint8                       depositID[11 + 1]           = {0};//MRF_NEW17 //ABS_NEW1

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****unsuccess charity****");

    strcpy(fileName, REVERSAL_CHARITY_LOG_FILE);

    if (!readTransactionListFile(fileName, reversalInfo, sizeof(reversalTransST), MAX_UNSUCCESS_CHARITY_TRANS,
        						&startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", reversalInfo[0].charity.InstituteCode);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", &reversalInfo[0].charity.InstituteCode);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", reversalInfo[1].charity.InstituteCode);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", &reversalInfo[1].charity.InstituteCode);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", reversalInfo[2].charity.InstituteCode);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", &reversalInfo[2].charity.InstituteCode);
        
    strcpy(title, "�����ǘ�� ������ ������");
    if (!displayunsuccessCharityTransaction(reversalInfo, transactionNumber, requestedNum, "������ ������", &startDateTime,
                   		&endDateTime, activeUserTitle, activeUser, sumValues))//ABS:CHANGE
           	return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
    	return FALSE;

    if (!printReceiptDate(reversalInfo[requestedNum - 1].dateTime.date,"�����"))
       return FALSE;

    printDash3();

    /** PRINT TRANSACTIONS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        if (check != 0)
        {
           if (!printReceiptDate(reversalInfo[counter].dateTime.date,"�����"))
               return FALSE;

           printDash3();
        }
        
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", reversalInfo[counter].charity.InstituteCode);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Chariy code: %s", &reversalInfo[counter].charity.InstituteCode);
        //HNO_DEBUG
        getCharityInstituteName(instituteName, reversalInfo[counter].charity.InstituteCode);

#ifdef VX520		//ABS:ADD
		if (!printTwoStringFarsi(instituteName, "��� �����:", PRN_NORM))
			return FALSE;
#else   
        //MRF_NEW17
        strcat(instituteName, "|");
        if (!printTwoStringFarsi(instituteName, "|��� �����:", PRN_NORM))
            return FALSE;
#endif

    	if (!printTransactionListCommonPart(reversalInfo[counter].amount, reversalInfo[counter].PAN, referenceNum,
    			reversalInfo[counter].STAN, /*--MRF_NEW2 &(reversalInfo[counter].dateTime)*/&(reversalInfo[counter].SWdateTime), depositID))//MRF_NEW17
    		return FALSE;

        check = compareDates(&reversalInfo[counter].dateTime, &reversalInfo[counter - 1].dateTime, FALSE);
        strcpy(tempSumValues, sumDailyValues);
        sumStringNumbers(tempSumValues, reversalInfo[counter].amount, sumDailyValues);

        if (counter != 0 && check == 0)
            printDash2();
        else
            printDash3();

        if (check != 0)
        {
            makeValueWithCommaStr(sumDailyValues, valueWithComma);

            if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
                return FALSE;

            printStar();
            memset(sumDailyValues, 0, sizeof(sumDailyValues));
        }

        if (cancelFunction())
            return FALSE;
    }

    if (!printTransactionListFooter(transactionNumber, requestedNum, sumValues))
    	return FALSE;

    return TRUE;
}

//HNO_CHARITY
uint8 reprintCharityTransaction(argumentListST* args)
{
    filesST*                files						= (filesST*) (args->argumentName);
    uint8                   fileName[FILE_NAME_LENTGH]	= {0};
    messageSpecST           messageSpec;
    lastCustomerReceiptST	lastCustomerReceipt;
    uint8					loginUserName[15 + 1] 		= { 0 }; //ABS:ADD
	uint8					selectUser[16]				= {0};//HNO_SHIFT

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));

    strcpy(fileName, LAST_CUSTOMER_CHARITY_RECIEPT_FILE);

    if (!readLastCustomerReceiptFile(fileName, &lastCustomerReceipt))
    	return FALSE;

    structCpy(&lastCustomerReceipt, &messageSpec);
    printTransactionCharity(files, &messageSpec, TRUE, REPRINT);

	return TRUE;
}

//HNO_CHARITY
uint8 merchantCharityReport(argumentListST* args)
{
    filesST*        files                                       = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                               = {0, 0};
    dateTimeST		endDateTime                                 = {0, 0};
    charityST	    transactions[MAX_CHARITY_TRANS_LOG_RECORD] 	= {0};
    int				transactionNumber                           = 0;            /** charge number with specified date in file */
    int				counter                                     = 0;            /** loop counter */
    uint32			requestedNum                                = 0;            /** user requested transaction number, integer form */
    uint8			sumValues[15 + 1]                           = {0};          /** sum of listed transactions value */
    uint8			fileName[FILE_NAME_LENTGH]                  = {0};
    uint8			activeUser[15 + 1]                          = {0};
    uint8			title[30]                                   = {0};
    uint8			activeUserTitle[15 + 1]                     = {0};
    uint8           check                                       = FALSE;
    uint8           sumDailyValues[15 + 1]                      = {0};
    uint8			tempSumValues[15 + 1]                       = {0};
    uint8			valueWithComma[20]                          = {0};
    uint8           transNum[5]                                 = {0};
    int             sumCountValues                      		= 0;
    uint8           sumAllUsersValues[15 + 1]                   = {0};

    strcpy(fileName, CHARITY_LOG_FILE);
	strcpy(title, "����� ����� ������");

	//HNO_DEBUG
    if (!readTransactionListFile(fileName, transactions, sizeof(charityST), MAX_CHARITY_TRANS_LOG_RECORD,
                        &startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;

	if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
		return FALSE;

	for (counter = requestedNum - 1; counter >= 0; counter--)
		sumStringNumbers(sumDailyValues, transactions[counter].amount, sumDailyValues);

	makeValueWithCommaStr(sumDailyValues, valueWithComma);

	sprintf(transNum, "%d", transactionNumber);
	if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
		return FALSE;

	if (!printThreeStringFarsi(" ����", valueWithComma , "�����", PRN_NORM, ALIGN_RIGHT, TRUE))
		return FALSE;

	printStar();
	printBlankLines(BLANK_LINES_COUNT);//HNO_IDENT

    return TRUE;
}

//HNO_INIT
void addInitializeReport(messageSpecST* messageSpec, uint8 response)  
{
    int16                               retValue                    = FAILURE;          /** write in file function return value */
    uint8                               fileName[FILE_NAME_LENTGH]	= {0};  //MRF_NEW3
    InitializeLogST                     initialize;                                        /** billPay transaction detail */
    
    memset(&initialize, 0, sizeof(InitializeLogST));
    strcpy(fileName, INITIALIZE_LOG_FILE);
   
    initialize.dateTime = messageSpec->dateTime;
    strcpy(initialize.macVersion, messageSpec->versionKey.macVersion);
    strcpy(initialize.pinVersion, messageSpec->versionKey.pinVersion);
    initialize.transType = messageSpec->transType;
   
    if(response)
    {
        strcpy(initialize.errorMsg, "����");
        retValue = updateFixedFileLastRecord(fileName, &initialize, 
                sizeof(InitializeLogST), MAX_INITIALIZE_TRANS_LOG_RECORDS); 
    }
      
    else
    {
        strcpy(initialize.errorMsg , "������");
        retValue = appendFixedFileInfo(fileName, &initialize, 
            sizeof(InitializeLogST), MAX_INITIALIZE_TRANS_LOG_RECORDS);
    }
    
    if (retValue != SUCCESS)
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);

}

//HNO_INIT
uint8 transactionInitializeList(argumentListST* args)
{
    filesST*        files                               = (filesST*) (args->argumentName);
    dateTimeST		startDateTime                       = {0, 0};
    dateTimeST		endDateTime                         = {0, 0};
    int             transactionNumber                   = 0;            /** charge number with specified date in file */
    uint32			requestedNum                        = 0;            /** user requested transaction number, integer form */
    int				counter                             = 0;            /** loop counter */
    uint8			fileName[FILE_NAME_LENTGH]          = {0};          //MEF_NEW3
    uint8			activeUser[15 + 1]                  = {0};
    uint8			activeUserTitle[15 + 1]             = {0};
    uint8			title[30]                           = {0};
    uint8			transNum[10]                        = {0};
    InitializeLogST transactions[MAX_INITIALIZE_TRANS_LOG_RECORDS]      = {0}; 

    strcpy(fileName, INITIALIZE_LOG_FILE);
    
    if (!readTransactionListFile(fileName, transactions, sizeof(InitializeLogST), MAX_INITIALIZE_TRANS_LOG_RECORDS, 
                &startDateTime, &endDateTime, &transactionNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
        
    strcpy(title, "����� �������"); 
    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
        return FALSE;
    
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        printReceiptDateTime(&(transactions[counter].dateTime), TRUE);
        
        if (!printTwoStringNumericFarsi(transactions[counter].macVersion, "����� ���� �", PRN_NORM))
            return FALSE;
        if (!printTwoStringNumericFarsi(transactions[counter].pinVersion, "����� ���� ���", PRN_NORM))
            return FALSE;
        if(transactions[counter].transType == TRANS_INITIALIZE)
            if (!printTwoStringNumericFarsi(transactions[counter].errorMsg, "��ǘ�� �������", PRN_NORM))
                return FALSE;
        if(transactions[counter].transType == TRANS_LOGON)
            if (!printTwoStringNumericFarsi(transactions[counter].errorMsg, "��ǘ�� ����� ����", PRN_NORM))
                return FALSE;

        printDash3();
    }

    if (cancelFunction())
        return FALSE;
    
    sprintf(transNum, "%d", transactionNumber);
if (!printTwoStringNumericFarsi(transNum, "�����", PRN_NORM))
    return FALSE;
    
    printBlankLines(BLANK_LINES_COUNT);

    return TRUE;
}