#include <string.h> 
//MRF #include <stdio.h>   
//MRF #include <stdlib.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_serial.h"
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_connection.h"
#include "N_cardSpec.h"
#include "N_dateTime.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_messaging.h"
#include "N_getUserInput.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
//#include "N_passManager.h" HNO_COMMENT
#include "N_printerWrapper.h" 
#include "N_printer.h" 
#include "N_terminalSpec.h" 
#include "N_barCode.h" 
#include "N_POSSpecific.h"
#include "N_displayWrapper.h" 
#include "N_display.h" 
#include "N_initialize.h"
#include "N_dateTimeWrapper.h" 
//#include "N_dateTime.h"
#include "N_log.h"
#include "N_fileManageWrapper.h"
#include "N_connectionWrapper.h"//ABS

int16 checkBillcodePayOff(uint8* buffer);

static uint8 preTitle[20]={0};
uint8   check = FALSE;


/**
 * The function enter buy information.
 * @param  amount [input]: the amount of buy
 * @param  PIN [input]: PIN number
 * @param  readAmount [input]: read amount
 * @see    getStringValue().
 * @see    showLog().
 * @see    getPass().
 * @return TRUE or FALSE.
 */
uint8 enterBuyInfo(filesST* files, messageSpecST* messageSpec, uint8* PIN, uint8 readAmount, uint8* depositID)//#MRF_970806 
{
	int32             	check               	= 0;
	serviceSpecST   	service             	= getPOSService();
	uint8           	retValue            	= 0;
	uint8           	message[60]         	= {0};
	uint8           	mechantID[13 + 1]    	= {0};
#ifdef VX520											//++ABS:980707
	int32             	discount            	= 0;
	int32             	amount              	= 0;
	int32             	result              	= 0;
	int32             	preAmount           	= 0;
	int32             	tipAmount           	= 0;
#else
	int64             	discount            	= 0;
	int64             	amount = 0;
	int64             	result = 0;
	int64             	preAmount = 0;
	int64             	tipAmount = 0;
#endif

	merchantSpecST  	merchantSpec;
    
    memset(&merchantSpec, 0, sizeof(merchantSpecST));

    if (readAmount)
    {   
        //start_#MRF_970806
        if (fileExist(SUPERVISOR_FILE)!= SUCCESS)
        {
            readMerchantSpec(files, &merchantSpec);
            if (service.depositID)
            {
                if (!getStringValue("����� �����:", 13, 9, depositID, FALSE, TRUE, FALSE, "0", 0)) 
                return FALSE;

                strncpy(mechantID, &merchantSpec.merchantID[6], 9);
                if (strcmp(depositID, mechantID) != 0)
                {
                    displayMessageBox("����� ����� ������ ���!", MSG_ERROR);
                    return FALSE;
                }
                strcpy(messageSpec->depositID, depositID);
            }
            
            clearDisplay();
             
            if (!getStringValue("���� ����:", 12, 4, messageSpec->amount, FALSE, FALSE, TRUE, "0", 0)) 
                return FALSE;
            
            if (service.discount)
            {
#ifdef CASTLES_V5S
                strcpy(messageSpec->preAmount, messageSpec->amount);

                discount = atoll(merchantSpec.discount);
                amount = atoll(messageSpec->amount);
                preAmount = atoll(messageSpec->preAmount);
                result = amount * (discount * 0.01);
                result = preAmount - result;
                sprintf(messageSpec->amount, "%lld", result);
            }
        
            if (service.tip)
            {
                if (!enterTip(messageSpec->tipAmount))
                    return FALSE;
                
                tipAmount = atoll(messageSpec->tipAmount);
                amount = atoll(messageSpec->amount);
                result = amount + tipAmount;
                sprintf(messageSpec->amount, "%lld", result);
            }
#else
            strcpy(messageSpec->preAmount, messageSpec->amount);

            discount = atol(merchantSpec.discount);
            amount = atol(messageSpec->amount);
            preAmount = atol(messageSpec->preAmount);
            result = amount * (discount * 0.01);
            result = preAmount - result;
            sprintf(messageSpec->amount, "%ld", result);


			}

        if (service.tip)
        {
			if (!enterTip(messageSpec->tipAmount))
				return FALSE;

//			tipAmount = atol(messageSpec->tipAmount);
//			amount = atol(messageSpec->amount);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "in tipp");
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "messageSpec->tipAmount: %s", messageSpec->tipAmount);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "messageSpec->amount: %s", messageSpec->amount);
			sumTwoNumericStr(messageSpec->amount, messageSpec->tipAmount, messageSpec->amount);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "resulttttt: %s", messageSpec->amount);
//			result = amount + tipAmount;
//			sprintf(messageSpec->amount, "%ld", result);
        }
#endif
        }
        else //if is supervisor
        {
            if (!getStringValue("���� ����:", 12, 4, messageSpec->amount, FALSE, FALSE, TRUE, "0", 0)) 
                return FALSE;
            
            check = atoi(messageSpec->amount);
            if (check >= 3000)//#MRF_971026
            {
                displayMessageBox("���� ���� ��� ��� ���� ���!", MSG_ERROR);
                return FALSE;
            }
        }
    }
    
    if (service.confirmationAmount)
    {
        makeValueWithCommaStr(messageSpec->amount, message);
        strcat(message," ����");
        retValue = displayMessageBox(message, MSG_CONFIRMATION);
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)
        {
            memset(PIN, 0, 5);
            if (!getPass("", PIN, 4, 2))
                return FALSE ;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        memset(PIN, 0, 5); 
        if (!getPass("", PIN, 4, 2))
            return FALSE ;
    }
    //end_#MRF_970806
    return TRUE ;
}

/**
 * The function get balance information.
 * @param  PIN [input]: PIN number
 * @see    displayMessageBox().
 * @see    selectSmallDisplayFontFarsi().
 * @see    getPass().
 * @see    showLog().
 * @return TRUE or FALSE.
 */
uint8 enterBalanceInfo(uint8* PIN) 
{
    uint8 key = 0;
    memset(PIN, 0, 5);
    
    key = displayMessageBox("������ ������ �� ������ 1206 ����",MSG_CONFIRMATION);
    if (key == KBD_ENTER || key == KBD_CONFIRM)
    {
        selectSmallDisplayFont();
        if (!getPass("", PIN, 4, 2))
            return FALSE;
        else
            return TRUE;
    }

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PIN : <%s>", PIN);
    return FALSE;
}

/**
 * Get needed information for bill payment transaction, shenaseh & PIN.
 * @param   billSpec [input]: bill amount
 * @param   billCount [input]: count of bill
 * @see     showLog().
 * @see     getNumeric().
 * @see     checkBillcode().
 * @see     displayMessageBox().
 * @see     checkBillPaymentcode().
 * @see     removePadLeft().
 * @see     clearDisplay().
 * @return  TRUE or FALSE.
 */
uint8 enterBillPaymentInfo(billSpecST* billSpec, uint8* billCount) 
{
    uint8       buffer[14]              = {0};                  /* shenaseh in string form */
    int         counter                 = 0;                    /* loop counter */
    int16       billType                = -1;                   /* bill type */
    uint8       billAmount[12]          = {0};                  /* bill amount in string form */
    uint8       lines[10][32]           = {0};                  /* display lines, bill information */
    uint8       error                   = FALSE;
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    billSpecST  localBillSpec;  
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "****enterBillPaymentInfo****");
    
    memset(&localBillSpec, 0 , sizeof(billSpecST));
    memset(lines, 0, sizeof(lines));

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "billID%s", localBillSpec.billID);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "paymentID %s", localBillSpec.paymentID);

    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ��� :", 13, 0, buffer, TRUE, 0)) 
            return FALSE;
        
        memset(localBillSpec.billID, '0', 13);
        memcpy(&localBillSpec.billID[13 - strlen(buffer)], buffer, strlen(buffer));
        localBillSpec.billID[13] = 0;

        billType = checkBillcode(localBillSpec.billID);
        if (billType == -1)
        {
            displayMessageBox("����� ��� ���� ����!", MSG_ERROR);
            error = TRUE;
        }
        else
            break;
    }

    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ������:", 13, 0, buffer, TRUE, 0)) 
            return FALSE;

        memset(localBillSpec.paymentID, '0', 13);
        memcpy(&localBillSpec.paymentID[13 - strlen(buffer)], buffer, strlen(buffer));
        localBillSpec.paymentID[13] = 0;

        memset(tempBillID, 0, sizeof(tempBillID));
        memset(tempPaymentID, 0, sizeof(tempPaymentID));

        memcpy(tempBillID, localBillSpec.billID , strlen(localBillSpec.billID));
        strcpy(tempPaymentID, localBillSpec.paymentID);

        billType = checkBillPaymentcode(localBillSpec.billID, localBillSpec.paymentID);
        if (billType == -2)
        {
            displayMessageBox("����� ������ ���� ����!", MSG_ERROR); 
            error = TRUE;
        }
        else
            break;
    }

    localBillSpec.type = billType;

    switch (billType)
    {
/*
        case -1:
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
            break;
        case -2:
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
            break;
*/
        case -3:
            displayMessageBox("����� ��� �� ����� ������ ���� ����!", MSG_ERROR);  
            error = TRUE;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8://MRF_NEW16
        case 9://MRF_NEW16
            break;
/*
        case 1:
            strcpy(billTitle, "��� ��");
            break;
        case 2:
            strcpy(billTitle, "��� ���");
            break;
        case 3:
            strcpy(billTitle, "��� ���");
            break;
        case 4:
            strcpy(billTitle, "��� ����");
            break;
        case 5:
            strcpy(billTitle, "��� ���� �����");
            break;
        case 6:
            strcpy(billTitle, "��� �������");
            break;
*/
        default:
            displayMessageBox("��� ������� ���!", MSG_ERROR);    
            error = TRUE;
            break;
    }

    if (error)
        return FALSE;
    else
        for (counter = 0; counter < (*billCount); counter++) 
        {  
            if ((strcmp(tempBillID, billSpec[counter].billID) == 0) && 
               (strcmp(tempPaymentID, billSpec[counter].paymentID) == 0))
            {
                displayMessageBox("��� ʘ���� ���!", MSG_ERROR); 
                return NO_DISPLAY;
            }
        }

    memcpy(billAmount, localBillSpec.paymentID, 8);
    billAmount[8] = 0; 
    removePadLeft(billAmount, '0');
    strcat(billAmount, "000");
    strcpy(localBillSpec.billAmount, billAmount); 
    strcpy(billSpec[(*billCount)].billAmount, localBillSpec.billAmount); 
    strcpy(billSpec[(*billCount)].billID, localBillSpec.billID);
    strcpy(billSpec[(*billCount)].paymentID, localBillSpec.paymentID);
    billSpec[(*billCount)].type = localBillSpec.type;

    clearDisplay();

    return TRUE;
}

uint8 enterBillPaymentInfo2(billSpecST* billSpec, uint8* billCount) 
{
    uint8       buffer[14]              = {0};                  /** shenaseh in string form */
    int         counter                 = 0;                    /** loop counter */
    uint8       key                     = 0;                    /** entered key (terminal keypad) */
    int16       billType                = -1;                   /** bill type */
    uint8       billTitle[25]           = {0};                  /** bill type title */
    uint8       billAmount[12]          = {0};                  /** bill amount in string form */
    uint8       lines[10][32]           = {0};                  /** display lines, bill information */
    uint8       error                   = FALSE;
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    uint16      i                       = 0;
    uint8       amountWithComma[13]     = {0};
    billSpecST  localBillSpec;  
    uint8       headerBill              =  {0};
       
    memset(&localBillSpec, 0 , sizeof(billSpecST));
    memset(lines, 0, sizeof(lines));

    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ��� :", 13, 0, buffer, TRUE, 0)) 
            return FALSE;

        memset(localBillSpec.billID, '0', 13);
        memcpy(&localBillSpec.billID[13 - strlen(buffer)], buffer, strlen(buffer));
        localBillSpec.billID[13] = 0;

        billType = checkBillcode(localBillSpec.billID);
        if (billType == -1)
        {
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
        }
        else
            break;
    }

    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ������:", 13, 0, buffer, TRUE, 0)) 
            return FALSE;

        memset(localBillSpec.paymentID, '0', 13);
        memcpy(&localBillSpec.paymentID[13 - strlen(buffer)], buffer, strlen(buffer));
        localBillSpec.paymentID[13] = 0;

        memset(tempBillID, 0, sizeof(tempBillID));
        memset(tempPaymentID, 0, sizeof(tempPaymentID));

        memcpy(tempBillID, localBillSpec.billID , strlen(localBillSpec.billID));
        strcpy(tempPaymentID, localBillSpec.paymentID);

        billType = checkBillPaymentcode(localBillSpec.billID, localBillSpec.paymentID);
        if (billType == -2)
        {
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
        }
        else
            break;
    }

    localBillSpec.type = billType;
    switch (billType)
    {
/*
        case -1:
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
            break;
        case -2:
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
            break;
*/
        case -3:
            displayMessageBox("����� ��� �� ����� ������ ���� ����", MSG_ERROR);  
            error = TRUE;
            break;
        case 1:
            strcpy(billTitle, "��� ���");
            headerBill = waterBill;
            break;
        case 2:
            strcpy(billTitle, "��� ���");
            headerBill = electricBill;
            break;
        case 3:
            strcpy(billTitle, "��� ���");
            headerBill = gasBill;
            break;
        case 4:
            strcpy(billTitle, "��� ����");
            headerBill = phoneBill;
            break;
        case 5:
            strcpy(billTitle, "��� ���� �����");
            headerBill = cellPhone;
            break;
        case 6:
            strcpy(billTitle, "��� �������");
            headerBill = municipalBills;
            break;
        case 8://MRF_NEW16
            strcpy(billTitle, "��� ������");
            headerBill = tax;
            break;
        case 9://MRF_NEW16
            strcpy(billTitle, "��� ����� �������� � ����ϐ�");
            headerBill = penalty;
            break;
        default:
            displayMessageBox("��� ������� ���.", MSG_ERROR);    
            error = TRUE;
            break;
    }
    
    if (error)
        return FALSE;
    else
        for (counter = 0; counter < (*billCount); counter++) 
        {  
            if ((strcmp(tempBillID, billSpec[counter].billID) == 0) && 
               (strcmp(tempPaymentID, billSpec[counter].paymentID) == 0))
            {
                displayMessageBox("��� ʘ���� ���", MSG_ERROR); 
                return TRUE;
            }
        }

    memcpy(billAmount, localBillSpec.paymentID, 8);
    billAmount[8] = 0; 
    removePadLeft(billAmount, '0');
    strcat(billAmount, "000");
    
    strcpy(localBillSpec.billAmount, billAmount); 
#ifndef CASTLES_V5S   
    /** BILL INFORMATION, LINE 0 : BILL TYPE TITLE */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, billTitle);
#endif
    
    /** BILL INFORMATION, LINE 1 : SHENASEH GHABZ HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ���");
    
    /** BILL INFORMATION, LINE 2 : SHENASEH GHABZ VALUE */
    removePadLeft(tempBillID,'0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempBillID);
    
    /** BILL INFORMATION, LINE 3 : SHENASEH PARDAKHT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ������");
    
    /** BILL INFORMATION, LINE 4 : SHENASEH PARDAKHT VALUE */
    removePadLeft(tempPaymentID,'0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempPaymentID);
    
    /** BILL INFORMATION, LINE 5 : AMOUNT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����");
    
    /** BILL INFORMATION, LINE 6 : AMOUNT VALUE */
    makeValueWithCommaStr(billAmount, amountWithComma);
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, amountWithComma, " ����");
    
    key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, TRUE, headerBill);
    if (key == KBD_ENTER || key == KBD_CONFIRM) 
    {
        strcpy(billSpec[(*billCount)].billAmount, localBillSpec.billAmount); 
        strcpy(billSpec[(*billCount)].billID, localBillSpec.billID);
        strcpy(billSpec[(*billCount)].paymentID, localBillSpec.paymentID);
        billSpec[(*billCount)].type = localBillSpec.type;
        (*billCount)++;
        clearDisplay();
        
        return TRUE;

    }
    else if (key == KBD_CANCEL || key == KBD_REJECT)
    {
    	clearDisplay();
	    return FALSE;
    }
}


/**
 * get needed information for bill payment transaction, shenaseh & PIN.
 * @param   amount bill amount.
 * @param   PIN card PIN.
 * @param   shGhbzPrdkht shenaseh ghabz & shenaseh pardakht.
 * @see     getStringValue()
 * @see     checkBillcode()
 * @see     displayScrollable()
 * @see     getPass()
 * @return  TRUE or FALSE.
 */
uint8 readBarCodeBillPaymentInfo(billSpecST* billSpec, uint8* billCount) 
{
    uint8       buffer[32]              = {0};                  /* shenaseh in string form */
    int         counter                 = 0;                    /* loop counter */
    uint8       key                     = 0;                    /* entered key (terminal keypad) */
    int16       billType                = -1;                   /* bill type */
    uint8       billTitle[25]           = {0};                  /* bill type title */
    uint8       billAmount[12]          = {0};                  /* bill amount in string form */
    uint8       lines[10][32]           = {0};                  /* display lines, bill information */
    uint8       error                   = FALSE;
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    uint16      i                       = 0;
    uint8       amountWithComma[13]     = {0};
    uint16		retValue				= FAILURE;
    billSpecST  localBillSpec; 
    uint8       headerBill              ={0};
       
    clearDisplay();

    memset(&localBillSpec, 0 , sizeof(billSpecST));
    memset(lines, 0, sizeof(lines));
    memset(buffer, 0, sizeof(buffer));
    
    displayStringFarsi("����� �Ә� ���...", DISPLAY_START_LINE + 1, PRN_NORM, ALIGN_CENTER);
    
    retValue = readBarcode(buffer, 30000); // wait 30 second 
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", buffer);
    if (retValue == SUCCESS)
    {
    	//read bill ID
    	memset(localBillSpec.billID, '0', 13);
        memcpy(&localBillSpec.billID, buffer, 13);
        localBillSpec.billID[13] = 0;
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "billID%s", localBillSpec.billID);
    	billType = checkBillcode(localBillSpec.billID);
        if (billType == -1)
        {
          //mgh  displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
        }
        
        //read payment ID
        memset(localBillSpec.paymentID, '0', 13);
        memcpy(&localBillSpec.paymentID, &buffer[13], 13);
        localBillSpec.paymentID[13] = 0;
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "paymentID%s", localBillSpec.paymentID);

        memset(tempBillID, 0, sizeof(tempBillID));
        memset(tempPaymentID, 0, sizeof(tempPaymentID));

        memcpy(tempBillID, localBillSpec.billID , strlen(localBillSpec.billID));
        strcpy(tempPaymentID, localBillSpec.paymentID);

        billType = checkBillPaymentcode(localBillSpec.billID, localBillSpec.paymentID);
        if (billType == -2)
        {
         //mgh   displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
        }	
    }
    else 
    	return FALSE;

    localBillSpec.type = billType;

    switch (billType)
    {
        case -1:
/*
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
            break;
*/
        case -2:
/*
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
            break;
*/
        case -3:
            displayMessageBox("����� ��� �� ����� ������ ���� ����", MSG_ERROR);  
            error = TRUE;
            break;
        case 1:
            strcpy(billTitle, "��� ��");
            headerBill = waterBill;
            break;
        case 2:
            strcpy(billTitle, "��� ���");
            headerBill = electricBill; 
            break;
        case 3:
            strcpy(billTitle, "��� ���");
            headerBill =  gasBill;
            break;
        case 4:
            strcpy(billTitle, "��� ����");
            headerBill = phoneBill;
            break;
        case 5:
            strcpy(billTitle, "��� ���� �����");
            headerBill = cellPhone;
            break;
        case 6:
            strcpy(billTitle, "��� �������");
            headerBill = municipalBills;
            break;
        case 8://MRF_NEW16
            strcpy(billTitle, "��� ������");
            headerBill = tax;
            break;
        case 9://MRF_NEW16
            strcpy(billTitle, "��� ����� �������� � ����ϐ�");
            headerBill = penalty;
            break;
        default:
            displayMessageBox("��� ������� ���.", MSG_ERROR);    
            error = TRUE;
            break;
    }

    if (error)
        return FALSE;
    else
        for (counter = 0; counter < (*billCount); counter++) 
        {  
            if ((strcmp(tempBillID, billSpec[counter].billID) == 0) && 
               (strcmp(tempPaymentID, billSpec[counter].paymentID) == 0))
            {
                displayMessageBox("��� ʘ���� ���", MSG_ERROR); 
                return TRUE;
            }
        }

    memcpy(billAmount, localBillSpec.paymentID, 8);
    billAmount[8] = 0; 
    removePadLeft(billAmount, '0');
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "amount: %s", billAmount);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "readBarCodeBillPaymentInfo", "amount: %s", billAmount);
    //showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "size: %d", strlen(billAmount));

    strcat(billAmount, "000");
    
    strcpy(localBillSpec.billAmount, billAmount); 
    
    //deleted clearDisplay();
#ifndef CASTLES_V5S
    /** BILL INFORMATION, LINE 0 : BILL TYPE TITLE */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, billTitle);
#endif
    
    /** BILL INFORMATION, LINE 1 : SHENASEH GHABZ HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ���");
    
    /** BILL INFORMATION, LINE 2 : SHENASEH GHABZ VALUE */
    removePadLeft(tempBillID,'0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempBillID);
    
    /** BILL INFORMATION, LINE 3 : SHENASEH PARDAKHT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ������");
    
    /** BILL INFORMATION, LINE 4 : SHENASEH PARDAKHT VALUE */
    removePadLeft(tempPaymentID,'0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempPaymentID);
    
    /** BILL INFORMATION, LINE 5 : AMOUNT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����");
    
    /** BILL INFORMATION, LINE 6 : AMOUNT VALUE */
    makeValueWithCommaStr(billAmount, amountWithComma);
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, amountWithComma, " ����");
    
    key = displayScrollableWithHeader("", lines, i,DISPLAY_TIMEOUT, TRUE, headerBill);
    if (key == KBD_ENTER || key == KBD_CONFIRM) 
    {
        strcpy(billSpec[(*billCount)].billAmount , localBillSpec.billAmount); 
        strcpy(billSpec[(*billCount)].billID , localBillSpec.billID);
        strcpy(billSpec[(*billCount)].paymentID , localBillSpec.paymentID);
        billSpec[(*billCount)].type = localBillSpec.type;
        (*billCount)++;
        clearDisplay();
        return TRUE;
    } 
    else if (key == KBD_CANCEL || key == KBD_REJECT)
    {
    	clearDisplay();
	    return FALSE;
    }
}

/**
 * Get needed information for multi bill payment transaction.
 * @param   PIN [input]: PIN number
 * @param   billSpec [input]: bill amount
 * @param   billCount [input]: count of bill
 * @see     selectSmallDisplayFontFarsi().
 * @see     getPass().
 * @see     showLog().
 * @see     clearDisplay().
 * @see     getNumeric().
 * @see     checkBillcode().
 * @see     displayMessageBox().
 * @see     padLeft().
 * @see     removePadLeft().
 * @return  TRUE or FALSE.
 */
uint8 enterMultiBillPaymentInfo(uint8* PIN, billSpecST* billSpec, uint8* billCount) 
{
    uint8       buffer[14]              = {0};                  /* shenaseh in string form */
    int         counter                 = 0;                    /* loop counter */
    int16       billType                = -1;                   /* bill type */
    uint8       billAmount[12]          = {0};                  /* bill amount in string form */
    uint8       error                   = FALSE;
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    billSpecST  localBillSpec ;
       
   
    
    memset(&localBillSpec, 0 , sizeof(billSpecST));
    
    //CM BY MRF
//    if (strcmp(PIN, "") == 0) 
//    {
//        selectSmallDisplayFont();
//        memset(PIN, 0, 5);
//        if (!getPass("", PIN, 4, 2))
//            return FALSE ;        
//    }

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "billID%s", localBillSpec.billID);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "paymentID %s", localBillSpec.paymentID);
    
    clearDisplay();
    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ��� :", 13, 0, buffer, TRUE, 0)) 
            return FALSE;

        memset(localBillSpec.billID, '0', 13);
        memcpy(&localBillSpec.billID[13 - strlen(buffer)], buffer, strlen(buffer));
        localBillSpec.billID[13] = 0;

        billType = checkBillcode(localBillSpec.billID);
        if (billType == -1)
        {
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
        }
        else
            break;
    }

    while (TRUE)
    {
        error = FALSE;
        memset(buffer, 0, 14);
        if (!getNumeric("����� ������:", 13, 0, buffer, TRUE, 0)) 
            return FALSE;

        memset(localBillSpec.paymentID, '0', 13);
//        memcpy(&localBillSpec.paymentID[13 - strlen(buffer)], buffer, strlen(buffer));
//        localBillSpec.paymentID[13] = 0;
        memcpy(localBillSpec.paymentID, buffer, strlen(buffer));
        localBillSpec.paymentID[strlen(buffer)] = 0;

        memset(tempBillID, 0, sizeof(tempBillID));
        memset(tempPaymentID, 0, sizeof(tempPaymentID));

        memcpy(tempBillID, localBillSpec.billID , strlen(localBillSpec.billID));
        strcpy(tempPaymentID, localBillSpec.paymentID);

        billType = checkBillPaymentcode(localBillSpec.billID, localBillSpec.paymentID);
        if (billType == -2)
        {
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
        }
        else
            break;
    }

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "billID%s", localBillSpec.billID);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "paymentID %s", localBillSpec.paymentID);

    localBillSpec.type = billType;

    switch (billType)
    {
/*
        case -1:
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
            break;
        case -2:
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
            break;
*/
        case -3:
            displayMessageBox("����� ��� �� ����� ������ ���� ����", MSG_ERROR);  
            error = TRUE;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8://MRF_NEW16
        case 9://MRF_NEW16
            break;
        default:
            displayMessageBox("��� ������� ���.", MSG_ERROR);    
            error = TRUE;
            break;
    }
    
    if (error)
        return FALSE;
    else
        for (counter = 0; counter < (*billCount); counter++) 
        {  
            if ((strcmp(tempBillID, billSpec[counter].billID) == 0) && 
               (strcmp(tempPaymentID, billSpec[counter].paymentID) == 0))
            {
                displayMessageBox("��� ʘ���� ���", MSG_ERROR); 
                return NO_DISPLAY;
            }
        }

    padLeft(tempPaymentID, 13);
    memcpy(billAmount, tempPaymentID, 8);
    billAmount[8] = 0; 
    removePadLeft(billAmount, '0');

    strcat(billAmount, "000");
    strcpy(localBillSpec.billAmount, billAmount); 
    
    strcpy(billSpec[(*billCount)].billAmount, localBillSpec.billAmount); 
    strcpy(billSpec[(*billCount)].billID, localBillSpec.billID);
    strcpy(billSpec[(*billCount)].paymentID, localBillSpec.paymentID);
    billSpec[(*billCount)].type = localBillSpec.type;
    
    return TRUE;
}

uint8 enterMultiBillPaymentInfo2(uint8* PIN, billSpecST* billSpec, uint8* billCount) 
{
    uint8 	first   = TRUE;
    uint8	key		= KBD_CANCEL;
    
    memset(PIN, 0, 5);
//MGH
//    if (!getPass("��� ����:", PIN, 4))
//        return FALSE ;
    
        
    while (*billCount < 100)
    {
        if (!first)
        {
        	key = displayMessageBox("��� ��� ���� �� ������ �� ���Ͽ", MSG_CONFIRMATION);
            if (key != KBD_ENTER && key != KBD_CONFIRM)
            {
                clearDisplay(); 
                break;
            }
        }

        if (!enterBillPaymentInfo(billSpec, billCount) && first)
            return FALSE;
        first = FALSE;
    }
    
    return TRUE;
}


uint8 readMultiBarCodeBillPaymentInfo(uint8* PIN, billSpecST* billSpec, uint8* billCount) 
{
    uint8       buffer[32]              = {0};                  /** shenaseh in string form */
    int         counter                 = 0;                    /** loop counter */
    int16       billType                = -1;                   /** bill type */
    uint8       billAmount[12]          = {0};                  /** bill amount in string form */
    uint8       error                   = FALSE;
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    uint16		retValue				= FAILURE;
    billSpecST  localBillSpec;  
       
    clearDisplay();

    memset(&localBillSpec, 0 , sizeof(billSpecST));
    memset(buffer, 0, sizeof(buffer));
    //CM BY MRF
//    if (strcmp(PIN, "") == 0) 
//    {
//        memset(PIN, 0, 5);
//
//        if (!getPass("", PIN, 4, 2))
//            return FALSE ;
//    }
    
    //MRF
    clearDisplay();
    displayStringFarsi("����� �Ә� ���...", DISPLAY_START_LINE + 1, PRN_NORM, ALIGN_CENTER);
    
    retValue = readBarcode(buffer, 30000); // wait 30 second
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", buffer);
    if (retValue == SUCCESS)
    {
    	//read bill ID
    	memset(localBillSpec.billID, '0', 13);
        memcpy(&localBillSpec.billID, buffer, 13);
        localBillSpec.billID[13] = 0;
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "billID%s", localBillSpec.billID);
    	billType = checkBillcode(localBillSpec.billID);
        if (billType == -1)
        {
          //mgh  displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
        }
        
        //read payment ID
        memset(localBillSpec.paymentID, '0', 13);
        memcpy(&localBillSpec.paymentID, &buffer[13], 13);
        localBillSpec.paymentID[13] = 0;
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "paymentID%s", localBillSpec.paymentID);

        memset(tempBillID, 0, sizeof(tempBillID));
        memset(tempPaymentID, 0, sizeof(tempPaymentID));

        memcpy(tempBillID, localBillSpec.billID , strlen(localBillSpec.billID));
        strcpy(tempPaymentID, localBillSpec.paymentID);

        billType = checkBillPaymentcode(localBillSpec.billID, localBillSpec.paymentID);
        if (billType == -2)
        {
        //mgh   displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
        }	
    }
    else 
    	return FALSE;

    localBillSpec.type = billType;

    switch (billType)
    {
        case -1:
/*
            displayMessageBox("����� ��� ���� ����", MSG_ERROR);
            error = TRUE;
            break;
*/
        case -2:
/*
            displayMessageBox("����� ������ ���� ����", MSG_ERROR); 
            error = TRUE;
            break;
*/
        case -3:
            displayMessageBox("����� ��� �� ����� ������ ���� ����", MSG_ERROR);  
            error = TRUE;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 8://MRF_NEW16
        case 9://MRF_NEW16
            break;
        default:
            displayMessageBox("��� ������� ���.", MSG_ERROR);    
            error = TRUE;
            break;
    }

    if (error)
        return FALSE;
    else
        for (counter = 0; counter < (*billCount); counter++) 
        {  
            if ((strcmp(tempBillID, billSpec[counter].billID) == 0) && 
               (strcmp(tempPaymentID, billSpec[counter].paymentID) == 0))
            {
                 displayMessageBox("��� ʘ���� ���", MSG_ERROR);
                return NO_DISPLAY;
            }
        }

    memcpy(billAmount, localBillSpec.paymentID, 8);
    billAmount[8] = 0; 
    removePadLeft(billAmount, '0');
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "amount: %s", billAmount);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "readBarCodeBillPaymentInfo", "amount: %s", billAmount);

    strcat(billAmount, "000");
    strcpy(localBillSpec.billAmount, billAmount); 
    
    strcpy(billSpec[(*billCount)].billAmount , localBillSpec.billAmount); 
    strcpy(billSpec[(*billCount)].billID , localBillSpec.billID);
    strcpy(billSpec[(*billCount)].paymentID , localBillSpec.paymentID);
    billSpec[(*billCount)].type = localBillSpec.type;
    
    return TRUE;
}


uint8 readMultiBarCodeBillPaymentInfo2(uint8* PIN, billSpecST* billSpec, uint8* billCount) 
{
    uint8 	first   = TRUE;
    uint8	key		= KBD_CANCEL;
    
    memset(PIN, 0, 5);
//MGH
//    if (!getPass("��� ����:", PIN, 4))
//        return FALSE ;
	
        
    while (*billCount < 100)
    {
        if (!first)
        {
        	key = displayMessageBox("��� ��� ���� �� ������ �� ���Ͽ", MSG_CONFIRMATION); 
            if (key != KBD_ENTER && key != KBD_CONFIRM)
            {
                clearDisplay(); 
                break;
            }
        }

        if (!readBarCodeBillPaymentInfo(billSpec, billCount) && first)
            return FALSE;
        first = FALSE;
    }
    
    return TRUE;
}


uint8 enterBuyChargeInfo(filesST* files, uint8* PIN, uint8* chargeCount, uint8* chargeAmount, uint8 chargeType) 
{
    uint8 neededNumStr[15]	= {0};			/** needed charge number in string form */
    int   neededNum         = 0;                    /** temp needed number */
    int   counter           = 0;                    /** loop counter */
    uint8 multiCharge		= FALSE;
  
    switch (chargeType) 
    {
        case MULTI_IRANCELL:
        case MULTI_MCI:
        case MULTI_RIGHTEL:
            multiCharge = TRUE;
            break;
        default:
            break;
    }

    if (multiCharge) 
    {
        if (!getStringValue("����� ��ю:", 2, 1, neededNumStr, 0, 0, 0, chargeAmount, 0)) 
            return FALSE;
        
        for (counter = 0; counter < strlen((const char*)neededNumStr); counter++) 
            neededNum = neededNum * 10 + (neededNumStr[counter] - '0');
        
        *chargeCount = neededNum;
    } 
    else 
        *chargeCount = 1;
    
    memset(PIN, 0, 5);
   
    if (!getPass("��� ����:", PIN, 4, 2))
        return FALSE ;
    
    return TRUE;
}



//uint8 enterBuyMultiChargeInfo(uint8* PIN, buyChargeSpecST* buyCharge, int* chargeTypeCount, int* multiTypeCharge ,uint8 offline)  
//{
//    uint8   neededNumStr[15]    = {0};          /** needed charge number in string form */
//    int     neededNum           = 0;            /** temp needed number */
//    int     counter             = 0;            /** loop counter */
//    uint8   key                 = KBD_CANCEL;
//    
//    memset(neededNumStr, 0, 15);
//    
//    while (strlen(neededNumStr) == 0)
//        if (!getNumeric("����� �� ���� ������:", 2, 0, neededNumStr, FALSE, FALSE))
//            return FALSE;
//
//    for (counter = 0; counter < strlen(neededNumStr); counter++)
//        neededNum = neededNum * 10 + (neededNumStr[counter] - '0');
//    buyCharge->productTypeCount += neededNum;
//   
//    key = displayMessageBox("��� ��ю ���� �� ���� �� ���Ͽ", MSG_CONFIRMATION);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "before confirm");
//    if (key != KBD_ENTER && key != KBD_CONFIRM)
//    {
//        memset(PIN, 0, 5);
//
//        //MGH
////        iif (!getPass("��� ����:", PIN, 4))
////            return FALSE ;
//        //MRF
//        if (!getPass("", PIN, 4, 2))
//            return FALSE;
//        
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "PIN: %d", PIN);
//
////        *multiTypeCharge = FALSE;
//    }
//    else
//    {
////        (*chargeTypeCount)++; MRF
//        *multiTypeCharge = TRUE;
//    }
//
//    return TRUE;
//}


uint8 enterCardBillInfo(uint8* PIN) 
{    
    memset(PIN, 0, 5);
  
     if (!getPass("��� ����:", PIN, 4, 2))
        return FALSE ;
    
    return TRUE;
}




/**
 * This function check bill code correctness.
 * @param   billID [input]: billID(shenase ghabz)
 * @see     removePadLeft().
 * @return  TRUE or FALSE.
 */
int16 checkBillcode(uint8* billID) 
{
    int16 billType                  = -1;                           /* bill type */
    uint8 localBillID[13 + 1]       = {0};                
    int   i                         = 0;
    int   j                         = 0;
    int   sum                       = 0;
    int   codes[6]                  = {2, 3, 4, 5, 6, 7};           /* control digits */
    int   len                       = 0;                            /* lenght of "shenaseh" variable */
    
    strcpy(localBillID, billID);
    
    billType = localBillID[11] - '0';
    
    removePadLeft(localBillID, '0');        
    len = strlen(localBillID);

    for (sum = 0, j = 0, i = len - 2; i >= 0; i--)
    {
         sum += (localBillID[i] - '0') * codes[j];
         j = (j < 5) ? j + 1 : 0;
    }

    i = sum % 11;
    i = (i > 1) ? 11 - i : 0;

    if (localBillID[len - 1] != i + '0')
        return -1;
    
    return TRUE;
}


/**
 * Check bill ID and payment ID correctness.
 * @param   billID [input]: shenase ghabz
 * @param   paymentID [input]: shenase pardakht
 * @see     removePadLeft().
 * @return  bill type or error code.
 */
int16 checkBillPaymentcode(uint8* billID, uint8* paymentID) 
{
    int16 billType                  = -1;                    /* bill type */
    uint8 billPaymentID[27]         = {0};            
    uint8 localBillID[13 + 1]       = {0};                
    uint8 localPaymentID[13 + 1]    = {0};                 
    int   i                         = 0;
    int   j                         = 0;
    int   sum                       = 0;
    int   codes[6]                  = {2, 3, 4, 5, 6, 7};    /* control digits */
    int   len                       = 0;                     /* lenght of "shenaseh" variable */
    
    strcpy(localBillID, billID);
    strcpy(localPaymentID, paymentID);
    
    billType = localBillID[11] - '0';
    
    removePadLeft(localBillID, '0');        
    len = strlen(localBillID);

    for (sum = 0, j = 0, i = len - 2; i >= 0; i--)
    {
         sum += (localBillID[i] - '0') * codes[j];
         j = (j < 5) ? j + 1 : 0;
    }

    i = sum % 11;
    i = (i > 1) ? 11 - i : 0;

    removePadLeft(localPaymentID, '0');
    len = strlen(localPaymentID);

    for (sum = 0, j = 0, i = len - 3 ; i >= 0 ; i--)
    {
         sum += (localPaymentID[i] - '0') * codes[j];
         j = (j < 5) ? j + 1 : 0;
    }

    i = sum % 11;
    i = (i > 1) ? 11 - i : 0;
    
    if (localPaymentID[len - 2] != i + '0')
        return -2;

    strcpy(billPaymentID, localBillID);
    strcat(billPaymentID, localPaymentID);    
    len = strlen(billPaymentID);

    for (sum = 0, j = 0, i = len - 2 ; i >= 0 ; i--)
    {
         sum += (billPaymentID[i] - '0') * codes[j];
         j = (j < 5) ? j + 1 : 0;
    }

    i = sum % 11;
    i = (i > 1) ? 11 - i : 0;

    if (billPaymentID[len - 1] != i + '0')
        return -3;

    return billType;
}

/**
 * Check bill code payoff.
 * @param   buffer [input]: buffer to store code
 * @return  error code.
 */
int16 checkBillcodePayOff(uint8* buffer)
{
	int16 	series[] 	= {7, 5, 3, 2, 21, 19, 17, 13, 11, 7, 5, 3, 2};
	int16 	checkDigit 	= 0;
	int16 	digit;
	uint8	i			= 0;
	
	if (strlen(buffer) < 15)
		padLeft(buffer, 15);
	
	for(i = 0; i < 13; i++)
    	checkDigit += (buffer[i] - '0') * series[i];
    
    checkDigit += 26;
    checkDigit %=19;
    
    digit = atoi(&buffer[14]);
    if (checkDigit != digit)
    {
    	return FALSE;
    }
    return TRUE;
}

///**   THIS FUNCTION IS FOR WHEN HAVE VARIABLE AMOUNT
// * Enter buy top up Charge information.
// * @param   mobileNoToReturn [input]: the mobile number to return
// * @param   amount [input]: amount of buy
// * @param   PIN [input]: PIN number
// * @see     getPass().
// * @see     showLog().
// * @see     getStringValue().
// * @see     displayMessageBox().
// * @see     compareStringNumbers().
// * @see     displayMessageBox().
// * @return  TRUE or FALSE.
// */
//uint8 enterBuyTopUpChargeInfo(topupST* topup, uint8* amount, uint8* PIN) 
//{
//    uint8   valueStr[15]                    = {0};      /* buy amount in string form */
//    uint8   mobileNo[10]                    = {0};      /* phone number to be charged.*/
//    uint8   selectedItemIndex               = 0;
//    uint8   selectionItemsOperator[][20]    = {"MCI", "IRANCELL"};
//    uint8   selectionItemsType[][20]        = {"NORMAL", "AMAZING"};
//    
//    memset(PIN, 0, 5);
// 
//    if (!getPass("", PIN, 4, 2))
//        return FALSE ;
//        
//    clearDisplay();
//    while (TRUE)
//    {
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " geting phone ");
//        memset(mobileNo, 0, sizeof(mobileNo));
//        
//        if (!getStringValue("����� ���� �� ���� ������:", 10, 1, mobileNo, FALSE,  FALSE, FALSE, "0", 0)) 
//            return FALSE;
//
//        if (strlen(mobileNo) != 10)
//        {
//             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
//             showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "len error ");
//             continue;
//        }
//        if (strncmp(mobileNo, "9", 1) != 0)
//        {
//             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
//             showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "digit  error ");
//             continue;
//        }
//
////        if (((mobileNo[2] - '0') < 5) || ((mobileNo[2] - '0') > 9))
////        {
////             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
////             showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " digit error ");
////             continue;
////        }
//        strcpy(topup->mobileNo, mobileNo);
//        break;
//    }
//
//    clearDisplay();
//    
//    //OPERATOR TYPE
//    if (selectItemPage("Operator Type:", selectionItemsOperator, 2, &selectedItemIndex, DISPLAY_TIMEOUT))
//    {
//        if (selectedItemIndex == MULTI_MCI)
//        {
//            topup->operatorType = 2;
//            showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " HAMRAHE AVAL: %d ", topup->operatorType);
//            //TODO: WHAT IS CHARGE TYPE IN FIELD63
//        }
//        else if (selectedItemIndex == MULTI_IRANCELL)
//        {
//            topup->operatorType = 1;
//            showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " IRANCELL: %d ", topup->operatorType);
//            //CHARGE TYPE
//            if (selectItemPage("Charge Type:", selectionItemsType, 2, &selectedItemIndex, DISPLAY_TIMEOUT))
//            {
//                if (selectedItemIndex == NORMAL)
//                {
//                    topup->type = 0;
//                    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " NORMAL: %d ", topup->type);
//                }
//                else if (selectedItemIndex == AMAZING)
//                {
//                    topup->type = 1;
//                    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " AMAZING: %d ", topup->type);
//                }
//
//                displayforeColor(BLUE);
//                clearDisplay();
//            }
//            else
//            {
//                displayforeColor(BLUE);
//                clearDisplay();
//                return FALSE;
//            }
//        }
//        
//        displayforeColor(BLUE);
//        clearDisplay();
//    }
//    else
//    {
//        displayforeColor(BLUE);
//        clearDisplay();
//        return FALSE;
//    }
//    
////    clearDisplay();
//    while (TRUE)
//    {
//        memset(valueStr, 0, 15);
//        if (!getStringValue("���� ����:", 6, 1, valueStr, 0,  0, 1, "0", 0)) 
//            return FALSE;
//
//        strcpy(amount, valueStr); 
//
//        if ((compareStringNumbers(amount, "10000") < 0) || (compareStringNumbers(amount, "500000") > 0))
//            displayMessageBox("���� ���� ��� ���� ����!", MSG_ERROR);
//        else
//            break;
//    }
//    
//    strcpy(topup->amount, amount);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " mobileNO: %s ", topup->mobileNo);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " charge Type: %d ", topup->type);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " Operator: %d ", topup->operatorType);
//    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", " amount: %s ", topup->amount);
//    
//    return TRUE ;
//}

/**
 * Get date distance from user in gerigorian format.
 * @param   startDate [input]: start date
 * @param   endDate [input]: end date
 * @see     systemDateTimeToDate().
 * @see     gregorianToJalali().
 * @see     getDateFromUser().
 * @see     jalaliToGregorian().
 * @see     displayMessageBox().
 * @see     clearDisplay().
 * @return  TRUE or FALSE.
 */
uint8 getDateDistanceFromUser(uint32* startDate, uint32* endDate)
{
    uint32       userDateJalali         = 0;            /** entered date by user (jalali) */
    uint32       userDateGerigorianFrom	= 0;            /** entered date by user (gerigorian), start date */
    uint32       userDateGerigorianTo	= 0;            /** entered date by user (gerigorian), end date */
    uint32       currentDate            = 0;            /** current date */
    uint8        getDateRes             = KBD_TIMEOUT;
    
//    deleted clearDisplay();
    
    currentDate = systemDateTimeToDate();
    
    gregorianToJalali(&userDateJalali, currentDate);
    
    while (TRUE) 
    {
        getDateRes = KBD_TIMEOUT;
        
        getDateRes = getDateFromUser("�� �����:", FALSE, &userDateJalali);
        if (getDateRes == KBD_CANCEL || getDateRes == KBD_TIMEOUT) 
            return FALSE;

        jalaliToGregorian(&userDateGerigorianFrom , userDateJalali);

        if (currentDate < userDateGerigorianFrom) 
        {
            displayMessageBox("����� ���� ��� ����� ����", MSG_ERROR);
            continue;
        }
        else 
            break;
    }
        
    gregorianToJalali(&userDateJalali, currentDate);
    
    while (TRUE) 
    {
        getDateRes = KBD_TIMEOUT;
        
        getDateRes = getDateFromUser("�� �����:", FALSE, &userDateJalali);
        if (getDateRes == KBD_CANCEL || getDateRes == KBD_TIMEOUT) 
            return FALSE;

        jalaliToGregorian(&userDateGerigorianTo , userDateJalali);

        if (currentDate < userDateGerigorianTo) 
        {
            displayMessageBox("����� ���� ��� ����� ����", MSG_ERROR);
            continue;
        }
        else if (userDateGerigorianTo < userDateGerigorianFrom) 
        {
            displayMessageBox("���� ����� ����� ����", MSG_ERROR);
            continue;
        }
        else 
            break;
    }
    
    clearDisplay();
    
    *startDate = userDateGerigorianFrom;
    *endDate = userDateGerigorianTo;
    return TRUE;
}

/**
 * Get date and time Distance from user.
 * @param   startDateTime [input]: start of date and time
 * @param   endDateTime [input]: end of date and time
 * @param   activeUserTitle [input]: active user title
 * @param   activeUser [input]: active user
 * @see     clearDisplay().
 * @see     gregorianToJalali().
 * @see     getDateFromUser().
 * @see     jalaliToGregorian().
 * @see     displayMessageBox().
 * @see     getTimeFromUser().
 * @see     clearDisplay().
 * @return  TRUE or FALSE.
 */
uint8 getDateTimeDistanceFromUser(dateTimeST* startDateTime, dateTimeST* endDateTime, uint8* activeUserTitle, 
									uint8* activeUser)
{
    uint32       userDateJalali         = 0;            /* entered date by user (jalali) */
    uint32       userDateGerigorianFrom	= 0;            /* entered date by user (gerigorian), start date */
    uint32       userDateGerigorianTo	= 0;            /* entered date by user (gerigorian), end date */
    uint32       currentDate            = 0;            /* current date */
    uint8        getDateRes             = KBD_TIMEOUT;
    uint32       userTimeJalali         = 0;            /* entered date by user (jalali) */
    uint32       userTimeGerigorianFrom	= 0;            /* entered date by user (gerigorian), start date */
    uint32       userTimeGerigorianTo	= 0;            /* entered date by user (gerigorian), end date */
    uint32       currentTime            = 0;            /* current date */
    uint8        getTimeRes             = KBD_TIMEOUT;
    uint8 		 title[15 + 1]			= {0};
    
    currentDate = systemDateTimeToDate();
    
    // DATE FROM
    clearDisplay();
    gregorianToJalali(&userDateJalali, currentDate);
    
    while (TRUE) 
    {
        getDateRes = KBD_TIMEOUT;
        
        getDateRes = getDateFromUser("�� �����:", FALSE, &userDateJalali);
        if (getDateRes == KBD_CANCEL || getDateRes == KBD_TIMEOUT) 
            return FALSE;

        jalaliToGregorian(&userDateGerigorianFrom , userDateJalali);

        if (currentDate < userDateGerigorianFrom) 
        {
            displayMessageBox("����� ���� ��� ����� ����", MSG_ERROR);
            continue;
        }
        else 
            break;
    }
    
    // TIME FROM
    clearDisplay();
    userTimeJalali = currentTime;
    getTimeRes = KBD_TIMEOUT;
    getTimeRes = getTimeFromUser("�� ����:", FALSE, &userTimeJalali);
    if (getTimeRes == KBD_CANCEL || getTimeRes == KBD_TIMEOUT) 
        return FALSE;
    userTimeGerigorianFrom = userTimeJalali;
    
    // DATE TO
    clearDisplay();
    gregorianToJalali(&userDateJalali, currentDate);
    
    while (TRUE) 
    {
        getDateRes = KBD_TIMEOUT;
        
        getDateRes = getDateFromUser("�� �����:", FALSE, &userDateJalali);
        if (getDateRes == KBD_CANCEL || getDateRes == KBD_TIMEOUT) 
            return FALSE;

        jalaliToGregorian(&userDateGerigorianTo , userDateJalali);

        if (currentDate < userDateGerigorianTo) 
        {
            displayMessageBox("����� ���� ��� ����� ����", MSG_ERROR);
            continue;
        }
        else if (userDateGerigorianTo < userDateGerigorianFrom) 
        {
            displayMessageBox("���� ����� ����� ����", MSG_ERROR);
            continue;
        }
        else 
            break;
    }
    
    // TIME TO
    clearDisplay();
    currentTime = setTime(24, 0, 0);
    userTimeJalali = currentTime;
    getTimeRes = KBD_TIMEOUT;
    getTimeRes = getTimeFromUser("�� ����:", FALSE, &userTimeJalali);
    if (getTimeRes == KBD_CANCEL || getTimeRes == KBD_TIMEOUT) 
        return FALSE;
    userTimeGerigorianTo = userTimeJalali;
        
    clearDisplay();
    
    startDateTime->date = userDateGerigorianFrom;
    endDateTime->date = userDateGerigorianTo;
    startDateTime->time = userTimeGerigorianFrom;
    endDateTime->time = userTimeGerigorianTo;
    
    return TRUE;
}

/**
 * Read pressed key in defined time duration.
 * @param   key [input]: pressed key 
 * @param   keyPressed [input]: key pressed or time out & key not pressed
 * @param   validKeys [input]: set of permissive keys
 * @param   timeout [input]: timeout
 * @see     clearKeyBuffer().
 * @see     getTicks().
 * @see     isKeyPressed().
 * @see     showLog().
 * @return  None. 
 */
void getKey(uint8* key, uint8* keyPressed, uint8* validKeys, uint32 timeout)
{
    uint8   anyKeyPressed   = 0;            /* any pressed key */
    int     counter         = 0;	    /* loop counter */
    int     validKeyCount   = strlen((const char*)validKeys);
    uint32  startTime	    = getTicks();
    uint32  nowTime         = getTicks();

    clearKeyBuffer();
    *keyPressed = FALSE;
    
#if defined (ICT250) || defined(IWL220)
	while (tickDuration(startTime) < timeout * 1000) {
#else
    while ((nowTime - startTime) < timeout * 1000)
    {
        nowTime = getTicks();
#endif
        anyKeyPressed = isKeyPressed();
        if (anyKeyPressed)
        {
            readPressedKey(&anyKeyPressed);

            for (counter = 0; counter < validKeyCount; counter++)
                if (anyKeyPressed == validKeys[counter])
                {
                    *key = anyKeyPressed;
                    *keyPressed = TRUE;
                    break;
                }
        }

        if (*keyPressed)
            break;
    }
}

/**
 * Draw get value page, use as drawer in "getInputPage" function.
 * @param   title [input]: title
 * @param   isPass [input]: the password
 * @param   withComma [input]: value With Comma
 * @param   transactionValue [input]: the value of transaction
 * @param   transactionRequestNum [input]: the number of transaction request
 * @param   value [input]: value
 * @see     isMenuCleared().
 * @see     clearDisplay().
 * @see     setMenuCleared().
 * @see     oneLineClear().
 * @see     displayString().
 * @see     displayDialogPassword().
 * @see     displayforeColor().
 * @see     displayStringFarsi().
 * @see     compareStringNumbers().
 * @see     makeValueWithCommaStr().
 * @return None.
 */
void getInputPageDrawer(uint8* title, int isPass, int withComma, uint8* transactionValue, int transactionRequestNum, uint8* value, uint8 withF4)
{
    uint8 showStr[30]           = {0};              /* string that displayed on LCD */
    uint8 displayStr[55]        = {0};
	uint8 displayStr1[55] = { 0 };
	uint8 valueWithComma[30] = { 0 };//ABS
//    int   checkFirst            = 0;

    
    //MRF
    if (isMenuCleared())
    {
        clearDisplay();
        setMenuCleared(FALSE);
    }
	else
	{
		if (withF4 == TRUE)
			oneLineClear(DISPLAY_START_LINE + 1 + EMPTY_LINE);//clean input line
		else
			oneLineClear(DISPLAY_START_LINE + 2 + EMPTY_LINE);//clean input line


	}
    
	sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, title);

    if ((isPass == 2) || (isPass == 3))//..HNO_980917
    {
        if (!check) //for one time write message
        {
            displayDialogPassword();
            displayforeColor(BLUE);
            displayStringFarsi("���� ��� ������ ���� �����:", /*LINE_1 --MRF_IDENT*/DISPLAY_START_LINE + EMPTY_LINE, SMALL_STRING, ALIGN_CENTER);
        }
        check = TRUE;
    }
    if (withF4 == TRUE)
        displayF4Picture(displayStr1);//..HNO_980906
    
    //display title on line 0 & 1
    if (strlen(title) > DISPLAY_CHARACTER_COUNT)  
    {
        displayString(displayStr, DISPLAY_START_LINE);
    }
    //display title on line 1
    else if(isPass != 3)//+HNO_980917
    {
    #ifdef IWL220//HNO_ADD
                    displayString(displayStr, DISPLAY_START_LINE + 1 /*CHANGE MRF_IDENT*/);
    #else
        if (withF4 == TRUE)
        {
            #ifdef CASTLES //..HNO_980906
                displayString(displayStr, 3 );

#else
			displayString(displayStr, DISPLAY_START_LINE + DISPLAY_START_LINE - 1 /*CHANGE MRF_IDENT*/);//yek khat balatar miravad
#endif
		}
		else
			displayString(displayStr, DISPLAY_START_LINE + DISPLAY_START_LINE  /*CHANGE MRF_IDENT*/);
#endif
	}
//	if (withF4 == TRUE)
//	{
//		sprintf(displayStr1, "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "->���� �����");
//		displayString(displayStr1, 4);
//	}

    if (compareStringNumbers(transactionValue, "0") > 0) 
    {
        /* transacttion value with comma in string form */
        makeValueWithCommaStr(transactionValue, valueWithComma);
        
        if (transactionRequestNum <= 1)
        {
              sprintf(showStr, "%s %s", valueWithComma, "����");
//            showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "showStr is : %s", showStr);
        }
        else 
        {
            sprintf(showStr, "%s %c %d %s", valueWithComma, '+', transactionRequestNum, "����");
//          showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "showStr 1 is : %s", showStr);
        }

        sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, showStr);
//        showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "displayStr 0 is : %s", displayStr);
        displayString(displayStr, DISPLAY_START_LINE + 3 + EMPTY_LINE); 

    }
    memset(showStr, 0 , 30);



    if (value[0] != 0 && strlen((const char*)value) > 0)
    {
	
        if (!isPass && !withComma)
        {
			
            sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, value);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr is : %s", displayStr);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr[0]: %d", displayStr[0]);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr[1]: %d", displayStr[1]);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr[2]: %d", displayStr[2]);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr[3]: %d", displayStr[3]);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr[4]: %d", displayStr[4]);

			if (withF4 == TRUE)
				displayString(displayStr, DISPLAY_START_LINE + 1 + EMPTY_LINE );
			else
				displayString(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE );

        }
        else if (!isPass) 
        {
            makeValueWithCommaStr(value, showStr);
            sprintf(displayStr, "%c%c%c%s%s", PRN_NORM, ALIGN_CENTER, FARSI, showStr, " ����");
#ifdef VX520 //ABS:ADD:960821
			if ((strlen(displayStr) < (DISPLAY_CHARACTER_COUNT + 4)) && (withF4 == FALSE))
				oneLineClear(4);//clean input line
                    if (withF4 == TRUE)
                    displayString(displayStr, DISPLAY_START_LINE + 1 + EMPTY_LINE);
					else
					displayString(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE);

            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "strlen(displayStr)=%d", strlen(displayStr));
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "displayStr 1 is : %s", displayStr);
#else
            displayString(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE);
#endif
        }
        else 
        {
            memset(showStr, '*', strlen((const char*)value));
            sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, showStr);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "displayStr 2 is : %s", displayStr);

			if (withF4 == TRUE)
				displayString(displayStr, DISPLAY_START_LINE + 1 + EMPTY_LINE);
			else
				displayString(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE);

        }
    }
    
}

/**
 * Get string value page.
 * @param   title [input]: page title.  
 * @param   maxLen [input]: max input length
 * @param   minLen [input]: min input length
 * @param   result [input]: entered value
 * @param   isPass [input]: is password?
 * @param   zeroBegin [input]: is begin with zero?
 * @param   withComma [input]: with comma? 
 * @param   transactionValue [input]: transaction value
 * @param   transactionRequestNum [input]: transaction request number
 * @see     getInputPageDrawer().
 * @see     getKey().
 * @return  TRUE or FALSE.
 */
uint8 getStringValue(uint8* title, int maxLen, int minLen, uint8* result, int isPass, int zeroBegin, int withComma, 
                     uint8* transactionValue, int transactionRequestNum)
{	
    uint8   pressedKey                      = 0;							/* pressed key */
    int     begin                           = 1;							/* set if this char is first input */
    int     inputLen            = strlen((const char*)result);                          /* input string lenght */
    uint8   valueStr[100]                   = { 0 };							/* input string */
    uint8   removeValue                     = TRUE;
    uint8   defValidKeys[16]                = { KBD_CANCEL, KBD_ENTER, KBD_CLEAR, KBD_00, KBD_0, KBD_1, KBD_2, KBD_3, KBD_4, KBD_5, KBD_6, KBD_7, KBD_8, KBD_9 };
    uint8   validPressedKey                 = 0;							/* valid pressed key */
    uint8   anyKeyPressed                   = FALSE;						/* set if any key is preesed */
    uint8   value[100]                      = { 0 };
    int     checkFirst                      = 0;
    int     plus                            = 0;
    uint8   displayStr[20]        = {0};//+HNO_980917
    uint8   displayStr2[15]        = {0};//+HNO_980917

    //#ifdef CASTLES_V5S // for one type write input pass message
    if ((isPass == 2) || (isPass == 3))
        check = FALSE;
    //#endif
    //strcpy((char*)value, "09");
    if (!strcmp(result,"09"))
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "topup-prefix");			
        plus = 2;
    }

		inputLen = strlen((const char*)result) + plus;



		while (TRUE)
		{
			if (!isPass)
			{
				strcpy((char*)valueStr, result);
				strcpy((char*)value, valueStr);
			}
			else
				strcpy((char*)value, result);

			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "value=%s", value);

        getInputPageDrawer(title, isPass, withComma, transactionValue, transactionRequestNum, value, FALSE);
        if (isPass == 3)//+HNO_980917 for PCPOS we use this type to display amount & pass in one page
        {

            sprintf(displayStr2, "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "���� �� ����:");
            displayString(displayStr2, 5);
            sprintf(displayStr, "%c%c%c%s", PRN_BIG, ALIGN_CENTER, FARSI, title);
            displayString(displayStr, 6);

        }
        getKey(&validPressedKey, &anyKeyPressed, defValidKeys, GET_INPUT_TIMEOUT);

			if (anyKeyPressed)
				pressedKey = validPressedKey;
			else
				pressedKey = KBD_TIMEOUT;


#ifdef VX520 //ABS:ADD
			if (Finish_dial == TRUE && OpenHDLC_Flag == FALSE && connectionStatus == DIALING && dialStatus == CONNECT_1200)
				OpenHDLC();
#endif
			if (pressedKey == KBD_ENTER && isPass && inputLen < minLen) //#MRF_970830 :MANAGE BY DEVELOPER MAX=MIN
				continue;

			if (pressedKey == KBD_ENTER && inputLen < minLen)  // MGH 130211 : added
				continue;

			if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL || pressedKey == KBD_TIMEOUT )
			{
				break;
			}

			if (pressedKey == KBD_CLEAR)
			{
				removeValue = FALSE;

				if (inputLen > (0 + plus))
					inputLen--;

				result[inputLen] = 0;

				if (inputLen == (0 + plus))
					begin = 1;

				continue;
			}

			if (inputLen >= maxLen && !removeValue)
				continue;

			if ((pressedKey == KBD_0 || pressedKey == KBD_00) && begin && !zeroBegin)
				continue;
			else
			{

				if (removeValue)
				{
					if (pressedKey == KBD_00)
					{
						if (maxLen < 2)
							continue;
						else
						{
							result[(0 + plus)] = '0';
							result[(1 + plus)] = '0';
							result[(2 + plus)] = 0;
							inputLen = 2 + plus;
						}
					}
					else
					{
						result[(0 + plus)] = pressedKey;
						result[(1 + plus)] = 0;
						inputLen = 1 + plus;
					}
				}
				else
				{
					if (pressedKey == KBD_00)
					{
						if (inputLen + 2 > maxLen)
							continue;
						else
						{
							result[inputLen++] = '0';
							result[inputLen++] = '0';
							result[inputLen] = 0;
						}
					}
					else
					{
						result[inputLen++] = pressedKey;
						result[inputLen] = 0;
					}
				}

				removeValue = FALSE;
				begin = 0;
			}
		}

		if (pressedKey == KBD_CANCEL || pressedKey == KBD_TIMEOUT)
		{
#ifdef VX520 //ABS:ADD
			if (pressedKey == KBD_CANCEL && connectionStatus == DIALING)
				closeModem(TRUE);
#endif
			result[(0 + plus)] = 0;
			//mgh: what is it!??! return TRUE;
		}

		// FORI   check = TRUE;
		//    clearDisplay();//MRF
		return (pressedKey == KBD_ENTER) ? TRUE : FALSE;
	
}

/*
 * Get date from user.
 * @param   title [input]: page title.
 * @param   nextprev [input]: exit or continue?
 * @param   enteredDate [input]: entered date by user
 * @see     DateToStr().
 * @see     getInputPageDrawer().
 * @see     getKey().
 * @see     showLog().
 * @see     strToDate().
 * @return  last pressed key.
 */
uint8 getDateFromUser(uint8* title, uint8 nextprev, uint32* enteredDate)
{
    int     dateLen         = 0;                        /* entered date lenght */
    uint8   key             = KBD_CLEAR;                /* pressed key */
    uint8   datePattern[11]	= "13--/--/--";             /* jalali date pattern */
    uint8   clearValue		= TRUE;                     /* clear current date value */
    uint8   inputBuffer[9]	= {0};                      /* input string buffer */
    uint8   validKeys[15]	= {KBD_CANCEL, KBD_ENTER, KBD_CLEAR, KBD_0, KBD_1, KBD_2, KBD_3, KBD_4, KBD_5, KBD_6, KBD_7, KBD_8, KBD_9};
    uint8   validPressedKey	= 0;                        /* valid pressed key */
    uint8   anyKeyPressed	= FALSE;                    /* set if any key is preesed */
    
    if (*enteredDate > 0)
    {
        DateToStr(*enteredDate, inputBuffer);
        datePattern[2] = inputBuffer[2];
        datePattern[3] = inputBuffer[3];
        datePattern[5] = inputBuffer[4];
        datePattern[6] = inputBuffer[5];
        datePattern[8] = inputBuffer[6];
        datePattern[9] = inputBuffer[7];
        dateLen = 6;
    }
    else
    {
        strcpy((char*)inputBuffer, "13");
        memset(&inputBuffer[2], 0, 7);
        strcpy((char*)datePattern, "13--/--/--");
        dateLen = 0;
    }

    while (TRUE) 
    {
		getInputPageDrawer(title, 0, 0, "0", 0, datePattern, FALSE);

        getKey(&validPressedKey, &anyKeyPressed, validKeys, GET_INPUT_TIMEOUT);
        
        if (anyKeyPressed)
            key = validPressedKey;
        else
            key = KBD_TIMEOUT;
        
        if (key == KBD_ENTER && nextprev) 
            continue;
        
        if (key == KBD_ENTER && dateLen < 6) 
            continue;
   
        if (key == KBD_TIMEOUT) 
        {
            if (dateLen != 6)
                *enteredDate = 0;
            else 
                *enteredDate = strToDate(inputBuffer);
            
            break;
        }
        
        if (key == KBD_CANCEL && nextprev) 
            continue;

        if (key == KBD_CANCEL) 
        {
            *enteredDate = 0;
            break;
        }
        
        if (key == KBD_ENTER || (nextprev && (key == KBD_UP || key == KBD_DOWN/*-- key == KBD_ATM_F3 || key == KBD_ATM_F4 || key == KBD_ATM_F2) --*/))) 
        {
            if (dateLen != 0 && dateLen < 6) 
                continue;

            if (dateLen == 0)
            {
                *enteredDate = 0;
            }
            else if (dateLen == 6)
            {
                *enteredDate = strToDate(inputBuffer);
            }
            
            break;
        }
        
        if (key == KBD_CLEAR)
        {
            clearValue = FALSE;
            
            if (dateLen > 0) 
            {
                datePattern[dateLen + (((dateLen % 2) == 0)?(dateLen / 2):(dateLen / 2) + 1)] = '-';
                dateLen--;
            }
            
            continue;
        }

        if (clearValue) 
        {
            clearValue = FALSE;
            strcpy((char*)datePattern, "13--/--/--");
            memset(&inputBuffer[2], 0, 7);
            dateLen = 0;
        } 
        
        if (dateLen >= 6) 
            continue;

        if (dateLen == 2 && key > KBD_1) 
            continue;
        
        if (dateLen == 4 && key > KBD_3) 
            continue;
        
        if (dateLen == 3 && inputBuffer[4] == '0' && key == KBD_0) 
            continue;
        
        if (dateLen == 5 && inputBuffer[6] == '0' && key == KBD_0) 
            continue;
        
        if (dateLen == 3 && inputBuffer[4] == '1' && key > KBD_2)
            continue;
        
        if (dateLen == 5 && inputBuffer[6] == '3' && key > KBD_1) 
            continue;
        
        if (dateLen == 5 && inputBuffer[6] == '3' && key > KBD_0 && zeroPadStrToInt(&inputBuffer[4], 2) > 6) 
            continue;
        
        if (dateLen == 4 && (zeroPadStrToInt(&inputBuffer[2], 2) % 4) != 3 && zeroPadStrToInt(&inputBuffer[4], 2) == 12 && key == KBD_3 ) 
            continue;
        
        inputBuffer[dateLen + 2] = key;

        dateLen++;
        
        datePattern[(dateLen + (((dateLen % 2) == 0)?(dateLen / 2):(dateLen / 2) + 1))] = key;
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "getDateFromUser", "patt=%s", datePattern);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "getDateFromUser", "inpbuff=%s", inputBuffer);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "getDateFromUser", "len=%d", dateLen);
    }
    
    return key;
}

/**
 * Get time from user.
 * @param   title [input]: page title.
 * @param   nextprev [input]: exit or continue?
 * @param   enteredTime [input]: entered time by user
 * @see     timeToStr().
 * @see     getInputPageDrawer().
 * @see     getKey().
 * @see     strToTime().
 * @return  last pressed key.
 */
uint8 getTimeFromUser(uint8* title, uint8 nextprev, uint32* enteredTime) 
{
    int     timeLen             = 0;                /* entered time lenght */
    uint8   timePattern[6]      = "--:--";          /* time pattern */
    uint8   inputTimeStr[7]     = {0};              /* input time in string form */
    uint8   key                 = KBD_CLEAR;        /* pressed key */
    uint8   clearValue          = TRUE;             /* clear current time value */
    uint8   defValidKeys[15]	= {KBD_CANCEL, KBD_ENTER, KBD_CLEAR, KBD_0, KBD_1, KBD_2, KBD_3, KBD_4, KBD_5, KBD_6, KBD_7, KBD_8, KBD_9};
    uint8   validPressedKey     = 0;                /* valid pressed key */
    uint8   anyKeyPressed       = FALSE;            /* set if any key is preesed */

    timeToStr(*enteredTime , inputTimeStr);
    timePattern[0] = inputTimeStr[0];
    timePattern[1] = inputTimeStr[1];
    timePattern[3] = inputTimeStr[2];
    timePattern[4] = inputTimeStr[3];
    timeLen = 4;
    
    while (TRUE) 
    {
		getInputPageDrawer(title, 0, 0, "0", 0, timePattern, FALSE);
        getKey(&validPressedKey, &anyKeyPressed, defValidKeys, GET_INPUT_TIMEOUT);

        if (anyKeyPressed)
            key = validPressedKey;
        else
            key = KBD_TIMEOUT;
       
        if (key == KBD_ENTER && nextprev) 
            continue;
        
        if (key == KBD_ENTER && timeLen < 4) 
            continue;
        
        if (key == KBD_TIMEOUT) 
        {
            if (timeLen == 4)
                *enteredTime = strToTime(inputTimeStr);
            else
                *enteredTime = 0;
            
            break;
        }
        
        if (key == KBD_CANCEL && nextprev) 
            continue;
        
        if (key == KBD_CANCEL) 
        {
            *enteredTime = 0;
            break;
        }
        
        if (key == KBD_ENTER || (nextprev && (key == KBD_UP || key == KBD_DOWN/*-- key == KBD_ATM_F3 || key == KBD_ATM_F4 || key == KBD_ATM_F2--*/))) 
        {
            if (timeLen != 0 && timeLen < 4) 
                continue;

            if (timeLen == 0)
                *enteredTime = 0;
            else if (timeLen == 4)
                *enteredTime = strToTime(inputTimeStr);
            break;
        }

        if (key == KBD_CLEAR)
        {
            clearValue = FALSE;
            
            if (timeLen > 0) 
            {
                timePattern[timeLen - ((timeLen > 2) ? 0 : 1)] = '-';
                timeLen--;
            }
            continue;
        }

        if (clearValue) 
        {
            clearValue = FALSE;
            strcpy((char*)timePattern, "--:--");
            memset(inputTimeStr, 0, 4);
            timeLen = 0;
        } 
        
        if (timeLen >= 4)
            continue;

        if (timeLen == 0 && key > KBD_2) 
            continue;
        
/* 
        if (timeLen == 1 && inputTimeStr[0]  == '2' && key > KBD_3) 
            continue;
*/
        if (timeLen == 1 && inputTimeStr[0]  == '2' && key > KBD_4) 
            continue;
        if (timeLen == 2 && inputTimeStr[0]  == '2' && inputTimeStr[1]  == '4' && key > KBD_0) 
            continue;
        if (timeLen == 3 && inputTimeStr[0]  == '2' && inputTimeStr[1]  == '4' && inputTimeStr[2]  == '0' && key > KBD_0) 
            continue;
        
        if (timeLen == 2 && key > KBD_5) 
            continue;
        
        if (timeLen == 4 && key > KBD_9) 
            continue;
        
        inputTimeStr[timeLen] = key;
        
        timeLen++;
        timePattern[timeLen - ((timeLen > 2) ? 0 : 1)] = key;
    }

    return key;
}

/**
 * Show confirm/cancel page.
 * @param   title [input]: title.
 * @param   title [input]:_len title lenght.
 * @param   timeout [input]: timeout.
 * @see     displayPic()
 * @return  last pressed key or time out.
 */
//uint8 confirmCancelPage(uint8* title, int timeout) 
//{
//    uint8 pressedKey          = 0;                        /* pressed key */
//    uint8 keyPressed          = FALSE;                    /* is any key pressed */
//    int   line                = DISPLAY_START_LINE + 1;   /* LCD line number */
//    uint8 titleMsg[200]		= {0};                      /* page title message */
//    uint8 cpyTitleMsg[200]	= {0};                      /* copy of page title message */
//    uint8 validKeys[4]		= {KBD_CANCEL, KBD_ENTER, KBD_CONFIRM, KBD_REJECT};  /* valid keys */

/*

    strcpy((char*)cpyTitleMsg, title);
    if (strlen((const char*)cpyTitleMsg) > 42)
        line = 0;
    
    strcpy((char*)titleMsg, title);
    displayAlertDelay(titleMsg, line, 0, TRUE);
        
    displayPic(confirmPic);
    displayPic(cancelPic);
    
    getKey(&pressedKey, &keyPressed, validKeys, timeout);
    

    if (!keyPressed)
        return KBD_TIMEOUT;
    
    if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL || 
    		pressedKey == KBD_CONFIRM || pressedKey == KBD_REJECT) 
        return pressedKey;
    return FALSE;
}
*/

/**
 * Select item page
 * @param   title [input]: page title.
 * @param   items [input]: items
 * @param   itemsNum [input]: the number of items
 * @param   selectedItemIndex [input]: selected the index of items
 * @param   timeout [input]: timeout
 * @see     displayforeColor().
 * @see     displayStringEnglish().
 * @see     oneLineClear().
 * @see     displayString().
 * @see     displayPic().
 * @see     displayString().
 * @see     getKey().
 * @return  TRUE or FALSE.
 */
//HNO_IDENT9
uint8 selectItemPage(uint8* title, uint8 items[][20], int language, uint8 itemsNum, uint8* selectedItemIndex, uint32 timeout)
{
	uint8               counter             = 0;            /* loop counter */
    uint8               pressedKey          = 0;            /* pressed key */
    uint8               keyPressed          = FALSE;        /* is any key pressed */
    uint8               startIndex          = 0;
    uint8               selectedIndex       = 0;
    uint8               validKeys[5]        = {KBD_CANCEL, KBD_ENTER, KBD_UP, KBD_DOWN};
    uint8               tempItems[50][20]	= {0, 0};
    uint8               changePage          = TRUE;
    uint8				invertedItem[32]	= {0};//HNO_ADD
    uint8				invertedLine		= 0;//HNO_ADD
    terminalSpecST      terminalSpec        = getTerminalCapability();

    clearDisplay();
    
    if (terminalSpec.graphicCapability)
    {
       displayforeColor(BLUE1);
       if(language == FARSI)//MRF_NEW14
    		displayStringFarsi(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
       else
            displayStringEnglish(title, DISPLAY_START_LINE , PRN_NORM, ALIGN_CENTER);
    }
    else//HNO_IDENT9
    {
    	if(language == FARSI)
    		displayInvertStringFarsi(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
    	else
    		displayInvertStringEnglish(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
    }

    if (itemsNum == 0)
        return FALSE;

    for (counter = 0; counter < itemsNum; counter++)
    {
    	if(language == FARSI)//HNO_IDENT9
    		sprintf(tempItems[counter], "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, items[counter]);
    	else
    		sprintf(tempItems[counter], "%c%c%c%s", PRN_NORM, ALIGN_CENTER, ENGLISH, items[counter]);
    }
    
    clearKeyBuffer();
    pressedKey = 0;

    while (TRUE)
    {
        keyPressed = FALSE;
        if (pressedKey == KBD_ENTER)// || pressedKey == KBD_CANCEL)
            break;
        else if (pressedKey == KBD_CANCEL)
                return FALSE;

        if (pressedKey == KBD_UP && selectedIndex > 0)
        {
            selectedIndex--;
            if (selectedIndex < startIndex)
                startIndex--;
            
            changePage = TRUE;
        }

        if (pressedKey == KBD_DOWN && selectedIndex < itemsNum - 1)
        {
            selectedIndex++;
            if (selectedIndex > startIndex + MAX_LINE_LOG - 1)
                startIndex++;
            
            changePage = TRUE;
        }

        changePage = TRUE;
        
        if (changePage)
        {
			//HNO_ADD
			#if defined(ICT250) || defined(IWL220)
				clearDisplay();
			#endif
            if (!terminalSpec.graphicCapability)
            {
            	clearMenu();
            }
                
            if (strcmp(preTitle, title) != 0)
            {
                if (terminalSpec.graphicCapability)
                {
                   displayforeColor(BLUE1);
                   if(language == FARSI)//MRF_NEW14
                       displayStringFarsi(title, DISPLAY_START_LINE , PRN_NORM, ALIGN_CENTER);
                   else
                       displayStringEnglish(title, DISPLAY_START_LINE , PRN_NORM, ALIGN_CENTER);

                   strcpy(preTitle,title);
                   displayforeColor(BLUE);
                }
                else
                {
                    if(language == FARSI)//HNO_IDENT9
                        displayInvertStringFarsi(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
                    else
                        displayInvertStringEnglish(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
   #ifndef INGENICO	//HNO_ADD
                   strcpy(preTitle,title);
   #endif
                }
            }
            for (counter = 0; counter < MAX_LINE_LOG; counter++)
            {
               if ((counter + startIndex) < itemsNum)
               {
                   if (terminalSpec.graphicCapability && itemsNum > MAX_LINE_LOG)//MRF_NEW15
                        oneLineClear(counter + DISPLAY_START_LINE + 1); 
                   
                   if (selectedIndex - startIndex == counter)
                   {
                       if (terminalSpec.graphicCapability)
                       {
                           displayforeColor(RED);
                           displayString(tempItems[counter + startIndex], counter + DISPLAY_START_LINE + 1 );
                           displayPic(borderPic);
                           displayforeColor(BOLD_BLUE);
                       }
                       else
                       {
   #if defined(IWL220) || defined(ICT250)//HNO_ADD
                           strcpy(invertedItem, items[counter + startIndex]);
                           invertedLine = counter + DISPLAY_START_LINE + 1;
   #else
                           displayInvertString(tempItems[counter + startIndex], counter + DISPLAY_START_LINE + 1);
                           displayPic(borderPic);
   #endif
                       }
                   }
                   else
                       displayString(tempItems[counter + startIndex], counter + DISPLAY_START_LINE + 1 );
               }
            }
			//HNO_ADD
#if defined(IWL220) || defined(ICT250)
            if(language == FARSI)//HNO_IDENT9
            	displayInvertStringFarsi(invertedItem, invertedLine, PRN_NORM, ALIGN_CENTER);
            else
            	displayInvertStringEnglish(invertedItem, invertedLine, PRN_NORM, ALIGN_CENTER);
			displayPic(borderPic);
#endif
            changePage = FALSE;
      }

        getKey(&pressedKey, &keyPressed, validKeys, timeout);
        if (!keyPressed)
        {
            pressedKey = KBD_CANCEL;
            return FALSE;
        }
    }
    memset(preTitle,0,sizeof(preTitle));
    *selectedItemIndex = selectedIndex;

    return TRUE;
}

/**
 * Get numeric value.
 * @param   title [input]: page title.  
 * @param   maxLen [input]: max input length
 * @param   minLen [input]: min input length
 * @param   result [input]: entered value.
 * @param   zeroBegin [input]: is begin with zero?
 * @param   withComma [input]: with comma? 
 * @see     getStringValue().
 * @return  retValue (OK or CANCEL).
 */
uint8 getNumeric(uint8* title, int maxLen, int minLen, uint8* result, int zeroBegin, 
                  int withComma) 
{
    int retValue = FALSE;

    retValue = getStringValue(title, maxLen, minLen, result, FALSE, zeroBegin, 
                               withComma, "0", 0);

    return retValue;
}

/**
 * Edit numeric value.
 * @param   inputNumber [input]: input number.
 * @param   message [input]: message, page title
 * @param   min [input]: minimume value
 * @param   max [input]: maximume value
 * @param   zeroBegin [input]: string begin with 0 or not
 * @param   withComma [input]:string with comma or not
 * @see     getNumeric().
 * @see     displayMessageBox().
 * @return  TRUE or FALSE.
 */
uint8 editNumericValue(uint32* inputNumber, uint8* message, uint32 min, 
                       uint32 max, int zeroBegin, /*int farsi,*/ int withComma)  
{
    uint8   valueStr[10]    = {0};                      /* retry minutes string */
    uint32  tempNumber      = 0;                        /* entered retry minutes */
    uint8   counter         = 0;                        /* loop counter */
    uint8   len[128]        = {0};  
    
    sprintf(valueStr, "%ld", *inputNumber);
    sprintf(len, "%ld", max);
    
    if (getNumeric(message, strlen(len), 0, valueStr, zeroBegin, withComma))
    {
        for (counter = 0; counter < strlen(valueStr); counter++) 
            tempNumber = tempNumber * 10 + (valueStr[counter] - '0');
        
        if (tempNumber >= min && tempNumber <= max && tempNumber != *inputNumber)
        {   
            *inputNumber = tempNumber;
            return TRUE; 
        }
        else if (!(tempNumber >= min && tempNumber <= max))
            displayMessageBox("����� �������", MSG_ERROR);
    }
    return FALSE;
}

/**
 * Edit Port.
 * @param   inputNumber [input]: input number.
 * @param   message [input]: message, page title
 * @param   min [input]: minimume value
 * @param   max [input]: maximume value
 * @param   zeroBegin [input]: string begin with 0 or not
 * @param   withComma [input]: string with comma or not
 * @see     getNumeric().
 * @return  TRUE or FALSE.
 */
uint8 editPort(uint32* inputNumber, uint8* message, uint32 min, 
                       uint32 max, int zeroBegin,  int withComma)
{
    uint8   valueStr[50]    = {0};
    uint8   tempString[50]  = {0};
    uint8   counter         = 0;
    uint32  tempNumber      = 0;

    if (*inputNumber > 0)
        sprintf(valueStr, "%ld", *inputNumber); 
    strcpy(tempString, valueStr);
    
    if (getNumeric(message, 6, 0, tempString, zeroBegin, withComma))
    {
        if (strcmp(tempString, valueStr) != 0)
        {   
            strcpy(valueStr, tempString);
            for (counter = 0; counter < strlen(valueStr); counter++) 
                tempNumber = tempNumber * 10 + (valueStr[counter] - '0');
            *inputNumber = tempNumber;
            return TRUE;
        }
    }
    return FALSE;                    
}

/**
 * Edit numeric string value.
 * @param   inputString [input]: input number
 * @param   maxLen [input]: minimum value
 * @param   message [input]: message, page title
 * @param   zeroBegin [input]: string begin with 0 or not
 * @param   withComma [input]: string with comma or not
 * @see     getNumeric().
 * @return  TRUE or FALSE.
 */
uint8 editNumericStringValue(uint8* inputString, uint8 maxLen, uint8 minLen, uint8* message, 
                             int zeroBegin, int withComma)  
{
    uint8  tempString[50] = {0};
    
    strcpy(tempString, inputString);
    
    if (getNumeric(message, maxLen, minLen, tempString, zeroBegin, withComma))
    {
        if (strcmp(tempString, inputString) != 0)
        {   
            strcpy(inputString, tempString);
            return TRUE;
        }
    }
    return FALSE;
}

/**
 * Edit IP number.
 * @param   ip [input]: ip number
 * @param   message [input]: message
 * @see     clearDisplay().
 * @see     displayString().
 * @see     getKey().
 * @see     clearDisplay().
 * @return  TRUE or FALSE.
 */
uint8 editIp(uint8* ip, uint8* message) 
{        
    int     ipIndex             = 3;                    /* current IP part index */
    int     ipLenInIndex        = 3;                    /* lenght of IP part */
    uint8   validKeys[13 + 1]   = {KBD_ENTER, KBD_CANCEL, KBD_CLEAR, KBD_0, KBD_1, KBD_2, KBD_3, KBD_4, KBD_5, KBD_6, KBD_7, KBD_8, KBD_9};
    uint8   pressedKey          = 0;                    /* pressed key */        
    uint8   keyPressed          = FALSE;                /* key pressed or not */
    uint8   ipStr[16]           = {0};                  /* IP string */
    uint8   tempIp[5]           = {0};                  /* entered IP */ 
    uint8   removeValue         = TRUE;             
    uint8   displayStr[100]     = {0};                  /* display string */
    int     tmpIdx              = 0;

    memcpy(tempIp, ip, 4);
    tempIp[4] = 0;
    
//  --MRF_NEW clearDisplay();
    
    //MRF_NEW
    if (isMenuCleared())
    {
        clearDisplay();
        setMenuCleared(FALSE);
    }
    else
        oneLineClear(DISPLAY_START_LINE + 2 + EMPTY_LINE);//clean input line

    sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, message);
    displayString(displayStr, DISPLAY_START_LINE + 1);

    while (!((pressedKey == KBD_ENTER && ipIndex == 3 && ipLenInIndex == 3) || pressedKey == KBD_CANCEL
             || (pressedKey == KBD_ENTER && ipIndex == 0 && ipLenInIndex == 0)))
    {
        keyPressed = FALSE;
        
// --MRF_NEW       clearDisplay();
#if defined(ICT250) || defined(IWL220)//HNO_ADD
		clearDisplay();
#endif
        sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, message);
        displayString(displayStr, DISPLAY_START_LINE + 1);
         
        //MGH
        incompleteIpToStr(tempIp, ipIndex, ipLenInIndex, ipStr);
#ifndef INGENICO//HNO_ADD because it doesn't erase ip correctly, in order to insert new ip
        oneLineClear(DISPLAY_START_LINE + 2 + EMPTY_LINE); //MRF_NEW
#endif
        sprintf(displayStr, "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, ipStr);
        displayString(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE); //MRF_NEW
            
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
            
        if (!keyPressed)
        {
            pressedKey = KBD_CANCEL;
            break;
        }
        else if (pressedKey == KBD_CLEAR)
        {
            if (removeValue) 
                removeValue = FALSE;
            
            if (ipIndex == 0 && ipLenInIndex == 0) 
                continue;
            
            ipLenInIndex--;
            tempIp[ipIndex] /= 10;
            
            if (ipIndex > 0 && ipLenInIndex == 0) 
            {
                ipLenInIndex = 3;
                ipIndex--;
            }
            
            continue;
        }
        else if (pressedKey >= KBD_0 && pressedKey <= KBD_9)
        {
            if (removeValue) 
            {
                ipIndex = 0;
                ipLenInIndex = 0;
                memcpy(tempIp, "\x00\x00\x00\x00", 4);
                tempIp[4] = 0;
                removeValue = FALSE;
            }

            if (ipIndex == 3 && ipLenInIndex == 3) 
                continue;

            tmpIdx = ipIndex;

            if (ipLenInIndex == 3) 
                tmpIdx++;

            if ((tempIp[tmpIdx] * 10 + (pressedKey - KBD_0)) > 255) 
                continue;

            tempIp[tmpIdx] = (tempIp[tmpIdx] * 10 + (pressedKey - KBD_0));
            
            ipIndex = tmpIdx;

            if (ipLenInIndex == 3) 
                ipLenInIndex = 1;
            else 
                ipLenInIndex++;
            
            continue;
        }
    }
    
    if (pressedKey == KBD_ENTER)
    {
        memcpy(ip, tempIp, 4);
        tempIp[4] = 0;
        return TRUE;
    }
    if (pressedKey == KBD_CANCEL)
        return FALSE;
}

/**
 * Edit two status item.
 * @param   itemValue [input]: itemValue
 * @param   selectionItems [input]: selection items page
 * @param   message [input]: message string
 * @see     selectItemPage().
 * @return  TRUE or FALSE.
 */
uint8 edit2StatusItem(uint8* itemValue, uint8 selectionItems[][20], uint8* message) 
{        
    uint8 selectedItemIndex = ! *itemValue;                    /* selected item index */
    uint8 tempValue         = *itemValue;                       /* selected item value */
    
    //HNO_IDENT9
    if (selectItemPage(message, selectionItems, FARSI, 2, &selectedItemIndex, DISPLAY_TIMEOUT))//ABS:FARSI
    {
        tempValue = !selectedItemIndex;
        
        if (tempValue != *itemValue)
        {
            *itemValue = tempValue;
            return TRUE; 
        }
    }    
    return FALSE;
}

/**
 * Cancel function.
 * @see     readPressedKey().
 * @return  TRUE or FALSE.
 */
int cancelFunction(void) 
{
    uint8   pressedKey      = 0;
    uint8   validKeys[2]    = {KBD_CANCEL};
    int     counter         = 0;
    
    if (isKeyPressed()) 
    {
        readPressedKey(&pressedKey);
        for (counter = 0; counter < strlen(validKeys); counter++)
            if (pressedKey == validKeys[counter])
            {
                if (pressedKey == KBD_CANCEL)
                {  
                    return TRUE;
                }
            }
    }
    return FALSE;
}

uint8 enterBuyMultiChargeInfo(uint8* PIN, buyChargeSpecST* buyCharge, int* chargeTypeCount, int* multiTypeCharge, uint8 offline)  
{
    uint8   neededNumStr[15]    = {0};          /** needed charge number in string form */
    int     neededNum           = 0;            /** temp needed number */
    int     counter             = 0;            /** loop counter */
    uint8   key                 = KBD_CANCEL;
    
    memset(neededNumStr, 0, 15);
    
    while (strlen(neededNumStr) == 0)
        if (!getNumeric("����� �� ���� ������:", 2, 0, neededNumStr, FALSE, FALSE))
            return FALSE;

    for (counter = 0; counter < strlen(neededNumStr); counter++)
        neededNum = neededNum * 10 + (neededNumStr[counter] - '0');
    if (!offline)
        buyCharge->productTypeCount = neededNum;
    else
        buyCharge->productTypeCount += neededNum; 
   
    key = displayMessageBox("��� ��ю ���� �� ���� �� ���Ͽ", MSG_CONFIRMATION);
#ifdef VX520 //ABS:ADD
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Finish_dial:%d", Finish_dial);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "dialStatus:%d", dialStatus);
	if (Finish_dial == TRUE && OpenHDLC_Flag == FALSE && connectionStatus == DIALING && dialStatus == CONNECT_1200)
		OpenHDLC();
#endif
    if (key != KBD_ENTER && key != KBD_CONFIRM)
    {
        memset(PIN, 0, 5);

        if (!getPass("", PIN, 4, 2))
            return FALSE;
        
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PIN: %d", PIN);

        *multiTypeCharge = FALSE;
    }
    else
    {
        (*chargeTypeCount)++; 
        *multiTypeCharge = TRUE;
    }

    return TRUE;
}

//HNO_ADD
uint8 enterLoanPayInfo(cardSpecST* cardSpec, loanPayST* loanPay) 
{
	uint8 destinationCardPAN[19 + 1] = { 0 };
	uint8 valueStr[15 + 1] = { 0 };

	clearDisplay();
	//HNO_ADD
#ifdef INGENICO
	if (!getPass("��� ���� ������ ���� �����:", cardSpec->PIN, 4))
		return FALSE;
#else
    if (!getPass("", cardSpec->PIN, 4, 2))
        return FALSE;
#endif

	clearDisplay();
	if (!getStringValue("���� ����:", 12, 1, valueStr, 0, 0, 1, "0", 0))//MRF_NEW16
		return FALSE;

	clearDisplay();
	if (!getNumeric("����� ���� ���:", 16, 16, destinationCardPAN, 0, 0))
		return FALSE;

	if (/* --MRF(strncmp(destinationCardPAN, "628023", 6) != 0) || */
            (strncmp(destinationCardPAN, "62802370", 8) != 0) &&
                (strncmp(destinationCardPAN, "62802380", 8) != 0))//MRF_NEW3
    {
		displayMessageBox("���� ���� ������� ���.", MSG_ERROR);
		return FALSE;
	}

	if (strcmp(destinationCardPAN, cardSpec->PAN) == 0) 
    {
		displayMessageBox("����� ���� ���� � ���� �� ���!", MSG_ERROR);
		return FALSE;
	}

	strcpy(loanPay->payAmount, valueStr);
	strcpy(loanPay->destinationCardPAN, destinationCardPAN);

	clearDisplay();

	return TRUE;
}

//HNO_LOAN
uint8 enterLoanPayTrackingInfo(uint8* stan, uint8* PIN) {
	clearDisplay();

	if (!getNumeric("����� �����:", 6, 1, stan, FALSE, FALSE))
		return FALSE;

	clearDisplay();
	return TRUE;
}

//MRF_ETC
uint8 enterETCInfo(cardSpecST* cardSpec, ETCST* ETC) 
{
	uint8 serialETC[13 + 1]         = {0};
	uint8 amount[12 + 1]            = {0};
//	uint8 cardHolderID[10 + 1]      = {0};
	int intAmount                   = 0;

	clearDisplay();
	//HNO_ADD
#ifdef INGENICO
	if (!getPass("��� ���� ������ ���� �����:", cardSpec->PIN, 4))
		return FALSE;
#else
    if (!getPass("", cardSpec->PIN, 4, 2))
        return FALSE;
#endif

	clearDisplay();
	if (!getNumeric("����� �э��:", 13, 13, serialETC, 0, 0))
		return FALSE;

	clearDisplay();
	if (!getStringValue("���� ����:", 12, 4, amount, 0, 0, 1, "0", 0))
		return FALSE;

	intAmount = atoi(amount);
    
	while (intAmount < 50000 || intAmount > 5000000 || (intAmount % 10000) != 0 )//MRF_NEW14
    {
        if (intAmount < 50000)
    		displayMessageBox("���� ���� ��� ���� �� �� ���� ���!", MSG_WARNING);
        
        if (intAmount > 5000000)
    		displayMessageBox("���� ���� ��� ����� �� �� ���� ���!", MSG_WARNING);
        
        if ((intAmount % 10000) != 0) 
             displayMessageBox("���� ���� ����� �� 10000 ���� ����!", MSG_WARNING);//ABS:CHANGE
        
		memset(amount, 0, sizeof(amount));
		if (!getStringValue("���� ����:", 12, 4, amount, 0, 0, 1, "0", 0))
			return FALSE;
        
        intAmount = atoi(amount);
	}

	strcpy(ETC->serialETC, serialETC);
	strcpy(ETC->amount, amount);

	clearDisplay();

	return TRUE;
}

uint8 enterETCTrackingInfo(uint8* stan , uint8* PIN) 
{
//    clearDisplay();
//    if (!getPass("", PIN, 4, 2))
//        return FALSE;
//    
   clearDisplay();
    if (!getNumeric("����� �����:", 6, 1, stan, FALSE, FALSE))
        return FALSE;
   
   clearDisplay();
   return TRUE;
}

//MRF_TOPUP_NEW
/**
 * Enter buy top up Charge information.
 * @param   mobileNoToReturn [input]: the mobile number to return
 * @param   amount [input]: amount of buy
 * @param   PIN [input]: PIN number
 * @see     getPass().
 * @see     showLog().
 * @see     getStringValue().
 * @see     displayMessageBox().
 * @see     compareStringNumbers().
 * @see     displayMessageBox().
 * @return  TRUE or FALSE.
 */
uint8 enterBuyTopUpChargeInfo(topupST* topup, uint8* PIN, uint8 variable) //ABS:CHANGE:960801
{
    uint8   valueStr[15]                    = {0};      /* buy amount in string form */
    uint8   mobileNo[12]                    = {0};      /* phone number to be charged.*/
    uint8   selectedItemIndex               = 0;
	uint8   selectionItemsType[][20]		= { "20000 �����", "50000 �����", "100000 �����" };
    uint8   operatorCheckStr[5]             = {0};
    int     operatorCheck                   = 0;
    uint8   operatorType                    = 0;
    uint8   operatorName[15]                = { 0 };

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " operatorType: %d ", operatorType);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " variable: %d ", variable);
    
    memset(PIN, 0, 5);
 
    //if (!getPass("", PIN, 4, 2))
    //    return FALSE ;
        
    clearDisplay();
    while (TRUE)
    {
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " geting phone ");
        memset(mobileNo, 0, sizeof(mobileNo));
        strcpy((char*)mobileNo, "09");

        if (!getStringValue("����� ���� ����� �� ���� ������:", 11, 1, mobileNo, FALSE, TRUE, FALSE, "0", 0))
            return FALSE;

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "strlen(mobileNo)=%d", strlen(mobileNo));
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "strlen(mobileNo)=%s", (mobileNo));
		

        if (strlen(mobileNo) != 11)
        {
             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
			 showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "len error ");
             continue;
        }
        if (strncmp(mobileNo, "09", 2) != 0)
        {
             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
			 showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "digit  error ");
             continue;
        }

//        if (((mobileNo[2] - '0') < 5) || ((mobileNo[2] - '0') > 9))
//        {
//             displayMessageBox("����� ����� ���� ����!", MSG_ERROR);
//             showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " digit error ");
//             continue;
//        }
        strcpy(topup->mobileNo, mobileNo);
        break;
    }
    
    strncpy(operatorCheckStr, &mobileNo[1], 3);
    operatorCheck = atoi(operatorCheckStr);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " mobileNo:%s ", mobileNo);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " operatorCheck:%d ", operatorCheck);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " operatorCheckStr:%s ", operatorCheckStr);
    
    clearDisplay();
    

	switch (operatorCheck)
        {
            case MCI_1:
            case MCI_2:
            case MCI_3:
			case MCI_4:
			case MCI_5:
			case MCI_6:
			case MCI_7:
			case MCI_8:
			case MCI_9:
			case MCI_10:
			case MCI_11:
			case MCI_12:
			case MCI_13:
				operatorType = MULTI_MCI;
				strcpy(operatorName, "����� ���");
				topup->operatorType = 2;
                break;
			case MTN_1:
			case MTN_2:
			case MTN_3:
			case MTN_4:
			case MTN_5:
			case MTN_6:
			case MTN_7:
			case MTN_8:
			case MTN_9:
			case MTN_10:
			case MTN_11:
			case MTN_12:
				operatorType = MULTI_IRANCELL;
				strcpy(operatorName, "�������");
				topup->operatorType = 1;
			    break;
			case RTL_1:
			case RTL_2:	
			case RTL_3:
				operatorType = MULTI_RIGHTEL;
				strcpy(operatorName, "�����");
				topup->operatorType = 3;
				break;
            default:
                displayMessageBox("ǁ����� ������!", MSG_ERROR);
                return FALSE;
                
        }



    if (variable)
    {
       // operatorType = MULTI_IRANCELL;//HNO_CHANGE!!!! PLZ CHECK
        while (TRUE)
        {
            memset(valueStr, 0, 15);

			
			if (!getStringValue_WithF4("���� ��ю:", 7, 1, valueStr, 0, 0, 1, "0", 0))
                return FALSE;

        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " valueStr: %s ", valueStr);

        if (!strcmp(valueStr, "KBD_F4"))//..HNO_980906
        {
            strcpy(valueStr, "");

				if (selectItemPage(operatorName, selectionItemsType, FARSI, 3, &selectedItemIndex, DISPLAY_TIMEOUT))
				{
					//if (!strcmp(&selectedItemIndex, "20000 �����"))
					if (!strcmp((const char*)selectionItemsType[(int)selectedItemIndex], "20000 �����"))
					{
						strcpy(valueStr, "20000");
						//       topup->type = 0;
						showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " valueStr:%s ", valueStr);
					}					
					else if (!strcmp((const char*)selectionItemsType[(int)selectedItemIndex], "50000 �����"))
					{
						strcpy(valueStr, "50000");
						//      topup->type = 1;
						showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " valueStr:%s ", valueStr);
					}
					
					else if (!strcmp((const char*)selectionItemsType[(int)selectedItemIndex], "100000 �����"))
					{
						strcpy(valueStr, "100000");
						//	topup->type = 1;
						showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " valueStr:%s ", valueStr);
					}

					displayforeColor(BLUE);
					clearDisplay();
				}
				else
				{
					displayforeColor(BLUE);
					clearDisplay();
					return FALSE;
				}

			}

            //strcpy(amount, valueStr); 

			if (operatorType == MULTI_MCI)		//++ABS_980728		baraye hamrah aval saghf darad
			{

				if ((compareStringNumbers(valueStr, "10000") < 0) || (compareStringNumbers(valueStr, "2000000") > 0))
					displayMessageBox("���� ���� ��� ���� ����!", MSG_ERROR);
				else
					break;
			}
			else								//baraye baghiye saghf nadarad
			{
				if ((compareStringNumbers(valueStr, "10000") < 0))
					displayMessageBox("���� ���� ��� ���� ����!", MSG_ERROR);
				else
					break;
			}

        }
    }
    
	strcpy(topup->amount, valueStr);
	topup->type = NORMAL;//++ABS

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " mobileNO: %s ", topup->mobileNo);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " charge Type: %d ", topup->type);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " Operator: %d ", topup->operatorType);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " amount: %s ", topup->amount);
    

	if (!getPass("", PIN, 4, 2))
		    return FALSE ;

    return TRUE ;
}



//HNO_ADD_ROLL
uint8 enterRollRequestTrackingInfo(uint8* stan) 
{
	clearDisplay();

	if (!getNumeric("����� �����:", 6, 1, stan, FALSE, FALSE))
		return FALSE;

	clearDisplay();
	return TRUE;
}
	
//MRF_NEW13
/**
 * Edit status item.
 * @param   itemValue [input]: itemValue
 * @param   selectionItems [input]: selection items page
 * @param   message [input]: message string
 * @see     selectItemPage().
 * @return  TRUE or FALSE.
 */
uint8 editStatusItem(uint8* itemValue, uint8 selectionItems[][20], uint8* message, uint8 count) 
{        
    uint8 selectedItemIndex = ! *itemValue;                    /* selected item index */
    uint8 tempValue         = *itemValue;                       /* selected item value */
    
    //HNO_IDENT9
    if (selectItemPage(message, selectionItems, ENGLISH, count, &selectedItemIndex, DISPLAY_TIMEOUT))
    {
        tempValue = !selectedItemIndex;
        
        if (tempValue != *itemValue)
        {
            *itemValue = tempValue;
            return TRUE; 
        }
    }    
    return FALSE;
}

uint8 enterCharityInfo(uint8* amount, uint8* PIN)
{
    uint8 		valueStr[12 + 1]	= {0};			/* buy amount in string form */
    uint8       message[60]         = {0};
    uint8       retValue            = 0;


	if (!getStringValue("���� ����:", 12, 1, valueStr, FALSE, FALSE, TRUE, "0", 0))
		return FALSE;

	strcpy(amount, valueStr);

    makeValueWithCommaStr(amount, message);
    strcat(message," ����");
    
    if (getPOSService().confirmationAmount)//MRF_NEW19
    {
        retValue = displayMessageBox(message, MSG_CONFIRMATION);//MRF_NEW18
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)
        {
            memset(PIN, 0, 5);
            if (!getPass("", PIN, 4, 2))
                return FALSE ;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        memset(PIN, 0, 5);
        if (!getPass("", PIN, 4, 2))
            return FALSE ;
    }
    
    return TRUE ;
}

//MRF_NEW17
uint8 enterTip(uint8* tip) 
{
    uint8           valueStr[12 + 1]	= {0};			/* buy amount in string form */
    
    clearDisplay();

    if (!getStringValue("���� �����:", 12, 0, valueStr, FALSE, FALSE, TRUE, "0", 0)) //MRF_NEW20
        return FALSE;

    strcpy(tip, valueStr);

    return TRUE;
}


/**
* Get string value page.
* @param   title [input]: page title.
* @param   maxLen [input]: max input length
* @param   minLen [input]: min input length
* @param   result [input]: entered value
* @param   isPass [input]: is password?
* @param   zeroBegin [input]: is begin with zero?
* @param   withComma [input]: with comma?
* @param   transactionValue [input]: transaction value
* @param   transactionRequestNum [input]: transaction request number
* @see     getInputPageDrawer().
* @see     getKey().
* @return  TRUE or FALSE.
*/
uint8 getStringValue_WithF4(uint8* title, int maxLen, int minLen, uint8* result, int isPass, int zeroBegin, int withComma,
	uint8* transactionValue, int transactionRequestNum)
{
	uint8   pressedKey = 0;							/* pressed key */
	int     begin = 1;							/* set if this char is first input */
	int     inputLen = strlen((const char*)result);                          /* input string lenght */
	uint8   valueStr[100] = { 0 };							/* input string */
	uint8   removeValue = TRUE;
	uint8   defValidKeys[16] = { KBD_CANCEL, KBD_ENTER, KBD_CLEAR, KBD_00, KBD_0, KBD_1, KBD_2, KBD_3, KBD_4, KBD_5, KBD_6, KBD_7, KBD_8, KBD_9, KBD_F4 };
	uint8   validPressedKey = 0;							/* valid pressed key */
	uint8   anyKeyPressed = FALSE;						/* set if any key is preesed */
	uint8   value[100] = { 0 };
	int     checkFirst = 0;

	//#ifdef CASTLES_V5S // for one type write input pass message
	if (isPass == 2)
		check = FALSE;
	//#endif
	while (TRUE)
	{
		if (!isPass)
		{
			strcpy((char*)valueStr, result);
			strcpy((char*)value, valueStr);
		}
		else
			strcpy((char*)value, result);

		getInputPageDrawer(title, isPass, withComma, transactionValue, transactionRequestNum, value, TRUE);
		getKey(&validPressedKey, &anyKeyPressed, defValidKeys, GET_INPUT_TIMEOUT);

		if (anyKeyPressed)
			pressedKey = validPressedKey;
		else
			pressedKey = KBD_TIMEOUT;



#ifdef VX520 //ABS:ADD
		if (Finish_dial == TRUE && OpenHDLC_Flag == FALSE && connectionStatus == DIALING && dialStatus == CONNECT_1200)
			OpenHDLC();
#endif
		if (pressedKey == KBD_ENTER && isPass && inputLen < minLen) //#MRF_970830 :MANAGE BY DEVELOPER MAX=MIN
			continue;

		if (pressedKey == KBD_ENTER && inputLen < minLen)  // MGH 130211 : added
			continue;

		if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL || pressedKey == KBD_TIMEOUT || pressedKey == KBD_F4)
		{
			break;
		}

		if (pressedKey == KBD_CLEAR)
		{
			removeValue = FALSE;

			if (inputLen > 0)
				inputLen--;

			result[inputLen] = 0;

			if (inputLen == 0)
				begin = 1;

			continue;
		}

		if (inputLen >= maxLen && !removeValue)
			continue;

		if ((pressedKey == KBD_0 || pressedKey == KBD_00) && begin && !zeroBegin)
			continue;
		else
		{

			if (removeValue)
			{
				if (pressedKey == KBD_00)
				{
					if (maxLen < 2)
						continue;
					else
					{
						result[0] = '0';
						result[1] = '0';
						result[2] = 0;
						inputLen = 2;
					}
				}
				else
				{
					result[0] = pressedKey;
					result[1] = 0;
					inputLen = 1;
				}
			}
			else
			{
				if (pressedKey == KBD_00)
				{
					if (inputLen + 2 > maxLen)
						continue;
					else
					{
						result[inputLen++] = '0';
						result[inputLen++] = '0';
						result[inputLen] = 0;
					}
				}
				else
				{
					result[inputLen++] = pressedKey;
					result[inputLen] = 0;
				}
			}

			removeValue = FALSE;
			begin = 0;
		}
	}

	if (pressedKey == KBD_CANCEL || pressedKey == KBD_TIMEOUT)
	{
#ifdef VX520 //ABS:ADD
		if (pressedKey == KBD_CANCEL && connectionStatus == DIALING)
			closeModem(TRUE);
#endif
		result[0] = 0;
		//mgh: what is it!??! return TRUE;
	}

    if (pressedKey == KBD_F4)
    {
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pressedKey=%d", pressedKey);
            strcpy(result, "KBD_F4");//..HNO_980906
//            result[0] = '1';
//            result[1] = '2';
//            result[2] = '5';
//            result[3] = 0;
            return TRUE;
    }
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "8888");

    // FORI   check = TRUE;
    //    clearDisplay();//MRF
                clearDisplay();//+HNO_980913
    return (pressedKey == KBD_ENTER) ? TRUE : FALSE;
}



