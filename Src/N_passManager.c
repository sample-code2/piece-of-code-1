#include <string.h>
#include "N_common.h"
#include "N_log.h" 
#include "N_passManager.h"
#include "N_dateTime.h" 
#include "N_cardSpec.h" 
#include "N_terminalReport.h" 
#include "N_fileManageWrapper.h"
#include "N_fileManage.h" 
#include "N_error.h"
#include "N_binaryTree.h" 
#include "N_menu.h" 
#include "N_scheduling.h" 
#include "N_connection.h" 
#include "N_merchantSpec.h" 
#include "N_messaging.h" 
#include "N_getUserInput.h" 
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h" 
#include "N_display.h" 
#include "N_initialize.h"
#include "N_printer.h"
#include "N_printerWrapper.h"
#include "N_displayWrapper.h" 
#include "N_userManagement.h"

#define     GLOBAL_MENU_PASSWORDS_FILE              "glmpw0100"
#define     MERCHANT_MENU_PASSWORDS_FILE            "mmpw0100"
#define     USER_MANAGER_MENU_PASSWORDS_FILE        "umpw0100"
#define     SUPERVISOR_MENU_PASSWORDS_FILE          "smpw0100"
#define     SUPERVISOR_PASS_FLAG_FILE               "passflag"


/**
 * Get and check password.
 * @param   correctPass [input]: correct password
 * @see     getPass().
 * @see     displayMessageBox().
 * @return  TRUE or FALSE.
 */
uint8 checkPassword(uint8* correctPass)
{
    uint8  enteredPass[MAX_PASS_LEN + 1] = {0};
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "correctPass = %s", correctPass);
    if (!getPass("��� ����:", enteredPass, strlen(correctPass), 1))
        return FALSE;
    
    if (enteredPass[0] == NULL)
    {
        return NO_PASS;
    }

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "enteredPass = %s", enteredPass);
    if (strcmp(correctPass, enteredPass) != 0)
    {
        displayMessageBox("��� ������!", MSG_ERROR);
        return INCORRECT_PASS;
    }
    
    return CORRECT_PASS;
}


/**
 * Write password in file.
 * @param   fileName [input]: File name
 * @param   password [input]: Password
 * @see     updateFileInfo().
 * @see     addSystemErrorReport().
 * @return  TRUE or FALSE.
 */
uint8 writePassword(uint8* fileName, uint8* password) 
{
    uint16 retValue = FAILURE;   /* file update return value */
  
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "strlen(password) = %d", strlen(password));
    
    retValue = updateFileInfo(fileName, password, strlen(password));  
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    return TRUE;
} 


/**
 * Read password from file and if can not read and last argument is set write 
 * default password in file and return it.
 * @param   fileName [input]: File name
 * @param   defaultPass [input]: Default password
 * @param   password [input]: Password
 * @param   passLen [input]: Password lenght
 * @param   setDefault [input]: If true  and can not read password write default password in file
 * @see     fileExist().
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     writePassword().
 * @return  TRUE or FALSE.
 */
uint8 readPassword(uint8* fileName, uint8* defaultPass, uint8* password, uint32 passLen , uint8 setDefault) 
{
    int16 retValue = FAILURE;          /* file update return value */
    
    retValue = fileExist(fileName);
    if (retValue == SUCCESS)
    {
        memset(password, 0, sizeof(password));
        retValue = readFileInfo(fileName, password, &passLen);  
        if (retValue != SUCCESS) 
        {
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        }
    }
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "read Pass = %s", password);
    if (retValue != SUCCESS && setDefault) 
    {
        retValue = writePassword(fileName, defaultPass);
        strcpy(password, defaultPass);
    }
    
    if (retValue == SUCCESS)
        return TRUE;
    else
        return FALSE;
}


/**
 * Get older password and get new password and Change password.
 * @param   fileName [input]: File Name
 * @param   message [input]: Message to show on LCD
 * @param   passLen [input]: password lenght
 * @see     readPassword().
 * @see     displayMessageBox().
 * @see     getPass().
 * @see     writePassword().
 * @return  None.
 */
uint8 changePassword(uint8* fileName, uint8* message, uint32 passLen)  //HNO_DEBUG change return value
{
    uint8 			enteredPass[4 + 1]		= {0};
    uint8 			currentPass[4 + 1]		= {0};
    uint8 			newPass[4 + 1]        	= {0};
    uint8 			renewPass[4 + 1]      	= {0};
	uint8 			loginUserName[15 + 1] 	= {0};  //ABS:ADD
	userNodeST*		currentNode 			= NULL; //ABS:ADD
	

	if (getPOSService().shiftService == TRUE)//ABS:ADD:SHIFT
	{
		getLoginUser(loginUserName);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "searchUser");
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "loginUserName=%s", loginUserName);


		currentNode = getUserList();
		//getUserList(currentNode);

		while (currentNode)
		{
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", currentNode->data.userName);
			if (!strcmp(currentNode->data.userName, loginUserName))
				break;

			currentNode = currentNode->next;
		}

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", currentNode->data.userName);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", currentNode->data.pass);
	}
	else
	{
		if (!readPassword(fileName, "", currentPass, passLen, FALSE))
		{
			displayMessageBox("���� �����!", MSG_ERROR);
			return FALSE;//HNO_DEBUG
		}
	}

    if (!getPass(message, enteredPass, passLen, 1))
        return FALSE;//HNO_DEBUG

	if (getPOSService().shiftService == TRUE)//ABS:ADD:SHIFT
	{
		if (strcmp(currentNode->data.pass, enteredPass) != 0)
		{
			displayMessageBox("��� ������!", MSG_ERROR);
			return FALSE;//HNO_DEBUG
		}
	}
	else
	{
		if (strcmp(currentPass, enteredPass) != 0)
		{
			displayMessageBox("��� ������!", MSG_ERROR);
			return FALSE;//HNO_DEBUG
		}
	}

    if (!getPass("��� ����:", newPass, passLen, 1))
        return FALSE;//HNO_DEBUG
    
    if (!getPass("ʘ��� ��� ����:", renewPass, passLen, 1))
        return FALSE;//HNO_DEBUG
          
    if (strcmp(newPass, renewPass) != 0)
    {
        displayMessageBox("����� ����� ������!", MSG_ERROR);
        return FALSE;//HNO_DEBUG
    }
    
	if (getPOSService().shiftService == TRUE)//ABS:ADD
	{
		if(strcmp(newPass, "1111") == 0)//HNO_DEBUG
		{
			displayMessageBox("���  ����� �������!", MSG_ERROR);
			return FALSE;
		}
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "newPass=%s", newPass);
		strcpy(currentNode->data.pass, newPass);
		setPassUserList(currentNode->data.userName, newPass);

		if (updateUsersListFile())
		{
			displayMessageBox("���� ������� �� �������� ���!", MSG_ERROR);
			return FALSE;//HNO_DEBUG
		}
		else
			displayMessageBox("��� ����� ����.", MSG_INFO);
		return TRUE;
	}
	else
	{
		if (!writePassword(fileName, newPass))
		{
			displayMessageBox("��� �� ����� ����!", MSG_ERROR);
			return FALSE;//HNO_DEBUG
		}
		else
			displayMessageBox("��� ����� ����.", MSG_INFO);
		return TRUE;
	}
}


/**
 * Change merchant menu password.
 * @see     changePassword().
 * @return  TRUE.
 */
uint8 changeSupervisorMenuPassword(void)
{    
    changePassword(SUPERVISOR_MENU_PASSWORDS_FILE, "�������:", 4);
    
    return TRUE;
}


/**
 * Change merchant menu password.
 * @see     changePassword().
 * @return  TRUE.
 */
uint8 changeMerchantMenuPassword(void)
{
	if(changePassword(MERCHANT_MENU_PASSWORDS_FILE, "�������:", 4))
		return TRUE;
	else
		return FALSE;
}


/**
 * Change user manager menu password.
 * @see     changePassword().
 * @return  TRUE.
 */
uint8 changeUserManagerMenuPassword(void)
{    
    changePassword(USER_MANAGER_MENU_PASSWORDS_FILE, "�������:", 4);
    return TRUE;
}



/**
* Set merchant password with defult value and write in file.
* @see     writePassword().
* @see     displayMessageBox().
* @return  TRUE or FALSE.
*/
uint8 resetMerchantMenuPassword(uint8 showMsg)//MRF_NEW15  //mgh_modify_970528
{
	if (getPOSService().shiftService == TRUE)//HNO_DEBUG
	{
		resetUserMenuPassword();
		return TRUE;
	}

	if (!writePassword(MERCHANT_MENU_PASSWORDS_FILE, MERCHANT_PASS))
	{
		displayMessageBox("��� �� ����� ����!", MSG_ERROR);
		return FALSE;
	}
	else if (showMsg) //MRF_NEW15
	{
		displayMessageBox("��� ���� ��!", MSG_INFO);
		return TRUE;
	}
}

/**
 * Set merchant password with defult value and write in file.
 * @see     writePassword().
 * @see     displayMessageBox().
 * @return  TRUE or FALSE.
 */
 uint8 resetMerchantMenuPasswordMenu(void)//MRF_NEW15 //mgh_add_970528
{    

	 resetMerchantMenuPassword(TRUE);

 //   uint8 check = (uint8) (args->argumentName); //MRF_NEW17 //ABS_NEW1

	//if (getPOSService().shiftService == TRUE)//HNO_DEBUG
	//{
	//	resetUserMenuPassword();
	//	return TRUE;
	//}

 //   if (!writePassword(MERCHANT_MENU_PASSWORDS_FILE, MERCHANT_PASS))
 //   {
 //       displayMessageBox("��� �� ����� ����!", MSG_ERROR);
 //       return FALSE;
 //   }
 //   else if (!check) //MRF_NEW15
 //   {
 //       displayMessageBox("��� ���� ��!", MSG_INFO);
 //       return TRUE;
 //   }
}

/**
 * When Supervisor menu locked this function reset supervisor menu password.
 * @see    checkPassword().
 * @see    updateFileInfo().
 * @see    displayMessageBox().
 * @return FALSE. 
 */
uint8 ActiveSupervisorMenu(void)
{
//    uint8   enteredPass[MAX_PASS_LEN] = {0};
    uint8   passRetry 				  = 0;
    uint8   correctPass[MAX_PASS_LEN] = {0};
    int16   retValue                  = FAILURE;//MRF_NEW19
    
    clearDisplay();
    
    //HNO_IDENT
#ifdef INGENICO
    DisplayFooter(0);
    DisplayFooter(1);
#endif

    strcpy(correctPass,RESET_SUPERVISOR_PASS);

//        if (!getPass( "��� ����:", enteredPass,strlen(correctPass), 1))
//        return FALSE;
    if (strlen(correctPass) != 0)
        if (checkPassword(correctPass) != CORRECT_PASS)
        {
            return FALSE;
        }
        else
        {
            passRetry = 0;
            retValue = updateFileInfo(SUPERVISOR_PASS_FLAG_FILE, &passRetry, 1);
            if (retValue != SUCCESS)
            {
                displayMessageBox("���� ���� �������!", MSG_ERROR);
            }
        }
    
    displayMessageBox("���� �������� ���� ��.",MSG_INFO);
}

/**
 * This function get serial number of device and generate password for supervisor menu.
 * @see     getTerminalSerialNumber().
 * @see     writePassword().
 * @return  TRUE or FALSE.
 */
uint8 PasswordGenerator(void)//MRF_NEW19
{ 
	terminalSpecST terminalCapability = getTerminalCapability();
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**PasswordGenerator");//ABS:CHANGE place

	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
	{
	dateTimeST	dateTime		= {0, 0};
	uint8       Serial[20]      = {0};
	uint8       passGen[6]      = {0};//..HNO_980716 change 5 to 6
	uint8       date[9]         = {0};
	uint8       sumSD[13]       = {0};
	uint8       str1[20]        = {0};
	uint8       first[6]        = {0};
	uint8       multistr[7]     = {0};
	int64       sumSDNum        = 0; //#MRF_980828	//--HNO_970925
	uint32      pass1           = 0;
	int64       division        = 0; //#MRF_980828	//--HNO_970925
	uint32      multiNum        = 0;
	uint32      pass            = 0;
	int         multistrLen     = 0;
	int         day             = 0;
	int         i               = 0;

	//get the terminal serial number
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
		
	//get the terminal date
	dateTime = getDateTime();
	DateToStr(dateTime.date, date);   
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "date: %s", date);

	//generate supervisor password
	sumStringNumbers(Serial, date, sumSD);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sum date & serial = %s", sumSD);

	//+HNO_970925
	calculatePass(sumSD, str1);//..ABS_971022
	//--HNO_970925
//	sumSDNum = atoll(sumSD); //#MRF_980828
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sumSDNum = %lld", sumSDNum);
//
//	division = sumSDNum / 2;
//	sprintf(str1,"%lld",division); //#MRF_980828
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "str1 = %s", str1);

	passGen[0] = str1[0];
	passGen[1] = str1[3];
	passGen[2] = str1[2];
	passGen[3] = str1[6];
	passGen[4] = str1[5];
        
        passGen[5] = '\0';//+HNO_980703
	pass1 = atoi(passGen);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pass1 = %d", pass1);

	//calculate & add day for generate OTP
	day = getDay(dateTime.date);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "gregorian day = %d", day);

	multiNum = pass1 * day;
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "multiNum = %d", multiNum);

	sprintf(multistr,"%d",multiNum);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "multistr = %s", multistr);

	multistrLen = strlen(multistr);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "multistrLen = %d", multistrLen);

	for(i = 0; i < 5; i++)
	{
		first[i] = multistr[multistrLen - (i + 1)];
		pass = atoi(first);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pass = %d", pass);
	}   

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "strlen(first) = %d", strlen(first));
        if (!writePassword(SUPERVISOR_MENU_PASSWORDS_FILE, first))
            return FALSE;
        else
            return TRUE;
    }
	else //ABS:CHANGE
    {
        uint8       Serial[16]      = {0};
        uint8       passGen[4]      = {0};

        getTerminalSerialNumber(Serial);

        passGen[0] = (( Serial[strlen(Serial) - 1] + 1 ) > '9' ? '0' : Serial[strlen(Serial) - 1] + 1);
        passGen[1] = (( Serial[strlen(Serial) - 3] + 1 ) > '9' ? '0' : Serial[strlen(Serial) - 3] + 1);
        passGen[2] = (( Serial[strlen(Serial) - 5] + 1 ) > '9' ? '0' : Serial[strlen(Serial) - 5] + 1);
        passGen[3] = (( Serial[strlen(Serial) - 7] + 1 ) > '9' ? '0' : Serial[strlen(Serial) - 7] + 1);

        if (!writePassword(SUPERVISOR_MENU_PASSWORDS_FILE, passGen))
            return FALSE;
        else
            return TRUE;
    }
}

/**
 * Check Merchant password is true or false.
 * @see readPassword().
 * @see checkPassword().
 * @return TRUE Or FALSE.
 */
uint8 checkMerchantMenuPassword(void)
{
	uint8       passWord[MAX_PASS_LEN + 1] = { 0 };
	readPassword(MERCHANT_MENU_PASSWORDS_FILE, MERCHANT_PASS, passWord, MAX_PASS_LEN, TRUE);

	if (getPOSService().shiftService == TRUE)//ABS:ADD
	{
		if (userLogin(passWord) != TRUE)
			return FALSE;

		//HNO_DEBUG
		if(strcmp(passWord, "1111") == 0)
		{
			displayMessageBox("���� ��� ��� �� ����� ����", MSG_ERROR);
			if(changeMerchantMenuPassword())
				return TRUE;
			else
				return FALSE;
		}
	}
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "getPOSService().shiftService: %d", getPOSService().shiftService);


	if (strlen(passWord) != 0)
	{	
		if (checkPassword(passWord) != CORRECT_PASS)
			return FALSE;
	}
    return TRUE;
}

/**
 * Check Supervisor password if number of wrong passwords more than 3time supervisor menu will be lock.
 * @see readPassword().
 * @see readFileInfo().
 * @see updateFileInfo().
 * @see displayMessageBox().
 * @see checkPassword().
 * @return True Or False.
 */
uint8 checkSupervisorMenuPassword(void)
{    
    uint8           passWord[MAX_PASS_LEN + 2]	= {0};
    uint8           passRetry                   = 1;
    uint32          len                         = 1;
    int16           retValue                    = FAILURE;
    uint8           msg[80]                     = {0};
    uint8           passState                   = INCORRECT_PASS;
    uint8           passLen                     = 5;
    uint8           temp[6]                     = {0};
    terminalSpecST  terminalCapability          = getTerminalCapability();
   
    PasswordGenerator();//+HNO_980721
  //#HNO_980721  
    if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        readPassword(SUPERVISOR_MENU_PASSWORDS_FILE, SUPERVISOR_PASS, temp, MAX_PASS_LEN + 1, TRUE);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "temp: %s", temp);
        temp[6] = 0; //MRF_OTP
        strcpy(passWord, temp);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passWord: %s", passWord);
    }
    else
    {
        readPassword(SUPERVISOR_MENU_PASSWORDS_FILE, SUPERVISOR_PASS, passWord, MAX_PASS_LEN, TRUE);
    }
    
    retValue = readFileInfo(SUPERVISOR_PASS_FLAG_FILE, &passRetry, &len);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry1: %d", passRetry);
    if (retValue != SUCCESS)
    {
        passRetry = 0;
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "cant read SUPERVISOR_PASS_FLAG_FILE");
        retValue = updateFileInfo(SUPERVISOR_PASS_FLAG_FILE, &passRetry, 1);
        if (retValue != SUCCESS)
        {
            displayMessageBox("���� ���� �������!", MSG_ERROR);
        }
    }
    
    if (passLen != 0 )
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry2: %d", passRetry);
        if (passRetry >= PASS_RETRY)
        {
            displayMessageBox("���� �������� ������� ��� ���.", MSG_ERROR);
            return FALSE;
        }
        else
        {
            passState = checkPassword(passWord);
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry3: %d", passRetry);
            if (passState == INCORRECT_PASS)
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "INCORRECT_PASS");
                passRetry++;
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry4: %d", passRetry);
                
                if (passRetry >= PASS_RETRY)
                {
                    displayMessageBox("���� ��� ������ ��� �� 3 ���!", MSG_ERROR);
                }
                else
                {
                    sprintf(msg, "����%d ������ ���� �� ���Ϙ��� ��� �� �����.", PASS_RETRY - passRetry);
                    displayMessageBox(msg, MSG_ERROR);
                }
                
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry5: %d", passRetry);
                retValue = updateFileInfo(SUPERVISOR_PASS_FLAG_FILE, &passRetry, len);
                if (retValue != SUCCESS)
                {
                    displayMessageBox("���� ���� �������!", MSG_ERROR);
                }
                
                return FALSE;
            }
            else if (passState == NO_PASS)
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "No Pass: %d", passRetry);
                return FALSE;
            }
        }
    }
    
    passRetry = 0;
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "passRetry6: %d", passRetry);
    retValue = updateFileInfo(SUPERVISOR_PASS_FLAG_FILE, &passRetry, 1);
    if (retValue != SUCCESS)
    {
        displayMessageBox("���� ���� �������!", MSG_ERROR);
    }
    return TRUE;
}

/**
 * Check user manager password.
 * @see readPassword().
 * @see checkPassword().
 * @return TRUE or FALSE.
 */
uint8 checkUserManagerMenuPassword(void)
{    
    uint8       passWord[MAX_PASS_LEN + 1]	= {0};  
    
    readPassword(USER_MANAGER_MENU_PASSWORDS_FILE, USER_MANAGER_PASS, passWord, MAX_PASS_LEN, TRUE);

    if (strlen(passWord) != 0)
        if ( checkPassword(passWord) != CORRECT_PASS )
            return FALSE;

    return TRUE;
}

/**
 * Get charachter as Password and show that *.
 * @param   message [input]: Message is title page
 * @param   enteredPass [input]: Entered value
 * @param   maxlen [input]: Max input lenght
 * @param   passType [input]: Type of Password
 * @return  getStringValue().
 */
uint8 getPass(uint8* message, uint8* enteredPass, int maxlen, int passType) 
{
#if defined(ICT250) || defined(IWL220)//HNO_ADD when we enter pass the title should'nt be remove
	setMenuCleared(TRUE);
#endif
    return getStringValue(message, maxlen, maxlen, enteredPass, passType, TRUE, FALSE, "0", 0);
}

/**
 * Check entered Password.
 * @see    readPassword().
 * @see    checkPassword(). 
 * @return TRUE or FALSE.
 */
uint8 checkLogMenuPassword(void)
{    
    uint8   passWord[MAX_PASS_LEN + 1]	= {0};
    int 	DisplayHeaderStatus = changeDisplayHeader(0);//HNO_ADD

    readPassword(LOG_TYPE_PASSWORDS_FILE, LOG_PASS, passWord, MAX_PASS_LEN, TRUE);

    changeDisplayHeader(DisplayHeaderStatus);//HNO_ADD

    if (strlen(passWord) != 0)
        if (checkPassword(passWord) != CORRECT_PASS)
            return FALSE;

    return TRUE;
}

//HNO_ADD
uint8 checkDownMenuPassword(void)
{
	clearDisplay();

	if (checkPassword(DOWN_PASS) != TRUE)
		return FALSE;

    return TRUE;
}


uint8 resetUserMenuPassword(void)//ABS:ADD:SHIFT
{
	userNodeST* currentNode = 0;
//	uint8   	selectionItems[20][20] = { 0, 0 };
//	uint8		index = 0;
//	uint8   	selectedItemIndex = 0;
//	int16		retValue = -1;
//	uint8		userName[15 + 1] = { 0 };
	uint8		loginUserName[15 + 1] = { 0 }; //ABS:ADD
	uint8		selectUser[16] = { 0 };


	getLoginUser(loginUserName);//ABS:ADD:SHIFT

	if (!displayUsersList(selectUser, FALSE))//ABS
		return FALSE;

		currentNode = getUserList();
		//getUserList(currentNode);
		while (currentNode && strcmp(currentNode->data.userName, selectUser) != 0)
			currentNode = currentNode->next;

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "USER");
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%s", currentNode->data.userName);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "%d", currentNode->data.userFileIndex);

//	if(strcmp("����", currentNode->data.userName) == 0)//HNO_SHIFT10
	setPassUserList(currentNode->data.userName, "1111");//HNO_DEBUG
//	else
//		setPassUserList(currentNode->data.userName, "0000");


	if (updateUsersListFile())
		displayMessageBox("��� �� ����� ����!", MSG_ERROR);
	else
		displayMessageBox("��� ���� ��!", MSG_INFO);

	return TRUE;
}


//MRF_NEW20
uint8 printSupervisorPass(void)
{
    uint8           password[6]                 = {0};
//    uint32          passLen                     = 0;
//    int16           retValue                    = FAILURE;
    uint8           temp[6]                     = {0};
    terminalSpecST  terminalCapability          = getTerminalCapability();
    
    clearDisplay();

    if (checkPassword(PRINT_PASS) != TRUE)
            return FALSE;
    PasswordGenerator();//+HNO_980721
    //if ((strcmp(terminalCapability.deviceModel, VEGA3000) != 0) && (strncmp(terminalCapability.deviceModel, "VX520",5) != 0))//ABS:CHANGE
    if (terminalCapability.OTPCapability == TRUE)//..ABS_971101
    {
        readPassword(SUPERVISOR_MENU_PASSWORDS_FILE, SUPERVISOR_PASS, temp, MAX_PASS_LEN + 1, TRUE);
        temp[6] = 0; //MRF_OTP
        strcpy(password, temp);
    }
    else
    {
        readPassword(SUPERVISOR_MENU_PASSWORDS_FILE, SUPERVISOR_PASS, password, MAX_PASS_LEN, TRUE);
    }
    
//    printOneStringFarsi("��� �������", PRN_NORM, ALIGN_CENTER);
//    printStar();
    
    displayMessageBox(password, MSG_INFO);
    
//    if (!printOneStringFarsi(password, PRN_BIG, ALIGN_CENTER))
//        return FALSE;
    
//    printBlankLines(BLANK_LINES_COUNT);
    
    return TRUE;
}




