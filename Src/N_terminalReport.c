#include <string.h>   
//MRF #include <stdio.h>    
#include "N_common.h"
#include "N_utility.h"
#include "N_error.h"
#include "N_dateTime.h"
#include "N_printerWrapper.h" 
#include "N_printer.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_messaging.h"
#include "N_transactions.h"
#include "N_fileManage.h" 
#include "N_transactionReport.h" 
#include "N_getUserInput.h" 
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_displayWrapper.h" 
#include "N_display.h" 
#include "N_initialize.h"
#include "N_log.h" 
#include "N_connectionWrapper.h"
#include "N_userManagement.h"

#define     SYSTEM_ERROR_LOG_FILE                   "syserr0100"

//HNO_IDENT comment & move it to N_displayWrapper
//#define     SYSTEM_ERROR_REPORT_MAX                 200 //MRF_NEW3
//#define     MAX_LINE_SYSTEM_ERROR_DISPLAY        	2000 //MRF_NEW3


 /** system error log */
typedef struct 
{
    dateTimeST dateTime;          /* system error date and time */
    int        type;              /* system error type */
    int32      code;              /* system error code */
}sysErrorST;

/**
 * Show and print Hardware Report in scrollable page.
 * @see     selectSmallDisplayFont().
 * @see     getMemoryStatus().
 * @see     getDateTime().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     getHour().
 * @see     getMinute().
 * @see     getTerminalSerialNumber().
 * @see     removePadLeft().
 * @see     isBitSet().
 * @see     displayScrollableWithHeader().
 * @see     displayScrollable().
 * @see     PrinterAccess().
 * @see     displayMessage().
 * @see     printScrolableLines().
 * @return  TRUE or FALSE.
 */
uint8 hardwareReport(void)   
{
    uint32                  memSize                 = 0; 
    uint8                   lines[50][32]           = {0, 0};
    int                     year                    = 0;
    int                     month                   = 0;
    int                     day                     = 0;
    int                     hour                    = 0;
    int                     minute                  = 0;
    uint8                   datetimeStr[17]         = {0};
    //uint8                   Serial[16]              = {0};
    uint8                   versionNum[17 + 1]	    = {0};
    uint16                  i                       = 0;
    uint8                   title[17]               = {0};
    uint8                   key                     = KBD_CANCEL;
    uint16                  retValue                = FAILURE;
    uint32                  len                     = sizeof(kcvST);
    terminalSpecST			terminalCapability		= getTerminalCapability();
    dateTimeST              dateTime;
    memoryStatusST          memoryStatus;
    kcvST                   kcvSpec;
    
     
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "hardware report");

    memset(&kcvSpec, 0, len);
    memset(&dateTime, 0, sizeof(dateTimeST));
    memset(&memoryStatus, 0, sizeof(memoryStatusST));
    
    selectSmallDisplayFont();
    getMemoryStatus(&memoryStatus);
    strcpy(versionNum, VERSION);    
   
    dateTime = getDateTime();    
    year = getYear(dateTime.date);
    month = getMonth(dateTime.date);
    day = getDay(dateTime.date);
    hour = getHour(dateTime.time);
    minute = getMinute(dateTime.time);
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "MAC KCV: %02X%02X%02X",kcvSpec.macKCV[0], kcvSpec.macKCV[1], kcvSpec.macKCV[2]);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Voucher KCV: %02X%02X%02X",kcvSpec.voucherKCV[0], kcvSpec.voucherKCV[1], kcvSpec.voucherKCV[2]);
    
    retValue = readFileInfo(KCV_FILE, &kcvSpec, &len);
    if (retValue != SUCCESS)
        //To DO: Show True Message
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "READ KCV FILE NOT SUCCESS");
    else
        //To DO: Show True Message
         showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "READ KCV FILE SUCCESS");
       
    /*********************************LINES************************************/
    memset(lines, 0, sizeof(lines));
    
    sprintf(lines[i++], "%c%c%c%s", NORMAL_INVERT_STRING, ALIGN_CENTER, ENGLISH, "SOFTWARE INFO");
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, &versionNum[4]);//#MRF_971015
    
    sprintf(datetimeStr, "%04d/%02d/%02d %02d:%02d", year, month, day, hour, minute);
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, datetimeStr);
    
    sprintf(lines[i++], "%c%c%c%s %s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "TMS Version:", TMS_VERSION);//+MRF_980210
    
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "                ");

    /*******************************MEMORY STATUS******************************/
    
    sprintf(lines[i++], "%c%c%c%s", NORMAL_INVERT_STRING, ALIGN_CENTER, ENGLISH, "MEMORY STATUS");
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Flash(KBytes)");
    
    memSize = memoryStatus.totalFlash;
    sprintf(lines[i++], "%c%c%c%s %ld", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Total:", memSize);
    
    memSize = memoryStatus.usedFlash;
    sprintf(lines[i++], "%c%c%c%s %ld", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Used:", memSize);

    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "RAM(KBytes)");

    memSize = memoryStatus.totalRam;
    sprintf(lines[i++], "%c%c%c%s %ld", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Total:", memSize);

    memSize = memoryStatus.usedRam;
    sprintf(lines[i++], "%c%c%c%s %ld", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Used:", memSize);
    
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "                "); 
    
    /*******************************PCI INFO***********************************/
    
    sprintf(lines[i++], "%c%c%c%s", NORMAL_INVERT_STRING, ALIGN_CENTER, ENGLISH, "PCI INFO");
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Expire Date:");
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, PCI_EXPIRE_DATE);
    sprintf(lines[i++], "%c%c%c%s%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Version#:", PCI_VERSION);
    
    /*******************************KCV****************************************/
#if !defined(INGENICO) && !defined(VX520)//ABS:CHANGE:961207
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "                ");
    sprintf(lines[i++], "%c%c%c%s", NORMAL_INVERT_STRING, ALIGN_CENTER, ENGLISH, "KCV");
    sprintf(lines[i++],"%c%c%c%s%02X%02X%02X", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "MASTER:",kcvSpec.masterKCV[0],kcvSpec.masterKCV[1],kcvSpec.masterKCV[2]);
    sprintf(lines[i++],"%c%c%c%s%02X%02X%02X", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "MAC:",kcvSpec.macKCV[0],kcvSpec.macKCV[1],kcvSpec.macKCV[2]);
    sprintf(lines[i++],"%c%c%c%s%02X%02X%02X", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "PIN:",kcvSpec.pinKCV[0],kcvSpec.pinKCV[1],kcvSpec.pinKCV[2]);
    sprintf(lines[i++],"%c%c%c%s%02X%02X%02X", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "VOUCHER:",kcvSpec.voucherKCV[0],kcvSpec.voucherKCV[1],kcvSpec.voucherKCV[2]);
    sprintf(lines[i++], "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "                "); 
#endif
    
    //Getting Hardware Info
    getHardWareSetting(lines, &i);
       
    /* LINE 0 : TITLE */
    sprintf(title, "%c%c%c%s", NORMAL_STRING, ALIGN_CENTER, ENGLISH, "Hardware Report\n");
    i++;
    
    if (terminalCapability.graphicCapability != TRUE)
        key = displayScrollable(title, lines, i, DISPLAY_TIMEOUT, FALSE);
    else    
        key = displayScrollableWithHeader(title, lines, i, DISPLAY_TIMEOUT, FALSE ,headerHardwareReportPic);

    if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;

    if (!PrinterAccess(TRUE))  
        return FALSE;

    displayMessage("�ǁ ����� ...", DISPLAY_START_LINE + 1, ALL_LINES);
    
    printScrolableLines(title, lines, i);
    
//    printBlankLines(BLANK_LINES_COUNT);//#HNO_980605   
/** PRINT LOGO */
    if(! printHeaderLogoAndMerchantInfo())//+HNO_980613
        return FALSE;
    
    return TRUE;
}


/**
 * Add system error report to file.
 * @param   errorCode [input]: error code
 * @param   errorType [input]: error type
 * @see     getDateTime().
 * @see     getSystemErrorMessage().
 * @see     appendFixedFileInfo().
 * @return  None.
 */
void addSystemErrorReport(int32 errorCode, int errorType) 
{
    int16      retValue         = FAILURE;              /* write in file function return value */
    uint8      errorMsg[60]     = {0};                  /* error message */
    sysErrorST error;                                   /* transaction error detail */
    
    memset(&error, 0, sizeof(sysErrorST));

    error.dateTime = getDateTime();
    error.code = errorCode;
    error.type = errorType;
    
    getSystemErrorMessage(errorMsg, errorCode, errorType);

    if (getLogType() == TEST)
        displayMessageBox(errorMsg, MSG_ERROR);
    
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ERR--> errorType: %d    , errorCode: %x", errorType, errorCode);
    retValue = appendFixedFileInfo(SYSTEM_ERROR_LOG_FILE, &error, sizeof(sysErrorST), SYSTEM_ERROR_REPORT_MAX);
}

/**
 * Show and print System Error list. 
 * @param   args [input]: Args is structure and variable
 * @see     readTransactionListFile().
 * @see     fillTransListHeaderLines().
 * @see     gregorianToJalali().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     getHour().
 * @see     getMinute().
 * @see     justifyTwoStringFarsi().
 * @see     getSystemErrorMessage().
 * @see     justifyOneStringFarsi().
 * @see     displayScrollable().
 * @see     printTransactionListHeader().
 * @see     printTwoStringNumericFarsi().
 * @see     cancelFunction().
 * @see     printOneStringFarsi().
 * @see     cancelFunction().
 * @see     printReceiptDateTime().
 * @see     printDash().
 * @see     printBlankLines ().
 * @return  TRUE or False. 
 */
uint8 systemErrorList(argumentListST* args)
{
    filesST*       files                       					= (filesST*) (args->argumentName);
    uint8          fileName[FILE_NAME_LENTGH]  					= {0};  //MRF_NEW3
    dateTimeST     startDateTime               					= {0, 0};
    dateTimeST     endDateTime                 					= {0, 0};
    terminalSpecST terminalCapability		   					= getTerminalCapability();
    sysErrorST     errors[SYSTEM_ERROR_REPORT_MAX];   //MRF_NEW3 
    int            errorNumber                 					= 0;                        	/* errors number with specified date in file */
    int            errorYear                   					= 0;	
    int            errorMonth                  					= 0; 	
    int            errorDay                    					= 0;	
    int            errorHour                   					= 0;	
    int            errorMinute                 					= 0; 	
    int            counter                     					= 0;                        	//mgh int -> uint16		/** loop counter */
    uint32         jalaliDate                  					= 0;                        	/* jalali date */
    uint32         requestedNum                					= 0;                        	/* user requested errors number, integer form */  
    uint8          errorsNum[10]               					= {0};       //ABS:CHANGE              	/* errors number in dates, exist or listed */
    uint16         i                           					= 0;                        	//mgh int -> uint16 
    uint8          printStr[100]               					= {0};                      	/* print styring */
    uint8          dateValueStr[11]            					= {0};	
    uint8          timeValueStr[6]             					= {0};	
    uint8          errorCode[11]               					= {0};	
    uint8          errorMsg[50]                					= {0};	
    uint8          key                         					= KBD_CANCEL;	
    uint8          activeUser[15 + 1]          					= {0};	
    uint8          activeUserTitle[15 + 1]     					= {0};	
    uint8          title[15 + 1]               					= {0};	
    uint8          spaceCountStr[10]           					= {0};	
    uint8          lines[MAX_LINE_SYSTEM_ERROR_DISPLAY + 4][32] = {0, 0};   //MRF_NEW3      	/* display lines */	


    memset(errors, 0, sizeof(sysErrorST));
    memset(lines, 0, sizeof(lines));

    strcpy(fileName, SYSTEM_ERROR_LOG_FILE);        

    if (!readTransactionListFile(fileName, errors, sizeof(sysErrorST), SYSTEM_ERROR_REPORT_MAX, &startDateTime,
                				&endDateTime, &errorNumber, &requestedNum, activeUserTitle, activeUser))
    	return FALSE;
    
    strcpy(title, "������ ������");
    
    fillTransListHeaderLines(lines, &i, title, activeUserTitle, activeUser, &startDateTime, &endDateTime);
    
    /* ERRORS LOG */
    for (counter = requestedNum - 1; counter >= 0 && i < (MAX_LINE_SYSTEM_ERROR_DISPLAY - 2 - 3); counter--) //MRF_NEW3
    {
        /* ERRORS LOG ; LINE 1 & 2: ERROR DATE AND TIME */
        gregorianToJalali(&jalaliDate , errors[counter].dateTime.date);
        errorYear = getYear(jalaliDate);
        errorMonth = getMonth(jalaliDate);
        errorDay = getDay(jalaliDate);
        errorHour = getHour(errors[counter].dateTime.time);
        errorMinute = getMinute(errors[counter].dateTime.time);
        
        sprintf(errorCode, "%ld", errors[counter].code); 
        justifyTwoStringFarsi(errorCode, "���", lines[i++], PRN_NORM);
        
        getSystemErrorMessage(errorMsg, errors[counter].code, errors[counter].type);
//        justifyTwoStringFarsi(errorCode, errorMsg, lines[i++], PRN_NORM); //MRF_NEW3
        
        memset(spaceCountStr,' ', SPACE_COUNT);
        //HNO_ADD in ICT we can't add space at the end of date value!!!
#if defined(ICT250) || defined(IWL220)
		sprintf(timeValueStr, "%02d:%02d",errorHour,errorMinute);
        sprintf(dateValueStr, "%04d/%02d/%02d", errorYear, errorMonth, errorDay);
        sprintf(lines[i++], "%c%c%c%s%s%s", PRN_NORM, ALIGN_CENTER, FARSI, dateValueStr,spaceCountStr,timeValueStr);
#else
        sprintf(dateValueStr, "%04d/%02d/%02d%s", errorYear, errorMonth, errorDay,spaceCountStr);
        sprintf(timeValueStr, "%02d:%02d",errorHour,errorMinute);
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_CENTER, FARSI, dateValueStr,timeValueStr);
#endif
        justifyOneStringFarsi(DISPLAY_DASH, lines[i++], PRN_NORM, ALIGN_CENTER);
    }

    /* LINE (ERRORS LOG + 4) : ERRORS NUMBER IN DATES */
    sprintf(errorsNum, "%d", errorNumber);
    justifyTwoStringFarsi(errorsNum, "�����", lines[i++], PRN_NORM);

    /* LINE (ERRORS LOG + 5) : REQUESTED ERRORS */
    sprintf(errorsNum, "%ld", requestedNum);
    justifyTwoStringFarsi(errorsNum, "���� ���", lines[i++], PRN_NORM);
    
    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
	else
		key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, FALSE, systemError); 

    if (key != KBD_ENTER && key != KBD_CONFIRM) 
        return FALSE;

    if (!printTransactionListHeader(files, title, activeUserTitle, activeUser, &startDateTime, &endDateTime))
        	return FALSE;
    
    /** PRINT ERRORS DETAILS */
    for (counter = requestedNum - 1; counter >= 0; counter--)
    {
        /* PRINT ERROR CODE */
        memset(errorCode, 0, sizeof(errorCode));
        sprintf(errorCode, "%ld", errors[counter].code);
        if (!printTwoStringNumericFarsi(errorCode, "���", PRN_NORM))
            return FALSE;
        
        if (cancelFunction())
            return FALSE;
    
        /* PRINT ERROR MESSAGE */
        memset(printStr, 0, sizeof(printStr));
        getSystemErrorMessage(printStr, errors[counter].code, errors[counter].type);
        #ifdef VX520
		if (!printOneStringFarsi(printStr, PRN_NORM, ALIGN_RIGHT))//ABS:CHANGE
        	return FALSE;
        #else
		 if (!printOneStringFarsi(printStr, PRN_NORM, ALIGN_LEFT))
        	return FALSE;
		#endif
        if (cancelFunction())
            return FALSE;
    
        if (!printReceiptDateTime(&errors[counter].dateTime, TRUE))
            return;

        if (cancelFunction())
            return FALSE;

        if (!printDash())
            return FALSE;
    }
   
    /* PRINT ERRORS NUMBER IN DATES */
    sprintf(errorsNum, "%d", errorNumber);
    if (!printTwoStringNumericFarsi(errorsNum, "�����", PRN_NORM))
        return FALSE;

    if (cancelFunction())
        return FALSE;
    
    /* PRINT REQUESTED ERRORS NUMBER */
    sprintf(errorsNum, "%ld", requestedNum);
    if (!printTwoStringNumericFarsi(errorsNum, "���� ���", PRN_NORM))
        return FALSE;
    
//     printBlankLines(BLANK_LINES_COUNT);
    if(! printHeaderLogoAndMerchantInfo())//+HNO_980613
        return FALSE;
    /* PRINT EMPTY LINES */
//    printBlankLines(BLANK_LINES_COUNT);
    return TRUE;
}


void settingReport(filesST* files)
{
    merchantSpecST      merchantSpec;
    generalConnInfoST   generalConnInfo;
    HDLCConnInfoST      HDLCConnInfo;
    LANConnInfoST       LANConnInfo;
    uint8               versionNum[17 + 1]	= {0};
    uint8               connCount               = {0};
    uint8               connType                = 0;
    uint8               connNum                 = 0;
    uint8               buff[10]                = {0};
    uint8               key                     = KBD_TIMEOUT;
    uint32 		len 			= sizeof(HDLCConnInfoST);
    uint8               type[10]                = {0};
    uint8  		itemVal[12] 		= {0};
    uint8  		preNum[6]               = {0};
    uint8  		ipAddress[17]           = {0};
    int                 i                       = 0;
    uint8               shift[22]               = {0};//MRF_SHIFT
    uint8               activeUserName[16]      = {0};//ABS
    int8                ipStr[50]               = {0};	//ABS:ADD:960813
    uint32              lanConnLen 		= sizeof(LANConnInfoST);//HNO_DEBUG2
    
    memset(&HDLCConnInfo, 0, sizeof(HDLCConnInfoST));
    memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
    
    key = displayMessageBox("�ǁ �������",MSG_CONFIRMATION);
#ifndef VX520  //ABS:ADD
 initPrinter();
#endif
 if (key == KBD_ENTER || key == KBD_CONFIRM)
 {
	 readMerchantSpec(files, &merchantSpec);

	 //print header
#ifndef CASTLES_V5S //MRF_TTF
#ifdef VX520
	 printOneStringFarsi("     ����� �������      ", PRN_NORM_INVERSE, ALIGN_CENTER);
#else
	 //HNO_CHANGE in ict should be PRN_BIG_INVERSE
	 printOneStringFarsi("����� �������",PRN_BIG_INVERSE,ALIGN_CENTER);
#endif
#else
	 printOneStringFarsi("����� �������", PRN_NORM, ALIGN_CENTER);
	 printStar();
#endif

	 if (!printTwoStringNumericFarsi(merchantSpec.terminalID, "������", PRN_NORM))//ABS
		 return;

	 /** PRINT MERCHANT ID */
	 if (!printTwoStringNumericFarsi(merchantSpec.merchantID, "�������", PRN_NORM))//ABS
		 return;

	 /** PRINT VERSION */
	 strcpy(versionNum, VERSION);
#ifdef CASTLES_V5S //MRF_TTF
	 printTwoString( "����:", versionNum, PRN_NORM, TRUE);
#elif defined VX520		//ABS:ADD:960813
	 printTwoString(versionNum, "����", PRN_NORM, TRUE);
#else
	 printTwoStringNumericFarsi(versionNum, "����", PRN_NORM);
#endif
	 //Active User
	 if (getPOSService().shiftService) //MRF_SHIFT
	 {
		 getActiveUser(activeUserName);		//ABS:CHANGE
		 if (!strcmp(activeUserName, ""))	//ABS:CHANGE
         {
             
#ifdef VX520		//ABS:ADD
			 printTwoStringNumericFarsi("---", "���� ����", PRN_NORM);
#else
			 printTwoString("���� ����:", "---", PRN_NORM, TRUE);
#endif	
         }
         else
         {
            strcpy(shift, "���� ");//MRF_SHIFT
			strcat(shift, activeUserName);	//ABS:CHANGE
			printTwoStringFarsi(shift, "���� ����:", PRN_NORM);
         }
     }
        //count of connection
        generalConnInfo = getGeneralConnInfo(files->id);
        connCount = generalConnInfo.connectionCount;
        sprintf(buff,"%d",connCount);
        printTwoStringNumericFarsi(buff,"����� ������",PRN_NORM);
        printDash();
        
        //type of connection
        for(i = 0; i < connCount; i++)
        {
            memset(itemVal, 0 , sizeof(itemVal));
            connType = generalConnInfo.connectionsType[i];
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "connType: %d", connType);

#if defined(ICT250) || defined(IWL220)
            switch (connType)
            {
                case CONNECTION_HDLC:
                    strcpy(type, "����");
                    break;
                case CONNECTION_LAN:
                    strcpy(type, "�����");
                    break;
                case CONNECTION_GPRS: //MRF_GPRS
                    strcpy(type, "�� ���");
                    break;
                default:
                    strcpy(type, "������");
                    break;
            }
#else
            switch (connType) 
            {
                case CONNECTION_HDLC:
                    strcpy(type, "HDLC");
                    break;
                case CONNECTION_LAN:
                    strcpy(type, "LAN");
                    break;
                case CONNECTION_GPRS: //MRF_GPRS
                    strcpy(type, "GPRS");
                    break;
                case CONNECTION_PPP: //MRF_GPRS
                    strcpy(type, "PPP");
                    break;
                default:
                    strcpy(type, "Unknown");
                    break;
            }
#endif
            switch(i)
            {
#ifdef CASTLES_V5S    //MRF_TTF
                case 0:
                    printTwoString( "������ ���:", type, PRN_NORM, TRUE);
                    break;
                case 1:
                    printTwoString("������ ���:", type,PRN_NORM, TRUE);
                    break;
                case 2:
                    printTwoString("������ ���:", type,PRN_NORM, TRUE);
                    break;
                case 3:
                    printTwoString("������ �����:", type,PRN_NORM, TRUE);
                    break;
                case 4:
                    printTwoString("������ ����:", type,PRN_NORM, TRUE);
                    break;
                case 5:
                    printTwoString("������ ���:", type,PRN_NORM, TRUE);
                    break;
                default:
                    break;
#elif defined VX520		//ABS:ADD:960813

			case 0:
				printTwoString(type, "������ ���", PRN_NORM, TRUE);
				break;
			case 1:
				printTwoString(type, "������ ���", PRN_NORM, TRUE);
				break;
			case 2:
				printTwoString(type, "������ ���", PRN_NORM, TRUE);
				break;
			case 3:
				printTwoString(type, "������ �����", PRN_NORM, TRUE);
				break;
			case 4:
				printTwoString(type, "������ ����", PRN_NORM, TRUE);
				break;
			case 5:
				printTwoString(type, "������ ���", PRN_NORM, TRUE);
				break;
			default:
				break;
#else
                    case 0:
                    printTwoStringNumericFarsi(type, "������ ���", PRN_NORM);
                    break;
                case 1:
                    printTwoStringNumericFarsi(type, "������ ���", PRN_NORM);
                    break;
                case 2:
                    printTwoStringNumericFarsi(type, "������ ���", PRN_NORM);
                    break;
                case 3:
                    printTwoStringNumericFarsi(type, "������ �����", PRN_NORM);
                    break;
                case 4:
                    printTwoStringNumericFarsi(type, "������ ����", PRN_NORM);
                    break;
                case 5:
                    printTwoStringNumericFarsi(type, "������ ���", PRN_NORM);
                    break;
                default:
                    break;
#endif
            }
            
            //MRF_REPORT
            if (connType == CONNECTION_HDLC)
            {
                readFileInfo(files->connFiles[i], &HDLCConnInfo, &len);
                //PREFIX
                if (strlen((const char*)HDLCConnInfo.prefix) > 0) 
                {
                    strcpy(preNum, HDLCConnInfo.prefix);    
                    printTwoStringNumericFarsi(preNum,"��� �����",PRN_NORM);//MRF_NEW2
                }
                else 
                {
                    strcpy(preNum, "---");
#ifdef VX520		//ABS:ADD:960813
					printTwoStringNumericFarsi(preNum, "��� �����", PRN_NORM);
#else
                    printTwoString("��� �����:", preNum, PRN_NORM, TRUE);//MRF_NEW2
#endif	
                }
                
//#ifdef CASTLES_V5S //MRF_TTF --MRF_NEW2
//                printTwoString("��� �����:", preNum, PRN_NORM, TRUE);
//#else
//                printTwoStringNumericFarsi(preNum,"��� �����",PRN_NORM);
//#endif
                //PHONE NUMBER  
                if (strlen((const char*)HDLCConnInfo.phone) > 0) 
                {
                    strcpy(itemVal, HDLCConnInfo.phone);  
                    printTwoStringNumericFarsi(itemVal,"����� ����",PRN_NORM);//MRF_NEW2
                }
                else
                {
                    strcpy(itemVal, "---");
#ifdef VX520		//ABS:ADD:960813
					printTwoStringNumericFarsi(itemVal, "����� ����", PRN_NORM);
#else
					printTwoString("����� ����:", itemVal, PRN_NORM, TRUE);//MRF_NEW2
#endif				
                }

//              printTwoStringNumericFarsi(itemVal,"����� ����",PRN_NORM); --MRF_NEW2
                printDash();
            }
            else if (connType == CONNECTION_LAN) //MRF_NEW : CHANGE IT
            {
#if defined(ICT250) || defined(IWL220)//HNO
                readFileInfo(files->connFiles[i], &LANConnInfo, &lanConnLen);
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "localIpAddress: %s", LANConnInfo.localIpAddress);
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "LANConnInfo.dhcp: %d", LANConnInfo.dhcp);
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ipAddress: %s", LANConnInfo.ipAddress);
            	getLocalIpAddress(LANConnInfo.localIpAddress);
                ipToString(LANConnInfo.localIpAddress, ipAddress);

                printTwoStringNumericFarsi(ipAddress,"����� �� ��", PRN_NORM);
#else
                getLANConfig(files->connFiles[i], &LANConnInfo);
                
                if (strlen((const char*)LANConnInfo.localIpAddress) > 0) 
                {
                    LANConnInfo.localIpAddress[15] = 0;//+HNO_980713    ..HNO_980808

#ifdef VX520		//ABS:ADD:960813
					sprintf(ipStr, "%d.%d.%d.%d", LANConnInfo.localIpAddress[0], LANConnInfo.localIpAddress[1], LANConnInfo.localIpAddress[2], LANConnInfo.localIpAddress[3]);
					sprintf(ipAddress, "%s", ipStr);
#else
					strcpy(ipAddress, LANConnInfo.localIpAddress);
#endif	   
				}
                else 
                    strcpy(ipAddress, "---");
                
#ifdef VX520		//ABS:ADD:960813
				printTwoString(ipAddress, "�� ��", PRN_NORM, TRUE);
#else
				printTwoStringFarsi(":IP", ipAddress, PRN_NORM);
#endif
#endif

//                //PORT 
//                if (strlen((const char*)LANConnInfo.tcpPort) <= 0) 
//                    strcpy(itemVal, "---");
//                else
//                    sprintf(itemVal, "%s", LANConnInfo.tcpPort);
//                
////              printOneStringEnglish(itemVal, PRN_NORM, ALIGN_RIGHT);
//                printTwoStringNumericFarsi(itemVal,"����",PRN_NORM);
//                printDash();
#if defined(ICT250) || defined(IWL220)//HNO
                //DHCP MODE
                if (LANConnInfo.dhcp)
                    strcpy(itemVal, "����");
                else
                    strcpy(itemVal, "������");

                printTwoStringFarsi(itemVal,"���", PRN_NORM);
#else
                //DHCP MODE 
                if (LANConnInfo.dhcp == 0)
                    strcpy(itemVal, "Static");
                else if (LANConnInfo.dhcp == 1)
                    strcpy(itemVal, "DHCP");
#ifdef VX520
				printTwoString(itemVal, "���", PRN_NORM, TRUE);
#else
			  printTwoStringFarsi(":Mode", itemVal,PRN_NORM);
#endif			  
#endif
                printDash();
            }
        }   
        /** PRINT LOGO */
    printHeaderLogoAndMerchantInfo();//+HNO_980613

    }
}

