#include <string.h>
//#include <stdarg.h>//HNO_IDENT
#include "N_common.h"
#include "N_utility.h"
#include "N_dateTime.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_connectionWrapper.h"
//#include "N_connection.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h"
#include "N_scheduling.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_merchantSpec.h"
#include "N_messaging.h" 
#include "N_displayWrapper.h"
#include "N_display.h"
#include "N_getUserInput.h"
#include "N_terminalReport.h" 
#include "N_terminalSpec.h"
#include "N_error.h" 
#include "N_printerWrapper.h" 
#include "N_printer.h" 
#include "N_config.h" 
#include "N_POSSpecific.h" 
#include "N_initialize.h"
//#include "N_connection.h" 
#include "N_log.h"
#include "N_passManager.h"
#include "N_TMS.h" 

static generalConnInfoST    generalConnectionInfo[SERVERS_COUNT];
static uint8                showPINCounter         = 0; //+MRF_970903


/**
 * Set general connection information.
 * @param   serverID [input]: serverID
 * @param   generalConnInfo [input]: general connection information
 * @return  None.
 */
void setGeneralConnInfo(uint8 serverID, generalConnInfoST* generalConnInfo)
{
    generalConnectionInfo[serverID] = *generalConnInfo;
}

/**
 * Get general connection information.
 * @param   serverID [input]: serverID0
 * @return  general Connection Information.
 */
generalConnInfoST getGeneralConnInfo(uint8 serverID)
{
    return generalConnectionInfo[serverID];
}

/**
 * Disconnect connection and close port.
 * @param   argumentCount [input]: argument Count
 * @see     displayMessage().
 * @see     connection DC().
 * @return  True or False.
 */
uint8 disconnectAndClose(int argumentCount, ...)
{
    uint8 	showMsg = 0;
    va_list arg;
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*****disconnectAndClose*****");//ABS:IMPROVE

    va_start(arg, argumentCount);

    if (argumentCount > 0)
    	showMsg = va_arg(arg, int);
    else
    	showMsg = SHOW_DC_MSG;
    
    va_end(arg);
    
    if (showMsg == SHOW_DC_MSG)
        displayMessage("��� ������ ...", DISPLAY_START_LINE + 1, ALL_LINES);

    if (!connectionDC())
        return FALSE;

    return TRUE;
}

/**
 * Disconnect server connection.
 * @see     displayMessageBox().
 * @return  True or False.
 */
uint8 disconnectServer(void)
{
    if (!disconnectAndClose(0))
        return FALSE;
    
    displayMessageBox("������ �� ������ ��� ��.", MSG_INFO);
    
    return TRUE;
}

/**
 * Initiate one HDLC connection parameters.
 * @param   files [input]: files
 * @param   connectionCount [input]: number of connection
 * @see     showLog().
 * @see     updateFileInfo().
 * @see     addSystemErrorReport().
 * @return  TRUE or FALSE.
 */
uint8 initHDLCConn(filesST* files, uint8 connectionCount)
{   
    int16 				retValue 	= FAILURE;          // file update return value 
    HDLCConnInfoST      HDLCConnInfo;
    
    memset(&HDLCConnInfo, 0, sizeof(HDLCConnInfoST));
    memset(HDLCConnInfo.phone, 0, 17);
    memset(HDLCConnInfo.prefix, 0, 7);

    HDLCConnInfo.retries                = 3;
    HDLCConnInfo.useToneDialing         = TRUE;
    HDLCConnInfo.communicationTimeout   = 50;

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: connFiles");
    retValue = updateFileInfo(files->connFiles[connectionCount], &HDLCConnInfo, sizeof(HDLCConnInfoST));  
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    return TRUE;
}

/**
 * Initiate one PPP connection parameters.
 * @param   connectionCount [input]: number of connection
 * @param   files [input]: files
 * @see     fileExist().
 * @see     readFileInfo().
 * @see     showLog().
 * @see     updateFileInfo().
 * @see     addSystemErrorReport().
 * @return  TRUE or FALSE.
 */
uint8 initPPPConn(filesST* files, uint8 connectionCount)
{//--MRF_NEW8
//	int16 					retValue 	= FAILURE;          // file update return value 
//    uint32                  len 		= sizeof(PPPConfigST);
//    PPPConnInfoST           PPPConnInfo;
//    PPPConfigST             PPPConfig;
//    
//    memset(&PPPConnInfo, 0, sizeof(PPPConnInfoST));
//    memset(&PPPConfig, 0, sizeof(PPPConfigST));
//    memcpy(PPPConnInfo.ipAddress, "\x00\x00\x00\x00", 4);
//    memset(PPPConnInfo.HDLCConnInfo.phone, 0, 17);
//    memset(PPPConnInfo.HDLCConnInfo.prefix, 0, 7);
//
//    PPPConnInfo.ipAddress[4]                        = 0;
//    PPPConnInfo.tcpPort                             = 0;
//    PPPConnInfo.HDLCConnInfo.retries                = 3;
//    PPPConnInfo.HDLCConnInfo.useToneDialing         = TRUE;
//    PPPConnInfo.HDLCConnInfo.communicationTimeout   = 80;
//
//    strcpy(PPPConnInfo.PPPConfig.user, "");
//    strcpy(PPPConnInfo.PPPConfig.password, "");
//
//    if (fileExist(files->pppUserPass) != SUCCESS)
//    {
//        readFileInfo(files->pppUserPass, &PPPConfig, &len);
//        strcpy(PPPConnInfo.PPPConfig.user, PPPConfig.user);
//        strcpy(PPPConnInfo.PPPConfig.password, PPPConfig.password);
//    }
//
//    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: connFiles");
//    retValue = updateFileInfo(files->connFiles[connectionCount], &PPPConnInfo, sizeof(PPPConnInfoST));  
//    if (retValue != SUCCESS) 
//    {
//        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
//        return FALSE;
//    }
//    
//    return TRUE;
}

/**
 * Initiate one LAN connection parameters.
 * @param   files [input]: files
 * @param   connectionCount [input]: number of connection
 * @see     showLog().
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     updateFileInfo().
 * @return  TRUE or FALSE.
 */
uint8 initLanConn(filesST* files, uint8 connectionCount)
{
	int16 				retValue 		= FAILURE;            		// file update return value
    LANConnInfoST       LANConnInfo;
    merchantSpecST      merchantSpec;//+HNO_971212

    memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
    memset(&merchantSpec, 0, sizeof(merchantSpecST));//+HNO_971212

    readMerchantSpec(files, &merchantSpec);//+HNO_971212

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****initLanConn****");

    memcpy(LANConnInfo.ipAddress, LAN_CONN_IP_ADDRESS, 4);//..HNO_971113
    LANConnInfo.ipAddress[4] = 0;

    if (merchantSpec.merchantID[0] == '2')//+HNO_971212
    	LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_BEHPARDAKHT;//HNO_971212
    else
    	LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_FANAVA;//..HNO_971113 , HNO_971212
  
    memcpy(LANConnInfo.localIpAddress, "\x00\x00\x00\x00", 4);
    LANConnInfo.localIpAddress[4] = 0;
    
    memcpy(LANConnInfo.gateway, "\x00\x00\x00\x00", 4);
    LANConnInfo.gateway[4] = 0;
    
    memcpy(LANConnInfo.networkMask, "\x00\x00\x00\x00", 4);
    LANConnInfo.networkMask[4] = 0;
    
    LANConnInfo.dhcp = TRUE;
    //+HNO_980312+++++++++++++++++++++++++++++++
    LANConnInfo.DNSCapability = TRUE;
    memcpy(LANConnInfo.DNS1, "\x00\x00\x00\x00", 4);
    LANConnInfo.DNS1[4] = 0;
    memcpy(LANConnInfo.DNS2, "\x00\x00\x00\x00", 4);
    LANConnInfo.DNS2[4] = 0;

#ifdef LIVE_SWITCH
    strcpy(LANConnInfo.URL, DNS_CONN_URL_STRING_LIVE);
#endif

#ifdef TEST_SWITCH
	strcpy(LANConnInfo.URL, DNS_CONN_URL_STRING_TEST);
#endif

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "DNSConnInfo.URL: %s", LANConnInfo.URL);
    //+++++++++++++++++++++++++++++++++++++++++++++++++
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: connFiles");
    retValue = updateFileInfo(files->connFiles[connectionCount], &LANConnInfo, sizeof(LANConnInfoST));  
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    return TRUE;
}

/**
 * Initiate one tGPRS connection parameters.
 * @param   files [input]: files
 * @param   connectionCount [input]: number of connection
 * @see     showLog().
 * @see     addSystemErrorReport().
 * @see     updateFileInfo().
 * @return  TRUE or FALSE.
 */
uint8 initGPRSConn(filesST* files, uint8 connectionCount)
{
    uint16              retValue        = FAILURE;            // file update return value 
    uint32 				len 			= sizeof(terminalSpecST);
    uint8               model[20]       = {0};//MRF_NEW18
    GPRSConnInfoST      GPRSConnInfo;
    terminalSpecST		terminalSpec;
    merchantSpecST      merchantSpec;//+MRF_971213

    
    memset(&merchantSpec, 0, sizeof(merchantSpecST));//+MRF_971213
    memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));
    memset(&terminalSpec, 0, sizeof(terminalSpecST));
    readMerchantSpec(files, &merchantSpec);//+MRF_971213

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**initGPRSConn**");
    
    retValue = readFileInfo(TERMINAL_SPEC_FILE, &terminalSpec, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    strcpy(GPRSConnInfo.apn, "");        
    memcpy(GPRSConnInfo.ipAddress, GPRS_CONN_IP_ADDRESS, 4);//HNO_CHANGE define in connection wrapper
    GPRSConnInfo.ipAddress[4] = 0;
    
    getDeviceModel(&model[0]); //MRF_NEW18 //ABS_NEW1
    if (strcmp(model, "VEGA3000") != 0)
    {
        if (merchantSpec.merchantID[0] == '2')
            GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT;
        else
            GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA;
    }
    else
    {
        if (merchantSpec.merchantID[0] == '2')
            GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT_V3;
        else
            GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA_V3;
    }
    
    memcpy(GPRSConnInfo.pin, "\x00\x00\x00\x00", 4);
    GPRSConnInfo.pin[4] = 0;
    
    retValue = updateFileInfo(files->connFiles[connectionCount], &GPRSConnInfo, sizeof(GPRSConnInfoST));  
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    return TRUE;
}

/**
 * Convert number of IP to common string.
 * @param   ip [input]: number of IP
 * @param   ipIndex [input]: ipIndex
 * @param   lenInIndex [input]: len In Index 
 * @param   ipStr [output]: the string of IP
 * @return  the IP number which convert to string.
 */
void incompleteIpToStr(uint8* ip, int ipIndex, int lenInIndex, uint8* ipStr) 
{
    int8 counter 	= 0;	// counter 
    int8 index 		= 0;   // index in IP string 
    
    for (counter = 0; counter < ipIndex; counter++) 
    {
        sprintf(&ipStr[index], "%03d", ip[counter]);
        index += 3;
        ipStr[index++] = '.';
    }
    
    if (lenInIndex == 1)
        sprintf(&ipStr[index], "%01d", ip[counter]);
    else if (lenInIndex == 2)
        sprintf(&ipStr[index], "%02d", ip[counter]);
    else if (lenInIndex == 3)
        sprintf(&ipStr[index], "%03d", ip[counter]);
    
    index += lenInIndex;
    ipStr[index] = 0;     
}

/**
 * Edit the number of connection.
 * @param   generalConnInfo [input]: information of connection
 * @param   files [input]: files
 * @see     initHDLCConn().
 * @return  TRUE or FALSE.
 */
uint8 editConnectionNumber(generalConnInfoST* generalConnInfo, filesST *files) 
{        
    uint8	selectedItemIndex       = 0;	// selected item index 
    uint8 	tempConnCount           = 0;    // selected connection number
	uint8 	counter                 = 0;
	uint8 	connCount               = generalConnInfo->connectionCount;
	uint8   selectionItems[][20] = { "� �������", "�� �������", "�� �������","���� �������", "��� �������", "�� �������" };//ABS:FARSI
    
    terminalSpecST   terminalCapability = getTerminalCapability();
    

	if (selectItemPage("����� ������", selectionItems, FARSI, 6, &selectedItemIndex, DISPLAY_TIMEOUT))//ABS:FARSI

    {
        tempConnCount = selectedItemIndex + 1;

        if (tempConnCount != connCount)
        {
            if (tempConnCount > connCount)
            {
                for (counter = connCount; counter < tempConnCount; counter++)
                {
                    if (terminalCapability.modemCapability == TRUE)
                    {
                        generalConnInfo->connectionsType[counter] = CONNECTION_HDLC;
                        initHDLCConn(files, counter);
                    }
                    else if (terminalCapability.GPRSCapability == TRUE)
                    {
                        generalConnInfo->connectionsType[counter] = CONNECTION_GPRS;
                        initGPRSConn(files, counter);
                    }
                }
            }
            
            if (tempConnCount < connCount)
            {
                for (counter = tempConnCount; counter < connCount; counter++)
                {
					generalConnInfo->connectionsType[counter] = CONNECTION_NOT_DEFINED;

                    if (fileExist(files->connFiles[counter]) == SUCCESS)
                        fileRemove(files->connFiles[counter]);
                }
            }

			generalConnInfo->connectionCount = tempConnCount;
            return TRUE; 
        }
    }    
    
    return FALSE;
}

/**
 * Edit the type of connection.
 * @param   connectionCount [input]: the count of connection
 * @param   connectionType [input]: connection type
 * @param   files [input]: files
 * @see     clearDisplay().
 * @see     showLog().
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     selectItemPage().
 * @see     displayMessageBox().
 * @see     initHDLCConn().
 * @see     initPPPConn().
 * @see     initLanConn().
 * @see     initGPRSConn().
 * @see     getGeneralConnInfo().
 * @see     addSystemErrorReport().
 * @see     updateFileInfo().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @return  None.
 */
void editConnectionType(uint8 connectionCount, uint8* connectionType, filesST* files)
{     
    uint8               selectedItemIndex 		= 0;                // selected item index 
    int                 itemsCount 				= 0;
    uint8               selectedType            = 0;                // selected connection type 
    int16               retValue 				= FAILURE;          // file update return value 
    uint32              len 					= 0;                // length that must read from file
    uint8               selectionItems[5][20]	= {0};
    uint8               key 					= KBD_CANCEL;
    uint8               errorMsg[50]            = {0};
    generalConnInfoST 	generalConnInfo;                            // general connection information 
    terminalSpecST      terminalSpec;
    
    memset(selectionItems, 0, sizeof(selectionItems));
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    memset(&terminalSpec, 0, sizeof(terminalSpecST));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Edit connType");
    
    clearDisplay();
    
    len = sizeof(terminalSpecST);
    retValue = readFileInfo(TERMINAL_SPEC_FILE, &terminalSpec, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return;
    }
    
    
    if (terminalSpec.GPRSCapability == TRUE)
    {
    	showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "GPRS");
		strcpy((char*)selectionItems[0], "��� ����");//ABS:FARSI
    	itemsCount++;
    }
    
    if (terminalSpec.LANCapability == TRUE)
    {
        showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "LAN");
		strcpy((char*)selectionItems[0], "�Ș�");//ABS:FARSI
        itemsCount++;
    }
    
    if (terminalSpec.modemCapability == TRUE)
    {
        showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "HDLC");
		strcpy((char*)selectionItems[1], "�� ����");//ABS:FARSI
        itemsCount++;
    }
    

	if (selectItemPage("��� ������", selectionItems, FARSI, itemsCount, &selectedItemIndex, DISPLAY_TIMEOUT))//ABS:FARSI
    {
        if (strcmp((const char*)selectionItems[(int)selectedItemIndex], "�� ����") == 0)//ABS:FARSI
            selectedType = CONNECTION_HDLC;
        else if (strcmp((const char*)selectionItems[(int)selectedItemIndex], "PPP") == 0)//ABS:FARSI
            selectedType = CONNECTION_PPP;
        else if (strcmp((const char*)selectionItems[(int)selectedItemIndex], "�Ș�") == 0)//ABS:FARSI
            selectedType = CONNECTION_LAN;
        else if (strcmp((const char*)selectionItems[(int)selectedItemIndex], "��� ����") == 0)//ABS:FARSI
            selectedType = CONNECTION_GPRS;
        else
            return;

        if (selectedType == *connectionType)
            return; 
        else
        {
        	key = displayMessageBox("�� ��� ������� ���� ������Ͽ", MSG_CONFIRMATION);
			#ifdef VX520
                closeModem(TRUE);//ABS:ADD
			#endif
        	if (key == KBD_ENTER || key == KBD_CONFIRM)
        	{
	            if (selectedType == CONNECTION_HDLC)
	            {
	                if (!initHDLCConn(files, connectionCount))
	                    return;
	            }
	            else if (selectedType == CONNECTION_PPP)
	            {
	                if (!initPPPConn(files, connectionCount))
	                    return;
	            }
	            else if (selectedType == CONNECTION_LAN)
	            {
	                if (!initLanConn(files, connectionCount))
	                    return;
	            }
	            else if (selectedType == CONNECTION_GPRS)
                {
                    if (!initGPRSConn(files, connectionCount))
                    	return;
                }
	
                generalConnInfo = getGeneralConnInfo(files->id); 
	            generalConnInfo.connectionsType[(int)connectionCount] = selectedType;//HNO
	
	            retValue = updateFileInfo(files->generalConnInfoFile, &generalConnInfo, sizeof(generalConnInfoST));  
	            if (retValue != SUCCESS) 
	            {
	                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
	                getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
	                displayMessageBox(errorMsg, MSG_ERROR);
	                return;
	            }
	            else
	                *connectionType = selectedType;
                setGeneralConnInfo(files->id, &generalConnInfo); 
                
                 //..HNO_980903
                if ((getTerminalCapability().TMSCapability ) && (getPOSService().TMSService)&& (!getTerminalCapability().GPRSCapability))
                {
                    if (!changeTMSConnectionType(*connectionType))
                    {
                        displayMessageBox("������� TMS ����� ���!", MSG_ERROR);
                    }
                }
	        }
        }
    }   
} 


/**
 * Setup general configuration.
 * @param   args [input]: Arguments
 * @see     readMerchantSpec().
 * @see     getGeneralConnInfo().
 * @see     clearKeyBuffer().
 * @see     displayAndEditValue().
 * @see     getKey().
 * @see     editConnectionNumber().
 * @see     editNumericValue().
 * @see     isInitialized().
 * @see     showLog().
 * @see     fileRemove().
 * @see     editNumericStringValue().
 * @see     displayMessageBox().
 * @see     updateInitializeFile().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     setGeneralConnInfo().
 * @see     writeMerchantSpec().
 * @see     clearDisplay().
 * @return  None.
 */
void setupGeneralConfig(argumentListST* args)
{
    filesST*            files                   = (filesST*) (args->argumentName);
    int8                page                    = 1;                            				// configuration page */        
    uint8               pressedKey              = 0;                            				// pressed key */   
    uint8               itemVal[100]            = {0};                          				// item value */
    uint8               key                     = 0;      
    uint8               keyPressed              = 0;
    uint8               validKeys[5]            = {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8               showMenu                = TRUE;                         				// loop control, always "TRUE" 
    uint8               updateFile              = FALSE;                        				// udate file after call each edit function 
    uint16              retValue                = FAILURE;                      				// file function retun values 
    uint8               errorMsg[50]            = {0};				
    uint32              SWINii                  = 0;				
    uint8               terminalID[9]           = {0};                          				// terminal ID
    uint8               merchantID[16]          = {0};                          				// merchant ID 
    uint8               depositID[13 + 1]       = {0};				
    uint8               discount[4 + 1]         = {0};				
    uint8               updateInitializeFile    = FALSE;                        				// udate file after call each edit function 
    uint8               maxPage                 = 0;				
    uint8               connectionType 			= 0;//+HNO_971211
    uint8               updateFileLan 			= FALSE;//+HNO_971211
    uint32              len 					= sizeof(LANConnInfoST);//+HNO_971211
    uint32              lenGPRS					= sizeof(GPRSConnInfoST);
	serviceSpecST       posService              = getPOSService();	
    uint8               model[20]               = {0};//+MRF_971213
    generalConnInfoST   generalConnInfo;                                        				// general configuration parameter 
    generalConnInfoST   bkpgeneralConnInfo;                                     				// general configuration parameter, backup 
    merchantSpecST      merchantSpec;  
    LANConnInfoST       LANConnInfo;   	//+HNO_971211														// LAN configuration parameter
    GPRSConnInfoST      GPRSConnInfo;   //+MRF_971213	

    memset(&LANConnInfo, 0, sizeof(LANConnInfoST));//+HNO_971211
    memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));//+MRF_971213
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    memset(&bkpgeneralConnInfo, 0, sizeof(generalConnInfoST));
    memset(&merchantSpec, 0, sizeof(merchantSpecST));
    
    readMerchantSpec(files, &merchantSpec);
    
    generalConnInfo = getGeneralConnInfo(files->id);
    connectionType = generalConnInfo.connectionsType[0];//+HNO_971211
    getDeviceModel(&model[0]);//+MRF_971213

    if (connectionType != CONNECTION_GPRS)
        retValue = readFileInfo(files->connFiles[0], &LANConnInfo, &len);//+HNO_971211
    else
        retValue = readFileInfo(files->connFiles[0], &GPRSConnInfo, &lenGPRS);//+HNO_971211

    clearKeyBuffer();

    while (showMenu) 
    {
        pressedKey = 0;
        updateFile = FALSE;
        bkpgeneralConnInfo = generalConnInfo;
        
        if ((!posService.depositID) && (!posService.discount))
        {
            switch (page) 
            {
                case 1:
                    if (generalConnInfo.connectionCount != 0)
                        sprintf(itemVal, "%d", generalConnInfo.connectionCount);
                    else
                        strcpy(itemVal, "---");

                    displayAndEditValue("����� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break;
                case 2:  
                    if (strcmp(merchantSpec.terminalID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.terminalID);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    if (strcmp(merchantSpec.merchantID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.merchantID);
                    else 
                        strcpy(itemVal, "---");
                        displayAndEditValue("����� �������", itemVal, FALSE, SHOW_ALL);
                        break;
                case 4:
                    if (generalConnInfo.SWINii != 0)
                        sprintf(itemVal, "%ld", generalConnInfo.SWINii);
                    else 
                        strcpy(itemVal, "---");

                    displayAndEditValue("NII ����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                    break;
            }

            maxPage = 4;
        }
        else if ((posService.depositID) && (!posService.discount))
        {
            switch (page) 
            {
                case 1:
                    if (generalConnInfo.connectionCount != 0)
                        sprintf(itemVal, "%d", generalConnInfo.connectionCount);
                    else
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break;
                case 2:  
                    if (strcmp(merchantSpec.terminalID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.terminalID);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    if (strcmp(merchantSpec.merchantID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.merchantID);
                    else 
                        strcpy(itemVal, "---");
                    
					displayAndEditValue("����� �������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 4:
                    if (generalConnInfo.SWINii != 0)
                        sprintf(itemVal, "%ld", generalConnInfo.SWINii);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("NII ����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 5:
                    if ((strcmp(merchantSpec.depositID, "") == 0) && strcmp(merchantSpec.merchantID, ""))
                    {
                        strncpy(merchantSpec.depositID, &merchantSpec.merchantID[6], 9);
                        sprintf(itemVal, "%s", merchantSpec.depositID);
                    }
                    else if (strcmp(merchantSpec.depositID, ""))
                    {
                        sprintf(itemVal, "%s", merchantSpec.depositID);
                    }
                    else if ((strcmp(merchantSpec.depositID, "") == 0) && (strcmp(merchantSpec.merchantID, "") == 0))
                        strcpy(itemVal, "---");
                    
					displayAndEditValue("����� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                    break;
            }

            maxPage = 5;
        }
        else if ((!posService.depositID) && (posService.discount))
        {
            switch (page) 
            {
                case 1:
                    if (generalConnInfo.connectionCount != 0)
                        sprintf(itemVal, "%d", generalConnInfo.connectionCount);
                    else
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break;
                case 2:  
                    if (strcmp(merchantSpec.terminalID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.terminalID);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    if (strcmp(merchantSpec.merchantID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.merchantID);
                    else 
                        strcpy(itemVal, "---");
                    
					displayAndEditValue("����� �������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 4:
                    if (generalConnInfo.SWINii != 0)
                        sprintf(itemVal, "%ld", generalConnInfo.SWINii);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("NII ����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 5:
                    if (strcmp(merchantSpec.discount, ""))
                        strcpy(itemVal, merchantSpec.discount);
                    else 
                    {
                        strcpy(merchantSpec.discount, "---");
                        strcpy(itemVal, "---");
                    }
						displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                    break;
            }

            maxPage = 5;
        }
        else if ((posService.depositID) && (posService.discount))
        {
            switch (page) 
            {
                case 1:
                    if (generalConnInfo.connectionCount != 0)
                        sprintf(itemVal, "%d", generalConnInfo.connectionCount);
                    else
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break;
                
                case 2:  
                    if (strcmp(merchantSpec.terminalID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.terminalID);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("����� ������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    if (strcmp(merchantSpec.merchantID, "")) 
                        sprintf(itemVal, "%s", merchantSpec.merchantID);
                    else 
                        strcpy(itemVal, "---");
					displayAndEditValue("����� �������", itemVal, FALSE, SHOW_ALL);
                    break;
                case 4:
                    if (generalConnInfo.SWINii != 0)
                        sprintf(itemVal, "%ld", generalConnInfo.SWINii);
                    else 
                        strcpy(itemVal, "---");

					displayAndEditValue("NII ����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 5:
                    if ((strcmp(merchantSpec.depositID, "") == 0) && strcmp(merchantSpec.merchantID, ""))
                    {
                        strncpy(merchantSpec.depositID, &merchantSpec.merchantID[6], 9);
                        sprintf(itemVal, "%s", merchantSpec.depositID);
                    }
                    else if (strcmp(merchantSpec.depositID, ""))
                    {
                        sprintf(itemVal, "%s", merchantSpec.depositID);
                    }
                    else if ((strcmp(merchantSpec.depositID, "") == 0) && (strcmp(merchantSpec.merchantID, "") == 0))
                        strcpy(itemVal, "---");
                    
					displayAndEditValue("����� �����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 6:
                    if (strcmp(merchantSpec.discount, "")) 
                        strcpy(itemVal, merchantSpec.discount);
                    else
                    {
                        strcpy(merchantSpec.discount, "---");
                        strcpy(itemVal, "---");
                    }
					displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                        break;
            }

            maxPage = 6;
        }
            
        getKey(&key, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
        
        if (!keyPressed)
            key = KBD_CANCEL;
        
        switch (key)
        {
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
                if ((!posService.depositID) && (posService.discount))
                {
                    switch (page) 
                    {
                        case 1:
                            if (editConnectionNumber(&generalConnInfo, files))
                                updateFile = TRUE;
                            break;
                        case 2:
                            strcpy(terminalID, merchantSpec.terminalID);
							if (editNumericStringValue(terminalID, 8, 5, "����� ������:", TRUE, FALSE))
                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                        updateInitializeFile = TRUE;
                                        strcpy(merchantSpec.terminalID, terminalID);
                                    }
                                }
                                else
                                {
                                    updateInitializeFile = TRUE;
                                    strcpy(merchantSpec.terminalID, terminalID);
                                }
                            }
                            break;
                        case 3:
                            strcpy(merchantID, merchantSpec.merchantID);
                            
                            
                                
							if (editNumericStringValue(merchantID, 15, 2, "����� �������:", TRUE, FALSE))
                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                        updateInitializeFile = TRUE;
                                        strcpy(merchantSpec.merchantID, merchantID);
                                    }
                                }
                                else
                                {
                                    updateInitializeFile = TRUE;
                                    strcpy(merchantSpec.merchantID, merchantID);
                                }
                                
                                if (strcmp(model, "VEGA3000") != 0)//#MRF_971213
                                {
                                    if (merchantSpec.merchantID[0] == '2')
                                    {
                                        generalConnInfo.SWINii = getBehpardakhtNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT;
                                        LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_BEHPARDAKHT;//+HNO_971211
                                    }
                                    else
                                    {
                                        generalConnInfo.SWINii = getFanavaNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA;
                                        LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_FANAVA;//+HNO_971211
                                    }
                                }
                                else
                                {
                                    if (merchantSpec.merchantID[0] == '2')
                                    {
                                        generalConnInfo.SWINii = getBehpardakhtNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT_V3;
                                    }
                                    else
                                    {
                                        generalConnInfo.SWINii = getFanavaNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA_V3;
                                    }
                                }
     
                                updateFile = TRUE;
                                updateFileLan = TRUE;//+HNO_971211
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "tcp port: %d", LANConnInfo.tcpPort);//+HNO_971211
                            }
                            break;
                        case 4:
                            SWINii = generalConnInfo.SWINii;
							if (editNumericValue(&SWINii, "NII ����:", 1, 999, FALSE, FALSE))
                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        generalConnInfo.SWINii = SWINii;
                                        updateFile = TRUE;
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");

                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                    }       
                                }
                                else
                                {
                                    generalConnInfo.SWINii = SWINii;
                                    updateFile = TRUE;
                                }
                            }
                            break;
                       case 5:
                           strcpy(discount, merchantSpec.discount);
							if (editNumericStringValue(discount, 2, 1, "�����:", FALSE, FALSE))
                            {
                                updateInitializeFile = TRUE;
                                strcpy(merchantSpec.discount, discount);
                            }
                            break; 
                        default:
                            break;
                    }
                }
                else
                {
                    switch (page) 
                    {
                        case 1:
                            if (editConnectionNumber(&generalConnInfo, files))
                                updateFile = TRUE;
                            break;
                        case 2:
                            strcpy(terminalID, merchantSpec.terminalID);
							if (editNumericStringValue(terminalID, 8, 5, "����� ������:", TRUE, FALSE))

                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                        updateInitializeFile = TRUE;
                                        strcpy(merchantSpec.terminalID, terminalID);
                                    }
                                }
                                else
                                {
                                    updateInitializeFile = TRUE;
                                    strcpy(merchantSpec.terminalID, terminalID);
                                }
                            }
                            break;
                        case 3:
                            strcpy(merchantID, merchantSpec.merchantID);
							if (editNumericStringValue(merchantID, 15, 2, "����� �������:", TRUE, FALSE))
                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                        updateInitializeFile = TRUE;
                                        strcpy(merchantSpec.merchantID, merchantID);
                                    }
                                }
                                else
                                {
                                    updateInitializeFile = TRUE;
                                    strcpy(merchantSpec.merchantID, merchantID);
                                }
                                
                                if (strcmp(model, "VEGA3000") != 0)//#MRF_971213
                                {
                                    if (merchantSpec.merchantID[0] == '2')
                                    {
                                        generalConnInfo.SWINii = getBehpardakhtNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT;
                                        LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_BEHPARDAKHT;//+HNO_971211
                                    }
                                    else
                                    {
                                        generalConnInfo.SWINii = getFanavaNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA;
                                        LANConnInfo.tcpPort = LAN_CONN_TCP_PORT_FANAVA;//+HNO_971211
                                    }
                                }
                                else
                                {
                                    if (merchantSpec.merchantID[0] == '2')
                                    {
                                        generalConnInfo.SWINii = getBehpardakhtNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_BEHPARDAKHT_V3;
                                    }
                                    else
                                    {
                                        generalConnInfo.SWINii = getFanavaNII();
                                        GPRSConnInfo.tcpPort = GPRS_CONN_TCP_PORT_FANAVA_V3;
                                    }
                                }
                                
                                updateFileLan = TRUE;//+HNO_971211
                                updateFile = TRUE;
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "edited port: %d", LANConnInfo.tcpPort);//+HNO_971211
                            }
                            break;    
                        case 4:
                            SWINii = generalConnInfo.SWINii;
							if (editNumericValue(&SWINii, "NII ����:", 1, 999, FALSE, FALSE))
                            {
                                if (isInitialized(files))
                                {
                                    key = displayMessageBox("���� �� ������� ����. ������Ͽ", MSG_CONFIRMATION);
                                    if (key == KBD_ENTER || key == KBD_CONFIRM)
                                    {
                                        generalConnInfo.SWINii = SWINii;
                                        updateFile = TRUE;
                                        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");

                                        #ifdef INGENICO5100
                                                sramFileRemove(files->masterKeyFile);
                                        #else
                                                fileRemove(files->masterKeyFile);
                                        #endif
                                    }       
                                }
                                else
                                {
                                    generalConnInfo.SWINii = SWINii;
                                    updateFile = TRUE;
                                }
                            }
                            break;    
                       case 5:
                            strcpy(depositID, merchantSpec.depositID);
							if (editNumericStringValue(depositID, 13, 9, "����� �����:", TRUE, FALSE))
                            {
                                updateInitializeFile = TRUE;
                                strcpy(merchantSpec.depositID, depositID);
                            }
                            break; 
                       case 6:
                            strcpy(discount, merchantSpec.discount);
							if (editNumericStringValue(discount, 2, 1, "�����:", FALSE, FALSE))
                            {
                                updateInitializeFile = TRUE;
                                strcpy(merchantSpec.discount, discount);
                            }
                            break; 
                        default:
                            break;
                    }
                }
                
                if (updateFile)
                {
                    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: generalConnInfoFile");
                    retValue = updateFileInfo(files->generalConnInfoFile, &generalConnInfo, 
                                                 sizeof(generalConnInfoST));  
                    if (retValue != SUCCESS) 
                    {
                        memset(errorMsg, 0, sizeof(errorMsg));
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        generalConnInfo = bkpgeneralConnInfo;
                    }
                    updateFile = FALSE;
                    setGeneralConnInfo(files->id, &generalConnInfo); 
                }
                
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "CONNECTION_HDLC: %d", connectionType);
                if((connectionType != CONNECTION_HDLC) && (updateFileLan))//#MRF_971213
                {
                	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "port: %d", LANConnInfo.tcpPort);
                    if (connectionType != CONNECTION_GPRS)//+MRF_971213
                        retValue = updateFileInfo(files->connFiles[0], &LANConnInfo, sizeof(LANConnInfoST));
                    else
                        retValue = updateFileInfo(files->connFiles[0], &GPRSConnInfo, sizeof(GPRSConnInfoST));//+MRF_971213
                    
					if (retValue != SUCCESS)
					{
						memset(errorMsg, 0x00, sizeof(errorMsg));
						addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
						getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
						displayMessageBox(errorMsg, MSG_ERROR);
					}
                }
                
                break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    }    
      
    if (updateInitializeFile)
        writeMerchantSpec(files, merchantSpec);
          
    clearDisplay();
} 


/**
 * Setup Connection type.
 * @param   args [input]: Arguments
 * @see     showLog().
 * @see     getGeneralConnInfo().
 * @see     displayAndEditValue().
 * @see     getKey().
 * @see     editConnectionType().
 * @see     clearDisplay().
 * @return  None.
 */
uint8 setupConnectionType(argumentListST* args)
{
    uint8               connectionNumber	= *((uint8*) (args->argumentName));
    filesST*            files 				= (filesST*) (args->next->argumentName);    
    uint8               connectionType 		= 0;          			// connection type 
    uint8               pressedKey 			= 0;              		// pressed key   
    uint8               keyPressed 			= FALSE;          		// key pressed or not
    uint8               validKeys[3] 		= {KBD_EDIT, KBD_CANCEL};
    uint8               showMenu 			= TRUE;             	// loop control, always "TRUE" 

    generalConnInfoST	generalConnInfo; 

    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "setupConnectionType");

    generalConnInfo = getGeneralConnInfo(files->id);
    connectionType = generalConnInfo.connectionsType[connectionNumber - 1];
    
    clearKeyBuffer();
    while (showMenu) 
    {
        pressedKey = 0;
        
        switch (connectionType) 
        {
            case CONNECTION_HDLC:
				displayAndEditValue("��� ������", "�� ����", FALSE, SHOW_EDIT);
                break;
            case CONNECTION_PPP:
				displayAndEditValue("��� ������", "PPP", FALSE, SHOW_EDIT);
                break;
            case CONNECTION_LAN:
				displayAndEditValue("��� ������", "�Ș�", FALSE, SHOW_EDIT);
				break;
            case CONNECTION_GPRS:
				displayAndEditValue("��� ������", "��� ����", FALSE, SHOW_EDIT);
                break;
            default:
                break;
        }
        
        keyPressed = FALSE;
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
        if (!keyPressed)
            pressedKey = KBD_CANCEL; 
        
        switch (pressedKey)
        {
            case KBD_EDIT:
                editConnectionType(connectionNumber - 1, &connectionType, files);
                break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    } 
        
    clearDisplay();
} 

/**
 * Setting HDLC connection.
 * @param   connectionNumber [input]: connections number
 * @param   files [input]: files
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @see     clearKeyBuffer().
 * @see     displayAndEditValue().
 * @see     getKey().
 * @see     editNumericStringValue().
 * @see     edit2StatusItem().
 * @see     editNumericValue().
 * @see     updateFileInfo().
 * @see     addSystemErrorReport().
 * @see     clearDisplay().
 * @return  None.
 */
void setupHDLCConnection(uint8 connectionNumber, filesST* files)
{
    int8   				page 					= 1;             								// configuration page        
    uint8  				pressedKey 				= 0;       										// pressed key     
    uint8  				keyPressed 				= FALSE;   										// key pressed or not
    uint8  				itemVal[100] 			= {0};   										// item value 
    uint8  				validKeys[5] 			= {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8  				showMenu 				= TRUE;      									// loop control, always "TRUE"      
    uint8  				updateFile 				= FALSE;   										// update file after call each edit function 
    int16 				retValue 				= FAILURE;     									// file function return values 
    uint32 				len 					= sizeof(HDLCConnInfoST);
    uint8               errorMsg[50]            = {0};
    uint8               selectionItems[2][20] 	= {"TONE", "PULSE"};
    uint8               maxPage                 = 0;
    HDLCConnInfoST      HDLCConnInfo; 															// HDLC configuration parameter 
    HDLCConnInfoST      bkpHDLCConnInfo;

    memset(&HDLCConnInfo, 0, sizeof(HDLCConnInfoST));
    memset(&bkpHDLCConnInfo, 0, sizeof(HDLCConnInfoST));

    retValue = readFileInfo(files->connFiles[connectionNumber - 1], &HDLCConnInfo, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }

    clearKeyBuffer(); 

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkpHDLCConnInfo = HDLCConnInfo;

        switch (page) 
        {
            case 1:
                if (strlen((const char*)HDLCConnInfo.prefix) > 0) 
                    strcpy((char*)itemVal, HDLCConnInfo.prefix);    
                else 
                    strcpy((char*)itemVal, "---");

				displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                break;
            case 2:
                if (strlen((const char*)HDLCConnInfo.phone) > 0) 
                    strcpy((char*)itemVal, HDLCConnInfo.phone);    
                else 
                    strcpy((char*)itemVal, "---");
                
				displayAndEditValue("����� ����", itemVal, FALSE, SHOW_ALL);
                break;
            case 3:
                if (HDLCConnInfo.useToneDialing)
                    strcpy(itemVal, "Tone");
                else 
                    strcpy(itemVal, "Pulse");
                
				 displayAndEditValue("��� ����� ����", itemVal, FALSE, SHOW_ALL);
                break;
            case 4:
                if (HDLCConnInfo.retries != 0) 
                    sprintf(itemVal, "%ld", HDLCConnInfo.retries);
                else 
                    strcpy((char*)itemVal, "---");
                
				displayAndEditValue("����� ���� ����", itemVal, FALSE, SHOW_ALL);
                break;
            case 5:
                if (HDLCConnInfo.communicationTimeout != 0) 
                    sprintf(itemVal, "%ld", HDLCConnInfo.communicationTimeout);
                else 
                    strcpy((char*)itemVal, "---");

				displayAndEditValue("���� ������", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                break;          
            default:
                break;
        }

        keyPressed = FALSE; 

        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);

        if (!keyPressed)
            pressedKey = KBD_CANCEL; 

        maxPage = 5;
        switch (pressedKey)
        {
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
                switch (page) 
                {
                    case 1:
						if (editNumericStringValue(HDLCConnInfo.prefix, 6, 0, "��� �����:", TRUE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 2:
						if (editNumericStringValue(HDLCConnInfo.phone, 16, 0, "����� ����:", TRUE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 3:
						if (edit2StatusItem(&HDLCConnInfo.useToneDialing, selectionItems, "��� ����� ����"))
                            updateFile = TRUE;
                        break;
#if defined(INGENICO)//+HNO_971113
					case 4:
						if (editNumericValue(&HDLCConnInfo.retries, "����� ���� ����(1-9)", 1, 9, FALSE, FALSE))
                            updateFile = TRUE;
						break; 
                    case 5:
						if (editNumericValue(&HDLCConnInfo.communicationTimeout, "���� ������(50-180)", 50, 180, FALSE, FALSE))
                            updateFile = TRUE;
                        break;
#else

#ifdef VX520
					case 4:
						if (editNumericValue(&HDLCConnInfo.retries, "����� ���� ����(1-9)", 1, 9, FALSE, FALSE))
                            updateFile = TRUE;
						break;

					case 5:
						if (editNumericValue(&HDLCConnInfo.communicationTimeout, "���� ������(50-180)", 50, 180, FALSE, FALSE))
							updateFile = TRUE;
						break;
#else
					case 4:
						if (editNumericValue(&HDLCConnInfo.retries, "����� ���� ����)1-9(", 1, 9, FALSE, FALSE))
							updateFile = TRUE;
						break;

                    case 5:
						if (editNumericValue(&HDLCConnInfo.communicationTimeout, "���� ������)50-180(", 50, 180, FALSE, FALSE))
                            updateFile = TRUE;
                        break;
#endif
#endif
                    default:
                        break;
                }

                if (updateFile)
                {               
                    retValue = updateFileInfo(files->connFiles[connectionNumber - 1], &HDLCConnInfo, sizeof(HDLCConnInfoST));  
                    if (retValue != SUCCESS) 
                    {
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        HDLCConnInfo = bkpHDLCConnInfo;
                    }
                    updateFile = FALSE;
                }

                break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    } 

    clearDisplay();
}

/**
 * setting PPP connection.
 * @param   connectionNumber [input]: connections number
 * @param   files [input]: files
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @see     clearKeyBuffer().
 * @see     displayAndEditValue().
 * @see     getKey().
 * @see     editIp().
 * @see     editPort().
 * @see     editNumericStringValue().
 * @see     edit2StatusItem().
 * @see     editNumericValue().
 * @see     updateFileInfo().
 * @see     clearDisplay().
 * @return  None.
 */
void setupPPPConnection(uint8 connectionNumber, filesST* files)
{
	int8   					page 					= 1;            								// configuration page 
    uint8  					pressedKey 				= 0;      										// pressed key 
    uint8  					keyPressed 				= FALSE;  										// key pressed or not 
    uint8  					itemVal[100] 			= {0};  										// item value 
    uint8  					validKeys[5] 			= {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8  					showMenu 				= TRUE;     									// loop control, always "TRUE" 
    uint8  					updateFile 				= FALSE;  										// udate file after call each edit function 
    int16 					retValue 				= FAILURE;    									// file function retun values 
    uint32 					len 					= sizeof(PPPConnInfoST);
    uint8 					errorMsg[50] 			= {0};
    uint8                   selectionItems[2][20] 	= {"TONE", "PULSE"};
    uint8                   maxPage                 = 0;
    PPPConnInfoST           PPPConnInfo;
    PPPConnInfoST           bkpPPPConnInfo;
    
    memset(&PPPConnInfo, 0, sizeof(PPPConnInfoST));
    memset(&bkpPPPConnInfo, 0, sizeof(PPPConnInfoST));

    retValue = readFileInfo(files->connFiles[connectionNumber - 1], &PPPConnInfo, &len);

    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }

    clearKeyBuffer(); 

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkpPPPConnInfo = PPPConnInfo;
        memset(itemVal, 0, sizeof(itemVal));

        switch (page) 
        {
            case 1:
                ipToString(PPPConnInfo.ipAddress, itemVal);
                displayAndEditValue("Server IP", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                break;
            case 2:
                if (PPPConnInfo.tcpPort > 0)
                    sprintf(itemVal, "%ld", PPPConnInfo.tcpPort);
                else 
                    strcpy((char*)itemVal, "---");
                displayAndEditValue("Server Port", itemVal, FALSE, SHOW_ALL);
                break;
            case 3:
                if (strlen((const char*)PPPConnInfo.HDLCConnInfo.prefix) > 0) 
                    strcpy((char*)itemVal, PPPConnInfo.HDLCConnInfo.prefix);    
                else 
                    strcpy((char*)itemVal, "---");

                displayAndEditValue("Prefix", itemVal, FALSE, SHOW_ALL);
                break;
            case 4:
                if (strlen((const char*)PPPConnInfo.HDLCConnInfo.phone) > 0) 
                    strcpy((char*)itemVal, PPPConnInfo.HDLCConnInfo.phone);    
                else 
                    strcpy((char*)itemVal, "---");

                displayAndEditValue("Phone", itemVal, FALSE, SHOW_ALL);
                break;
            case 5:
                if (PPPConnInfo.HDLCConnInfo.useToneDialing)
                    strcpy(itemVal, "Tone");
                else 
                    strcpy(itemVal, "Pulse");
                
                displayAndEditValue("Dialing mode", itemVal, FALSE, SHOW_ALL);
                break;
            case 6:
                if (PPPConnInfo.HDLCConnInfo.retries != 0) 
                    sprintf(itemVal, "%ld", PPPConnInfo.HDLCConnInfo.retries);
                else 
                    strcpy((char*)itemVal, "---");

                displayAndEditValue("Retry Number", itemVal, FALSE, SHOW_ALL);
                break;
            case 7:
                if (PPPConnInfo.HDLCConnInfo.communicationTimeout != 0) 
                    sprintf(itemVal, "%ld", PPPConnInfo.HDLCConnInfo.communicationTimeout);
                else 
                    strcpy((char*)itemVal, "---");

                displayAndEditValue("Comm. Timeout", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                break;  
            default:
                break;
        }

        keyPressed = FALSE;

        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);

        if (!keyPressed)
            pressedKey = KBD_CANCEL; 

        maxPage = 7;
        switch (pressedKey)
        {
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
                switch (page) 
                {

                    case 1:
                        if (editIp(PPPConnInfo.ipAddress, "Server IP"))
                            updateFile = TRUE;
                        break;
                    case 2:
                        if (editPort(&PPPConnInfo.tcpPort, "Server Port:", 1, 99999, FALSE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 3:
                        if (editNumericStringValue(PPPConnInfo.HDLCConnInfo.prefix, 6,0, "Prefix:", TRUE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 4:
                        if (editNumericStringValue(PPPConnInfo.HDLCConnInfo.phone, 16,0, "Phone:", TRUE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 5:
                        if (edit2StatusItem(&PPPConnInfo.HDLCConnInfo.useToneDialing, selectionItems, "Dialing mode"))
                            updateFile = TRUE;
                        break;
                    case 6:
                        if (editNumericValue(&PPPConnInfo.HDLCConnInfo.retries, "(1-9): Retry Number", 1, 9, FALSE, FALSE))
                            updateFile = TRUE;
                        break;
                    case 7:
                        if (editNumericValue(&PPPConnInfo.HDLCConnInfo.communicationTimeout, "(50-180): Comm. Timeout", 
                            50, 180, TRUE, FALSE))
                            updateFile = TRUE;
                        break; 
                    default:
                        break;
                }

                if (updateFile)
                {
                    retValue = updateFileInfo(files->connFiles[connectionNumber - 1], &PPPConnInfo, sizeof(PPPConnInfoST));  
                    if (retValue != SUCCESS) 
                    {
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        PPPConnInfo = bkpPPPConnInfo;
                    }
                    updateFile = FALSE;
                }

                break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    } 

    clearDisplay();
} 

/**
 * Setting LAN connection.
 * @param   connectionNumber [input]: connections number
 * @param   files [input]: files
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @see     clearKeyBuffer().
 * @see     ipToString().
 * @see     displayAndEditValue().
 * @see     getNetworkMak().
 * @see     getKey().
 * @see     editIp().
 * @see     editPort().
 * @see     edit2StatusItem().
 * @see     configLAN().
 * @see     updateFileInfo().
 * @see     clearDisplay().
 * @return  None.
 */
void setupLANConnection(uint8 connectionNumber, filesST* files)
{
    int8                page 					= 1;             								// configuration page
    uint8               pressedKey 				= 0;       										// pressed key  
    uint8               keyPressed 				= FALSE;   										// key pressed or not 
    uint8               itemVal[100] 			= {0};   										// item value 
    uint8               validKeys[5] 			= {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8               showMenu 				= TRUE;      									// loop control, always "TRUE" 
    uint8               updateFile 				= FALSE;   										// udate file after call each edit function 
    int16               retValue 				= FAILURE;     									// file function retun values 
    uint32              len 					= sizeof(LANConnInfoST);

    uint8               errorMsg[50] 			= {0};
    uint8               selectionItems[2][20]	= {"����", "�������"};
    uint8               maxPage                 = 0;
    LANConnInfoST       LANConnInfo;   															// LAN configuration parameter 
    LANConnInfoST       bkpLANConnInfo;															// LAN configuration parameter, backup 
    LANConnInfoST       configST;
    uint8               resetPos 				= FALSE;//+HNO_980322

    memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
    memset(&bkpLANConnInfo, 0, sizeof(LANConnInfoST));
    memset(&configST, 0, sizeof(LANConnInfoST));
    memset(itemVal, 0, sizeof(itemVal));

    retValue = readFileInfo(files->connFiles[connectionNumber - 1], &LANConnInfo, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }

    clearKeyBuffer(); 

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
       // updateFile = FALSE;
        bkpLANConnInfo = LANConnInfo;

		switch (page)
		{
		case 1://+HNO_980311
			if (LANConnInfo.DNSCapability)
			{
				showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "dns active");
				strcpy((char*)itemVal, "DNS ����");
			}
			else
				strcpy((char*)itemVal, "DNS ��� ����");

			displayAndEditValue("DNS Mode", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
			break;

		case 2:
			if (LANConnInfo.DNSCapability)//+HNO_980313
				displayAndEditValue("DNS URL", LANConnInfo.URL, FALSE, SHOW_ARROWS);//..HNO_980314                }
			else
			{
				ipToString(LANConnInfo.ipAddress, itemVal);
				displayAndEditValue("Server IP", itemVal, FALSE, SHOW_ALL);//..HNO_980311
			}
			break;
		case 3:
			if (LANConnInfo.tcpPort > 0)
			{
				sprintf(itemVal, "%ld", LANConnInfo.tcpPort);
			}
			else
				strcpy((char*)itemVal, "---");
			displayAndEditValue("Server Port", itemVal, FALSE, SHOW_ALL);
			break;
		case 4:
			if (LANConnInfo.dhcp)
			{
				strcpy((char*)itemVal, "DHCP");
			}
			else
			{
				strcpy((char*)itemVal, "Static");
			}
			displayAndEditValue("DHCP Mode", itemVal, FALSE, SHOW_ALL);
			break;
		case 5:
			if (LANConnInfo.dhcp == FALSE)
			{
				ipToString(LANConnInfo.localIpAddress, itemVal);
				displayAndEditValue("Local IP", itemVal, FALSE, SHOW_ALL);
			}
			else
			{
#ifdef ICT250
				getLocalIpAddress(LANConnInfo.localIpAddress);
				ipToString(LANConnInfo.localIpAddress, itemVal);
				displayAndEditValue("Local IP", itemVal, FALSE, SHOW_ARROWS);
#else
	            getLANConfig(files->connFiles[connectionNumber - 1], &configST); //ABS:CHANGE:960819
#ifdef CASTLES
                    //+HNO_980713 for fix new DNS address 
                    showLog(JUST_CASTLES, __FILE__, __LINE__, TRACE, "", "URL from modem:%s", configST.URL);
                    if(LANConnInfo.DNSCapability == TRUE)
                    {
                    #ifdef LIVE_SWITCH
                        if(strcmp(configST.URL ,DNS_CONN_URL_STRING_LIVE) != 0)
                        {
                            strcpy(LANConnInfo.URL, DNS_CONN_URL_STRING_LIVE);
                            updateFile = TRUE;
                            resetPos = TRUE;
                        }
                    #endif
                    #ifdef TEST_SWITCH
                        if(strcmp(configST.URL ,DNS_CONN_URL_STRING_TEST) != 0)
                        {
                            strcpy(LANConnInfo.URL, DNS_CONN_URL_STRING_TEST);
                            updateFile = TRUE;
                            resetPos = TRUE;
                        }
                    #endif
                    }
#endif

#ifdef VX520
				displayAndEditValue("Local IP", configST.localIpAddress, TRUE, SHOW_ARROWS);
#else
				displayAndEditValue("Local IP", configST.localIpAddress, FALSE, SHOW_ARROWS);
				stringToIP(configST.localIpAddress, LANConnInfo.localIpAddress);//+HNO_980327 to save in lanconn file
				updateFile = TRUE;//+HNO_980327
#endif
#endif
			}
			break;
		case 6:
			if (LANConnInfo.dhcp == FALSE)
			{
				ipToString(LANConnInfo.gateway, itemVal);
				displayAndEditValue("Gateway IP", itemVal, FALSE, SHOW_ALL);
			}
			else
			{
#ifdef ICT250
				getGateWay(LANConnInfo.gateway);
				ipToString(LANConnInfo.gateway, itemVal);
				displayAndEditValue("Gateway IP", itemVal, FALSE, SHOW_ARROWS);
#else
#ifdef VX520
				displayAndEditValue("Gateway IP", configST.gateway, TRUE, SHOW_ARROWS);
#else
				displayAndEditValue("Gateway IP", configST.gateway, FALSE, SHOW_ARROWS);
				stringToIP(configST.gateway, LANConnInfo.gateway);//+HNO_980327 to save in lanconn file
				updateFile = TRUE;//+HNO_980327
#endif
#endif
			}
			break;
		case 7:
			if ((LANConnInfo.dhcp == FALSE) && (LANConnInfo.DNSCapability == FALSE))//+HNO_980314
			{
				ipToString(LANConnInfo.networkMask, itemVal);
				displayAndEditValue("Network Mask", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
			}
			else if ((LANConnInfo.dhcp) && (LANConnInfo.DNSCapability))//+HNO_980314
			{
#ifdef ICT250
				getNetworkMak(LANConnInfo.networkMask);
				ipToString(LANConnInfo.networkMask, itemVal);
				displayAndEditValue("Network Mask", itemVal, FALSE, SHOW_ARROWS);//..HNO_980311
#else
#ifdef VX520
				displayAndEditValue("Network Mask", configST.networkMask, TRUE, SHOW_LEFT_ARROW);
#else
				displayAndEditValue("Network Mask", configST.networkMask, FALSE, SHOW_ARROWS);//..HNO_980311
				stringToIP(configST.networkMask, LANConnInfo.networkMask);//+HNO_980327 to save in lanconn file
				updateFile = TRUE;//+HNO_980327
#endif
#endif
			}
			else if ((LANConnInfo.dhcp) && (LANConnInfo.DNSCapability == FALSE)) //+HNO_980314
			{
#ifdef VX520
				displayAndEditValue("Network Mask", configST.networkMask, FALSE, SHOW_LEFT_ARROW);
#else
				displayAndEditValue("Network Mask", configST.networkMask, FALSE, SHOW_LEFT_ARROW);
				stringToIP(configST.networkMask, LANConnInfo.networkMask);//+HNO_980327 to save in lanconn file
				updateFile = TRUE;//+HNO_980327
#endif
			}
			else    //+HNO_980314
			{
				ipToString(LANConnInfo.networkMask, itemVal);
				displayAndEditValue("Network Mask", itemVal, FALSE, SHOW_ALL);
			}
			break;
		case 8://+HNO_980311
			if ((LANConnInfo.DNSCapability) && (LANConnInfo.dhcp == TRUE))
			{
#ifdef ICT250//+HNO_980328
                    getDNS1Address(LANConnInfo.DNS1);
                    ipToString(LANConnInfo.DNS1, itemVal);
                    displayAndEditValue("DNS1 address", itemVal, FALSE, SHOW_ARROWS);
#else

	#ifdef VX520	//++ABS_980328
				displayAndEditValue("DNS1 address", configST.DNS1, TRUE, SHOW_ALL);
	#else
                    displayAndEditValue("DNS1 address", configST.DNS1, FALSE, SHOW_ARROWS);  //..HNO_980327
                    stringToIP(configST.DNS1, LANConnInfo.DNS1);
                    updateFile = TRUE;//+HNO_980327
	#endif
#endif
                }
                else if ((LANConnInfo.DNSCapability)  && (LANConnInfo.dhcp != TRUE))//+HNO_980320
                {
                    ipToString(LANConnInfo.DNS1, itemVal);
                    displayAndEditValue("DNS1 address", itemVal, FALSE, SHOW_ALL);
                }
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "LANConnInfo.DNS1: %s", LANConnInfo.DNS1);
                break;
            case 9://+HNO_980311
                if ((LANConnInfo.DNSCapability)  && (LANConnInfo.dhcp == TRUE))
                {
#ifdef ICT250//+HNO_980328
                    getDNS2Address(LANConnInfo.DNS2);
                    ipToString(LANConnInfo.DNS2, itemVal);
                    displayAndEditValue("DNS2 address", itemVal, FALSE, SHOW_ARROWS);
#else
	#ifdef VX520	//++ABS_980328
				displayAndEditValue("DNS2 address", configST.DNS2, TRUE, SHOW_ALL);
	#else
                    displayAndEditValue("DNS2 address", configST.DNS2, FALSE, SHOW_LEFT_ARROW); //..HNO_980327
                    stringToIP(configST.DNS2, LANConnInfo.DNS2);
                    updateFile = TRUE;//+HNO_980327;
	#endif
#endif
                }
                else if ((LANConnInfo.DNSCapability)  && (LANConnInfo.dhcp != TRUE))//+HNO_980320
                {
                    ipToString(LANConnInfo.DNS2, itemVal);
                    displayAndEditValue("DNS2 address", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW); 
                }
                break;
                
            default:
                break;
        }
        
        keyPressed = FALSE;      /** key pressed or not */

        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
        
        if (!keyPressed)
            pressedKey = KBD_CANCEL; 
        
        maxPage = 9;//..HNO_980311
        switch (pressedKey)
        {
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            case KBD_NEXT:
                if (page != maxPage)
                {
                    page++;
                }
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
                switch (page) 
                {
                    case 1://+HNO_980311
                        strcpy(selectionItems[0], "DNS ����");
                        strcpy(selectionItems[1], "DNS ��� ����");
                        if (edit2StatusItem(&LANConnInfo.DNSCapability, selectionItems, "DNS Mode"))
                            updateFile = TRUE;
                        break;
                    case 2:
                        if(LANConnInfo.DNSCapability != TRUE)//+HNO_980314
                            if (editIp(LANConnInfo.ipAddress, "Server IP:"))
                                updateFile = TRUE;
                        break;
                    case 3:
                        if (editPort(&LANConnInfo.tcpPort, "Server Port:", 1, 99999, TRUE, FALSE))//#HNO_970903
                            updateFile = TRUE;
                        break;
                    case 4:
                        strcpy(selectionItems[0], "DHCP");
                        strcpy(selectionItems[1], "Static");
                        if (edit2StatusItem(&LANConnInfo.dhcp, selectionItems, "DHCP Mode"))
                            updateFile = TRUE;
                        
                        if (LANConnInfo.dhcp)
                        {
                            ActivateDHCP(TRUE);
#ifdef VX520
                            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "reset other IP ");//ABS:ADD:960819
                            memcpy(LANConnInfo.localIpAddress, "\x00\x00\x00\x00", 4);
                            LANConnInfo.localIpAddress[4] = 0;

                            memcpy(LANConnInfo.gateway, "\x00\x00\x00\x00", 4);
                            LANConnInfo.gateway[4] = 0;

                            memcpy(LANConnInfo.networkMask, "\x00\x00\x00\x00", 4);
                            LANConnInfo.networkMask[4] = 0;
                            updateFile = TRUE;
#endif
                        }
                        else if (updateFile) //reset other IP if change mode from DHCP to Static
                        {
                            ActivateDHCP(FALSE);
                            
                            memcpy(LANConnInfo.localIpAddress, "\x00\x00\x00\x00", 4);
                            LANConnInfo.localIpAddress[4] = 0;
                            
                            memcpy(LANConnInfo.gateway, "\x00\x00\x00\x00", 4);
                            LANConnInfo.gateway[4] = 0;
                            
                            memcpy(LANConnInfo.networkMask, "\x00\x00\x00\x00", 4);
                            LANConnInfo.networkMask[4] = 0;
                        }
                        break;
                    case 5:
                        if (LANConnInfo.dhcp == FALSE)
                        {
                            if (editIp(LANConnInfo.localIpAddress, "Local IP:"))
                                updateFile = TRUE;
                        }
                        break;
                    case 6:
                        if (LANConnInfo.dhcp == FALSE)
                        {
                            if (editIp(LANConnInfo.gateway, "Gateway IP:"))
                                updateFile = TRUE;
                        }
                        break;
                    case 7:
                        if (LANConnInfo.dhcp == FALSE)
                        {
                            if (editIp(LANConnInfo.networkMask, "Network Mask:"))
                                updateFile = TRUE;
                        }
                        break; 
                    case 8:
                        if ((LANConnInfo.DNSCapability) && (LANConnInfo.dhcp == FALSE))//..HNO_980327
                        {
                            if (editIp(LANConnInfo.DNS1, "DNS1 address:"))
                            {
                                updateFile = TRUE;
                                resetPos = TRUE;
                            }   
                        }
                        break;
                        case 9:
                        if ((LANConnInfo.DNSCapability) && (LANConnInfo.dhcp == FALSE))//..HNO_980327
                        {
                            if (editIp(LANConnInfo.DNS2, "DNS2 address:"))
                            {
                                updateFile = TRUE;
                                resetPos = TRUE;
                            }
                        }
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    } 
    //..HNO_980314
	if (updateFile)//++ABS_971129
	{
		retValue = updateFileInfo(files->connFiles[connectionNumber - 1], &LANConnInfo, sizeof(LANConnInfoST));
		if (retValue != SUCCESS)
		{
			memset(errorMsg, 0x00, sizeof(errorMsg));
			addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
			getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
			displayMessageBox(errorMsg, MSG_ERROR);
			LANConnInfo = bkpLANConnInfo;
		}

		if (LANConnInfo.dhcp == FALSE)//+HNO_980410 for implement static connection in ICT
			setStaticLANOption(LANConnInfo);

		setLANConfig(files->connFiles[connectionNumber - 1], LANConnInfo);
		updateFile = FALSE;
		//+HNO_980322
		if (resetPos)
			resetTerminal();
	}

	clearDisplay();
}
/**
 * Setup GPRS connection.
 * @param   connectionNumber [input]: connections number
 * @param   files [input]: files
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @see     clearKeyBuffer().
 * @see     ipToString().
 * @see     displayAndEditValue().
 * @see     getKey().
 * @see     editIp().
 * @see     editPort().
 * @see     edit2StatusItem().
 * @see     editNumericStringValue().
 * @see     updateFileInfo().
 * @see     clearDisplay().
 * @return  None.
 */
void setupGPRSConnection(uint8 connectionNumber, filesST* files)
{
    int8   				page 					= 1;                 
    uint8  				pressedKey 				= 0;      
    uint8  				keyPressed 				= FALSE;  
    uint8  				itemVal[100] 			= {0};  
    uint8  				validKeys[5] 			= {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL, KBD_F2};
    uint8  				showMenu 				= TRUE;     
    uint8  				updateFile 				= FALSE; 	 
    uint16 				retValue 				= FAILURE; 
    uint32 				len 					= sizeof(GPRSConnInfoST);
    uint8 				errorMsg[50] 			= {0};
    uint8 				selectionItems[3][20]	= {"����", "�������"};    
    uint8               item                    = 0;
    uint8               maxPage                 = 0;
    uint8               selectedItemIndex 		= 0;
    uint8               pin[8 + 1]       		= {0};
    uint8  				key                     = 0;
    GPRSConnInfoST      GPRSConnInfo;
    GPRSConnInfoST      bkpGPRSConnInfo;


#ifdef VX680
    setupGPRSConnectionTouch(files, connectionNumber);
        return;
#endif
    memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));
    memset(&bkpGPRSConnInfo, 0, sizeof(GPRSConnInfoST));

    retValue = readFileInfo(files->connFiles[connectionNumber - 1], &GPRSConnInfo, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }
    clearKeyBuffer();
    
    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkpGPRSConnInfo = GPRSConnInfo;
        strcpy(pin,GPRSConnInfo.pin); //+MRF_970903
        switch (page) 
        {

            case 1:
                ipToString(GPRSConnInfo.ipAddress, itemVal);
                displayAndEditValue("Server IP", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                break;
            case 2:
                if (GPRSConnInfo.tcpPort > 0)
                    sprintf(itemVal, "%ld", GPRSConnInfo.tcpPort);
                else 
                    strcpy((char *)itemVal, "---");
                displayAndEditValue("Server Port", itemVal, FALSE, SHOW_ALL);
                break;
//            case 3:
//                if (strlen((const char *)GPRSConnInfo.apn) > 0) 
//                    strcpy((char *)itemVal, GPRSConnInfo.apn);    
//                else 
//                    strcpy((char *)itemVal, "---");
//
//                displayAndEditValue("APN", itemVal, FALSE, SHOW_ALL);
//                break;
            case 3:
                //START #MRF_970903
                memset(itemVal, 0,sizeof(itemVal)); 
                if (strlen((const char *)GPRSConnInfo.pin) > 0) 
                    memset((char *)itemVal, '*', strlen((const char*)GPRSConnInfo.pin));
                else 
                    strcpy((char *)itemVal, "----");
                
                displayAndEditValue("��� PIN", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                //END #MRF_970903
                break;
            default:
                break;
        }

        keyPressed = FALSE;      /** key pressed or not */
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);           

        if (!keyPressed)
            pressedKey = KBD_CANCEL; 
        
        maxPage = 3;
        switch (pressedKey)
        {
            case KBD_NEXT:            
                showPINCounter = 0;
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                showPINCounter = 0;
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
                showPINCounter = 0;
                switch (page) 
                {

                    case 1:
                        if (editIp(GPRSConnInfo.ipAddress, "Server IP"))
                            updateFile = TRUE;
                        break;
                    case 2:
                        if (editPort(&GPRSConnInfo.tcpPort, "Server Port", 1, 99999, FALSE, FALSE))
                            updateFile = TRUE;
                        break;
//                    case 3:
//                        {
//                            strcpy (selectionItems[0] , "MCINET");
//                            strcpy (selectionItems[1] , "MTN");
//                            strcpy (selectionItems[2] , "RighTel");
//                            
//                            if (strcmp(GPRSConnInfo.apn, "MTN") == 0)
//                            {
//                                item = 0;
//                            }
//                            else if (strcmp(GPRSConnInfo.apn, "MCINET") == 0)
//                            {
//                                item = 1;
//                            }
//                            else
//                            {
//                                item = 2;
//                                strcpy(GPRSConnInfo.apn, "RighTel");
//                            }
//                            
//                            if (selectItemPage("APN", selectionItems, ENGLISH, 3, &selectedItemIndex, DISPLAY_TIMEOUT))
//                            {
//                                updateFile = TRUE;
//                                if (strcmp((const char*)selectionItems[selectedItemIndex], "MTN") == 0)
//                                    strcpy(GPRSConnInfo.apn, "MTN");
//                                else if (strcmp((const char*)selectionItems[selectedItemIndex], "MCINET") == 0)
//                                    strcpy(GPRSConnInfo.apn, "MCINET");
//                                else if (strcmp((const char*)selectionItems[selectedItemIndex], "RighTel") == 0)
//                                    strcpy(GPRSConnInfo.apn, "RighTel");
//                                else
//                                    return;
//                            }
//                        }
//                        break;
                    case 3:
                        //if (editNumericStringValue(GPRSConnInfo.pin, 4,0, "��� PIN:", TRUE, FALSE))//-MRF_970905
                        //START #MRF_970905
                    	//-HNO_971113 delete ":"
                        if (getStringValue("��� PIN", 8, 4, pin, TRUE, TRUE, FALSE, "0", 0))
                        {
                            if (strlen(pin) > 0)
                                strcpy(GPRSConnInfo.pin, pin);
                            
                            updateFile = TRUE;
                        }
                        
                        //END #MRF_970905
                        break;
                    default:
                        break;
                }

                if (updateFile)
                {
                    setPINParam(GPRSConnInfo.pin);//+MRF_971009
                    retValue = updateFileInfo(files->connFiles[connectionNumber - 1], &GPRSConnInfo, sizeof(GPRSConnInfoST));  
                    if (retValue != SUCCESS) 
                    {
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        GPRSConnInfo = bkpGPRSConnInfo;
                    }
                    updateFile = FALSE;
                }

                break;
            case KBD_F2://+MRF_970905
                showPINSimCard(GPRSConnInfo.pin);
                break;
            case KBD_CANCEL:
                showPINCounter = 0;
                showMenu = FALSE;
                break;
            default:
                break;
        }
    } 

    clearDisplay();
}
//    int8        	page 					= 1;                 
//    uint8  		pressedKey 				= 0;      
//    uint8  		keyPressed 				= FALSE;  
//    uint8  		itemVal[100]                            = {0};  
//    uint8  		validKeys[5]                            = {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL, KBD_F2};
//    uint8  		showMenu 				= TRUE;     
//    uint8  		updateFile 				= FALSE; 	 
//    uint16 		retValue 				= FAILURE; 
//    uint32 		len 					= sizeof(GPRSConnInfoST);
//    uint8 		errorMsg[50]                            = {0};
//    uint8 		selectionItems[3][20]                   = {"����", "�������"};    
//    uint8               item                                    = 0;
//    uint8               maxPage                                 = 0;
//    uint8               selectedItemIndex                       = 0;
//    uint8               pin[8 + 1]                              = {0};
//    uint8               key                                     = 0;
//    GPRSConnInfoST      GPRSConnInfo;
//    GPRSConnInfoST      bkpGPRSConnInfo;
//
//
//#ifdef VX680
//    setupGPRSConnectionTouch(files, connectionNumber);
//        return;
//#endif
//    memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));
//    memset(&bkpGPRSConnInfo, 0, sizeof(GPRSConnInfoST));
//
//    retValue = readFileInfo(files->connFiles[connectionNumber - 1], &GPRSConnInfo, &len);
//    if (retValue != SUCCESS)  
//    {
//        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
//        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
//        displayMessageBox(errorMsg, MSG_ERROR);
//        return;
//    }
//    clearKeyBuffer();
//    
//    while (showMenu) 
//    {
//        keyPressed = FALSE;
//        pressedKey = 0;
//        updateFile = FALSE;
//        bkpGPRSConnInfo = GPRSConnInfo;
//        strcpy(pin,GPRSConnInfo.pin); //+MRF_970903
//        switch (page) 
//        {
//            case 1://+HNO_980411
//                if (GPRSConnInfo.DNSCapability)
//                {
//                    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "dns active");
//                    strcpy((char*)itemVal, "DNS ����");
//                }
//                else
//                    strcpy((char*)itemVal, "DNS ��� ����");
//
//                displayAndEditValue("DNS Mode", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
//            break;
//
//            case 2:
//                if(GPRSConnInfo.DNSCapability)//+HNO_980313
//                {
////                    strcpy(GPRSConnInfo.URL, DNS_CONN_URL_STRING);//#HNO_980722
//                    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS DNS url: %s", GPRSConnInfo.URL);
//                    displayAndEditValue("DNS URL", GPRSConnInfo.URL, FALSE, SHOW_ARROWS);//..HNO_980314
//                }
//                else
//                {
//                    ipToString(GPRSConnInfo.ipAddress, itemVal);
//                    displayAndEditValue("Server IP", itemVal, FALSE, SHOW_ALL);//..HNO_980311
//                }
//                break;
//
//            case 3:
//                if (GPRSConnInfo.tcpPort > 0)
//                    sprintf(itemVal, "%ld", GPRSConnInfo.tcpPort);
//                else 
//                    strcpy((char *)itemVal, "---");
//                displayAndEditValue("Server Port", itemVal, FALSE, SHOW_ALL);
//                break;
////            case 3:
////                if (strlen((const char *)GPRSConnInfo.apn) > 0) 
////                    strcpy((char *)itemVal, GPRSConnInfo.apn);    
////                else 
////                    strcpy((char *)itemVal, "---");
////
////                displayAndEditValue("APN", itemVal, FALSE, SHOW_ALL);
////                break;
//            case 4:
//                //START #MRF_970903
//                memset(itemVal, 0,sizeof(itemVal)); 
//                if (strlen((const char *)GPRSConnInfo.pin) > 0) 
//                    memset((char *)itemVal, '*', strlen((const char*)GPRSConnInfo.pin));
//                else 
//                    strcpy((char *)itemVal, "----");
//                
//                displayAndEditValue("��� PIN", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
//                //END #MRF_970903
//                break;
//            default:
//                break;
//        }
//
//        keyPressed = FALSE;      /** key pressed or not */
//        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);           
//
//        if (!keyPressed)
//            pressedKey = KBD_CANCEL; 
//        
//        maxPage = 4;
//        switch (pressedKey)
//        {
//            case KBD_NEXT:            
//                showPINCounter = 0;
//                if (page != maxPage)
//                    page++;
//                break;
//            case KBD_PREV:
//                showPINCounter = 0;
//                if (page != 1)
//                    page--;
//                break;
//            case KBD_EDIT:
//                showPINCounter = 0;
//                switch (page) 
//                {
//                    case 1://+HNO_980311
//                        strcpy(selectionItems[0], "DNS ����");
//                        strcpy(selectionItems[1], "DNS ��� ����");
//                        if (edit2StatusItem(&GPRSConnInfo.DNSCapability, selectionItems, "DNS Mode"))
//                                updateFile = TRUE;
//                        break;
//                    case 2:
//                        if(GPRSConnInfo.DNSCapability != TRUE)//+HNO_980314
//                            if (editIp(GPRSConnInfo.ipAddress, "Server IP:"))
//                                updateFile = TRUE;
//                    break;
//
//                    case 3:
//                    	//-HNO_971113 delete ":"
//                        if (editPort(&GPRSConnInfo.tcpPort, "Server Port", 1, 99999, FALSE, FALSE))
//                            updateFile = TRUE;
//                        break;
////                    case 3:
////                        {
////                            strcpy (selectionItems[0] , "MCINET");
////                            strcpy (selectionItems[1] , "MTN");
////                            strcpy (selectionItems[2] , "RighTel");
////                            
////                            if (strcmp(GPRSConnInfo.apn, "MTN") == 0)
////                            {
////                                item = 0;
////                            }
////                            else if (strcmp(GPRSConnInfo.apn, "MCINET") == 0)
////                            {
////                                item = 1;
////                            }
////                            else
////                            {
////                                item = 2;
////                                strcpy(GPRSConnInfo.apn, "RighTel");
////                            }
////                            
////                            if (selectItemPage("APN", selectionItems, ENGLISH, 3, &selectedItemIndex, DISPLAY_TIMEOUT))
////                            {
////                                updateFile = TRUE;
////                                if (strcmp((const char*)selectionItems[selectedItemIndex], "MTN") == 0)
////                                    strcpy(GPRSConnInfo.apn, "MTN");
////                                else if (strcmp((const char*)selectionItems[selectedItemIndex], "MCINET") == 0)
////                                    strcpy(GPRSConnInfo.apn, "MCINET");
////                                else if (strcmp((const char*)selectionItems[selectedItemIndex], "RighTel") == 0)
////                                    strcpy(GPRSConnInfo.apn, "RighTel");
////                                else
////                                    return;
////                            }
////                        }
////                        break;
//                    case 4:
//                        //if (editNumericStringValue(GPRSConnInfo.pin, 4,0, "��� PIN:", TRUE, FALSE))//-MRF_970905
//                        //START #MRF_970905
//                    	//-HNO_971113 delete ":"
//                        if (getStringValue("��� PIN", 8, 4, pin, TRUE, TRUE, FALSE, "0", 0))
//                        {
//                            if (strlen(pin) > 0)
//                                strcpy(GPRSConnInfo.pin, pin);
//                            
//                            updateFile = TRUE;
//                        }
//                        
//                        //END #MRF_970905
//                        break;
//                    default:
//                        break;
//                }
//
//                if (updateFile)
//                {
//                    setPINParam(GPRSConnInfo.pin);//+MRF_971009
//                    retValue = updateFileInfo(files->connFiles[connectionNumber - 1], &GPRSConnInfo, sizeof(GPRSConnInfoST));  
//                    if (retValue != SUCCESS) 
//                    {
//                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
//                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
//                        displayMessageBox(errorMsg, MSG_ERROR);
//                        GPRSConnInfo = bkpGPRSConnInfo;
//                    }
//                    updateFile = FALSE;
//                }
//
//                break;
//            case KBD_F2://+MRF_970905
//                showPINSimCard(GPRSConnInfo.pin);
//                break;
//            case KBD_CANCEL:
//                showPINCounter = 0;
//                showMenu = FALSE;
//                break;
//            default:
//                break;
//        }
//    } 
//
//    clearDisplay();
//} 

/**
 * Setup connection.
 * @param   args [input]: arguments
 * @see     showLog().
 * @see     getGeneralConnInfo().
 * @see     setupHDLCConnection().
 * @see     setupPPPConnection().
 * @see     setupLANConnection().
 * @see     setupGPRSConnection().
 * @see     displayAndEditValue().
 * @return  None.
 */
void setupConnection(argumentListST* args)
{
    uint8               connectionNumber	= *((uint8*) (args->argumentName));
    filesST*            files 				= (filesST*) (args->next->argumentName);
    uint8               connectionType 		= 0;                        //connection type 
    generalConnInfoST   generalConnInfo;                                // general configuration information

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "first of setup connection");
    
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));

    generalConnInfo = getGeneralConnInfo(files->id); 
    
    connectionType = generalConnInfo.connectionsType[connectionNumber - 1];
    
    switch (connectionType) 
    {
        case CONNECTION_HDLC:
            setupHDLCConnection(connectionNumber, files);
            break;
        case CONNECTION_PPP:
            setupPPPConnection(connectionNumber, files);
            break;
        case CONNECTION_LAN:
            setupLANConnection(connectionNumber, files);
            break;
        case CONNECTION_GPRS:
            setupGPRSConnection(connectionNumber, files);
            break;
        default:
            break;
    }
} 

/**
 * PreDial.
 * @param   files [input]: files
 * @param   index [input]: index
 * @see     showLog().
 * @see     checkConnectionStatus().
 * @see     disconnectAndClose().
 * @see     getGeneralConnInfo().
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     configHDLC().
 * @see     dial().
 * @return  None.
 */
void preDial(filesST* files, uint8 index)
{
	uint32					len					= 0;
	int16					retValue			= -1;
    generalConnInfoST		generalConnInfo;
    HDLCConnInfoST          HDLCConnInfo;
    PPPConnInfoST           PPPConnInfo;
	
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    memset(&HDLCConnInfo, 0, sizeof(HDLCConnInfoST));
    memset(&PPPConnInfo, 0, sizeof(PPPConnInfoST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****preDial****");
    
    retValue = checkConnectionStatus();
	if (retValue != DISCONNECTED && retValue != MODEM_HDLC_IS_CONNECTED 
            && retValue != MODEM_HDLC_IS_SDLC_MODE) //MRF_NEW3
		disconnectAndClose(1, NOT_SHOW_DC_MSG);


    generalConnInfo = getGeneralConnInfo(files->id); 

    switch (generalConnInfo.connectionsType[(int)index])//HNO_CANGHE (casting)
    {
        case CONNECTION_HDLC:
            len = sizeof(HDLCConnInfoST); 
				retValue = readFileInfo(files->connFiles[(int)index], &HDLCConnInfo, &len);//HNO_CANGHE (casting)
            if (retValue != SUCCESS)  
            {
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ERR in read connfile %d", index);
                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
            }

            configHDLC(HDLCConnInfo.phone, HDLCConnInfo.prefix, 
                    HDLCConnInfo.retries, HDLCConnInfo.useToneDialing, HDLCConnInfo.communicationTimeout);
            dialHDLC(); //MRF_NEW3
            break;
        case CONNECTION_PPP:
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PreDial PPP");
            len = sizeof(PPPConnInfoST);
				retValue = readFileInfo(files->connFiles[(int)index], &PPPConnInfo, &len);//HNO_CANGHE (casting)
            if (retValue != SUCCESS)  
            {
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ERR in read connfile %d", index);
                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
            }
            configPPP(PPPConnInfo.ipAddress, PPPConnInfo.tcpPort, PPPConnInfo.PPPConfig.user, PPPConnInfo.PPPConfig.password, 
                    PPPConnInfo.HDLCConnInfo.phone, PPPConnInfo.HDLCConnInfo.prefix, PPPConnInfo.HDLCConnInfo.retries, 
                    PPPConnInfo.HDLCConnInfo.useToneDialing, PPPConnInfo.HDLCConnInfo.communicationTimeout);
            dialPPP(); //MRF_NEW3
            break;
        case CONNECTION_LAN:
        case CONNECTION_GPRS:
            break;
        default:
            break;
    }  
    
    setPreDialCheck(TRUE); //MRF_NEW3
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PreDial DONE");
}

/**
 * Do Connection.
 * @param   files [input]: files
 * @see     showLog().
 * @see     getGeneralConnInfo().
 * @see     displayMessage().
 * @see     checkConnectionStatus().
 * @see     preDial().
 * @see     clearDisplay().
 * @see     displayStringFarsi().
 * @see     HDLCConnect().
 * @see     delay().
 * @see     dial().
 * @see     PPPConnect().
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     configLAN().
 * @see     disconnectAndClose().
 * @see     displayMessageBox().
 * @return  None.
 */
uint8 doConnect(filesST* files)
{
    uint32					len					= 0;
    uint8                   connStatus          = DISCONNECTED;
    uint16  				connCounter			= 0;
    uint32                  currentTry          = 1;
    uint8                   connMsg[20]         = {0};
    uint8                   connMsg2[20]        = {0};
    uint8  	 				atLeastOneConnSet 	= FALSE;
    uint8 					connResult			= CONNECTION_FAILED;
    uint8 					connectionType 		= 0;
    int16					retValue			= FAILURE;
    generalConnInfoST 		generalConnInfo;   
    LANConnInfoST           LANConnInfo;
    GPRSConnInfoST          GPRSConnInfo;
	
    memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
    memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " doConnect");

    generalConnInfo = getGeneralConnInfo(files->id);

    for (connCounter = 0; connCounter < generalConnInfo.connectionCount; connCounter++)
    {
        connectionType = generalConnInfo.connectionsType[connCounter];
        setConnectionType(connectionType); 
        
        //mgh: add for test v5s POS
        if (connCounter != 0)
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
          
        connStatus = checkConnectionStatus();
        if (connStatus == MODEM_HDLC_IS_CONNECTED || connStatus == MODEM_PPP_IS_CONNECTED /*MRF_IDENT*/
                || connStatus == LAN_IS_CONNECTED /*|| connStatus == GPRS_IS_CONNECTED*/)
            return TRUE;

		switch (connectionType)
        {
            case CONNECTION_HDLC:
            {         
            	if (connStatus == DISCONNECTED || connStatus == MODEM_HDLC_IS_SDLC_MODE) //MRF_NEW3
    	            preDial(files, connCounter);
    	        
                atLeastOneConnSet = TRUE;
                for (currentTry = 1; currentTry <= getRetries()/*HDLCConnInfo.retries*/; currentTry++)
                { 
                    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "currentTry: %ld", currentTry);
                    //mgh: changed
                    sprintf(connMsg, "%s%d", "������", (connCounter + 1));
                    sprintf(connMsg2, "%s%ld", "����� ����", currentTry);
                    clearDisplay();
                    displayStringFarsi(connMsg, DISPLAY_START_LINE + 1, PRN_NORM, ALIGN_CENTER);
                    displayStringFarsi(connMsg2, DISPLAY_START_LINE + 2, PRN_NORM, ALIGN_CENTER);
                    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "currentTry: %ld", currentTry);
                    connResult = HDLCConnect(); 
#ifndef VX520
					if ((connResult == CONNECTION_RETRY) && (currentTry == getRetries()))//+HNO_980631
						disconnectModem(TRUE);
#endif

                    if (connResult != CONNECTION_RETRY)
                        break;
                    else if (currentTry <= getRetries())
                    {
                        delay(1000, JUST_INGENICO);
                        #if defined(ICT250) || defined(IWL220)//HNO_ADD because pos hang after second retry
                                preDial(files, connCounter);
						#elif defined (VX520)
                            dialHDLC(); //MRF_NEW3 //#HNO_980630 comment not usefull
                        #endif
                    }
                 }
#ifdef VX520		//++ABS:980707
					if(connectionStatus != HDLC_IS_CONNECTED)
						closeModem(TRUE);
#endif
                break;
            }
            case CONNECTION_PPP:
            {            
            	if (connStatus == DISCONNECTED)
    	        {
    	        	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "DC,call dial");
    	            preDial(files, connCounter);
    	        }
    	        
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "do connect PPP");       

                atLeastOneConnSet = TRUE;
                for (currentTry = 1; currentTry <= getRetries()/*PPPConnInfo.HDLCConnInfo.retries*/; currentTry++)
                { 
                    sprintf(connMsg, "%s%d    %s%ld", "������", (connCounter + 1), "����� ����", currentTry);
                    clearDisplay();
                    displayStringFarsi(connMsg, DISPLAY_START_LINE + 1, PRN_NORM, ALIGN_CENTER);

                    connResult = PPPConnect();
                    if (connResult != CONNECTION_RETRY)
                        break;
                    else if (currentTry <= getRetries())
                        dial(TRUE); //start again
                }
                break;
            }
            case CONNECTION_LAN:
            {
            	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "DO CONNECT LAN");
                
       			len = sizeof(LANConnInfoST); 
                retValue = readFileInfo(files->connFiles[connCounter], &LANConnInfo, &len);
                if (retValue != SUCCESS)  
                {
                    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ERR read file!");
                    addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                }
                
                retValue = setLANConfig(&(files->id), LANConnInfo); 
                if (retValue != TRUE) 
                {
					showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "SetLANConfig is Failed");
//                    displayMessageBox("Config LAN ����� ���!", MSG_ERROR);
//                    return FALSE; //--MRF_NEW3
                }
                
                atLeastOneConnSet = TRUE;
                connResult = connectLAN(files->connFiles[connCounter], LANConnInfo, connCounter, TRUE);
                break;
            }
            case CONNECTION_GPRS:
            {                    	
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS conn");
                
                len = sizeof(GPRSConnInfoST); 
                retValue = readFileInfo(files->connFiles[connCounter], &GPRSConnInfo, &len);
                if (retValue != SUCCESS)  
                    addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
//                retValue = configGPRS(GPRSConnInfo.ipAddress, GPRSConnInfo.tcpPort, GPRSConnInfo.apn, GPRSConnInfo.pin);
                retValue = configGPRS(&GPRSConnInfo);
                if (retValue != SUCCESS)//#MRF_970829
                {
					showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS Error");
	                addSystemErrorReport(retValue, ERR_TYPE_GSM);
                    return FALSE;
                }
                
                atLeastOneConnSet = TRUE;

                sprintf(connMsg, " %s %d", "������", (connCounter + 1));
                clearDisplay();
                displayStringFarsi(connMsg, DISPLAY_START_LINE + 1, PRN_NORM, ALIGN_CENTER);

                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "DNSCapability is: %d", GPRSConnInfo.DNSCapability);
                connResult = GPRSConnect(GPRSConnInfo.DNSCapability);//..HNO_980423
                break;
            }
            default:
                break;
        } //switch-case End
            
        if (connResult == CONNECTION_SUCCESS)
		{
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "CONNECTION SUCCESS");
            break;
        }
		else
		{
			disconnectAndClose(1, NOT_SHOW_DC_MSG);
			if (connResult == CONNECTION_USER_CANCEL)
	            break;
		}
    }
    
    if (!atLeastOneConnSet)
	{
		displayMessageBox("�� � �� ������� ���� ��� �����!", MSG_ERROR);
		return FALSE;
	}
    
	if (connResult != CONNECTION_SUCCESS)
		return FALSE;
           
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "TRUE connect");
    return TRUE;
}

/**
 * Update PPP Configuration.
 * @param   files [input]: files
 * @param   user [input]: user
 * @param   password [input]: password
 * @param   connectionNumber [input]: connection number
 * @see     readFileInfo().
 * @see     addSystemErrorReport().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().
 * @see     showLog().
 * @return  TRUE or FALSE.
 */
uint8 updatePPPConfig(filesST* files, uint8* user, uint8* password, int connectionNumber)
{
    uint16           retValue        = 0;
    uint8            errorMsg[50]    = {0};
    uint32           len             = sizeof(PPPConnInfoST);
    PPPConnInfoST    PPPConnInfo;
    
    memset(&PPPConnInfo, 0, sizeof(PPPConnInfoST));
    
    retValue = readFileInfo(files->connFiles[connectionNumber], &PPPConnInfo, &len);
    if (retValue != SUCCESS)  
    {
        memset(errorMsg, 0, sizeof(errorMsg));
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }

    strcpy(PPPConnInfo.PPPConfig.user, user);
    strcpy(PPPConnInfo.PPPConfig.password, password);

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: connFiles");
    retValue = updateFileInfo(files->connFiles[connectionNumber], &PPPConnInfo, 
                                 sizeof(PPPConnInfoST));  
    if (retValue != SUCCESS) 
    {
        memset(errorMsg, 0, sizeof(errorMsg));
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }
        
    return TRUE;
}


/**
 * send message.
 * @param   buff [output]: message to send
 * @param   packedSize [input]: size of message
 * @see     sendViaTCP()
 * @see     sendViaModem()
 * @return  TRUE or FALSE.
 */
uint8 connSendBuff(uint8* buff, uint16 packedSize)  
{
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sending msg...");
    comportHexShow(buff, packedSize);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkConnectionStatus");
    switch (checkConnectionStatus()) 
    {
        case MODEM_HDLC_IS_CONNECTED: 
            return sendViaModem(buff, packedSize);
            break;
        case MODEM_PPP_IS_CONNECTED:
        case LAN_IS_CONNECTED:
        case GPRS_IS_CONNECTED:
            return sendViaTCP(buff, packedSize);
            break;
        default:
            break;
    }
    
    return FALSE;
}


/**
 * Receive message.
 * @param   buff [input]: received message
 * @param   packedSize [input]: size of message
 * @see     showLog().
 * @see     checkConnectionStatus().
 * @see     receiveViaTCP().
 * @see     receiveViaModem().
 * @return  TRUE or FALSE.
 */
uint8 connReceiveBuff(uint8* buff, uint16* packedSize) 
{
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "recieving msg...");
    
    switch (checkConnectionStatus())
    {
        case MODEM_HDLC_IS_CONNECTED:
            return receiveViaModem(buff, packedSize);
            break;
        case MODEM_PPP_IS_CONNECTED:
        case LAN_IS_CONNECTED:
        case GPRS_IS_CONNECTED:
            return receiveViaTCP(buff, packedSize);
            break;
        default:
            break;
    }
    
    return FALSE;
}

//+MRF_970903
void showPINSimCard(uint8* pin)
{
    uint8   msg[40]      = {0};
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pin is : %s", pin);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "counter is : %d", showPINCounter);
    
    showPINCounter++;
    if (showPINCounter == 5)
    {
        showPINCounter = 0;
        sprintf(msg,"��� ��� %s", pin);//-HNO_971113 remove ":"
        displayMessageBox(msg, MSG_INFO);
    }
    
    return;
}

//HNO_TCP
/**
 * setup and configure the PcPos connection.
 * @param   args
 * @see     clearKeyBuffer()
 * @see     displayMessageBox()
 * @see     showlog()
 * @see     getGeneralConnInfo()
 * @see     getLANConfig()
 * @see     displayAndEditValue()
 * @see     getKey()
 * @see     selectItemPage()
 * @see     editPort()
 * @see     editip()
 * @see     ipToString()
 * @see     updateFileInfo()
 * @return  NONE.
 */
uint8 setupPcPosConnection(argumentListST* args)//HNO_TCP
{
    filesST*            files                           = (filesST*) (args->argumentName);
    int8                page                            = 1;             	// configuration page
    uint8               pressedKey                      = 0;       			// pressed key  
    uint8               keyPressed                      = FALSE;   			// key pressed or not 
    uint8               itemVal[100]                    = {0};   			// item value 
    uint8               validKeys[5]                    = {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8               showMenu                        = TRUE;      		// loop control, always "TRUE" 
    int16               retValue                        = FAILURE;     		// file function retun values 
    uint8               selectionItems[2][20]           = {"Enable", " Disable"};
    uint8               pcPosconnType[2][20]            = {0, 0};
    uint8               maxPage                         = 0;
    uint8               selectedItemIndex               = 0;                // selected item index 
    int                 itemsCount                      = 0;
    uint8               connectionType                  = 0;
    uint8               posConnectionType               = 0;
    uint8               connectionNumber                = 0;
    uint32              len                             = sizeof(PcPosConnInfoST);
    terminalSpecST      terminalCapability              = getTerminalCapability();
    serviceSpecST       POSService                      = getPOSService();
    generalConnInfoST 	generalConnInfo;
    PcPosConnInfoST     pcposConnInfo;  //structure for save pcpos 


    memset(&pcposConnInfo, 0, sizeof(PcPosConnInfoST));
    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    memset(itemVal, 0, sizeof(itemVal));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*********setup PcPos Connection********");
    
    clearKeyBuffer(); 
    
    if ((!terminalCapability.pcPosCapability) || (!POSService.PcPOSService))
    {
        displayMessageBox("������ ��� ����", MSG_ERROR);
        return FALSE;
    }
    
    generalConnInfo = getGeneralConnInfo(files->id);
    posConnectionType = generalConnInfo.connectionsType[connectionNumber];
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "posConnectionType :%d", posConnectionType);
    
    retValue = fileExist(PCPOS_INFO_FILE);
    if(retValue == SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos file exist");
        retValue = readFileInfo(PCPOS_INFO_FILE, &pcposConnInfo, &len);
        if (retValue != SUCCESS)  
        {
            displayMessageBox("���� ������ ���", MSG_ERROR);
            fileRemove(PCPOS_INFO_FILE);
            return FALSE;
        }
        if(strcmp(pcposConnInfo.connTypePcPos, "TCP") == 0)
            connectionType = CONNECTION_LAN;

        else
            connectionType = CONNECTION_HDLC;
    }
    
    else
    {
        if(posConnectionType == CONNECTION_LAN)
            connectionType = CONNECTION_LAN;

        else
            connectionType = CONNECTION_HDLC;
    }
    
    if((connectionType == CONNECTION_LAN) && (posConnectionType == CONNECTION_LAN))
    {
        if(!(getLANConfig(files->connFiles[0], &pcposConnInfo.lanPcPos)))
            displayMessageBox("������� ����� ���", MSG_ERROR); 
    }

    switch(connectionType)
    {
        case CONNECTION_HDLC:
            strcpy(pcPosconnType[0], "Serial");
            itemsCount++;
            strcpy(pcPosconnType[1], "TCP");
            itemsCount++;

            break;

        case CONNECTION_LAN:
            strcpy(pcPosconnType[0], "TCP");
            itemsCount++;
            strcpy(pcPosconnType[1], "Serial");
            itemsCount++; 

            break;

        default:
            break;
        }

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos conn type11: %s", pcposConnInfo.connTypePcPos);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos conn type22: %s", &pcposConnInfo.connTypePcPos);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos port: %d", pcposConnInfo.pcposPort);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos localIpAddress: %s", pcposConnInfo.lanPcPos.localIpAddress);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos gateway: %s", pcposConnInfo.lanPcPos.gateway);
    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;

        switch (page) 
        {
            case 1:
                if(strcmp(pcPosconnType[0], "Serial") == 0)
                {
                    displayAndEditValue("Connection Type", pcPosconnType[0], FALSE, SHOW_EDIT);//..ABS_971022
                    strcpy(pcposConnInfo.connTypePcPos , pcPosconnType[0]);//..ABS_971022
                    retValue = updateFileInfo(PCPOS_INFO_FILE, &pcposConnInfo, sizeof(PcPosConnInfoST));  
                    if (retValue != SUCCESS) 
                        displayMessageBox("������� ����� ���", MSG_WARNING);
                }
                else
                    displayAndEditValue("Connection Type", pcPosconnType[0], FALSE, SHOW_EDIT_RIGHT_ARROW);//..ABS_971022
                    strcpy(pcposConnInfo.connTypePcPos , pcPosconnType[0]);//..ABS_971022
                    break;
                    
            case 2:
                if(strcmp(pcPosconnType[0], "Serial") == 0)
                    return;
                
                if ((connectionType == CONNECTION_LAN)&&(pcposConnInfo.lanPcPos.dhcp))
                    
                    strcpy((char*)itemVal, "DHCP");
                else
                    strcpy((char*)itemVal, "Static");

                if(posConnectionType != CONNECTION_LAN)
                    displayAndEditValue("DHCP Mode", itemVal, FALSE, SHOW_ALL);
                else
                    displayAndEditValue("DHCP Mode", itemVal, FALSE, SHOW_ARROWS);
                break;

            case 3:
                sprintf(itemVal, "%ld", pcposConnInfo.pcposPort);
                displayAndEditValue("Port", itemVal, FALSE, SHOW_ALL);
                break;
                
            case 4:
                if(posConnectionType != CONNECTION_LAN)
                {
                    strcpy(itemVal, pcposConnInfo.lanPcPos.localIpAddress);
                    displayAndEditValue("Local IP", itemVal, FALSE, SHOW_ALL);
                }
                
                else if(posConnectionType == CONNECTION_LAN)
                {
#ifndef VX520
                    strcpy(itemVal, pcposConnInfo.lanPcPos.localIpAddress);//..HNO_980304
                    displayAndEditValue("Local IP", itemVal, FALSE, SHOW_ARROWS);
                    
#else
                    memcpy(itemVal, pcposConnInfo.lanPcPos.localIpAddress,4);
                    displayAndEditValue("Local IP", itemVal, TRUE, SHOW_ARROWS);//..ABS_971023
#endif
                }

                break;
                
            case 5:
                if(posConnectionType != CONNECTION_LAN)
                {
                    strcpy(itemVal, pcposConnInfo.lanPcPos.gateway);
#ifndef VX520
					displayAndEditValue("Gateway IP", itemVal, FALSE, SHOW_ALL);
#else
					displayAndEditValue("Gateway IP", itemVal, TRUE, SHOW_ALL);//..ABS_971023
#endif
                }
                else if(posConnectionType == CONNECTION_LAN)
                {
                    strcpy(itemVal, pcposConnInfo.lanPcPos.gateway);
#ifndef VX520
					displayAndEditValue("Gateway IP", itemVal, FALSE, SHOW_ARROWS);
#else
					displayAndEditValue("Gateway IP", itemVal, TRUE, SHOW_ARROWS);//..ABS_971023
#endif
                }

                break;
                
            case 6:
                if(posConnectionType != CONNECTION_LAN)
                {
                    strcpy(itemVal, pcposConnInfo.lanPcPos.networkMask);
#ifndef VX520
					displayAndEditValue("Network Mask", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
#else
					displayAndEditValue("Network Mask", itemVal, TRUE, SHOW_EDIT_LEFT_ARROW);//..ABS_971023
#endif
                }
                else if(posConnectionType == CONNECTION_LAN)
                {
                    strcpy(itemVal, pcposConnInfo.lanPcPos.networkMask);
#ifndef VX520
					displayAndEditValue("Network Mask", itemVal, FALSE, SHOW_LEFT_ARROW);
#else
					displayAndEditValue("Network Mask", itemVal, TRUE, SHOW_LEFT_ARROW);//..ABS_971023
#endif
                }
                    
                break;
            default:
                break;
        }
        
    keyPressed = FALSE;  

    getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);

    if (!keyPressed)
        pressedKey = KBD_CANCEL; 
    
    if(strcmp(pcPosconnType[0], "Serial") == 0)
        maxPage = 1;
    else
        maxPage = 6;
    switch (pressedKey)
    {
        case KBD_CANCEL:
            showMenu = FALSE;
            break;
        case KBD_NEXT:
            if (page != maxPage)
            {
                page++;
            }
            break;
        case KBD_PREV:
            if (page != 1)
                page--;
            break;
        case KBD_EDIT:
            switch (page) 
            {
                case 1:
                    if (selectItemPage("Connection Type", pcPosconnType, ENGLISH, itemsCount, &selectedItemIndex, DISPLAY_TIMEOUT))
                    {
                        if (strcmp((const char*)pcPosconnType[(int)selectedItemIndex], "TCP") == 0)
                        {
                            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "tcp");
                            strcpy(pcPosconnType[0],"TCP");
                            strcpy(pcPosconnType[1],"Serial");
                        }

                         else if (strcmp((const char*)pcPosconnType[(int)selectedItemIndex], "Serial") == 0)
                        {
                            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serial");
                            strcpy(pcPosconnType[0],"Serial");
                            strcpy(pcPosconnType[1],"TCP");
                        }
                        
                        strcpy(pcposConnInfo.connTypePcPos , pcPosconnType[0]);//..ABS_971022
                    }
                    break;
                    
                case 2:
                    if(posConnectionType != CONNECTION_LAN)
                    {
//                        if(strcmp(pcPosconnType[0], "Serial") == 0)
//                            break;
                        strcpy (selectionItems[0] , "DHCP");
                        strcpy (selectionItems[1] , "Static");

                        edit2StatusItem(&pcposConnInfo.lanPcPos.dhcp, selectionItems, "DHCP Mode");
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "dhcp mode: %d", pcposConnInfo.lanPcPos.dhcp);
                        if(pcposConnInfo.lanPcPos.dhcp == FALSE)
                        {
                            memcpy(pcposConnInfo.lanPcPos.localIpAddress, "\x00\x00\x00\x00", 4);
                            pcposConnInfo.lanPcPos.localIpAddress[4] = 0;
                            memcpy(pcposConnInfo.lanPcPos.gateway, "\x00\x00\x00\x00", 4);
                            pcposConnInfo.lanPcPos.gateway[4] = 0;
                            memcpy(pcposConnInfo.lanPcPos.networkMask, "\x00\x00\x00\x00", 4);
                            pcposConnInfo.lanPcPos.networkMask[4] = 0;
                        }
                    }
                    break;

                case 3:
                    editPort(&pcposConnInfo.pcposPort, "Port:", 1, 99999, FALSE, /*TRUE,*/ FALSE);
                                
                    if(pcposConnInfo.lanPcPos.tcpPort == pcposConnInfo.pcposPort)//+HNO_970914
                    {
                        displayMessageBox("����� ���� �������", MSG_ERROR);
                        pcposConnInfo.pcposPort = 0;
                    }
                    break;
                    
                case 4:
                    if(posConnectionType != CONNECTION_LAN)// && (pcposConnInfo.lanPcPos.dhcp == FALSE))
                    {
                        memset(itemVal, 0, sizeof(itemVal));
                        
                        editIp(itemVal, "Local IP:");
                        ipToString(itemVal, pcposConnInfo.lanPcPos.localIpAddress);
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "localIpAddress: %s", pcposConnInfo.lanPcPos.localIpAddress);
                    }
                    break;

                case 5:
                    if(posConnectionType != CONNECTION_LAN)//&&(pcposConnInfo.lanPcPos.dhcp == FALSE))
                    {
                        memset(itemVal, 0, sizeof(itemVal));
                        
                        editIp(itemVal, "Gateway IP:");
                        ipToString(itemVal, pcposConnInfo.lanPcPos.gateway);
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "gateway: %s", pcposConnInfo.lanPcPos.gateway);
                    }
                    break;

                case 6:
                    if(posConnectionType != CONNECTION_LAN) //&& (pcposConnInfo.lanPcPos.dhcp == FALSE))
                    {
                        memset(itemVal, 0, sizeof(itemVal));
                        
                        editIp(itemVal, "Network Mask:");
                        ipToString(itemVal, pcposConnInfo.lanPcPos.networkMask);
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "networkMask: %s", pcposConnInfo.lanPcPos.networkMask);
                    }
                    break; 

                default:
                    break;
            }      
            break;

            default:
                break;
        }
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos conn type: %s", &pcposConnInfo.connTypePcPos);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos localIpAddress: %s", &pcposConnInfo.lanPcPos.localIpAddress);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos gateway: %s", &pcposConnInfo.lanPcPos.gateway);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos mask: %s", pcposConnInfo.lanPcPos.networkMask);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pcpos port: %d", pcposConnInfo.pcposPort);
    
    retValue = updateFileInfo(PCPOS_INFO_FILE, &pcposConnInfo, sizeof(PcPosConnInfoST));  
    if (retValue != SUCCESS) 
        displayMessageBox("������� ����� ���", MSG_WARNING);

    clearDisplay();
}      





