#include <string.h> 
#include "N_common.h"
#include "N_dateTime.h" 
#include "N_merchantSpec.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h"
#include "N_terminalReport.h" 
#include "N_connection.h" 
#include "N_scheduling.h" 
#include "N_cardSpec.h" 
#include "N_messaging.h" 
#include "N_error.h"
#include "N_binaryTree.h" 
#include "N_getUserInput.h" 
#include "N_menu.h" 
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h" 
#include "N_display.h" 
#include "N_initialize.h" 
#include "N_log.h" 


static settingSpecST     userSettingInfo; //MRF_NEW15
/**
 * This function check whether program is initialize or not?
 * @param   files [input] : file which have initialize
 * @see     fileExist().
 * @return  TRUE or FALSE.
 */
uint8 isInitialized(filesST* files) 
{
	//MRF_IDENT
#ifdef INGENICO5100
	if (sramFileExist(files->masterKeyFile) == SUCCESS)
#else
    if (fileExist(files->masterKeyFile) == SUCCESS)
#endif
        return TRUE;
    else
        return FALSE;
}


/**
 * Read merchant information from file.
 * @param   files [input]: where merchant information has been saved
 * @param   merchantSpec [output]: merchant specification
 * @see     readFileInfo().
 * @see     writeMerchantSpec().
 * @return  TRUE or FALSE.
 */
uint8 readMerchantSpec(filesST* files, merchantSpecST* merchantSpec)  
{
    int16 retValue      = FAILURE;						/* file update return value */
    uint32 lenght       = sizeof(merchantSpecST);       /* size of merchant information */

    retValue = readFileInfo(files->merchantSpecFile, merchantSpec, &lenght);
    if (retValue != SUCCESS)
    {
        memset(merchantSpec->terminalID, 0, 9);
        memset(merchantSpec->merchantID, 0, 16);
        merchantSpec->STAN = 0;
        merchantSpec->batchNumber = 1;
//        memset(merchantSpec->headerLine1, 0, 24);//HNO_DEBUG3
//        memset(merchantSpec->headerLine2, 0, 24);//HNO_DEBUG3
        memset(merchantSpec->footerLine, 0, 31);
        strcpy(merchantSpec->helpPhone, "43570");
        //--MRF_ NEW memset(merchantSpec->merchantInfo, 0, 24);
        memset(merchantSpec->marketName, 0, sizeof(merchantSpec->marketName));
        memset(merchantSpec->marketAddress, 0, sizeof(merchantSpec->marketAddress));
        memset(merchantSpec->postalCodeMarket, 0, sizeof(merchantSpec->postalCodeMarket));
        memset(merchantSpec->merchantPhone, 0, sizeof(merchantSpec->merchantPhone));
        memset(merchantSpec->depositID, 0, sizeof(merchantSpec->depositID));//MRF_NEW17
        memset(merchantSpec->discount, 0, sizeof(merchantSpec->discount));//MRF_NEW17

        writeMerchantSpec(files, *merchantSpec);        

        return FALSE;
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "marketName:%s",merchantSpec->marketName);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "marketAddress:%s",merchantSpec->marketAddress);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "postalCodeMarket:%s",merchantSpec->postalCodeMarket);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "merchantPhone:%s",merchantSpec->merchantPhone);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "READ merchant DONE");//HNO_IDENT
    
    return TRUE;
}

/**
 * set primary information in file.
 * @param   files [input]: where merchant information has been saved
 * @see     fileExist().
 * @see     writeMerchantSpec().
 * @return  writeMerchantSpec()(success or not success).
 */
uint8 initMerchantSpec(filesST* files)
{
	if (fileExist(files->merchantSpecFile) != SUCCESS)
	{
		merchantSpecST merchantSpec;
        
        memset(merchantSpec.terminalID, 0, 9); 
	    memset(merchantSpec.merchantID, 0, 16);
        
	    merchantSpec.STAN = 0;
	    merchantSpec.batchNumber = 1;
//	    memset(merchantSpec.headerLine1, 0, 24);//HNO_DEBUG
//	    memset(merchantSpec.headerLine2, 0, 24);//HNO_DEBUG
	    memset(merchantSpec.footerLine, 0, 31);
	    strcpy(merchantSpec.helpPhone, "43570");
        memset(merchantSpec.marketName, 0, sizeof(merchantSpec.marketName));
        memset(merchantSpec.marketAddress, 0, sizeof(merchantSpec.marketAddress));
        memset(merchantSpec.postalCodeMarket, 0, sizeof(merchantSpec.postalCodeMarket));
        memset(merchantSpec.merchantPhone, 0, sizeof(merchantSpec.merchantPhone));
        memset(merchantSpec.depositID, 0, sizeof(merchantSpec.depositID));//MRF_NEW17
        strcpy(merchantSpec.discount,"1");//MRF_NEW17
	
	    return writeMerchantSpec(files, merchantSpec);        
	}
}

/**
 * Write merchant information in file.
 * @param   files [input]: files where merchant information has been write
 * @param   merchantSpec [output]: merchant specification
 * @see     showLog().
 * @see     updateFileInfo().
 * @see     addSystemErrorReport().
 * @see     displayMessageBox().
 * @return  TRUE or FALSE.
 */
uint8 writeMerchantSpec(filesST* files, merchantSpecST merchantSpec)  
{
    int16 retValue = FAILURE;          /** file update return value */

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: merchantSpecFile");
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "marketName:%s",merchantSpec.marketName);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "marketAddress:%s",merchantSpec.marketAddress);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "postalCodeMarket:%s",merchantSpec.postalCodeMarket);//HNO_IDENT
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "merchantPhone:%s",merchantSpec.merchantPhone);//HNO_IDENT
    
    retValue = updateFileInfo(files->merchantSpecFile, &merchantSpec, sizeof(merchantSpecST));  
    if (retValue != SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #11");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox("������� ������� ����� ���!", MSG_ERROR);
        return FALSE;
    }

    return TRUE;
}

/**
 * Change merchant phone number.
 * @param   args : argument that give new phone number
 * @see     showLog().
 * @see     readMerchantSpec().
 * @see     editNumericStringValue().
 * @see     writeMerchantSpec().
 * @return  TRUE or FALSE.
 */
uint8 changeMerchantPhone(argumentListST* args)
{    
    serverSpecST*       serverSpec          = (serverSpecST*) (args->argumentName);
    serverSpecST*       servers             = NULL;
    uint8               merchantPhone[17]   = {0};    
    merchantSpecST      merchantSpec;
    merchantSpecST      merchantSpecOld;

    showLog(JUST_VERIFONE, __FILE__, __LINE__, DEBUG, "changeMerchantPhone", "********START***********");
    
    readMerchantSpec(&(serverSpec->files), &(merchantSpec)); 
    
    strcpy(merchantPhone, merchantSpec.merchantPhone);
    
    editNumericStringValue(merchantPhone, 16,0, "���� ���Ԑ��", TRUE, FALSE);

    servers = serverSpec;
    while (servers != NULL)
    {
        memset(&merchantSpec, 0, sizeof(merchantSpecST));
        memset(&merchantSpecOld, 0, sizeof(merchantSpecST));
    
        readMerchantSpec(&(servers->files), &(merchantSpec)); 

        merchantSpecOld = merchantSpec;

        strcpy(merchantSpec.merchantPhone, merchantPhone);

        if (memcmp(&merchantSpec, &merchantSpecOld, sizeof(merchantSpecST)) != 0)
        {
            writeMerchantSpec(&(servers->files), merchantSpec);
        }
        
        servers = servers->next;
    }     
    showLog(JUST_VERIFONE, __FILE__, __LINE__, DEBUG, "changeMerchantPhone", "********FINISH***********");

    return TRUE;
}

//MRF_NEW16
settingSpecST getUserSetting(void)
{
    return userSettingInfo;
}

void setUserSetting(settingSpecST setting)
{
   userSettingInfo = setting; 
}

