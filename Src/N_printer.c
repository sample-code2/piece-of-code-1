//MRF #include <stdio.h>
#include <string.h>
#include "N_common.h" 
#include "N_utility.h" 
#include "N_dateTime.h"
#include "N_binaryTree.h" 
#include "N_menu.h" 
#include "N_printerWrapper.h"
#include "N_printer.h"
#include "N_cardSpec.h" 
#include "N_error.h"
#include "N_terminalReport.h"
#include "N_scheduling.h" 
#include "N_terminalSpec.h" 
#include "N_connection.h" 
#include "N_merchantSpec.h" 
#include "N_messaging.h" 
#include "N_getUserInput.h" 
#include "N_POSSpecific.h" 
#include "N_display.h"
#include "N_initialize.h"
#include "N_displayWrapper.h"
//#include "hasin_fontlib.h" 

/**
 * Check printer access.
 * @see     printerStatus().
 * @see     getSystemErrorMessage().
 * @see     displayMessageBox().  
 * @return  TRUE or FALSE.
 */
uint8 PrinterAccess(uint8 showMsg) 
{
    uint8  errorMsg[20]     = {0};              
    int16  printerCheck     = FAILURE;
    
    printerCheck = printerStatus();
    if (printerCheck != SUCCESS)
    {
        if (showMsg)
        {
            getSystemErrorMessage(errorMsg, printerCheck, ERR_TYPE_PRINTER);
            displayMessageBox(errorMsg, MSG_ERROR);
        }
        
        printerCheck = printerStatus();
        if (printerCheck != SUCCESS)
        {
        	//HNO_ADD for managing printer in ingenico(printer should close)
    		disablePrinter();
    		initPrinter();
            return FALSE;
        }
    }
    return TRUE;
}

/**
 * Print Star.
 * @see     getPrinterCharacterCount().
 * @see     printOneStringFarsi().
 * @return  True or False.
 */
uint8 printStar(void) 
{
    uint8 star[50]  = {0};
    uint8 i         = 0;
    
#if  defined(IWL220) || defined(ICT250)
    pprintf("************************\n");
#else
    for (i = 0; i < getPrinterCharacterCount(); i++)
        star[i] = '*';
    if (!printOneStringFarsi(star, PRN_NORM ,ALIGN_LEFT))
        return FALSE;
#endif
    return TRUE;
}

/**
 * Print dash.
 * @see     getPrinterCharacterCount().
 * @see     printOneStringFarsi().
 * @see     printBlankLines().
 * @return  True or False.
 */
uint8 printDash(void) 
{
    uint8 dash[50]  = {0};
    uint8 i         = 0;
    
#if defined(IWL220) || defined(ICT250)
    pprintf("------------------------\n");

#else
    for (i = 0; i < getPrinterCharacterCount(); i++)
        dash[i] = '_';
    if (!printOneStringFarsi(dash, PRN_NORM ,ALIGN_LEFT))
        return FALSE;
#endif
    
    return TRUE;
}



/**
 * Print scrollable lines
 * @param   title [input]: title of printer text
 * @param   lines [input]: number of printer text lines
 * @param   lineCount [input]: count number of lines
 * @see     selectNormalPrinterFontEnglish().
 * @see     printDash().
 * @see     printOneStringEnglish().
 * @see     cancelFunction().
 * @see     printBlankLines().
 * @see     selectNormalPrinterFontFarsi().
 * @return  True or False.
 */
uint8 printScrolableLines(uint8* title, uint8 lines[][32], uint8 lineCount)
{
    uint8 i = 0;
    
    selectNormalPrinterFontEnglish();

    if (!printOneStringEnglish(&title[3], title[0], title[1]))
        return FALSE;
    
    for (i = 0; i < lineCount; i++)
    {
        if (!printOneStringEnglish(&lines[i][3], lines[i][0], lines[i][1]))
            return FALSE;

        if (cancelFunction())
            return FALSE;
    }     

//    printBlankLines(BLANK_LINES_COUNT);//#HNO_980617
    selectNormalPrinterFontFarsi();
    
    return TRUE;
}

/**
 * Print receipt date.
 * @param   date [output]: date
 * @param   message [output]: message
 * @see     gregorianToJalali().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     printTwoStringNumericFarsi().
 * @see     printOneStringFarsi().
 * @return  TRUE or FALSE.
 */
uint8 printReceiptDate(uint32 date, uint8* message)   
{
    uint8  messageStr[50]       = {0};              /* message string */
    uint32 jalaliDate           = 0;                /* date in jalali format */
    int    year                 = 0;                /* year */
    int    month                = 0;                /* month */
    int    day                  = 0;                /* day */
    uint8  dateStr[10]          = {0};              /* date string */
    
    strcpy(messageStr, message);

    gregorianToJalali(&jalaliDate, date);
    year = getYear(jalaliDate);
    month = getMonth(jalaliDate);
    day = getDay(jalaliDate);

#ifdef CASTLES_V5S //MRF_TTF
    sprintf(dateStr, "%2d/%02d/%04d",  day, month, year);
#else
    sprintf(dateStr, "%4d/%02d/%02d", year, month, day);
#endif
    
    if (strlen(messageStr) > 0) 
    {
        if (!printTwoStringNumericFarsi(dateStr, message, PRN_NORM))
            return FALSE;
    }
    else 
    {      
        if (!printOneStringFarsi(dateStr, PRN_NORM, ALIGN_RIGHT))
            return FALSE;
    }

    return TRUE;
}

/**
 * Print receipt date & time.
 * @param   dateTime [output]: date and time
 * @param   dayName [output]: print day name
 * @see     getHour().
 * @see     getMinute().
 * @see     gregorianToJalali().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     getDayNumInYear().
 * @see     getPrinterCharacterCount().
 * @see     printOneStringFarsi().
 * @return  TRUE or FALSE.
 */ 
uint8 printReceiptDateTime(dateTimeST* dateTime, uint8 isNormal)  
{
    uint32  jalaliDate          = 0;                    /* date in jalali format */
    int     year                = 0;                    /* year */
    int     month               = 0;                    /* month */
    int     day                 = 0;                    /* day */
    int     hour                = 0;                    /* hour */
    int     minute              = 0;                    /* minute */
    uint8   time[9]             = {0};                  /* time string */
    uint8   date[11]            = {0};                  /* date string */
    uint8   dateStr[20*6]        = {0};
    int     second              = 0; 

    hour = getHour(dateTime->time);
    minute = getMinute(dateTime->time);
    second = getSecond(dateTime->time);
    
    if (isNormal)
        sprintf(time, "%02d:%02d:%02d", hour, minute, second);
    else
        sprintf(time, "%02d:%02d", hour, minute);
    
    gregorianToJalali(&jalaliDate, dateTime->date);
    year = getYear(jalaliDate);
    month = getMonth(jalaliDate);
    day = getDay(jalaliDate);
    
    if (isNormal != 2)
    {
        sprintf(date, "%04d/%02d/%02d", year, month, day);
        strcpy(dateStr, date);
#ifdef INGENICO	//+HNO_970903
        if (!printTwoStringFarsi(time, dateStr, PRN_NORM))
             return FALSE;
#else
        if (!printTwoStringFarsi(dateStr, time, PRN_NORM))
             return FALSE;
#endif
    }
    else
    {      
#ifdef VX520
		sprintf(date, "%04d/%02d/%02d", year, month, day);
		strcpy(dateStr, date);
		if (!printTwoStringNumericFarsi(dateStr, "����� �����", PRN_NORM))
			return FALSE;
#else
		sprintf(date, "%02d/%02d/%04d", day, month, year);
		strcpy(dateStr, date);
        if (!printTwoStringFarsi(dateStr, "����� �����:", PRN_NORM))
            return FALSE;
#endif
    }
    
    return TRUE;
}

/**
 * Print one string farsi.
 * @param  string [input]: input string
 * @param  size [input]: size of string
 * @param  align [input]: align of string
 * @see    selectNormalPrinterFontFarsi().
 * @see    selectBigPrinterFontFarsi().
 * @see    selectSmallPrinterFontFarsi().
 * @see    convertCp1256ToIransystem().
 * @see    invertString().
 * @see    wordWrapFarsi().
 * @see    getPrinterCharacterCount().
 * @see    alignedCenterString().
 * @see    printString().
 * @see    alignedRightString().
 * @see    getSystemErrorMessage().
 * @see    displayMessageBox().
 * @return True or False.
 */
#ifdef CASTLES_V5S //#MRF_971019
uint8 printOneStringFarsi(uint8* string, int size, int align) 
{
    uint8   cpyStr[250]             = {0};
    uint8   separatedStr[10][250]   = {0, 0};
    int     i                       = 0;
    int     loop                    = 0;
    int16   retValue                = FAILURE;
    uint8   errorMsg[220]           = {0};
    int     length                  = 0 ;

	setprinterSize(size,FARSI);//..ABS_971130
	sprintf(cpyStr,"%s",string);
    length = strlen(cpyStr);

    if (size != PRN_BIG)
        loop = wordWrapFarsi(cpyStr, separatedStr, getPrinterCharacterCount() + getAddedCharCount(), length);
    else
        loop = wordWrapEnglish(cpyStr, separatedStr, getPrinterCharacterCount() + getAddedCharCount());
         
    for (i = 0; i <= loop; i++)
    {
        if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
        {
            convertCp1256ToTTFFont(separatedStr[loop - i], FALSE);
            invertString(separatedStr[loop - i], cpyStr);
            retValue = printAlignString(cpyStr, (uint8*)align);
        }
        else
        {
            if (size != PRN_BIG)
            {
                convertCp1256ToTTFFont(separatedStr[loop - i], FALSE);
                retValue = printAlignString(separatedStr[loop - i], (uint8*)align);
            }
            else if (size == PRN_BIG)
            {
                convertCp1256ToTTFFont(separatedStr[i], FALSE);
                retValue = printAlignString(separatedStr[i], (uint8*)align);
            }
        }
    }
    
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  

    if (retValue == SUCCESS)
        return TRUE;
    else
    {
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
        displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }
}
#else
uint8 printOneStringFarsi(uint8* string, int size, int align) 
{
    uint8   cpyStr[250]             = {0};             /* copy of input string */
    uint8   cpyStr2[250]            = {0};
    uint8   alignedStr[250]         = {0};
    uint8   alignedStr2[250]        = {0};
    uint8   separatedStr[10][250]   = {0, 0};
    int     i                       = 0;
    int     loop                    = 0;
    int16   retValue                = FAILURE;
    uint8   errorMsg[220]           = {0};            // printer error message 
    int     length                  = 0 ;
    uint16	spaceCount				= 0;
	uint8	buffer[2]               = { '\n', 0 };//ABS:ADD:960806

#if defined(ICT250) || defined(IWL220)//HNO_ADD
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "printOneStringFarsi");
    memset(cpyStr, 0, sizeof(cpyStr));
    
	setprinterSize(size,FARSI);//..ABS_971130

    if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
        invertString(string, cpyStr);
    else
        strcpy(cpyStr, string);

    memset(separatedStr, 0, sizeof(separatedStr));
//    loop = wordWrapFarsi(cpyStr, separatedStr, getPrinterCharacterCount() + getAddedCharCount()); //just for alignment char in ingenico
    loop = 0;
    strncpy(separatedStr[0], cpyStr, 50);

    for (i = 0; i <= loop; i++)
    {
        memset(alignedStr, 0, sizeof(alignedStr));
        memset(cpyStr, 0, sizeof(cpyStr));

        setBigStr(size, separatedStr[i], cpyStr); //just for bitel, fot other POS do: strcpy(cpyStr, separatedStr[i]);

        strcpy(alignedStr, cpyStr);
		convert2PrinterFontFormat(alignedStr);
		memcpy(cpyStr, alignedStr, 100);
        if (align == ALIGN_CENTER)
        {
        	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "align center");
            spaceCount = alignCenterString(strlen(separatedStr[i]), size, alignedStr);
            retValue = printString(alignedStr, spaceCount);
            printLineFeed(1);
        }
        else if (align == ALIGN_RIGHT)
        {
        	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "align right");
            retValue = printString(alignedStr, 0);
            printLineFeed(1);
        }
        else
        {
        	spaceCount = alignLeftString(strlen(separatedStr[i]), size, alignedStr);
            retValue = printString(alignedStr, spaceCount);
            printLineFeed(1);
        }
    }
#else
	setprinterSize(size,FARSI);//..ABS_971130
    sprintf(cpyStr,"%s",string);
    length = strlen(cpyStr);
    convertCp1256ToIransystem(cpyStr);
#ifdef VX520
	IransystemToPft(cpyStr);
#endif
    loop = wordWrapFarsi(cpyStr, separatedStr, getPrinterCharacterCount() + getAddedCharCount()/*INGENICO_ADDED_CHAR MRF_IDENT*/, length); //just for alignment char in ingenico   
    for (i = 0; i <= loop; i++)
    {
        memset(alignedStr, 0, sizeof(alignedStr));
        memset(alignedStr2, 0, sizeof(alignedStr2));
        
        if (align == ALIGN_CENTER)  
            alignedCenterString(separatedStr[i], size, alignedStr);        
        else if (align == ALIGN_RIGHT)            
            alignedRightString(separatedStr[i], size, alignedStr);
        else
            alignedLeftString(separatedStr[i], size, alignedStr);
        
        
        if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
            invertString(alignedStr, alignedStr2);
        else
            strcpy(alignedStr2, alignedStr);

        retValue = printString(alignedStr2);
#ifdef VX520
		if (retValue == SUCCESS)
		printString(buffer);		//ABS:ADD:960806  typing enter key
#endif
	} //ABS:ADD:960806
    
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  
#endif
    if (retValue == SUCCESS)
        return TRUE;
    else
    {
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
        displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }
}
#endif


/**
 * Print one string English.
 * @param  string [input]: the input string
 * @param  size [input]: size of string
 * @param  align [input]: align of string
 * @see    selectNormalPrinterFontEnglish().
 * @see    selectBigPrinterFontEnglish().
 * @see    selectSmallPrinterFontEnglish().
 * @see    invertString().
 * @see    wordWrapEnglish().
 * @see    alignedCenterString().
 * @see    printString().
 * @see    alignedRightString().
 * @see    getSystemErrorMessage().
 * @see    displayMessageBox().
 * @see    getPrinterCharacterCount().
 * @return True or False.
 */
uint8 printOneStringEnglish(uint8* string, int size, int align)  
{
#if defined(ICT250) || defined(IWL220)
    uint8   cpyStr[250]             = {0};             /* copy of input string */
    uint8   alignedStr[250]         = {0};
	uint8   separatedStr[7][50]     = {{0}, {0}};
    int     i                       = 0;
    int     loop                    = 0;
    int16   retValue                = FAILURE;
    uint8  	errorMsg[20]     		= {0};             /** printer error message */
    uint16	spaceCount				= 0;

    if (size == PRN_NORM || size == PRN_NORM_INVERSE)
        selectNormalPrinterFontEnglish();
    else if (size == PRN_BIG || size == PRN_BIG_INVERSE)
        selectBigPrinterFontEnglish();
    else if (size == PRN_SMALL || size == PRN_SMALL_INVERSE)
        selectSmallPrinterFontEnglish();

    if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
        invertString(string, cpyStr);
    else
        strcpy(cpyStr, string);

//    loop = wordWrapEnglish(cpyStr, separatedStr, getPrinterCharacterCount());
    memset(separatedStr, 0, sizeof(separatedStr));
    loop = 0;
    strncpy(separatedStr[0], cpyStr, 50);

    for (i = 0; i <= loop; i++)
    {
        memset(alignedStr, 0, sizeof(alignedStr));
        memset(cpyStr, 0, sizeof(cpyStr));

        setBigStr(size, separatedStr[i], cpyStr); //just for bitel, fot other POS do: strcpy(cpyStr, separatedStr[i]);
        strcpy(alignedStr, cpyStr);

        if (align == ALIGN_CENTER)
        {
            strcpy(alignedStr, cpyStr);
            spaceCount = alignCenterStringEnglish(strlen(separatedStr[i]), size, alignedStr);
//            strcat(alignedStr, cpyStr);
            retValue = printStringEnglish(alignedStr, spaceCount);
            printLineFeed(1);
        }
        else if (align == ALIGN_RIGHT)
        {
            spaceCount = alignRightStringEnglish(strlen(separatedStr[i]), size, alignedStr);
//            strcat(alignedStr, cpyStr);
            retValue = printStringEnglish(alignedStr, 0);
            printLineFeed(1);
        }
        else
        {
            strcpy(alignedStr, cpyStr);
//            spaceCount = alignLeftString(strlen(separatedStr[i]), size, alignedStr);
//            strcat(alignedStr, cpyStr);
            retValue = printStringEnglish(alignedStr, spaceCount);
            printLineFeed(1);
        }
    }

    if (size != PRN_NORM)  //#MRF_971019
        selectNormalPrinterFontEnglish();

    if (retValue != SUCCESS)
    {
        //FKH	121106 : for add system report
        addSystemErrorReport(retValue, ERR_TYPE_PRINTER);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
        displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }

    return TRUE;
#else
    uint8   cpyStr[250]             = {0};             /* copy of input string */
    uint8   alignedStr[250]         = {0};
    uint8   separatedStr[10][250]   = {0, 0}; 
    int     i                       = 0; 
    int     loop                    = 0; 
    int16   retValue                = FAILURE;
    uint8   errorMsg[20]     	    = {0};            // printer error message
    uint8	buffer[2]               = { '\n', 0 };  //ABS:ADD:960806 
	
	
	setprinterSize(size, ENGLISH);//..ABS_971130

    loop = wordWrapEnglish(string, separatedStr, getPrinterCharacterCount() - 1); //ABS:ADD:960807
    
    
    for (i = 0; i <= loop; i++)
    {
        memset(alignedStr, 0, sizeof(alignedStr));
        
        if (align == ALIGN_CENTER)
        {
            alignedCenterStringEnglish(separatedStr[i], size, alignedStr);

        }
        else if (align == ALIGN_RIGHT)
        {
            alignedRightStringEnglish(separatedStr[i], size, alignedStr);
        }
        else if (align == ALIGN_LEFT)
        {
            alignedLeftStringEnglish(separatedStr[i], size, alignedStr);
        }
    }
    
    if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
       invertString(alignedStr, cpyStr);
    else
        strcpy(cpyStr, alignedStr);
    
    retValue = printString(cpyStr);
#ifdef VX520
		printString(buffer);		//ABS:ADD:960806  typing enter key
#endif
            
    if (size != PRN_NORM) //MRF_971019
    	selectNormalPrinterFontEnglish(); //set default font
    
    if (retValue == SUCCESS)
        return TRUE;
    else
    {
    	getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
    	displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }
#endif
}

/**
 * Print two string numeric and farsi.
 * @param  firstStr [input]: the first string
 * @param  secondStr [input]: the second string
 * @param  size [input]: size of string
 * @see    selectNormalPrinterFontFarsi().
 * @see    selectBigPrinterFontFarsi().
 * @see    selectSmallPrinterFontFarsi().
 * @see    alignedRightString().
 * @see    bindTwoStringFarsi().
 * @see    printOneStringFarsi() .
 * @return True or False.
 */
uint8 printTwoStringNumericFarsi(uint8* firstStr, uint8* secondStr, int size) 
{

#if defined(ICT250) || defined(IWL220)//HNO_CHANGE to be similar as castles
    uint8   outputStr[100]          = {0};
    int16   retValue                = FAILURE;
    uint8   errorMsg[100]           = {0};
    uint8   string[10]           = {0};
    uint8   cpyStr[10]           = {0};

	setprinterSize(size,FARSI);//..ABS_971130

    if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
        invertString(string, cpyStr);

    retValue = bindTwoStringFarsi(firstStr, secondStr, outputStr, FALSE);
    if (retValue != TRUE)
    {
        //HNO_ADD
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
        displayMessageBox(errorMsg, MSG_ERROR);
            return FALSE;
    }

    setAddedCharCount(0); //set default value
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  

    return TRUE;
#else

    uint8 outputStr[500]        = {0};

	setprinterSize(size, FARSI);//..ABS_971130

    attachNumericString(firstStr, secondStr, size, outputStr);
    
#if defined(CASTLES_V5S) || defined(VX520)
    if (!printOneStringFarsi(outputStr, size, ALIGN_LEFT)) //MRF_TTF
        return FALSE;
#else
    if (!printOneStringFarsi(outputStr, size, ALIGN_RIGHT)) //MRF_TTF 
        return FALSE;
#endif

    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  
    
    return TRUE;
#endif
}

/**
 * Print two string farsi.
 * @param  firstStr [input]; the first string
 * @param  secondStr [input]: the second string
 * @param  size [input]: size of string
 * @see    selectNormalPrinterFontFarsi().
 * @see    selectBigPrinterFontFarsi().
 * @see    selectSmallPrinterFontFarsi().
 * @see    alignedRightString().
 * @see    bindTwoStringFarsi().
 * @see    printOneStringFarsi() .
 * @return True or False.
 */
uint8 printTwoStringFarsi(uint8* firstStr, uint8* secondStr, int size) 
{
#if defined(ICT250) || defined(IWL220)//HNO_ADD
    uint8   outputStr[100]          = {0};
    int16   retValue                = FAILURE;
    uint8   errorMsg[220]           = {0};
    uint8   string[10]           = {0};
    uint8   cpyStr[10]           = {0};

	setprinterSize(size,FARSI);//..ABS_971130

    if (size == PRN_SMALL_INVERSE || size == PRN_NORM_INVERSE || size == PRN_BIG_INVERSE)
        invertString(string, cpyStr);
    setAddedCharCount(1); //1 char added in Ingenico for align character
    retValue = bindTwoStringFarsi(firstStr, secondStr, outputStr, TRUE);//HNO_CHANGE remove :

    if (retValue != TRUE)
    {
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
        displayMessageBox(errorMsg, MSG_ERROR);
            return FALSE;
    }

    setAddedCharCount(0); //set default value
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  

#else
    uint8 outputStr[250]     = {0};
    attachTwoString(firstStr, secondStr, size, outputStr, FALSE);
    setAddedCharCount(1); //++MRF_IDENT: 1 char added in Ingenico for align character

#ifndef CASTLES_V5S    
    if (!printOneStringFarsi(outputStr, size, ALIGN_LEFT))
        return FALSE;
#else
    if (!printOneStringFarsi(outputStr, size, ALIGN_RIGHT))
        return FALSE;
#endif
    setAddedCharCount(0); //++MRF_IDENT: set default value
#endif
    return TRUE;
}

/**
 * Print Two String.
 * @param  firstStr [input]: first String
 * @param  secondStr [input]: Second String
 * @param  size [input]; size of string
 * @see    selectNormalPrinterFontFarsi().
 * @see    selectBigPrinterFontFarsi().
 * @see    selectSmallPrinterFontFarsi().
 * @see    convertCp1256ToIransystem().
 * @see    alignedRightString().
 * @see    printString().
 * @see    getSystemErrorMessage().
 * @see    displayMessageBox().
 * @return Ture or False.
 */
uint8 printTwoString(uint8* firstStr, uint8* secondStr, int size, uint8 en) //MRF_TTF
{
#if defined(ICT250) || defined(IWL220)
    return printTwoStringFarsi(secondStr, firstStr, size);
#else
    uint8   cpyStr1[500]     = {0};          /* copy of firstStr */
    uint8   cpyStr2[500]     = {0};          /* copy of secondStr */
    uint8   alignedStr[250]  = {0};
    uint8   outputStr[500]   = {0};
    int16   retValue	     = FAILURE;
    uint8   errorMsg[220]    = {0};	    // printer error message 
        
	setprinterSize(size, FARSI);//..ABS_971130

    strncpy(cpyStr1, firstStr, (getPrinterCharacterCount() - strlen(secondStr)));
    strcpy(cpyStr2, secondStr);

#ifdef CASTLES_V5S //MRF_TTF
    convertCp1256ToTTFFont(cpyStr1, FALSE);
    attachTwoString(cpyStr1, cpyStr2, size, outputStr, en);
    retValue = printAlignString(outputStr, ALIGN_RIGHT);
#elif defined VX520  //ABS:ADD:960806
    attachNumericString(firstStr, secondStr, size, outputStr);
    if (!printOneString_FAEN(outputStr, size, ALIGN_LEFT))
        retValue = FAILURE;
    else
        retValue = SUCCESS;
#else
    convertCp1256ToIransystem(cpyStr2);
    retValue = printString(outputStr, PRN_NORM);//HNO_CHANGE in ict we have 2 significant input
#endif
    
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  
    
    if (retValue == SUCCESS)
        return TRUE;
    else
    {
    	getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_PRINTER);
    	displayMessageBox(errorMsg, MSG_ERROR);
        return FALSE;
    }
#endif
}

/**
 * Print three string farsi.
 * @param  firstStr [input]: first String
 * @param  secondStr [input]: second String
 * @param  thirdStr [input]: Third String
 * @param  size [input]: size of string
 * @param  align [input]: align of string
 * @see    selectNormalPrinterFontFarsi().
 * @see    selectBigPrinterFontFarsi().
 * @see    selectSmallPrinterFontFarsi().
 * @see    getPrinterCharacterCount().
 * @see    printOneStringFarsi().
 * @return True or False.
 */
uint8 printThreeStringFarsi(uint8* firstStr, uint8* secondStr, uint8* thirdStr, int size, int align , uint8 colon) 
{
    int   spaceCount         = 0;            /* escape string length */
    uint8 cpyStr1[250]       = {0};          /* copy of firstStr */
    uint8 cpyStr2[250]       = {0};          /* copy of secondStr */
    uint8 cpyStr3[250]       = {0};          /* copy of thirdStr */
    uint8 spaceStr[250]      = {0};   
    uint8 strWithSpace[250]  = {0};
    uint8 outputStr[250]     = {0};
        
	setprinterSize(size, FARSI);//..ABS_971130

    strcpy(cpyStr1, firstStr);
    strcpy(cpyStr2, secondStr);
    
#ifdef INGENICO
    strcat(cpyStr2,cpyStr1);
#endif
    strcpy(cpyStr3, thirdStr);
    
    if (align == ALIGN_CENTER)
        strcpy(strWithSpace, " ");
    else
    {
        memset(spaceStr, 0x20, sizeof(spaceStr));
        spaceCount = getPrinterCharacterCount() - strlen(cpyStr1) - strlen(cpyStr2) - strlen(cpyStr3);
        if (spaceCount < 2)
            spaceCount = 2;
        if (colon)
            strncat(strWithSpace, spaceStr, spaceCount - 2); // 2 for " " & ":"
        else
            strncat(strWithSpace, spaceStr, spaceCount);
    }
  
    if (colon)
    {
#ifdef CASTLES_V5S //MRF_TTF
      sprintf(outputStr, "%s%s:%s%s", thirdStr,strWithSpace, secondStr, firstStr);//..HNO_980604 because of long amount in receipt
//       sprintf(outputStr, "%s%s:%s %s", thirdStr,strWithSpace, secondStr, firstStr);
#else
      {
        #ifndef INGENICO
        sprintf(outputStr, "%s %s%s:%s", thirdStr, secondStr, strWithSpace, firstStr);
        #endif
      }
#endif
    }
    else
    {
        #ifndef INGENICO
                sprintf(outputStr, "%s%s%s%s", thirdStr, secondStr, strWithSpace, firstStr);// :/
        #endif
    }

#ifdef INGENICO
    if(!bindTwoStringFarsi(cpyStr2, cpyStr3, outputStr, FALSE))
    	return FALSE;
#else
    if (!printOneStringFarsi(outputStr, size, align))
        return FALSE;
#endif
    
    if (size != PRN_NORM)//#MRF_971019 
        selectNormalPrinterFontFarsi();//set default font  
    
    return TRUE;
}

/**
 * Print Logo.
 * @param  files [input] : logo file
 * @param  type [input] : type of logo
 * @see    PrinterAccess().
 * @see    printHamraheAvalLogo().
 * @see    printTaliaLogo().
 * @see    printIrancellLogo().
 * @see    printBankLogo().
 * @return True or False.
 */
uint16 printLogo(filesST* files, int type) 
{
    if (!PrinterAccess(TRUE))    
        return FALSE;

    switch (type)
    {
        case HamrahLogo:
            return printHamraheAvalLogo(files);
        case IrancellLogo:
            return printIrancellLogo(files);
        case ShaparakLogo:
            return printShaparakLogo(files);
        case BankLogo:
            return printBankLogo(files);
        default:
            return FALSE;
    }
}

uint8 printDash2(void) 
{
    uint8 dash[50]  = {0};
    uint8 i         = 0;
    
#if  defined(IWL220) || defined(ICT250)
    pprintf("------------------------\n");
#elif defined(VX520)
	for (i = 0; i < getPrinterCharacterCount(); i++)
		dash[i] = '_';
	if (!printOneStringFarsi(dash, PRN_NORM, ALIGN_LEFT))
		return FALSE;
#else
    for (i = 0; i < getPrinterCharacterCount(); i++)
        dash[i] = '-';

    dash[0] = '|';
    dash[getPrinterCharacterCount() - 1] = '|';
    
    if (!printOneStringFarsi(dash, PRN_NORM ,ALIGN_LEFT))
        return FALSE;
#endif
    return TRUE;
}



uint8 printDash3(void) 
{
    uint8 dash[50]  = {0};
    uint8 i         = 0;
    
#if  defined(IWL220) || defined(ICT250)
    pprintf("------------------------\n");
#elif defined(VX520)
    for (i = 0; i < getPrinterCharacterCount(); i++)
            dash[i] = '_';
    if (!printOneStringFarsi(dash, PRN_NORM, ALIGN_LEFT))
            return FALSE;
#else
    for (i = 0; i < getPrinterCharacterCount(); i++)
        dash[i] = '-';

    for (i = 1; i < getPrinterCharacterCount(); i++)
        dash[i] = '-';

    dash[0] = ' ';
    dash[getPrinterCharacterCount() - 1] = 0;
    
    if (!printOneStringFarsi(dash, PRN_NORM ,ALIGN_LEFT))
        return FALSE;
#endif
    return TRUE;
}

//+HNO_980512
uint8 printReceiptDateTimeWithString(dateTimeST* dateTime, uint8* string3)  
{
    uint32  jalaliDate          = 0;                    /* date in jalali format */
    int     year                = 0;                    /* year */
    int     month               = 0;                    /* month */
    int     day                 = 0;                    /* day */
    int     hour                = 0;                    /* hour */
    int     minute              = 0;                    /* minute */
    uint8   time[50]            = {0};                  /* time string */
    uint8   date[50]            = {0};                  /* date string */
    uint8   dateTimeStr[20*6]       = {0};
    int     second              = 0; 
    uint8   spaceStr[50]       = {0};
    int     spaceCount          = 0;

    hour = getHour(dateTime->time);
    minute = getMinute(dateTime->time);
    second = getSecond(dateTime->time);
    
    gregorianToJalali(&jalaliDate, dateTime->date);
    year = getYear(jalaliDate);
    month = getMonth(jalaliDate);
    day = getDay(jalaliDate);
    
#ifdef VX520
    sprintf(date, "%04d/%02d/%02d", year, month, day);
	setPrinterCharCount(PRINTER_CHARACTER_COUNT_SMALL);
#else
    sprintf(date, "%02d/%02d/%04d", day, month, year);

#endif
    memset(spaceStr, 0x20, sizeof(spaceStr));
	spaceCount = getPrinterCharacterCount() - strlen(string3) - 18;
    spaceCount = spaceCount / 2;

//    strncat(strWithSpace, spaceStr, 1);
    strncat(date, spaceStr,spaceCount);
    strcat(date, string3);

#ifdef VX520
    sprintf(time, "%02d:%02d:%02d", hour, minute, second);
	//setPrinterCharCount(PRINTER_CHARACTER_COUNT_SMALL);
#else
    sprintf(time, "%02d:%02d:%02d", second, minute, hour);
#endif
	
#ifdef VX520
	strcpy(dateTimeStr, time);
	if (!printTwoStringNumericFarsi(dateTimeStr, date, PRN_SMALL))
         return FALSE;
#else
	strcpy(dateTimeStr, date);
    if (!printTwoStringFarsi(time, dateTimeStr, PRN_NORM))
        return FALSE;
#endif

	return TRUE;
}


