    #include <string.h>
#include "N_common.h"
#include "N_utility.h" 
#include "N_error.h"
#include "N_dateTime.h"
#include "N_cardSpecWrapper.h"
#include "N_cardSpec.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_connection.h" 
#include "N_connectionWrapper.h"
//#include "N_connection.h"
#include "N_messaging.h"
#include "N_printerWrapper.h" 
#include "N_printer.h" 
#include "N_terminalReport.h" 
#include "N_transactionReport.h" 
#include "N_terminalSpec.h"
#include "N_getUserInput.h"
#include "N_POSSpecific.h"
#include "N_dateTimeWrapper.h"
#include "N_displayWrapper.h"
#include "N_display.h"
#include "N_initialize.h"
#include "N_transactions.h"
#include "N_TMS.h"
#include "N_log.h"
#include "N_passManager.h"
#include "N_userManagement.h" 


void checkUnsuccessTransaction(filesST* files);


/**
 * Terminal Initiation.
 * @param  serverSpec [input] : server specification
 * @see    showLog().
 * @see    getConsoleHandle().
 * @see    enableKeyboard().
 * @see    turnOnLCDBackLight().
 * @see    displayMessage().
 * @see    getClockHandle().
 * @see    saveTMSScheduleDateTimeFromServer().
 * @see    getScheduleDateTime().
 * @see    enableClock().
 * @see    startCommunication().
 * @see    setCardHandle().
 * @see    setSocketHandle().
 * @see    initDisplayPic().
 * @see    initQx1000().
 * @see    getContactLessState().
 * @see    displayMessageBox().
 * @see    initPP1000SE().
 * @see    getPrinterHandle().
 * @see    initPrinter().
 * @see    getSmartCardHandle().
 * @see    enableSmartCard().
 * @see    fileExist().
 * @see    initTerminalSpec().
 * @see    initTMSFiles().
 * @see    setPeriodicTMSSchedule().
 * @see    writeTMS().
 * @see    readFileInfo().
 * @see    addSystemErrorReport().
 * @see    getSystemErrorMessage().
 * @see    setGeneralConnInfo().
 * @see    initSchedules().
 * @see    writeLogon().
 * @see    checkUnsuccessTransaction().
 * @see    TMSUpdate().
 * @return TRUE or FALSE.
 */
uint8 terminalInitiation(serverSpecST* serverSpec)
{
    serverSpecST*       server              = NULL;
    dateTimeST          scheduleDateTime    = {0, 0};
    uint32              len                 = 0;
    uint8               errorMsg[50]        = {0};
    int16               retValue            = 0;
    int                 connCounter         = 0;
    uint8               connectionType      = 0;
    uint8               check               = 0;
    dateTimeST          dateTime;
    generalConnInfoST   generalConnInfo;
    serviceSpecST       POSService      = getPOSService();//+HNO_980902
    terminalSpecST      terminalCapability = getTerminalCapability();


    memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
    memset(&scheduleDateTime, 0, sizeof(dateTimeST));
    memset(&dateTime, 0, sizeof(dateTimeST));
   
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***Terminal Initiation***");
    
	if (terminalCapability.GPRSCapability)
    {
        setOpenPort(FALSE);
        readPIN(&(serverSpec->files));//+MRF_971009
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "getpinParam:%s",getPINParam());
        powerMode(SLEEP_MODE);
    }
  
    readUserSetting();
    initLogType();
    
    if (getConsoleHandle() <= 0)
    {
        if (!enableKeyboard())
            return FALSE;
    }
    
    turnOnLCDBackLight();
    turnOnKBDBackLight();
    
#if defined(IWL220) || defined(ICT250)
    displayMessage("���� ��� ����. . .", DISPLAY_START_LINE + 1, ALL_LINES);
#else
    displayMessage("���� ��� ����...", DISPLAY_START_LINE + 1, ALL_LINES);
#endif

    //get the schedule date time
    //two functions below are not for castles POS
    saveTMSScheduleDateTimeFromServer();
    scheduleDateTime = getScheduleDateTime();    //For what???
    if (getClockHandle() <= 0)
    {
        if (!enableClock())
            return FALSE;
    }

    deleteOldAppFiles(serverSpec); //just for ingenico for TMS
    startCommunication(&(serverSpec->files));
    setCardHandle(0);
    setSocketHandle(-1);
   
    initDisplayPic(serverSpec);    //ABS:CHANGE:960806
    initQx1000(); // contactless for verifone
    
    if (!getContactLessState())
        initPP1000SE(); // pin pad for verifone
    
    if (getPrinterHandle() <= 0)
        initPrinter();
    
    if (getSmartCardHandle() <= 0)
    	enableSmartCard();

/******Open Ethernet Port******/
	if (terminalCapability.LANCapability)
    {
        retValue = openLANPort();
        if ((retValue != SUCCESS) && (retValue != SOCKET_IS_OPEN))
        {
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ETHERNET NOT OPEN : %x ", retValue);
            addSystemErrorReport(retValue, ERR_TYPE_LAN);
            displayMessageBox("���� LAN ��� ���!", MSG_ERROR);
        }
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ETHERNET is OPEN : %x ", retValue);
    }
	 server = serverSpec;
	 showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "terminalInitiation", "Checking swich data");

/******INIT TMS FILE******/     
    if ((terminalCapability.TMSCapability) && (POSService.TMSService))//..HNO_980902
    { 
        if (fileExist(TMS_INFO_FILE) != SUCCESS)
        {
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "terminalInitiation", "TMS Schedule");
            if (initTMSFiles() != TRUE)
                displayMessageBox("���� �������� TMS", MSG_ERROR);

            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "set Periodic TMSSchedule");
        }                                                

        TMSupdateAppVer();
        TMSAutoSendConfirmation();
        
        check = checkAppVersion();
        if (check)
            updateLastTMSTime();
    }
//#HNO_980721
//    if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
//    {
//        if (fileExist(POS_TIME_FILE) != SUCCESS) 
//        {
//            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "POS_TIME_FILE");
//            if (!PasswordGenerator())
//            {
//                displayMessageBox("��� ������� ����� ���!", MSG_ERROR);
//                return FALSE;
//            }
//
//            if (updateFileInfo(POS_TIME_FILE, &dateTime.date, sizeof(uint32)) != SUCCESS)
//                return FALSE;
//        }
//    }
//    else
//    {
//        PasswordGenerator();
//    }
    
    if (getPOSService().shiftService == TRUE)//MRF_NEW15 MOVE FROM UP
       if (!loadUserListFile())
           displayMessageBox("��� �� ��ѐ���� ���� �������!", MSG_ERROR);
    
    while (server != NULL)
    {
        if (fileExist(server->files.generalConnInfoFile) != SUCCESS)
        {       
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "terminalInitiation", "connection file doesn't exist");
            displayMessageBox("��� ���� ������� �����", MSG_ERROR); 
        }
        else
        {
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "generalConnInfoFile is exist");
            len = sizeof(generalConnInfoST);
            retValue = readFileInfo(server->files.generalConnInfoFile, &generalConnInfo, &len);
            if (retValue != SUCCESS)  
            {
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "readFileInfo", "generalConnInfoFile dont read");
                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                displayMessageBox(errorMsg, MSG_ERROR);
                return FALSE;
            }
            setGeneralConnInfo(server->files.id, &generalConnInfo);
        }
        
        for (connCounter = 0; connCounter < generalConnInfo.connectionCount; connCounter++)
        {
            connectionType = generalConnInfo.connectionsType[connCounter];
            if (connectionType == CONNECTION_HDLC)
            {
                setConnectionType(CONNECTION_HDLC);
                openModemHDLC(DEFAULT_HDLC_MODEM_MODE, DEFAULT_HDLC_MODEM_HANDSHAKE);
                break;
            }
        }
        
        initSchedules(&(server->files)); //++MRF_IDENT ??  
        checkUnsuccessTransaction(&(server->files));  
        
        if ((terminalCapability.TMSCapability) && (POSService.TMSService))//..HNO_980902
            checkUnsuccessTMS(&(server->files));
             
        server = server->next;
    }
    
  //--HNO_980722  
//    if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
//    {
//        retValue = checkDate(); 
//        if (retValue == FALSE)
//            displayMessageBox("��� ������� ����� ���!", MSG_ERROR);
//    }
    
    return TRUE;
}

/**
 * Init terminal spec.
 * @see        showLog().
 * @see        initPOSCapability().
 * @see        updateFileInfo().
 * @see        addSystemErrorReport().
 * @return     TRUE or FALSE.
 */
uint8 initTerminalSpec(uint8 flag)//#MRF_971225
{
    int16               retValue            = FAILURE;          /** file update return value */
    terminalSpecST      terminalInfo        = {0};
    serviceSpecST       services            = {0};
    uint32 				len                 = sizeof(terminalSpecST);
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "***********initTerminalSpec***********");
    
    initPOSCapability(&terminalInfo);
    
    if (flag)//+MRF_971225 is first time
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Flag Is True");
        initPOSServices(&services);
        
         //MRF_NEW12
        retValue = updateFileInfo(POS_SERVICE_FILE, &services,  sizeof(serviceSpecST));  
        if (retValue != SUCCESS) 
        {
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "POS SERVICE FILE dont update");
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
            return FALSE;
        }
    }
    
    if (fileExist(SETTING_FILE) != SUCCESS)
        initUserSetting();
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: terminalSpecFile");
    retValue = updateFileInfo(TERMINAL_SPEC_FILE, &terminalInfo,  sizeof(terminalSpecST));  
    if (retValue != SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "terminal spec file dont update");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "initTerminalSpec", "********END**********");
    return TRUE;
}

/**
 * Check unsuccess transaction.
 * @param  files [input] : one record of files.
 * @see    fileExist().
 * @see    readFileInfo().
 * @see    addSystemErrorReport().
 * @see    displayMessageBox().
 * @see    printTransactionBuy().
 * @see    printTransactionBuy().
 * @see    printTransactionBillPay().
 * @see    fileRemove().
 * @see    fileExist().
 * @see    printReversal().
 * @see    doConnect().
 * @see    clearDisplay().
 * @see    reversalTransMaskan().
 * @see    reversalTrans().
 * @see    disconnectAndClose().
 * @see    setReversalScheduleAfterUnsuccessReversal().
 * @return None.
 */
void checkUnsuccessTransaction(filesST* files)
{
    int16           		retValue    		= 0;
    uint32          		len         		= sizeof(lastCustomerReceiptST);            /** size of transaction receipt */
    messageSpecST   		messageSpec;
    lastCustomerReceiptST	lastCustomerReceipt;
    
    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&lastCustomerReceipt, 0, sizeof(lastCustomerReceiptST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkUnsuccessTransaction");
    
    if (fileExist(files->customerReceiptFile) == SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "customer Receipt file exist");
        retValue = readFileInfo(files->customerReceiptFile, &lastCustomerReceipt, &len);        
        if (retValue != SUCCESS)
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        else 
        {
            if ((strlen(lastCustomerReceipt.PAN) != 0)
                || (strlen(lastCustomerReceipt.amount) != 0))
            {
                    structCpy(&lastCustomerReceipt, &messageSpec);
                    if (messageSpec.transType == TRANS_LOANPAY)//HNO_ADD_lOAN
                    {
                    	//HNO_ADD for managing transaction's which does'nt have reference number
                    	messageSpec.responseStatus = 95;
                        printLoanPayTransaction(files,&messageSpec,TRUE, NO_REPRINT);
                    }
                    else if (messageSpec.transType == TRANS_ETC)//HNO_IDENT
                    {
                    	//HNO_ADD for managing transaction's which does'nt have reference number
                    	messageSpec.responseStatus = 95;
                        printETCTransaction(files,&messageSpec,TRUE, NO_REPRINT);
                    }
                    else
                    {
                    retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
                    if (retValue == KBD_TIMEOUT || retValue == KBD_ENTER || retValue == KBD_CONFIRM)
                    {
                        if (messageSpec.transType == TRANS_BUY)
                            printTransactionBuy(files, &messageSpec, FALSE, NO_REPRINT);
                        else if (messageSpec.transType == TRANS_BILLPAY)
                            printTransactionBillPay(files, &messageSpec, FALSE, NO_REPRINT);
//   --HNO_NEW10                     else if (messageSpec.transType == TRANS_BUYCHARGE)
//                            printTransactionBuyCharge(files, &messageSpec, FALSE, NO_REPRINT);
                    }
            }
        }
        }
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
        fileRemove(files->customerReceiptFile);        
    }
    
   if (fileExist(files->reversalReceiptFile) == SUCCESS)
   {
        printHeaderLogoAndMerchantInfo();//+HNO_980620
        printReversal(files);
   }
    
    if (fileExist(files->reversalTransFile) == SUCCESS)
    {
    	//HNO_ADD
		#ifdef IWL220
				int displayHeaderStatus = DisplayHeader(0);
		#endif
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "reversal exist");

        if (doConnect(files))
        {
            reversalTrans(files);
            disconnectAndClose(0);
        }
        else
            setReversalScheduleAfterUnsuccessReversal(files);
        //HNO_ADD
		#ifdef IWL220
				DisplayHeader(displayHeaderStatus);
		#endif
    }
    
    
    if (fileExist(files->settelmentInfoFile) == SUCCESS) 
    {
		#ifdef IWL220
				int displayHeaderStatus = DisplayHeader(0);
		#endif
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "settlement exist");
        
        if (doConnect(files))
        {
            settlementTrans(files);
            disconnectAndClose(0);
        }
        else
           setSettlementScheduleAfterUnsuccessSettlement(files);
        //HNO_ADD
		#ifdef IWL220
				DisplayHeader(displayHeaderStatus);
		#endif
    }
    
    if (fileExist(LOGON_INFO_FILE) == SUCCESS) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "logon exist");
        
        if (doConnect(files))
        {
            logonTrans(files);
            disconnectAndClose(0);
        }
		else
        	setLogonScheduleAfterUnsuccessLogon(files);
    }
}

//#MRF_971015
void getVersion (uint8* verNumber)
{
   uint8    version[16]   = {0}; 
   
   strcpy(version,VERSION);
   strncpy(verNumber,&version[8],4);
   verNumber[4] = 0;
   showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "verNumber: %s", verNumber);
}

//#MRF_971015
void getAppName(uint8* appName)
{
   uint8    name[16]   = {0};
   
   strcpy(name,VERSION);
   strncpy(appName, name, 7);
   appName[7] = 0;
   
   showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "appName: %s", appName);
}


//MRF_NEW2
uint8 initUserSetting(void)
{   
    int16 				retValue 	= FAILURE;          // file update return value 
    settingSpecST       settingInfo;
    
    memset(&settingInfo, 0, sizeof(settingSpecST));

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*****initUserSetting*****");
    
    settingInfo.kbdSound                = TRUE;
    settingInfo.beepSound               = TRUE;
    settingInfo.displayTimeout          = STANDBY_TIMEOUT;
    settingInfo.backLight               = TRUE;
    strcpy(settingInfo.fontType, "TTF_FONT");
    strcpy(settingInfo.fontName, "roya");
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: Setting File");
    retValue = updateFileInfo(SETTING_FILE, &settingInfo, sizeof(settingSpecST));  
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }

    return TRUE;
}

//MRF
void userSetting(void)
{
    int8   				page 						= 1;             								// configuration page        
    uint8  				pressedKey 					= 0;       										// pressed key     
    uint8  				keyPressed 					= FALSE;   										// key pressed or not
    uint8  				itemVal[100] 				= {0};   										// item value 
    uint8  				validKeys[5] 				= {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8  				showMenu 					= TRUE;      									// loop control, always "TRUE"      
    uint8  				updateFile 					= FALSE;   										// update file after call each edit function 
    int16 				retValue 					= FAILURE;     									// file function return values 
    uint32 				len 						= sizeof(settingSpecST);
    uint8               errorMsg[50]            	= {0};
    uint8               selectionItems[2][20] 		= {"����", "�����"};
    uint8               selectionSoundItems[2][20] 	= {"���� ����", "���� �����"};
    uint8               selectionFontItems[][20]	= {"roya","Yekan","Sols","Yas"};
    uint8               maxPage                 	= 0;
    uint8               displayFontChange       	= FALSE;
    uint8               selectedItemIndex       	= FALSE;
    terminalSpecST      terminalCapability      	= getTerminalCapability();
    settingSpecST       settingInfo;
    settingSpecST       bkpSettingInfo;
//    uint8               printerFontChange     	= FALSE;
//    uint8               selectionFontType[2][20]= {"TTF", "FNT"};

    memset(&settingInfo, 0, sizeof(settingSpecST));
    memset(&bkpSettingInfo, 0, sizeof(settingSpecST));
    
    retValue = readFileInfo(SETTING_FILE, &settingInfo, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }

    clearKeyBuffer(); 

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkpSettingInfo = settingInfo;
        
#ifdef  IWL220	//+HNO_971015 we have 3 item in iwl
        switch (page)
        {
            case 1:
                if (settingInfo.kbdSound)
                    strcpy(itemVal, "����");
                else
                    strcpy(itemVal, "�����");
				displayAndEditValue("���� ���� ����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);//ABS:FARSI
                break;
            case 2:
                if (settingInfo.displayTimeout != 0)
                    sprintf(itemVal, "%ld", settingInfo.displayTimeout);
                else
                    strcpy((char*)itemVal, "---");
                displayAndEditValue("standby timeout", itemVal, FALSE, SHOW_ALL);//..HNO_971012
                break;
            case 3://MRF_NEW20
                if (settingInfo.beepSound)
                    strcpy(itemVal, "���� ����");
                else
                    strcpy(itemVal, "���� �����");
					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                break;
            default:
                break;
        }
        maxPage = 3;
#else
        if ((!terminalCapability.GPRSCapability) && (terminalCapability.supportTTFFont))
        {
            switch (page) 
            {
                case 1:
                    if (settingInfo.kbdSound)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("���� ���� ����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);//ABS:FARSI
                    break;
                case 2:
                    if (settingInfo.backLight)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    strcpy(itemVal, settingInfo.fontName); 
 					displayAndEditValue("���� ���� �����", settingInfo.fontName, FALSE, SHOW_ALL);
                    break;
                case 4://MRF_NEW20
                    if (settingInfo.beepSound)
                        strcpy(itemVal, "���� ����");
                    else 
                        strcpy(itemVal, "���� �����");
 					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
//                case 4: //MRF_NEW15
//                    strcpy(itemVal, settingInfo.fontType); 
//                    displayAndEditValue("Printer Font", settingInfo.fontType, FALSE, SHOW_EDIT_LEFT_ARROW);
//                    break;
                default:
                    break;
            }
            maxPage = 4;
        }
        else if ((terminalCapability.GPRSCapability) && (terminalCapability.supportTTFFont))
        {
            switch (page) 
            {
                case 1:
                    if (settingInfo.kbdSound)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("���� ���� ����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);//ABS:FARSI
                    break;
                case 2:
                    if (settingInfo.backLight)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:
                    if (settingInfo.displayTimeout != 0) 
                        sprintf(itemVal, "%ld", settingInfo.displayTimeout);
                    else 
                        strcpy((char*)itemVal, "---");
                    displayAndEditValue("Display Timeout", itemVal, FALSE, SHOW_ALL);
                    break;          
                case 4:
                    strcpy(itemVal, settingInfo.fontName);//#MRF_971030
					displayAndEditValue("���� ���� �����", settingInfo.fontName, FALSE, SHOW_ALL);
                    break;
                case 5://MRF_NEW20
                    if (settingInfo.beepSound)
                        strcpy(itemVal, "���� ����");
                    else 
                        strcpy(itemVal, "���� �����"); 
 					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
//                case 5: //MRF_NEW15
//                    strcpy(itemVal, settingInfo.fontType); 
//                    displayAndEditValue("Printer Font", settingInfo.fontType, FALSE, SHOW_EDIT_LEFT_ARROW);
//                    break;
                default:
                    break;
            }
            maxPage = 5;
        }
        else if ((!terminalCapability.GPRSCapability) && (!terminalCapability.supportTTFFont))
        {
            switch (page) 
            {
                case 1:
                    if (settingInfo.kbdSound)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("���� ���� ����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);//ABS:FARSI
                    break;
                case 2:
                    if (settingInfo.backLight)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_ALL);//ABS:FARSI
                    break;
                case 3://MRF_NEW20
                    if (settingInfo.beepSound)
                        strcpy(itemVal, "���� ����");
                    else 
                        strcpy(itemVal, "���� �����");
 					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                    break;
            }

            maxPage = 3;
        }
        else if ((terminalCapability.GPRSCapability) && (!terminalCapability.supportTTFFont))
        {
            switch (page) 
            {
                case 1:
                    if (settingInfo.kbdSound)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("���� ���� ����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);//ABS:FARSI
                    break;
                case 2:
                    if (settingInfo.backLight)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�����");
					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_ALL);//ABS:FARSI
                    break;
                case 3:
                    if (settingInfo.displayTimeout != 0) 
                        sprintf(itemVal, "%ld", settingInfo.displayTimeout);
                    else 
                        strcpy((char*)itemVal, "---");
                    displayAndEditValue("Standby Timeout", itemVal, FALSE, SHOW_ALL);//#MRF_971019
                    break;  
                case 4://MRF_NEW20
                    if (settingInfo.beepSound)
                        strcpy(itemVal, "���� ����");
                    else 
                        strcpy(itemVal, "���� �����");
 					displayAndEditValue("��� �����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;
                default:
                    break;
            }
            maxPage = 4;
        }
#endif
        
        keyPressed = FALSE; 
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);

        if (!keyPressed)
            pressedKey = KBD_CANCEL; 

        switch (pressedKey)
        {
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
#ifdef  IWL220//+HNO_971015
			switch (page)
			{
				case 1:
					if (edit2StatusItem(&settingInfo.kbdSound, selectionItems, "���� ���� ����"))//ABS:FARSI
						updateFile = TRUE;
					break;
				case 2:
					if (editNumericValue(&settingInfo.displayTimeout, "timeout(15-120)", 15, 120, FALSE, FALSE))
						updateFile = TRUE;
					break;
				case 3:
					if (edit2StatusItem(&settingInfo.beepSound, selectionSoundItems, "��� �����"))
						updateFile = TRUE;
					break;
				default:
					break;
			}
#else
                if ((!terminalCapability.GPRSCapability) && (terminalCapability.supportTTFFont))
                {
                    switch (page) 
                    {
                        case 1:
							if (edit2StatusItem(&settingInfo.kbdSound, selectionItems, "���� ���� ����"))//ABS:FARSI						
                                updateFile = TRUE;
                            break;
                        case 2:
							if (edit2StatusItem(&settingInfo.backLight, selectionItems, "��� �����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 3:
                            if (selectItemPage("���� ���� �����", selectionFontItems, FARSI, 4, &selectedItemIndex, DISPLAY_TIMEOUT))
                            {
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "index: %d", selectedItemIndex);
                                strcpy(settingInfo.fontName,selectionFontItems[selectedItemIndex]);
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Font Name: %s", settingInfo.fontName);
                                updateFile = TRUE;
                                displayFontChange = TRUE;
                            }
                            break;
                        case 4://MRF_NEW20
                            if (edit2StatusItem(&settingInfo.beepSound, selectionSoundItems, "��� �����"))
                                updateFile = TRUE;
                            break;
//                        case 4://MRF_NEW15
//                            if (selectItemPage("Printer Font", selectionFontType, ENGLISH, 2, &selectedItemIndex, DISPLAY_TIMEOUT))
//                            {
//                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "index: %d", selectedItemIndex);
//                                strcpy(settingInfo.fontType,selectionFontType[selectedItemIndex]);
//                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Font Name: %s", settingInfo.fontType);
//                                updateFile = TRUE;
//                                printerFontChange = TRUE;
//                            }
//                            break;  
                        default:
                            break;
                    }
                }
                else if ((terminalCapability.GPRSCapability) && (terminalCapability.supportTTFFont))
                {
                    switch (page) 
                    {
                        case 1:
							if (edit2StatusItem(&settingInfo.kbdSound, selectionItems, "���� ���� ����"))//ABS:FARSI		
                                updateFile = TRUE;
                            break;
                        case 2:
							if (edit2StatusItem(&settingInfo.backLight, selectionItems, "��� �����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 3:
                            if (editNumericValue(&settingInfo.displayTimeout, "Timeout(1-120)", 1, 120, FALSE, FALSE))//#MRF_971019
                                updateFile = TRUE;
                            break;     
                        case 4:
                            if (selectItemPage("���� ���� �����", selectionFontItems, FARSI, 4, &selectedItemIndex, DISPLAY_TIMEOUT))//#MRF_971030
                            {
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "index: %d", selectedItemIndex);
                                strcpy(settingInfo.fontName,selectionFontItems[selectedItemIndex]);
                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Font Name: %s", settingInfo.fontName);
                                updateFile = TRUE;
                                displayFontChange = TRUE;
                            }
                            break;
                        case 5://MRF_NEW20
                            if (edit2StatusItem(&settingInfo.beepSound, selectionSoundItems, "��� �����"))
                                updateFile = TRUE;
                            break;
//                        case 5://MRF_NEW15
//                            if (edit2StatusItem(&settingInfo.fontType[0], selectionItems, "Printer Font")) //ABS:CHANGE
//                            {
//                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "index: %d", selectedItemIndex);
//                                strcpy(settingInfo.fontType,selectionFontType[selectedItemIndex]);
//                                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Font Name: %s", settingInfo.fontType);
//                                updateFile = TRUE;
//                                printerFontChange = TRUE;
//                            }
//                            break;
                        default:
                            break;
                    }
                }
                else if ((!terminalCapability.GPRSCapability) && (!terminalCapability.supportTTFFont))
                {
                    switch (page) 
                    {
                        case 1:
							if (edit2StatusItem(&settingInfo.kbdSound, selectionItems, "���� ���� ����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 2:
							if (edit2StatusItem(&settingInfo.backLight, selectionItems, "��� �����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 3://MRF_NEW20
                            if (edit2StatusItem(&settingInfo.beepSound, selectionSoundItems, "��� �����"))
                                updateFile = TRUE;
                            break;
                        default:
                            break;
                    }
                }
                else if ((terminalCapability.GPRSCapability) && (!terminalCapability.supportTTFFont))
                {
                    switch (page) 
                    {
                        case 1:
							if (edit2StatusItem(&settingInfo.kbdSound, selectionItems, "���� ���� ����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 2:
							if (edit2StatusItem(&settingInfo.backLight, selectionItems, "��� �����"))//ABS:FARSI	
                                updateFile = TRUE;
                            break;
                        case 3:
                            if (editNumericValue(&settingInfo.displayTimeout, "Timeout(1-120)", 1, 120, FALSE, FALSE))//#MRF_971019
                                updateFile = TRUE;
                            break;
                        case 4://MRF_NEW20
                            if (edit2StatusItem(&settingInfo.beepSound, selectionSoundItems, "��� �����"))
                                updateFile = TRUE;
                            break;
                        default:
                            break;
                    }
                }
#endif

                if (updateFile)
                {               
                    retValue = updateFileInfo(SETTING_FILE, &settingInfo, sizeof(settingSpecST));  
                    if (retValue != SUCCESS) 
                    {
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        settingInfo = bkpSettingInfo;
                    }
                    updateFile = FALSE;
                    setUserSetting(settingInfo);
                    readUserSetting();
                    if (displayFontChange)
                    {
                        clearDisplay();
                        displayMessageBox("���� �� ������ ����� ����.", MSG_INFO);
                    }
                }

                break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    } 
    
    clearDisplay();
//#endif
}

//MRF_NEW9
void setupServices(argumentListST* args)
{
    filesST*            files 				    = (filesST*) (args->argumentName); //MRF_SHIFT
    int8                page                    = 1;                            // configuration page */        
    uint8               pressedKey              = 0;                            // pressed key */   
    uint8               itemVal[100]            = {0};                          // item value */
    uint8               keyPressed              = 0;
    uint8               validKeys[5]            = {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8               showMenu                = TRUE;                         // loop control, always "TRUE" 
    uint8               updateFile              = FALSE;                        // udate file after call each edit function 
    int16               retValue                = FAILURE;                      // file function retun values 
    uint8               errorMsg[50]            = {0};
    uint8               maxPage                 = 0;
    uint32 				len 					= sizeof(terminalSpecST);
	uint8               selectionItems[2][20]   = {"����", "�������"};
    terminalSpecST      terminalSpec            = getTerminalCapability();
    serviceSpecST       bkPOSService;
    serviceSpecST       POSService;
            
    memset(&POSService, 0, sizeof(serviceSpecST));
    memset(&bkPOSService, 0, sizeof(serviceSpecST));
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "setupServices");

    retValue = readFileInfo(POS_SERVICE_FILE, &POSService, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }
    
    clearKeyBuffer();

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkPOSService = POSService;
        if ((terminalSpec.pcPosCapability) && (terminalSpec.TMSCapability))//..HNO_980902
        {
            switch (page) 
            {
                case 1:  
//                    if (terminalSpec.pcPosCapability)//#HNO_980902 not useful
                if (POSService.PcPOSService)
                    strcpy(itemVal, "����");
                else 
                    strcpy(itemVal, "�������");

                    displayAndEditValue("������ �� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break; 
                case 2:  //+HNO_980902
                if (terminalSpec.TMSCapability)
                    if (POSService.TMSService)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�������");
                    displayAndEditValue("TMS", itemVal, FALSE, SHOW_ALL);
                    break; 
                case 3:  
                    if (POSService.shiftService)
						strcpy(itemVal, "����");
                    else 
						strcpy(itemVal, "�������");
					displayAndEditValue("����", itemVal, FALSE, SHOW_ALL);
                    break; 
                case 4:  
                    if (POSService.depositID)
						strcpy(itemVal, "����");
                    else 
						strcpy(itemVal, "�������");
					displayAndEditValue("����� �����", itemVal, FALSE, SHOW_ALL);
                    break; 
                case 5:
                    if (POSService.discount)
						strcpy(itemVal, "����");
                    else 
						strcpy(itemVal, "�������");
				   displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break;  
                default:
                    break;
            }
            maxPage = 5;//..HNO_980902
        }
        else
        {

#ifdef VX520			//+ABS_981002
			switch (page)
			{
			case 1:
				 if (terminalSpec.pcPosCapability)
				if (POSService.PcPOSService)
					strcpy(itemVal, "����");
				else
					strcpy(itemVal, "�������");

				displayAndEditValue("������ �� ������", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
				break;
			case 2:    
				if (POSService.shiftService)
					strcpy(itemVal, "����");
				else
					strcpy(itemVal, "�������");
				displayAndEditValue("����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
				break; 
			case 3:  
				if (POSService.depositID)
					strcpy(itemVal, "����");
				else
					strcpy(itemVal, "�������");
				displayAndEditValue("����� �����", itemVal, FALSE, SHOW_ALL);
				break; 
			case 4:
				if (POSService.discount)
					strcpy(itemVal, "����");
				else
					strcpy(itemVal, "�������");
				displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
				break; 
			default:
				break;
			}

			maxPage = 4;
#else
            switch (page) 
            {
                case 1:    
					if (POSService.shiftService)
						strcpy(itemVal, "����");
					else
						strcpy(itemVal, "�������");
					displayAndEditValue("����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break; 
                case 2:  
					if (POSService.depositID)
						strcpy(itemVal, "����");
					else
						strcpy(itemVal, "�������");
					displayAndEditValue("����� �����", itemVal, FALSE, SHOW_ALL);
                    break; 
                case 3:
					if (POSService.discount)
						strcpy(itemVal, "����");
					else
						strcpy(itemVal, "�������");
					displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break; 
                default:
                    break;
            }
            
            maxPage = 3;
#endif
        }
        keyPressed = FALSE; 
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
        
        if (!keyPressed)
            pressedKey = KBD_CANCEL;
        
        switch (pressedKey)
        {
            
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
            {
                if ((terminalSpec.pcPosCapability) && (terminalSpec.TMSCapability))//..HNO_980902
                {
                    switch (page) 
                    {
                        case 1:
                            if (edit2StatusItem(&POSService.PcPOSService, selectionItems, "������ �� ������"))
                                updateFile = TRUE;
                            break;
                        case 2:
                            if (edit2StatusItem(&POSService.TMSService, selectionItems, "TMS"))//+HNO_980902
                                updateFile = TRUE;
                            break;
                        case 3:
                            if (edit2StatusItem(&POSService.shiftService, selectionItems, "����"))
                                updateFile = TRUE;
                            break;
                        case 4:
                            if (edit2StatusItem(&POSService.depositID, selectionItems, "����� �����"))
                                updateFile = TRUE;
                            break;
                        case 5:
                            if (edit2StatusItem(&POSService.discount, selectionItems, "�����"))
                                updateFile = TRUE;
                            break;
                        default:
                            break;
                    }
                }
                else
                {

#ifdef VX520			//+ABS_981002
					switch (page)
					{
					case 1:
						if (edit2StatusItem(&POSService.PcPOSService, selectionItems, "������ �� ������"))
							updateFile = TRUE;
						break;
					case 2:
						if (edit2StatusItem(&POSService.shiftService, selectionItems, "����"))
							updateFile = TRUE;
						break;
					case 3:
						if (edit2StatusItem(&POSService.depositID, selectionItems, "����� �����"))
							updateFile = TRUE;
						break;
					case 4:
						if (edit2StatusItem(&POSService.discount, selectionItems, "�����"))
							updateFile = TRUE;
						break;
					default:
						break;
					}
#else
                    switch (page) 
                    {
                        case 1:
							if (edit2StatusItem(&POSService.shiftService, selectionItems, "����"))
                                updateFile = TRUE;
                            break;
                        case 2:
							if (edit2StatusItem(&POSService.depositID, selectionItems, "����� �����"))
                                updateFile = TRUE;
                            break;
                        case 3:
							if (edit2StatusItem(&POSService.discount, selectionItems, "�����"))
                                updateFile = TRUE;
                            break;
                        default:
                            break;
                    }
#endif
                }
                
                if (updateFile)
                {
                    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: POS_SERVICE_FILE");
                    retValue = updateFileInfo(POS_SERVICE_FILE, &POSService, sizeof(serviceSpecST));  
                    if (retValue != SUCCESS) 
                    {
                        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                        displayMessageBox(errorMsg, MSG_ERROR);
                        POSService = bkPOSService;
                    }

                    readPOSService();

                    if (POSService.shiftService == TRUE)
                     {
                        if (checkPrerequisitDisableShift(files, FALSE) == FALSE)//HNO_DEBUG2
                        {
                            POSService.shiftService = FALSE;
                            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: POS_SERVICE_FILE");
                            retValue = updateFileInfo(POS_SERVICE_FILE, &POSService, sizeof(serviceSpecST));
                            if (retValue != SUCCESS)
                            {
                                    addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                                    getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                                    displayMessageBox(errorMsg, MSG_ERROR);
                                    POSService = bkPOSService;
                            }
                            readPOSService();
                            break;
                        }
                        
                        if(fileExist(USER_MANAGEMENT_FILE) != SUCCESS)
                        {
                           addUser();
                           setShiftChange(NO_DISPLAY);
                        }
                    }
					else if (POSService.shiftService == FALSE && bkPOSService.shiftService == TRUE)
                    {
                        if (checkPrerequisitDisableShift(files, TRUE) == FALSE)
                        {
                            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "EXIST REVERS");
                            POSService.shiftService = TRUE;
                            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: POS_SERVICE_FILE");
                            retValue = updateFileInfo(POS_SERVICE_FILE, &POSService, sizeof(serviceSpecST));  
                            if (retValue != SUCCESS) 
                            {
                                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                                getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                                displayMessageBox(errorMsg, MSG_ERROR);
                                POSService = bkPOSService;
                            }
                            readPOSService();
                        }
                    }
                    
                    updateFile = FALSE;
                }
            }
            break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    }
    
    clearDisplay();
} 

//MRF_SHIFT
uint8 checkPrerequisitDisableShift(filesST* files, uint8 check)
{
    int16           retValue     = -1;
    uint8           key          = KBD_CANCEL;
    uint8           showMsg      = FALSE;       //ABS_NEW1

    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkPrerequisitDisableShift");

    if (fileExist(files->reversalTransFile) == SUCCESS || fileExist(files->reversalReceiptFile) == SUCCESS)
    {
         showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "REVERSAL EXIST");
		 displayMessageBox("�� ���� ���� ���� �ѐ�� ���� ���� ����!", MSG_ERROR);//ABS:CHANGE

		 return FALSE;
    }
    else if (check)
    {
        key = displayMessageBox("���� ���� ��� ������� �ǘ ����� ��!", MSG_CONFIRMATION);//ABS:CHANGE
        if (key == KBD_ENTER || key == KBD_CONFIRM)
        {        
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "","**fileRemove USER_MANAGEMENT_FILE");
            retValue = fileRemove(USER_MANAGEMENT_FILE);
            if (retValue != SUCCESS)
            {
                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                displayMessageBox("����� ������ ���� ��� ���� ���� ����!", MSG_ERROR);
                return FALSE;
            }

            retValue = deletAllDependentFilesShift();
            if (retValue != SUCCESS)
            {
                addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                return FALSE;
            }
			    
            resetAllVariableShift();
			resetMerchantMenuPassword(showMsg);
        }
        else
            return FALSE;
    }
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "END OF CHECK");
    return TRUE;
}

//MRF_NEW14
uint8 saveAppVersion(void)
{
    uint8   verNumber[12] = {0};
    uint32  retValue      = 0;
    
    getVersion(verNumber);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: TMSScheduleFile");
    
    retValue = updateFileInfo(VERSIONS_FILE, verNumber, sizeof(verNumber));   
    if (retValue != SUCCESS)
    {
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "TMS ERR#13");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    } 
}

//MRF_NEW14
uint8 readLastAppVersion(uint8* verNumber)
{
    
    uint32  retValue      = 0;
    uint32  len           = sizeof(verNumber);
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: TMSScheduleFile");
    if (fileExist(VERSIONS_FILE) == SUCCESS)
    {
        retValue = readFileInfo(VERSIONS_FILE, verNumber, &len);   
        if (retValue != SUCCESS)
        {
			showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "TMS ERR#13");
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
            return FALSE;
        } 
    } 
}

//MRF_NEW14
uint8 checkAppVersion(void)
{
    uint8    currentVersion[12]  = {0};
    uint8    lastVersion[12]     = {0};
    
    getVersion(currentVersion);
    readLastAppVersion(lastVersion);
    if (strcmp(currentVersion, lastVersion) == 0)
        return FALSE;
    else
        return TRUE;
}

//+MRF_971207
void setupMerchantServices(void)
{
    int8                page                    = 1;                            // configuration page */        
    uint8               pressedKey              = 0;                            // pressed key */   
    uint8               itemVal[100]            = {0};                          // item value */
    uint8               keyPressed              = 0;
    uint8               validKeys[5]            = {KBD_EDIT, KBD_NEXT, KBD_PREV, KBD_CANCEL};
    uint8               showMenu                = TRUE;                         // loop control, always "TRUE" 
    uint8               updateFile              = FALSE;                        // udate file after call each edit function 
    int16               retValue                = FAILURE;                      // file function retun values 
    uint8               errorMsg[50]            = {0};
    uint8               maxPage                 = 0;
    uint32              len                     = sizeof(terminalSpecST);
    uint8               selectionItems[2][20]   = {"����", "�������"};
    serviceSpecST       bkPOSService;
    serviceSpecST       POSService;
            
    memset(&POSService, 0, sizeof(serviceSpecST));
    memset(&bkPOSService, 0, sizeof(serviceSpecST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "setupServices");

    retValue = readFileInfo(POS_SERVICE_FILE, &POSService, &len);
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox(errorMsg, MSG_ERROR);
        return;
    }
    
    clearKeyBuffer();

    while (showMenu) 
    {
        keyPressed = FALSE;
        pressedKey = 0;
        updateFile = FALSE;
        bkPOSService = POSService;
        {
            switch (page) 
            {
                case 1:  
                    if (POSService.tip)
                        strcpy(itemVal, "����");
                    else
                        strcpy(itemVal, "�������");
                    displayAndEditValue("�����", itemVal, FALSE, SHOW_EDIT_RIGHT_ARROW);
                    break; 
               case 2:  
                    if (POSService.confirmationAmount)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�������");
                    displayAndEditValue("������� ����", itemVal, FALSE, SHOW_ALL);
                    break;
                case 3:  
                    if (POSService.merchantReceipt)
                        strcpy(itemVal, "����");
                    else 
                        strcpy(itemVal, "�������");
                    displayAndEditValue("�ǁ ���� �������", itemVal, FALSE, SHOW_EDIT_LEFT_ARROW);
                    break; 
                default:
                    break;
            }
            maxPage = 3;
        }
        keyPressed = FALSE; 
        getKey(&pressedKey, &keyPressed, validKeys, GET_INPUT_TIMEOUT);
        
        if (!keyPressed)
            pressedKey = KBD_CANCEL;
        
        switch (pressedKey)
        {
            
            case KBD_NEXT:
                if (page != maxPage)
                    page++;
                break;
            case KBD_PREV:
                if (page != 1)
                    page--;
                break;
            case KBD_EDIT:
            {
                switch (page) 
                {
                    case 1:
                         if (edit2StatusItem(&POSService.tip, selectionItems, "�����"))
                            updateFile = TRUE;
                        break;
                    case 2:
                        if (edit2StatusItem(&POSService.confirmationAmount, selectionItems, "������� ����"))
                            updateFile = TRUE;
                        break;
                    case 3:
                        if (edit2StatusItem(&POSService.merchantReceipt, selectionItems, "�ǁ ���� �������"))
                            updateFile = TRUE;
                        break;
                    default:
                        break;
                }
            }

            if (updateFile)
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: POS_SERVICE_FILE");

                retValue = updateFileInfo(POS_SERVICE_FILE, &POSService, sizeof(serviceSpecST));  
                if (retValue != SUCCESS) 
                {
                    addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
                    getSystemErrorMessage(errorMsg, retValue, ERR_TYPE_FILE_SYSTEM);
                    displayMessageBox(errorMsg, MSG_ERROR);
                    POSService = bkPOSService;
                }

                readPOSService();
                updateFile = FALSE;
            }
            break;
            case KBD_CANCEL:
                showMenu = FALSE;
                break;
            default:
                break;
        }
    }
    clearDisplay();
} 



