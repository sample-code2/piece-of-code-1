 //#include "SDK30.h"	//HNO_IDENT
#include <string.h>
//MRF #include <stdio.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_dateTime.h"
#include "N_printerWrapper.h" 
#include "N_cardSpec.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_connection.h" 
#include "N_scheduling.h"
#include "N_merchantSpec.h" 
#include "N_messaging.h" 
#include "N_getUserInput.h" 
#include "N_terminalSpec.h" 
#include "N_transactions.h" 
#include "N_POSSpecific.h"
#include "N_displayWrapper.h" 
#include "N_display.h"
#include "N_printer.h"
#include "N_initialize.h"
#include "N_log.h"

#ifdef CASTLES_V7
#include "HASIN_FONT.h" 
#endif

//#define d_BUFF_SIZE 32 --MRF_NEW9 DONT USED
// babuff[d_BUFF_SIZE];//ABS:COMMENT
/*
* Show string on LCD.
* @param  string [input]: string
* @param  line [input]: row
* @see    setLCDInvert().
* @see    selectNormalDisplayFont().
* @see    selectNormalDisplayFont().
* @see    selectBigDisplayFont().
* @see    selectSmallDisplayFont().
* @see    getDisplayCharacterCount().
* @see    wordWrapEnglish().
* @see    convertCp1256ToIransystem().
* @see    wordWrapFarsi().
* @see    getMaxLineInScreen().
* @see    convertCp1256ToTTFFont().
* @see    displayOnScreen().
* @return None.
*/
#ifndef INGENICO //MRF_IDENT2
void displayString(uint8* string, uint8 line) 
{
    uint8   cpyStr[250]             				= {0};
    uint8   separatedStr[10][250]                   = {0, 0};
    uint32	stringLen               				= 0;
    uint32	leftMargin              				= 0;
    uint32	rightMargin                             = 0;
    int		align                   				= string[1];
    int		language                				= string[2];
    int     size                                    = string[0];
    int		loop                    				= 0;
    int		i                       				= 0;
    int     length                                  = 0;
    int     displayCharacterCount                   = 0;
    
    
    setLCDInvert(FALSE); 
    
    #ifdef CASTLES_V5S
    if (size == NORMAL_STRING || size == NORMAL_INVERT_STRING)
    	selectNormalDisplayFont();
    else if (size == BIG_STRING || size == BIG_INVERT_STRING)
    	selectBigDisplayFont();
    else if (size == SMALL_STRING || size == SMALL_INVERT_STRING)
    	selectSmallDisplayFont();
#endif
    
    displayCharacterCount = getDisplayCharacterCount();
    
    strcpy((char*)cpyStr, &string[3]);
    if (language == FARSI)
    {
        length = strlen(cpyStr);
        
        #ifdef CASTLES_V5S
             loop = wordWrapEnglish(cpyStr, separatedStr, displayCharacterCount); 
        #else
            convertCp1256ToIransystem(cpyStr);
            loop = wordWrapFarsi(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT,length); // wrapping the string
        #endif
        for (i = 0; i <= loop; i++)
        {
            stringLen = strlen((const char*)separatedStr[i]);
            if (stringLen > 0)
            {
                if (align == ALIGN_CENTER)
                {
                    leftMargin = (DISPLAY_CHARACTER_COUNT) / 2 - (stringLen) / 2 ;
                    rightMargin = DISPLAY_CHARACTER_COUNT - leftMargin - stringLen;
                }
                else if (align == ALIGN_RIGHT)
                {
                    leftMargin = (DISPLAY_CHARACTER_COUNT - stringLen);
                    rightMargin = 0;
                }

                if (line + i <= (getMaxLineInScreen() + DISPLAY_START_LINE))
                {
#ifdef CASTLES_V5S
                        convertCp1256ToTTFFont(separatedStr[i], TRUE);
#endif
						displayOnScreen(leftMargin + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen,align);
                   
                }
            }
        }
    }
    if (language == ENGLISH)
    {       
        loop = wordWrapEnglish(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT); // wrapping the string
        
        for (i = 0; i <= loop; i++)
        {
            stringLen = strlen((const char*)separatedStr[i]);           
            if (stringLen > 0)
            {               
                if (align == ALIGN_CENTER)
                {
                    leftMargin = (DISPLAY_CHARACTER_COUNT) / 2 - (stringLen) / 2 ;
                    rightMargin = DISPLAY_CHARACTER_COUNT - leftMargin - stringLen;
                }
                else if (align == ALIGN_RIGHT)
                {
                    leftMargin = (DISPLAY_CHARACTER_COUNT - stringLen) ;
                    rightMargin = 0;
                }

                if (line + i <= (getMaxLineInScreen() + DISPLAY_START_LINE))
                {
	                displayOnScreen(leftMargin + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen,align);                
                }
            }
        }
    }
}
#else
void displayString(uint8* string, uint8 line) //MRF_IDENT2 REMOVE OTHER DEFINE
{
	uint8   cpyStr[250]             				= {0};
	uint8   separatedStr[10][250]                   = {0, 0};
	uint8	alignedStr[100]					= {0};//HNO_IDENT
	uint32	stringLen               				= 0;
	uint32	leftMargin              				= 0;
	uint32	rightMargin                             = 0;
	int		align                   				= string[1];
	int		language                				= string[2];
	int     size                                    = string[0];
	int		loop                    				= 0;
	int		i                       				= 0;
	int     length                                  = 0;
	int     displayCharacterCount                   = 0;

	strcpy((char*)cpyStr, &string[3]);

	loop = wordWrapEnglish(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT_CHECK);

	for (i = 0; i <= loop; i++)
	{
		memset(alignedStr, 0, sizeof(alignedStr));
		stringLen = strlen((const char*)separatedStr[i]) * 2;
		memcpy(alignedStr, separatedStr[i], strlen(separatedStr[i]));
		convertCp1256ToLCD(alignedStr);
		if (stringLen > 0)
		{
			if (align == ALIGN_CENTER)
			{
				rightMargin = alignCenterLCDString(stringLen, alignedStr);
			}
			else if (align == ALIGN_RIGHT)
			{
				leftMargin = (DISPLAY_CHARACTER_COUNT - stringLen);
				rightMargin = 0;
			}

			if (line + i <= (getMaxLineInScreen() + DISPLAY_START_LINE))
			{
				displayOnScreen(rightMargin + DISPLAY_START_COLUMN, line + i, alignedStr, stringLen, align);
			}
		}
	}
}
#endif



/**
 * Show reverse string on LCD.
 * @param  string [input]: string
 * @param  line [input]: row
 * @see    setLCDInvert().
 * @see    selectNormalDisplayFont()selectNormalDisplayFont().
 * @see    selectBigDisplayFont().
 * @see    selectSmallDisplayFont().
 * @see    getDisplayCharacterCount().
 * @see    wordWrapEnglish().
 * @see    convertCp1256ToIransystem().
 * @see    wordWrapFarsi().
 * @see    getMaxLineInScreen().
 * @see    convertCp1256ToTTFFont().
 * @see    displayOnScreen().
 * @return None.
 */
void displayInvertString(uint8* string, uint8 line) //this function get first character for Align & get second character for language
{
    uint8	    cpyStr[250]                             = {0};      /* copy of input string */
    uint8	    separatedStr[10][250]                   = {0, 0};
    uint8		alignedStr[100]							= {0};//HNO_IDENT
    int32	    leftSpace                               = 0;//HNO_IDENT
    int32	    rightSpace                              = 0;//HNO_IDENT
    uint8       spaceStr[DISPLAY_CHARACTER_COUNT + 1]   = {0};
    uint32      stringLen                               = 0;
    int         i                                       = 0;
    int         loop                                    = 0;
    int         align                                   = string[1];
    int         language                                = string[2];
    int         length                                  = 0;
    int         size                                    = string[0];
    uint8		leftMarginPixel            				= 0;//HNO_IDENT
    uint8		rightMarginPixel						= 0;//HNO_IDENT
    uint8		leftMargin              				= 0;//HNO_IDENT
    uint8		rightMargin								= 0;//HNO_IDENT
    
    setLCDInvert(TRUE); 
    

    #ifdef CASTLES_V5S
        if (size == NORMAL_STRING || size == NORMAL_INVERT_STRING)
            selectNormalDisplayFont();
        else if (size == BIG_STRING || size == BIG_INVERT_STRING )
            selectBigDisplayFont();
        else if (size == SMALL_STRING || size == SMALL_INVERT_STRING)
            selectSmallDisplayFont();
    #endif
    
    strcpy((char*)cpyStr, &string[3]);  
    memset(separatedStr, 0, sizeof(separatedStr));
    memset(spaceStr,' ', sizeof(spaceStr));
    spaceStr[DISPLAY_CHARACTER_COUNT] = 0;

    if (language == FARSI)
    {
        length = strlen(cpyStr);
#ifdef  CASTLES_V5S
        loop = wordWrapEnglish(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT); // wrapping the string
#else
#ifdef VX520
        convertCp1256ToIransystem(cpyStr); 
#endif
        loop = wordWrapFarsi(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT,length); // wrapping the string
#endif

        for (i = 0; i <= loop; i++)
        {
            stringLen = strlen((const char*)separatedStr[i]);
#if defined(IWL220) || defined(ICT250)
        	memcpy(alignedStr, separatedStr[i], strlen(separatedStr[i]));
        	convertCp1256ToLCD(alignedStr); //for IWL & ICT
        	stringLen = stringLen * 2;
#endif
            if (stringLen > 0)
            {
                if (align == ALIGN_CENTER)
                {
#if	defined(IWL220) || defined(ICT250)

                    rightMarginPixel = getRightMarginPixel(separatedStr[i], align);
                    leftMarginPixel = getLeftMarginPixel(separatedStr[i], rightMarginPixel, align);

                    rightMargin = getRightMargin(stringLen, alignedStr, align);
                    strcat(alignedStr, separatedStr[i]); // cat left margin + string
                    leftMargin = getLeftMargin(stringLen, alignedStr, align);
#else
                    leftSpace = (DISPLAY_CHARACTER_COUNT) / 2 - (stringLen) / 2;
                    rightSpace = DISPLAY_CHARACTER_COUNT - leftSpace - stringLen;
#endif
                }
                else if (align == ALIGN_RIGHT)
                {
#if	defined(IWL220) || defined(ICT250)

                	rightMarginPixel = getRightMarginPixel(separatedStr[i], align);
                	leftMarginPixel = getLeftMarginPixel(separatedStr[i], rightMarginPixel, align);

                    rightMargin = getRightMargin(stringLen, alignedStr, align);
                    strcat(alignedStr, separatedStr[i]); // cat left margin + string
                    leftMargin = getLeftMargin(stringLen, alignedStr, align);
#else
                    leftSpace = (DISPLAY_CHARACTER_COUNT - stringLen);
                    rightSpace = 0;
#endif
                }
#if	defined(IWL220) || defined(ICT250) //IDENT2
                displayOnScreen(DISPLAY_START_COLUMN, line + i, spaceStr, rightMargin, align);
				displayOnScreen(rightMarginPixel  + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen, align);
				displayOnScreen(leftMarginPixel + DISPLAY_START_COLUMN, line + i, spaceStr, leftMargin, align);
#elif defined(CASTLES_V5S)
                convertCp1256ToTTFFont(separatedStr[i], TRUE);
                displayOnScreen(DISPLAY_START_COLUMN, line + i, spaceStr, leftSpace,align);
                displayOnScreen(leftSpace + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen,align);                
                displayOnScreen( stringLen + leftSpace + DISPLAY_START_COLUMN, line + i, spaceStr, rightSpace,align); 
#else
				displayOnScreen(DISPLAY_START_COLUMN, line + i, spaceStr, leftSpace,align);
                displayOnScreen(leftSpace + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen,align);                
                displayOnScreen( stringLen + leftSpace + DISPLAY_START_COLUMN, line + i, spaceStr, rightSpace,align); 
#endif
            }
        }
    }
    
    if (language == ENGLISH)
    {   
        loop = wordWrapEnglish(cpyStr, separatedStr, DISPLAY_CHARACTER_COUNT); // wrapping the string

        for (i = 0; i <= loop; i++)
        {
            stringLen = strlen((const char*)separatedStr[i]);
            if (stringLen > 0)
            {
                if (align == ALIGN_CENTER)
                {
                    leftSpace = (DISPLAY_CHARACTER_COUNT) / 2 - (stringLen) / 2;
                    rightSpace = DISPLAY_CHARACTER_COUNT - leftSpace - stringLen;
                }
                else if (align == ALIGN_LEFT)
                {
                    rightSpace = (DISPLAY_CHARACTER_COUNT - stringLen);
                    leftSpace = 0;
                }
                displayOnScreen(DISPLAY_START_COLUMN, line + i, spaceStr, leftSpace,align);
                displayOnScreen(leftSpace + DISPLAY_START_COLUMN, line + i, separatedStr[i], stringLen,align);
                displayOnScreen(stringLen + leftSpace + DISPLAY_START_COLUMN, line + i, spaceStr, rightSpace,align);              
            }
        }
    }
    setLCDInvert(FALSE); //HNO_IDENT2
}

/**
 * Show One Pic on LCD.
 * @param   picType [input]: Type is defined
 * @see     displayBorderPic().
 * @see     displayReportPic().
 * @see     displayBankPic().
 * @see     displayBoxPic().
 * @see     displayErrorPic().
 * @see     displayWarningPic().
 * @see     displayInfoPic().
 * @see     displayConfirmationPic().
 * @see     displayConfirmPic().
 * @see     displayCancelPic().
 * @see     displayUpPic().
 * @see     displayDownPic().
 * @see     displayHeaderHardwareReportPic().
 * @see     displayHeaderBalance().
 * @see     displayHeaderShortBill().
 * @see     displayHeaderSyatemError().
 * @see     displayForwardPic().
 * @see     displayBackwardPic().
 * @see     displayEditPic()
 * @see     displayHeaderCellPhone().
 * @see     displayHeaderUnknwonBill().
 * @see     displayHeaderTransferMoney().
 * @see     displayHeaderPhoneBill().
 * @see     displayHeaderMunicipalBills().
 * @see     displayHeaderGasBill().
 * @see     displayHeaderElectricBill().
 * @see     displayHeaderLoanInfo().
 * @see     displayHeaderWaterBill().
 * @return  one pic
 */
void displayPic(uint8 picType)
{
    switch (picType)
    {
        case borderPic:
            displayBorderPic();
            break;
        case reportPic:
            displayReportPic();
            break;
        case bankPic:
            displayBankPic();
            break;
        case boxPic:
            displayBoxPic();
           break;
        case errorPic:
            displayErrorPic();
            break;
        case warningPic:
            displayWarningPic();
            break;
        case infoPic:
            displayInfoPic();
            break;
        case confirmationPic:
            displayConfirmationPic();
            break;
        case confirmPic:
            displayConfirmPic();
            break;
        case cancelPic:
            displayCancelPic();
            break;
        case upPic:
            displayUpPic();
            break;
        case downPic:
            displayDownPic();
            break;
        case headerHardwareReportPic:
            displayHeaderHardwareReportPic();
            break;
        case balance:
            displayHeaderBalance();
            break;
        case shortBill:
            displayHeaderShortBill();
        case systemError:
            displayHeaderSyatemError();
            break;
        case forwardPic:
            displayForwardPic();
            break;
        case backwardPic:
            displayBackwardPic();
            break;
        case editPic:
            displayEditPic();
            break;
        case cellPhone:
            displayHeaderCellPhone();
            break; 
        case unknownBill:
            displayHeaderUnknwonBill();
            break; 
        case transferMoney:
            displayHeaderTransferMoney();
            break; 
        case phoneBill:
            displayHeaderPhoneBill();
            break; 
        case municipalBills:
            displayHeaderMunicipalBills();
            break; 
        case gasBill:
            displayHeaderGasBill();
            break; 
        case electricBill:
            displayHeaderElectricBill();
            break; 
        case loanInfo: //MRF_NEW8
            displayHeaderLoanInfo();//MRF_NEW8
            break; 
        case waterBill:
            displayHeaderWaterBill();
            break;
        case buyTransPic:
            displayHeaderbuyTransPic();
            break;
        case buyUnsuccessTransPic:
            displayHeaderUnSuccessBuyTransPic();
            break;
        case unsuccessBillPic:
            displayHeaderUnSuccessBillPayTransPic();
            break;
        case bankingErrors:
            displayHeaderBankingErrors();
            break;
        case billTransPic:
            displayHeaderbillTransPic();
            break;
        case chargeTrans:
            displayHeaderChargeTransListPic();
            break; 
        case unsucessChargeTrans:
            displayHeaderUnsuccessChargeTransListPic();
            break;
        case sucessLoanPay:
            displayHeaderSuccessLoanPayTransListPic();
            break;
        case unsuccessLoanPay:
            displayHeaderUnSuccessLoanPayTransPic();
            break; 
        case supervisorActivitiesPic:
            displayHeaderSupervisorActivities();
            break;
        case ETCBuy:
            displayHeaderETCTrans(); //MRF_ETC
            break;
        case successETC:
            displayHeaderSuccessETCTransPic(); //MRF_ETC
            break;
        case unsuccessETC:
            displayHeaderUnSuccessETCTransPic(); //MRF_ETC
            break;
        case successTopup:
            displayHeaderSuccessTopupTransPic();    //MRF_TOPUP
            break;
        case unsuccessTopup:
            displayHeaderUnSuccessTopupTransPic();  //MRF_TOPUP
            break;
        case tmsReport:
            displayHeaderTMSReport();
            break;
        case ETCInfo:
            displayHeaderETCInfo(); //MRF_NEW8
            break;
        case successCharity:
            displayHeaderSuccessCharity(); //MRF_NEW16
            break;
        case unsuccessCharity:
            displayHeaderUnsuccessCharity(); //MRF_NEW17
            break;
        case tax:
            displayHeaderTax(); //MRF_NEW16
            break;
        case penalty:
            displayHeaderPenalty(); //MRF_NEW16
            break;
        default:
            break;
    }
}


/**
 * Show main page on screen.
 * @param  forcedDisplayLogo [input]: force for show logo get TRUE or FALSE
 * @see    getDateTime().
 * @see    isTouchScreen().
 * @see    getDisplayLogoCount().
 * @see    setLCDInvert().
 * @see    displayCyclicServersLogo().
 * @see    oneLineClear().
 * @see    displayDateTimeFarsi().
 * @return None.
 */
void displayMainPage(uint8 forcedDisplayLogo)
{
    dateTimeST  nowDateTime        = getDateTime();
    
    if (isTouchScreen())
       return;

    if ((forcedDisplayLogo == TRUE) || getDisplayLogoCount() > 1) 
   {
#if defined(ICT250) || defined(IWL220) //HNO_ADD

		clearDisplay();
#endif
           setLCDInvert(FALSE);
           displayCyclicServersLogo();
   }
   else
   {
	   setLCDInvert(FALSE);
   }
	
   displayDateTimeFarsi(&nowDateTime);
}


/**
 * Display available & ledger amount after balance transaction.
 * @param   accountBalance [output]: available amount
 * @param   withdrawalAmount [output]: leger amount
 * @see     clearDisplay().
 * @see     justifyOneStringFarsi().
 * @see     removePadLeft().
 * @see     makeValueWithCommaStr().
 * @see     displayScrollableWithHeader().
 * @return  pressed key.
 */
uint8 displayBalance(uint8* accountBalance, uint8* withdrawalAmount) 
{
    uint8   		valueWithComma[30]      = {0};                  /* amount value with comma, string form */
    uint8   		key                     = KBD_CANCEL;           /* entered key (terminal keypad) */
    uint8   		lines[6][32]            = {0};                  /* display information */
    uint16  		i                       =  0;
    terminalSpecST	terminalCapability 		= getTerminalCapability();
    
    clearDisplay(); 

    if (!terminalCapability.graphicCapability)
        justifyOneStringFarsi("������", lines[i++], PRN_NORM, ALIGN_CENTER);
    
    if (withdrawalAmount[0] != 0) 
    {
        justifyOneStringFarsi("���� ������", lines[i++], PRN_NORM, ALIGN_RIGHT);

        removePadLeft(withdrawalAmount, '0');
        makeValueWithCommaStr(withdrawalAmount, valueWithComma);
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI,valueWithComma,"����");

        //at least accountBalance must be '0'
        if (withdrawalAmount[0] == 0)
        {
        	withdrawalAmount[0] = '0';
        	withdrawalAmount[1] = 0;
        }
    }
    
    memset(valueWithComma, 0, sizeof(valueWithComma));
    if (accountBalance[0] != 0) 
    {
        justifyOneStringFarsi("�����", lines[i++], PRN_NORM, ALIGN_RIGHT);
        removePadLeft(accountBalance, '0');
        makeValueWithCommaStr(accountBalance, valueWithComma);
        sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI,valueWithComma,"����");

        //at least accountBalance must be '0'
        if (accountBalance[0] == 0)
        {
        	accountBalance[0] = '0';
        	accountBalance[1] = 0;
        }
    }

    strcpy(lines[i++], "");
    i--;
    
    if (!terminalCapability.graphicCapability) //++MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, FALSE);
    else
    	key = displayScrollableWithHeader("������", lines, i, DISPLAY_TIMEOUT, FALSE,balance);
    
    return key;
}


/**
 * Show date & time on LCD in English.
 * @param   dateTime [input]: date and time
 * @see     gregorianToJalali().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     getHour().
 * @see     getMinute().
 * @see     displayOnScreen().
 * @return  date time English.
 */
void displayDateTimeEnglish(dateTimeST* dateTime)
{
    uint8	dateTimeStr[17]	= {0};		/* date and time string */
    uint32	jalaliDate      = 0;		/* date in jalali form */
    int		year            = 0;
    int		month           = 0;
    int		day             = 0;
    int		hour            = 0; 
    int		minute          = 0; 

    gregorianToJalali(&jalaliDate, dateTime->date);

    year = getYear(jalaliDate);
    month = getMonth(jalaliDate);
    day = getDay(jalaliDate);
    hour = getHour(dateTime->time);
    minute = getMinute(dateTime->time);
    
    sprintf(dateTimeStr, "%02d/%02d/%02d %02d:%02d", year, month, day, hour, minute);
    displayOnScreen(DISPLAY_START_LINE, DISPLAY_START_COLUMN, dateTimeStr, strlen(dateTimeStr),NULL);
}

/**
 * Show date & time on LCD in Farsi.
 * @param   dateTime [input]: date and time
 * @see     gregorianToJalali().
 * @see     getYear().
 * @see     getMonth().
 * @see     getDay().
 * @see     getHour().
 * @see     getMinute().
 * @see     displayTimeBox().
 * @see     displayforeColor().
 * @see     displayBackColor().
 * @see     displayTextOut().
 * @see     convertCp1256ToIransystem().
 * @see     displayOnScreen().
 * @return  date time Farsi.
 */
void displayDateTimeFarsi(dateTimeST* dateTime)//HNO_CHANGE
{
    uint8               dateTimeStr[100]    = {0};		/** date and time string */
	uint8               dateTimeStr_move[100] = { 0 };		/** date and time string */
    uint8               dateTimeStr1[30]    = {0};		/* date and time string */
    uint32              jalaliDate          = 0;		/* date in jalali form */
    int                 year                = 0;
    int                 month               = 0;
    int                 day                 = 0;
    int                 hour                = 0; 
    int                 minute              = 0; 
    terminalSpecST      terminalCapability  = getTerminalCapability();
    
    gregorianToJalali(&jalaliDate, dateTime->date);

    year   = getYear(jalaliDate) - 1300;
    month  = getMonth(jalaliDate);
    day    = getDay(jalaliDate);
    hour   = getHour(dateTime->time);
    minute = getMinute(dateTime->time);

    if (terminalCapability.graphicCapability)
    {
        sprintf(dateTimeStr, " %02d/%02d/%02d ", year, month, day);
        sprintf(dateTimeStr1, " %02d:%02d ", hour, minute);

        if (!terminalCapability.GPRSCapability)
        {
//            displayforeColor(BLUE);
            displayforeColor(WHITE);
            displayBackColor(VIOLET_LIGHT);
//            displayBackColor(ORANGE);
//            displayTextOut(47,17,dateTimeStr,DISPLAY_SIZE_SMALL,FALSE);
            displayTextOut(44,10,dateTimeStr,DISPLAY_SIZE_SMALL,FALSE);
            displayBackColor(RED);
//            displayBackColor(ORANGE);
//            displayTextOut(198,17,dateTimeStr1,0x0E18,FALSE);
            displayTextOut(210,10,dateTimeStr1,0x0E18,FALSE);
            displayforeColor(BLUE);
            displayBackColor(WHITE);
//            displayBackColor(WHITE);
        }
        else
        {
            displayforeColor(VIOLET_LIGHT);
            displayBackColor(WHITE);
            displayTextOut(34,10,dateTimeStr,0x0D17,FALSE);
            displayTextOut(152,10,dateTimeStr1,0x0D17,FALSE);
            displayforeColor(BLUE);
            displayBackColor(WHITE);
        }
    }
    else

		{
#if	defined(ICT250) || defined(IWL220)
    memset(dateTimeStr, 0, sizeof(dateTimeStr));
    sprintf(dateTimeStr, "%02d:%02d", hour, minute);
    showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "**** dateTimeStr: %s", dateTimeStr);
    showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "**** hour: %02d", hour);
    showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "**** Min: %02d", minute);
    convertCp1256ToLCD(dateTimeStr);
    dateTimeStr[10] = 0;
#ifdef IWL220
    _DrawExtendedString(0, 12, "                                                ", _ON_, _XLARGE_, _PROPORTIONNEL_);
    displayOnScreenWithoutRefresh(DISPLAY_START_COLUMN, DISPLAY_START_LINE + DATE_TIME_LINE + 1, dateTimeStr, strlen(dateTimeStr));
#else
    _DrawExtendedString(0, 0, "                                                ", _OFF_, _XLARGE_, _PROPORTIONNEL_);
    displayOnScreenWithoutRefresh(DISPLAY_START_COLUMN, DISPLAY_START_LINE + DATE_TIME_LINE, dateTimeStr, strlen(dateTimeStr));
#endif

    memset(dateTimeStr, 0, sizeof(dateTimeStr));
    sprintf(dateTimeStr, "%02d/%02d/%02d", year, month, day);
    convertCp1256ToLCD(dateTimeStr);//HNO_CHANGE the name of function should be change
#ifdef IWL220
    displayOnScreenWithoutRefresh(/*80*/68, DISPLAY_START_LINE + DATE_TIME_LINE + 1, dateTimeStr, strlen(dateTimeStr));
    _DrawLine(0, 13, 127, 13, _OFF_);
    _DrawLine(0, 28, 127, 28, _OFF_);
    _DrawLine(0, 29, 127, 29, _ON_);
#else
    showLog(JUST_INGENICO, __FILE__, __LINE__, TRACE, "", "**** X Size: %d", SizeOfUnicodeString(dateTimeStr, _dNORMAL_, _PROP_WIDTH_));
	displayOnScreenWithoutRefresh(128 - SizeOfUnicodeString(dateTimeStr, _dNORMAL_, _PROP_WIDTH_), DISPLAY_START_LINE + DATE_TIME_LINE, dateTimeStr, strlen(dateTimeStr));
#endif
	PaintGraphics();
//	InitContexteGraphique(PERIPH_DISPLAY);
#else
            sprintf(dateTimeStr, "%02d/%02d/%02d   %02d:%02d", year, month, day, hour, minute);
            convertCp1256ToIransystem(dateTimeStr);
            displayOnScreen(DISPLAY_START_COLUMN, DISPLAY_START_LINE + DATE_TIME_LINE, dateTimeStr, strlen(dateTimeStr),NULL);
#endif
    }
}

//void displayDateTimeFarsiF4(dateTimeST* dateTime)
//{
//    uint8	dateTimeStr[30]	= {0};		/* date and time string */
//    uint8	dateTimeStr1[30] = {0};		/* date and time string */
//    uint32	jalaliDate      = 0;		/* date in jalali form */
//    int		year            = 0;
//    int		month           = 0;
//    int		day             = 0;
//    int		hour            = 0; 
//    int		minute          = 0; 
//    
//    gregorianToJalali(&jalaliDate, dateTime->date);
//
//    year   = getYear(jalaliDate);
//    month  = getMonth(jalaliDate);
//    day    = getDay(jalaliDate);
//    hour   = getHour(dateTime->time);
//    minute = getMinute(dateTime->time);
//    
//    year = year - 1300;
//    sprintf(dateTimeStr, " %02d/%02d/%02d ", year, month, day);
//    sprintf(dateTimeStr1, " %02d:%02d ", hour, minute);
//
//    #ifdef CASTLES_V5S
//            displayforeColor(WHITE);
//            displayBackColor(DARK_BLUE);
//            displayTextOut(52,58,dateTimeStr,0x0B18,FALSE);
//            displayBackColor(DARK_GREEN);
//            displayTextOut(170,45,dateTimeStr1,0x0B18,FALSE);
//            displayforeColor(BLUE);
//            displayBackColor(WHITE);
//    #else
//            sprintf(dateTimeStr, "%02d/%02d/%02d %02d:%02d", year, month, day, hour, minute);
//            convertCp1256ToIransystem(dateTimeStr);
//            displayOnScreen(DISPLAY_START_COLUMN, DISPLAY_START_LINE + DATE_TIME_LINE, dateTimeStr, strlen(dateTimeStr),NULL);
//    #endif
//}


/**
 * Show reverse string of Farsi.
 * @param  string [input]: string
 * @param  line [input]: row
 * @param  size [input]: size of string
 * @param  align [input]: align of string
 * @see    displayInvertString().
 * @return None.
 */
void displayInvertStringFarsi(uint8* string, uint8 line, int size, int align)  
{
    uint8 cpyStr[250] = {0};                   /* copy of input string */
    
    sprintf(cpyStr, "%c%c%c%s", size, align, FARSI, string);
#if defined(ICT250) || defined(IWL220) //HNO_ADD
    setLCDInvert(TRUE);
    setLineInvert(TRUE, line);
    displayString(cpyStr, line);
    setLCDInvert(FALSE);
#else
    displayInvertString(cpyStr, line);
#endif
}


// MGH 300311 : added
/**
 * Show reverse string of English.
 * @param  string [input]: string
 * @param  line [input]: row
 * @param  size [input]: size of string
 * @param  align [input]: align of string
 * @see    displayInvertString().
 * @return None.
 */
void displayInvertStringEnglish(uint8* string, uint8 line, int size, int align)  
{
    uint8 cpyStr[250] = {0};                   /* copy of input string */

    sprintf(cpyStr, "%c%c%c%s", size, align, ENGLISH, string);
    //HNO_ADD
#if defined(ICT250) || defined (IWL220)
    setLCDInvert(TRUE);
    setLineInvert(TRUE, line);
    displayString(cpyStr, line);
    setLCDInvert(FALSE);
#else
    displayInvertString(cpyStr, line);
#endif
}


//MGH: TODO: mrf correct this function. new vs old...!
/**
 * Display  information scrollable.
 * @param   title [input]: title of page
 * @param   lines [input]: lines for display include information
 * @param   linesCount [input]: number of lines
 * @param   timeout [input]: timeout 
 * @param   confirmable [input]: Confirmable is or not
 * @see     clearDisplay().
 * @see     clearKeyBuffer().
 * @see     displayforeColor().
 * @see     displayInvertString().
 * @see     displayPic().
 * @see     displayMessageBox().
 * @see     displayBleach().
 * @see     oneLineClear().
 * @see     displayInvertString().
 * @see     displayString().
 * @see     getKey().  
 * @return  last pressed key or time out.
 */
uint8 displayScrollable2(uint8* title, uint8 lines[][32], uint16 linesCount, uint32 timeout, uint8 confirmable) 
{
    int		counter         	= 0;            /* loop counter */
    int		index           	= 0;            /* lines index */
    int		lastIndex       	= 0;
    uint8	pressedKey      	= 0;            /* pressed key */
    uint8	keyPressed      	= FALSE;        /* is any key pressed */
    uint8	first           	= TRUE;         /* first key press */
    uint8   validKeys[8 + 1]	= {KBD_CANCEL, KBD_ENTER, KBD_UP, KBD_DOWN, KBD_PAGE_UP, KBD_PAGE_DOWN, KBD_CONFIRM, KBD_REJECT};
    uint8	key 				= KBD_CANCEL; 

     
    clearDisplay();
    
    if (linesCount == 0) 
        return KBD_CANCEL;

    clearDisplay();
    clearKeyBuffer();
    displayforeColor(BLACK);
    pressedKey = 0;
    
    #ifndef CASTLES_V5S
        displayInvertString(title, DISPLAY_START_LINE);
        displayPic(reportPic);
    #endif
         
    while (TRUE) 
    {
        lastIndex = index;
        keyPressed = FALSE;
        
        if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL)
        {
            if (!confirmable || (linesCount <= index + 3))
            {
            	if (pressedKey == KBD_CANCEL)
            	{
            		key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
                    { 
                        displayforeColor(BLACK);
        				lastIndex = -1; //refresh display screen 
                    }
            	}
            	else
            		return pressedKey;
            }
        }
        
        if (pressedKey == KBD_CONFIRM || pressedKey == KBD_REJECT) 
        { 
            if ((linesCount <= index + 3))
        	{
        		if (pressedKey == KBD_REJECT)
        		{
        			key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
        				lastIndex = -1; //refresh display screen
        		}
        		else
        			return pressedKey;
        	}
        }
                
        if (pressedKey == KBD_UP && index > 0) 
        {
            #ifdef CASTLES_V5S
                if (index == linesCount - 3)
                    displayBleach();
            #endif
            index--;
        }
        if (pressedKey == KBD_DOWN && index <= (linesCount - 4)) 
            index++; 
        
        if (pressedKey == KBD_PAGE_UP && index > 0)
        {
            index -= 4;
            if (index < 0) 
                index = 0;
        }

        if (pressedKey == KBD_PAGE_DOWN)
        {
            index += 4;
            if (index + 3 > linesCount)
                index = linesCount - 3;
        }

        if (first || index != lastIndex) 
        {
            #ifndef CASTLES_V5S
                oneLineClear(DISPLAY_START_LINE + 1);
            #endif
            oneLineClear(DISPLAY_START_LINE + 2);
            oneLineClear(DISPLAY_START_LINE + 3);
            #ifdef CASTLES_V5S
                oneLineClear(DISPLAY_START_LINE + 4);
                oneLineClear(DISPLAY_START_LINE + 5);
            #endif

            for (counter = 0; counter < MAX_LINE_IN_SCREEN + 1; counter++)
            {
                if ((counter + index) < linesCount)
                {
                    if ((lines[counter + index][0] == SMALL_INVERT_STRING) || (lines[counter + index][0] == NORMAL_INVERT_STRING)||
                        (lines[counter + index][0] == BIG_INVERT_STRING))
                    {
                        #ifdef CASTLES_V5S
                                displayforeColor(RED);
                                displayString(lines[counter + index ], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                                displayforeColor(BLACK);
                        #else
                                displayInvertString(lines[counter + index], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                        #endif
                    }
                    else
                        displayString(lines[counter + index], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                }
            }
        }

        if (linesCount <= index + 3)
        {
            displayPic(confirmPic);
            displayPic(cancelPic);
        }
        else 
        {
	        if (index != 0)
	            displayPic(upPic);
	        if (index != linesCount - 3)
	            displayPic(downPic);  
        }
        
        first = FALSE;
        
        getKey(&pressedKey, &keyPressed, validKeys, timeout);
        
        if (!keyPressed)
        {
            confirmable = FALSE; 
            pressedKey = KBD_TIMEOUT;
            return pressedKey;
        }
    } 
}

//MGH: TODO: mrf correct this function. new vs old...!
uint8 displayScrollable(uint8* title, uint8 lines[][32], uint16 linesCount, uint32 timeout, uint8 confirmable) 
{
    int		counter         	= 0;            /** loop counter */
    uint8	pressedKey      	= 0;            /** pressed key */
    uint8	keyPressed      	= FALSE;        /** is any key pressed */
    int		index           	= 0;            /** lines index */
    uint8	first           	= TRUE;         /** first key press */
    int		lastIndex       	= 0;
    uint8   validKeys[8 + 1]	= {KBD_CANCEL, KBD_ENTER, KBD_UP, KBD_DOWN, KBD_PAGE_UP, KBD_PAGE_DOWN, KBD_CONFIRM, KBD_REJECT};
    uint8	key 				= KBD_CANCEL; 
     
    if (linesCount == 0) 
        return KBD_CANCEL;

    clearKeyBuffer();
    
    pressedKey = 0;

    while (TRUE) 
    {
        lastIndex = index;
        keyPressed = FALSE;

        if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL)
        {
            if (!confirmable || (linesCount <= index + 3))
            {
            	if (pressedKey == KBD_CANCEL)
            	{
            		key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
        				lastIndex = -1; //refresh display screen 
            	}
            	else
            		return pressedKey;
            }
        }
        
        if (pressedKey == KBD_CONFIRM || pressedKey == KBD_REJECT) 
        { 
            if ((linesCount <= index + 3))
        	{
        		if (pressedKey == KBD_REJECT)
        		{
        			key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
        				lastIndex = -1; //refresh display screen
        		}
        		else
        			return pressedKey;
        	}
        }
                
        if (pressedKey == KBD_UP && index > 0) 
            index--;
        
        if (pressedKey == KBD_DOWN && index <= (linesCount - 4)) 
            index++; 
        
        if (pressedKey == KBD_PAGE_UP && index > 0)
        {
            index -= 4;
            if (index < 0) 
                index = 0;
        }

        if (pressedKey == KBD_PAGE_DOWN)
        {
            index += 4;
            if (index + 3 > linesCount)
                index = linesCount - 3;
        }

        if (first || index != lastIndex) 
        {
            clearDisplay();
            
            for (counter = 0; counter < 4; counter++)
            {
                if ((counter + index) == 0)
                {
#if defined (ICT250) || defined (IWL220)//HNO_ADD
                	displayInvertStringFarsi(lines[counter + index], counter + DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
#else
                    displayInvertString(lines[counter + index], counter + DISPLAY_START_LINE);
#endif
                    displayPic(borderPic);
                }
                else if ((counter + index) < linesCount)
                    displayString(lines[counter + index], counter + DISPLAY_START_LINE);
            }
        }

        if (linesCount <= index + 3)
        {
            displayPic(confirmPic);
            displayPic(cancelPic);
        }
        else
        {
	        if (index != 0)
	            displayPic(upPic);
	        if (index != linesCount - 3)
	            displayPic(downPic);
        }
        
        first = FALSE;
        
        getKey(&pressedKey, &keyPressed, validKeys, timeout);
        
        if (!keyPressed)
        {
            confirmable = FALSE; 
            pressedKey = KBD_TIMEOUT;
            return pressedKey;
        }
    } 
}

/**
 * Display  Message Box.
 * @param   msg [input]: Message shown on screen
 * @param   type [input]: Type of Message
 * @see     clearDisplay().
 * @see     displayStringFarsi().
 * @see     displayInvertStringFarsi().
 * @see     displayPic().
 * @see     errorBeep().
 * @see     warningBeep().
 * @see     displayforeColor().
 * @see     getKey().
 * @return  pressedKey().
 */
uint8 displayMessageBox(uint8* msg, uint8 type)
{
    uint32  timeout             = 0;
    uint8   pressedKey          = KBD_TIMEOUT;                                      /* pressed key */
    uint8   keyPressed          = FALSE;                                            /* user pressed key or not */
    uint8   validKeys[4 + 1]    = {KBD_CANCEL, KBD_ENTER, KBD_CONFIRM, KBD_REJECT};	/* valid keys */
    uint8   line                = DISPLAY_START_LINE + 2 + EMPTY_LINE;              /* error message desplay line */
    int 	DisplayHeaderStatus = changeDisplayHeader(0);//HNO_ADD
    
    clearDisplay();
    
    if (strlen(msg) > DISPLAY_CHARACTER_COUNT)
    	line--;
#if defined(IWL220) || defined(ICT250) //IDENT2
    displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);//HNO 
#endif
    if (type == MSG_ERROR)
    {    
         #ifndef CASTLES_V5S   
            displayInvertStringFarsi("�����", DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
         #endif
        displayPic(errorPic);
        displayforeColor(BLACK);
        #if !defined(IWL220) && !defined(ICT250) //IDENT2
        displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);
        #endif
        errorBeep();
        timeout = ERR_TIMEOUT;        
    }
    else if (type == MSG_WARNING)
    {
        #ifndef CASTLES_V5S   
            displayInvertStringFarsi(" �����", DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
        #endif
        displayPic(warningPic);
        displayforeColor(BLACK);
        #if !defined(IWL220) && !defined(ICT250) //IDENT2
        displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);
        #endif
        warningBeep();
        timeout = WARN_TIMEOUT;
    }
    else if (type == MSG_INFO)
    {
        #ifndef CASTLES_V5S   
        displayInvertStringFarsi(" �����", DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
        #endif
        displayPic(infoPic);
        displayforeColor(BLACK);
        #if !defined(IWL220) && !defined(ICT250) //IDENT2
        displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);
        #endif
        timeout = INFO_TIMEOUT;
    }
    else if (type == MSG_CONFIRMATION)
    {
        #ifndef CASTLES_V5S   
        displayInvertStringFarsi(" �������", DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);
        #endif
        displayPic(confirmationPic);
        displayPic(confirmPic);
        displayPic(cancelPic);
        displayforeColor(BLACK);
        #if !defined(IWL220) && !defined(ICT250) //IDENT2
        displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);
        #endif
        timeout = GET_CONFIRM_TIMEOUT;
    }

//    displayforeColor(BLACK);
    //HNO_IDENT in !defined we should have AND not OR
//    #if !defined(IWL220) && !defined(ICT250) //IDENT2
//        displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);
//    #endif
    displayforeColor(BLUE);
    #ifndef CASTLES_V5S
        displayPic(boxPic);
    #endif
    
    getKey(&pressedKey, &keyPressed, validKeys, timeout);
    changeDisplayHeader(DisplayHeaderStatus);//HNO_ADD IDENT2
    if (!keyPressed)   
        pressedKey = KBD_TIMEOUT;
    
    clearDisplay();
    return pressedKey;
}

/**
 * Display  Show One Message.
 * @param   msg [input]: Message
 * @param   line [input]: show message on this line
 * @param   clearLines [input]: Number of line will be clear
 * @see     clearDisplay().
 * @see     oneLineClear().
 * @see     displayStringFarsi().
 * @return  None.
 */
uint8 displayMessage(uint8* msg, uint8 line, uint8 clearLines)
{
     switch (clearLines)
    {
        case NO_LINES:
            break;
        case ALL_LINES:
            clearDisplay();
            break;
        default:
            if (clearLines & LINE_1)
                oneLineClear(DISPLAY_START_LINE);
            if (clearLines & LINE_2)
                oneLineClear(DISPLAY_START_LINE + 1);
            if (clearLines & LINE_3)
                oneLineClear(DISPLAY_START_LINE + 2);
            if (clearLines & LINE_4)
                oneLineClear(DISPLAY_START_LINE + 3);
            break;
           
    }
    
    displayforeColor(BLUE);
    displayStringFarsi(msg, line, PRN_NORM, ALIGN_CENTER);

}

/**
 * Display edit value page.
 * @param   title [input]: One String as title of page
 * @param   value [input]: Value
 * @param   isIP [input]: Is Ip or not
 * @param   editLineFeature [input]: line feature
 * @see     clearDisplay().
 * @see     displayforeColor().
 * @see     displayStringEnglish().
 * @see     displayStringFarsi().
 * @see     displayEditPic().
 * @see     displayForwardPic().
 * @see     displayBackwardPic().
 * @return  None.  
 */
void displayAndEditValue(uint8* title, uint8* value, int isIP, uint8 editLineFeature) 
{
    uint8   displayStr[100]         = {0};          /* display string */
    uint8   ipStr[50]               = {0};	//HNO_CHANGE

    clearDisplay();
    displayforeColor(BLUE);

    if (getTerminalCapability().graphicCapability)//MRF_NEW19
        displayStringFarsi(title, LINE_2, PRN_NORM, ALIGN_CENTER);//ABS:FARSI
    else
    	displayInvertStringFarsi(title, DISPLAY_START_LINE, PRN_NORM, ALIGN_CENTER);//HNO_1
  
    if (!isIP) 
    {
        if (strlen((const char*)value) == 0)
            sprintf(displayStr, "%s", "0");
        else
            sprintf(displayStr, "%s", value);
    }
    else 
    {       
        sprintf(ipStr, "%d.%d.%d.%d", value[0], value[1], value[2], value[3]);
        sprintf(displayStr, "%s", ipStr);
    }
 
    displayStringFarsi(displayStr, DISPLAY_START_LINE + 2 + EMPTY_LINE, PRN_NORM, ALIGN_CENTER);
    
    switch (editLineFeature)
    {
        #ifdef CASTLES_V5S
                case SHOW_EDIT:
                    displayEditPic();
                    break;
                case SHOW_EDIT_RIGHT_ARROW:
                     displayEditPic();
                     displayForwardPic();
                    break;
                case SHOW_EDIT_LEFT_ARROW:
                    displayEditPic();
                    displayBackwardPic();
                    break;
                case SHOW_ARROWS:
                    displayBackwardPic();
                    displayForwardPic();
                    break;
                case SHOW_RIGHT_ARROW:
                    displayForwardPic();
                    break;
                case SHOW_LEFT_ARROW:
                    displayBackwardPic();
                    break;
                case SHOW_ALL:
                    displayEditPic();
                    displayBackwardPic();
                    displayForwardPic();
                    break;
                default:
                    break;
        #else
                case SHOW_EDIT:
                    //displayStringEnglish(MENU_ACCESSORY_EDIT, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);
					displayInvertStringFarsi(MENU_ACCESSORY_EDIT_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_EDIT_RIGHT_ARROW:
                    //displayStringEnglish(MENU_ACCESSORY_EDIT_RIGHT_ARROW, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);
					displayInvertStringFarsi(MENU_ACCESSORY_EDIT_RIGHT_ARROW_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_EDIT_LEFT_ARROW:
                    //displayStringEnglish(MENU_ACCESSORY_EDIT_LEFT_ARROW, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);	
					displayInvertStringFarsi(MENU_ACCESSORY_EDIT_LEFT_ARROW_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_ARROWS:
                   // displayStringEnglish(MENU_ACCESSORY_ARROWS, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);
					displayInvertStringFarsi(MENU_ACCESSORY_ARROWS_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_RIGHT_ARROW:
                    //displayStringEnglish(MENU_ACCESSORY_RIGHT_ARROW, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);
					displayInvertStringFarsi(MENU_ACCESSORY_RIGHT_ARROW_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_LEFT_ARROW:
					displayInvertStringFarsi(MENU_ACCESSORY_LEFT_ARROW_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI
                    break;
                case SHOW_ALL:
                   // displayStringEnglish(MENU_ACCESSORY, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);
					displayInvertStringFarsi(MENU_ACCESSORY_FARSI, DISPLAY_START_LINE + 3, PRN_NORM, ALIGN_LEFT);//ABS:FARSI

                    break;
                default:
                    break;
        #endif
    }
}

/**
 * Display String Farsi.
 * @param   string [input]: One String
 * @param   line [input]: Number of Line
 * @param   size [input]: Size of string
 * @param   align [input]: Align of string
 * @see     displayString().
 * @return  cpyStr.
 */
void displayStringFarsi(uint8* string, uint8 line, int size, int align)  
{
    uint8 cpyStr[250] = {0};                   /* copy of input string */

    sprintf(cpyStr, "%c%c%c%s", size, align, FARSI, string);
//	if (size == PRN_NORM_INVERSE)
//		displayInvertString(cpyStr, line);//ABS:FARSI
//	else
		displayString(cpyStr, line);
}

/**
 * Display string English.
 * @param   string [input]: One String
 * @param   line [input]: Number of Line
 * @param   size [input]: Size of string
 * @param   align [input]: Align of string
 * @see     displayString().
 * @return  cpyStr.
 */
void displayStringEnglish(uint8* string, uint8 line, int size, int align)  
{
    uint8 cpyStr[250] = {0};
    
    sprintf(cpyStr, "%c%c%c%s", size, align, ENGLISH, string);
    displayString(cpyStr, line);
}

/**
 * Justify One String Farsi.
 * @param   s1 [input]: One String is input
 * @param   outStr [output]: is output
 * @param   size [input]: Size of string
 * @param   align [input]: Align of string
 * @return  outStr.
 */
void justifyOneStringFarsi(uint8* s1, uint8* outStr, uint8 size, uint8 align)
{
    sprintf(outStr, "%c%c%c%s", size, align, FARSI, s1);
}

/**
 * Justify  Two String Farsi.
 * @param   s1 [input]: first String
 * @param   s2 [input]: second String
 * @param   outStr [output]: is output
 * @param   size [input]: size of string
 * @return  outStr.
 */
void justifyTwoStringFarsi(uint8* s1, uint8* s2, uint8* outStr, uint8 size)
{
    int     spaceCount          = 0;
    uint8   spaceCountStr[50]   = {0};

    spaceCount = DISPLAY_CHARACTER_COUNT - (strlen(s1) + strlen(s2)) - 1; // -1 is for ':'
    if (spaceCount > 0)
    	memset(spaceCountStr, ' ', spaceCount);

//HNO
#ifndef CASTLES_V5S
    sprintf(outStr, "%c%c%c%s%s:%s", size, ALIGN_LEFT, FARSI, s2, s1, spaceCountStr); //MRF_IDENT
#else
    sprintf(outStr, "%c%c%c%s:%s", size, ALIGN_RIGHT, FARSI, s2,s1);
#endif   
    
}


/**
 * Show information of bill payment.
 * @param  billSpec [input]: is structure
 * @param  billCount [input]: bill count
 * @see    clearDisplay().
 * @see    removePadLeft().
 * @see    makeValueWithCommaStr().
 * @see    displayScrollableWithHeader().
 * @return TRUE or FALSE.
 */
uint8 displayBillPaymentInfo(billSpecST* billSpec, uint8* billCount)
{
    uint8       lines[10][32]           = {0};                  /* display lines, bill information */
    uint8       key                     = 0;                    /* entered key (terminal keypad) */
    uint8       billTitle[35]           = {0};                  /* bill type title */
    uint8       amountWithComma[13]     = {0};
    uint8       tempBillID[13 + 1]      = {0};
    uint8       tempPaymentID[13 + 1]   = {0};
    uint8       typeBill                = 0;
    uint16      i                       = 0;
    terminalSpecST	terminalCapability  = getTerminalCapability();
    
    
    clearDisplay();
    
    strcpy(tempBillID, billSpec[(*billCount)].billID);
    
    strcpy(tempPaymentID, billSpec[(*billCount)].paymentID);
    
    switch (billSpec[(*billCount)].type)
    {
        case 1:
            strcpy(billTitle, "��� ��");
            typeBill = waterBill;
            break;
        case 2:
            strcpy(billTitle, "��� ���");
            typeBill = electricBill;
            break;
        case 3:
            strcpy(billTitle, "��� ���");
            typeBill = gasBill;
            break;
        case 4:
            strcpy(billTitle, "��� ����");
            typeBill = phoneBill;
            break;
        case 5:
            strcpy(billTitle, "��� ���� �����");
            typeBill = cellPhone;
            break;
        case 6:
            strcpy(billTitle, "��� �������");
            typeBill = municipalBills;
            break;
        case 8://MRF_NEW16
            strcpy(billTitle, "��� ������");
            typeBill = tax;
            break;
        case 9://MRF_NEW16
//        	strcpy(billTitle, "��� ����� ��������/����ϐ�");
            strcpy(billTitle, "����� ��������/����ϐ�");
            typeBill = penalty;
            break;
        default:
            strcpy(billTitle, "��� ������");
            typeBill = unknownBill;
            break;
    }
    
    if (!terminalCapability.graphicCapability) //++MRF_IDENT
        /* BILL INFORMATION, LINE 0 : BILL TYPE TITLE */
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_CENTER, FARSI, billTitle);

    /* BILL INFORMATION, LINE 1 : SHENASEH GHABZ HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ���");
    
    /* BILL INFORMATION, LINE 2 : SHENASEH GHABZ VALUE */
    removePadLeft(tempBillID, '0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempBillID);
   
    /* BILL INFORMATION, LINE 3 : SHENASEH PARDAKHT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����� ������");
    
    /* BILL INFORMATION, LINE 4 : SHENASEH PARDAKHT VALUE */
    removePadLeft(tempPaymentID,'0');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, tempPaymentID);
    
    /* BILL INFORMATION, LINE 5 : AMOUNT HEADER */
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, "����");
    
    /* BILL INFORMATION, LINE 6 : AMOUNT VALUE */
    makeValueWithCommaStr(billSpec[(*billCount)].billAmount, amountWithComma);
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, amountWithComma, " ����");
    
    if (!terminalCapability.graphicCapability) //++MRF_IDENT
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, TRUE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, TRUE , typeBill);
    
    if (key == KBD_ENTER || key == KBD_CONFIRM) 
    {
        (*billCount)++;
        return TRUE;
    }
    else if (key == KBD_CANCEL || key == KBD_REJECT || key == KBD_TIMEOUT)//HNO_ADD kbd-timeout
    {
	    return FALSE;
    }
}


//Added by MRF
/**
 * Show information in scrollable page whith header. header just shown in V5S.
 * @param title [input]: One string as title
 * @param lines [input]: include information
 * @param linesCount [input]: number of lines
 * @param timeout [input]: timeout
 * @param confirmable [input]: TRUE or FALSE
 * @param header [input]: Name of page
 * @see   clearDisplay().
 * @see   clearKeyBuffer().
 * @see   displayforeColor().
 * @see   displayPic().
 * @see   displayInvertString().
 * @see   displayMessageBox().
 * @see   oneLineClear().
 * @see   displayString().
 * @see   cleanButtonUp().
 * @see   getKey().
 * @return pressed key.
 */
uint8 displayScrollableWithHeader(uint8* title, uint8 lines[][32], uint16 linesCount, uint32 timeout, uint8 confirmable,uint8 header) 
{

    int		counter         	= 0;            /* loop counter */
    uint8	pressedKey      	= 0;            /* pressed key */
    uint8	keyPressed      	= FALSE;        /* is any key pressed */
    int		index           	= 0;            /* lines index */
    uint8	first           	= TRUE;         /* first key press */
    int		lastIndex       	= 0;
    uint8   validKeys[8 + 1]	= {KBD_CANCEL, KBD_ENTER, KBD_UP, KBD_DOWN, KBD_PAGE_UP, KBD_PAGE_DOWN, KBD_CONFIRM, KBD_REJECT};
    uint8	key 				= KBD_CANCEL; 


    if (linesCount == 0) 
        return KBD_CANCEL;
    
    clearDisplay();
    
    clearKeyBuffer();
    displayforeColor(BLACK);
    pressedKey = 0;

    #ifdef CASTLES_V5S
        displayPic(header);
    #else
        displayInvertString(title, DISPLAY_START_LINE);
        displayPic(reportPic);
    #endif
         
    while (TRUE) 
    {
        lastIndex = index;
        keyPressed = FALSE;
        
        if (pressedKey == KBD_ENTER || pressedKey == KBD_CANCEL)
        {
            if (!confirmable || (linesCount <= index + 3 + EMPTY_LINE))
            {
            	if (pressedKey == KBD_CANCEL)
            	{
            		key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
                    { 
                        displayPic(header);
                        displayforeColor(BLACK);
        				lastIndex = -1; //refresh display screen 
                    }
            	}
            	else
            		return pressedKey;
            }
        }
        
        if (pressedKey == KBD_CONFIRM || pressedKey == KBD_REJECT) 
        { 
            if ((linesCount <= index + 3 + EMPTY_LINE))
        	{
        		if (pressedKey == KBD_REJECT)
        		{
        			key = displayMessageBox("��� ���� �� ��� ������ ����Ͽ", MSG_CONFIRMATION);
        			if (key == KBD_ENTER || key == KBD_CONFIRM)
        				return pressedKey;
        			else
        				lastIndex = -1; //refresh display screen
        		}
        		else
        			return pressedKey;
        	}
        }
                
        if (pressedKey == KBD_UP && index > 0) 
        {
            
            #ifdef CASTLES_V5S
                if (index == linesCount - 3 - EMPTY_LINE)
                    displayBleach();
            #endif
            index--;
        }
        if (pressedKey == KBD_DOWN && index <= (linesCount - 4 - EMPTY_LINE)) 
            index++; 
        
        if (pressedKey == KBD_PAGE_UP && index > 0)
        {
            index -= 4;
            if (index < 0) 
                index = 0;
        }

        if (pressedKey == KBD_PAGE_DOWN)
        {
            index += 4;
            if (index + 3 + EMPTY_LINE > linesCount)
                index = linesCount - 3 - EMPTY_LINE;
        }

        if (first || index != lastIndex) 
        {
            #ifndef CASTLES_V5S
                oneLineClear(DISPLAY_START_LINE + 1);
            #endif
            oneLineClear(DISPLAY_START_LINE + 2);
            oneLineClear(DISPLAY_START_LINE + 3);
            
            #ifdef CASTLES_V5S
                oneLineClear(DISPLAY_START_LINE + 4);
                oneLineClear(DISPLAY_START_LINE + 5);
            #endif

            for (counter = 0; counter < 4; counter++)
            {
                if ((counter + index) < linesCount)
                {
                    if ((lines[counter + index][0] == SMALL_INVERT_STRING) 
                            || (lines[counter + index][0] == NORMAL_INVERT_STRING)
                            || (lines[counter + index][0] == BIG_INVERT_STRING))
                    {
                        #ifdef CASTLES_V5S
                                displayforeColor(RED);
                                displayString(lines[counter + index ], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                                displayforeColor(BLACK);
                        #else
                                displayInvertString(lines[counter + index], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                        #endif
                    }
                    else
                        displayString(lines[counter + index], counter + DISPLAY_START_LINE + 1 + EMPTY_LINE);
                }
            }
        }

        if (linesCount <= index + 3 + EMPTY_LINE)
        {
            displayPic(confirmPic);
            displayPic(cancelPic);
        }
        else 
        {
	        if (index != 0)
	            displayPic(upPic);
	        if (index != linesCount - 3)
	            displayPic(downPic);  
             if (index == 0)
                cleanButtonUp();
        }
        
        first = FALSE;
        
        getKey(&pressedKey, &keyPressed, validKeys, timeout);
        if (!keyPressed)
        {
            confirmable = FALSE; 
            pressedKey = KBD_TIMEOUT;
            return pressedKey;
        }
    } 
}


uint8 displayLoanPay( cardSpecST* cardSpec, loanPayST* loanPay)  
{
    uint8           key                 = KBD_CANCEL;           /** entered key (terminal keypad) */
    uint8           valueWithComma[20]  = {0};
    uint8           lines[20][32]       = {0};                  /** display information */
    uint8           displayStr[50]      = {0};
    uint16          i                   = 0;
    uint8			loop				= 0;
    uint8 			separatedStr[5][50]	= {0, 0};
    uint8			wordWrapIndex		= 0;
    int             length              = 0;
    terminalSpecST  terminalCapability	= getTerminalCapability();
    
    memset(lines, 0, sizeof(lines));
    clearDisplay();
    
    #ifndef CASTLES_V5S
	justifyOneStringFarsi("������� ���� ���", lines[i++], PRN_NORM, ALIGN_RIGHT);
    #endif

      //HNO
//from card:
//    justifyTwoStringFarsi("", "�� ����", lines[i++], PRN_NORM);
//    maskCardId(cardSpec->PAN, maskedPAN);
//    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, maskedPAN);
    
//to card:  
//    justifyTwoStringFarsi("", "�� ����", lines[i++], PRN_NORM);
//    maskCardId(loanPay->destinationCardPAN, maskedPAN);
//    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, maskedPAN);

    //show name 
    justifyOneStringFarsi("���:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    removePad(loanPay->destinationCardHolderName, ' ');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, loanPay->destinationCardHolderName); //mgh
    
//    memset(displayStr , 0, sizeof(displayStr));
//    sprintf(displayStr, "%s", loanPay->destinationCardHolderName); 
//    length = strlen(displayStr);
//    loop = wordWrapFarsi(displayStr, separatedStr, DISPLAY_CHARACTER_COUNT, length); // wrapping the string
//    for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
//    {
//    	sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, separatedStr[wordWrapIndex]);
//    }
    
    //show family
    justifyOneStringFarsi("��� �����ϐ�:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    removePad(loanPay->destinationCardHolderFamily, ' ');
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, loanPay->destinationCardHolderFamily); //mgh
    
//    memset(displayStr , 0, sizeof(displayStr));
//    sprintf(displayStr, "%s %s", loanPay->destinationCardHolderName, loanPay->destinationCardHolderFamily);
// 
//    length = strlen(displayStr);
//    loop = wordWrapFarsi(displayStr, separatedStr, DISPLAY_CHARACTER_COUNT, length); // wrapping the string
//    for (wordWrapIndex = 0; wordWrapIndex <= loop; wordWrapIndex++)
//    {
//    	sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_RIGHT, FARSI, separatedStr[wordWrapIndex]);
//    }

    //show real amount of loan pay
    justifyOneStringFarsi("���� ���:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    memset(valueWithComma, 0, sizeof(valueWithComma));
    makeValueWithCommaStr(loanPay->realAmount, valueWithComma); 
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, "����");
    
    //show insert amount by customer
    justifyOneStringFarsi("���� ���� ���:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    memset(valueWithComma, 0, sizeof(valueWithComma));
    makeValueWithCommaStr(loanPay->payAmount, valueWithComma); //HNO_IDENT
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, "����");
    
#if defined (ICT250) || defined(IWL220)
    key = displayScrollable("", lines, i, 4 * DISPLAY_TIMEOUT, TRUE);
#else
    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, TRUE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, TRUE, loanInfo);//MRF_NEW8
#endif
    
    return key;
}


//MRF_ETC
uint8 displayETCInfo(ETCST* ETC , uint8 checked)  
{
    uint8           key                 = KBD_CANCEL;           /** entered key (terminal keypad) */
    uint8           lines[20][32]       = {0};                  /** display information */
    uint16          i                   = 0;
    uint8           valueWithComma[25]  = {0};//ABS:CHANGE:961207
    terminalSpecST  terminalCapability	= getTerminalCapability();
    
    memset(lines, 0, sizeof(lines));
    clearDisplay();
    
    #ifndef CASTLES_V5S
	justifyOneStringFarsi("������� �э��", lines[i++], PRN_NORM, ALIGN_CENTER);//ABS:CHANGE delete avarez va change right to center
    #endif

    //show serial of ETC
    justifyOneStringFarsi("����� �э��:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, ETC->serialETC);
    
    if (checked)
    {
        //show name 
        justifyOneStringFarsi("���:", lines[i++], PRN_NORM, ALIGN_RIGHT);
        removePad(ETC->cardHolderName, ' ');
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, ETC->cardHolderName);

        //show family
        justifyOneStringFarsi("��� �����ϐ�:", lines[i++], PRN_NORM, ALIGN_RIGHT);
        removePad(ETC->cardHolderFamily, ' ');
        sprintf(lines[i++], "%c%c%c%s", PRN_NORM, ALIGN_LEFT, FARSI, ETC->cardHolderFamily);
    }
    
    //show amount 
    justifyOneStringFarsi("����:", lines[i++], PRN_NORM, ALIGN_RIGHT);
    removePadLeft(ETC->amount, '0');
    makeValueWithCommaStr(ETC->amount, valueWithComma); 
    sprintf(lines[i++], "%c%c%c%s%s", PRN_NORM, ALIGN_LEFT, FARSI, valueWithComma, "����");

    if (!terminalCapability.graphicCapability)
    	key = displayScrollable("", lines, i, DISPLAY_TIMEOUT, TRUE);
    else
    	key = displayScrollableWithHeader("", lines, i, DISPLAY_TIMEOUT, TRUE, ETCInfo); //MRF_NEW8
    
    return key;
}

//MRF_NEW6
void LEDBlink(uint8 loop)
{
    int8 i  = 0;
    
    for (i = 0; i <= loop; i++)
        {
            displayRewardPage();

            warningBeep();
            setLED(LED_GREEN, TRUE);
            delay(500, ALL_POS);

            setLED(LED_GREEN, FALSE);
            warningBeep();

            setLED(LED_ORANG, TRUE);
            delay(500, ALL_POS);
            setLED(LED_ORANG, FALSE);

            warningBeep();
            setLED(LED_RED, TRUE);
            delay(500, ALL_POS);
            setLED(LED_RED, FALSE);

        }
        delay(800, ALL_POS);
}

