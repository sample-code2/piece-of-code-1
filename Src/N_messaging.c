#include <string.h> 
#include "N_common.h"
#include "dl_iso8583.h"
#include "dl_iso8583_common.h"
#include "dl_iso8583_defs_1987.h"
#include "N_utility.h" 
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_dateTime.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_messaging.h"
#include "N_keyManagerWrapper.h"
#include "N_keyManager.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
#include "N_transactions.h"
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h" 
#include "N_initialize.h"
#include "N_log.h" 
#include "N_display.h"
#include "N_printer.h"
#include "N_printerWrapper.h"
#include "N_getUserInput.h"

#define AMOUNT_MAX_LEN                     12

#define IRANCELL_PIN_MAX_LEN               20
#define IRANCELL_SERIAL_MAX_LEN            20

#define IRANCELL_PRODUCT_CODE_1            "IRNCEL.0027"
#define IRANCELL_PRODUCT_CODE_2            "IRNCEL.0028"    
#define IRANCELL_PRODUCT_CODE_5            "IRNCEL.0029"    
#define IRANCELL_PRODUCT_CODE_10           "IRNCEL.0030"  
#define IRANCELL_PRODUCT_CODE_20           "IRNCEL.0031"   

#define MCI_PIN_MAX_LEN                    20
#define MCI_SERIAL_MAX_LEN                 20

#define MCI_PRODUCT_CODE_1                 "HAMRAH.0000"
#define MCI_PRODUCT_CODE_2                 "HAMRAH.0023"
#define MCI_PRODUCT_CODE_5                 "HAMRAH.0022"
#define MCI_PRODUCT_CODE_10                "HAMRAH.0007"

#define HAMRAHEAVAL                        "919"
#define IRANCELL                           "936"
#define RIGHTEL                            "921"

#define MTI_BALANCE_REQ_87                 "0200"
#define MTI_BUY_REQ_87                     "0200"
#define MTI_BILL_PAY_REQ_87                "0200"
#define MTI_BUY_CHARGE_REQ_87              "0200"
#define MTI_LOAN_PAY_REQ_87                "0200"
#define MTI_ETC_REQ_87                     "0200"
#define MTI_TOPUP_REQ_87                   "0200" //MRF_TOPUP
#define MTI_CHARITY_REQ_87                 "0200"
#define MTI_INITIALIZE_REQ_87              "0800"
#define MTI_LOGON_REQ_87                   "0800"
#define MTI_ROLL_REQ_87                    "0800"
#define MTI_SETTLEMENT_REQ_87              "0220"
#define MTI_REVERSAL_REQ_87                "0400"

    
#define MTI_BALANCE_RES_87                 "0110"
#define MTI_BUY_RES_87                     "0210"
#define MTI_BILL_PAY_RES_87                "0210"
#define MTI_BUY_CHARGE_RES_87              "0210"
#define MTI_ETC_RES_87                     "0210"
#define MTI_TOPUP_RES_87                   "0210"  //MRF_TOPUP
#define MTI_INITIALIZE_RES_87              "0810"
#define MTI_LOGON_RES_87                   "0810"
#define MTI_SETTLEMENT_RES_87              "0230"
#define MTI_REVERSAL_RES_87                "0410"


static  uint16  messageHeaderLen87              = 0;
static  int     field24                         = 0;
static  uint8   voucherKeyDecryptor[16 + 1]     = {0};
static  uint8   voucherData[999 + 1]            = {0};

uint32 getMessageHeaderLen87(void);
int createMessageHeader87(uint8* buffer, uint32 tpduNii);
int getField24(void);
void resetField24(void);
uint8* getVoucherKeyDecryptor(void);
void resetVoucherKeyDecryptor(void);



void createCommonPartMessage87(DL_ISO8583_MSG* isoMsg, messageSpecST* messageSpec)
{
    uint8               stanStr[7]              = {0};                          /** stan string */
//    int8                NIIstr[4]               = {0};                          /** message NII */
    int                 counter                 = 0;                            /** loop counter */
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len;
    uint8               tempCardTrack2[37 + 1]  = {0};
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    uint8               RRN[12]                 = {0};
    uint8               versionKey[16 + 1]      = {0};
    uint8               POSType					= 0; 
    
   
    // field 3
    if(messageSpec->transType == TRANS_BUY)
    {
        DL_ISO8583_MSG_SetField_Str(0, MTI_BUY_REQ_87, isoMsg);//ABS:CHANGE:961207
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY, isoMsg);//ABS:CHANGE:961207
    }
    else if(messageSpec->transType == TRANS_BALANCE)
    {
        DL_ISO8583_MSG_SetField_Str(0, MTI_BALANCE_REQ_87, isoMsg);//ABS:CHANGE:961207
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BALANCE, isoMsg);//ABS:CHANGE:961207
    }
    else if(messageSpec->transType == TRANS_BUYCHARGE)
    {
        DL_ISO8583_MSG_SetField_Str(0, MTI_BUY_CHARGE_REQ_87, isoMsg);//ABS:CHANGE:961207
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY_CHARGE, isoMsg);//ABS:CHANGE:961207
    }
    else if(messageSpec->transType == TRANS_BILLPAY)
    {
        DL_ISO8583_MSG_SetField_Str(0, MTI_BILL_PAY_REQ_87, isoMsg);//ABS:CHANGE:961207
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BILLPAY, isoMsg);//ABS:CHANGE:961207
    }
    
    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, isoMsg);//ABS:CHANGE:961207
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, isoMsg);//ABS:CHANGE:961207
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, isoMsg);//ABS:CHANGE:961207

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], isoMsg);//ABS:CHANGE:961207
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    tempCardTrack2[counter] = 0;
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, isoMsg);//ABS:CHANGE:961207

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, isoMsg);//ABS:CHANGE:961207
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, isoMsg);//ABS:CHANGE:961207
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, isoMsg);//ABS:CHANGE:961207

    // field 52
    createPin87(messageSpec, pinHex, pinBin);              
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, isoMsg);//ABS:CHANGE:961207
        
    // field 53
    getPOSTypeConnection(&POSType);
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, isoMsg);//ABS:CHANGE:961207
        
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", isoMsg);
}


int parseCommonPartResponse87(DL_ISO8583_MSG* isoMsg, messageSpecST* messageSpec) 
{
    int8                fieldData[7]		= {0};                              /** field data */
    uint8               date[9]             = {0};                              /** temp date str */
    int                 i                   = 0;
    
    // field 12
    if (!DL_ISO8583_MSG_HaveField(12, isoMsg)) 
        return PARSE_ERROR_INVALID_BITMAP;
    else if (isoMsg->field[12].len == 6)
    {
        memset(fieldData, 0, sizeof(fieldData));
        strncpy(fieldData, (int8*)isoMsg->field[12].ptr, 6);
        messageSpec->dateTime.time = strToTime(fieldData);
    }

    // field 13
    if (!DL_ISO8583_MSG_HaveField(13, isoMsg)) 
        return PARSE_ERROR_INVALID_BITMAP;
    else if (isoMsg->field[13].len == 4)
    {
        memset(fieldData, 0, sizeof(fieldData));
        strncpy(fieldData, (int8*)isoMsg->field[13].ptr, 4);	
        DateToStr(messageSpec->dateTime.date , date);
        
        for (i = 4; i < 8; i++) 
            date[i] = fieldData[i - 4];
        
        date[8] = 0;
    }

    // field 37
    if (DL_ISO8583_MSG_HaveField(37, isoMsg) && (isoMsg->field[37].len == 12)) 
    {
        memset(messageSpec->retrievalReferenceNumber, 0, sizeof(messageSpec->retrievalReferenceNumber));
        strncpy(messageSpec->retrievalReferenceNumber, (int8*)isoMsg->field[37].ptr, 12);
    }
    else 
        return PARSE_ERROR_INVALID_BITMAP;
    
    // field 59
    if (DL_ISO8583_MSG_HaveField(59, isoMsg)) 
        if(isoMsg->field[59].len == 4)
        {
            memset(fieldData, 0, sizeof(fieldData));
            strncpy(fieldData, (int8*)isoMsg->field[59].ptr, 4);	

            for (i = 0; i < 4; i++) 
                date[i] = fieldData[i];
        }
    
    messageSpec->dateTime.date = strToDate(date);
    
    return PARSE_NO_ERROR;
}

int createBuyMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             			/** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 			/** iso message */    
    int                 messageLen              			= 0;                            /** final message lenght */
    int                 MessageHeaderSize       			= 0;
    uint16              packedSize              			= 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]   = {0};
    uint8               stanStr[7]              			= {0};                          /** stan string */
    int8                currentdate[9]          			= {0};                          /** current date, string form */
    int8                currenttime[7]          			= {0};                          /** current time, string form */
    int                 counter                 			= 0;                            /** loop counter */
    int                 cardTrack2Len           			= (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1]  			= {0};
    uint8               pinHex[16]              			= {0};                          /** hex form of pin */
    uint8               pinBin[8]               			= {0};                          /** binary form of pin */
    uint8               RRN[12]                 			= {0};
    uint8               versionKey[16 + 1]      			= {0}; 
    uint8               Serial[16]              			= {0};
    uint8               serialInfo[200]          			= {0};
    uint8               len[3]                  			= {0};
    uint8               versionNum[15]          			= {0};
    uint8               encryptedData[80 + 1]   			= {0}; 
    uint8               year[4 + 1]             			= {0};
    uint8               POSType								= 0; 
    uint8               AddData[99 + 1]         			= {0};
    uint8               IMSI[30]            				= {0};
    uint8				shiftData[30]						= {0};//+HNO,ABS_971128
    uint8				additionalMsg[99 + 1]				= {0};//+HNO,ABS_971128
    uint8           	activeUserName[16]  				= {0};//+HNO,ABS_971128
    uint8				data63Len[3]						= {0};//+HNO,ABS_971128
    int                 len63								= 0;//+HNO_971222
	uint8				data48Len[3]						= { 0 };
	uint8				data48[999 + 1]						= { 0 };
    terminalSpecST      terminalCapability      			= getTerminalCapability();
    serviceSpecST		service								= getPOSService();//+HNO,ABS_971128


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create BUY Message 87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_BUY_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);
   
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    
    tempCardTrack2[counter] = 0;
    
#ifdef ENCRYPTED_PAN    
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData); 
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);  
#endif   
    
    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

	if (strcmp(messageSpec->shebaField, ""))
	{

	//field 48
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "messageSpec->shebaField: %s", messageSpec->shebaField);

	sprintf(data48Len, "%04d", strlen(messageSpec->shebaField));
	asciiToBCD(data48Len, data48Len, strlen(data48Len), 0, 0);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data48Len: %s", data48Len);
	strcpy(data48, data48Len);
	//showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data48: %s", data48);
	strcat(data48, messageSpec->shebaField);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data48: %s", data48);
	DL_ISO8583_MSG_SetField_Str(48, data48, &isoMsg);
	}


    // field 52
    createPin87(messageSpec, pinHex, pinBin); 
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
    
    // field 53
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);
        
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);
    
	// field 60			//++ABS_971128
	if (getPOSService().depositID)
	{
		DL_ISO8583_MSG_SetField_Str(60, messageSpec->depositID, &isoMsg);
	}
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    
    //..HNO_971215
    strcpy(AddData,"P13");
    if ((terminalCapability.GPRSCapability == TRUE) && (service.shiftService == FALSE))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    }
    
	else if ((terminalCapability.GPRSCapability == FALSE) && (service.shiftService == TRUE))
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "not GPRS & shiftService");
		getActiveUser(activeUserName);
		strcpy(shiftData, "{Shift:");
		if (strcmp("����", activeUserName) == 0)		
			convertCp1256ToUtf8(activeUserName, "����");//..HNO_980304		
		strcat(shiftData, activeUserName);
		strcat(shiftData,"}");
		strcpy(additionalMsg, shiftData);
		sprintf(data63Len, "%03d",  strlen(shiftData));
    }

    else if((terminalCapability.GPRSCapability == TRUE) && (service.shiftService == TRUE))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & shiftService");
        getActiveUser(activeUserName);
        strcpy(additionalMsg,"{IMSI:");
        strcat(additionalMsg, messageSpec->IMSI);
        strcat(additionalMsg,",Shift:");
		if (strcmp("����", activeUserName) == 0)
			convertCp1256ToUtf8(activeUserName, "����");
		strcat(additionalMsg , activeUserName);
		strcat(additionalMsg,"}");
		sprintf(data63Len, "%03d",  strlen(additionalMsg));
    }
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

	strcat(AddData,data63Len);
	strcat(AddData, additionalMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "AddData: %s", AddData);
	len63 = atoi(data63Len);//+HNO_971222
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "len63: %d", len63);
	if(len63 != 0)
		strcat(serialInfo, AddData);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
    
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg); 
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return FALSE;
          
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
    
    messageLen = MessageHeaderSize + packedSize;

    return messageLen;

}

//int createBuyChargeOfflineMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
//{
//    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
//    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
//    int                 messageLen              = 0;                            /** final message lenght */
//    int                 MessageHeaderSize       = 0;
//    uint16              packedSize              = 0;
//    uint8               staticBuffer[2048]      = {0};
////    uint8               valueStr[13]            = {0};
//    uint8               stanStr[7]              = {0};                          /** stan string */
//    int8                currentdate[9]          = {0};                          /** current date, string form */
//    int8                currentyear[4]          = {0};
//    int8                currenttime[7]          = {0};                          /** current time, string form */
//    int8                NIIstr[4]               = {0};                          /** message NII */
//    int                 counter                 = 0;                            /** loop counter */
//    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
//    uint8               tempCardTrack2[37 + 1]  = {0};
//    uint8               pinHex[16]              = {0};                          /** hex form of pin */
//    uint8               pinBin[8]               = {0};                          /** binary form of pin */
//    uint8               RRN[12]                 = {0};
//    uint8               serialInfo[2048]        = {0};
//    uint8               versionKey[16 + 1]      = {0};
//    int                 parseResponseResult     = -1;
//    uint8               response[2048]          = {0}; 				/** response message */
//    uint8               operator[3 + 1]         = {0};
//    int                 count                   = 0;
//    uint8               amount[12 + 1]          = {0};
//    int                 lenOperator             = 0;
//    int                 lenCount                = 0;
//    int                 lenAmount               = 0;
//    uint8               message[2048]           = {0};
//    uint8               strCount[2 + 1]         = {0};
//    int                 i                       = 0;
//    uint8               Serial[16]              = {0};
//    uint8               len[3]                  = {0};
//    uint16              responseStatus          = 0;
//    uint8               field60Data[5 + 1]      = {0};
//    int                 field24Data             = 0;
//    int                 intField24              = 0;
//    uint8               versionNum[15]          = {0}; 
//    uint8               encryptedData[80 + 1]   = {0}; 
//    uint8               year[4 + 1]             = {0};
//    
//    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
//    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
//    
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "charge BUY Message 87");
//    /** message header size */
//    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    
//
//    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
//    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
//    
//    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
//    
//    // field 0
//    DL_ISO8583_MSG_SetField_Str(0, MTI_BUY_CHARGE_REQ_87, &isoMsg);
//
//    // field 3
//    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY_CHARGE, &isoMsg);
//
//    // field 4 
//    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);
//    
//    // field 11
//    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
//    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
//    
//    // field 12
//    timeToStr(messageSpec->dateTime.time , currenttime);
//    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);
//
//    // field 13
//    DateToStr(messageSpec->dateTime.date , currentdate);
//    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
//    
//    // field 35
//    for (counter = 0 ; counter < cardTrack2Len ; counter++)
//    {
//        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
//        if(counter == messageSpec->cardSpec.PANLen)
//            tempCardTrack2[counter] = 'D';
//    }
//    
//    tempCardTrack2[counter] = 0;
//    
//
//#ifdef ENCRYPTED_PAN   
//    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData); //MRF
//    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg); //mgh_950312
//#else
//    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
//#endif
//
//    // field 37
//    sprintf(RRN, "%s%s", currenttime, stanStr);
//    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
//    
//    // field 41
//    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
//    
//    // field 42
//    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);
//
//    // field 52
//    createPin87(messageSpec, pinHex, pinBin);   
//    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
//        
//    // field 53 
//    asciiToBCD(POS_TYPE, versionKey, strlen(POS_TYPE), 0, 0);
//    strcat (versionKey,messageSpec->versionKey.pinVersion);
//    strcat (versionKey,"16");
//    strcat (versionKey,messageSpec->versionKey.macVersion);
//    strcat (versionKey,"1600000");
//
//    DL_ISO8583_MSG_SetField_Str(53,versionKey, &isoMsg);
//      
//    // field 59
//    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));//MRF_0330
//    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
//       
//    // field 60
//    responseStatus = messageSpec->responseStatus;
//    if (responseStatus == PARSE_REPEAT_BUY_CHARGE_TRANS)
//    {
//        intField24 = atoi("00001");
//        field24Data = getField24();
//        intField24  = intField24 + field24Data;
//        sprintf(field60Data,"%05d", intField24);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "intField24:%d", intField24);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field60Data:%s", field60Data);
//    }
//    else
//        strcpy(field60Data,"00001");
//        
//    DL_ISO8583_MSG_SetField_Str(60, field60Data, &isoMsg);
//    
//    // field 63
//    getTerminalSerialNumber(Serial); 
//    removePadLeft(Serial, '0');
//    getVersion(versionNum);
//    padLeft(versionNum,5);
//    sprintf(serialInfo ,"%s" ,versionNum);
//    sprintf(len, "%02d", strlen(Serial));
//    strcat(serialInfo, len);
//    strcat(serialInfo, Serial);
//
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**********SET FIELD63 OF BUY CHARGE***************");
//    
//    for(i = 0; i <= OPERATOR_COUNT; i++)
//    {
//        if (messageSpec->buyChargeOfflineSpec[i].operatorId[0] == NULL)
//             continue;
//        
//        strcpy(operator, messageSpec->buyChargeOfflineSpec[i].operatorId);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "operatorId:%s", operator);
//
//        strcpy(amount, messageSpec->buyChargeOfflineSpec[i].amount);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Amount:%s", amount);
//
//        lenOperator = strlen(operator);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenOperator: %02d", lenOperator);
//
//        lenAmount = strlen(amount);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenAmount: %02d", lenAmount);
//
//        count = messageSpec->buyChargeOfflineSpec[i].productTypeCount;
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Count: %02d", count);
//        sprintf(strCount,"%02d",count);
//        lenCount = strlen(strCount);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenCount: %02d", lenCount);
//
//        sprintf(message, "%02d%s%02d%02d%02d%s", lenOperator, messageSpec->buyChargeOfflineSpec[i].operatorId, lenCount, count, lenAmount, amount);
//        strcat(serialInfo, message);
//        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "message: %s", message);
//    }
//    
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Field63 Total :%s", serialInfo);
//
//    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
//    
//    // field 64
//    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);
//
//    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
//    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
//    DL_ISO8583_MSG_Free(&isoMsg);
//
//    if (packedSize == 0)
//        return 0;
//          
//    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
//    
//    messageLen = MessageHeaderSize + packedSize;
//
//    return messageLen;
//}
    

/**
 * parse buy response message.
 * @param   buffer buy response message.
 * @param   bufferLen response len.
 * @param   messageSpec message specifications.
 * @return  error code
 */
int parseBuyResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
   	DL_ISO8583_HANDLER	isoHandler;                                         				/** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                             				/** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               headerLen           				= getMessageHeaderLen87();      /** message index, without message header */
    uint8               field63[999 + 1]    				= {0};          				/** field 62 */
    int                 field63Len          				= 0;
    uint8               pcValue[3]          				= {0};
    int                 rewardLen           				= 0;
	int                 taxLen = 0;
    uint8               temp[4]             				={0};
    int                 i                   				= 0;
	uint8               taxValue[4] = { 0 };

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**Parse Buy Response 87");
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);
    
    if (messageSpec->transType == TRANS_BUY)
    {
        //field 63
        if (DL_ISO8583_MSG_HaveField(63, &isoMsg))
        {
            memset(field63,0,sizeof(field63));
            field63Len = (int)isoMsg.field[63].len;//ABS
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field63Len: %d", field63Len);
            strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
        }

		//*******************+ABD_980927
		for (i = 0; i < field63Len; i)
		{
			strncpy(taxValue, &field63[i], 3);
			taxValue[3] = 0;
			i += 3;
			if (strcmp(taxValue, "TAX") == 0)
			{
				strncpy(temp, &field63[i], 3);
				taxLen = atoi(temp);
				showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "taxLen:%d", taxLen);
				i += 3;
				strncpy(messageSpec->taxNumber, &field63[i], taxLen);
				showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "taxNumber:%s", messageSpec->taxNumber);
			}
		}



        for (i = 0; i < field63Len; i)
        {
            strncpy(pcValue, &field63[i], 3);
            pcValue[3] = 0;
            i += 3;
            if(strcmp(pcValue , "P12") == 0)
            { 
                strncpy(temp,&field63[i],3);
                rewardLen = atoi(temp); 
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "rewardLen: %d", rewardLen);
                i += 3;
                strncpy(messageSpec->reward, &field63[i],rewardLen);
				showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "reward:%s", messageSpec->reward);
            }
        }



    }

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}




int parseBuyChargeResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               staticBuffer[2048]          ={0};
    uint8               headerLen                   = getMessageHeaderLen87();  /** message index, without message header */
    int                 retVal                      = PARSE_NO_ERROR;
    int                 field24Len                  = 0;
    uint8               field24Data[3 + 1]          = {0};
    uint8               amount[12 + 1]              = {0};
    int                 amountlen                   = 0;
    int                 fieldLen                    = 0;
    uint8               fieldData[2048 + 1]         = {0}; //this size is for test MRF
    
    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "******Parse Buy Charge Response 87*****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);
    
    if (retVal = parseCommonPartResponse87(&isoMsg, messageSpec) != PARSE_NO_ERROR)
        return retVal; 
    
    //field 4
    if (!DL_ISO8583_MSG_HaveField(4, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
    else
    {
        memset(amount,0,sizeof(amount));
        amountlen = (int)isoMsg.field[4].len;//ABS
        strncpy(amount, (int8*)isoMsg.field[4].ptr, amountlen);  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "amount: %s", amount);
        amount[amountlen] = 0;
    }
    
    //field 24
    if (!DL_ISO8583_MSG_HaveField(24, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
    else
    {
        field24Len = (int)isoMsg.field[24].len;//ABS
        strncpy(field24Data, (int8*)isoMsg.field[24].ptr, field24Len);  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Field 24: %s", field24Data);
        field24Data[field24Len] = 0;
    }
    
    // field 63
    if (!DL_ISO8583_MSG_HaveField(63, &isoMsg)) 
        return PARSE_ERROR_INVALID_BITMAP;
    else 
    {
        memset(fieldData,0,sizeof(fieldData));
        fieldLen = (int)isoMsg.field[63].len;//ABS
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field63Len: %d", fieldLen);
        strncpy(fieldData, (int8*)isoMsg.field[63].ptr, fieldLen);
        fieldData[fieldLen] = 0;
    }   

    if (fieldLen == 0) 
        return PARSE_ERROR_INVALID_BITMAP;
    else
    {
        /*strcat(voucherData,fieldData);*/ //for offline
        strcpy(voucherData,fieldData); //for online
    }
    
     //Checking validity of field 24
    if (strcmp(field24Data, "301")  == 0)
    {   
        field24++;
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field 24 is 301");
        return PARSE_REPEAT_BUY_CHARGE_TRANS;
    }
    else if (strcmp(field24Data, "300")  == 0)
        return PARSE_NO_ERROR;
    else
        return PARSE_ERROR_INVALID_RESPONSE;
}

int createBillPayMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             			/** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 			/** iso message */    
    int                 messageLen              			= 0;                            /** final message lenght */
    int                 MessageHeaderSize       			= 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]   = {0};
    uint8               field63[400]          				= {0}; //..HNO_971215                       /** field 63 */
    uint16              packedSize             				= 0;
    uint8               stanStr[7]             				= {0};                          /** stan string */
    int8                currentdate[9]         				= {0};                          /** current date, string form */
    int8                currenttime[7]         				= {0};                          /** current time, string form */
    int                 counter                				= 0;                            /** loop counter */
    int                 cardTrack2Len          				= (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1] 				= {0};
    uint8               pinHex[16]             				= {0};                          /** hex form of pin */
    uint8               pinBin[8]              				= {0};                          /** binary form of pin */
    uint8               RRN[12]                				= {0};   
    uint8               billIDlen[3]           				= {0};
    uint8               paymentIDlen[3]        				= {0};
    uint8               versionKey[16 + 1]     				= {0}; 
    uint8               serial[16]             				= {0};
    uint8               len[3]                 				= {0};
    uint8               versionNum[15]         				= {0};
    uint8               encryptedData[80 + 1]  				= {0};
    uint8               year[4 + 1]            				= {0};
    uint8               POSType								= 0; 
    uint8               AddData[99 + 1]        				= {0}; 
    uint8               IMSI[99 + 1]           				= {0}; 
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215
    terminalSpecST      terminalCapability     				= getTerminalCapability();


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create BillPay Message 87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_BILL_PAY_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BILLPAY, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    tempCardTrack2[counter] = 0;
    
    
#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);  
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);              
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
        
    // field 53
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");
    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);
        
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
    
    // field 63
    memset(field63, 0x00, sizeof(field63));
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(field63 ,"%s" ,versionNum);
    getTerminalSerialNumber(serial); 
    removePadLeft(serial, '0');
    sprintf(len, "%02d", strlen(serial));
    strcat(field63, len);
    strcat(field63, serial);
    
    sprintf(billIDlen, "%02d", strlen(messageSpec->billSpec.billID));
    strcat(field63, billIDlen);
    strcat(field63, messageSpec->billSpec.billID);
    sprintf(paymentIDlen, "%02d", strlen(messageSpec->billSpec.paymentID));
    strcat(field63, paymentIDlen);
    strcat(field63, messageSpec->billSpec.paymentID); 
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(field63, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serifield63alInfo: %s", field63);

    DL_ISO8583_MSG_SetField_Str(63, field63, &isoMsg);
        
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
          
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}


int parseBillPayResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER		isoHandler;                                             				/** iso message handler */
    DL_ISO8583_MSG      	isoMsg;                                                 				/** iso message */
    uint8               	staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               	headerLen           				= getMessageHeaderLen87();          /** message index, without message header */

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse BillPay Response 87****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}

int createBalanceMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{  
    DL_ISO8583_HANDLER	isoHandler;                                             			/** iso message handler */
    DL_ISO8583_MSG      isoMsg;    
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]    = {0};							 /** iso message */    
    int                 messageLen             				 = 0;                            /** final message lenght */
    int                 MessageHeaderSize      				 = 0;
    uint16              packedSize             				 = 0;
    uint8               stanStr[7]             				 = {0};                          /** stan string */
    int8                currentdate[9]         				 = {0};                          /** current date, string form */
    int8                currenttime[7]         				 = {0};                          /** current time, string form */
    int                 counter                				 = 0;                            /** loop counter */
    int                 cardTrack2Len          				 = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1] 				 = {0};
    uint8               pinHex[16]             				 = {0};                          /** hex form of pin */
    uint8               pinBin[8]              				 = {0};                          /** binary form of pin */
    uint8               RRN[12]                				 = {0};
    uint8               appVer[5]              				 = {0};
    uint8               versionKey[16 + 1]     				 = {0};
    uint8               Serial[16]             				 = {0};
    uint8               serialInfo[200]         				 = {0};
    uint8               versionNum[15]         				 = {0} ;
    uint8               len[3]                 				 = {0};
    uint8               encryptedData[80 + 1]  				 = {0}; 
    uint8               year[4 + 1]            				 = {0}; 
    uint8               POSType								 = 0; 
    uint8               AddData[99 + 1]        				 = {0}; 
    uint8               IMSI[30 + 1]           				 = {0};

    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO,ABS_971128
    uint8           	activeUserName[16]  				= {0};//+HNO,ABS_971128
    uint8				data63Len[3]						= {0};//+HNO,ABS_971128
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215
    terminalSpecST      terminalCapability     				 = getTerminalCapability();


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create Balance Message 87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_BALANCE_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BALANCE, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    tempCardTrack2[counter] = 0;

#ifdef ENCRYPTED_PAN   
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData); 
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg); 
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);  
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);             
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
        
    // field 53
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);
    
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
    
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "AddData: %s", AddData);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
    
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
    
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
    
    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}


int parseBalanceResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
	DL_ISO8583_HANDLER  isoHandler;                                             				/** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 				/** iso message */
    int                 fieldLen            				= 0;                                /** field length */
    int8                fieldData[1000]     				= {0};                              /** field data */
    int                 field54Index        				= 0;                                /** field 54 index */
    int                 tempAccountType     				= 0; 
    int                 accountType         				= -1;                               /** account type */
    uint8               firstLeger          				= TRUE;                             /** ledger info is first */ //mablaghe vagheie
    uint8               firstAvailable      				= TRUE;                             /** available info is first */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]   = {0};
    uint8               headerLen           				= getMessageHeaderLen87();          /** message index, without message header */
    int                 retVal              				= PARSE_NO_ERROR;
    
    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse Balance Response 87****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    if (retVal = parseCommonPartResponse87(&isoMsg, messageSpec) != PARSE_NO_ERROR)
        return retVal;   

    // field 54
    if (!DL_ISO8583_MSG_HaveField(54, &isoMsg)) 
        return PARSE_ERROR_INVALID_BITMAP;
    else 
    {
        fieldLen = isoMsg.field[54].len;
        strncpy(fieldData, (int8*)isoMsg.field[54].ptr, fieldLen);
        fieldData[fieldLen] = 0;
        
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fieldLen: %d", fieldLen);
        
        if (fieldLen == 0) 
        {
            sprintf(messageSpec->balanceSpec.accountBalance, "%012d", 0);
            sprintf(messageSpec->balanceSpec.withdrawalAmount, "%012d", 0);
        }
        else 
        {            
            while (field54Index < fieldLen) 
            {
                tempAccountType = zeroPadStrToInt(&fieldData[field54Index], 2);
                
                if (tempAccountType == 1 || ((firstLeger || firstAvailable) && (accountType == -1 || accountType == tempAccountType))) 
                {
                    if (memcmp("01", &fieldData[field54Index + 2], 2) != 0) 
                    {
                        strncpy(messageSpec->balanceSpec.withdrawalAmount,&fieldData[field54Index + 8], 12);
                        messageSpec->balanceSpec.withdrawalAmount[12] = 0;
                        accountType = tempAccountType;
                        firstLeger = FALSE;
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "account Amount is %s", messageSpec->balanceSpec.accountBalance);
                    }
                    else 
                    {
                        strncpy(messageSpec->balanceSpec.accountBalance, &fieldData[field54Index + 8], 12);
                        messageSpec->balanceSpec.accountBalance[12] = 0; 
                        accountType = tempAccountType;
                        firstAvailable = FALSE;
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "withdrawalAmount is %s", messageSpec->balanceSpec.withdrawalAmount);
                    }
                }
                
                field54Index += 20;
            }
        }
    }
    return PARSE_NO_ERROR;
}


uint32 getMessageHeaderLen87(void)
{
    return messageHeaderLen87;
}


int createMessageHeader87(uint8* buffer, uint32 SWINii)   
{
    uint8   NII[3 + 1]    = {0};                                                /** message TPDU NII */    
    int     headerLen     = 0;                                                  /** index in message that mac create from this index */

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create Message Header 87****");
        
    buffer[headerLen] = 0x60;
    headerLen += 1;
    
    //TPDU
    // Destination Address (2)
    sprintf(NII, "%03ld", SWINii);
    asciiToBCD(NII, &buffer[headerLen], 3, 0, 0);
    headerLen += 2;
    
    // Originator Address (2)
    buffer[headerLen] = 0x00;
    headerLen += 1;
    buffer[headerLen] = 0x02;
    headerLen += 1;
    
    messageHeaderLen87 = headerLen;
            
    return headerLen;
}


uint8 checkConnectionPrerequisites(messageSpecST* messageSpec)
{
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "SWINii:%d", messageSpec->generalConnInfo.SWINii);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "TerminalID:%s", messageSpec->merchantSpec.terminalID);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "MerchantID:%s", messageSpec->merchantSpec.merchantID);

	if( (messageSpec->generalConnInfo.SWINii == NULL) 
            || strlen(messageSpec->merchantSpec.terminalID) < 4
			|| (strlen(messageSpec->merchantSpec.merchantID)) < 4)
		return FALSE;

	return TRUE;
}

uint8 initMessageSpec(filesST* files, uint8 transType, messageSpecST* messageSpec)   
{
    int16               retValue        = FAILURE;
    uint32              len             = sizeof (generalConnInfoST);
    generalConnInfoST   generalConnInfo;
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****initiating message spec****");
    
    memset(&generalConnInfo, 0, sizeof (generalConnInfoST));

/* 
    retValue = readFileInfo(files->generalConnInfoFile, &(messageSpec->generalConnInfo), &len);
    if (retValue != SUCCESS) 
    {
        
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #4");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
*/
    
    messageSpec->generalConnInfo = getGeneralConnInfo(files->id); 
    messageSpec->dateTime = getDateTime();
    messageSpec->settlementSpec.dateTime = getDateTime();
    
    if (!readVersionKeySpec(files, &(messageSpec->versionKey)))
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ReadVersionKey NOK");
        return FALSE;
    }
    
    if (!readMerchantSpec(files, &(messageSpec->merchantSpec)))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ReadMerchant NOK");
        if (transType == TRANS_INITIALIZE || transType == TRANS_FILE_MANAGEMENT 
                || transType == TRANS_LOGON)
            return TRUE;
        else
            return FALSE;
    }

    
    if(!checkConnectionPrerequisites(messageSpec))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkConnectionPrerequisites NOK");
    	return FALSE;
    }
    
    //MRF_IDENT: JUST FOR TEST
//    memset(messageSpec->terminalKey.masterKey, 0 , sizeof(messageSpec->terminalKey.masterKey));
//    memcpy(messageSpec->terminalKey.masterKey,  MASKAN_TEST_GENERAL_KEY, 16);
        
    if (transType == TRANS_INITIALIZE 
    		|| transType == TRANS_FILE_MANAGEMENT 
            || transType == TRANS_LOGON)
        return TRUE;  
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***** Loading keys *****");
  
/*  TODO: FOR WHEN INGECT GENERAL KEY
 	if (!loadSecurityKey(files, files->masterKeyFile, files->generalKey, messageSpec->terminalKey.masterKey, 16))
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "3");
        return FALSE;
    }
*/

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***** MAC Key *****");
    if (!loadSecurityKey(files, files->macKeyFile, MASKAN_TEST_GENERAL_KEY /*mgh_93messageSpec->terminalKey.masterKey*/, messageSpec->terminalKey.macKey, 16))
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "4");
        return FALSE;
    }

#ifndef INGENICO5100 
    if ((transType != TRANS_LOGON) && (fileExist(files->pinKeyFile) == SUCCESS))
#else
	if ((transType != TRANS_LOGON) && (sramFileExist(files->pinKeyFile) == SUCCESS))
#endif
    {        
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***** PIN Key *****");
        if (!loadSecurityKey(files, files->pinKeyFile, MASKAN_TEST_GENERAL_KEY/*mgh_93 messageSpec->terminalKey.masterKey*/, messageSpec->terminalKey.pinKey, 16))
        {
            showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "5");
            return FALSE;
        }
    }
    
#ifndef INGENICO5100 
    if ((transType == TRANS_BUYCHARGE) && (fileExist(files->voucherKeyFile) == SUCCESS) )
#else
	if ((transType == TRANS_BUYCHARGE) && (sramFileExist(files->voucherKeyFile) == SUCCESS) )
#endif
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***** Voucher Key *****");
         if (!loadSecurityKey(files, files->voucherKeyFile, MASKAN_TEST_GENERAL_KEY /*mgh_93messageSpec->terminalKey.masterKey*/, messageSpec->terminalKey.voucherKey, 16))
        {
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "6");
            return FALSE;
        }
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "******MAC******");
    comportHexShow(messageSpec->terminalKey.macKey, 16);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "******PIN******");
    comportHexShow(messageSpec->terminalKey.pinKey, 16);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "******VOUCHER******");
    comportHexShow(messageSpec->terminalKey.voucherKey, 16);
   
    //MGH_MRF_HNO_COMMENT 
//    if (fileExist(files->settelmentInfoFile) == SUCCESS)
//    {
//        len = sizeof(settlementSpecST); 
//        retValue = readFileInfo(files->settelmentInfoFile, &(messageSpec->settlementSpec), &len);
//        
//        if ((retValue != SUCCESS))
//        {
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "7");
//            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
//            return FALSE;
//        }
//    }
    
     showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "InitMessageSpec Done");
    return TRUE;
}


uint8 saveMessageSpec(filesST* files, messageSpecST* messageSpec)   
{
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****save MessageSpec****");
    
    if (messageSpec->transType == TRANS_INITIALIZE 
            || messageSpec->transType == TRANS_LOGON
            	|| messageSpec->transType == TRANS_FILE_MANAGEMENT)
    { //++MRF_IDENT
        if (!writeKey(files, files->masterKeyFile, messageSpec->terminalKey.masterKey, 8))
            return FALSE;

   /* --MRF_IDENT if (messageSpec->transType == TRANS_INITIALIZE || messageSpec->transType == TRANS_LOGON)
    { */
        if (!writeKey(files, files->macKeyFile, messageSpec->terminalKey.macKey, 16))
            return FALSE;

        if (!writeKey(files, files->pinKeyFile, messageSpec->terminalKey.pinKey, 16))
            return FALSE;

        if (!writeKey(files, files->voucherKeyFile, messageSpec->terminalKey.voucherKey, 16))
            return FALSE;
        
        if (!writeVersionKeySpec(files, messageSpec->versionKey))
            return FALSE;
    }
    
    if (messageSpec->transType == TRANS_INITIALIZE || 
            messageSpec->transType == TRANS_LOGON ||
            	messageSpec->transType == TRANS_SETTLEMENT ||
            		messageSpec->transType == TRANS_FILE_MANAGEMENT)
                		if (!writeMerchantSpec(files, messageSpec->merchantSpec))
                			return FALSE;

    return TRUE;
}


uint16 setMac(uint8* key, uint8* buffer, uint8 headerLen, uint16 packedSize)  
{
    uint8   macHex[16 + 1]	= {0};                                              /** hex form of mac */
    uint8   macBin[8 + 1]	= {0};                                              /** binary form of mac */
    uint16  messageLen		= headerLen + packedSize;
     
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**setMac**");
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Master key:");
//    comportHexShow(key, 16);
    
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Data:");
//    comportHexShow(buffer, packedSize);
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "headerLen: %d", headerLen);
	
    createMac(key, &buffer[(int)headerLen], (packedSize - 8), macHex, macBin);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "macHex: %s", macHex);
    memcpy(&buffer[messageLen - 8], &macHex, 8);
	
    return messageLen;
}



/**
 * check response validity. 
 * @param   type transaction type.
 * @param   response response message.
 * @param   responseLen lenght of response message.
 * @param   request request message.
 * @param   requestLen lenght of request message.
 * @param   requestLen lenght of request message.
 * @param   macKey mac key.
 * @param   masterKey master key.
 * @return  response code or error code.
 */
int checkResponseValidity87(messageSpecST* messageSpec, int type, uint8* response, uint16 responseLen, 
                          uint8* request, uint16 requestLen)   
{   
    int                 responseIndex								= 0;                        /** response index */
    uint8               responseCode								= 0;                        /** message response code, field 39 */
    uint8               headerLen                   				= getMessageHeaderLen87();	/** message index, without message header */
    int8                reqField11[6 + 1]							= {0};                      /** request message fild 11 */
    int8                resField11[6 + 1]							= {0};                      /** response message fild 11 */
    int8                reqField0[4 + 1]							= {0};                      /** request message fild 0 */
    int8                resField0[4 + 1]							= {0};                      /** response message fild 0 */
    int8                reqField3[6 + 1]							= {0};                      /** request message fild 3 */
    int8                resField3[6 + 1]							= {0};                      /** response message fild 3 */
    int8                field39Data[2 + 1]							= {0};
    uint8               staticBufferRequest[MESSAGE_BUFFER_SIZE]    = {0};
    uint8               staticBufferResponse[MESSAGE_BUFFER_SIZE]   = {0};
    int8                reqField41[8 + 1]           				= {0};                  	/** request message fild 41 */
    int8                resField41[8 + 1]           				= {0};                  	/** response message fild 41 */
//  int8                reqField42[15 + 1]							= {0};						/** request message fild 42 */
//  int8                resField42[15 + 1]							= {0};						/** response message fild 42 */

    // CHECK FIELDES
    DL_ISO8583_HANDLER  isoHandlerRespons;                                      /** response message handler */
    DL_ISO8583_HANDLER  isoHandlerRequest;                                      /** request message handler */
    DL_ISO8583_MSG      isoMsgRespons;                                          /** response iso message */
    DL_ISO8583_MSG      isoMsgRequest;                                          /** request iso message */

    memset(&isoHandlerRespons, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoHandlerRequest, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsgRespons, 0, sizeof(DL_ISO8583_MSG));
    memset(&isoMsgRequest, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****check Response Validity 87****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandlerRespons);
    DL_ISO8583_MSG_Init(staticBufferResponse, sizeof(staticBufferResponse), &isoMsgRespons);
    DL_ISO8583_MSG_Unpack(&isoHandlerRespons, &response[(int)headerLen], responseLen - headerLen , &isoMsgRespons);//HNO_CHANGE casting
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandlerRequest);
    DL_ISO8583_MSG_Init(staticBufferRequest, sizeof(staticBufferRequest), &isoMsgRequest);
    DL_ISO8583_MSG_Unpack(&isoHandlerRequest, &request[(int)headerLen], requestLen - headerLen , &isoMsgRequest);//HNO_CHANGE casting

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseLen = %d", responseLen);
    
    // CHECK SIZE  // Header ID
    if ((responseLen <= 8) || (request[responseIndex] != response[responseIndex]))
    {   
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "#Err 1");
        return PARSE_ERROR_INVALID_RESPONSE;
    }
    
     // CHECK FIELD 11 - STAN  TODO: CHECK THIS
    if (type != TRANS_LOGON && type != TRANS_REVERSAL && type != TRANS_INITIALIZE && type != TRANS_ROLL_REQUEST)
    {   
        if (DL_ISO8583_MSG_HaveField(11, &isoMsgRequest) && (isoMsgRequest.field[11].len == 6)) 
        {
            strncpy(reqField11, (int8*)isoMsgRequest.field[11].ptr, 6);	
             reqField11[6] = 0;
        }
        else 
            return PARSE_ERROR_INVALID_BITMAP;
        
        if (DL_ISO8583_MSG_HaveField(11, &isoMsgRespons) && (isoMsgRespons.field[11].len == 6)) 
        {
            strncpy(resField11, (int8*)isoMsgRespons.field[11].ptr, 6);	
            resField11[6] = 0;
        }
        else 
            return PARSE_ERROR_INVALID_BITMAP;

        messageSpec->merchantSpec.recieveSTAN = atoi(resField11) ;
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field11 : %d",  messageSpec->merchantSpec.recieveSTAN );
    }
    
    // CHECK FIELD 41 - TERMINAL ID
    if (type != TRANS_LOANPAY_INQUERY && type != TRANS_ETC_INQUERY) //#MRF_971221
    {
        if (DL_ISO8583_MSG_HaveField(41, &isoMsgRequest) && (isoMsgRequest.field[41].len == 8)) 
        {
            strncpy(reqField41, (int8*)isoMsgRequest.field[41].ptr, 8);
            reqField41[8] = 0;
        }
        else 
            return PARSE_ERROR_INVALID_BITMAP;

        if (DL_ISO8583_MSG_HaveField(41, &isoMsgRespons) && (isoMsgRespons.field[41].len == 8))
        {
            strncpy(resField41, (int8*)isoMsgRespons.field[41].ptr, 8);	
            resField41[8] = 0;
        }
        else 
            return PARSE_ERROR_INVALID_BITMAP;


        if (strcmp((const char*)reqField41, resField41) != 0) 
            return PARSE_ERROR_INVALID_RESPONSE;
    }
    
    // CHECK FIELD 39 - REPONSE CODE
    if (DL_ISO8583_MSG_HaveField(39, &isoMsgRespons) && (isoMsgRespons.field[39].len == 2)) 
    {
        strncpy(field39Data, (int8*)isoMsgRespons.field[39].ptr, 2);	
        field39Data[2] = 0;
        responseCode = (field39Data[0] - '0') * 10 + field39Data[1] - '0'; //HNO
       
        messageSpec->responseCode = responseCode; 
        
        if ((responseCode != SUCCESS) && (responseCode != 8) && (responseCode != 16))
        {
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseCode = %d", responseCode);
            return responseCode;
        }
    } 
    else 
        return PARSE_ERROR_INVALID_BITMAP;
    
    // CHECK MAC
    if (type != TRANS_INITIALIZE && type != TRANS_LOGON) 
    {   
        if (!verifyMac(messageSpec->terminalKey.macKey, &response[headerLen], (responseLen - headerLen - 8), TRUE)) 
            return PARSE_ERROR_INVALID_MAC;
    }
    else
    {   
        if (!verifyMac(MASKAN_TEST_GENERAL_KEY, &response[headerLen], (responseLen - headerLen - 8), TRUE)) 
            return PARSE_ERROR_INVALID_MAC;
    }
    
    // Header Destination  // Header Originator
    responseIndex++;
    if ((memcmp(&request[responseIndex], &response[responseIndex + 2], 2) != 0) ||
            (memcmp(&response[responseIndex], &request[responseIndex + 2], 2) != 0))
    {
        return PARSE_ERROR_INVALID_RESPONSE;
    }
    responseIndex += 4;
    
    // CHECK FIELD 0 - MTI
    if (DL_ISO8583_MSG_HaveField(0, &isoMsgRequest) && (isoMsgRequest.field[0].len == 4)) 
    {
        strncpy(reqField0, (int8*)isoMsgRequest.field[0].ptr, 4);
        reqField0[2] = 0;
    }
    else 
        return PARSE_ERROR_INVALID_BITMAP;

    if (DL_ISO8583_MSG_HaveField(0, &isoMsgRespons) && (isoMsgRespons.field[0].len == 4)) 
    {
        strncpy(resField0, (int8*)isoMsgRespons.field[0].ptr, 4);
        resField0[2] = 0;
    }
    else 
        return PARSE_ERROR_INVALID_BITMAP;

    if (strcmp((const char*)reqField0, (const char*)resField0) != 0) 
    {
        return PARSE_ERROR_INVALID_RESPONSE;
    }
    
    // CHECK FIELD 3 - PROCESS CODE
    if (DL_ISO8583_MSG_HaveField(3, &isoMsgRequest) && (isoMsgRequest.field[3].len == 6)) 
    {
        strncpy(reqField3, (int8*)isoMsgRequest.field[3].ptr, 6);
         reqField3[6] = 0;
    }
    else 
        return PARSE_ERROR_INVALID_BITMAP;

    if (DL_ISO8583_MSG_HaveField(3, &isoMsgRespons) && (isoMsgRespons.field[3].len == 6))
    {
        strncpy(resField3, (int8*)isoMsgRespons.field[3].ptr, 6);	
        resField3[6] = 0;
    }
    else 
        return PARSE_ERROR_INVALID_BITMAP;

    if (strcmp((const char*)reqField3, (const char*)resField3) != 0) 
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "reqField3: %s , resField3: %s", reqField3, resField3);
        return PARSE_ERROR_INVALID_RESPONSE;
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Response code is : %d", responseCode);
    return SUCCESS;
}
 

int createSettlementMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    int                 messageLen                  = 0;                        /** final message lenght */
    int                 MessageHeaderSize           = 0;
    uint16              packedSize                  = 0;                        /** message lengh - message header lenght - mac lenght */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]          = {0};
    int8                currentdate[9]              = {0};                      /** current date, string form */
    int8                currenttime[7]              = {0};                      /** current time, string form */
    int8                RRN[12]                     = {0};                      /** current batch number */
    uint8               stanStr[7]                  = {0};                      /** stan string */
    uint8               Serial[16]                  = {0};
    uint8               versionNum[14]              = {0};
    uint8               serialInfo[20]              = {0};
    uint8               len[3]                      = {0};
    uint8               encryptedPAN[19 + 1]        = {0};
    uint8               year[4 + 1]                 = {0};
    uint8               AddData[99 + 1]             = {0}; 
    uint8               IMSI[30 + 1]                = {0};
    int                 IMSILen                     = 0;   
    terminalSpecST      terminalCapability          = getTerminalCapability();

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
        
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create Settlement Message 87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_SETTLEMENT_REQ_87, &isoMsg);

    // field 2
#ifdef ENCRYPTED_PAN 
    encryptData_DES(messageSpec->terminalKey.pinKey, messageSpec->settlementSpec.PAN, encryptedPAN);
    DL_ISO8583_MSG_SetField_Bin(2, encryptedPAN, 16, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(2, messageSpec->settlementSpec.PAN, &isoMsg);
#endif
       
    // field 3
    if(messageSpec->settlementSpec.transType == TRANS_BUY)
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY, &isoMsg);
    else if(messageSpec->settlementSpec.transType == TRANS_BUYCHARGE)
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY_CHARGE, &isoMsg);
    else if(messageSpec->settlementSpec.transType == TRANS_BILLPAY)
        DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BILLPAY, &isoMsg);
    else if(messageSpec->settlementSpec.transType == TRANS_LOANPAY)
    	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_LOAN_PAY_TRANS, &isoMsg);
    else if(messageSpec->settlementSpec.transType == TRANS_CHARITY)//HNO_CHARITY
    	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_CHARITY, &isoMsg);
    
    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->settlementSpec.transAmount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->settlementSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

    // field 12
//    timeToStr(messageSpec->dateTime.time , currenttime);MRF
    timeToStr(messageSpec->settlementSpec.dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
//    DateToStr(messageSpec->dateTime.date , currentdate);MRF
    DateToStr(messageSpec->settlementSpec.dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

    // field 37
    sprintf(RRN, "%s", messageSpec->settlementSpec.retrievalReferenceNumber);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);
    
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);
    
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    
    /* SENDING IMSI*/ 
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI); 
        strcat(IMSI, "}");
        IMSILen = strlen(IMSI);
        sprintf(AddData, "P13%03d", IMSILen);
        strcat(AddData, IMSI);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Add data :%s", AddData);
        strcat(serialInfo, AddData);
    }
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize] , &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg); 
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
          
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}


int parseSettlementResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER  isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               headerLen               = getMessageHeaderLen87();      /** message index, without message header */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               parseResponse           = 0;
    uint8               key[999 + 1]            = {0};
    int32               fieldLen                = 0;
    uint8               decryptedData[999 + 1]  = {0};
    uint8               field3[6 + 1]           = {0};
    int                 lenField3               = 0;
    uint8               field63Data[999 + 1]    = {0};

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****parse Settlement Response 87********");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen , &isoMsg);//for hdlc

    parseResponse = parseCommonPartResponse87(&isoMsg, messageSpec);
    if (parseResponse != PARSE_NO_ERROR )
        return parseResponse;

    //field3
    if (!DL_ISO8583_MSG_HaveField(3, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
    else
    {
        memset(field3,0,sizeof(field3));
        lenField3 = (int)isoMsg.field[3].len;//ABS
        strncpy(field3,(int8*)isoMsg.field[3].ptr,lenField3);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field3 : %s", field3);
        field3[lenField3] = 0;
    }
    
    if (strcmp(field3,PROCESS_CODE_BUY_CHARGE) == 0) //&&  (getChargeTypeCount() > 1))///(field24) != 0) 
    {
       //field 63
        if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
            return PARSE_ERROR_INVALID_BITMAP;
        else
        {
            //data field 63 will be decrypt with voucher key and decrypted data will be decrypt for voucher data
            memset(field63Data,0,sizeof(field63Data));
			fieldLen = (int32)isoMsg.field[63].len; //ABS  
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "fieldLen : %ld", fieldLen);
			strncpy(field63Data, (int8*)isoMsg.field[63].ptr, fieldLen); //ABS  
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "key: %s", field63Data);
            field63Data[fieldLen] = 0;
        }
        //Decrypt and Save key with voucher key
        hex2bin(field63Data, key, fieldLen);
        decryptData_3DES(messageSpec->terminalKey.voucherKey, key, 16, decryptedData);
        strcpy(voucherKeyDecryptor, decryptedData);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherKeyDecryptor: %s", voucherKeyDecryptor);
    }
    
    return PARSE_NO_ERROR;
}


int createReversalMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{ 
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint16              packedSize              = 0;                            /** message lengh - message header lenght - mac lenght */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    int8                RRN[12]                 = {0};                          /** current batch number */
    uint8               processCode[6 +1]       = {0};
    uint8               Serial[16]              = {0};
    uint8               serialInfo[20]          = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[14]          = {0};
    uint8               encryptedPAN[19 + 1]    = {0};
    uint8               year[4 + 1]             = {0};
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    int                 IMSILen                 = 0;   
    terminalSpecST      terminalCapability      = getTerminalCapability();

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "create Reversal Message 87");
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_REVERSAL_REQ_87, &isoMsg);

    // field 2
#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey,messageSpec->cardSpec.PAN, encryptedPAN);
    DL_ISO8583_MSG_SetField_Bin(2, encryptedPAN, 16, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(2, messageSpec->cardSpec.PAN, &isoMsg);
#endif
        
    switch (messageSpec->transType)
    {
        case TRANS_BUY:
            strcpy(processCode, PROCESS_CODE_BUY);
            break;
        case TRANS_BUYCHARGE:
            strcpy(processCode, PROCESS_CODE_BUY_CHARGE);
            break;
        case TRANS_BILLPAY:
            strcpy(processCode, PROCESS_CODE_BILLPAY);
            break;
        case TRANS_LOANPAY_INQUERY:
            strcpy(processCode, PROCESS_CODE_LOAN_PAY_INQUERY);//HNO_LOAN
            break;
        case TRANS_LOANPAY:
            strcpy(processCode, PROCESS_CODE_LOAN_PAY_TRANS);//HNO_LOAN
            break;
        case TRANS_ETC:
            strcpy(processCode, PROCESS_CODE_ETC_TRANS);//MRF_ETC
            break;
        case TRANS_CHARITY://HNO_CHARITY
        	strcpy(processCode, PROCESS_CODE_CHARITY);
        	break;
    }

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, processCode, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);

    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.recieveSTAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

    // field 37
//    sprintf(RRN, "%s", messageSpec->settlementSpec.retrievalReferenceNumber);
//    sprintf(RRN, "%s%s", currenttime, stanStr);   --MRF_NEW5
//    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    DL_ISO8583_MSG_SetField_Str(37, messageSpec->retrievalReferenceNumber, &isoMsg); 
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);
      
   // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);
    
   // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    
    /* SENDING IMSI*/ 
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI, "}");
        IMSILen = strlen(IMSI);
        sprintf(AddData, "P13%03d", IMSILen);
        strcat(AddData, IMSI);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Add data :%s", AddData);
        strcat(serialInfo, AddData);
    }
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize] , &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg); 
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
    
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}


int createInitializeMessage87(uint8* buffer, messageSpecST* messageSpec)   
{
    DL_ISO8583_HANDLER  isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    uint16              packedSize              = 0;                                /** message lengh - message header lenght - mac lenght */
    int                 messageLen              = 0;                                /** final message lenght */
    uint8               Serial[16]              = {0};                              /** terminal serial number */
    uint8               stanStr[7]              = {0};                              /** stan string */                        /** field 42 */    
    int8                currentdate[9]          = {0};                              /** current date, string form */
    int8                currenttime[7]          = {0};                              /** current time, string form */
    uint8               serialInfo[20]          = {0};
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0}; //HNO_IDENT
    int                 MessageHeaderSize       = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);/** message header size */
    uint8               versionKey[16 + 1]      = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
    uint8               year[4 + 1]             = {0};
    uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    int                 IMSILen                 = 0;   
    terminalSpecST      terminalCapability      = getTerminalCapability();

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create Initialize Message 87****");

    memset(&isoHandler, 0, sizeof (DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof (DL_ISO8583_MSG));
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_INITIALIZE_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_INITIALIZE, &isoMsg);

    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);   
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
     
    //field 53
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);
    
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
    
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0'); 
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    
    /* SENDING IMSI*/ 
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI); 
        strcat(IMSI, "}");
        IMSILen = strlen(IMSI);
        sprintf(AddData, "P13%03d", IMSILen);
        strcat(AddData, IMSI);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Add data :%s", AddData);
        strcat(serialInfo, AddData);
    }
    
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
        
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);
    
    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);
    
    if (packedSize == 0)
        return FALSE;
       
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "packedSize: %d", packedSize);
    setMac(MASKAN_TEST_GENERAL_KEY, buffer, MessageHeaderSize, packedSize);
    messageLen = MessageHeaderSize + packedSize;
    
    return messageLen;
}


/**
 * parse initialize response message.
 * @param   response initialize response message.
 * @param   responseLen response len.
 * @param   messageSpec message specifications.
 * @return  error code
 */
int parseInitializeResponse87(uint8* response, int16 responseLen, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER  isoHandler;                                             /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
	uint8               headerLen = getMessageHeaderLen87();          /** message index, without message header */
	int8                field63[999 + 1] = { 0 };                              /** field 60 */
	int                 field63Len = 0;                                /** field 60 length */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE] = { 0 };
	uint8               field53[16 + 1] = { 0 };                              /** field 53 */
	int                 field53Len = 0;
	uint8               field62[999 + 1] = { 0 };                              /** field 62 */
	int                 field62Len = 0;
	int                 nameLen = 0;
	int                 addressLen = 0;
	int                 postalLen = 0;
	int                 phoneLen = 0;
	uint8               tempName[4] = { 0 };
	uint8               tempAdd[4] = { 0 };
	uint8               tempPostalCode[4] = { 0 };
	uint8               tempTell[4] = { 0 };
	uint8               pcValue[4] = { 0 };
	int                 i = 0;
	int8                fieldData[7] = { 0 };                              /** field data */
	uint8               date[9] = { 0 };                              /** temp date str */
	uint8               field48[999 + 1] = { 0 };//+HNO_980916
	int                 field48Len = 0;//+HNO_980916
	uint8               ibanId[10] = { 0 };//+HNO_980916
	uint8               ibanCode[30] = { 0 };//+HNO_980916
	int8                ibanValue = 0;//+HNO_980916
	uint32              id = 0;//+HNO_980916
	int                 ibanNum = 0;//+HNO_980916
	int16               retValue = FAILURE;


	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse Initialize Response 87****");
	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	DL_ISO8583_MSG_Unpack(&isoHandler, &response[headerLen], responseLen - headerLen, &isoMsg);

	//    TODO: CONDITION SHOULD BE ONE IF
	// field 12
	if (!DL_ISO8583_MSG_HaveField(12, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else if ((int)isoMsg.field[12].len == 6)//ABS
	{
		memset(fieldData, 0, sizeof(fieldData));
		strncpy(fieldData, (int8*)isoMsg.field[12].ptr, 6);
		messageSpec->dateTime.time = strToTime(fieldData);
	}

	// field 13
	if (!DL_ISO8583_MSG_HaveField(13, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else if ((int)isoMsg.field[13].len == 4)//ABS
	{
		memset(fieldData, 0, sizeof(fieldData));
		strncpy(fieldData, (int8*)isoMsg.field[13].ptr, 4);
		DateToStr(messageSpec->dateTime.date, date);

		for (i = 4; i < 8; i++)
			date[i] = fieldData[i - 4];

		date[8] = 0;
		messageSpec->dateTime.date = strToDate(date);
	}

	// field 48 //+HNO_980916
	//if (!DL_ISO8583_MSG_HaveField(48, &isoMsg))
	//	return PARSE_ERROR_INVALID_BITMAP;
	//else
	if (DL_ISO8583_MSG_HaveField(48, &isoMsg))
	{
		memset(field48, 0, sizeof(field48));
		field48Len = (int)isoMsg.field[48].len;
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field48Len: %d", field48Len);
		strncpy(field48, (int8*)isoMsg.field[48].ptr, field48Len);
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field48 is: %s", field48);
	for (i = 0; i < field48Len; i++)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "i1: %d", i);
		do{
			ibanValue = field48[i + 1];
			strncat(ibanId, &field48[i], 1);
			i++;
			//            ibanId[i - strlen(ibanCode)] = '\0';
		} while (ibanValue != ':');

		do
		{
			ibanValue = field48[i + 1];
			i++;//move one step to scape ':'
			strncat(ibanCode, &field48[i], 1);
			//            ibanCode[i - strlen(ibanId)] = '\0';
		} while (ibanValue != ';');

		id = atoi(ibanId);
		
		messageSpec->merchantSpec.ibanInfo[ibanNum].IbanID = id;
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ibanId: %d", messageSpec->merchantSpec.ibanInfo[ibanNum].IbanID);

		strcpy(messageSpec->merchantSpec.ibanInfo[ibanNum].IbanCode, ibanCode);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ibanCode: %s", messageSpec->merchantSpec.ibanInfo[ibanNum].IbanCode);
		messageSpec->merchantSpec.ibanInfo[0].IbanCount = ibanNum + 1;
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ibanNummmmmmm: %d", messageSpec->merchantSpec.ibanInfo[0].IbanCount);
		retValue = appendFixedFileInfo(IBAN_INFO_FILE, &(messageSpec->merchantSpec.ibanInfo[ibanNum]), sizeof(ibanST), MAX_IBAN_INFO);

		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "retValue: %d", retValue);

		ibanNum++;

		memset(ibanCode, 0, sizeof(ibanCode));
		memset(ibanId, 0, sizeof(ibanId));
	}
	//        retValue = updateFileInfo(IBAN_BANK_COUNT_FILE , &ibanCount, 2);
	//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "retValue: %d", retValue);

	// field 53
	if (!DL_ISO8583_MSG_HaveField(53, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		memset(field53, 0, sizeof(field53));
		field53Len = (int)isoMsg.field[53].len;//ABS
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field53Len: %d", field53Len);
		strncpy(field53, (int8*)isoMsg.field[53].ptr, field53Len);
	}
	memset(messageSpec->versionKey.pinVersion, 0, sizeof(messageSpec->versionKey.pinVersion));
	memset(messageSpec->versionKey.macVersion, 0, sizeof(messageSpec->versionKey.macVersion));

	strncpy(messageSpec->versionKey.pinVersion, &field53[1], 3);
	strncpy(messageSpec->versionKey.macVersion, &field53[6], 3);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pin ver is :%s", messageSpec->versionKey.pinVersion);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Mac ver is :%s", messageSpec->versionKey.macVersion);

	// field 59
	if (DL_ISO8583_MSG_HaveField(59, &isoMsg))
	if ((int)isoMsg.field[59].len == 4)//ABS
	{
		memset(fieldData, 0, sizeof(fieldData));
		strncpy(fieldData, (int8*)isoMsg.field[59].ptr, 4);
		//mgh: comment
		//            DateToStr(messageSpec->dateTime.year , date);
		//            date[4] = 0;	

		for (i = 0; i < 4; i++)
			date[i] = fieldData[i];
	}

	messageSpec->dateTime.date = strToDate(date);

	// field 62
	if (!DL_ISO8583_MSG_HaveField(62, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		memset(field62, 0, sizeof(field62));
		field62Len = (int)isoMsg.field[62].len;//ABS
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field62Len: %d", field62Len);
		strncpy(field62, (int8*)isoMsg.field[62].ptr, field62Len);
	}

	memset(messageSpec->merchantSpec.marketName, 0, sizeof(messageSpec->merchantSpec.marketName));
	memset(messageSpec->merchantSpec.marketAddress, 0, sizeof(messageSpec->merchantSpec.marketAddress));
	memset(messageSpec->merchantSpec.postalCodeMarket, 0, sizeof(messageSpec->merchantSpec.postalCodeMarket));
	memset(messageSpec->merchantSpec.merchantPhone, 0, sizeof(messageSpec->merchantSpec.merchantPhone));

	for (i = 0; i < field62Len; i)
	{
		strncpy(pcValue, &field62[i], 4);
		pcValue[4] = 0;
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "pcValue: %s", pcValue);
		i += 4;
		if (strcmp(pcValue, "PC01") == 0)
		{
			strncpy(tempName, &field62[i], 3);
			nameLen = atoi(tempName);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "nameLen: %d", nameLen);
			i += 3;
			strncpy(messageSpec->merchantSpec.marketName, &field62[i], nameLen);
			i += nameLen;
		}
		else if (strcmp(pcValue, "PC02") == 0)
		{
			strncpy(tempAdd, &field62[i], 3);
			addressLen = atoi(tempAdd);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "addressLen: %d", addressLen);
			i += 3;
			strncpy(messageSpec->merchantSpec.marketAddress, &field62[i], addressLen);
			i += addressLen;
		}
		else if (strcmp(pcValue, "PC03") == 0)
		{
			strncpy(tempPostalCode, &field62[i], 3);
			postalLen = atoi(tempPostalCode);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "postalLen: %d", postalLen);
			i += 3;
			strncpy(messageSpec->merchantSpec.postalCodeMarket, &field62[i], postalLen);
			i += postalLen;
		}
		else if (strcmp(pcValue, "PC04") == 0)
		{
			strncpy(tempTell, &field62[i], 3);
			phoneLen = atoi(tempTell);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "tellLen: %d", phoneLen);
			i += 3;
			strncpy(messageSpec->merchantSpec.merchantPhone, &field62[i], phoneLen);
			i += phoneLen;
		}
	}

	/**    beginName = 4 + 3;
	strncpy(tempName,&field62[4],3);
	nameLen = atoi(tempName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "nameLen: %d", nameLen);

	beginAddress = beginName + nameLen + 4 + 3;
	strncpy(tempAdd,&field62[beginAddress - 3],3);
	addressLen = atoi(tempAdd);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "addressLen: %d", addressLen);

	beginPostalCode = beginAddress + addressLen + 4 + 3;
	strncpy(tempPostalCode,&field62[beginPostalCode - 3],3);
	postalLen = atoi(tempPostalCode);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "postalLen: %d", postalLen);

	beginPhone = beginPostalCode + postalLen + 4 + 3;
	strncpy(tempTell,&field62[beginPhone - 3],3);
	phoneLen = atoi(tempTell);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "tellLen: %d", phoneLen);

	strncpy(messageSpec->merchantSpec.marketName,&field62[beginName],nameLen);
	strncpy(messageSpec->merchantSpec.marketAddress,&field62[beginAddress],addressLen);
	strncpy(messageSpec->merchantSpec.postalCodeMarket,&field62[beginPostalCode],postalLen);
	strncpy(messageSpec->merchantSpec.merchantPhone,&field62[beginPhone],phoneLen);
	*/

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Name: %s", messageSpec->merchantSpec.marketName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Address: %s", messageSpec->merchantSpec.marketAddress);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PostalCode: %s", messageSpec->merchantSpec.postalCodeMarket);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Phone: %s", messageSpec->merchantSpec.merchantPhone);

	// field 63
	if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		memset(field63, 0, sizeof(field63));
		field63Len = (int)isoMsg.field[63].len;//ABS
		strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "keys:", field63);
	comportHexShow(field63, 100);

	memset(messageSpec->terminalKey.pinKey, 0, sizeof(messageSpec->terminalKey.pinKey));
	hex2bin(&field63[0], messageSpec->terminalKey.pinKey, 32);

	memset(messageSpec->terminalKey.macKey, 0, sizeof(messageSpec->terminalKey.macKey));
	hex2bin(&field63[32], messageSpec->terminalKey.macKey, 32);

	memset(messageSpec->terminalKey.voucherKey, 0, sizeof(messageSpec->terminalKey.voucherKey));
	hex2bin(&field63[64], messageSpec->terminalKey.voucherKey, 32);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherKey key is :");
	comportHexShow(messageSpec->terminalKey.voucherKey, 16);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "mac key is :");
	comportHexShow(messageSpec->terminalKey.macKey, 16);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "pin key is :");
	comportHexShow(messageSpec->terminalKey.pinKey, 16);

	//Calculate & Save KCV
	getKCV(MAC_INDEX, messageSpec->terminalKey.macKey);
	getKCV(PIN_INDEX, messageSpec->terminalKey.pinKey);
	getKCV(VOUCHER_INDEX, messageSpec->terminalKey.voucherKey);

	return PARSE_NO_ERROR;
}

uint8 initVersionKeySpec(filesST* files, versionKeyST* versionKey)
{
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "init Version KeySpec");
    
    strcpy(versionKey->pinVersion, "000");
    strcpy(versionKey->macVersion, "000");
    return writeVersionKeySpec(files, *versionKey);        
}

uint8 writeVersionKeySpec(filesST* files, versionKeyST versionKeySpec)  
{
    int16 retValue = FAILURE;

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**UPDATE: VersionKeySpecFile");
    
    retValue = updateFileInfo(files->versionKeyFile, &versionKeySpec, sizeof(versionKeyST));  
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "versionKeyFile: %s ",files->versionKeyFile);
    
    if (retValue != SUCCESS) 
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox("������� ���� ���� ����� ���!", MSG_ERROR); 
        return FALSE;
    }

    return TRUE;
}

uint8 readVersionKeySpec(filesST* files, versionKeyST* versionKeySpec)  
{
    int16   retValue      = FAILURE;                                              /* file update return value */
    uint32  lenght        = sizeof(merchantSpecST);                               /* size of merchant information */
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "***readVersionKeySpec***");

    retValue = readFileInfo(files->versionKeyFile, versionKeySpec, &lenght);
    if (retValue != SUCCESS)
    {
        if (retValue != ERR_FILE_NOT_FOUND)
            return FALSE; 
        else
        {
            memset(versionKeySpec->pinVersion, 0, 3);
            memset(versionKeySpec->macVersion, 0, 3);
            writeVersionKeySpec(files, *versionKeySpec);      
        }
    }
    return TRUE;
}

int getField24(void)
{
    return field24;
}

void resetField24(void)
{
    field24 = 0;
}

int parseField63ForBuyChargeOfflineResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
//    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
//    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
//    uint8               staticBuffer[2048]          ={0};
//    uint8               headerLen                   = getMessageHeaderLen87(); /** message index, without message header */
//    int                 retVal                      = PARSE_NO_ERROR;
//    int                 fieldLen                    = 0;
//    uint8               fieldData[999 + 1]          = {0};
//    uint8               operatorIDLenStr[3]         = {0};
//    int                 operatorIDLen               = 0;
//    uint8               operatorID[4]               = {0};
//    uint8               usingDescriptionLenStr[4]   = {0};
//    int                 usingDescriptionLen         = 0;
//    uint8               usingDescription[999 + 1]   = {0};
//    int                 beginOfData                 = 0;
//    uint8               voucherCount[3]             = {0};
//    uint8               voucherPINLenStr[3]         = {0};
//    int                 voucherPINLen               = 0;
//    uint8               voucherPIN[32 + 1]          = {0};
//    uint8               decryptedPin[16 + 1]        = {0};
//    uint8               voucherSerialLenStr[3]      = {0};
//    int                 voucherSerialLen            = 0;
//    uint8               voucherSerial[20 + 1]       = {0};
//    uint8               orginalAmountLenStr[3]      = {0};
//    int                 orginalAmountLen            = 0;
//    uint8               orginalAmount[99 + 1]       = {0};
//    int                 i                           = 0;
//    int                 j                           = 0;
//    int                 k                           = 0;
//    uint8               output[999 + 1]             = {0};
//    uint8               output1[999 + 1]            = {0};
//    int                 count                       = 0;
//    int32               mod                         = 0;
//    int32               binLen                      = strlen(voucherData) / 2;
//    
// 
//    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "********** parse Field 63 For BuyCharge Offline**************");
//
//    // field 63
//    //TO DO: error handle
//    //Check multi buy charge
//    hex2bin(voucherData, output1, sizeof(voucherData));
//    mod = binLen % 8;
//    if (mod != 0)
//        mod = 8 - mod;
//    if (mod != 0) //data is not multiple of 8
//        memset(&output1[binLen],'\0', mod);
//    
////    decryptData_3DES_2(messageSpec->voucherKeyDecryptor, output1, 1000 ,output);
//    decryptData_3DES_2(messageSpec->voucherKeyDecryptor, output1, binLen + mod,output);
//    
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "mod: %d", mod);
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "strlen: %d", strlen(output1));
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "output:");
//    comportHexShow(output,strlen(output));
//
//    //Save Charge Info in to Structure
//    for (i = 0; i <= OPERATOR_COUNT; i++)
//    {
//        if (messageSpec->buyChargeOfflineSpec[i].type == NULL)
//            continue;
//
//        //Operator ID Length
//        strncpy(operatorIDLenStr,&output[0],2);
//        operatorIDLen = atoi(operatorIDLenStr);
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "operatorIDLen: %d", operatorIDLen);
//
//        //Operator ID
//        strncpy(operatorID,&output[2],operatorIDLen);
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "operatorID: %s", operatorID);
//
//        //Using Description Length
//        beginOfData = operatorIDLen + 2;
//        strncpy(usingDescriptionLenStr,&output[beginOfData],3);
//        usingDescriptionLen = atoi(usingDescriptionLenStr);
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "usingDescriptionLen: %d", usingDescriptionLen);
//
//        //Using Description 
//        beginOfData = beginOfData + 3;
//        strncpy(usingDescription,&output[beginOfData],usingDescriptionLen);
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "usingDescription: %s", usingDescription);
//        strcpy(messageSpec->buyChargeSpec.usingDescription, usingDescription);
//        
//        //Voucher count
//        beginOfData = beginOfData + usingDescriptionLen;
//        strncpy(voucherCount,&output[beginOfData],2);
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherCount: %s", voucherCount);
//        
//        strcpy(messageSpec->boughtChargeOffline[i][k].operatorId,operatorID);
//        strcpy(messageSpec->boughtChargeOffline[i][k].usingDescription,usingDescription);
//        count = atoi(voucherCount);
//        messageSpec->boughtChargeOffline[i][k].productTypeCount = count;
//         
//        for(j = 0; j < count; j++)
//        {
//            //Voucher PIN length
//            beginOfData = beginOfData + 2;
//            strncpy(voucherPINLenStr,&output[beginOfData],2);
//            voucherPINLen = atoi(voucherPINLenStr);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherPINLen: %d", voucherPINLen);
//
//            //Voucher PIN
//            beginOfData = beginOfData + 2;
//            strncpy(voucherPIN,&output[beginOfData],voucherPINLen);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherPIN: %s", voucherPIN);
//            
//
//            //decrypt PIN
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "terminalKey.voucherKey:");
//            comportHexShow(messageSpec->terminalKey.voucherKey, sizeof(messageSpec->terminalKey.voucherKey));
//            decryptData_3DES(messageSpec->terminalKey.voucherKey, voucherPIN, 16, decryptedPin);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "decrypted PIN: %s", decryptedPin);
//            strcpy(messageSpec->boughtChargeOffline[i][j].PIN,decryptedPin);
//
//            //Voucher Serial Length
//            beginOfData = beginOfData + voucherPINLen;
//            strncpy(voucherSerialLenStr,&output[beginOfData],2);
//            voucherSerialLen = atoi(voucherSerialLenStr);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherSerialLen: %d", voucherSerialLen);
//
//            //Voucher Serial
//            beginOfData = beginOfData + 2;
//            strncpy(voucherSerial,&output[beginOfData],voucherSerialLen );
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherSerial: %s", voucherSerial);
//            strcpy(messageSpec->boughtChargeOffline[i][j].serial,voucherSerial);
//
//            //Original Amount Length
//            beginOfData = beginOfData + voucherSerialLen;
//            strncpy(orginalAmountLenStr,&output[beginOfData],2 );
//            orginalAmountLen = atoi(orginalAmountLenStr);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "orginalAmountLen: %d", orginalAmountLen);
//
//            //Original Amount
//            beginOfData = beginOfData + 2;
//            strncpy(orginalAmount,&output[beginOfData],orginalAmountLen);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "orginalAmount: %s", orginalAmount);
//            strcpy(messageSpec->boughtChargeOffline[i][j].realAmount,orginalAmount);
//        }
//        
////        if ((messageSpec->buyChargeOfflineSpec[i].realAmount != 0) &&
////                    (messageSpec->buyChargeOfflineSpec[i].realAmount != messageSpec->buyChargeOfflineSpec[i].amount))
////                        return PARSE_ERROR_INVALID_AMOUNT;
//        
//        k++;
//    }
}

uint8* getVoucherKeyDecryptor(void)
{
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Getting Voucher: %s", voucherKeyDecryptor);
    return voucherKeyDecryptor;
}

void resetVoucherKeyDecryptor(void)
{
    memset(voucherKeyDecryptor, 0 ,sizeof(voucherKeyDecryptor));
}

void resetVoucherData(void)
{
    memset(voucherData, 0, sizeof(voucherData));
}

int createBuyChargeMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)//HNO
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint16              packedSize              = 0;
    uint8               staticBuffer[2048]      = {0};
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    int                 counter                 = 0;                            /** loop counter */
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1]  = {0};
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    uint8               RRN[12]                 = {0};
    uint8               serialInfo[2048]        = {0};
    uint8               versionKey[16 + 1]      = {0};
    uint8               operator[3 + 1]         = {0};
    int                 count                   = 0;
    uint8               amount[12 + 1]          = {0};
    int                 lenOperator             = 0;
    int                 lenCount                = 0;
    int                 lenAmount               = 0;
    uint8               message[2048]           = {0};
    uint8               strCount[2 + 1]         = {0};
    uint8               Serial[16]              = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
    uint8               encryptedData[80 + 1]   = {0}; 
    uint8               year[4 + 1]             = {0};
    uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215

    terminalSpecST      terminalCapability      = getTerminalCapability();

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
	memset(&encryptedData, 0, sizeof(encryptedData)); //++ABS_980201

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****charge BUY Message 87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_BUY_CHARGE_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY_CHARGE, &isoMsg);

    // field 4 
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->buyChargeSpec.amount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    
    tempCardTrack2[counter] = 0;
    

#ifdef ENCRYPTED_PAN   
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);   
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
        
    // field 53 
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53,versionKey, &isoMsg);
    
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
    
    //Field 60
    DL_ISO8583_MSG_SetField_Str(60, "00001", &isoMsg);
      
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**********SET FIELD63 OF BUY CHARGE***************");
        
    strcpy(operator, messageSpec->buyChargeSpec.operatorId);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "operatorId:%s", operator);

    strcpy(amount, messageSpec->buyChargeSpec.amount);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Amount:%s", amount);

    lenOperator = strlen(operator);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenOperator: %02d", lenOperator);

    lenAmount = strlen(amount);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenAmount: %02d", lenAmount);

    count = 1;
    sprintf(strCount,"%02d",count);
    lenCount = strlen(strCount);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "lenCount: %02d", lenCount);

    sprintf(message, "%02d%s%02d%02d%02d%s", lenOperator, messageSpec->buyChargeSpec.operatorId, lenCount, count, lenAmount, amount);
    strcat(serialInfo, message);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "message: %s", message);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Field63 Total :%s", serialInfo);
    
    //..HNO_971215
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);
    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "AddData: %s", AddData);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
    
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
          
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
    
    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}


int parseField63ForBuyChargeResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]          ={0};
    uint8               headerLen                   = getMessageHeaderLen87();  /** message index, without message header */
//    int                 retVal                      = PARSE_NO_ERROR;
//    int                 fieldLen                    = 0;
//    uint8               fieldData[999 + 1]          = {0};
    uint8               operatorIDLenStr[3]         = {0};
    int                 operatorIDLen               = 0;
    uint8               operatorID[4]               = {0};
    uint8               usingDescriptionLenStr[4]   = {0};
    int                 usingDescriptionLen         = 0;
    uint8               usingDescription[999 + 1]   = {0};
    int                 beginOfData                 = 0;
    uint8               voucherCount[3]             = {0};
    uint8               voucherPINLenStr[3]         = {0};
    int                 voucherPINLen               = 0;
    uint8               voucherPIN[32 + 1]          = {0};
    uint8               decryptedPin[32 + 1]        = {0};
    uint8               voucherSerialLenStr[3]      = {0};
    int                 voucherSerialLen            = 0;
    uint8               voucherSerial[20 + 1]       = {0};
    uint8               orginalAmountLenStr[3]      = {0};
    int                 orginalAmountLen            = 0;
    uint8               orginalAmount[99 + 1]       = {0};
    int                 i                           = 0;
    int                 j                           = 0;
    int                 k                           = 0;
    uint8               output[999 + 1]             = {0};

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "********** parse Field 63 For BuyCharge**************");

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

   // field 63
   //TO DO: error handle
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**********Start of separate field 63**************");
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "voucher Data: %s", voucherData);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "voucher Key Decryptor: %x", messageSpec->voucherKeyDecryptor);

    strcpy(output,voucherData);

    //Operator ID Length
    strncpy(operatorIDLenStr,&output[0],2);
    operatorIDLen = atoi(operatorIDLenStr);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "operatorIDLen: %d", operatorIDLen);

    //Operator ID
    strncpy(operatorID,&output[2],operatorIDLen);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "operatorID: %s", operatorID);
    strcpy(messageSpec->buyChargeSpec.operatorId, operatorID);

    //Using Description Length
    beginOfData = operatorIDLen + 2;
    strncpy(usingDescriptionLenStr,&output[beginOfData],3);
    usingDescriptionLen = atoi(usingDescriptionLenStr);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "usingDescriptionLen: %d", usingDescriptionLen);

    //Using Description 
    beginOfData = beginOfData + 3;
    strncpy(usingDescription,&output[beginOfData],usingDescriptionLen);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "usingDescription: %s", usingDescription);
    strcpy(messageSpec->buyChargeSpec.usingDescription, usingDescription);

    //Voucher count
    beginOfData = beginOfData + usingDescriptionLen;
    strncpy(voucherCount,&output[beginOfData],2);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherCount: %s", voucherCount);
    messageSpec->buyChargeSpec.productTypeCount = atoi(voucherCount);

    //Voucher PIN length
    beginOfData = beginOfData + 2;
    strncpy(voucherPINLenStr,&output[beginOfData],2);
    voucherPINLen = atoi(voucherPINLenStr);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherPINLen: %d", voucherPINLen);

    //Voucher PIN
    beginOfData = beginOfData + 2;
    hex2bin(&output[beginOfData], voucherPIN, voucherPINLen);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherPIN: %d", voucherPIN);

    //decrypt PIN
    decryptData_3DES(messageSpec->terminalKey.voucherKey, voucherPIN, 16, decryptedPin);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "decrypted PIN: %d", decryptedPin);
    strcpy(messageSpec->buyChargeSpec.PIN,decryptedPin);
    
    //Voucher Serial Length
    beginOfData = beginOfData + voucherPINLen;
    strncpy(voucherSerialLenStr,&output[beginOfData],2);
    voucherSerialLen = atoi(voucherSerialLenStr);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherSerialLen: %d", voucherSerialLen);

    //Voucher Serial
    beginOfData = beginOfData + 2;
    strncpy(voucherSerial,&output[beginOfData],voucherSerialLen );
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "voucherSerial: %s", voucherSerial);
    strcpy(messageSpec->buyChargeSpec.serial,voucherSerial);

    //Original Amount Length
    beginOfData = beginOfData + voucherSerialLen;
    strncpy(orginalAmountLenStr,&output[beginOfData],2 );
    orginalAmountLen = atoi(orginalAmountLenStr);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "orginalAmountLen: %d", orginalAmountLen);

    //Original Amount
    beginOfData = beginOfData + 2;
    strncpy(orginalAmount,&output[beginOfData],orginalAmountLen);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "orginalAmount: %s", orginalAmount);
    strcpy(messageSpec->buyChargeSpec.realAmount,orginalAmount);

    if ((messageSpec->buyChargeSpec.realAmount != 0) &&
                (messageSpec->buyChargeSpec.realAmount != messageSpec->buyChargeSpec.amount))
                    return SUCCESS;
    k++;
    
    return SUCCESS;
}

/*******************************LOAN PAY****************************************/

int createloanPayTransMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint16              packedSize              = 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    int                 counter                 = 0;                            /** loop counter */
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1]  = {0};
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    uint8               RRN[12]                 = {0};
    uint8               versionKey[16 + 1]      = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
    uint8               loanAmountlen[3]        = {0};
    uint8               field63[300]             = {0};//+HNO_971215
    uint8               loanPanlen[3]           = {0};
    uint8               serial[16]              = {0};
    uint8               encryptedData[80 + 1]   = {0};
    uint8               year[4 + 1]             = {0};
    uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
	int					len63								= 0;//+HNO_971222
    terminalSpecST      terminalCapability      			= getTerminalCapability();
    serviceSpecST		service								= getPOSService();//+HNO_971215

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****createloanPaytransMessage87****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_LOAN_PAY_REQ_87, &isoMsg);
    
    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_LOAN_PAY_TRANS, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->loanPay.payAmount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    tempCardTrack2[counter] = 0;
    
#ifdef  ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);          
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
        
    // field 53
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);
        
    // field 63
    memset(field63, 0x00, sizeof(field63));
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(field63 ,"%s" ,versionNum);
    getTerminalSerialNumber(serial); 
    removePadLeft(serial, '0');
    sprintf(len, "%02d", strlen(serial));
    strcat(field63, len);
    strcat(field63, serial);
    
    //HNO
    sprintf(loanAmountlen, "%02d", strlen(messageSpec->loanPay.realAmount));
    strcat(field63, loanAmountlen);
    strcat(field63, messageSpec->loanPay.realAmount);
    sprintf(loanPanlen, "%02d", strlen(messageSpec->loanPay.destinationCardPAN));
    strcat(field63, loanPanlen);
    strcat(field63, messageSpec->loanPay.destinationCardPAN);
    
    //..HNO_971215
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "AddData: %s", AddData);
    	strcat(field63, AddData);
    }
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field63: %s", field63);
    
    DL_ISO8583_MSG_SetField_Str(63, field63, &isoMsg);
    
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg); 
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
    
    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}

//HNO_ADD_LOANPAY
int createloanPayInqueryMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                             	/** iso message handler */
	DL_ISO8583_MSG		isoMsg;
	uint8               stanStr[7]              		= {0};          /** stan string */
	int                 counter                 		= 0;            /** loop counter */
	int                 cardTrack2Len           		= (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len;
	uint8               tempCardTrack2[38 + 1]  		= {0};
	uint8               pinHex[16]              		= {0};          /** hex form of pin */
	uint8               pinBin[8]               		= {0};          /** binary form of pin */
	uint8               serial[16]          			= {0};                          /** terminal serial number */
	uint8               fieldData[200]   				= {0};  //..HNO_971215                        /** field 42 */
	uint8               currentdate[9]     				= {0};			/** current date, string form */
	uint8               currenttime[7]     				= {0};                      /** current time, string form */
	uint8				amount[12 + 1]					= {0};
	uint8 				refNumber[12 + 1]				= {0};
	uint8				serialLen[2 + 1]				= {0};
	uint8				terminalType[1 + 1]				= {0};
	uint8				year[4 + 1]						= {0};
	uint8               encryptedData[80 + 1]   		= {0};
	uint8               encryptedDataPan[80 + 1]   		= {0};
	int                 messageLen              		= 0;        /** final message lenght */
	int                 MessageHeaderSize       		= 0;
	uint16              packedSize              		= 0;        /** message lengh - message header lenght - mac lenght */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
//	uint8				dumpMsg[1000]					= {0};
	uint8				temporallyPAN[20 + 1]			= {0};
	uint8               versionNum[15]          		= {0};
   uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    terminalSpecST      terminalCapability      			= getTerminalCapability();
    serviceSpecST		service								= getPOSService();//+HNO_971215


	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "create loan pay Message 87");
	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));


	/** message header size */
	MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_LOAN_PAY_REQ_87, &isoMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "MTI_LOAN_PAY_REQ_87: %s", MTI_LOAN_PAY_REQ_87);

	// filed 2
	strcpy(temporallyPAN, messageSpec->loanPay.destinationCardPAN);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "temporallyPAN:%s", temporallyPAN);
	if (strlen(temporallyPAN) == 19)
		strcat(temporallyPAN, "0"); //Add for PANLEN 19 Like Tejarat Card
#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, temporallyPAN, encryptedDataPan);
	DL_ISO8583_MSG_SetField_Bin(2, encryptedDataPan, 16, &isoMsg);
#else
	DL_ISO8583_MSG_SetField_Str(2, temporallyPAN, &isoMsg);
#endif
//    DL_ISO8583_MSG_SetField_Str(2, messageSpec->loanPay.destinationCardPAN, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_LOAN_PAY_INQUERY, &isoMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PROCESS_CODE_LOAN_INQUERY: %s", PROCESS_CODE_LOAN_PAY_INQUERY);

	// field 4
	strcpy(amount,"000000000000");
	DL_ISO8583_MSG_SetField_Str(4, amount, &isoMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "amount: %s", amount);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "STAN: %d", messageSpec->merchantSpec.STAN);

	// field 12
	timeToStr(messageSpec->dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);


	// field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }

    tempCardTrack2[counter] = 0;

#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
	DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, isoMsg);
	#endif

	// field 37
	//    sprintf(refNumber, "%06s%06s", currenttime, stanStr);
    sprintf(refNumber, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, refNumber, &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
	DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

	// field 52
//	createPin87(messageSpec, pinHex, pinBin);
//	DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);

	// field 53
    getPOSTypeConnection(&POSType); //MRF_NEW
    asciiToBCD(&POSType, fieldData, strlen(&POSType), 0, 0);
    strcat (fieldData,messageSpec->versionKey.pinVersion);
    strcat (fieldData,"16");
    strcat (fieldData,messageSpec->versionKey.macVersion);
    strcat (fieldData,"1600000");
    
	DL_ISO8583_MSG_SetField_Str(53, fieldData, &isoMsg);

	// field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	memset(fieldData, 0, 20);

    getTerminalSerialNumber(serial);
    removePadLeft(serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(fieldData ,"%s" ,versionNum);
    sprintf(serialLen, "%02d", strlen(serial));
    strcat(fieldData, serialLen);
    strcat(fieldData, serial);
    
    //..HNO_971215
    if (terminalCapability.GPRSCapability == TRUE)
    {
        strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(fieldData, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fieldData: %s", fieldData);
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, fieldData, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize] , &packedSize);
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
	return 0;

	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

	messageLen = MessageHeaderSize + packedSize;

	return messageLen;
}

//HNO_LOAN
int createLoanPayTrackingMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
	int                 messageLen              = 0;                            /** final message lenght */
	int                 MessageHeaderSize       = 0;
	uint16              packedSize              = 0;
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
	uint8               stanStr[7]              = {0};                          /** stan string */
	int8                currentdate[9]          = {0};                          /** current date, string form */
	int8                currenttime[7]          = {0};                          /** current time, string form */
	uint8               RRN[12]                 = {0};
	uint8               versionKey[16 + 1]      = {0};
	uint8               Serial[16]              = {0};
	uint8               serialInfo[200]          = {0};//+HNO_971215
	uint8               len[3]                  = {0};
	uint8               versionNum[15]          = {0};
	uint8               year[4 + 1]             = {0};
	uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]			= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]	= {0};//+HNO_971215
    uint8           	activeUserName[16]  	= {0};//+HNO_971215
    uint8				data63Len[3]			= {0};//+HNO_971215
    int					len63					= 0;//+HNO_971222

    terminalSpecST      terminalCapability      = getTerminalCapability();
    serviceSpecST		service					= getPOSService();//+HNO_971215

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " create loan Tracking Message87");

	/** message header size */
	MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_LOAN_PAY_REQ_87, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_LOAN_TRACKING, &isoMsg);

	// field 4
	DL_ISO8583_MSG_SetField_Str(4, messageSpec->loanPay.payAmount, &isoMsg);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->loanPay.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

	// field 12
	timeToStr(messageSpec->loanPay.dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->loanPay.dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

	// field 37
	sprintf(RRN, "%s%s", currenttime, stanStr);
	DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
	DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

	// field 53
	getPOSTypeConnection(&POSType);
	asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
	strcat (versionKey,messageSpec->versionKey.pinVersion);
	strcat (versionKey,"16");
	strcat (versionKey,messageSpec->versionKey.macVersion);
	strcat (versionKey,"1600000");

	DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

   // field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
	getVersion(versionNum);
	padLeft(versionNum,5);

	sprintf(serialInfo ,"%s" ,versionNum);
	sprintf(len, "%02d", strlen(Serial));

	strcat(serialInfo, len);
	strcat(serialInfo, Serial);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/

	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
		return 0;

	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
	messageLen = MessageHeaderSize + packedSize;
	return messageLen;
}

int parseLoanPayTransResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER	isoHandler;                                         /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                             /** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               headerLen           = getMessageHeaderLen87();      /** message index, without message header */

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parseLoanPayTransResponse87****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}

int parseLoanPayInqueryResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                         /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                             /** iso message */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
	uint8               headerLen           = getMessageHeaderLen87();      /** message index, without message header */
	uint8               field62[999 + 1]    = {0};              			/** field 62 */
	int                 field62Len          = 0;
	int                 firstNameLength     = 0;
	int                 lastNameLength      = 0;
	int                 loanAmountLen       = 0;
	int                 beginLoanAmountLen  = 0;
	uint8               tempFirstName[4]    = {0};
	uint8               tempLastName[4]     = {0};
	uint8               tempLoanAmount[4]   = {0};
	int                 beginFirstName      = 0;
	int                 beginLastName       = 0;

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "parse LoanPayInquery Response 87");
	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

	// field 62
	if (!DL_ISO8583_MSG_HaveField(62, &isoMsg))
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "no field 62");
		return PARSE_ERROR_INVALID_BITMAP;
	}
	else
	{
		memset(field62,0,sizeof(field62));
		field62Len = (int8)isoMsg.field[62].len;//ABS:CHANGE:961207
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field62Len: %d", field62Len);
		strncpy(field62, (int8*)isoMsg.field[62].ptr, field62Len);
	}

	beginFirstName = 3;
	strncpy(tempFirstName,&field62[0],3);
	firstNameLength = atoi(tempFirstName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstNameLength: %d", firstNameLength);

	beginLastName = beginFirstName + firstNameLength + 3;
	strncpy(tempLastName,&field62[beginLastName - 3],3);
	lastNameLength = atoi(tempLastName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "lastNameLength: %d", lastNameLength);

	beginLoanAmountLen = beginLastName + lastNameLength + 2;
	strncpy(tempLoanAmount,&field62[beginLoanAmountLen - 2],2);
	loanAmountLen = atoi(tempLoanAmount);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loanAmountLen: %d", loanAmountLen);

	memset(messageSpec->loanPay.destinationCardHolderName,0,sizeof(messageSpec->loanPay.destinationCardHolderName));
	memset(messageSpec->loanPay.destinationCardHolderFamily,0,sizeof(messageSpec->loanPay.destinationCardHolderFamily));
	memset(messageSpec->loanPay.realAmount,0,sizeof(messageSpec->loanPay.realAmount));


	strncpy(messageSpec->loanPay.destinationCardHolderName,&field62[beginFirstName],firstNameLength);
	strncpy(messageSpec->loanPay.destinationCardHolderFamily,&field62[beginLastName],lastNameLength);
	strncpy(messageSpec->loanPay.realAmount,&field62[beginLoanAmountLen],loanAmountLen);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", messageSpec->loanPay.destinationCardHolderName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", messageSpec->loanPay.destinationCardHolderFamily);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan real Amount: %s", messageSpec->loanPay.realAmount);

	return parseCommonPartResponse87(&isoMsg, messageSpec);
}

//HNO_LOAN
int parseLoanPayTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                         /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                             /** iso message */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};//HNO_CHANGE because ICT erorr
	uint8               headerLen           = getMessageHeaderLen87();      /** message index, without message header */
	uint8               field63[999 + 1]    = {0};                          /** field 62 */
	int                 field63Len          = 0;
	uint8               statusLenstr[3]     = {0};
	uint8               errorLenstr[3]      = {0};
	int                 statusLen           = 0;
	int                 errorLen            = 0;

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**parseLOANtrackingResponse87**");
	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[(int)headerLen], bufferLen - headerLen, &isoMsg);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**field 63**");
    
	 // field 63
	if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		memset(field63,0,sizeof(field63));
		field63Len = (int8)isoMsg.field[63].len;//ABS:CHANGE
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field63Len: %d", field63Len);
		strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
	}
            
	memset(messageSpec->loanPay.status,0,sizeof(messageSpec->loanPay.status));
    memset(messageSpec->loanPay.error,0,sizeof(messageSpec->loanPay.error));
    
	strlcpy(statusLenstr, &field63[0], 4); //MRF_NEW8
	statusLen = atoi(statusLenstr);
	strncpy(messageSpec->loanPay.status, &field63[3], statusLen);
	errorLen = statusLen + 3;
	strlcpy(errorLenstr,&field63[errorLen],4);//ABS:CHANGE
	errorLen = atoi(errorLenstr);
	strncpy(messageSpec->loanPay.error,&field63[statusLen + 3 + 3],errorLen);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "status: %s", messageSpec->loanPay.status);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "error: %s", messageSpec->loanPay.error);

	return parseCommonPartResponse87(&isoMsg, messageSpec);
}


//HNO_ADD_LOANPAY
uint8 saveLoanInfo(uint8* files, loanPayST loanSpec)
{
	int16 retValue = FAILURE;                    /** file update return value */

	if (strcmp(files, UNKNOWN_LOAN_INFO_FILE) == 0)//ABS:CHANGE:961205
	{
		retValue = updateFileInfo(UNKNOWN_LOAN_INFO_FILE, &loanSpec, sizeof(loanPayST));
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "save unknown loan info in file ");
	}
	if (strcmp(files, LOAN_INFO_FILE) == 0)//ABS:CHANGE:961205
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "save LOAN Info");
		retValue = appendFixedFileInfo(LOAN_INFO_FILE, &loanSpec, sizeof(loanPayST), MAX_LOAN_TRANS);
	}

	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "information could not save in file");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		displayMessageBox("������� ��ǘ�� ����� ���!", MSG_ERROR);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loanSpec.STAN:%d", loanSpec.STAN);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loanSpec.amount:%s", loanSpec.payAmount);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", loanSpec.destinationCardHolderName);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", loanSpec.destinationCardHolderFamily);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PAN: %s", loanSpec.destinationCardPAN);

	return TRUE;
}

/*******************************RTC****************************************/
int createETCTransMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
	int                 messageLen              = 0;                            /** final message lenght */
	int                 MessageHeaderSize       = 0;
	uint16              packedSize              = 0;
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
	uint8               stanStr[7]              = {0};                          /** stan string */
	int8                currentdate[9]          = {0};                          /** current date, string form */
	int8                currenttime[7]          = {0};                          /** current time, string form */
	uint8               pinHex[16]              = {0};                          /** hex form of pin */
	uint8               pinBin[8]               = {0};                          /** binary form of pin */
	uint8               RRN[12]                 = {0};
	uint8               versionKey[16 + 1]      = {0};
	uint8               Serial[16]              = {0};
	uint8               serialInfo[300]          = {0};//..HNO_971215
	uint8               len[3]                  = {0};
	uint8               versionNum[15]          = {0};
	uint8               temp[100]               = {0};
	uint8               year[4 + 1]             = {0};
	uint8               field24[4 + 1]          = {0};
	uint8               encryptedData[80 + 1]   = {0};
	uint8               tempCardTrack2[37 + 1]  = {0};
	int                 counter                 = 0;
	uint8 				refNumber[12 + 1]		= {0};
	int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len;
	uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215
    terminalSpecST      terminalCapability      = getTerminalCapability();

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create ETC Trans Message87****");

	/** message header size */
	MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// bitmap : 1,3,4,11,12,13,24,35,37,41,42,52,53,63,64

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_ETC_REQ_87, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_ETC_TRANS, &isoMsg);

	// field 4
	DL_ISO8583_MSG_SetField_Str(4, messageSpec->ETC.amount, &isoMsg);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->ETC.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

	// field 12
	timeToStr(messageSpec->ETC.dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->ETC.dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

	//field 24
	strcpy(field24, "220");
	DL_ISO8583_MSG_SetField_Str(24, field24, &isoMsg);

	// field 35
	for (counter = 0 ; counter < cardTrack2Len ; counter++)
	{
		tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
		if(counter == messageSpec->cardSpec.PANLen)
			tempCardTrack2[counter] = 'D';
	}

	tempCardTrack2[counter] = 0;

#ifdef ENCRYPTED_PAN
	encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
	DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
	DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

	// field 37
	sprintf(refNumber, "%s%s", currenttime, stanStr);
	DL_ISO8583_MSG_SetField_Str(37, refNumber, &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
	DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

	// field 52
	createPin87(messageSpec, pinHex, pinBin);
	DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);

	// field 53
	getPOSTypeConnection(&POSType);
	asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
	strcat (versionKey,messageSpec->versionKey.pinVersion);
	strcat (versionKey,"16");
	strcat (versionKey,messageSpec->versionKey.macVersion);
	strcat (versionKey,"1600000");

	DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

   // field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
	getVersion(versionNum);
	padLeft(versionNum,5);
	sprintf(serialInfo ,"%s" ,versionNum);
	sprintf(len, "%02d", strlen(Serial));

	strcat(serialInfo, len);
	strcat(serialInfo, Serial);
	strcat(serialInfo, messageSpec->ETC.serialETC);
	strcat(serialInfo, "#");	//HNO_COMMENT presently delete from sending message
	strcat(serialInfo, messageSpec->ETC.cardHolderID);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/
    
	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
	//HNO_IDENT
	DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg); 
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
		return 0;
	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

	messageLen = MessageHeaderSize + packedSize;

	return messageLen;
}

int createETCInqueryMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint16              packedSize              = 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    uint8               RRN[12]                 = {0};
    uint8               versionKey[16 + 1]      = {0};
    uint8               Serial[16]              = {0};
    uint8               serialInfo[300]          = {0};//..HNO_971215
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
//    uint8               temp[100]               = {0};
    uint8               year[4 + 1]             = {0};
    uint8               field24[4 + 1]          = {0};
    uint8               encryptedData[80 + 1]   = {0};
    uint8               tempCardTrack2[37 + 1]  = {0};
    int                 counter                 = 0;
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len;
    uint8               amount[12 + 1]          = {0};
    uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int                 len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215
    terminalSpecST      terminalCapability      = getTerminalCapability();


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create ETCInquery Message87****");

    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

    // bitmap : 1,3,4,11,12,13,24,35,37,41,42,52,53,63,64

    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_ETC_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_ETC_TRANS ,&isoMsg);

    // field 4
    strcpy(amount,"000000000000");
    DL_ISO8583_MSG_SetField_Str(4, amount, &isoMsg);

    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

    //field 24
    strcpy(field24, "210");
    DL_ISO8583_MSG_SetField_Str(24, field24, &isoMsg);

    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }

    tempCardTrack2[counter] = 0;

#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);

    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);

    // field 53
    getPOSTypeConnection(&POSType); //MRF_NEW
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

   // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

    // field 63
    getTerminalSerialNumber(Serial);
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    sprintf(len, "%02d", strlen(Serial));

    strcat(serialInfo, len);
    strcat(serialInfo, Serial);
    strcat(serialInfo, messageSpec->ETC.serialETC);
    strcat(serialInfo, "#"); //HNO_COMMENT presently delete from sending message
    strcat(serialInfo, messageSpec->ETC.cardHolderID);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}

int createETCTrackingMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
	int                 messageLen              = 0;                            /** final message lenght */
	int                 MessageHeaderSize       = 0;
	uint16              packedSize              = 0;
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
	uint8               stanStr[7]              = {0};                          /** stan string */
	int8                currentdate[9]          = {0};                          /** current date, string form */
	int8                currenttime[7]          = {0};                          /** current time, string form */
	uint8               RRN[12]                 = {0};
	uint8               versionKey[16 + 1]      = {0};
	uint8               Serial[16]              = {0};
	uint8               serialInfo[200]          = {0};//..HNO_971215
	uint8               len[3]                  = {0};
	uint8               versionNum[15]          = {0};
	uint8               year[4 + 1]             = {0};
	uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215
    terminalSpecST      terminalCapability      = getTerminalCapability();

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " create ETC Tracking Message87");

	/** message header size */
	MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_ETC_REQ_87, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_ETC_TRACKING, &isoMsg);

	// field 4
	DL_ISO8583_MSG_SetField_Str(4, messageSpec->ETC.amount, &isoMsg);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->ETC.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

	// field 12
	timeToStr(messageSpec->ETC.dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->ETC.dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

	// field 37
	sprintf(RRN, "%s%s", currenttime, stanStr);
	DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
	DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

	// field 53
	getPOSTypeConnection(&POSType); //MRF_NEW
	asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
	strcat (versionKey,messageSpec->versionKey.pinVersion);
	strcat (versionKey,"16");
	strcat (versionKey,messageSpec->versionKey.macVersion);
	strcat (versionKey,"1600000");

	DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

   // field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
	getVersion(versionNum);
	padLeft(versionNum,5);

	sprintf(serialInfo ,"%s" ,versionNum);
	sprintf(len, "%02d", strlen(Serial));

	strcat(serialInfo, len);
	strcat(serialInfo, Serial);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	 strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/

	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
		return 0;

	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
	messageLen = MessageHeaderSize + packedSize;
	return messageLen;
}

int parseETCTransResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
		DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
	    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
	    uint8               staticBuffer[2048]	= {0};
	    uint8               headerLen           = getMessageHeaderLen87();       /** message index, without message header */

	    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parseETCTransResponse87****");

	    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

	    return parseCommonPartResponse87(&isoMsg, messageSpec);
}


int parseETCInqueryResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               headerLen           = getMessageHeaderLen87();          /** message index, without message header */
    uint8               field63[999 + 1]    = {0};                              /** field 62 */
    int                 field63Len          = 0;
    int                 firstNameLen        = 0;
    int                 lastNameLen         = 0;
    uint8               tempFirstName[4]    = {0};
    uint8               tempLastName[4]     = {0};
    int                 beginFirstName      = 0;
    int                 beginLastName       = 0;


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse ETCInquery Response 87****");

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    // field 63
    if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
    else
    {
        memset(field63,0,sizeof(field63));
        field63Len = (int8)isoMsg.field[63].len;//ABS:CHANGE
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field63Len: %d", field63Len);
        strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
    }

    //FIRST NAME
    beginFirstName = 3;
    strncpy(tempFirstName,&field63[0],3);
    firstNameLen = atoi(tempFirstName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstNameLength: %d", firstNameLen);

    //LAST NAME
    beginLastName = beginFirstName + firstNameLen + 3;
    strncpy(tempLastName,&field63[beginLastName - 3],3);
    lastNameLen = atoi(tempLastName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "lastNameLength: %d", lastNameLen);

    memset(messageSpec->ETC.cardHolderName,0,sizeof(messageSpec->ETC.cardHolderName));
    memset(messageSpec->ETC.cardHolderFamily,0,sizeof(messageSpec->ETC.cardHolderFamily));

    strncpy(messageSpec->ETC.cardHolderName,&field63[beginFirstName],firstNameLen);
    strncpy(messageSpec->ETC.cardHolderFamily,&field63[beginLastName],lastNameLen);

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", messageSpec->ETC.cardHolderName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", messageSpec->ETC.cardHolderFamily);

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}


int parseETCTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                         /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                             /** iso message */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};//HNO_CHANGE because ICT erorr
	uint8               headerLen           = getMessageHeaderLen87();      /** message index, without message header */
	uint8               field63[999 + 1]    = {0};                          /** field 62 */
	int                 field63Len          = 0;
	uint8               statusLenstr[3]     = {0};
	uint8               errorLenstr[3]      = {0};
	int                 statusLen           = 0;
	int                 errorLen            = 0;

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**parseETCTrackingResponse87**");
	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**field 63**");
	 // field 63
	if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**pin elseee**");
		memset(field63,0,sizeof(field63));
		field63Len = (int8)isoMsg.field[63].len;//ABS:CHANGE:961207
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field63Len: %d", field63Len);
		strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
	}

	memset(messageSpec->ETC.status,0,sizeof(messageSpec->ETC.status));
	memset(messageSpec->ETC.error,0,sizeof(messageSpec->ETC.error));
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**STATUS**");
	//STATUS
	strlcpy(statusLenstr, &field63[0], 4);//MRF_NEW8
	statusLen = atoi(statusLenstr);
	strncpy(messageSpec->ETC.status, &field63[3], statusLen);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**ST/ERROR CODEATUS**");
	//ERROR CODE
	errorLen = statusLen + 3;
	strlcpy(errorLenstr,&field63[errorLen],4);//MRF_NEW8
	errorLen = atoi(errorLenstr);
	strncpy(messageSpec->ETC.error,&field63[statusLen + 3 + 3],errorLen);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", messageSpec->ETC.status);
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", messageSpec->ETC.error);

	return parseCommonPartResponse87(&isoMsg, messageSpec);
}


uint8 saveETCInfo(uint8* fileName, ETCST ETCSpec)//ABS:CHANGE:961205
{
    int16 retValue = FAILURE;               /** file update return value */

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: ETCSpecFile");

	if (strcmp(fileName, UNKNOWN_ETC_INFO_FILE) == 0) //ABS:CHANGE:961205
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ETCTemp");
        retValue = updateFileInfo(UNKNOWN_ETC_INFO_FILE, &ETCSpec, sizeof(ETCST));
    }
	else if (strcmp(fileName, ETC_INFO_FILE) == 0)//ABS:CHANGE:961205
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ETCInfo");
        retValue = appendFixedFileInfo(ETC_INFO_FILE, &ETCSpec, sizeof(ETCST), MAX_ETC_TRANS);
    }

    if (retValue != SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #11");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        displayMessageBox("������� ��ǘ�� ����� ���!", MSG_ERROR);
        return FALSE;
    }

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ETCSpec.STAN:%d", ETCSpec.STAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ETCSpec.serialETC:%s", ETCSpec.serialETC);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ETCSpec.amount:%s", ETCSpec.amount);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", ETCSpec.cardHolderName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", ETCSpec.cardHolderFamily);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PAN: %s", ETCSpec.PAN);

    return TRUE;
}


/*******************************TOPUP****************************************/
//MRF_TOPUP
int createTopupMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */    
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint16              packedSize              = 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    int                 counter                 = 0;                            /** loop counter */
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1]  = {0};
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    uint8               RRN[12]                 = {0};
    uint8               serialInfo[2048]        = {0};
    uint8               versionKey[16 + 1]      = {0};
    int                 operator                = 0;
    int                 chargeType              = 0;
    uint8               mobileNo[11 + 1]        = {0};
    uint8               message[2048]           = {0};
    uint8               Serial[16]              = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
    uint8               encryptedData[80 + 1]   = {0}; 
    uint8               year[4 + 1]             = {0};
    uint8               POSType					= 0; 
    uint8               AddData[99 + 1]         = {0}; 
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
	int					len63								= 0;//+HNO_971222
    terminalSpecST      terminalCapability     			    = getTerminalCapability();
    serviceSpecST		service								= getPOSService();//+HNO_971215

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****TOPUP Message 87*****");
    
    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);    

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    
    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64
    
    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_TOPUP_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_BUY_CHARGE_TOPUP, &isoMsg);

    // field 4 
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->topup.amount, &isoMsg);
    
    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);
    
    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);
    
    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    
    tempCardTrack2[counter] = 0;
    
#ifdef ENCRYPTED_PAN   
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);
    
    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);
    
    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);   
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);
        
    // field 53 
    getPOSTypeConnection(&POSType); 
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");

    DL_ISO8583_MSG_SetField_Str(53,versionKey, &isoMsg);
    
    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg); 
    
      
    // field 63
    getTerminalSerialNumber(Serial); 
    removePadLeft(Serial, '0');
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(serialInfo ,"%s" ,versionNum);
    
    sprintf(len, "%02d", strlen(Serial));
    strcat(serialInfo, len);
    strcat(serialInfo, Serial);

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**********SET FIELD63 OF BUY CHARGE***************");
    
    operator = messageSpec->topup.operatorType;
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "operatorId:%d", operator);

    strcpy(mobileNo, messageSpec->topup.mobileNo);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "mobileNo:%s", mobileNo);

    chargeType = messageSpec->topup.type;
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "operatorId:%d", chargeType);
    
    sprintf(message, "%d;%s;%d", operator, mobileNo, chargeType);
    strcat(serialInfo, message);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "message: %s", message);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Field63 Total :%s", serialInfo);
    
    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(serialInfo, AddData);
    }

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "serialInfo: %s", serialInfo);
    /******************************/

    DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);
    
    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;
          
    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
    
    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}

//MRF_TOPUP
int parseBuyChargeTopupResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec) 
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               headerLen           = getMessageHeaderLen87();          /** message index, without message header */


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse Buy Response 87****");
    
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}


//HNO_ADD_ROLL
int createRollRequestMessage87(uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER  isoHandler;                                         /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                             /** iso message */

    uint16              packedSize          = 0;                            /** message lengh - message header lenght - mac lenght */
    int                 messageLen          = 0;                            /** final message lenght */
    int8                NIIstr[3 + 1]       = {0};                          /** message NII */
    uint8               Serial[16]          = {0};                          /** terminal serial number */
    uint8               pin42[16]           = {0};
    uint8               stanStr[7]          = {0};                          /** stan string */                        /** field 42 */
    int8                currentdate[9]      = {0};                          /** current date, string form */
    int8                currenttime[7]      = {0};                          /** current time, string form */
    uint8               transactionsInfo[127]       = {0};
    uint8               serialInfo[20]      = {0};
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0}; // MGH 080511 : added
    int                 MessageHeaderSize   = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);/** message header size */
    uint8               versionKey[16 + 1]  = {0};
    uint8               len[3]              = {0};
    uint8               versionNum[15]      = {0};
    uint8               year[4 + 1]         = {0};
    uint8               fieldData[20]   	= {0};//HNO_ADD
    uint8				dumpMsg[1000]		= {0};//HNO_ADD
    uint8				terminalType[1 + 1]	= {0};//HN0_ADD
    //HNO_ADD
    uint8				demand[10]		= {0}; //in protocol is 999 but we use 10
    uint8               demandLen[3]        = {0};
    uint8				requsetCountLen[3]		= {0};
    uint8				requsetCount[100]		= {0}; //in protocol is 999 but we use 100


    strcpy(demand, "1");

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "create ROLL Message 87");

	memset(&isoHandler, 0, sizeof (DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof (DL_ISO8583_MSG));

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_ROLL_REQ_87, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_ROLL_REQUEST, &isoMsg);

	// field 24
//    sprintf(NIIstr, "%03ld", messageSpec->generalConnInfo.messageNii);
//    DL_ISO8583_MSG_SetField_Str(24, NIIstr, &isoMsg);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

	// field 12
	timeToStr(messageSpec->dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 53
//	    strcat (versionKey, POS_TYPE);
	asciiToBCD(POS_TYPE, versionKey, strlen(POS_TYPE), 0, 0);
	strcat (versionKey,messageSpec->versionKey.pinVersion);
	strcat (versionKey,"16");
	strcat (versionKey,messageSpec->versionKey.macVersion);
	strcat (versionKey,"1600000");

	DL_ISO8583_MSG_SetField_Str(53,versionKey, &isoMsg);

//    // field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
	getVersion(versionNum);
	padLeft(versionNum,5);
	sprintf(serialInfo ,"%s" ,versionNum);
	sprintf(len, "%02d", strlen(Serial));
	strcat(serialInfo, len);
	strcat(serialInfo, Serial);

	strcat(serialInfo, "P06");
	sprintf(demandLen,"%03d", strlen(demand));
	strcat(serialInfo, demandLen);
	strcat(serialInfo, demand);//roll request is 1

	strcat(serialInfo, "P07");
	sprintf(requsetCountLen,"%03d", strlen(messageSpec->rollRequest.rollCount));
	strcat(serialInfo, requsetCountLen);
	strcat(serialInfo, messageSpec->rollRequest.rollCount);

	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
	DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
		return 0;
//HNO_CHANGE change key for setting mac
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "","mac: %x", messageSpec->terminalKey.macKey);
	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
	messageLen = MessageHeaderSize + packedSize;

	return messageLen;
}

//HNO_ADD_ROLL
int parseRollRequestResponse87(uint8* response, int16 responseLen, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER  isoHandler;											/** iso message handler */
    DL_ISO8583_MSG      isoMsg;												/** iso message */
    uint8               headerLen           = getMessageHeaderLen87();		/** message index, without message header */
    int8                field63[999 + 1]    = {0};							/** field 60 */
    int                 field63Len          = 0;							/** field 60 length */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]  = {0};
    uint8               field53[16 + 1]     = {0};          				/** field 53 */
    int                 field53Len          = 0;
    uint8               field62[999 + 1]    = {0};          				/** field 62 */
    int                 field62Len          = 0;
    int                 nameLen             = 0;
    int                 addressLen          = 0;
    int                 postalLen           = 0;
    int                 phoneLen            = 0;
    uint8               tempName[4]         = {0};
    uint8               tempAdd[4]          = {0};
    uint8               tempPostalCode[4]   = {0};
    uint8               tempTell[4]         = {0};
    uint8               pcValue[4]          = {0};
    int                 i                   = 0;
    int8                fieldData[7]		= {0};				/** field data */
    uint8               date[9]             = {0};              /** temp date str */


    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "parse roll Response 87");
    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &response[headerLen], responseLen - headerLen, &isoMsg);

//    TODO: CONDITION SHOULD BE ONE IF
    // field 12
    if (!DL_ISO8583_MSG_HaveField(12, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
	else if ((int8)isoMsg.field[12].len == 6)//ABS:CHANGE:961207
    {
        memset(fieldData, 0, sizeof(fieldData));
        strncpy(fieldData, (int8*)isoMsg.field[12].ptr, 6);
        messageSpec->dateTime.time = strToTime(fieldData);
    }

    // field 13
    if (!DL_ISO8583_MSG_HaveField(13, &isoMsg))
        return PARSE_ERROR_INVALID_BITMAP;
	else if ((int8)isoMsg.field[13].len == 4)//ABS:CHANGE:961207
    {
        memset(fieldData, 0, sizeof(fieldData));
        strncpy(fieldData, (int8*)isoMsg.field[13].ptr, 4);
        DateToStr(messageSpec->dateTime.date , date);

        for (i = 4; i < 8; i++)
            date[i] = fieldData[i - 4];

        date[8] = 0;
        messageSpec->dateTime.date = strToDate(date);
    }
    //TODO: WE DONT CHEK
    // field 53
    if (!DL_ISO8583_MSG_HaveField(53, &isoMsg))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "noo53");
        return PARSE_ERROR_INVALID_BITMAP;
    }
    else
    {
        memset(field53, 0, sizeof(field53));
		field53Len = (int8)isoMsg.field[53].len;//ABS:CHANGE:961207
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field53Len: %d", field53Len);
        strncpy(field53, (int8*)isoMsg.field[53].ptr, field53Len);
    }
    memset(messageSpec->versionKey.pinVersion, 0, sizeof(messageSpec->versionKey.pinVersion));
    memset(messageSpec->versionKey.macVersion, 0, sizeof(messageSpec->versionKey.macVersion));

    strncpy(messageSpec->versionKey.pinVersion,&field53[1],3);
    strncpy(messageSpec->versionKey.macVersion,&field53[6],3);

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pin ver is :%s",messageSpec->versionKey.pinVersion);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Mac ver is :%s",messageSpec->versionKey.macVersion);

    // field 59
    if (DL_ISO8583_MSG_HaveField(59, &isoMsg))
	if ((int8)isoMsg.field[59].len == 4)//ABS:CHANGE:961207
        {
            memset(fieldData, 0, sizeof(fieldData));
            strncpy(fieldData, (int8*)isoMsg.field[59].ptr, 4);
            //mgh: comment
//            DateToStr(messageSpec->dateTime.year , date);
//            date[4] = 0;

            for (i = 0; i < 4; i++)
                date[i] = fieldData[i];
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Mac ver is :%s",messageSpec->versionKey.macVersion);
        }

    messageSpec->dateTime.date = strToDate(date);

    // field 62
    if (!DL_ISO8583_MSG_HaveField(62, &isoMsg))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "noo62");
       return PARSE_ERROR_INVALID_BITMAP;
    }
    else
    {
        memset(field62,0,sizeof(field62));
		field62Len = (int8)isoMsg.field[62].len;//ABS:CHANGE:961207
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field62Len: %d", field62Len);
        strncpy(field62, (int8*)isoMsg.field[62].ptr, field62Len);
    }

    memset(messageSpec->merchantSpec.marketName,0,sizeof(messageSpec->merchantSpec.marketName));
    memset(messageSpec->merchantSpec.marketAddress,0,sizeof(messageSpec->merchantSpec.marketAddress));
    memset(messageSpec->merchantSpec.postalCodeMarket,0,sizeof(messageSpec->merchantSpec.postalCodeMarket));
    memset(messageSpec->merchantSpec.merchantPhone,0,sizeof(messageSpec->merchantSpec.merchantPhone));

    for (i = 0; i < field62Len; i)
    {
        strncpy(pcValue, &field62[i], 4);
        pcValue[4] = 0;
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "pcValue: %s", pcValue);
        i += 4;
        if(strcmp(pcValue , "PC01") == 0)
        {
            strncpy(tempName,&field62[i],3);
            nameLen = atoi(tempName);
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "nameLen: %d", nameLen);
            i += 3;
            strncpy(messageSpec->merchantSpec.marketName,&field62[i],nameLen);
            i += nameLen;
        }
        else if(strcmp(pcValue , "PC02") == 0)
    	{
	        strncpy(tempAdd,&field62[i],3);
	        addressLen = atoi(tempAdd);
	        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "addressLen: %d", addressLen);
	        i += 3;
	        strncpy(messageSpec->merchantSpec.marketAddress,&field62[i],addressLen);
	        i += addressLen;
    	}
        else if(strcmp(pcValue , "PC03") == 0)
        {
            strncpy(tempPostalCode,&field62[i],3);
            postalLen = atoi(tempPostalCode);
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "postalLen: %d", postalLen);
            i += 3;
            strncpy(messageSpec->merchantSpec.postalCodeMarket,&field62[i],postalLen);
            i += postalLen;
        }
        else if(strcmp(pcValue , "PC04") == 0)
        {
            strncpy(tempTell,&field62[i],3);
            phoneLen = atoi(tempTell);
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "tellLen: %d", phoneLen);
            i += 3;
            strncpy(messageSpec->merchantSpec.merchantPhone,&field62[i],phoneLen);
            i += phoneLen;
        }
    }

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Name: %s", messageSpec->merchantSpec.marketName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Address: %s", messageSpec->merchantSpec.marketAddress);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PostalCode: %s", messageSpec->merchantSpec.postalCodeMarket);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Phone: %s", messageSpec->merchantSpec.merchantPhone);

    // field 63
    if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
    {
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "noo63");
        return PARSE_ERROR_INVALID_BITMAP;
    }
    else
    {
        memset(field63, 0, sizeof(field63));
		field63Len = (int8)isoMsg.field[63].len;//ABS:CHANGE:961207
        strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
    }

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "keys: %s", field63);

    return PARSE_NO_ERROR;
}

//HNO_ADD_ROLL_TRACKING
int createRollTrackingMessage87(uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER  isoHandler;                                         /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                             /** iso message */

    uint16              packedSize          = 0;                            /** message lengh - message header lenght - mac lenght */
    int                 messageLen          = 0;                            /** final message lenght */
    int8                NIIstr[3 + 1]       = {0};                          /** message NII */
    uint8               Serial[16]          = {0};                          /** terminal serial number */
    uint8               pin42[16]           = {0};
    uint8               stanStr[7]          = {0};                          /** stan string */                        /** field 42 */
    int8                currentdate[9]      = {0};                          /** current date, string form */
    int8                currenttime[7]      = {0};                          /** current time, string form */
    uint8               transactionsInfo[127]       = {0};
    uint8               serialInfo[20]      = {0};
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0}; // MGH 080511 : added
    int                 MessageHeaderSize   = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);/** message header size */
    uint8               versionKey[16 + 1]  = {0};
    uint8               len[3]              = {0};
    uint8               versionNum[15]      = {0};
    uint8               year[4 + 1]         = {0};
    uint8               fieldData[20]   	= {0};//HNO_ADD
    uint8				dumpMsg[1000]		= {0};//HNO_ADD
    uint8				terminalType[1 + 1]	= {0};//HN0_ADD
    //HNO_ADD
    uint8				demand[10]		= {0}; //in protocol is 999 but we use 10
    uint8               demandLen[3]        = {0};
    uint8				stanLen[3]			= {0};
    uint8				requsetCount[100]		= {0}; //in protocol is 999 but we use 100

    strcpy(demand, "1");
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "create ROLL TRACK Message 87");

	memset(&isoHandler, 0, sizeof (DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof (DL_ISO8583_MSG));

	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

	// field 0
	DL_ISO8583_MSG_SetField_Str(0, MTI_ROLL_REQ_87, &isoMsg);

	// field 3
	DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_ROLL_TRACKING, &isoMsg);

	// field 24
//    sprintf(NIIstr, "%03ld", messageSpec->generalConnInfo.messageNii);
//    DL_ISO8583_MSG_SetField_Str(24, NIIstr, &isoMsg);

	// field 11
	sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
	DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

	// field 12
	timeToStr(messageSpec->dateTime.time , currenttime);
	DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

	// field 13
	DateToStr(messageSpec->dateTime.date , currentdate);
	DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

	// field 41
	DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

	// field 53
	//    strcat (versionKey, POS_TYPE);
	asciiToBCD(POS_TYPE, versionKey, strlen(POS_TYPE), 0, 0);
	strcat (versionKey,messageSpec->versionKey.pinVersion);
	strcat (versionKey,"16");
	strcat (versionKey,messageSpec->versionKey.macVersion);
	strcat (versionKey,"1600000");

	DL_ISO8583_MSG_SetField_Str(53,versionKey, &isoMsg);

//    // field 59
	sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
	DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

	// field 63
	getTerminalSerialNumber(Serial);
	removePadLeft(Serial, '0');
	getVersion(versionNum);
	padLeft(versionNum,5);
	sprintf(serialInfo ,"%s" ,versionNum);
	sprintf(len, "%02d", strlen(Serial));
	strcat(serialInfo, len);
	strcat(serialInfo, Serial);

	strcat(serialInfo, "P06");
	sprintf(demandLen,"%03d", strlen(demand));
	strcat(serialInfo, demandLen);
	strcat(serialInfo, demand);//roll request is 1

	strcat(serialInfo, "P07");
	sprintf(stanLen, "%03d", strlen((const char *)messageSpec->rollRequest.STAN));//ABS:CHANGE:961207
	strcat(serialInfo, stanLen);
	strcat(serialInfo, (const char *)messageSpec->rollRequest.STAN);//ABS:CHANGE:961207

	DL_ISO8583_MSG_SetField_Str(63, serialInfo, &isoMsg);

	// field 64
	DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

	DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
	DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", dumpMsg);
	DL_ISO8583_MSG_Free(&isoMsg);

	if (packedSize == 0)
		return 0;
//HNO_CHANGE change key for setting mac
	setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);
	messageLen = MessageHeaderSize + packedSize;
//	setMac(MASKAN_TEST_GENERAL_KEY, buffer, MessageHeaderSize, packedSize);
//	messageLen = MessageHeaderSize + packedSize;

	return messageLen;
}

//HNO_ADD_ROLL_TRACKING
int parseRollTrackingResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
	DL_ISO8583_HANDLER	isoHandler;                                         /** iso message handler */
	DL_ISO8583_MSG      isoMsg;                                             /** iso message */
	uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};//HNO_CHANGE because ICT erorr
	uint8               headerLen           = getMessageHeaderLen87();      /** message index, without message header */
	uint8               field63[999 + 1]    = {0};                          /** field 62 */
	int                 field63Len          = 0;
	uint8               dataStrLen[3]     	= {0};
	uint8               errorLenstr[3]      = {0};
	int                 dataLen           		= 0;
	int					fieldDataIndex      = 0;
	uint8				recieveData[999]	= {0};
	uint8				ch					= {0};
	uint8				point				= {0};
	int					i					= 0;
	int					pointLen			= 0;
	uint8				stanStr[6]			= {0};
//	uint8				point				= {0};
//	uint8				point				= {0};

	memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
	memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**parse roll trackingResponse87**");
	DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
	DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
	DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[(int)headerLen], bufferLen - headerLen, &isoMsg);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**field 63**");
	 // field 63
	if (!DL_ISO8583_MSG_HaveField(63, &isoMsg))
		return PARSE_ERROR_INVALID_BITMAP;
	else
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**pin elseee**");
		memset(field63,0,sizeof(field63));
		field63Len = (int8)isoMsg.field[63].len;//ABS:CHANGE:961207
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "field63Len: %d", field63Len);
		strncpy(field63, (int8*)isoMsg.field[63].ptr, field63Len);
	}

	fieldDataIndex = 0;
	if (strncmp(&field63[fieldDataIndex], "P08", 3) == 0)
	{
		fieldDataIndex = fieldDataIndex + 3;
		strncpy(dataStrLen, &field63[fieldDataIndex], 3);
		dataLen = atoi(dataStrLen);
		fieldDataIndex = fieldDataIndex + 3;
		strncpy(recieveData, &field63[fieldDataIndex], dataLen);
	}
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "recieveData: %s", recieveData);
//	ch = '|';
//	for(i = 0; i < strlen(recieveData); i++)
//	{
//		point = strrchr(recieveData, ch);
//		pointLen = atoi(point);
//		strncpy(stanStr, recieveData, pointLen);
//	}

	return parseCommonPartResponse87(&isoMsg, messageSpec);
}

//HNO_ADD_ROLL
uint8 saveRollInfo(uint8* files, rollRequestST rollReuest)
{
	int16 retValue = FAILURE;                    /** file update return value */

//	if (files == UNKNOWN_LOAN_INFO_FILE)
//	{
//		retValue = updateFileInfo(UNKNOWN_LOAN_INFO_FILE, &loanSpec, sizeof(loanPayST));
//		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "save unknown loan info in file ");
//	}
	if (files == ROLL_INFO_FILE)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "save roll Info");
		retValue = appendFixedFileInfo(ROLL_INFO_FILE, &rollReuest, sizeof(rollRequestST), MAX_Roll_TRANS);
	}

	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "information could not save in file");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		displayMessageBox("������� ��ǘ�� ����� ���!", MSG_ERROR);
		return FALSE;
	}

//	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loanSpec.STAN:%d", loanSpec.STAN);
//	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loanSpec.amount:%s", loanSpec.payAmount);
//	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", loanSpec.destinationCardHolderName);
//	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", loanSpec.destinationCardHolderFamily);
//	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PAN: %s", loanSpec.destinationCardPAN);

	return TRUE;
}

//HNO_CHARGE
/**
 * This function save current bought charge in file.
 * @param   files [input]: name of file witch should be update
 * @param   count [input]: number of bought charge
 * @see     appendFixedFileInfo().
 * @see     updateFixedFileLastRecord().
 * @see     displayMessageBox().
 * @return  TRUE & FASE.
 */
uint8 savechargeInfo(chargeST charge, int count)//MRF_NEW8
{
	int16 retValue = FAILURE;

	if(count == 1)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "first charge");
		retValue = appendFixedFileInfo(CHARGE_INFO_FILE, &charge, sizeof(chargeST), MAX_CHARGE_TRANS);
	}
	else
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "next one charges");
		retValue = updateFixedFileLastRecord(CHARGE_INFO_FILE, &charge, sizeof(chargeST), MAX_CHARGE_TRANS);
	}

	if (retValue != SUCCESS)
	{
		displayMessageBox("������� ��ю ����� ���!", MSG_ERROR);
		return FALSE;
	}

	return TRUE;
}

//HNO_CHARITY
int createCharityMessage87(filesST* files, uint8* buffer, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    int                 messageLen              = 0;                            /** final message lenght */
    int                 MessageHeaderSize       = 0;
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]      = {0};
    uint8               field63[400]             = {0}; //..HNO_971215                         /** field 63 */
    uint16              packedSize              = 0;
    uint8               stanStr[7]              = {0};                          /** stan string */
    int8                currentdate[9]          = {0};                          /** current date, string form */
    int8                currenttime[7]          = {0};                          /** current time, string form */
    int                 counter                 = 0;                            /** loop counter */
    int                 cardTrack2Len           = (messageSpec->cardSpec.track2Len > 37) ? 37 : messageSpec->cardSpec.track2Len; /** card track 2 lenght */
    uint8               tempCardTrack2[37 + 1]  = {0};
    uint8               pinHex[16]              = {0};                          /** hex form of pin */
    uint8               pinBin[8]               = {0};                          /** binary form of pin */
    uint8               RRN[12]                 = {0};
    uint8               charitylen[3]           = {0};
    uint8               paymentIDlen[3]         = {0};
    uint8               versionKey[16 + 1]      = {0};
    uint8               serial[16]              = {0};
    uint8               len[3]                  = {0};
    uint8               versionNum[15]          = {0};
    uint8               encryptedData[80 + 1]   = {0};
    uint8               year[4 + 1]             = {0};
    uint8               POSType					= 0;
    uint8               AddData[99 + 1]         = {0};
    uint8               IMSI[30 + 1]            = {0};
    uint8				shiftData[30]						= {0};//+HNO_971215
    uint8				additionalMsg[99 + 1]				= {0};//+HNO_971215
    uint8           	activeUserName[16]  				= {0};//+HNO_971215
    uint8				data63Len[3]						= {0};//+HNO_971215
    int					len63								= 0;//+HNO_971222
    serviceSpecST		service								= getPOSService();//+HNO_971215

    terminalSpecST      terminalCapability      = getTerminalCapability();



    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****create Charity Message 87****");

    /** message header size */
    MessageHeaderSize = createMessageHeader87(buffer, messageSpec->generalConnInfo.SWINii);

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);

    // bitmap : 1,3,4,11,12,13,35,37,41,42,52,53,63,64

    // field 0
    DL_ISO8583_MSG_SetField_Str(0, MTI_CHARITY_REQ_87, &isoMsg);

    // field 3
    DL_ISO8583_MSG_SetField_Str(3, PROCESS_CODE_CHARITY, &isoMsg);

    // field 4
    DL_ISO8583_MSG_SetField_Str(4, messageSpec->amount, &isoMsg);

    // field 11
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    DL_ISO8583_MSG_SetField_Str(11, stanStr, &isoMsg);

    // field 12
    timeToStr(messageSpec->dateTime.time , currenttime);
    DL_ISO8583_MSG_SetField_Str(12, currenttime, &isoMsg);

    // field 13
    DateToStr(messageSpec->dateTime.date , currentdate);
    DL_ISO8583_MSG_SetField_Str(13, &currentdate[4], &isoMsg);

    // field 35
    for (counter = 0 ; counter < cardTrack2Len ; counter++)
    {
        tempCardTrack2[counter] = messageSpec->cardSpec.track2[counter];
        if(counter == messageSpec->cardSpec.PANLen)
            tempCardTrack2[counter] = 'D';
    }
    tempCardTrack2[counter] = 0;

    
#ifdef ENCRYPTED_PAN
    encryptData_DES(messageSpec->terminalKey.pinKey, tempCardTrack2, encryptedData);
    DL_ISO8583_MSG_SetField_Bin(35, encryptedData, 40, &isoMsg);
#else
    DL_ISO8583_MSG_SetField_Str(35, tempCardTrack2, &isoMsg);
#endif

    // field 37
    sprintf(RRN, "%s%s", currenttime, stanStr);
    DL_ISO8583_MSG_SetField_Str(37, RRN, &isoMsg);

    // field 41
    DL_ISO8583_MSG_SetField_Str(41, messageSpec->merchantSpec.terminalID, &isoMsg);

    // field 42
    padLeft(messageSpec->merchantSpec.merchantID, 15); //+MRF_971205
    DL_ISO8583_MSG_SetField_Str(42, messageSpec->merchantSpec.merchantID, &isoMsg);

    // field 52
    createPin87(messageSpec, pinHex, pinBin);
    DL_ISO8583_MSG_SetField_Bin(52, pinBin, 8, &isoMsg);

    // field 53
    getPOSTypeConnection(&POSType);
    asciiToBCD(&POSType, versionKey, strlen(&POSType), 0, 0);
    strcat (versionKey,messageSpec->versionKey.pinVersion);
    strcat (versionKey,"16");
    strcat (versionKey,messageSpec->versionKey.macVersion);
    strcat (versionKey,"1600000");
    DL_ISO8583_MSG_SetField_Str(53, versionKey, &isoMsg);

    // field 59
    sprintf(year, "%04d", getYear(messageSpec->dateTime.date));
    DL_ISO8583_MSG_SetField_Str(59, year, &isoMsg);

    // field 63
    memset(field63, 0x00, sizeof(field63));
    getVersion(versionNum);
    padLeft(versionNum,5);
    sprintf(field63 ,"%s" ,versionNum);
    getTerminalSerialNumber(serial);
    removePadLeft(serial, '0');
    sprintf(len, "%02d", strlen(serial));
    strcat(field63, len);
    strcat(field63, serial);

    sprintf(charitylen, "%03d", strlen(messageSpec->charity.InstituteCode));
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "institute data len :%s", charitylen);
    strcat(field63, charitylen);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Institute Code :%s", messageSpec->charity.InstituteCode);
    strcat(field63, messageSpec->charity.InstituteCode);

    //..HNO_971215

    if (terminalCapability.GPRSCapability == TRUE)
    {
    	strcpy(AddData,"P13");
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "GPRS & not shiftService");
        strcpy(IMSI,"{IMSI:");
        strcat(IMSI, messageSpec->IMSI);
        strcat(IMSI,"}");
        strcpy(additionalMsg, IMSI);
        sprintf(data63Len, "%03d", strlen(IMSI));
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "additionalMsg: %s", additionalMsg);
    	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data63Len: %s", data63Len);

    	strcat(AddData,data63Len);
    	strcat(AddData, additionalMsg);
    	strcat(field63, AddData);
    }
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "field63: %s", field63);
    /******************************/
    DL_ISO8583_MSG_SetField_Str(63, field63, &isoMsg);

    // field 64
    DL_ISO8583_MSG_SetField_Str(64, "00000000", &isoMsg);

    DL_ISO8583_MSG_Pack(&isoHandler, &isoMsg, &buffer[MessageHeaderSize], &packedSize);
    DL_ISO8583_MSG_Dump(&isoHandler, &isoMsg);
    DL_ISO8583_MSG_Free(&isoMsg);

    if (packedSize == 0)
        return 0;

    setMac(messageSpec->terminalKey.macKey, buffer, MessageHeaderSize, packedSize);

    messageLen = MessageHeaderSize + packedSize;

    return messageLen;
}

//HNO_CHARITY
int parseCharityResponse87(uint8* buffer, int16 bufferLen, messageSpecST* messageSpec)
{
    DL_ISO8583_HANDLER	isoHandler;                                             /** iso message handler */
    DL_ISO8583_MSG      isoMsg;                                                 /** iso message */
    uint8               staticBuffer[MESSAGE_BUFFER_SIZE]	= {0};
    uint8               headerLen           = getMessageHeaderLen87();          /** message index, without message header */

    memset(&isoHandler, 0, sizeof(DL_ISO8583_HANDLER));
    memset(&isoMsg, 0, sizeof(DL_ISO8583_MSG));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "****parse charity Response 87****");

    DL_ISO8583_DEFS_1987_GetHandler(&isoHandler);
    DL_ISO8583_MSG_Init(staticBuffer, sizeof(staticBuffer), &isoMsg);
    DL_ISO8583_MSG_Unpack(&isoHandler, &buffer[headerLen], bufferLen - headerLen, &isoMsg);

    return parseCommonPartResponse87(&isoMsg, messageSpec);
}



