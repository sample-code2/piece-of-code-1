#include <string.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_connection.h"
#include "N_connectionWrapper.h"
//#include "N_connection.h" 
#include "N_config.h"
#include "N_cardSpec.h"
#include "N_dateTime.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_initialize.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_messaging.h"
#include "N_error.h"
#include "N_terminalSpec.h"
#include "N_POSSpecific.h"
#include "N_display.h"


/** ------------------------------------------------------------------------ **/
/**                              GENERAL  CONFIG                             **/
/** ------------------------------------------------------------------------ **/

/**
 * Set General connection Count Defaults.
 * @param  generalConnInfo [input] :general connection information
 * @see    displayMessageBox().
 * @return True
 */
uint8 initGeneralConnectionCountDefaults(generalConnInfoST* generalConnInfo)
{
    displayMessageBox("����� ��� ��� �� ConnCount", MSG_INFO);
    generalConnInfo->connectionCount = GENERAL_CONNECTION_COUNT_DEFAULT;

    return TRUE;
}

/**
 * Set General Connection Type Defaults.
 * @param   connectionType [input] : type of connection
 * @see     displayMessageBox().
 * @return  True.
 */
uint8 initGeneralConnectionTypeDefaults(uint8* connectionType)
{
    terminalSpecST   terminalCapability = getTerminalCapability(); //MRF_NEW2
    
    displayMessageBox("����� ��� ��� �� connectiontype", MSG_INFO);

    if(terminalCapability.GPRSCapability)//MRF_NEW2
    	*connectionType = CONNECTION_GPRS;
    else
    	*connectionType = GENERAL_CONNECTION_TYPE_DEFAULT;

    return TRUE;
}

/**
 * Set General connection POS NII defults.
 * @param  generalConnInfo [input] : general connection information
 * @see    displayMessageBox().
 * @return True.
 */
uint8 initGeneralConnectionTPDUNIIDefaults(generalConnInfoST* generalConnInfo)
{
    displayMessageBox("����� ��� ��� �� TpdNii", MSG_INFO);
    generalConnInfo->POSNii = GENERAL_CONNECTION_POS_NII_DEFAULT;
    
    return TRUE;
}

/**
 * Set General connection Switch NII defaults.
 * @param  generalConnInfo [input] :general connection information
 * @see    displayMessageBox().
 * @return True.
 */
uint8 initGeneralConnectionMessageNiiDefaults(generalConnInfoST* generalConnInfo)
{
    displayMessageBox("����� ��� ��� �� MessNii", MSG_INFO);
    generalConnInfo->SWINii = GENERAL_CONNECTION_SWI_NII_DEFAULT;
    return TRUE;
}


/**
 * Set and init all of connection cofiguration defults.  
 * @param  generalConnInfo [input] :general connection information
 * @see    displayMessageBox().
 * @see    initGeneralConnectionCountDefaults().
 * @see    initGeneralConnectionTPDUNIIDefaults().
 * @see    initGeneralConnectionMessageNiiDefaults().
 * @return True
 */
uint8 initGeneralConnectionDefaults(generalConnInfoST* generalConnInfo)
{
    displayMessageBox("����� ��� ��� �� general", MSG_INFO);
    initGeneralConnectionCountDefaults(generalConnInfo);
    initGeneralConnectionTPDUNIIDefaults(generalConnInfo);
    initGeneralConnectionMessageNiiDefaults(generalConnInfo);
    return TRUE;
}


/** ------------------------------------------------------------------------ **/
/**                              HDLC  CONFIG                                **/
/** ------------------------------------------------------------------------ **/ 

/**
 * Set HDLC connection phone defaults.
 * @param  HDLCConnInfo [input] : information of HDLC connection
 * @see    displayMessageBox().
 * @return True.
 */
uint8 initHDLCConnectionPhoneDefaults(HDLCConnInfoST* HDLCConnInfo)
{
    displayMessageBox("����� ��� ��� �� phone", MSG_INFO);
    strcpy(HDLCConnInfo->phone, HDLC_CONNECTION_PHONE_DEFAULT);
    return TRUE;
}

/**
 * Set HDLC connection prefix defaults.
 * @param HDLCConnInfo [input] : information of HDLC connection
 * @see    displayMessageBox().
 * @return True
 */
uint8 initHDLCConnectionPrefixDefaults(HDLCConnInfoST* HDLCConnInfo)
{
// --MRF_NEW   displayMessageBox("����� ��� ��� �� prefix", MSG_INFO);
    strcpy(HDLCConnInfo->prefix, HDLC_CONNECTION_PREFIX_DEFAULT);
    return TRUE;
}

/**
 * Set HDLC connection Use Tone dialing defaults.
 * @param HDLCConnInfo [input] : information of HDLC connection
 * @return True
 */
uint8 initHDLCConnectionUseToneDialingDefaults(HDLCConnInfoST* HDLCConnInfo)
{
    HDLCConnInfo->useToneDialing = HDLC_CONNECTION_DIALING_TONE_DEFAULT;
    return TRUE;
}

/**
 * Set HDLC connection retries default.
 * @param HDLCConnInfo [input] : information of HDLC connection
 * @return True.
 */
uint8 initHDLCConnectionRetriesDefaults(HDLCConnInfoST* HDLCConnInfo)
{
    HDLCConnInfo->retries = HDLC_CONNECTION_RETRIES_DEFAULT;
    return TRUE;
}

/**
 * Set HDLC connection communication timeout per m/s defaults.
 * @param HDLCConnInfo [input] : information of HDLC connection
 * @return True.
 */
uint8 initHDLCConnectionCommunicationTimeoutDefaults(HDLCConnInfoST* HDLCConnInfo)
{
    HDLCConnInfo->communicationTimeout = HDLC_CONNECTION_COMMUNICATION_TIMEOUT_DEFAULT;
    return TRUE;
}

/**
 * Set and initialize all of HDLC connection defaults.
 * @param HDLCConnInfo [input] : information of HDLC connection
 * @see    initHDLCConnectionPhoneDefaults().
 * @see    initHDLCConnectionPrefixDefaults().
 * @see    initHDLCConnectionUseToneDialingDefaults().
 * @see    initHDLCConnectionCommunicationTimeoutDefaults().
 * @return True.
 */
uint8 initHDLCConnectionDefaults(HDLCConnInfoST* HDLCConnInfo)
{
    displayMessageBox("����� ��� ��� �� HDLC", MSG_INFO);
    initHDLCConnectionPhoneDefaults(HDLCConnInfo);
    initHDLCConnectionPrefixDefaults(HDLCConnInfo);
    initHDLCConnectionUseToneDialingDefaults(HDLCConnInfo); 
    initHDLCConnectionUseToneDialingDefaults(HDLCConnInfo);
    initHDLCConnectionCommunicationTimeoutDefaults(HDLCConnInfo);
    return TRUE;
}


/** ------------------------------------------------------------------------ **/
/**                              LAN  CONFIG                                **/
/** ------------------------------------------------------------------------ **/ 

/**
 * Set and initialize Lan Connection Ip Address Defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionIpAddressDefaults(LANConnInfoST* LANConnInfo)
{
    strcpy(LANConnInfo->ipAddress, LAN_CONNECTION_IP_ADDRESS_DEFAULT);    
    return TRUE;
}

/**
 * Set and initialize Lan Connection TCP Port Defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionTcpPortDefaults(LANConnInfoST* LANConnInfo)
{
    LANConnInfo->tcpPort = LAN_CONNECTION_TCP_PORT_DEFAULT;
    return TRUE;
}

/**
 * Set and initialize Lan Connection Local Ip Address Defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionLocalIpAddressDefaults(LANConnInfoST* LANConnInfo)
{
    strcpy(LANConnInfo->localIpAddress, LAN_CONNECTION_LOCAL_IP_ADDRESS_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize Lan Connection Gateway Defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionGatewayDefaults(LANConnInfoST* LANConnInfo)
{
    strcpy(LANConnInfo->gateway, LAN_CONNECTION_GATEWAY_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize Lan Connection Network Mask Defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionNetworkMaskDefaults(LANConnInfoST* LANConnInfo)
{
    strcpy(LANConnInfo->networkMask, LAN_CONNECTION_NETWORK_MASK_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize Lan Connection DHCP defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @return True.
 */
uint8 initLanConnectionDhcpDefaults(LANConnInfoST* LANConnInfo)
{
    LANConnInfo->dhcp = LAN_CONNECTION_DHCP_DEFAULT;
    return TRUE;
}

//+HNO_980716
uint8 initLanConnectionDNSURLDefaults(LANConnInfoST* LANConnInfo)
{
#ifdef LIVE_SWITCH
    strcpy(LANConnInfo->URL, DNS_CONN_URL_STRING_LIVE);
#endif
    
#ifdef TEST_SWITCH
    strcpy(LANConnInfo->URL, DNS_CONN_URL_STRING_TEST);
#endif
    return TRUE;
}
/**
 * Set and initialize all of LAN connection defaults.
 * @param  LANConnInfo [input]: information of LAN connection
 * @see    initLanConnectionIpAddressDefaults().
 * @see    initLanConnectionTcpPortDefaults().
 * @see    initLanConnectionLocalIpAddressDefaults().
 * @see    initLanConnectionGatewayDefaults().
 * @see    initLanConnectionNetworkMaskDefaults().
 * @see    initLanConnectionDhcpDefaults(). 
 * @return True.
 */
uint8 initLanConnectionDefaults(LANConnInfoST* LANConnInfo)
{
    initLanConnectionIpAddressDefaults(LANConnInfo);
    initLanConnectionTcpPortDefaults(LANConnInfo);
    initLanConnectionLocalIpAddressDefaults(LANConnInfo);
    initLanConnectionGatewayDefaults(LANConnInfo);
    initLanConnectionNetworkMaskDefaults(LANConnInfo);
    initLanConnectionDhcpDefaults(LANConnInfo);
    return TRUE;
}


/** ------------------------------------------------------------------------ **/
/**                              PPP CONFIG                                  **/
/** ------------------------------------------------------------------------ **/ 

/**
 * Set and initialize PPP Connection Ip Address Defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initPPPConnectionIpAddressDefaults(PPPConnInfoST* PPPConnInfo)
{
    strcpy(PPPConnInfo->ipAddress, PPP_CONNECTION_IP_ADDRESS_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize PPP Connection TCP Port defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initPPPConnectionTcpPortDefaults(PPPConnInfoST* PPPConnInfo)
{
    PPPConnInfo->tcpPort = PPP_CONNECTION_TCP_PORT_DEFAULT;
    return TRUE;
}

/**
 * Set and initialize PPP Connection Phone defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initPPPConnectionPhoneDefaults(PPPConnInfoST* PPPConnInfo)
{
    displayMessageBox("����� ��� ��� �� phone", MSG_INFO);
    strcpy(PPPConnInfo->HDLCConnInfo.phone, PPP_CONNECTION_PHONE_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize PPP Connection Prefix defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initPPPConnectionPrefixDefaults(PPPConnInfoST* PPPConnInfo)
{
//  --MRF_NEW  displayMessageBox("����� ��� ��� �� prefix", MSG_INFO);
    strcpy(PPPConnInfo->HDLCConnInfo.prefix, PPP_CONNECTION_PREFIX_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize PPP Connection Retries defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True
 */
uint8 initPPPConnectionRetriesDefaults(PPPConnInfoST* PPPConnInfo)
{
    PPPConnInfo->HDLCConnInfo.retries = PPP_CONNECTION_RETRIES_DEFAULT;
    return TRUE;
}

/**
 * Set and initialize PPP Connection Communication Timeout defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initPPPConnectionCommunicationTimeoutDefaults(PPPConnInfoST* PPPConnInfo)
{
    PPPConnInfo->HDLCConnInfo.communicationTimeout = PPP_CONNECTION_COMMUNICATION_TIMEOUT_DEFAULT;
    return TRUE;
}

/**
 * Set and initialize PPP Connection User defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True
 */
uint8 initPPPConnectionUserDefaults(PPPConnInfoST* PPPConnInfo)
{
    strcpy(PPPConnInfo->PPPConfig.user, PPP_CONNECTION_USER_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize PPP Connection Password defaults.
 * @param PPPConnInfo [input]: information of PPP connection
 * @return True
 */
uint8 initPPPConnectionPasswordDefaults(PPPConnInfoST* PPPConnInfo)
{
    strcpy(PPPConnInfo->PPPConfig.password, PPP_CONNECTION_PASSWORD_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize all of PPP connection defaults.
 * @param  PPPConnInfo [input]: information of PPP connection
 * @see    initPPPConnectionIpAddressDefaults().
 * @see    initPPPConnectionTcpPortDefaults().
 * @see    initPPPConnectionPhoneDefaults().
 * @see    initPPPConnectionPrefixDefaults().
 * @see    initPPPConnectionRetriesDefaults().
 * @see    initPPPConnectionCommunicationTimeoutDefaults().
 * @see    initPPPConnectionUserDefaults().
 * @see    initPPPConnectionPasswordDefaults().
 * @return True.
 */
uint8 initPPPConnectionDefaults(PPPConnInfoST* PPPConnInfo)
{
    initPPPConnectionIpAddressDefaults(PPPConnInfo); 
    initPPPConnectionTcpPortDefaults(PPPConnInfo); 
    initPPPConnectionPhoneDefaults(PPPConnInfo);
    initPPPConnectionPrefixDefaults(PPPConnInfo);
    initPPPConnectionRetriesDefaults(PPPConnInfo); 
    initPPPConnectionCommunicationTimeoutDefaults(PPPConnInfo);
    initPPPConnectionUserDefaults(PPPConnInfo);
    initPPPConnectionPasswordDefaults(PPPConnInfo);
    return TRUE;
}


/** ------------------------------------------------------------------------ **/
/**                              GPRS CONFIG                                **/
/** ------------------------------------------------------------------------ **/ 

/**
 * Set and initialize GPRS Connection Ip address defaults.
 * @param GPRSConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initGPRSConnectionIpAddressDefaults(GPRSConnInfoST* GPRSConnInfo)
{
    strcpy(GPRSConnInfo->ipAddress, GPRS_CONNECTION_IPADDRESS_DEFAULT);
    return TRUE;
}



/**
 * Set and initialize GPRS Connection TCP Port defaults.
 * @param  GPRSConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initGPRSConnectionTcpPortDefaults(GPRSConnInfoST* GPRSConnInfo)
{
    uint8        model[20]     = {0};//MRF_NEW18
    
    getDeviceModel(&model[0]); //MRF_NEW18 //ABS_NEW1
    if (strcmp(model, "VEGA3000") != 0)
        GPRSConnInfo->tcpPort = GPRS_CONNECTION_TCPPORT_DEFAULT;
    else
         GPRSConnInfo->tcpPort = 12001;
    return TRUE;
}

/**
 * Set and initialize GPRS Connection APN defaults.
 * @param  GPRSConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initGPRSConnectionApnDefaults(GPRSConnInfoST* GPRSConnInfo)
{
    strcpy(GPRSConnInfo->apn, GPRS_CONNECTION_APN_DEFAULT);
    return TRUE;
}

/**
 * Set and initialize GPRS Connection PIN Defaults.
 * @param  GPRSConnInfo [input]: information of PPP connection
 * @return True.
 */
uint8 initGPRSConnectionPinDefaults(GPRSConnInfoST* GPRSConnInfo)
{
    strcpy(GPRSConnInfo->pin, GPRS_CONNECTION_PIN_DEFAULT);
    return TRUE;
}
//+HNO_980721
uint8 initGPRSConnectionURLDefaults(GPRSConnInfoST* GPRSConnInfo)
{
#ifdef LIVE_SWITCH
    strcpy(GPRSConnInfo->URL, DNS_CONN_URL_STRING_LIVE);
#endif
#ifdef TEST_SWITCH
    strcpy(GPRSConnInfo->URL, DNS_CONN_URL_STRING_TEST);
#endif
    
    return TRUE;
}

/**
 * Set and initialize all of GPRS connection defults.
 * @param  GPRSConnInfo [input]: information of PPP connection
 * @see    initGPRSConnectionIpAddressDefaults().
 * @see    initGPRSConnectionTcpPortDefaults().
 * @see    initGPRSConnectionApnDefaults().
 * @see    initGPRSConnectionPinDefaults().
 * @return True.
 */
uint8 initGPRSConnectionDefaults(GPRSConnInfoST* GPRSConnInfo)
{
    initGPRSConnectionIpAddressDefaults(GPRSConnInfo); 
    initGPRSConnectionTcpPortDefaults(GPRSConnInfo); 
    initGPRSConnectionApnDefaults(GPRSConnInfo); 
    initGPRSConnectionPinDefaults(GPRSConnInfo);     
    return TRUE;
}


