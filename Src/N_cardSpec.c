#include <string.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_dateTime.h"
#include "N_cardSpec.h"
#include "N_cardSpecWrapper.h"
#include "N_scheduling.h"
#include "N_connection.h" 
#include "N_merchantSpec.h" 
#include "N_messaging.h" 
#include "N_fileManageWrapper.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_display.h"
#include "N_initialize.h"
#include "N_log.h"
#include "N_connectionWrapper.h" 

#define     CARD_INFO_FILE                          "cardinfo0100"

/**
 * This function insert data's of tracks to the structure and also it parse the informations. ( if lenght of tracks & data's tracks was in correct extent).
 * @param  files [input]: files
 * @param  cardTracks [input]: include card's informations
 * @param  cardSpec [output]: it fill with data's information which parse
 * @param  magnetCard [input]: magnet card
 * @see    displayMessageBox().
 * @see    processCardData().
 * @see    checkBINMaskan().
 * @return TRUE or FALSE (if insert datas are correct and parsing informations be correct ,return true,else return false)
 */
uint8 readTrackData(filesST* files, cardTracksST* cardTracks, cardSpecST* cardSpec, uint8 magnetCard)  
{
    terminalSpecST      terminalCapability  = getTerminalCapability();
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**readTrackData**");

    if (terminalCapability.modemCapability == TRUE)
    {
        setConnectionType(CONNECTION_HDLC);
        preDial(files,0);
    }
    
    if (cardTracks->track1Len == 0 && cardTracks->track2Len == 0 && cardTracks->track3Len == 0) 
    { 
        if (magnetCard)
            displayMessageBox("���� ���� ���� �� ����� �� Ș���", MSG_WARNING);
        else
            displayMessageBox("������� ���� �������", MSG_ERROR);
        
        return FALSE;
    } 
    
    if (!(cardTracks->track2Len <= MAX_TRACK2_BANK_LEN && 
    		cardTracks->track2Len >= MIN_TRACK2_BANK_LEN)) 
    {
        if (magnetCard)
            displayMessageBox("���� ���� ���� �� ����� �� Ș���", MSG_WARNING);
        else
            displayMessageBox("������� ���� �������", MSG_ERROR);
        
        return FALSE;
    }
    
    if (!processCardData(files, cardTracks, cardSpec))
        return FALSE;
        
    return TRUE;
}

/**
 * The function parse the raw data of card tracks. 
 * @param   cardTracks [input] : raw data of card specification
 * @param   cardSpec [output] : parse data of card and information
 * @see     showLog().
 * @see     comportHexShow().
 * @return  TRUE or FALSE (parse information,for example pan length,expDate,customerName ....)
 */
uint8 processCardData(filesST* files, cardTracksST* cardTracks, cardSpecST* cardSpec) 
{
    int                     i               = 0;          /* loop counter */
    int                     j               = 0;          /* loop counter */
    cardSpecST              card;                         /* bank card informatiom */
    uint8                   BIN[7]          = {0};
    uint8                   retvalue        = 0;
    supervisorSpec          supervisor;
    
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "processCardData");
    memset(&card, 0, sizeof(cardSpecST));                                       //be andazeh cardspecst fazaye khali be card ekhtesas midahad
    memset(&supervisor, 0, sizeof(supervisorSpec));
             
    strcpy(card.track2, cardTracks->track2);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "processCardData", "Track2:");   //track2 ra dar track1 copy mikonad
    comportHexShow(card.track2,sizeof(card.track2));
    
    card.track2Len = cardTracks->track2Len;                                     //andazeh track2 dar card rikhte mishavad
    card.PANLen = strcspn(cardTracks->track2, "=");                             //track 2 ra ta residan be = mipeymayad va be onvane andazeh pan gharar midahad
     
    memcpy(card.PAN, cardTracks->track2, card.PANLen);
    card.PAN[(int)card.PANLen] = 0;// entehaye araye ra ba null mibandad
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "processCardData", "PAN:");
    comportHexShow(card.PAN,sizeof(card.PAN));
    
    strncpy(card.expDate, cardTracks->track2 + card.PANLen + 1, 4);             //be tedade 4 carector hc panlen dar expdate copy mishavad
    card.expDate[4] = 0; 
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "processCardData", "EXP Date:");// entehaye araye ra ba null mibandad
    comportHexShow(card.expDate,sizeof(card.expDate));

    while (j < cardTracks->track1Len && cardTracks->track1[j] != 0x5e) 
        j++;
    j++;
    
    while ((j + i) < cardTracks->track1Len && i < 26 && cardTracks->track1[i + j] != 0x5e) 
    {
        card.customerName[i] = cardTracks->track1[i + j];
        i++;
    }
    
    card.customerName[i] = 0; 
    
    /*Get Information Of Supervisor*/
    strncpy(BIN, card.PAN, 6);
    if (strcmp(BIN,SUPERVISOR_BIN) == 0)
    {
        strncpy(card.personnelID, cardTracks->track2 + card.PANLen + 1 + 4 + 3 , 4);
        strcpy(supervisor.PAN, card.PAN);
        strcpy(supervisor.expDate, card.expDate);
        strcpy(supervisor.personnelID, card.personnelID);
        strncpy(supervisor.codeMeli, &card.PAN[6], 10);
        
        retvalue = updateFileInfo(SUPERVISOR_FILE, &supervisor, sizeof(supervisorSpec));
        if (retvalue != SUCCESS)
        {
            addSystemErrorReport(retvalue, ERR_TYPE_FILE_SYSTEM);
            return FALSE;
        }
        
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "process card data", "supervisor Pan: %s", supervisor.PAN);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "process card data", "supervisor EXP Date: %s", supervisor.expDate);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "process card data", "supervisor Person ID: %s", supervisor.personnelID);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "process card data", "supervisor Code Meli: %s", supervisor.codeMeli);
    }   
    
    *cardSpec = card;

    return TRUE;
}
/**
 * The function write parse information of card in corresponding file.
 * @param  cardSpec [input] :parse information of card
 * @see    showLog().
 * @see    updateFileInfo().
 * @see    addSystemErrorReport().
 * @return TRUE or FALSE (if it can write parse information in file returns true else return false).
 */
uint16 writeCardInfo(cardSpecST* cardSpec)  
{
    int16  retValue      = FAILURE;           /* write in file function return value */
    uint32 len           = sizeof(cardSpecST);      
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: cardSpec");
    retValue = updateFileInfo(CARD_INFO_FILE, cardSpec, len);    
    if (retValue != SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "write Card Info Err");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    return TRUE;
}
/**
 * The function read parse information of card from corresponding file.
 * @param  cardSpec [input]: parse information of card
 * @see    fileExist().
 * @see    readFileInfo().
 * @see    addSystemErrorReport().
 * @see    fileRemove().
 * @see    showLog().
 * @return TRUE or FALSE ( if it can read parse information from file returns true else return false).
 */
uint16 readCardInfo(cardSpecST* cardSpec)  
{
    int16  retValue     = FAILURE;           /* write in file function return value */
    uint32 len          = sizeof(cardSpecST);      

    if (fileExist(CARD_INFO_FILE) != SUCCESS)
        return FALSE;

    retValue = readFileInfo(CARD_INFO_FILE, cardSpec, &len);    
    if (retValue != SUCCESS)
    {
        showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #1");
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        return FALSE;
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: cardSpec");
    fileRemove(CARD_INFO_FILE);

    return TRUE;
}
/**
 * in this function permissible BINS for doing correct transaction will be check.
 * @param  files [input]:files
 * @param  cardSpec [input] :parse information of card
 * @see    addSystemErrorReport().
 * @see    showLog().
 * @see    fileRemove().
 * @see    displayMessageBox().
 * @see    closeFile().
 * @see    comportHexShow().
 * @return TRUE or FALSE (if BINS are permissible it returns success else return failure).
 */
uint16 checkBINMaskan(filesST* files, cardSpecST* cardSpec)
{
    uint16      retValue                = FAILURE;
    int         i                       = 0;       
    uint8       correctPAN              = FALSE;
    uint32      length                  = 0;    
    uint32      fileId                  = 0;               /* file handler */
    BINSpecST   BINSpec[MAX_BIN_COUNT]  = {0};
    
    memset(&BINSpec, 0, sizeof(BINSpecST) * MAX_BIN_COUNT);
    
    retValue = fileSize(files->binFile, &length);
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO5100
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif 

        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: binFile");
        fileRemove(files->binFile); 
        displayMessageBox("���� ������ ���� ����� ���� ������� ������", MSG_ERROR);
        return FALSE;
    }
    
    
    retValue = openFile(files->binFile, &fileId);
    if (retValue != SUCCESS)
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO5100
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif 

        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: binFile");
        fileRemove(files->binFile); 
        displayMessageBox("�����Ҙ��� ���� ����� ���� ������� ������", MSG_ERROR);
        return FALSE;
    }
    
     //mgh retValue = readFileInfo(files->binFile, &BINSpec, &length);
    retValue = readFixedFileInfo(fileId, &BINSpec, length, 0, MAX_BIN_COUNT); 
    if (retValue != SUCCESS)  
    {
        addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO5100
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif 
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: binFile");
        fileRemove(files->binFile); 
        displayMessageBox("���� ������ ���� ����� ���� ������� ������", MSG_ERROR);
        return FALSE;
    }
    
    closeFile(fileId); 
    
    i = 0;
    while (strlen(BINSpec[i].binBS) != 0)
    {
        comportHexShow(BINSpec[i].binBS, strlen(BINSpec[i].binBS));
        if (strncmp(BINSpec[i].binBS, cardSpec->PAN, strlen(BINSpec[i].binBS)) <= 0 && 
                strncmp(BINSpec[i].binES, cardSpec->PAN, strlen(BINSpec[i].binES)) >= 0)
        {
            correctPAN = TRUE;
            break;
        }
        i++;
    }
    if (!correctPAN)
    {
        displayMessageBox("��� ���� �� ��� ��� ������ ���� ������� ����", MSG_ERROR);
        return FALSE;         
    }
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "check BIN OK");
    return TRUE;
}

