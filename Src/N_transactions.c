#include <string.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_dateTime.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_connectionWrapper.h"
#include "N_messaging.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
#include "N_config.h"
#include "N_getUserInput.h" 
#include "N_printer.h" 
#include "N_transactionReport.h"
#include "N_transactions.h"
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_displayWrapper.h"
#include "N_display.h"
#include "N_initialize.h"
#include "N_barCode.h"
#include "N_log.h" 
#include "N_printerWrapper.h"
#include "N_PCPOS.h"
#include "N_userManagement.h"//ABS:ADD
#include "N_dateTimeWrapper.h"//ABS:ADD


static int              chargeTypeCount	= 0; 
static buyChargeSpecST 	buyOfflineCharge[OPERATOR_COUNT];
static buyChargeSpecST 	buyCharge[99];

void    setBuyChargeFields(int index);
int8 	checkTransPrerequisites(filesST* files, int8 transactionType);
uint8 	createSaveReversal(filesST* files, messageSpecST* messageSpec);
void    buyMultiCharge(argumentListST* args, int chargeType, uint8* amount , uint8 index);
void 	getChargeType(uint8* buff, uint8 type, uint8* amount);
//void    buyMultiCharge(argumentListST* args, int index);

uint8 NotAvalableService(void)
{ 
    displayMessageBox("����� ���� ��� ��� ���� ���.", MSG_WARNING);
    return 0;
}

uint8 buyTrans(argumentListST* args) 
{
    filesST*                	files                           = (filesST*) (args->argumentName);
    messageSpecST*          	messageSpec                     = (messageSpecST*) (args->next->argumentName);
    uint8*                  	keyMenu                         = (uint8*) (args->next->next->argumentName);
    uint8 						request[MESSAGE_BUFFER_SIZE] 	= {0};                          /** request message */
    uint16 						requestLen                      = 0;                            /** request message length */
    uint8 						response[MESSAGE_BUFFER_SIZE] 	= {0};                          /** response message */
    uint16 						responseLen 					= sizeof(response);             /** response message length */
    uint16 						checkResStatus 					= 0;
    uint16 						parseResStatus 					= 0;
    uint8 						known               			= FALSE; 			/** error is known or unknown */
    uint8 						errorMsg[70] 					= {0}; 				/** erropr message */
    uint8 						retValue            			= 0;
    uint8 						valueWithComma[64]				= {0};
    uint8 						readAmount              		= TRUE;
    uint8 						readPIN                 		= TRUE;
    uint32                      difSec              			= 0;
    uint16                      responceCode        			= 0;
    uint8                       shaparakState       			= 0;
    uint8                       logonFlag           			= FALSE;
    uint8                       key                 			= 0;
    uint8                       activeUserName[16]  			= {0};
    uint8                       depositID[13 + 1]   			= {0};
//    uint8                       mechantID[9 + 1]    			= {0};
    uint8                       isPOSStan                       = 0;
    terminalSpecST              terminalCapability				= getTerminalCapability();//MRF_NEW19
    cardSpecST                  cardSpec;
    dateTimeST                  localDateTime;
    messageSpecST               tempMessageSpec;

    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*****BUY TRANSACTION*****");

    if (getPOSService().shiftService == TRUE)
    {
        getActiveUser(activeUserName);
        if (!strcmp(activeUserName, "")) 
        {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Active User not Exist");
            displayMessageBox("���� ����� ���� �����.", MSG_ERROR);
            return FALSE;
        }

        if (getShiftChange() == TRUE)
        {
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Shift Changed");
            key = displayMessageBox("���� ����� ����� ����� ���!", MSG_CONFIRMATION);
            if (key == KBD_ENTER || key == KBD_CONFIRM)
                setShiftChange(FALSE);
            else
                return FALSE;
            }
	}
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
            return FALSE;
        
        if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
            return FALSE;
    }
    else
    {
        if (!readCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }
    
    displayWating();
    
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
//    if (!prePrint(TRUE, files)) -MRF_970903 : MOVE TO DOWN
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
    
    if (messageSpec == NULL) 
    {
        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
        messageSpec = &tempMessageSpec;
    }
    else
    {
        //the Amount fill in PCPOS
        if (strlen(messageSpec->amount) > 0)
            readAmount = FALSE;

        //the PIN fill in PCPOS
        if (strlen(messageSpec->cardSpec.PIN) > 0)
            readPIN = FALSE;
    }

    if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        supervisorTrans(files, messageSpec, cardSpec, keyMenu);
        return FALSE;
    } 
     
    if (readPIN)
    {
		
        if (!enterBuyInfo(files, messageSpec, cardSpec.PIN, readAmount, depositID))//#MRF_970806
        {
#ifdef VX520
			closeModem(TRUE);//ABS:ADD:961024
#endif
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
    	}
    }

//    if (!prePrint(TRUE, files)) //!MRF_970903: MOVE FROM UP   //HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
            
	messageSpec->transType = TRANS_BUY;

	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));

	if (!doConnect(files))
		return FALSE;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef  INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
    //--MRF_970806
    makeValueWithCommaStr(messageSpec->amount, valueWithComma); 
	strcat(valueWithComma, " ����");
    
	displayMessage("����", DISPLAY_START_LINE, ALL_LINES);
	displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);

	incAndSaveStan(files, &(messageSpec->merchantSpec));
    SIMGetIMSI(messageSpec->IMSI);
    
	requestLen = createBuyMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
    messageSpec->responseStatus = 95; //mgh: for managing reversal printer
	if (!createSaveReversal(files, messageSpec))
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		reversalTrans(files); 
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, 
            response, responseLen, request,	requestLen);

    parseResStatus = parseBuyResponse87(response, responseLen, messageSpec);
   
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec->responseStatus = parseResStatus;
    }
    
	if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
	{
		displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
		disconnectAndClose(0);
		return FALSE;
	}
    
    shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
    if (messageSpec->responseStatus == 99)
    {  
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
            return FALSE;
        setLogonScheduleAfterUnsuccessLogon(files);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
        fileRemove(files->settelmentInfoFile);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState != SUCCESS_TRANS");
        memset(errorMsg, 0, sizeof(errorMsg));              
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        disconnectAndClose(0);
    } 
    else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState == INTERNAL_ERROR");
        memset(errorMsg, 0, sizeof(errorMsg));
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        
        isPOSStan = TRUE; //MRF_NEW20
        if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
        {
            displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return FALSE;
        }
        
		reversalTrans(files);
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
    }
	
    //Set time with switch          
    localDateTime = getDateTime();    
    //    messageSpec->dateTime = setNextTime(&(messageSpec->dateTime), 3, 30, 0);
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
            
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
        return FALSE;
    
    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        reversalTrans(files);
        disconnectAndClose(0);
        return FALSE;
    }
    
	if (!saveMessageSpec(files, messageSpec))
	{
        reversalTrans(files);
        disconnectAndClose(0);
        return FALSE;
	}
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "preAmount: %s", messageSpec->preAmount);
    if (!printTransactionBuy(files, messageSpec, TRUE, NO_REPRINT))
    {
        reversalTrans(files);
        disconnectAndClose(0);
        return FALSE;
    }
    
    if (getPOSService().merchantReceipt)
    {
        retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
        //HNO_IDENT8 change true to false
        PrinterAccess(FALSE);

        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
            printTransactionBuy(files, messageSpec, FALSE, NO_REPRINT);
    }
    
    fileRemove(files->customerReceiptFile);
	settlementTrans(files);
	disconnectAndClose(0);
    
//    if (*keyMenu)
//        disconnectModem(); -MRF_970924 : REMOVE 10S UP CONNECTION
    
	return TRUE;
}

uint8 billPayTrans(argumentListST* args) 
{
    filesST*                    files 							= (filesST*) (args->argumentName);
    uint8*                      barCode 						= (uint8*) (args->next->argumentName);
    messageSpecST*              messageSpec 					= (messageSpecST*) (args->next->next->argumentName);
    uint8*                      keyMenu             			= (uint8*) (args->next->next->next->argumentName);
    uint8                       request[MESSAGE_BUFFER_SIZE] 	= {0}; 						/** request message */
    uint16                      requestLen 						= 0; 						/** request message lenght */
    uint8                       response[MESSAGE_BUFFER_SIZE] 	= {0}; 						/** response message */
    uint16                      responseLen 					= sizeof(response); 		/** response message lenght */
    uint8                       retValue						= TRUE;		
    uint8                       retValueConfirm					= TRUE;		
    uint16                      checkResStatus 					= 0;		
    uint16                      parseResStatus 					= 0;		
    uint8                       known							= FALSE; 					/** error is known or unknown */
    uint8                       errorMsg[70] 					= {0}; 						/** erropr message */
    uint8                       billCount 						= 0;
    uint8                       transCounter 					= 0;
    int                         paymentContinue					= TRUE;
    uint8                       valueWithComma[20]				= {0};
    uint8                       key                 			= KBD_CANCEL;
    uint8                       first               			= TRUE;
    uint8                       shaparakState       			= 0;
    uint8                       logonFlag           			= FALSE;
    uint32                      difSec              			= 0;
    uint8                       isPOSStan                       = 0;
	terminalSpecST              terminalCapability				= getTerminalCapability();
    cardSpecST                  cardSpec;
    messageSpecST               tempMessageSpec;
    billSpecST                  billSpec[99];
    dateTimeST                  localDateTime;
    
	memset(&cardSpec, 0, sizeof(cardSpecST));
	memset(billSpec, 0, sizeof(billSpecST) * 99);
    memset(&localDateTime, 0, sizeof(dateTimeST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*******billPayTrans*******");
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
        { 
            return FALSE;
        }    

        if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
        {
            return FALSE;
        }
    }
    else  if (!*keyMenu)
    {
        if (!readCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }
    
    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
    displayWating();
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
//    if (!prePrint(TRUE, files)) -MRF_970903 : MOVE TO DOWN
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//		return FALSE;
//    }    
    
	if (messageSpec == NULL) 
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

	if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return FALSE;
    }

    while (billCount < 100) 
    {
#ifdef VX520
		if (Finish_dial == TRUE && OpenHDLC_Flag == FALSE && connectionStatus == DIALING && dialStatus == CONNECT_1200)
			OpenHDLC();
#endif
        if (!first)
        {
            key = displayMessageBox("������ ���� �� ������ �� ���Ͽ", MSG_CONFIRMATION);
            if (key != KBD_ENTER && key != KBD_CONFIRM)
            {
                clearDisplay(); 
                break;
            }
        }
        else
            first = FALSE;

        if (barCode[0] == TRUE) 
        {
            retValue = readMultiBarCodeBillPaymentInfo(cardSpec.PIN, billSpec, &billCount);
        } 
        else
        {
            retValue = enterMultiBillPaymentInfo(cardSpec.PIN, billSpec, &billCount);
        }

        if (retValue == FALSE)
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }

        if (retValue != NO_DISPLAY)
        {
            if (!displayBillPaymentInfo(billSpec, &billCount))
            {
                disconnectAndClose(1, NOT_SHOW_DC_MSG);
                return FALSE;
            }
        }
    }

    if (strcmp(cardSpec.PIN, "") == 0) 
    {
        memset(cardSpec.PIN, 0, 5);

        if (!getPass("", cardSpec.PIN, 4, 2))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE ;
        }
    }
    
//    if (!prePrint(TRUE, files)) //!MRF_970903: MOVE FROM UP //HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
    
	messageSpec->transType = TRANS_BILLPAY;
	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));
    
	if (!doConnect(files))
    {
		return FALSE;
    }

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	for (transCounter = 0; transCounter < billCount; transCounter++)
	{
		retValue = TRUE;
		memset(request, 0, sizeof(request));
		memset(response, 0, sizeof(response));
		paymentContinue = TRUE;
		requestLen = 0;
		responseLen = sizeof(response);

		makeValueWithCommaStr(billSpec[transCounter].billAmount, valueWithComma); 
		strcat(valueWithComma, " ����");
		displayMessage("������ ���", DISPLAY_START_LINE, ALL_LINES);
		displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);

		strcpy(messageSpec->amount, billSpec[transCounter].billAmount); 
		strcpy(messageSpec->billSpec.billAmount, billSpec[transCounter].billAmount);
		strcpy(messageSpec->billSpec.billID, billSpec[transCounter].billID);
		strcpy(messageSpec->billSpec.paymentID, billSpec[transCounter].paymentID);
		messageSpec->billSpec.type = billSpec[transCounter].type;

		incAndSaveStan(files, &(messageSpec->merchantSpec));
        SIMGetIMSI(messageSpec->IMSI);
		requestLen = createBillPayMessage87(files, request, messageSpec);
		if (requestLen <= 0)
		{
			displayMessageBox("���� �����", MSG_ERROR);
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
			return FALSE;
		}
        
        messageSpec->responseStatus = 95; //mgh: for managing reversal printer
        if (!createSaveReversal(files, messageSpec))
        {
            displayMessageBox("���� �����", MSG_ERROR);
            disconnectAndClose(0);
            return FALSE;
        }
    
		displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
		if (!connSendBuff(request, requestLen))
		{
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
            fileRemove(files->reversalReceiptFile);
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
            fileRemove(files->reversalTransFile);
			displayMessageBox("����� ������", MSG_ERROR);
            disconnectAndClose(0);
			return FALSE;
		}
        
		displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
		if (!connReceiveBuff(response, &responseLen))
		{
			displayMessageBox("������ ������", MSG_ERROR);
//            disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
            reversalTrans(files);
            disconnectAndClose(0);
			return FALSE;
		}

        messageSpec->responseStatus = 0;
        checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, response, responseLen, request,
        requestLen);
        
        parseResStatus = parseBillPayResponse87(response, responseLen, messageSpec);
        if (checkResStatus != SUCCESS)
        {
            known = FALSE;
            messageSpec->responseStatus = checkResStatus;
        }
        else if (parseResStatus != SUCCESS)
        {
            messageSpec->responseStatus = parseResStatus;
        }
        
        if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
        {
            displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return FALSE;
        }
        
        shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
        if (messageSpec->responseStatus == 99)
        {  
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");
            if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
            {
                disconnectAndClose(1, NOT_SHOW_DC_MSG);
                return FALSE;
            }

            setLogonScheduleAfterUnsuccessLogon(files);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
            fileRemove(files->settelmentInfoFile);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
            fileRemove(files->reversalTransFile);
            getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
            addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
            displayMessageBox(errorMsg, MSG_ERROR);
            printReversal(files);
            logonFlag = TRUE;
            logonTrans(files);
        }
        else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
        {
            memset(errorMsg, 0, sizeof(errorMsg));
            getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
            displayMessageBox(errorMsg, MSG_ERROR);
            fileRemove(files->reversalTransFile);
            printReversal(files);
            if ((messageSpec->responseStatus == 94) || (messageSpec->responseStatus == 48) 
                    || (messageSpec->responseStatus == 51))
            {
                    paymentContinue = FALSE;
            }
            else
            {
                addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
                disconnectAndClose(0);
            }  
        }
        else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
        {
            memset(errorMsg, 0, sizeof(errorMsg));
//            disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
            
            isPOSStan = TRUE; //MRF_NEW20
            if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
            {
                displayMessageBox("���� �����", MSG_ERROR);
                reversalTrans(files);
                disconnectAndClose(0);
                return FALSE;
            }

            reversalTrans(files);
            getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
            addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
            displayMessageBox(errorMsg, MSG_ERROR);
            disconnectAndClose(0);
            return FALSE;
        }

    //Set time with switch          
    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                        
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

//mgh  if (difSec >= 60)
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
        
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
        {
            disconnectAndClose(0);
            return FALSE;
        }

        if (difSec >= 300)
        {
            displayMessageBox("������ ����� �� ����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return FALSE;
        }
        
		if (paymentContinue)
		{
			if (!saveMessageSpec(files, messageSpec))
			{
                reversalTrans(files);
                disconnectAndClose(0);
                return FALSE;
			}

			if (!printTransactionBillPay(files,messageSpec, TRUE, NO_REPRINT))
			{
                reversalTrans(files);
                disconnectAndClose(0);
                return FALSE;
			}
			
             if (getPOSService().merchantReceipt)//MRF_NEW19
            {
                retValueConfirm = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
                PrinterAccess(FALSE);
                if (retValueConfirm == KBD_ENTER || retValueConfirm == KBD_CONFIRM)//#MRF_971017
                    printTransactionBillPay(files, messageSpec, FALSE, NO_REPRINT);
             }
            
		}
    
        fileRemove(files->customerReceiptFile);
        if (settlementTrans(files) != TRANS_SUCCEED)
        {
            disconnectAndClose(0);
            return FALSE;
        }
	}
    
	disconnectAndClose(0);

	return TRUE;
}

uint8 balanceTrans(argumentListST* args) 
{
    filesST*            files 							= (filesST*) (args->argumentName);
    messageSpecST*      messageSpec 					= (messageSpecST*) (args->next->argumentName);
    uint8*              keyMenu         				= (uint8*) (args->next->next->argumentName);
    uint8               request[MESSAGE_BUFFER_SIZE] 	= {0}; 								/** request message */
    uint16              requestLen 						= 0; 								/** request message lenght */
    uint8               response[MESSAGE_BUFFER_SIZE] 	= {0}; 								/** response message */
    uint16              responseLen						= sizeof(response); 				/** response message lenght */
    uint16              responseStatus 					= 0;				
    uint8               key 							= 0;				
    uint8               known							= FALSE; 							/** error is known or unknown */
    uint8               errorMsg[70] 					= {0}; 								/** erropr message */
    uint32              difSec          				= 0;
    uint16              checkResStatus 					= 0;
    uint8               shaparakState   				= 0;
    uint8               logonFlag       				= FALSE;
    terminalSpecST      terminalCapability  			= getTerminalCapability();
    cardSpecST          cardSpec;
    messageSpecST       tempMessageSpec;
    dateTimeST          localDateTime;
    

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**balanceTrans**");
    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST)); 
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
        { 
            return FALSE;
        }    

        if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
        {
            return FALSE;
        }
    }
    else if (!*keyMenu)
    {
        if (!readCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }
        
    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
    displayWating();
    
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
    if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

	if (!enterBalanceInfo(cardSpec.PIN))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return FALSE;
    }

	messageSpec->transType = TRANS_BALANCE;
	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));

	if (!doConnect(files))
	{
		return FALSE;
	}

	if (checkTransPrerequisites(files, messageSpec->transType) != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������", DISPLAY_START_LINE, ALL_LINES);
	incAndSaveStan(files, &(messageSpec->merchantSpec));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "create msg...");
    SIMGetIMSI(messageSpec->IMSI);
    
	requestLen = createBalanceMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "check response...");
	checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType,
            response, responseLen, request, requestLen);
	
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "parse response...");
	responseStatus = parseBalanceResponse87(response, responseLen, messageSpec);
    
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (responseStatus != SUCCESS)
    {
        messageSpec->responseStatus = responseStatus;
    }
    
	shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
     if (messageSpec->responseStatus == 99)
    {  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
      
        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
	else if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "save msgSpec...");
	if (!saveMessageSpec(files, messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}
    
    localDateTime = getDateTime();    
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    //mgh    if (difSec >= 60)
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return FALSE;
    }
    
    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }
        
	key = displayBalance(messageSpec->balanceSpec.accountBalance, messageSpec->balanceSpec.withdrawalAmount);

	if (key == KBD_ENTER || key == KBD_CONFIRM)
		printBalanceReceipt(files, messageSpec);
    
	disconnectAndClose(0);
	return TRUE;
}


uint8 readReversalTrans(filesST* files, messageSpecST* messageSpec) 
{
	reversalTransST		reversalInfo;
	uint32 				sizeOfFile 	= sizeof(reversalTransST); 	/** size of reversal message file */
	int16 				retValue	= FAILURE; 					/** read file function return value */

	memset(&reversalInfo, 0, sizeof(reversalTransST));
	
	retValue = readFileInfo(files->reversalTransFile, &reversalInfo, &sizeOfFile);
	if (retValue != SUCCESS) 
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "retval File reversal = %d", retValue);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "err #5");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		return FALSE;
	}
    
    strcpy(messageSpec->cardSpec.PAN, reversalInfo.PAN);
    messageSpec->merchantSpec.recieveSTAN = reversalInfo.STAN;
    strcpy(messageSpec->amount, reversalInfo.amount);
    messageSpec->dateTime = reversalInfo.dateTime;
    messageSpec->SWdateTime = reversalInfo.SWdateTime;
    messageSpec->transType = reversalInfo.transType;
    messageSpec->billSpec = reversalInfo.billSpec;
    messageSpec->buyChargeSpec.type = reversalInfo.chargeType;
    strcpy(messageSpec->retrievalReferenceNumber, reversalInfo.retrievalReferenceNumber);
    strcpy(messageSpec->charity.InstituteCode, reversalInfo.charity.InstituteCode);

    return TRUE;
}

/**
 * save new STAN in file and merchant structure.
 * @param   merchantSpec   merchant specification.
 * @return  True or False
 */
void incAndSaveStan(filesST* files, merchantSpecST* merchantSpec) 
{
	if (!readMerchantSpec(files, merchantSpec))
		return;

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "incAndSaveStan");
    
	merchantSpec->STAN++;
    merchantSpec->recieveSTAN = merchantSpec->STAN;

	if (merchantSpec->STAN >= 1000000)
		merchantSpec->STAN = 1;

	writeMerchantSpec(files, *merchantSpec);
}


int8 checkTransPrerequisites(filesST* files, int8 transactionType) 
{
	dateTimeST 	logonScheduleDateTime;
	dateTimeST 	nowDateTime;
	
	memset(&logonScheduleDateTime, 0, sizeof(dateTimeST));
	memset(&nowDateTime, 0, sizeof(dateTimeST));
    

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkTransPrerequisites");

//	-- MRF_IDENT: NOT USED NOW if (transactionType == TRANS_ROLL_REQUEST  || transactionType == TRANS_ROLL_CONFIRM)
//		return TRUE;
    
	if (transactionType != TRANS_REVERSAL)
	{
		if (fileExist(files->reversalTransFile) == SUCCESS)
		{
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "reversal Exist");
			
			if (!reversalTrans(files))
            {
                disconnectAndClose(0);
				return UNSUCCESS_REVERSAL;
            }
		}
		if (transactionType != TRANS_SETTLEMENT)
		{
			if (fileExist(files->settelmentInfoFile) == SUCCESS)
			{
				showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Settelment Exist");
                if (settlementTrans(files) != TRANS_SUCCEED)
                {
                    disconnectAndClose(0);
                    return UNSUCCESS_SETTLEMENT;
                }
			}
		}
        if (transactionType != TRANS_LOGON && transactionType != TRANS_INITIALIZE)
		{
			if (fileExist(LOGON_INFO_FILE) == SUCCESS)
			{
				showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Logon Exist");
                if (logonTrans(files) != TRANS_SUCCEED)
                {
                    disconnectAndClose(0);
                    return UNSUCCESS_LOGON;
                }
			}
		}
	}
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "checkTransPrerequisites finished");
	return TRUE;
}

/**
 * creat & save reversal message & reversal receipt info.
 * @param   messageSpec transaction info.
 * @return  TRUE or FALSE.
 */
uint8 createSaveReversal(filesST* files, messageSpecST* messageSpec) 
{
	int16 					retValue		= FAILURE; 		/** file functions return value */
	reversalTransST 		reversalInfo; 					/** reversal receipt information */
    uint8                   transFlag       = TRUE;
    uint8                   recieptFlag     = TRUE;
    int8                    currenttime[7]  = {0};
    uint8                   stanStr[7]      = {0};

	memset(&reversalInfo, 0, sizeof(reversalTransST));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "createSaveReversal");

	reversalInfo.dateTime = messageSpec->dateTime;
    reversalInfo.SWdateTime = messageSpec->dateTime;
	reversalInfo.transType = messageSpec->transType;
	reversalInfo.responseStatus = messageSpec->responseStatus;
	reversalInfo.STAN = messageSpec->merchantSpec.STAN;
	strcpy(reversalInfo.amount, messageSpec->amount); 
	strcpy(reversalInfo.PAN, messageSpec->cardSpec.PAN);
    timeToStr(messageSpec->dateTime.time , currenttime);
    sprintf(stanStr, "%06lu", messageSpec->merchantSpec.STAN);
    sprintf(messageSpec->retrievalReferenceNumber,"%s%s",currenttime, stanStr);
    strcpy(reversalInfo.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    
    if (messageSpec->transType == TRANS_BUYCHARGE)
    {
        strcpy(reversalInfo.ChargeRealAmount, messageSpec->buyChargeSpec.realAmount); 
        reversalInfo.chargeType = messageSpec->buyChargeSpec.type; 
    }
    else if (messageSpec->transType == TRANS_TOPUP)   //MRF_TOPUP
    {
        strcpy(reversalInfo.topup.amount,messageSpec->topup.amount);
        reversalInfo.topup.type = messageSpec->topup.type;
        reversalInfo.topup.operatorType = messageSpec->topup.operatorType;
        strcpy(reversalInfo.topup.mobileNo,messageSpec->topup.mobileNo);
    }
    else if (messageSpec->transType == TRANS_BILLPAY)
    {
        strcpy(reversalInfo.billSpec.billID, messageSpec->billSpec.billID);
        strcpy(reversalInfo.billSpec.paymentID, messageSpec->billSpec.paymentID);
        reversalInfo.billSpec.type = messageSpec->billSpec.type;
        strcpy(reversalInfo.billSpec.billAmount, messageSpec->billSpec.billAmount); 
    }
    else if(messageSpec->transType == TRANS_CHARITY)
    	strcpy(reversalInfo.charity.InstituteCode, messageSpec->charity.InstituteCode);

    if (messageSpec->transType != TRANS_ETC && 
            messageSpec->transType != TRANS_LOANPAY && 
                 messageSpec->transType != TRANS_TOPUP)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: reversalTransFile");
        retValue = updateFileInfo(files->reversalTransFile, &reversalInfo, sizeof(reversalTransST));
        if (retValue != SUCCESS)
        {
            showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #6");
            addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
            transFlag = FALSE;
        }
    }

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE:reversalReceiptFile ");
	retValue = updateFileInfo(files->reversalReceiptFile, &reversalInfo, sizeof(reversalTransST));
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #7");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		recieptFlag = FALSE;
	}

    if (transFlag == FALSE || recieptFlag == FALSE)
        return FALSE;
    else
        return TRUE;
}

uint8 updateReversalInfo(filesST* files, messageSpecST* messageSpec,uint8 isPOSStan) //MRF_NEW20
{
	int16                   retValue        = FAILURE;                  /** file functions return value */
	uint32                  sizeOfFile      = sizeof(reversalTransST); 	/** size of reversal message file */
    uint8                   transFlag       = TRUE;
    uint8                   recieptFlag     = TRUE;
    reversalTransST 		reversalInfo;    
	memset(&reversalInfo, 0, sizeof(reversalTransST));                  /** reversal receipt information */
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*****updateReversalInfo*****");    
        
    retValue = readFileInfo(files->reversalTransFile, &reversalInfo, &sizeOfFile);
	if (retValue != SUCCESS) 
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "retval File reversal = %d", retValue);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "err #5");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		return FALSE;
	}

    if (!isPOSStan) //MRF_NEW20
        reversalInfo.STAN = messageSpec->merchantSpec.recieveSTAN;
    else
        reversalInfo.STAN = messageSpec->merchantSpec.STAN;
    
    reversalInfo.responseStatus = messageSpec->responseStatus;
    reversalInfo.SWdateTime = messageSpec->dateTime;// just for receipt & TRANSACTION UNSUCCESS LIST
    strcpy(reversalInfo.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    
    //for reversal transaction fields 11 should 
	retValue = updateFileInfo(files->reversalTransFile, &reversalInfo, sizeof(reversalTransST));
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #6");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		transFlag = FALSE;
	}

//    reversalInfo.dateTime = messageSpec->dateTime; //MRF_DEBUG just for receipt
	retValue = updateFileInfo(files->reversalReceiptFile, &reversalInfo, sizeof(reversalTransST));
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #7");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
		recieptFlag = FALSE;
	}
    
	if (transFlag == FALSE || recieptFlag == FALSE)
        return FALSE;
    else
        return TRUE;
}

uint8 reversalTrans(filesST* files) 
{
	uint8 			request[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** request message */
	uint16 			requestLen 						= 0; 		        	/** request message length */
	uint8 			response[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** response message */
	uint16 			responseLen 					= sizeof(response);		/** response message length */
	int 			responseCode 					= -1; 					/** response message response code(39) */
	uint8 			known							= FALSE; 		    	/** error is known or unknown */
	uint8 			errorMsg[70] 					= {0}; 					/** error message */
    int16           retValue        				= FAILURE;
	messageSpecST	messageSpec;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**ReversalTrans");
	memset(&messageSpec, 0, sizeof (messageSpecST));

	if (fileExist(files->reversalReceiptFile) == SUCCESS)
        printReversal(files);

    retValue = checkConnectionStatus();
	if (retValue != MODEM_HDLC_IS_CONNECTED 
            && retValue != LAN_IS_CONNECTED 
            && retValue != GPRS_IS_CONNECTED
            && retValue != MODEM_PPP_IS_CONNECTED) 
    {
        setReversalScheduleAfterUnsuccessReversal(files);
        return FALSE;
    }
        
    messageSpec.transType = TRANS_REVERSAL;
    
	if (checkTransPrerequisites(files, TRANS_REVERSAL) != TRUE)
	{
		setReversalScheduleAfterUnsuccessReversal(files);
		return FALSE;
	}
	
	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		//-- setLogonScheduleAfterUnsuccessLogon(files);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "log on");
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif

		//checkBillMenusVisibility(files);
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		return FALSE;
	}
	
	if (!readReversalTrans(files, &messageSpec))
	{
		displayMessageBox("��� �� ������ ����", MSG_ERROR);
		//setReversalScheduleAfterUnsuccessReversal(files);
		resetReversalSchedule(files);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
		return FALSE;
	}
	
	displayMessage("��ǘ�� �ѐ��", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);
	requestLen = createReversalMessage87(files, request, &messageSpec);
	if (requestLen <= 0)
	{
		setReversalScheduleAfterUnsuccessReversal(files);
		displayMessageBox("���� �����", MSG_ERROR);
		return FALSE;
	}
   
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
        displayMessageBox("����� ������ ��ǘ�� �ѐ��", MSG_ERROR);
		setReversalScheduleAfterUnsuccessReversal(files);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sent");
	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
        displayMessageBox("��ǘ�� �ѐ�� ������", MSG_ERROR);
		setReversalScheduleAfterUnsuccessReversal(files);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION16
        disconnectAndClose(0);//MRF_NEW16
		return FALSE;
	}

	responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, response, responseLen,
	request, requestLen);
    if ((responseCode == SUCCESS || responseCode == 25) &&   /*++MGH & HNO:MRF CHANGE NEW17 */
            (messageSpec.responseCode == 0 || messageSpec.responseCode == 25))
	{

		if (isScheduleSet(files, SCHEDULES_REVERSAL))
		{
			resetReversalSchedule(files);
		}
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
        addUnsuccessTransLog(&messageSpec);//MGH,HNO
	}
	else
	{
        known = FALSE;
        memset(errorMsg, 0, sizeof (errorMsg));
        getIsoErrorMessage(errorMsg, responseCode, &known);
        addTransactionErrorReport(TRANS_REVERSAL, responseCode, known);
        setReversalScheduleAfterUnsuccessReversal(files);
        return FALSE;
	}
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "reversal done");
    return TRUE;
}


//HNO_PCPOS
uint8 reversalTransPCPOS(filesST* files) 
{
    uint8 				request[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** request message */
    uint16 				requestLen 						= 0; 		        	/** request message length */
    uint8 				response[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** response message */
    uint16 				responseLen 					= sizeof(response);		/** response message length */
    int 				responseCode 					= -1; 					/** response message response code(39) */
    uint8 				known							= FALSE; 		    	/** error is known or unknown */
    uint8 				errorMsg[70] 					= {0}; 					/** error message */
    int16           	retValue        				= FAILURE;
    messageSpecST		messageSpec;
    merchantSpecST  	merchantSpec;

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "reversal trans");
    memset(&messageSpec, 0, sizeof (messageSpecST));

    if (fileExist(files->reversalReceiptFile) == SUCCESS)
        printReversal(files);

    retValue = checkConnectionStatus();
    if (retValue != MODEM_HDLC_IS_CONNECTED 
        && retValue != LAN_IS_CONNECTED 
        && retValue != GPRS_IS_CONNECTED
        && retValue != MODEM_PPP_IS_CONNECTED) 
    {
        openModem(TRUE, d_M_MODE_SDLC_FAST, d_M_HANDSHAKE_V22_ONLY);
        doConnect(files);
    }
    
    messageSpec.transType = TRANS_REVERSAL;
    
	if (checkTransPrerequisites(files, TRANS_REVERSAL) != TRUE)
	{
		setReversalScheduleAfterUnsuccessReversal(files);
		return FALSE;
	}
	
	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "log on");
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif

		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		return FALSE;
	}
	
	if (!readReversalTrans(files, &messageSpec))
	{
		displayMessageBox("��� �� ������ ����", MSG_ERROR);
		//setReversalScheduleAfterUnsuccessReversal(files);
		resetReversalSchedule(files);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
		return FALSE;
	}
	
	displayMessage("��ǘ�� �ѐ��", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);
	requestLen = createReversalMessage87(files, request, &messageSpec);
	if (requestLen <= 0)
	{
		setReversalScheduleAfterUnsuccessReversal(files);
		displayMessageBox("���� �����", MSG_ERROR);
		return FALSE;
	}
   
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
        displayMessageBox("����� ������ ��ǘ�� �ѐ��", MSG_ERROR);
		setReversalScheduleAfterUnsuccessReversal(files);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "sent");
	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
        displayMessageBox("��ǘ�� �ѐ�� ������", MSG_ERROR);
		setReversalScheduleAfterUnsuccessReversal(files);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);//MRF_NEW16
		return FALSE;
	}

	responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, response, responseLen,
	request, requestLen);
    if ((responseCode == SUCCESS || responseCode == 25) &&   /*++MGH & HNO:MRF CHANGE NEW17 */
            (messageSpec.responseCode == 0 || messageSpec.responseCode == 25))
	{

		if (isScheduleSet(files, SCHEDULES_REVERSAL))
		{
			resetReversalSchedule(files);
		}
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
        addUnsuccessTransLog(&messageSpec);//MGH,HNO
	}
	else
	{
        known = FALSE;
        memset(errorMsg, 0, sizeof (errorMsg));
        getIsoErrorMessage(errorMsg, responseCode, &known);
        addTransactionErrorReport(TRANS_REVERSAL, responseCode, known);
        setReversalScheduleAfterUnsuccessReversal(files);
        return FALSE;
	}
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "reversal done");
    return TRUE;
}


uint8 settlement(argumentListST* args)
{
    filesST*        files               = (filesST*) (args->argumentName);
    uint8           settlemented        = FALSE;
    cardSpecST      cardSpec;
    
    memset(&cardSpec, 0, sizeof(cardSpecST));
        
    if (fileExist(files->settelmentInfoFile) != SUCCESS)
    {
        displayMessageBox("������� ���� ���", MSG_INFO);
        return settlemented;
    }
    
    if (!doConnect(files))
        return settlemented;
    
    settlemented = settlementTrans(files);
    
    if (settlemented == TRANS_SUCCEED)
        displayMessageBox("���� ���� �� ������ ����� ��.", MSG_INFO);
    
    disconnectAndClose(0);
    
    return settlemented;
}

uint16 settlementTrans(filesST* files)
{
	uint8 				request[MESSAGE_BUFFER_SIZE]    = {0}; 					/** request message */
	uint16 				requestLen              		= 0; 					/** request message lenght */
	uint8 				response[MESSAGE_BUFFER_SIZE]   = {0}; 					/** response message */
	uint16 				responseLen             		= sizeof(response); 	/** response message lenght */
	uint16 				responseStatus          		= 0;
	uint8 				known                   		= FALSE; 				/** error is known or unknown */
	uint8 				errorMsg[70]            		= {0}; 					/** error message */
    uint8               shaparakState           		= 0;
    int                 parseResponseResult     		= -1;
    int                 responseCode            		= -1;
    int16               retValue                		= FAILURE;       
    dateTimeST 			localDateTime;
	messageSpecST 		messageSpec;
	settlementSpecST	settlementSpec; 

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****SETTLEMENT TRANS*****");
    
	memset(&messageSpec, 0, sizeof(messageSpecST));
	memset(&localDateTime, 0, sizeof(dateTimeST));
	memset(&settlementSpec, 0, sizeof(settlementSpecST));

	if (fileExist(files->settelmentInfoFile) != SUCCESS)
	{
		displayMessageBox("��� �� ������ ���� �������!", MSG_INFO);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "settlement file not exist");
        resetSettlementSchedule(files);
		return FALSE;
	}
    
    retValue = checkConnectionStatus();
	if (retValue != MODEM_HDLC_IS_CONNECTED 
            && retValue != LAN_IS_CONNECTED 
            && retValue != GPRS_IS_CONNECTED
            && retValue != MODEM_PPP_IS_CONNECTED) 
    {
         setSettlementScheduleAfterUnsuccessSettlement(files);
         return FALSE;
    }
        
    
	messageSpec.transType = TRANS_SETTLEMENT;
	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif

		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		return UNSUCCESS_TRANS_NOT_SENT;
	}

	if (!readSettlementSpec(files, &settlementSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "readSettlementSpec not success");
        setSettlementScheduleAfterUnsuccessSettlement(files);
        displayMessageBox("��� �� ������ ������� �������", MSG_ERROR);
        return FALSE;
    }
    
    messageSpec.settlementSpec.dateTime = settlementSpec.dateTime;
    strcpy(messageSpec.settlementSpec.PAN, settlementSpec.PAN); 
    messageSpec.settlementSpec.transType = settlementSpec.transType;
    strcpy(messageSpec.settlementSpec.transAmount, settlementSpec.transAmount);
    messageSpec.settlementSpec.STAN = settlementSpec.STAN;
    strcpy(messageSpec.settlementSpec.retrievalReferenceNumber, settlementSpec.retrievalReferenceNumber);

	displayMessage("��ǘ�� �������", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);
	requestLen = createSettlementMessage87(files, request, &messageSpec);
	if (requestLen <= 0)
	{
		setSettlementScheduleAfterUnsuccessSettlement(files);
		displayMessageBox("���� �����", MSG_ERROR);
		return UNSUCCESS_TRANS_NOT_SENT;
	}
    
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		setSettlementScheduleAfterUnsuccessSettlement(files);
		displayMessageBox("����� ������� ������", MSG_ERROR);
		return UNSUCCESS_TRANS_NOT_SENT;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		setSettlementScheduleAfterUnsuccessSettlement(files);
		displayMessageBox("������ ������� ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);//MRF_NEW16
		return UNSUCCESS_TRANS_NOT_RECEIVED;
	}

	responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, 
            response, responseLen, request, requestLen);
	
	parseResponseResult = parseSettlementResponse87(response, responseLen, &messageSpec);
    
    if (responseCode != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = responseCode;
    }
    else if (parseResponseResult != SUCCESS)
    {
        messageSpec.responseStatus = parseResponseResult;
    }

    shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
    if (shaparakState != SUCCESS_TRANS)
    {
        known = FALSE;
        memset(errorMsg, 0, sizeof(errorMsg));

        setSettlementScheduleAfterUnsuccessSettlement(files);
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        return UNSUCCESS_TRANS_RESPONSE_ERROR;
    }

	resetSettlementSchedule(files);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
	fileRemove(files->settelmentInfoFile);

	return TRANS_SUCCEED;
}


uint8 initializeTrans(argumentListST* args) 
{
	filesST* 		files 							= (filesST*) (args->argumentName);
	uint8 			request[MESSAGE_BUFFER_SIZE] 	= {0};                      /** request message */
	uint16 			requestLen 						= 0;                        /** request message lenght */
	uint8 			response[MESSAGE_BUFFER_SIZE] 	= {0};                      /** response message */
	uint16 			responseLen 					= sizeof(response);         /** response message lenght */
	int8 			checkPrerequisites				= 0;                        /** check transaction prequisite result */
	int 			responseCode 					= -1;                       /** response message response code(39) */
	uint8 			known							= FALSE;                    /** error is known or unknown */
	uint8 			errorMsg[70] 					= {0};
	int 			parseResponseResult 			= -1;
	uint8 			key								= KBD_CANCEL;
    uint32          difSec              			= 0;
    uint8           shaparakState       			= 0;
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
	messageSpecST   messageSpec;
    dateTimeST      localDateTime;
      
	memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*****INITIALIZE TRANSACTION*****");
    
    
	if (isInitialized(files))
	{
		key = displayMessageBox("����� ������� ���Ͽ", MSG_CONFIRMATION);
		if (key != KBD_ENTER && key != KBD_CONFIRM)
			return FALSE;
	}
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
	if (!doConnect(files))
        return FALSE;

	messageSpec.transType = TRANS_INITIALIZE;

	checkPrerequisites = checkTransPrerequisites(files, messageSpec.transType);
	if (checkPrerequisites != TRUE)
	{
		if (checkPrerequisites == UNSUCCESS_REVERSAL)
			displayMessageBox("��ǘ�� �ѐ�� ������", MSG_ERROR);
		else if (checkPrerequisites == UNSUCCESS_SETTLEMENT)
			displayMessageBox("��ǘ�� ������ ������", MSG_ERROR);
//		else if (checkPrerequisites == UNSUCCESS_LOGON)//--MRF_NEW3
//			displayMessageBox("����� ���� ������", MSG_ERROR);

		disconnectAndClose(0);

		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: masterKeyFile");
		displayMessageBox("����� ������� �� ����� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    //HNO_INIT
    addInitializeReport(&messageSpec, FALSE);
	displayMessage("�������", DISPLAY_START_LINE, ALL_LINES);

	incAndSaveStan(files, &(messageSpec.merchantSpec));
    SIMGetIMSI(messageSpec.IMSI);
	requestLen = createInitializeMessage87(request, &messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
        
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ResponseLen = %d", responseLen);

    responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, 
            response, responseLen, request,requestLen);

	parseResponseResult = parseInitializeResponse87(response, responseLen, &messageSpec);
    if (responseCode != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = responseCode;
    }
    else if (parseResponseResult != SUCCESS)
    {
        messageSpec.responseStatus = parseResponseResult;
    }
    shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
	
    if (shaparakState != SUCCESS_TRANS)
    {
			memset(errorMsg, 0, sizeof(errorMsg));
            known = FALSE;
            getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
            addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
            displayMessageBox(errorMsg, MSG_ERROR);
            disconnectAndClose(0);
            return FALSE;
    }
    //Set time with switch          
    localDateTime = getDateTime();   
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }
        
	if (!saveMessageSpec(files, &messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}
        //HNO_INIT
    addInitializeReport(&messageSpec, TRUE);
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PIN version is :%s",messageSpec.versionKey.pinVersion);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "MAC version is :%s",messageSpec.versionKey.macVersion);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Name: %s", messageSpec.merchantSpec.marketName);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Address: %s", messageSpec.merchantSpec.marketAddress);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PostalCode: %s", messageSpec.merchantSpec.postalCodeMarket);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Phone: %s", messageSpec.merchantSpec.merchantPhone);

    successInitializeReport(files, &messageSpec);//HNO_INIT

	displayMessageBox("������� ����� ��", MSG_INFO);
	disconnectAndClose(0);
   
	return TRUE;
}


uint8 preTransInitialize(filesST* files) 
{
    uint8 			request[2048] 		= {0}; 				/** request message */
	uint16 			requestLen 			= 0; 				/** request message lenght */
	uint8 			response[2048] 		= {0}; 				/** response message */
	uint16 			responseLen 		= sizeof(response); /** response message lenght */
	int 			responseCode 		= -1; 				/** response message response code(39) */
	uint8 			known				= FALSE; 			/** error is known or unknown */
	uint8 			errorMsg[70] 		= {0};
	int 			parseResponseResult = -1;
	messageSpecST   messageSpec;
	dateTimeST 		dateTime;

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "preTransInitialize");
	memset(&messageSpec, 0, sizeof(messageSpecST));

	messageSpec.transType = TRANS_INITIALIZE;

	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		displayMessageBox("����� ������� ������� �� ����� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
	displayMessage("�������", DISPLAY_START_LINE, ALL_LINES);

	if (fileExist(files->reversalTransFile) == SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
	}

	if (fileExist(files->settelmentInfoFile) == SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: settelmentInfoFile");
		fileRemove(files->settelmentInfoFile);
	}

	requestLen = createInitializeMessage87(request, &messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("���� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseLen =%d", responseLen);

	responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, response, responseLen, request,
	requestLen);
	if (responseCode != 0)
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseCode is <%d>", responseCode);

		//there is a problem in POS terminal, reConfigure
		if (messageSpec.responseCode == 90)
        
        #ifdef INGENICO5100
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		
		known = FALSE;
		memset(errorMsg, 0, sizeof(errorMsg));
		getIsoErrorMessage(errorMsg, responseCode, &known);
		addTransactionErrorReport(messageSpec.transType, responseCode, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	parseResponseResult = parseInitializeResponse87(response, responseLen, &messageSpec);
	if (parseResponseResult != PARSE_NO_ERROR)
	{
		known = FALSE; /** error is known or unknown */
		memset(errorMsg, 0x00, sizeof(errorMsg));

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "parseResponseResult is :%d", parseResponseResult);

		getIsoErrorMessage(errorMsg, parseResponseResult, &known);
		addTransactionErrorReport(messageSpec.transType, responseCode, known);

		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	if (!saveMessageSpec(files, &messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}

	memset(&dateTime, 0, sizeof(dateTimeST));
	successInitializeReport(files, &messageSpec);//#MRF_970807

	return TRUE;
}

uint16 readSettlementSpec(filesST* files, settlementSpecST* settlementSpec) 
{
	int16 	retValue	= FAILURE; 					/** file functions return value */
	uint32 	sizeOfFile 	= sizeof(settlementSpecST); /** size of settlement file */

	retValue = readFileInfo(files->settelmentInfoFile, settlementSpec,
			&sizeOfFile);

	if (retValue != SUCCESS)
		memset(settlementSpec, 0, sizeOfFile);

	return (retValue == SUCCESS) ? TRUE : FALSE;
}

uint8 writeSettlementSpec(filesST* files, settlementSpecST* settlementSpec) 
{
	int16 retValue= FAILURE;        /** file functions return value */

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: settelmentInfoFile");
    
	retValue = updateFileInfo(files->settelmentInfoFile, settlementSpec, sizeof(settlementSpecST));
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #10");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
	}

	return (retValue == SUCCESS) ? TRUE : FALSE;
}

uint16 addTransToSettlementFile(filesST* files, messageSpecST* messageSpec) 
{
	settlementSpecST settlementSpec;
    uint8            amount[12 + 1]     = {0};

	memset(&settlementSpec, 0, sizeof(settlementSpecST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Add trans to settlement");
    
	readSettlementSpec(files, &settlementSpec); 
    strcpy(amount,messageSpec->amount);
    
	//mgh: implementation add 2string here 
	sumStringNumbers(amount, settlementSpec.debitAmount, settlementSpec.debitAmount); 
	sumStringNumbers(amount, settlementSpec.netReconciliationAmount, settlementSpec.netReconciliationAmount); 
    strcpy(messageSpec->settlementSpec.PAN, messageSpec->cardSpec.PAN); 
    messageSpec->settlementSpec.transType = messageSpec->transType;
    strcpy(messageSpec->settlementSpec.transAmount, messageSpec->amount);
    messageSpec->settlementSpec.STAN = messageSpec->merchantSpec.recieveSTAN;
    strcpy(messageSpec->settlementSpec.retrievalReferenceNumber, messageSpec->retrievalReferenceNumber);
    
    if(!writeSettlementSpec(files, &(messageSpec->settlementSpec)))
            return FALSE;
    
	return TRUE;
}


/*************************** BUY MULTI CHARGE TRANSACTION *****************************/

uint8 buyMultiChargeMCI10000(argumentListST* args) 
{
    buyMultiCharge(args, MULTI_MCI, "10000" , MCI_10000);
//    buyMultiCharge(args, MCI_10000);
    return TRUE;
}


uint8 buyMultiChargeMCI20000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_MCI, "20000", MCI_20000);
//    buyMultiCharge(args, MCI_20000);
	return TRUE;
}


uint8 buyMultiChargeMCI50000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_MCI, "50000", MCI_50000);
//    buyMultiCharge(args, MCI_50000);
	return TRUE;
}


uint8 buyMultiChargeMCI100000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_MCI, "100000", MCI_100000);
//    buyMultiCharge(args, MCI_100000);
	return TRUE;
}

uint8 buyMultiChargeMCI200000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_MCI, "200000", MCI_200000);
//    buyMultiCharge(args, MCI_200000);
	return TRUE;
}


uint8 buyMultiChargeMCI500000(argumentListST* args)
{
	buyMultiCharge(args, MULTI_MCI, "500000", MCI_500000);
//    buyMultiCharge(args, MCI_200000);
	return TRUE;
}
 uint8 buyMultiChargeIrancell10000(argumentListST* args ) 
{
//    buyMultiCharge(args, MTN_10000);
    buyMultiCharge(args, MULTI_IRANCELL, "10000", MTN_10000);
    return TRUE;
}


uint8 buyMultiChargeIrancell20000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_IRANCELL, "20000", MTN_20000);
//    buyMultiCharge(args, MTN_20000);
	return TRUE;
}


uint8 buyMultiChargeIrancell50000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_IRANCELL, "50000", MTN_50000);
//    buyMultiCharge(args, MTN_50000);
	return TRUE;
}


uint8 buyMultiChargeIrancell100000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_IRANCELL, "100000", MTN_100000);
//    buyMultiCharge(args, MTN_100000);
	return TRUE;
}


uint8 buyMultiChargeIrancell200000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_IRANCELL, "200000", MTN_200000);
//    buyMultiCharge(args, MTN_200000);
	return TRUE;
}


uint8 buyMultiChargeIrancell500000(argumentListST* args)
{
	buyMultiCharge(args, MULTI_IRANCELL, "500000", MTN_500000);
//    buyMultiCharge(args, MTN_200000);
	return TRUE;
}

uint8 buyMultiChargeRightel20000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_RIGHTEL, "20000", RTL_20000);
//    buyMultiCharge(args, RTL_20000);
	return TRUE;
}

uint8 buyMultiChargeRightel50000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_RIGHTEL, "50000", RTL_50000);
//    buyMultiCharge(args, RTL_50000);
	return TRUE;
}

uint8 buyMultiChargeRightel100000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_RIGHTEL, "100000", RTL_100000);
//    buyMultiCharge(args, RTL_100000);
	return TRUE;
}

uint8 buyMultiChargeRightel200000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_RIGHTEL, "200000", RTL_200000);
//    buyMultiCharge(args, RTL_200000);
	return TRUE;
}

uint8 buyMultiChargeRightel500000(argumentListST* args) 
{
	buyMultiCharge(args, MULTI_RIGHTEL, "500000", RTL_500000);
//    buyMultiCharge(args, RTL_500000);
	return TRUE;
}


void buyChargeTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int* multiTypeCharge)  
{
    int 			counter                         = 0; 		/** loop counter */
    uint8 			request[MESSAGE_BUFFER_SIZE]    = {0}; 		/** request message */
    uint16 			requestLen                      = 0; 		/** request message lenght */
    uint8 			response[MESSAGE_BUFFER_SIZE]   = {0}; 		/** response message */
    uint16 			responseLen                     = 0; 		/** response message lenght */
    int 			responseCode                    = -1; 		/** response message response code(39) */
    int 			parseResponseResult             = -1; 		/** parse response message result */
    uint8 			printFirstPart                  = TRUE; 	/** for check print first part of transaction receipt */
    uint8 			known                           = FALSE; 	/** error is known or unknown */
    uint8 			errorMsg[70]                    = {0}; 		/** erropr message */
    int8 			retValue                        = 0;
    uint8 			PIN[4 + 1]                      = {0};
    uint32          difSec                          = 0;
    uint16 			checkResStatus                  = 0;
    uint8           ChargeTypeStr[30]               = {0};
    uint8           shaparakState                   = 0;
    uint8           logonFlag                       = FALSE;
    uint8           isPOSStan                       = 0;
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
    messageSpecST	tempMessageSpec;
    cardSpecST 		cardSpec;
    dateTimeST      localDateTime;
    uint32          timeStr;
    
    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }
    
    if (messageSpec == NULL) 
    {
        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
        messageSpec = &tempMessageSpec;
    }

	messageSpec->transType = TRANS_BUYCHARGE;

	if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return;
    }

    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }
    
    memset(PIN, 0, 5);
    if (!getPass("", PIN, 4, 2))
		return;

	if (!readCardInfo(&cardSpec))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return;
    }

	messageSpec->cardSpec = cardSpec;
	strcpy(messageSpec->cardSpec.PIN, PIN);

	if (!doConnect(files))
		return;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE) 
	{
		disconnectAndClose(0);
		return;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec)) 
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return;
    }

    /*save batch charge information in separate structure for retrieve charge report */
	messageSpec->buyChargeSpec.id = createID(messageSpec->dateTime.time);
    strcpy(messageSpec->charge.terminalID, messageSpec->merchantSpec.terminalID);
    strcpy(messageSpec->charge.PAN, messageSpec->cardSpec.PAN);
    
    resetVoucherData();
    
    messageSpec->buyChargeSpec.type = buyCharge[0].type;
    strcpy(messageSpec->buyChargeSpec.amount, buyCharge[0].amount);
    strcpy(messageSpec->amount, buyCharge[0].amount);

    /*for calculate the count of all Bought charges in one transaction */
    messageSpec->charge.chargeCount++;
    memset(request, 0, MESSAGE_BUFFER_SIZE);
    memset(response, 0, MESSAGE_BUFFER_SIZE);
    requestLen = 0;
    responseLen = sizeof(response);

    strcpy(messageSpec->buyChargeSpec.operatorId, buyCharge[0].operatorId);
    messageSpec->buyChargeSpec.productTypeCount = buyCharge[0].productTypeCount;

    displayMessage("", DISPLAY_START_LINE, ALL_LINES);
    displayMessage("���� ��ю", DISPLAY_START_LINE, NO_LINES);
    memset(ChargeTypeStr,0,sizeof(ChargeTypeStr));
    getChargeType(ChargeTypeStr, messageSpec->buyChargeSpec.type, messageSpec->buyChargeSpec.amount);
    displayMessage(ChargeTypeStr, DISPLAY_START_LINE + 1, NO_LINES);

    incAndSaveStan(files, &messageSpec->merchantSpec);
    SIMGetIMSI(messageSpec->IMSI); 
    requestLen = createBuyChargeMessage87(files, request, messageSpec);
    if (requestLen <= 0)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����1");
        displayMessageBox("���� �����", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }

    if (!createSaveReversal(files, messageSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����2");
        displayMessageBox("���� �����", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }

    displayMessage("����� �������...", DISPLAY_START_LINE + 3, NO_LINES);
    if (!connSendBuff(request, requestLen))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
        fileRemove(files->reversalReceiptFile);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        displayMessageBox("����� ������", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }

    displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connReceiveBuff(response, &responseLen))
    {
        displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem(); -MRF_970924 : REMOVE 10S UP CONNECTION
        reversalTrans(files);
        disconnectAndClose(0);
        return;
    }

    //Check response status of message
    messageSpec->responseStatus = 0; 
    responseCode = checkResponseValidity87(messageSpec, messageSpec->transType, 
            response, responseLen, request,requestLen);

    parseResponseResult = parseBuyChargeResponse87(response, responseLen, messageSpec);
    if (responseCode != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = responseCode;
    }
    else if (parseResponseResult != SUCCESS)
        messageSpec->responseStatus = parseResponseResult;


    if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����3");
        displayMessageBox("���� �����", MSG_ERROR);
        reversalTrans(files);
        disconnectAndClose(0);
        return;
    }

    shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
    if (messageSpec->responseStatus == 99)
    {  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return;
        }

        setLogonScheduleAfterUnsuccessLogon(files);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
        fileRemove(files->settelmentInfoFile);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
    {
        known = FALSE;
        memset(errorMsg, 0, sizeof(errorMsg));
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        disconnectAndClose(0);
    }  
    else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
    {
        memset(errorMsg, 0, sizeof(errorMsg));
//        disconnectModem(); -MRF_970924 : REMOVE 10S UP CONNECTION
                    
        isPOSStan = TRUE; //MRF_NEW20
        if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
        {
            displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return;//ABS:DELETE
        }

        reversalTrans(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
        displayMessageBox(errorMsg, MSG_ERROR);
        disconnectAndClose(0);
        return;
    }

    if (!saveMessageSpec(files, messageSpec))
    {
        reversalTrans(files); 
        disconnectAndClose(0);
        return;
    }

    parseResponseResult = parseField63ForBuyChargeResponse87(response, responseLen, messageSpec);
    if (parseResponseResult != SUCCESS)
    {
        reversalTrans(files);
        disconnectAndClose(0);
        return;
    }

    //Set time with switch          
    localDateTime = getDateTime();   
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);

    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        reversalTrans(files);
        disconnectAndClose(0);
        return;
    }

    if (counter == 0) 
        printFirstPart = TRUE;
    else
        printFirstPart = FALSE;

    if (!printTransactionBuyCharge(files, messageSpec, TRUE, NO_REPRINT))
    {
        reversalTrans(files);
        disconnectAndClose(0);
        return;
    }

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
    fileRemove(files->customerReceiptFile);

    if (settlementTrans(files) != TRANS_SUCCEED)
    {
        disconnectAndClose(0);
        return;
    }
    
    disconnectAndClose(0);
}

uint8 buyMultiChargeOfflineTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int index, int* multiTypeCharge) 
{
//    int 			counter 			= 0; 		/** loop counter */
//    int 			i 					= 0;
//    uint8 			request[2048] 		= {0}; 		/** request message */
//    uint16 			requestLen 			= 0; 		/** request message lenght */
//    uint8 			response[2048] 		= {0}; 		/** response message */
//    uint16 			responseLen 		= 0; 		/** response message lenght */
//    int 			responseCode 		= -1; 		/** response message response code(39) */
//    int 			parseResponseResult	= -1; 		/** parse response message result */
//    uint8 			printFirstPart		= TRUE; 	/** for check print first part of transaction receipt */
//    uint8 			known				= FALSE; 	/** error is known or unknown */
//    uint8 			errorMsg[70] 		= {0}; 		/** erropr message */
//    int8 			retValue 			= 0;
//    uint8 			PIN[4 + 1] 			= {0};
//    int             totalAmount         = 0;
//    messageSpecST	tempMessageSpec;
//    cardSpecST 		cardSpec;
//    dateTimeST      localDateTime;
//    uint32          difSec              = 0;
//    uint16 			checkResStatus 		= 0;
//    int             control             = 0;
//    uint8           check               = 0;
//    uint32          timeStr;
//
//    memset(&cardSpec, 0, sizeof(cardSpecST));
//    memset(&localDateTime, 0, sizeof(dateTimeST));
//    
//    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "buy Multi Charge Offline Trans");
//    
//    displayMessage("����� �������...", DISPLAY_START_LINE + 1, ALL_LINES);
//    preDial(files, 0);
//    
//    if (!printReversal(files)) 
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
//    
//    if (messageSpec == NULL) 
//    {
//        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
//        messageSpec = &tempMessageSpec;
//    }
//
//    if (!PrinterAccess(TRUE))
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
//
//    if (!enterBuyMultiChargeInfo(PIN, &buyCharge[index],chargeTypeCount, multiTypeCharge, TRUE))
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//		return FALSE;
//    }
//
//    if (*multiTypeCharge)
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG); 
//		return FALSE;
//    }
//
//	if (!readCardInfo(&cardSpec))
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//		return FALSE;
//    }
//
//    messageSpec->cardSpec = cardSpec;
//    strcpy(messageSpec->cardSpec.PIN, PIN);
//
//    if (!doConnect(files))
//		return FALSE;
//    
//    resetField24();
//    messageSpec->transType = TRANS_BUYCHARGE;
//    
//    retValue = checkTransPrerequisites(files, messageSpec->transType);
//    if (retValue != TRUE) 
//    {
//        disconnectAndClose(0);
//        return FALSE;
//    }
//    
//    if (!initMessageSpec(files, messageSpec->transType, messageSpec)) 
//    {
//        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
//		fileRemove(files->masterKeyFile);
//		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
//		disconnectAndClose(0);
//		return FALSE;
//    }
//    
//    timeStr = messageSpec->dateTime.time; // Just for TEST
//    
//    do
//    {
//        for (i = 0; i <= OPERATOR_COUNT; i++)
//        {
//            if (buyCharge[i].operatorId[0] == NULL)
//                continue;
//            messageSpec->buyChargeOfflineSpec[i].type = buyCharge[i].type; 
//            strcpy(messageSpec->buyChargeOfflineSpec[i].amount, buyCharge[i].amount); //mgh
//            strcpy(messageSpec->buyChargeOfflineSpec[i].operatorId, buyCharge[i].operatorId);
//            messageSpec->buyChargeOfflineSpec[i].productTypeCount = buyCharge[i].productTypeCount;
//            //Calculate Total Amount 
//            if (!check)
//            {
//                totalAmount = totalAmount + (atoi(messageSpec->buyChargeOfflineSpec[i].amount) * (buyCharge[i].productTypeCount));
//                sprintf(messageSpec->amount, "%d", totalAmount);
//                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Total Amount: %s",messageSpec->amount);
//            }
//        }
//        check++;
//        displayMessage("", DISPLAY_START_LINE, ALL_LINES);
//        displayMessage("���� ��ю", DISPLAY_START_LINE, NO_LINES);
//        
//        if (!control)
//        {
//            incAndSaveStan(files, &messageSpec->merchantSpec);
//            control++;
//        }
//        
//        messageSpec->dateTime.time = timeStr; // Just for TEST
//        
////        if (!createSaveReversal(files, messageSpec))
////        {
////            displayMessageBox("���� �����", MSG_ERROR);
////            disconnectAndClose(0);
////            return FALSE;
////        }
////        
//        requestLen = createBuyChargeOfflineMessage87(files, request, messageSpec);
//        if (requestLen <= 0)
//        {
//            displayMessageBox("���� �����", MSG_ERROR);
//            disconnectAndClose(0);
//            return FALSE;
//        }
//
//        displayMessage("����� �������...", DISPLAY_START_LINE + 3, NO_LINES);
//        if (!connSendBuff(request, requestLen))
//        {
//        //-- resetReversalSchedule(files);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
//            fileRemove(files->reversalReceiptFile);
//            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
//            fileRemove(files->reversalTransFile);
//            displayMessageBox("����� ������", MSG_ERROR);
//            disconnectAndClose(0);
//            return FALSE;
//        }
//
//        displayMessage("����� ������...", DISPLAY_START_LINE + 3, LINE_4);
//        if (!connReceiveBuff(response, &responseLen))
//        {
//            displayMessageBox("������ ������", MSG_ERROR);
////            reversalTrans(files);
//            disconnectAndClose(0);
//            return FALSE;
//        }
//
//        //Check response status of message
//        messageSpec->responseStatus = 0;
//        checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, 
//                response, responseLen, request,requestLen);
//        if (checkResStatus != SUCCESS)
//        {
//            known = FALSE;
//            messageSpec->responseStatus = checkResStatus;
//        }
//
//        parseResponseResult = parseBuyChargeResponse87(response, responseLen, messageSpec);
//        if (parseResponseResult != SUCCESS)
//            messageSpec->responseStatus = parseResponseResult;
//
////        if (!createSaveReversal(files, messageSpec))
////    	{
////    		displayMessageBox("���� �����", MSG_ERROR);
////            reversalTrans(files);
////    		disconnectAndClose(0);
////    		return FALSE;
////    	}
//        
//    }while (messageSpec->responseStatus == PARSE_REPEAT_BUY_CHARGE_TRANS );
//    
//	if (messageSpec->responseStatus != SUCCESS)
//	{
//        memset(errorMsg, 0, sizeof(errorMsg));
////        reversalTrans(files);                
//        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
//        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
//        displayMessageBox(errorMsg, MSG_ERROR);
//        disconnectAndClose(0);
//        return FALSE;
//	}   
//    
//    if (!saveMessageSpec(files, messageSpec))
//	{
////        reversalTrans(files); 
//        disconnectAndClose(0);
//        return FALSE;
//	}
//      
//    addTransToSettlementFile(files, messageSpec);    
//    settlementTrans(files);
//    strcpy(messageSpec->voucherKeyDecryptor, getVoucherKeyDecryptor());
//    
//    parseResponseResult = parseField63ForBuyChargeOfflineResponse87(response, responseLen, messageSpec);
//    if (parseResponseResult != SUCCESS)
//    {
////        reversalTrans(files);
//        disconnectAndClose(0);
//        return;
//    }
//    
//    resetVoucherData();
//
//    //Set time with switch          
//    localDateTime = getDateTime();    
////    messageSpec->dateTime = setNextTime(&(messageSpec->dateTime), 3, 30, 0);
//    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
//    difSec = (difSec >= 0) ? difSec : -difSec;
//
//    if (difSec >= 60)
//        setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
//
//    if (difSec >= 300)
//    {
//        displayMessageBox("������ ����� �� ����", MSG_ERROR);
////        reversalTrans(files);
//        disconnectAndClose(0);
//        return FALSE;
//    }
//
//    if (counter == 0) 
//        printFirstPart = TRUE;
//    else
//        printFirstPart = FALSE;
//    
////    if (!printTransactionBuyCharge(files, messageSpec, buyCharge[i].productTypeCount, printFirstPart, TRUE))
////    {
////        reversalTrans(files);
////        return;
////    }
////
////    retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
////    if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
////    {
////        prePrint(TRUE,files); //MRF
////        printTransactionBuyCharge(files, messageSpec,  buyCharge[i].productTypeCount, FALSE, FALSE);
////    }
////    
//    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
//    fileRemove(files->customerReceiptFile);
//    
//    fileRemove(files->reversalReceiptFile);//just for test
//    fileRemove(files->reversalTransFile);//just for test
//    disconnectAndClose(0);
}


void buyMultiCharge(argumentListST* args, int chargeType, uint8* amount , uint8 index)
{
    filesST*        files           			= (filesST*) (args->argumentName);
    messageSpecST* 	messageSpec 				= (messageSpecST*) (args->next->argumentName);
    uint8*          keyMenu         			= (uint8*) (args->next->next->argumentName);
    uint8*          onlinecharge    			= (uint8*) (args->next->next->next->argumentName);
    uint8           singleCharge    			= (uint8) (args->next->next->next->next->argumentName);
    int             multiTypeCharge				= FALSE;  
    cardSpecST 		cardSpec;
        
    memset(&cardSpec, 0, sizeof(cardSpecST));
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**buyMultiCharge");
    
    if (!checkSIMStatus()) //MRF_971015
        return;
    
    if ((*keyMenu) && (chargeTypeCount == 0))
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
#if defined(IWL220) || defined(ICT250)//HNO_CHANGE
        displayMessage("���� ���� �� Ș���. . .", DISPLAY_START_LINE + 1, ALL_LINES);
#else
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
#endif
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
            return;

        if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
            return;
        
        if (!writeCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return;
        }
    }
    
//    if (chargeTypeCount == 0) //only print bank header once for new -MRF_970903 : MOVE TO TRANS
//        if (!prePrint(TRUE, files))
//        {
//            disconnectAndClose(1, NOT_SHOW_DC_MSG);
//            return;
//        }

    setBuyChargeFields(index);
     
    if (!*onlinecharge)
        buyMultiChargeOfflineTrans(files, messageSpec, buyOfflineCharge, index, &multiTypeCharge);
    else
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "chargeType: %d",chargeType);
        switch (chargeType)
        {
            case MULTI_IRANCELL:
                strcpy(buyCharge[chargeTypeCount].operatorId, IRANCELL);
                break;
            case MULTI_MCI:
                strcpy(buyCharge[chargeTypeCount].operatorId, MCI);
                break;
            case MULTI_RIGHTEL:
                strcpy(buyCharge[chargeTypeCount].operatorId, RIGHTEL);
                break;
        }

        buyCharge[chargeTypeCount].type = chargeType;
        strcpy(buyCharge[chargeTypeCount].amount, amount);
        buyMultiChargeTrans(files, messageSpec, buyCharge, &multiTypeCharge, singleCharge);//MRF_NEW17
  }
    
    if (multiTypeCharge) 
    {  
//        if (onlinecharge)
//        {
            if (!keyMenu)
                showMenu(MENU_CARD_ITEM_BUY_MULTI_CHARGE);
            else 
                showMenu(MENU_ITEM_BUY_MULTI_CHARGE);
//        }
//        else
//            showMenu(MENU_SPECIAL_SERVICE_BUY_OFFLINE_CHARGE);
    }
    else 
        resetBuyChargeInfo();
}


void resetBuyChargeInfo(void) 
{
	chargeTypeCount = 0;
	memset(buyCharge, 0, sizeof(buyChargeSpecST) * 99);
}


void getChargeType(uint8* buff, uint8 type, uint8* amount) 
{
	uint8 chargeName[32] 			= {0};
	uint8 chargeAmountWithComma[32] = {0};
    
	makeValueWithCommaStr(amount, chargeAmountWithComma); 
	strcat(chargeAmountWithComma," �����");
	getChargeTypeStr(type, chargeName);
    strcat(buff, chargeName);
    strcat(buff, chargeAmountWithComma);
}

uint8 getChargeTypeCount(void)
{
    return chargeTypeCount;
}


void setBuyChargeFields(int index)
{
  switch (index)
    {
        case MTN_10000:
        {
            buyOfflineCharge[index].type = MULTI_IRANCELL;
            strcpy(buyOfflineCharge[index].operatorId, IRANCELL);
            strcpy(buyOfflineCharge[index].amount, "10000");
            break;
        }
        case MTN_20000:
        {
            buyOfflineCharge[index].type = MULTI_IRANCELL;
            strcpy(buyOfflineCharge[index].operatorId, IRANCELL);
            strcpy(buyOfflineCharge[index].amount, "20000");
            break;
        }
        case MTN_50000:
        {
            buyOfflineCharge[index].type = MULTI_IRANCELL;
            strcpy(buyOfflineCharge[index].operatorId, IRANCELL);
            strcpy(buyOfflineCharge[index].amount, "50000");
            break;
        }
        case MTN_100000:
        {
            buyOfflineCharge[index].type = MULTI_IRANCELL;
            strcpy(buyOfflineCharge[index].operatorId, IRANCELL);
            strcpy(buyOfflineCharge[index].amount, "100000");
            break;
        }
        case MTN_200000:
        {
            buyOfflineCharge[index].type = MULTI_IRANCELL;
            strcpy(buyOfflineCharge[index].operatorId, IRANCELL);
            strcpy(buyOfflineCharge[index].amount, "200000");
            break;
        }
        case MCI_10000:
        {
            buyOfflineCharge[index].type = MULTI_MCI;
            strcpy(buyOfflineCharge[index].operatorId, MCI);
            strcpy(buyOfflineCharge[index].amount, "10000");
            break;
        }
        case MCI_20000:
        {
            buyOfflineCharge[index].type = MULTI_MCI;
            strcpy(buyOfflineCharge[index].operatorId, MCI);
            strcpy(buyOfflineCharge[index].amount, "20000");
            break;
        }
        case MCI_50000:
        {
            buyOfflineCharge[index].type = MULTI_MCI;
            strcpy(buyOfflineCharge[index].operatorId, MCI);
            strcpy(buyOfflineCharge[index].amount, "50000");
            break;
        }
        case MCI_100000:
        {
            buyOfflineCharge[index].type = MULTI_MCI;
            strcpy(buyOfflineCharge[index].operatorId, MCI);
            strcpy(buyOfflineCharge[index].amount, "100000");
            break;
        }    
        case MCI_200000:
        {
            buyOfflineCharge[index].type = MULTI_MCI;
            strcpy(buyOfflineCharge[index].operatorId, MCI);
            strcpy(buyOfflineCharge[index].amount, "200000");
            break;
        }    
        case RTL_20000:
        {
            buyOfflineCharge[index].type = MULTI_RIGHTEL;
            strcpy(buyOfflineCharge[index].operatorId, RIGHTEL);
            strcpy(buyOfflineCharge[index].amount, "20000");
            break;
        }    
        case RTL_50000:
        {
            buyOfflineCharge[index].type = MULTI_RIGHTEL;
            strcpy(buyOfflineCharge[index].operatorId, RIGHTEL);
            strcpy(buyOfflineCharge[index].amount, "50000");
            break;
        }   
        case RTL_100000:
        {
            buyOfflineCharge[index].type = MULTI_RIGHTEL;
            strcpy(buyOfflineCharge[index].operatorId, RIGHTEL);
            strcpy(buyOfflineCharge[index].amount, "100000");
            break;
        }    
        case RTL_200000:
        {
            buyOfflineCharge[index].type = MULTI_RIGHTEL;
            strcpy(buyOfflineCharge[index].operatorId, RIGHTEL);
            strcpy(buyOfflineCharge[index].amount, "200000");
            break;
        }    
        case RTL_500000:
        {
            buyOfflineCharge[index].type = MULTI_RIGHTEL;
            strcpy(buyOfflineCharge[index].operatorId, RIGHTEL);
            strcpy(buyOfflineCharge[index].amount, "500000");
            break;
        }    
    }  
}

void buyMultiChargeTrans(filesST* files, messageSpecST* messageSpec,
		buyChargeSpecST* buyCharge, int* multiTypeCharge, uint8 singleCharge)
{
    int 			counter 							= 0; 		/** loop counter */
    int 			i 									= 0;
    uint8 			request[MESSAGE_BUFFER_SIZE] 		= {0}; 		/** request message */
    uint16 			requestLen 							= 0; 		/** request message lenght */
    uint8 			response[MESSAGE_BUFFER_SIZE] 		= {0}; 		/** response message */
    uint16 			responseLen 						= 0; 		/** response message lenght */
    int 			responseCode 						= -1; 		/** response message response code(39) */
    int 			parseResponseResult					= -1; 		/** parse response message result */
    uint8 			printFirstPart						= TRUE; 	/** for check print first part of transaction receipt */
    uint8 			known								= FALSE; 	/** error is known or unknown */
    uint8 			errorMsg[70] 						= {0}; 		/** erropr message */
    int8 			retValue 							= 0;
    uint8 			PIN[4 + 1] 							= {0};		
    uint32          difSec               				= 0;
    uint16 			checkResStatus 		 				= 0;
    uint32          timeStr								= 0;		
    uint8           ChargeTypeStr[30]   				= {0};
    uint8           shaparakState       				= 0;
    uint8           logonFlag           				= FALSE;
    uint8           isPOSStan                           = 0;
    terminalSpecST  terminalCapability  				= getTerminalCapability();//MRF_NEW19
	messageSpecST	tempMessageSpec;		
    cardSpecST 		cardSpec;		
    dateTimeST      localDateTime;
    
    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }
    
    if (messageSpec == NULL) 
    {
        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
        messageSpec = &tempMessageSpec;
    }

	messageSpec->transType = TRANS_BUYCHARGE;

	if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return;
    }

    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }
    
    if (!singleCharge)
    {
        if (!enterBuyMultiChargeInfo(PIN, &buyCharge[chargeTypeCount],
                &chargeTypeCount, multiTypeCharge, FALSE))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return;
        }
    }
    else
    {
        buyCharge->productTypeCount = 1;
        memset(PIN, 0, 5);

        if (!getPass("", PIN, 4, 2))
			return;
        
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PIN: %d", PIN);

        *multiTypeCharge = FALSE;
    }
    

    if (*multiTypeCharge)
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }
    
	if (!readCardInfo(&cardSpec))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return;
    }

//    if (!prePrint(TRUE, files))//!MR_970903 : MOVED//HNO_980602 #remove preprint from transactions
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return;
//    }
    
	messageSpec->cardSpec = cardSpec;
	strcpy(messageSpec->cardSpec.PIN, PIN);

	//HNO_IDENT uncomment
	if (!doConnect(files))
		return;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE) 
	{
		disconnectAndClose(0);
		return;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec)) 
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return;
    }

    /*save batch charge information in separate structure for retrieve charge report */
	messageSpec->buyChargeSpec.id = createID(messageSpec->dateTime.time); //HNO_IDENT3 change
    strcpy(messageSpec->charge.terminalID, messageSpec->merchantSpec.terminalID);
    strcpy(messageSpec->charge.PAN, messageSpec->cardSpec.PAN);
    
    resetVoucherData();
    for (i = 0; i <= chargeTypeCount; i++)
    {
        messageSpec->buyChargeSpec.type = buyCharge[i].type;
		strcpy(messageSpec->buyChargeSpec.amount, buyCharge[i].amount);
        strcpy(messageSpec->amount, buyCharge[i].amount);
        for (counter = 0; counter < buyCharge[i].productTypeCount; counter++)
		{
        	/*for calculate the count of all Bought charges in one transaction */
        	messageSpec->charge.chargeCount++;
        	//HNO_IDENT change
			memset(request, 0, MESSAGE_BUFFER_SIZE);
			memset(response, 0, MESSAGE_BUFFER_SIZE);
			requestLen = 0;
			responseLen = sizeof(response);

            strcpy(messageSpec->buyChargeSpec.operatorId, buyCharge[i].operatorId);
            messageSpec->buyChargeSpec.productTypeCount = buyCharge[i].productTypeCount;
            
            displayMessage("", DISPLAY_START_LINE, ALL_LINES);
            displayMessage("���� ��ю", DISPLAY_START_LINE, NO_LINES);
            memset(ChargeTypeStr,0,sizeof(ChargeTypeStr));
            getChargeType(ChargeTypeStr, messageSpec->buyChargeSpec.type, messageSpec->buyChargeSpec.amount);
            displayMessage(ChargeTypeStr, DISPLAY_START_LINE + 1, NO_LINES);

            incAndSaveStan(files, &messageSpec->merchantSpec);
            SIMGetIMSI(messageSpec->IMSI);
            requestLen = createBuyChargeMessage87(files, request, messageSpec);
            if (requestLen <= 0)
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����1");
                displayMessageBox("���� �����", MSG_ERROR);
                disconnectAndClose(0);
                return;
            }
     
            if (!createSaveReversal(files, messageSpec))
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����2");
                displayMessageBox("���� �����", MSG_ERROR);
                disconnectAndClose(0);
                return;
            }
            
            displayMessage("����� �������...", DISPLAY_START_LINE + 3, NO_LINES);
            if (!connSendBuff(request, requestLen))
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
                fileRemove(files->reversalReceiptFile);
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
                fileRemove(files->reversalTransFile);
                displayMessageBox("����� ������", MSG_ERROR);
                disconnectAndClose(0);
                return;
            }

            displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
            if (!connReceiveBuff(response, &responseLen))
            {
                displayMessageBox("������ ������", MSG_ERROR);
//                disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
                reversalTrans(files);
                disconnectAndClose(0);
                return;
            }

            //Check response status of message
            messageSpec->responseStatus = 0; 
            responseCode = checkResponseValidity87(messageSpec, messageSpec->transType, 
                    response, responseLen, request,requestLen);

            parseResponseResult = parseBuyChargeResponse87(response, responseLen, messageSpec);
            if (responseCode != SUCCESS)
            {
                known = FALSE;
                messageSpec->responseStatus = responseCode;
            }
            else if (parseResponseResult != SUCCESS)
                messageSpec->responseStatus = parseResponseResult;
                
            
            if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
            {
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "���� �����3");
                displayMessageBox("���� �����", MSG_ERROR);
                reversalTrans(files);
                disconnectAndClose(0);
                return;
            }
            
            shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
            if (messageSpec->responseStatus == 99)
            {  
                showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");
                if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
                {
                    disconnectAndClose(0);
                    return;
                }

                setLogonScheduleAfterUnsuccessLogon(files);
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
                fileRemove(files->settelmentInfoFile);
                showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
                fileRemove(files->reversalTransFile);
                getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
                addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
                displayMessageBox(errorMsg, MSG_ERROR);
                printReversal(files);
                logonFlag = TRUE;
                logonTrans(files);
            }
            else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
            {
                known = FALSE;
                memset(errorMsg, 0, sizeof(errorMsg));
                fileRemove(files->reversalTransFile);
                getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
                addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
                displayMessageBox(errorMsg, MSG_ERROR);
                printReversal(files);
                disconnectAndClose(0);
            }  
            else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
            {
                memset(errorMsg, 0, sizeof(errorMsg));
//                disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
                            
                isPOSStan = TRUE; //MRF_NEW20
                if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
                {
                    displayMessageBox("���� �����", MSG_ERROR);
                    reversalTrans(files);
                    disconnectAndClose(0);
                    return;
                }

                reversalTrans(files);
                getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
                addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
                displayMessageBox(errorMsg, MSG_ERROR);
                disconnectAndClose(0);
                return;
            }

            if (!saveMessageSpec(files, messageSpec))
            {
                reversalTrans(files); 
                disconnectAndClose(0);
                return;
            }
            
            parseResponseResult = parseField63ForBuyChargeResponse87(response, responseLen, messageSpec);
            if (parseResponseResult != SUCCESS)
            {
                reversalTrans(files);
                disconnectAndClose(0);
                return;
            }
            
            //Set time with switch          
            localDateTime = getDateTime();    
			if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
            {
                if (localDateTime.date != messageSpec->dateTime.date)
                {
                    if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                        return;
                                 
                    setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
                    if (!PasswordGenerator())
                        return;
                }
            }
            
            difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
            difSec = (difSec >= 0) ? difSec : -difSec;
            setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
            
            if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
            {
                disconnectAndClose(0);
                return;
            }

            if (difSec >= 300)
            {
                displayMessageBox("������ ����� �� ����", MSG_ERROR);
                reversalTrans(files);
                disconnectAndClose(0);
                return;
            }

            if (counter == 0) 
                printFirstPart = TRUE;
            else
                printFirstPart = FALSE;

            if (!printTransactionBuyCharge(files, messageSpec, TRUE, NO_REPRINT))
            {
                reversalTrans(files);
                disconnectAndClose(0);
                return;
            }
            
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: customerReceiptFile");
            fileRemove(files->customerReceiptFile);
            
            if (settlementTrans(files) != TRANS_SUCCEED)
            {
                disconnectAndClose(0);
                return;
            }
        }
    }
    
    disconnectAndClose(0);
}


uint8 supervisorTrans(filesST* files,messageSpecST* messageSpec,  cardSpecST cardSpec, uint8* keyMenu)
{
	uint8 			request[MESSAGE_BUFFER_SIZE] 	= {0}; 						/** request message */
	uint16 			requestLen          			= 0; 						/** request message lenght */
	uint8 			response[MESSAGE_BUFFER_SIZE] 	= {0}; 						/** response message */
	uint16 			responseLen 					= sizeof(response);         /** response message lenght */
    uint16 			checkResStatus 					= 0;
    uint16 			parseResStatus 					= 0;
	uint8 			known               			= FALSE; 					/** error is known or unknown */
	uint8 			errorMsg[70] 					= {0}; 						/** erropr message */
	uint8 			retValue            			= 0;
	uint8 			valueWithComma[64]				= {0};
	uint8 			readAmount          			= TRUE;
	uint8 			readPIN             			= TRUE;
    uint8           depositID[13 + 1]				= {0};
    uint32          difSec              			= 0;
    uint16          responceCode        			= 0;
	serviceSpecST   service             			= getPOSService();
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
    supervisorSpec  supervisor;
	dateTimeST 		localDateTime;			
	messageSpecST   tempMessageSpec;		

	memset(&localDateTime, 0, sizeof(dateTimeST));
    memset(&supervisor, 0, sizeof(supervisorSpec));
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Supervisor Trans**");

    if (readPIN)
    {
        if (!enterBuyInfo(files, messageSpec, cardSpec.PIN, readAmount, depositID))
        {
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
            fileRemove(SUPERVISOR_FILE);
            return FALSE;
        }

    }
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "buy amount: %s", messageSpec->amount);

	messageSpec->transType = TRANS_BUY;

	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));

	if (!doConnect(files))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		return FALSE;
    }

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		disconnectAndClose(0);
		return FALSE;
	}

	makeValueWithCommaStr(messageSpec->amount, valueWithComma); 
	strcat(valueWithComma, " ����");
    
	displayMessage("����", DISPLAY_START_LINE, ALL_LINES);
	displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);

	incAndSaveStan(files, &(messageSpec->merchantSpec));
    SIMGetIMSI(messageSpec->IMSI);
	requestLen = createBuyMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, response, 
            responseLen, request, requestLen);
        
	parseResStatus = parseBuyResponse87(response, responseLen, messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec->responseStatus = parseResStatus;
    }
        
	if (messageSpec->responseStatus != SUCCESS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(0);
//        return FALSE;--MRF_NEW20
	}        

    //Set time with switch          
    localDateTime = getDateTime();    
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }

    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(0);
        return FALSE;
    }
    
	if (!saveMessageSpec(files, messageSpec))
	{
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(0);
        return FALSE;
	}
    
	if (messageSpec->responseStatus == 0)//+MRF_971026
		if (!printTransactionSupervisor(messageSpec, files))
		{
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**Remove supervisor file **");
			fileRemove(SUPERVISOR_FILE);
			disconnectAndClose(0);
			return FALSE;
		}
    
	disconnectAndClose(0);
    
	return TRUE;
}


uint8 logonTrans(filesST* files) 
{
	uint8 			request[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** request message */
	uint16 			requestLen 						= 0; 					/** request message lenght */
	uint8 			response[MESSAGE_BUFFER_SIZE] 	= {0}; 					/** response message */
	uint16 			responseLen 					= sizeof(response);		/** response message lenght */
	int 			responseCode 					= -1; 					/** response message response code(39) */
	uint8 			known							= FALSE; 				/** error is known or unknown */
	uint8 			errorMsg[70] 					= {0};
	int 			parseResponseResult 			= -1;
	uint8 			key								= KBD_CANCEL;				
    uint32          difSec              			= 0;
    int8            retValue            			= 0;  
    uint8           shaparakState       			= 0;
	messageSpecST   messageSpec;	
    dateTimeST      localDateTime;


	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**logonTrans");
    
	memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    
    if (fileExist(LOGON_INFO_FILE) != SUCCESS)
	{
		displayMessageBox("���� ���� ��� ����� ����!", MSG_INFO);
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Logon file not exist");
        resetLogonSchedule(files);
		return FALSE;
	}

	if (!doConnect(files))
        return FALSE;
	
	messageSpec.transType = TRANS_LOGON;
    retValue = checkTransPrerequisites(files, messageSpec.transType);
	if (retValue != TRUE)
	{
        setLogonScheduleAfterUnsuccessLogon(files);
		disconnectAndClose(0);
		return FALSE;
	}
    
	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		setLogonScheduleAfterUnsuccessLogon(files);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "Logon");
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_NOT_SENT;
	}
       
        //HNO_INIT
    addInitializeReport(&messageSpec, FALSE);
	incAndSaveStan(files, &(messageSpec.merchantSpec));
    displayMessage("����� ������", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);
    
	requestLen = createInitializeMessage87(request, &messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		setLogonScheduleAfterUnsuccessLogon(files);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_NOT_SENT;
	}
    
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		setLogonScheduleAfterUnsuccessLogon(files);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_NOT_SENT;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
		setLogonScheduleAfterUnsuccessLogon(files);
		disconnectAndClose(0);
		return FALSE;
	}
        
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseLen =%d", responseLen);

    responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, 
            response, responseLen, request,requestLen);

	parseResponseResult = parseInitializeResponse87(response, responseLen, &messageSpec);
    if (responseCode != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = responseCode;
    }
    else if (parseResponseResult != SUCCESS)
    {
        messageSpec.responseStatus = parseResponseResult;
    }
    
    shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
	if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
        setLogonScheduleAfterUnsuccessLogon(files);
		getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
		addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_RESPONSE_ERROR;
	}
       
	if (!saveMessageSpec(files, &messageSpec))
	{
        setLogonScheduleAfterUnsuccessLogon(files);
		disconnectAndClose(0);
		return FALSE;
	}

    //HNO_INIT
    addInitializeReport(&messageSpec, TRUE);
    
    resetLogonSchedule(files);
    fileRemove(LOGON_INFO_FILE);
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Logon success");
	disconnectAndClose(0);
    
	return TRANS_SUCCEED;
}

/****************************ETC TRANS FUNCTIONS********************************************/
//MRF_ETC
uint8 ETCTrans(argumentListST* args)
{
    filesST*        files               = (filesST*) (args->argumentName);
    uint8*          keyMenu             = (uint8*) (args->next->next->argumentName);
    messageSpecST* 	messageSpec 		= (messageSpecST*) (args->next->argumentName);
    uint8           key                 = 0;
    uint16          transResult         = ACCEPTED;
    uint16          retValue            = FAILURE;
    uint8 			readPIN             = TRUE;
    serviceSpecST   POSService          = getPOSService();
    terminalSpecST  terminalCapability  = getTerminalCapability();
    cardSpecST      cardInfo;
    messageSpecST   tempMessageSpec;
    dateTimeST      localDateTime;
    ETCST           ETC; 
    
    memset(&localDateTime, 0, sizeof(dateTimeST)); 
    memset(&ETC, 0, sizeof(ETCST));
    memset(&cardInfo, 0, sizeof(cardSpecST)); 
        
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "ETC TRANS");
    
    clearDisplay();
    
//    if (!POSService.ETCService)
//    {
//        displayMessageBox("����� ���� ��� ��� ���� ���", MSG_INFO);
//        return FALSE;
//    }
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
        
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
        { 
            return FALSE;
        }    

        if (!readTrackData(files, &cardTracks, &cardInfo, FALSE)) 
        {
            return FALSE;
        }
    }
    else if (!*keyMenu)//..HNO_980623 move up from down
    {
        if (!readCardInfo(&cardInfo))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }
    
   if (fileExist(SUPERVISOR_FILE) == SUCCESS)//..HNO_980623 move up from down
   {
       displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
       fileRemove(SUPERVISOR_FILE);
       disconnectAndClose(1, NOT_SHOW_DC_MSG);
       return;
   }

   
    displayWating();

    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
    if (messageSpec == NULL) 
    {
        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
        messageSpec = &tempMessageSpec;
    }

    if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
   
    if (readPIN)
    {
        if (!enterETCInfo(&cardInfo, &ETC))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }

    if (!doConnect(files))
        return FALSE;

    retValue = checkTransPrerequisites(files, TRANS_ETC);
    if (retValue != TRUE)
    {
        disconnectAndClose(0);
        return FALSE;
    } 

    transResult = ETCInquery(files,&cardInfo, &ETC);
    if (transResult == TRANS_SUCCEED)
    {
        clearKeyBuffer();

        key = displayETCInfo(&ETC, TRUE);
        if (key == KBD_ENTER || key == KBD_CONFIRM)
        {
            clearDisplay();
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkConnectionStatus");
            if (checkConnectionStatus() == DISCONNECTED)
            {
                if (!doConnect(files))
                {
                    return FALSE;
                }
            }
            ETCFinancialTransaction(files, &cardInfo, &ETC);//HNO_CHANGE use structure in pointer form
        }
    }
    else
        displayMessageBox("��ǘ�� ����� ���!", MSG_WARNING);
    
    disconnectAndClose(0);
    
}

//MRF_ETC
uint16 ETCInquery(filesST* files, cardSpecST* cardInfo, ETCST* ETC) 
{
    uint8           request[MESSAGE_BUFFER_SIZE]        = {0};   							//HNO_CHANGE   use buffer size                        /** request message */
    int             requestLen          				= 0;                                /** request message lenght */
    uint8           response[MESSAGE_BUFFER_SIZE]       = {0};                              /** response message */
    uint16          responseLen         				= sizeof(response);                 /** response message lenght */
    uint16          parseResStatus      				= 0;
    uint16          checkResStatus      				= 0; 
    uint8           known               				= FALSE;                            /** error is known or unknown */
    uint8           errorMsg[70]        				= {0};                              /** erropr message */
    uint32          difSec              				= 0;
    uint8           logonFlag           				= FALSE;
    uint8           shaparakState       				= 0;
    terminalSpecST  terminalCapability  				= getTerminalCapability();//MRF_NEW19
    dateTimeST      localDateTime;
    messageSpecST   messageSpec;
    
    memset(&messageSpec, 0, sizeof(messageSpecST)); 
    memset(&localDateTime, 0, sizeof(dateTimeST));
    memcpy(&(messageSpec.cardSpec), cardInfo, sizeof(cardSpecST));
    
    messageSpec.transType = TRANS_ETC_INQUERY;
   
    if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #if defined (INGENICO)
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_NOT_SENT;
    }
    
    displayMessage("������� �э�� �����", DISPLAY_START_LINE, ALL_LINES);
    
    incAndSaveStan(files, &(messageSpec.merchantSpec));
    strcpy(messageSpec.ETC.serialETC, ETC->serialETC);

    SIMGetIMSI(messageSpec.IMSI);
    requestLen = createETCInqueryMessage87(files, request, &messageSpec);
    if (requestLen <= 0)
    {
        displayMessageBox("���� �����", MSG_ERROR);
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_SENT;
    }
    
    displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connSendBuff(request, requestLen))
    {
        displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_SENT;
    }
    
    displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connReceiveBuff(response, &responseLen))
    {
        displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_RECEIVED;
    }
    
    messageSpec.responseStatus = 0;
    checkResStatus = checkResponseValidity87(&messageSpec, messageSpec.transType, 
            response, responseLen, request, requestLen);
    
    parseResStatus = parseETCInqueryResponse87(response, responseLen, &messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec.responseStatus = parseResStatus;
    }
    
    if (messageSpec.responseStatus == 99)
    {  
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return FALSE;
        }
      
        setLogonScheduleAfterUnsuccessLogon(files);
        
        getEnqueryErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else  if (messageSpec.responseStatus != SUCCESS && messageSpec.responseStatus != 16
            && messageSpec.responseStatus != 8)
    {
        getEnqueryErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        displayMessageBox(errorMsg, MSG_ERROR);
    }
    
    //-MRF_971015
//    removePad(messageSpec.ETC.cardHolderName, ' ');
//    removePad(messageSpec.ETC.cardHolderFamily, ' ');

    strcpy(ETC->cardHolderName, messageSpec.ETC.cardHolderName);
    strcpy(ETC->cardHolderFamily, messageSpec.ETC.cardHolderFamily);

    //Set time with switch          
    localDateTime = getDateTime();    
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
        }
    }
    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    
    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);
    if ((logonFlag == TRUE) || (messageSpec.responseStatus != SUCCESS))
    {
        disconnectAndClose(0);
        return FALSE;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }

    if (!saveMessageSpec(files, &messageSpec))
    {
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_RESPONSE_ERROR;
    }
    
    return TRANS_SUCCEED;
}

//MRF_ETC
void ETCFinancialTransaction(filesST* files, cardSpecST* cardInfo, ETCST* ETC)//HNO_CHANGE
{  
    uint8           request[MESSAGE_BUFFER_SIZE]    = {0};                      /** request message */
    int             requestLen          			= 0;            			/** request message lenght */     
    uint8           response[MESSAGE_BUFFER_SIZE]   = {0};          			/** response message */
    uint16          responseLen         			= sizeof(response);         /** response message lenght */     
    uint16          parseResStatus      			= 0;
    uint16          checkResStatus      			= 0;
    uint8           known               			= FALSE;        			/** error is known or unknown */
    uint8           errorMsg[70]        			= {0};          			/** erropr message */
    uint32          difSec              			= 0;
    uint8           valueWithComma[64] 				= {0};
    uint8           shaparakState       			= 0;
    uint8           logonFlag           			= FALSE;
    uint8           retValue            			= FALSE;  
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
    dateTimeST      localDateTime;
    messageSpecST   messageSpec;


    memset(&localDateTime, 0, sizeof(dateTimeST));
    memset(&messageSpec, 0, sizeof(messageSpecST));
    
    messageSpec.transType = TRANS_ETC;
    
    memcpy(&(messageSpec.cardSpec), cardInfo, sizeof(cardSpecST));
    
//    if (!prePrint(TRUE, files))//HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return;
//    }
    
    if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");

        #if defined (INGENICO)
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return;
    }
    
    incAndSaveStan(files, &(messageSpec.merchantSpec));
    
    //HNO_CHANGE: save etc structure information in messagespec structure for create etc message
    messageSpec.ETC.STAN = messageSpec.merchantSpec.STAN;
    messageSpec.ETC.dateTime = messageSpec.dateTime;    //it will be use for etc tracking
    strcpy(messageSpec.ETC.amount, ETC->amount);
    strcpy(messageSpec.ETC.serialETC, ETC->serialETC);
    strcpy(messageSpec.ETC.PAN, messageSpec.cardSpec.PAN);//it will be use for etc tracking
    strcpy(messageSpec.ETC.cardHolderName, ETC->cardHolderName);//it will be use for print & etc tracking
    strcpy(messageSpec.ETC.cardHolderFamily, ETC->cardHolderFamily);//it will be use for print & etc tracking

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan STAN:%d", messageSpec.ETC.STAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "etc amount:%s", messageSpec.ETC.amount);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "PAN: %s", messageSpec.ETC.PAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "etc date time: %d", messageSpec.ETC.dateTime);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "pan: %s", messageSpec.ETC.PAN);
  
    displayMessage("��ю ����� ��������", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);
    requestLen = createETCTransMessage87(files, request, &messageSpec);
    if (requestLen <= 0)
    {
        displayMessageBox("���� �����", MSG_ERROR);
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connSendBuff(request, requestLen))
    {
        displayMessageBox("����� ������", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }
    
    //HNO_ADD: save ETC info in file to print uncertain(power off)ETC info when pos power on
    messageSpec.responseStatus = FAILURE; //for manage data log
    makeTransPrintDataLog(files, &messageSpec);
    
    //HNO_ADD: save ETC information in file to use in tracking transaction
    retValue = saveETCInfo(ETC_INFO_FILE, messageSpec.ETC);
    if (retValue != TRUE)
    {
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    //HNO_ADD for manage printer when we don't receive financial trans response
    messageSpec.responseStatus = 95;
    displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connReceiveBuff(response, &responseLen))
    {
        displayMessageBox("������ ������", MSG_ERROR);
        if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT))
            displayMessageBox("���� ��ǘ�� �� ����� ������", MSG_ERROR);

        fileRemove(files->customerReceiptFile);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);
        return;
    }

    //HNO_COMMENT print retrievalReference empty in last customer print
    messageSpec.responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(&messageSpec, messageSpec.transType, 
            response, responseLen, request, requestLen);
        
	parseResStatus = parseETCTransResponse87(response, responseLen, &messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec.responseStatus = parseResStatus;
    }

    if ((messageSpec.responseStatus == SUCCESS) || (messageSpec.responseStatus == 8)
            || (messageSpec.responseStatus == 16))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "success ETC trans");
        addETCReport(&messageSpec);
        makeTransPrintDataLog(files, &messageSpec);
    }

	shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
	fileRemove(files->customerReceiptFile);
    if (messageSpec.responseStatus == 99)
    {  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return;
        }
      
        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
	else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR))
	{
        memset(errorMsg, 0, sizeof(errorMsg));
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        disconnectAndClose(0);
	}  
    else if (shaparakState == INTERNAL_ERROR)
    {
        memset(errorMsg, 0, sizeof(errorMsg));
		getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
		addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus,known);
		displayMessageBox(errorMsg, MSG_ERROR);
        
        if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT))
            displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);
        
        fileRemove(files->customerReceiptFile);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return;
    }
   
    localDateTime = getDateTime();   
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return;
        }
    }

    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        
        displayMessageBox("���� ��ǘ�� �� ����� ������", MSG_ERROR);
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }
    
    if (!saveMessageSpec(files, &messageSpec))
    {
        if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    if (!printETCTransaction(files, &messageSpec, TRUE, NO_REPRINT)) 
    {
		displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
    	fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }
    
    fileRemove(files->customerReceiptFile);
    
     if (getPOSService().merchantReceipt)//MRF_NEW19
    {
        retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
        PrinterAccess(FALSE);
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
            printETCTransaction(files, &messageSpec, FALSE, NO_REPRINT);
     }

    fileRemove(files->customerReceiptFile);
	disconnectAndClose(0);
}


uint16 ETCTrackingTrans(argumentListST* args)
{
    filesST*            files 			  				= (filesST*) (args->argumentName);
    messageSpecST*      messageSpec 	  				= (messageSpecST*) (args->next->argumentName);
    uint8*              keyMenu           				= (uint8*) (args->next->next->argumentName);
    uint8               request[MESSAGE_BUFFER_SIZE] 	= {0}; 						/** request message */
    uint8               response[MESSAGE_BUFFER_SIZE]   = {0}; 						/** response message */
    uint16              requestLen 		  				= 0;                        /** request message lenght */
    uint16              responseLen	      				= sizeof(response);         /** response message lenght */
    uint16              responseStatus    				= 0;
    uint8               key 			  				= 0;
    uint8               known			  				= FALSE;                   /** error is known or unknown */
    uint8               errorMsg[70] 	  				= {0};                     /** error message */
    uint32              difSec            				= 0;
    uint16              checkResStatus 	  				= 0;
    uint8               shaparakState     				= 0;
    uint8               logonFlag         				= FALSE;
    uint8               strSTAN[6 + 1]    				= {0};
    uint32              STAN              				= 0;
    uint8               readPIN           				= TRUE;
    int                 transactionNumber 				= 0;                       /** charge number with specified date in file */
    uint32              requestedNum      				= 0;
    uint8               status[70]        				= {0};
    serviceSpecST       POSService        				= getPOSService();
    terminalSpecST      terminalCapability				= getTerminalCapability();//MRF_NEW19
    ETCST               ETCSpec;
    cardSpecST          cardSpec;
    messageSpecST       tempMessageSpec;
    dateTimeST          localDateTime;
    

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**ETCTrackingTrans**");
    
    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST)); 
    memset(&ETCSpec, 0, sizeof(ETCST)); 
    
    //HNO_IDENT remove checking supervisor file because we don't read any card
//    if (!POSService.ETCService)
//    {
//        displayMessageBox("����� ���� ��� ��� ���� ���", MSG_INFO);
//        return FALSE;
//    }
    
    printReversal(files);
    if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

    if (!enterETCTrackingInfo(strSTAN, cardSpec.PIN))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

	messageSpec->transType = TRANS_ETC_TRACKING;
    
    STAN = atoi(strSTAN);

    if (!readSTANTransFromFile(ETC_INFO_FILE, &ETCSpec, sizeof(ETCST), MAX_ETC_TRANS, STAN,
            &transactionNumber, &requestedNum))
    {
        return FALSE;
    }

	if (!doConnect(files))
	{
		return FALSE;
	}

	if (checkTransPrerequisites(files, messageSpec->transType) != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #if defined (INGENICO)
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
    messageSpec->ETC = ETCSpec;
	displayMessage("����� �����", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec->IMSI);
    
	requestLen = createETCTrackingMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return FALSE;
	}

	checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType,
            response, responseLen, request, requestLen);
	
	responseStatus = parseETCTrackingResponse87(response, responseLen, messageSpec);
    
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (responseStatus != SUCCESS && responseStatus != 99)//HNO_IDENT
    {
        messageSpec->responseStatus = responseStatus;
    }
    
	shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
     if (messageSpec->responseStatus == 99)
    {  
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");

        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
      
        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
	else if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
	}

    displayMessageBox(messageSpec->ETC.status, MSG_INFO);
    
	if (!saveMessageSpec(files, messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}
    
    localDateTime = getDateTime(); 
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }

    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return FALSE;
    }
    
    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }
    
	key = displayMessageBox("��� ���� �� �ǁ ������ ����Ͽ", MSG_CONFIRMATION);
	if (key == KBD_ENTER || key == KBD_CONFIRM)
		printETCTrakingTransaction(files, messageSpec, TRUE, NO_REPRINT);
    
	disconnectAndClose(0);
	return TRUE;
}


void deleteFiles(argumentListST* args)
{
    filesST*        files               = (filesST*) (args->argumentName);
    
    if (fileExist(files->reversalTransFile) == SUCCESS)
    {
        if (fileRemove(files->reversalTransFile) == SUCCESS)
            displayMessageBox("���� ��ǘ�� �ѐ�� �ǘ ��!", MSG_INFO);
    }
    
    if (fileExist(files->settelmentInfoFile) == SUCCESS)
    {
        if (fileRemove(files->settelmentInfoFile) == SUCCESS)
            displayMessageBox("���� ������� �ǘ ��!", MSG_INFO);
    }
    
    if (fileExist(files->logonInfoFile) == SUCCESS)
    {
        if(fileRemove(files->logonInfoFile) == SUCCESS)
            displayMessageBox("���� ����� ���� �ǘ ��!", MSG_INFO);
    }
}

//MRF_TOPUP
uint8 buyTopupChargeTrans(filesST* files, messageSpecST* messageSpec, uint8* keyMenu, uint8 variable)
{
//	filesST* 	    files               = (filesST*) (args->argumentName);
//	messageSpecST* 	messageSpec 		= (messageSpecST*) (args->next->argumentName);
//	uint8*          keyMenu             = (uint8*) (args->next->next->argumentName);   
//	uint8 			amount[12 + 1]      = {0};
    uint8 			request[MESSAGE_BUFFER_SIZE] 		= {0};                         
	uint16 			requestLen          = 0;                           
	uint8 			response[MESSAGE_BUFFER_SIZE] 		= {0}; 
	uint16 			responseLen 		= sizeof(response);             
    uint16 			checkResStatus 		= 0;
    uint16 			parseResStatus 		= 0;
	uint8 			known               = FALSE; 			
	uint8 			errorMsg[70] 		= {0}; 				
	uint8 			retValue            = 0;
	uint8 			valueWithComma[64]	= {0};
	uint8 			readPIN             = TRUE;
    uint8           readAmount          = FALSE;
    uint32          difSec              = 0;
    uint16          responceCode        = 0;
    uint8           shaparakState       = 0;
    uint8           logonFlag           = FALSE;
    uint8           mobileNo[10 + 1]    = {0};
	terminalSpecST  terminalCapability  = getTerminalCapability();
    cardSpecST 		cardSpec;
	dateTimeST 		localDateTime;
	messageSpecST   tempMessageSpec;
    topupST         topupSpec;

	memset(&cardSpec, 0, sizeof(cardSpecST));
	memset(&localDateTime, 0, sizeof(dateTimeST));
    memset(&topupSpec, 0, sizeof(topupST));
    
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "*********TOPUP trans*********");
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " amount: %s ", amount);
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " operatorType: %d ", operatorType);
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " variable: %d ", variable);

    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
    
        displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
        if(getCardHandle() <= 0)
            enableMSR(); 

        if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
        { 
            return FALSE;
        }    

        if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
        {
            return FALSE;
        }
    }
    
    displayWating();
    
    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }
    
//    if (!prePrint(TRUE, files))//HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return FALSE;
//    }
    
    if (messageSpec == NULL) 
    {
        memset(&tempMessageSpec, 0, sizeof(messageSpecST));
        messageSpec = &tempMessageSpec;
    }
    else
    {
        //the Amount fill in PCPOS
        if (strlen(messageSpec->amount) > 0)
            readAmount = FALSE;

        //the PIN fill in PCPOS
        if (strlen(messageSpec->cardSpec.PIN) > 0)
            readPIN = FALSE;
    }

    if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

    if (!*keyMenu)
    {
        if (!readCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }
    
    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    } 

    if (readPIN)
    {
        if (!enterBuyTopUpChargeInfo(&topupSpec, cardSpec.PIN, variable))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
    	}
    }

    strcpy(messageSpec->topup.mobileNo, topupSpec.mobileNo);
    messageSpec->topup.type = topupSpec.type;
    messageSpec->topup.operatorType = topupSpec.operatorType;
    strcpy(messageSpec->topup.amount ,topupSpec.amount);
    strcpy(messageSpec->amount, topupSpec.amount);
    
	messageSpec->transType = TRANS_TOPUP;
	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));

	if (!doConnect(files))
		return FALSE;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
        #ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}


	makeValueWithCommaStr(messageSpec->amount, valueWithComma); 
	strcat(valueWithComma, " ����");
    
	displayMessage("���� ��ю ���", DISPLAY_START_LINE, ALL_LINES);
	displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);
    
	incAndSaveStan(files, &(messageSpec->merchantSpec));
    SIMGetIMSI(messageSpec->IMSI);
    
	requestLen = createTopupMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 95; //mgh: for managing reversal printer
	if (!createSaveReversal(files, messageSpec))
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
        
	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		fileRemove(files->reversalReceiptFile);//++ABS
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
        printReversal(files);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 0; 
    checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, 
            response, responseLen, request,	requestLen);

	parseResStatus = parseBuyChargeTopupResponse87(response, responseLen, messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec->responseStatus = parseResStatus;
    }

    if (!updateResponse(files, messageSpec))
    {
        displayMessageBox("���� �����", MSG_ERROR);
        printReversal(files);
        disconnectAndClose(0);
        return;
    }
    
    shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
    if (messageSpec->responseStatus == 99)
    {  
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return FALSE;
        }
        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        printReversal(files);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR))
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "FIRST ERROR");
        memset(errorMsg, 0, sizeof(errorMsg));            
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        printReversal(files);
        displayMessageBox(errorMsg, MSG_ERROR);
        disconnectAndClose(0);
    } 
    else if (shaparakState == INTERNAL_ERROR)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "SECOND ERROR");
        memset(errorMsg, 0, sizeof(errorMsg));
        printReversal(files);
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
		displayMessageBox(errorMsg, MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return FALSE;
    }

    //Set time with switch          
    localDateTime = getDateTime();    
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return FALSE;
    }
    
    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }
    
	if (!saveMessageSpec(files, messageSpec))
	{
        disconnectAndClose(0);
        return FALSE;
	}
    
	if (!printTransactionBuyChargeTopup(files, messageSpec, TRUE, NO_REPRINT))
	{
        printReversal(files);
		disconnectAndClose(0);
		return FALSE;
	}
    
    //if (getPOSService().merchantReceipt)//MRF_NEW19
    //{
    //    retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
    //    PrinterAccess(FALSE);
    //    if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
    //        printTransactionBuyChargeTopup(files, messageSpec, FALSE, NO_REPRINT);
    //}
    
    fileRemove(files->customerReceiptFile);
    fileRemove(files->reversalReceiptFile);
	disconnectAndClose(0);
    
	return TRUE;
}


uint8 readSTANTransFromFile(uint8* fileName, void* transactionInfo, uint16 structLen, uint16 maxRecord, 
							  	uint32 STAN, int* transactionNum, uint32* requestedNum)
{
    int             searchTransRes          = -2;			/** search for transaction with specified date in transactions file */
    int             recordNum               = 0;			/** start record number of transactions with specified date in file */
    int             endRecordNum            = 0;			/** end record number of transactions with specified date in file */
    int				beginIndexInFile		= 0;            /** index of beginning requested transaction in transactions file */
    int16			retValue 				= FAILURE;
    uint32          fileId                  = 0; 
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**readSTANTransFromFile**");
    
    setflag(FALSE); //for search in file when low == mid
    
    searchTransRes = searchSTANInFixedFile(fileName, structLen, 0, STAN, maxRecord, &recordNum);
    
    if (searchTransRes == -2 || recordNum == -2 || recordNum == -1) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }

    if (*requestedNum > maxRecord)
    	if (*transactionNum <= maxRecord)
    		*requestedNum = *transactionNum;
    	else
    		*requestedNum = maxRecord;
    
    if (*requestedNum == 0 || *requestedNum > *transactionNum) 
        *requestedNum = *transactionNum;
    
    //show the last transactions
    beginIndexInFile = *transactionNum - *requestedNum + recordNum;   
    retValue = openFile(fileName, &fileId);
    if (retValue != SUCCESS)
        return retValue;
    
    retValue = readFixedFileInfo(fileId, transactionInfo, structLen, beginIndexInFile, 
                                        maxRecord);  
    if (retValue != SUCCESS) 
    {
        displayMessageBox("��ǘ��� ���� ���!", MSG_INFO);
        return FALSE;
    }
    
    closeFile(fileId);
    return TRUE;
}


uint8 buyPcPOSTrans(filesST* files, messageSpecST* messageSpec) 
{
	uint8 			request[MESSAGE_BUFFER_SIZE] 	= {0};                          /** request message */
	uint16 			requestLen          			= 0;                            /** request message length */
	uint8 			response[MESSAGE_BUFFER_SIZE] 	= {0};                          /** response message */
	uint16 			responseLen 					= sizeof(response);             /** response message length */
    uint16 			checkResStatus 					= 0;
    uint16 			parseResStatus 					= 0;
	uint8 			known               			= FALSE; 						/** error is known or unknown */
	uint8 			errorMsg[70] 					= {0}; 							/** erropr message */
	uint8 			retValue            			= 0;
	uint8 			valueWithComma[64]				= {0};
	uint8 			readAmount          			= TRUE;
	uint8 			readPIN             			= TRUE;
    uint32          difSec              			= 0;
    uint16          responceCode        			= 0;
    uint8           shaparakState       			= 0;
    uint8           logonFlag           			= FALSE;
    uint8           amount[40]          			= {0};
    uint8           message[60]         			= {0};
    uint8           PIN[4 + 1]          			= {0};
    uint8           isPOSStan                       = 0;
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
	cardTracksST    cardTracks;
	cardSpecST 		cardSpec;
	dateTimeST 		localDateTime;
    
	memset(&cardSpec, 0, sizeof(cardSpecST));
	memset(&localDateTime, 0, sizeof(dateTimeST));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "messageSpec->amount 4: %s", messageSpec->amount);

    memset(&cardTracks, 0, sizeof(cardTracksST));

    displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
    if(getCardHandle() <= 0)
        enableMSR(); 

    if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
    { 
        return FALSE;
    }    

    if (!readTrackData(files, &cardTracks, &cardSpec, FALSE)) 
    {
        return FALSE;
    }
        
    displayWating();
    
    if (!printReversal(files))
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "!printReversal");
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return PRINTER_FAIL;
    }
    
//    if (!prePrint(TRUE, files)) -MRF_970903 : MOVE TO DOWN
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return PRINTER_FAIL;
//    }
    
    if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return PRINTER_FAIL;
    }
    
    if (fileExist(SUPERVISOR_FILE) == SUCCESS)
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

    if (readPIN)
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "messageSpec->amount 5: %s", messageSpec->amount);
        strcpy(amount, messageSpec->amount);
        makeValueWithCommaStr(amount, message);
        strcat(message," ����");

        retValue = displayMessageBox(message, MSG_CONFIRMATION);
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)
        {
            memset(PIN, 0, 5); 
            if (!getPass("", PIN, 4, 2))
                return FALSE ;

			strcpy(cardSpec.PIN, PIN);
            showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "PIN2: %s", cardSpec.PIN);
        }
        else
        {
            return FALSE;
        }
    }
    
//    if (!prePrint(TRUE, files)) //!MRF_970903 : MOVE FROM UP //HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return PRINTER_FAIL;
//    }
    
	messageSpec->transType = TRANS_BUY;
    memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));
    
	if (!doConnect(files))
		return FALSE;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		fileRemove(files->masterKeyFile);

		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	makeValueWithCommaStr(messageSpec->amount, valueWithComma); 
	strcat(valueWithComma, " ����");
    
	displayMessage("����", DISPLAY_START_LINE, ALL_LINES);
	displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);

	incAndSaveStan(files, &(messageSpec->merchantSpec));

	requestLen = createBuyMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}
    
    messageSpec->responseStatus = 95; //mgh: for managing reversal printer
	if (!createSaveReversal(files, messageSpec))
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
        reversalTransPCPOS(files); 
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, 
            response, responseLen, request,	requestLen);

	parseResStatus = parseBuyResponse87(response, responseLen, messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec->responseStatus = parseResStatus;
    }
    
	if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
	{
		displayMessageBox("���� �����", MSG_ERROR);
        reversalTransPCPOS(files); 
		disconnectAndClose(0);
		return FALSE;
	}
    
    shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
    if (messageSpec->responseStatus == 99)
    {  
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
            return FALSE;
        
        setLogonScheduleAfterUnsuccessLogon(files);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
        fileRemove(files->settelmentInfoFile);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState != SUCCESS_TRANS");
        memset(errorMsg, 0, sizeof(errorMsg));                
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        disconnectAndClose(0);
    } 
    else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
    {
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState == INTERNAL_ERROR");
        memset(errorMsg, 0, sizeof(errorMsg));
                    
        isPOSStan = TRUE; //MRF_NEW20
        if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
        {
            displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return FALSE;
        }

        reversalTransPCPOS(files); 
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
    }

    //Set time with switch          
    localDateTime = getDateTime();   
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }

    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
        return FALSE;
    
    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        reversalTransPCPOS(files); 
        disconnectAndClose(0);
        return FALSE;
    }
    
	if (!saveMessageSpec(files, messageSpec))
	{
        reversalTransPCPOS(files); 
        disconnectAndClose(0);
        return FALSE;
	}
    
    //+HNO_970924
    messageSpec->pcPos = TRUE;
    
	if (!printTransactionBuy(files, messageSpec, TRUE, NO_REPRINT))
	{
        reversalTransPCPOS(files); 
		disconnectAndClose(0);
		return PRINTER_FAIL;
	}
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "buyPcPOSTrans success");
    sendDataPCPOS(messageSpec, TRUE);
    
    if (getPOSService().merchantReceipt)//MRF_NEW19
    {
        retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
        PrinterAccess(FALSE);
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
            printTransactionBuy(files, messageSpec, FALSE, NO_REPRINT);
    }
    
    fileRemove(files->customerReceiptFile);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Before Settlement Trans");
	settlementTrans(files);
    disconnectAndClose(0);
    return TRUE;
}


//MRF_TOPUP
void setTopupFields(argumentListST* args)
{
    filesST* 	    files               = (filesST*) (args->argumentName);
    messageSpecST*  messageSpec 	= (messageSpecST*) (args->next->argumentName);
    uint8*          keyMenu             = (uint8*) (args->next->next->argumentName);
    int             index               = (int) (args->next->next->next->argumentName); 
    uint8           variable            = (uint8) (args->next->next->next->next->argumentName);
    //uint8*          amount[12 + 1]      = {0};
    uint8           chargeType          = 0;
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "setTopupFields");
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " keyMenu: %d ", keyMenu);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " index: %d ", index);
    
    //showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " amount: %s ", amount);
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", " operatorType: %d ", chargeType);
	buyTopupChargeTrans(files, messageSpec, keyMenu, variable);//ABS:CHANGE:960801
}


uint8 updateResponse(filesST* files, messageSpecST* messageSpec) 
{
	int16                   retValue            = FAILURE;                  /** file functions return value */
    uint32                  sizeOfFile          = sizeof(reversalTransST);
    reversalTransST 		reversalInfo;                           /** reversal receipt information */
    
	memset(&reversalInfo, 0, sizeof(reversalTransST));
        
    retValue = readFileInfo(files->reversalReceiptFile, &reversalInfo, &sizeOfFile);
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #7");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
	}
    
  	reversalInfo.responseStatus = messageSpec->responseStatus;
    reversalInfo.STAN = messageSpec->merchantSpec.recieveSTAN;

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**UPDATE: reversalTransFile");
	retValue = updateFileInfo(files->reversalReceiptFile, &reversalInfo, sizeof(reversalTransST));
	if (retValue != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "err #7");
		addSystemErrorReport(retValue, ERR_TYPE_FILE_SYSTEM);
	}

    return TRUE;
}

//HNO
uint8 loanPayTrans(argumentListST* args)
{
	filesST*        		files               	= (filesST*) (args->argumentName);
	uint8*          		keyMenu             	= (uint8*) (args->next->next->argumentName);
	messageSpecST*  		messageSpec         	= (messageSpecST*) (args->next->argumentName);
	uint8           		key                 	= 0;
	uint16          		transResult         	= ACCEPTED;
	uint16          		retValue            	= FAILURE;
	uint8           		readPIN             	= TRUE;
    serviceSpecST   		POSService          	= getPOSService();
    terminalSpecST  		terminalCapability  	= getTerminalCapability();//MRF_NEW19    
	cardSpecST      		cardInfo;
	messageSpecST   		tempMessageSpec;
	dateTimeST      		localDateTime;
	loanPayST       		loanPay;


	memset(&localDateTime, 0, sizeof(dateTimeST));
	memset(&loanPay, 0, sizeof(loanPayST));
	memset(&cardInfo, 0, sizeof(cardSpecST));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "loanPay");

	clearDisplay();
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
	if (*keyMenu)
	{
		cardTracksST    cardTracks;
		memset(&cardTracks, 0, sizeof(cardTracksST));

		displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);

		if(getCardHandle() <= 0)
			enableMSR();

		if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
		{
			return FALSE;
		}

        if (!readTrackData(files, &cardTracks, &cardInfo, FALSE))
        {
                return FALSE;
        }
    }
    else if (!*keyMenu)//..HNO_980623 move up from down
    {
        if (!readCardInfo(&cardInfo))
        {
                disconnectAndClose(1, NOT_SHOW_DC_MSG);
                return FALSE;
        }
    }

    if (fileExist(SUPERVISOR_FILE) == SUCCESS)//..HNO_980623 move up from down
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return;
    }

    
    displayWating();

	if (!printReversal(files))
	{
		disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return FALSE;
	}

	if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

	if (!PrinterAccess(TRUE))
	{
		disconnectAndClose(1, NOT_SHOW_DC_MSG);
		return FALSE;
	}

    if (readPIN)
    {
        if (!enterLoanPayInfo(&cardInfo, &loanPay))
        {
                disconnectAndClose(1, NOT_SHOW_DC_MSG);
                return FALSE;
        }
    }

    if (!doConnect(files))
                    return FALSE;

	retValue = checkTransPrerequisites(files, TRANS_LOANPAY);
	if (retValue != TRUE)
	{
			disconnectAndClose(0);
			return FALSE;
	}

	transResult = LoanPayInquery(files, &cardInfo, &loanPay);

	if (transResult == TRANS_SUCCEED)
	{
		clearKeyBuffer();
		key = displayLoanPay(&cardInfo, &loanPay);
		if (key == KBD_ENTER || key == KBD_CONFIRM)
		{
			clearDisplay();
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "checkConnectionStatus");
			if (checkConnectionStatus() == DISCONNECTED)
			{
				if (!doConnect(files))
				{
					return FALSE;
				}
			}
			LoanPayFinancialTransaction(files, &cardInfo, &loanPay);
		}
	}
		else
			displayMessageBox("��ǘ�� ����� ���!", MSG_WARNING);

	disconnectAndClose(0);
}


//HNO
uint16 LoanPayInquery(filesST* files, cardSpecST* cardInfo, loanPayST* loanpay)
{
    uint8           request[MESSAGE_BUFFER_SIZE]    = {0};								/** request message */
    int             requestLen          			= 0;                                /** request message lenght */
    uint8           response[MESSAGE_BUFFER_SIZE]   = {0};                              /** response message */
    uint16          responseLen         			= sizeof(response);                 /** response message lenght */
    uint16          parseResStatus      			= 0;
    uint16          checkResStatus      			= 0;
    uint8           known               			= FALSE;                            /** error is known or unknown */
    uint8           errorMsg[70]        			= {0};                              /** erropr message */
    uint32          difSec              			= 0;
    uint8           logonFlag           			= FALSE;
    uint8           shaparakState       			= 0;
    terminalSpecST  terminalCapability  			= getTerminalCapability();//MRF_NEW19
    dateTimeST      localDateTime;
    messageSpecST   messageSpec;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    memcpy(&(messageSpec.cardSpec), cardInfo, sizeof(cardSpecST));

    messageSpec.transType = TRANS_LOANPAY_INQUERY;

    if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");

        #if defined INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return UNSUCCESS_TRANS_NOT_SENT;
    }
    
    displayMessage("������� ���", DISPLAY_START_LINE, ALL_LINES);
    
    incAndSaveStan(files, &(messageSpec.merchantSpec));
    strcpy(messageSpec.loanPay.destinationCardPAN ,loanpay->destinationCardPAN);
    strcpy(messageSpec.loanPay.payAmount, loanpay->payAmount);
    
    SIMGetIMSI(messageSpec.IMSI);
    requestLen = createloanPayInqueryMessage87(files, request, & messageSpec);

    if (requestLen <= 0)
    {
        displayMessageBox("���� �����", MSG_ERROR);
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_SENT;
    }

    displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connSendBuff(request, requestLen))
    {
        displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_SENT;
    }
    
    displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connReceiveBuff(response, &responseLen))
    {
        displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_NOT_RECEIVED;
    }
    
    messageSpec.responseStatus = 0;
    checkResStatus = checkResponseValidity87(&messageSpec, messageSpec.transType,
            response, responseLen, request, requestLen);
    
    parseResStatus = parseLoanPayInqueryResponse87(response, responseLen, &messageSpec);
    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec.responseStatus = parseResStatus;
    }
    
    if (messageSpec.responseStatus == 99)
    {  
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return FALSE;
        }

        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        setLogonScheduleAfterUnsuccessLogon(files);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else  if (messageSpec.responseStatus != SUCCESS && messageSpec.responseStatus != 16
            && messageSpec.responseStatus != 8)
    {
    	getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
    	addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus,known);//HNO_ADD
        displayMessageBox(errorMsg, MSG_ERROR);
    }
    
    //save information in loan structure to access it in financial transaction
    //-MRF_971015
//    removePad(messageSpec.loanPay.destinationCardHolderName, ' ');
//    removePad(messageSpec.loanPay.destinationCardHolderFamily, ' ');
//    removePad(messageSpec.loanPay.realAmount, ' ');

    strcpy(loanpay->destinationCardHolderName, messageSpec.loanPay.destinationCardHolderName);
    strcpy(loanpay->destinationCardHolderFamily, messageSpec.loanPay.destinationCardHolderFamily);
    strcpy(loanpay->realAmount, messageSpec.loanPay.realAmount);
    
    //Set time with switch
    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);

    if ((logonFlag == TRUE) || (messageSpec.responseStatus != SUCCESS))
    {
        disconnectAndClose(0);
        return FALSE;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }

    if (!saveMessageSpec(files, &messageSpec))
    {
        disconnectAndClose(0);
        return UNSUCCESS_TRANS_RESPONSE_ERROR;
    }

    return TRANS_SUCCEED;
}

void LoanPayFinancialTransaction(filesST* files, cardSpecST* cardInfo, loanPayST* loanPay)
{
    uint8           		request[MESSAGE_BUFFER_SIZE]    = {0};          			/** request message */
    int             		requestLen          			= 0;            			/** request message lenght */
    uint8           		response[MESSAGE_BUFFER_SIZE]   = {0};          			/** response message */
    uint16          		responseLen         			= sizeof(response);         /** response message lenght */
    uint16          		parseResStatus      			= 0;
    uint16          		checkResStatus      			= 0;
    uint8           		known               			= FALSE;        			/** error is known or unknown */
    uint8           		errorMsg[70]        			= {0};          			/** erropr message */
    uint32          		difSec              			= 0;
    uint8           		valueWithComma[64] 				= {0};
    uint8           		shaparakState       			= 0;
    uint8           		logonFlag           			= FALSE;
    uint8           		retValue            			= FALSE;
    terminalSpecST  		terminalCapability  			= getTerminalCapability();//MRF_NEW19
    dateTimeST      		localDateTime;
    messageSpecST   		messageSpec;

    memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));

    messageSpec.transType = TRANS_LOANPAY;
    memcpy(&(messageSpec.cardSpec), cardInfo, sizeof(cardSpecST));
    
//    if (!prePrint(TRUE, files))//HNO_980602 #remove preprint from transactions
//    {
//    	disconnectAndClose(1, NOT_SHOW_DC_MSG);
//        return;
//    }
    
    if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		
		#ifdef INGENICO
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return;
    }

    incAndSaveStan(files, &(messageSpec.merchantSpec));

    // save loan structure information in messagespec structure for create loan message
    messageSpec.loanPay.STAN = messageSpec.merchantSpec.STAN;                   //for add in last customer
    messageSpec.loanPay.dateTime = messageSpec.dateTime;                        //it will be use for loan tracking
    strcpy(messageSpec.loanPay.payAmount, loanPay->payAmount);
    strcpy(messageSpec.loanPay.realAmount, loanPay->realAmount);
    strcpy(messageSpec.loanPay.destinationCardPAN,loanPay->destinationCardPAN);
    strcpy(messageSpec.loanPay.PAN, messageSpec.cardSpec.PAN);                  //it will be use for loan tracking
    strcpy(messageSpec.loanPay.destinationCardHolderName, loanPay->destinationCardHolderName);//it will be use for print & loan tracking
    strcpy(messageSpec.loanPay.destinationCardHolderFamily, loanPay->destinationCardHolderFamily);//it will be use for print & loan tracking

    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan STAN:%d", messageSpec.loanPay.STAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "payAmount:%s", messageSpec.loanPay.payAmount);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "realAmount: %s", messageSpec.loanPay.realAmount);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "destinationCardPAN: %s", messageSpec.loanPay.destinationCardPAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan date time: %d", messageSpec.loanPay.dateTime);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "pan: %s", messageSpec.loanPay.PAN);

    displayMessage("������ ���", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec.IMSI);

    requestLen = createloanPayTransMessage87(files, request, &messageSpec);
    if (requestLen <= 0)
    {
        displayMessageBox("���� �����", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }

    displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connSendBuff(request, requestLen))
    {
        displayMessageBox("����� ������", MSG_ERROR);
        disconnectAndClose(0);
        return;
    }
    
    //HNO_IDENT save information after success send
    //save loan info in file to print uncertain(power off)loan info when pos power on
    messageSpec.responseStatus = FAILURE; //for manage data log
    makeTransPrintDataLog(files, &messageSpec);

    //save loan information in file to use in tracking transaction
    retValue = saveLoanInfo(LOAN_INFO_FILE, messageSpec.loanPay);
    if (retValue != TRUE)
    {
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    messageSpec.responseStatus = 95;//HNO_ADD for manage printer when we don't receive financial trans response
    displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
    if (!connReceiveBuff(response, &responseLen))
    {
        displayMessageBox("������ ������", MSG_ERROR);
        if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
            displayMessageBox("���� ��ǘ�� �� ����� ������", MSG_ERROR);

        fileRemove(files->customerReceiptFile);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);
        return;
    }
    
    //HNO_COMMENT print retrievalReference empty in last customer print
    messageSpec.responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(&messageSpec, messageSpec.transType, response, responseLen, request, requestLen);
    parseResStatus = parseLoanPayTransResponse87(response, responseLen, &messageSpec);

     if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = checkResStatus;
    }
    else if (parseResStatus != SUCCESS)
    {
        messageSpec.responseStatus = parseResStatus;
    }

    if ((messageSpec.responseStatus == SUCCESS)
            || (messageSpec.responseStatus == 8)
            || (messageSpec.responseStatus == 16))
    {
        addLoanPayReport(&messageSpec);
        makeTransPrintDataLog(files, &messageSpec);
    }

    shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
    if (messageSpec.responseStatus == 99)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");

        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(0);
            return;
        }

        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
    else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR))
    {
        memset(errorMsg, 0, sizeof(errorMsg));
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        disconnectAndClose(0);
    }
    else if (shaparakState == INTERNAL_ERROR)
    {
        memset(errorMsg, 0, sizeof(errorMsg));
        getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
        addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus,known);
        displayMessageBox(errorMsg, MSG_ERROR);
        
        if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        
        fileRemove(files->customerReceiptFile);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
        disconnectAndClose(0);
        return;
    }

    //Set time with switch
    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;

    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);

    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        displayMessageBox("���� ��ǘ�� �� ����� ������", MSG_ERROR);
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    if (!saveMessageSpec(files, &messageSpec))
    {
        if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
			displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    if (!printLoanPayTransaction(files, &messageSpec, TRUE, NO_REPRINT))
    {
		displayMessageBox(" ���� ���� ���� Ȑ����!", MSG_ERROR);//ABS:ADD
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

    fileRemove(files->customerReceiptFile);
    
    if (getPOSService().merchantReceipt)//MRF_NEW19
    {
        retValue = displayMessageBox("���� ������� �ǁ ��Ͽ", MSG_CONFIRMATION);
        PrinterAccess(FALSE);//HNO_IDENT8
        if (retValue == KBD_ENTER || retValue == KBD_CONFIRM)//#MRF_971017
            printLoanPayTransaction(files, &messageSpec, FALSE, NO_REPRINT);
    }

    disconnectAndClose(0);
}

//HNO
uint16 LoanPayTrackingTrans(argumentListST* args)
{
    filesST*            files 			  				= (filesST*) (args->argumentName);
    messageSpecST*      messageSpec 	  				= (messageSpecST*) (args->next->argumentName);
    uint8*              keyMenu           				= (uint8*) (args->next->next->argumentName);
    uint8               request[MESSAGE_BUFFER_SIZE] 	= {0}; 							  /** request message */
    uint8               response[MESSAGE_BUFFER_SIZE] 	= {0};            				  /** response message */
    uint16              requestLen 		  				= 0;                              /** request message lenght */
    uint16              responseLen	      				= sizeof(response);               /** response message lenght */
    uint16              responseStatus    				= 0;
    uint8               key 			  				= 0;
    uint8               known			  				= FALSE;                          /** error is known or unknown */
    uint8               errorMsg[70] 	  				= {0};                            /** erropr message */
    uint32              difSec            				= 0;
    uint16              checkResStatus 	  				= 0;
    uint8               shaparakState     				= 0;
    uint8               logonFlag         				= FALSE;
    uint8               strSTAN[6 + 1]    				= {0};
    uint32              STAN              				= 0;
    int                 transactionNumber 				= 0;                              /** charge number with specified date in file */
    uint32              requestedNum      				= 0;
    serviceSpecST       POSService        				= getPOSService();
    terminalSpecST      terminalCapability  			= getTerminalCapability();//MRF_NEW19
    messageSpecST       tempMessageSpec;
    loanPayST			loanPaySpec;
    cardSpecST          cardSpec;
    dateTimeST          localDateTime;


	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "loan tracking trans");

    memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
	memset(&loanPaySpec, 0, sizeof(loanPayST));

    printReversal(files);

    if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

	if (!enterLoanPayTrackingInfo(strSTAN, cardSpec.PIN))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

	messageSpec->transType = TRANS_LOAN_TRACKING;
    STAN = atoi(strSTAN);

    if (!readSTANTransFromFile(LOAN_INFO_FILE, &loanPaySpec, sizeof(loanPayST), MAX_LOAN_TRANS, STAN,
            &transactionNumber, &requestedNum))
    {
        return FALSE;
    }
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "stan: %d", loanPaySpec.STAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "real amount:%s", loanPaySpec.realAmount);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "firstName: %s", loanPaySpec.destinationCardHolderName);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "last name: %s", loanPaySpec.destinationCardHolderFamily);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan PAN: %s", loanPaySpec.destinationCardPAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "loan time: %d", loanPaySpec.dateTime.time);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "amount: %s", loanPaySpec.payAmount);

	if (!doConnect(files))
	{
		return FALSE;
	}

	if (checkTransPrerequisites(files, messageSpec->transType) != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		#if defined (INGENICO) || defined(INGENICO5100) //HNO_CHANGE
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->loanPay = loanPaySpec;

	displayMessage("����� ���", DISPLAY_START_LINE, ALL_LINES);
    SIMGetIMSI(messageSpec->IMSI);
	requestLen = createLoanPayTrackingMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		disconnectAndClose(0);
		return FALSE;
	}

	checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType,
            response, responseLen, request, requestLen);

	responseStatus = parseLoanPayTrackingResponse87(response, responseLen, messageSpec);

    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (responseStatus != SUCCESS && responseStatus != 99)
    {
        messageSpec->responseStatus = responseStatus;
    }

	shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
    if (messageSpec->responseStatus == 99)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");

        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }

        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
	else if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
	}

    displayMessageBox(messageSpec->loanPay.status, MSG_INFO);

	if (!saveMessageSpec(files, messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}

    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }

    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);

    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return FALSE;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }

	key = displayMessageBox("��� ���� �� �ǁ ������ ����Ͽ", MSG_CONFIRMATION);
	if (key == KBD_ENTER || key == KBD_CONFIRM)
		printLoanTrakingTransaction(files, messageSpec, TRUE, NO_REPRINT);

	disconnectAndClose(0);
	return TRUE;
}

//HNO
uint8 goToManager(void)
{
	uint8 retValue = FALSE;

	int 	displayHeaderStatus		= changeDisplayHeader(0);

	retValue = checkDownMenuPassword();
	changeDisplayHeader(displayHeaderStatus);

	return retValue;
}

//HNO_ADD_ROLL
uint8 rollRequestTrans(argumentListST* args)
{
	filesST* 		files 								= (filesST*) (args->argumentName);
	uint8 			request[MESSAGE_BUFFER_SIZE] 		= {0}; 								/** request message */
	uint16 			requestLen 							= 0; 								/** request message lenght */
	uint8 			response[MESSAGE_BUFFER_SIZE] 		= {0}; 								/** response message */
	uint16 			responseLen 						= sizeof(response);					/** response message lenght */
	int8 			checkPrerequisites					= 0; 								/** check transaction prequisite result */
	int 			responseCode 						= -1; 								/** response message response code(39) */
	uint8 			known								= FALSE; 							/** error is known or unknown */
	uint8 			errorMsg[70] 						= {0};
	int 			parseResponseResult 				= -1;
    uint32          difSec              				= 0;
    uint8           shaparakState       				= 0;
    uint8			rollCount[3] 						= {0};
    uint8           retValue            				= FALSE;
    terminalSpecST  terminalCapability  				= getTerminalCapability();//MRF_NEW19
	messageSpecST   messageSpec;
    dateTimeST      localDateTime;
    rollRequestST	rollRequest;

	memset(&messageSpec, 0, sizeof(messageSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));
    memset(&rollRequest, 0, sizeof(rollRequestST));

	if (!doConnect(files))
        return FALSE;

	messageSpec.transType = TRANS_ROLL_REQUEST;

	if (!initMessageSpec(files, messageSpec.transType, &messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
        
#ifdef INGENICO
		sramFileRemove(files->masterKeyFile);
#else
		fileRemove(files->masterKeyFile);
#endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	incAndSaveStan(files, &(messageSpec.merchantSpec));

	if (!getStringValue("����� ��� ��������:", 3, 1, rollCount, 0, 0, 0, "0", 0))
		return FALSE;

	strcpy(messageSpec.rollRequest.rollCount, rollCount);
	messageSpec.rollRequest.STAN = messageSpec.merchantSpec.STAN;//save for last customer file

	displayMessage("������� ���", DISPLAY_START_LINE, ALL_LINES);

	requestLen = createRollRequestMessage87(request, &messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    //save roll information in file to use in tracking transaction
    retValue = saveRollInfo(ROLL_INFO_FILE, messageSpec.rollRequest);
    if (retValue != TRUE)
    {
        fileRemove(files->customerReceiptFile);
        disconnectAndClose(0);
        return;
    }

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "responseLen =%d", responseLen);

    //HNO_COMMENT print retrievalReference empty in last customer print
    messageSpec.responseStatus = 0; //mgh: reset message status

    responseCode = checkResponseValidity87(&messageSpec, messageSpec.transType, response, responseLen, request,requestLen);

	parseResponseResult = parseRollRequestResponse87(response, responseLen, &messageSpec);
    if (responseCode != SUCCESS)
    {
        known = FALSE;
        messageSpec.responseStatus = responseCode;
    }

    else if (parseResponseResult != SUCCESS)
    {
        messageSpec.responseStatus = parseResponseResult;
    }

    if ((messageSpec.responseCode == SUCCESS) || (messageSpec.responseCode == 8) || (messageSpec.responseCode == 16))
    {
    	addRollRequestReport(&messageSpec);
    	makeTransPrintDataLog(files, &messageSpec);
    }

    shaparakState = getShaparakMessages(errorMsg, messageSpec.responseStatus);
	if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
		getIsoErrorMessage(errorMsg, messageSpec.responseStatus, &known);
		addTransactionErrorReport(messageSpec.transType, messageSpec.responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
	}

    //Set time with switch
    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec.dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec.dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec.dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec.dateTime), TRUE, 0);

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }

	if (!saveMessageSpec(files, &messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}

	printRollRequestTrans(files, &messageSpec);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "roll request finish");

	disconnectAndClose(0);
	return TRUE;
}

//HNO_ADD_ROLL
uint16 rollRequestTrackingTrans(argumentListST* args)
{
    filesST*            files 			  				= (filesST*) (args->argumentName);
    messageSpecST*      messageSpec 	  				= (messageSpecST*) (args->next->argumentName);
    uint8               request[MESSAGE_BUFFER_SIZE] 	= {0}; 							/** request message */
    uint16              requestLen 		  				= 0; 							/** request message lenght */
    uint8               response[MESSAGE_BUFFER_SIZE] 	= {0}; 							/** response message */
    uint16              responseLen	      				= sizeof(response); 			/** response message lenght */
    uint16              responseStatus    				= 0;			
    uint8               key 			  				= 0;			
    uint8               known			  				= FALSE; 						/** error is known or unknown */
    uint8               errorMsg[70] 	  				= {0}; 							/** erropr message */
    uint32              difSec            				= 0;			
    uint16              checkResStatus 	  				= 0;			
    uint8               shaparakState     				= 0;			
    uint8               logonFlag         				= FALSE;			
    uint8               strSTAN[6 + 1]    				= {0};			
    uint32              STAN              				= 0;			
    int                 transactionNumber 				= 0;            				/** charge number with specified date in file */
    uint32              requestedNum      				= 0;
    terminalSpecST      terminalCapability  			= getTerminalCapability();//MRF_NEW19
    messageSpecST       tempMessageSpec;
    rollRequestST		rollRequest;
    dateTimeST          localDateTime;

    memset(&localDateTime, 0, sizeof(dateTimeST));
	memset(&rollRequest, 0, sizeof(rollRequestST));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**rollRequestTrackingTrans");
    
//    preDial(files, 0);

    if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

	if (!enterRollRequestTrackingInfo(strSTAN))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

	messageSpec->transType = TRANS_ROLL_TRACKING;
    STAN = atoi(strSTAN);

    if (!readSTANTransFromFile(ROLL_INFO_FILE, &rollRequest, sizeof(rollRequest), MAX_Roll_TRANS, STAN,
            &transactionNumber, &requestedNum))
    {
        return FALSE;
    }
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "stan: %d", rollRequest.STAN);
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "roll count:%s", rollRequest.rollCount);

	if (!doConnect(files))
	{
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
		#if defined (INGENICO) || defined(INGENICO5100) //HNO_CHANGE
                sramFileRemove(files->masterKeyFile);
        #else
                fileRemove(files->masterKeyFile);
        #endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->rollRequest = rollRequest;

	displayMessage("����� ������� ���", DISPLAY_START_LINE, ALL_LINES);

	requestLen = createRollTrackingMessage87(request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ �������...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType,
            response, responseLen, request, requestLen);

	responseStatus = parseRollTrackingResponse87(response, responseLen, messageSpec);

    if (checkResStatus != SUCCESS)
    {
        known = FALSE;
        messageSpec->responseStatus = checkResStatus;
    }
    else if (responseStatus != SUCCESS && responseStatus != 99)
    {
        messageSpec->responseStatus = responseStatus;
    }

	shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
     if (messageSpec->responseStatus == 99)
    {
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "response Code: 99");

        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }

        setLogonScheduleAfterUnsuccessLogon(files);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        logonFlag = TRUE;
        logonTrans(files);
    }
	else if (shaparakState != SUCCESS_TRANS)
	{
        memset(errorMsg, 0, sizeof(errorMsg));
		known = FALSE;
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
	}

    displayMessageBox(messageSpec->loanPay.status, MSG_INFO);

	if (!saveMessageSpec(files, messageSpec))
	{
		disconnectAndClose(0);
		return FALSE;
	}

    localDateTime = getDateTime();

	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);

    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
    {
        disconnectAndClose(0);
        return FALSE;
    }

    if (difSec >= 300)
    {
        displayMessageBox("������ ����� �� ����", MSG_ERROR);
        disconnectAndClose(0);
        return FALSE;
    }

	key = displayMessageBox("��� ���� �� �ǁ ������ ����Ͽ", MSG_CONFIRMATION);
//	if (key == KBD_ENTER || key == KBD_CONFIRM)
//		printLoanTrakingTransaction(files, messageSpec, TRUE, NO_REPRINT);

	disconnectAndClose(0);
	return TRUE;
}


uint8 behzistiCharity(argumentListST* args)
{
	charityTrans(args, "1");
	return TRUE;
}

uint8 mahakCharity(argumentListST* args)
{
	charityTrans(args, "5");
	return TRUE;
}

uint8 kahrizakCharity(argumentListST* args)
{
	charityTrans(args, "6");
	return TRUE;
}

uint8 komitehEmdadCharity(argumentListST* args)
{
	charityTrans(args, "7");
	return TRUE;
}

uint8 hemmateJavananCharity(argumentListST* args)
{
	charityTrans(args, "8");
	return TRUE;
}

uint8 bonyadeMaskaneEnghelabCharity(argumentListST* args)
{
	charityTrans(args, "9");
	return TRUE;
}

uint8 ashrafolAnbiaCharity(argumentListST* args)
{
	charityTrans(args, "10");
	return TRUE;
}


//HNO_CHARITY
uint8 charityTrans(argumentListST* args, uint8* instituteCode)
{
    filesST*                    files                           = (filesST*) (args->argumentName);
    messageSpecST*              messageSpec                     = (messageSpecST*) (args->next->argumentName);
    uint8*                      keyMenu                         = (uint8*) (args->next->next->argumentName);
    uint8                       request[MESSAGE_BUFFER_SIZE] 	= {0}; 				/** request message */
    uint16                      requestLen                      = 0; 				/** request message lenght */
    uint8                       response[MESSAGE_BUFFER_SIZE] 	= {0}; 				/** response message */
    uint16                      responseLen                     = sizeof(response); /** response message lenght */
    uint8                       retValue                        = TRUE;
    uint16                      checkResStatus                  = 0;
    uint16                      parseResStatus                  = 0;
    uint8                       known                           = FALSE; 			/** error is known or unknown */
    uint8                       errorMsg[70]                    = {0}; 				/** erropr message */
    int                         paymentContinue                 = TRUE;
    uint8                       shaparakState                   = 0;
    uint8                       logonFlag                       = FALSE;
    uint32                      difSec                          = 0;
    uint8 						valueWithComma[64]              = {0};
    uint8                       isPOSStan                       = 0;
    terminalSpecST              terminalCapability              = getTerminalCapability();//MRF_NEW19
    cardSpecST                  cardSpec;
    messageSpecST               tempMessageSpec;
    dateTimeST                  localDateTime;
	uint8						key								= KBD_CANCEL;//ABS:CHANGE place

	memset(&cardSpec, 0, sizeof(cardSpecST));
    memset(&localDateTime, 0, sizeof(dateTimeST));

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****************Charity Trans*****************");
    
    if (!checkSIMStatus()) //MRF_971015
        return FALSE;
    
    if (*keyMenu)
    {
        cardTracksST    cardTracks;
        memset(&cardTracks, 0, sizeof(cardTracksST));
        
        if (!getPOSService().pinPad)//MRF_PINPAD
        {
            displayMessage("���� ���� �� Ș���...", DISPLAY_START_LINE + 1, ALL_LINES);
            if(getCardHandle() <= 0)
                enableMSR();

            if (!readMagnetCardWait(cardTracks.track1, cardTracks.track2, cardTracks.track3, &cardTracks.track1Len, &cardTracks.track2Len, &cardTracks.track3Len))
            {
                return FALSE;
            }

            if (!readTrackData(files, &cardTracks, &cardSpec, FALSE))
            {
                return FALSE;
            }
        }
        else
        {
            //TODO: MRF_PINPAD
        }
    }
    else if (!*keyMenu)
    {
        if (!readCardInfo(&cardSpec))
        {
            disconnectAndClose(1, NOT_SHOW_DC_MSG);
            return FALSE;
        }
    }

    if (fileExist(SUPERVISOR_FILE) == SUCCESS)//..HNO_980623 move up from down
    {
        displayMessageBox("������� ���� �� ����� ��� ��ǘ�� ��� ����!", MSG_INFO);
        fileRemove(SUPERVISOR_FILE);
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    } 
    
    key = displayCharityConfirm();//..HNO_980623 move from up to down
    if (key != KBD_ENTER && key != KBD_CONFIRM)
    {
        displayMessageBox("��ǘ�� ������ ��� ��!", MSG_INFO);
        return FALSE;
    }
    
    displayWating();

    if (!printReversal(files))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    }

//    if (!prePrint(TRUE, files)) -MRF_970903 : MOVE TO DOWN
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//		return FALSE;
//    }

	if (messageSpec == NULL)
	{
		memset(&tempMessageSpec, 0, sizeof(messageSpecST));
		messageSpec = &tempMessageSpec;
	}

    if (!PrinterAccess(TRUE))
    {
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
    } 

    if (!enterCharityInfo(messageSpec->amount, cardSpec.PIN))
    {
#ifdef VX520
		closeModem(TRUE);//ABS:ADD:961024
#endif
        disconnectAndClose(1, NOT_SHOW_DC_MSG);
        return FALSE;
	}

//    if (!prePrint(TRUE, files))//!MRF_970903 : MOVE FROM UP//HNO_980602 #remove preprint from transactions
//    {
//        disconnectAndClose(1, NOT_SHOW_DC_MSG);
//		return FALSE;
//    }
    
	messageSpec->transType = TRANS_CHARITY;

	memcpy(&(messageSpec->cardSpec), &cardSpec, sizeof(cardSpecST));
	strcpy(messageSpec->charity.InstituteCode, instituteCode);

	if (!doConnect(files))
		return FALSE;

	retValue = checkTransPrerequisites(files, messageSpec->transType);
	if (retValue != TRUE)
	{
		disconnectAndClose(0);
		return FALSE;
	}

	if (!initMessageSpec(files, messageSpec->transType, messageSpec))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: masterKeyFile");
#ifdef INGENICO
        sramFileRemove(files->masterKeyFile);
#else
        fileRemove(files->masterKeyFile);
#endif
		displayMessageBox("��� ���� ����� ������� ������.", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	makeValueWithCommaStr(messageSpec->amount, valueWithComma);
	strcat(valueWithComma, " ����");

	displayMessage("������", DISPLAY_START_LINE, ALL_LINES);
	displayMessage(valueWithComma, DISPLAY_START_LINE + 1, NO_LINES);

	incAndSaveStan(files, &(messageSpec->merchantSpec));
    SIMGetIMSI(messageSpec->IMSI);

	requestLen = createCharityMessage87(files, request, messageSpec);
	if (requestLen <= 0)
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	messageSpec->responseStatus = 95; //mgh: for managing reversal printer
	if (!createSaveReversal(files, messageSpec))
	{
		displayMessageBox("���� �����", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("����� ������� ...", DISPLAY_START_LINE + 3, NO_LINES);
	if (!connSendBuff(request, requestLen))
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalReceiptFile");
		fileRemove(files->reversalReceiptFile);
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "**REMOVE: reversalTransFile");
		fileRemove(files->reversalTransFile);
		displayMessageBox("����� ������", MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

	displayMessage("������ ������� ...", DISPLAY_START_LINE + 3, LINE_4);
	if (!connReceiveBuff(response, &responseLen))
	{
		displayMessageBox("������ ������", MSG_ERROR);
//		disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
		reversalTrans(files);
		disconnectAndClose(0);
		return FALSE;
	}

    messageSpec->responseStatus = 0; //mgh: reset message status
    checkResStatus = checkResponseValidity87(messageSpec, messageSpec->transType, 
            response, responseLen, request,	requestLen);

	parseResStatus = parseCharityResponse87(response, responseLen, messageSpec);
	if (checkResStatus != SUCCESS)
	{
		known = FALSE;
		messageSpec->responseStatus = checkResStatus;
	}
	else if (parseResStatus != SUCCESS)
	{
		messageSpec->responseStatus = parseResStatus;
	}

	if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
	{
		displayMessageBox("���� �����", MSG_ERROR);
		reversalTrans(files);
		disconnectAndClose(0);
		return FALSE;
	}

	shaparakState = getShaparakMessages(errorMsg, messageSpec->responseStatus);
	if (messageSpec->responseStatus == 99)
	{
        
        if (fileCreate(LOGON_INFO_FILE) != SUCCESS)
            return FALSE;
        
        setLogonScheduleAfterUnsuccessLogon(files);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: settelmentInfoFile");
        fileRemove(files->settelmentInfoFile);
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**REMOVE: reversalTransFile");
        fileRemove(files->reversalTransFile);
        
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        logonFlag = TRUE;
        logonTrans(files);
	}
	else if ((shaparakState != SUCCESS_TRANS) && (shaparakState != INTERNAL_ERROR) 
            && (shaparakState != UNKOWN_TRANS))//MRF_NEW20
	{
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState != SUCCESS_TRANS");
        memset(errorMsg, 0, sizeof(errorMsg));              
        fileRemove(files->reversalTransFile);
        getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
        addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus, known);
        displayMessageBox(errorMsg, MSG_ERROR);
        printReversal(files);
        disconnectAndClose(0);
	}
	else if ((shaparakState == INTERNAL_ERROR) || (shaparakState == UNKOWN_TRANS))//MRF_NEW20
	{
        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "shaparakState == INTERNAL_ERROR");
        memset(errorMsg, 0, sizeof(errorMsg));
//        disconnectModem();-MRF_970924 : REMOVE 10S UP CONNECTION
                    
        isPOSStan = TRUE; //MRF_NEW20
        if (!updateReversalInfo(files, messageSpec, isPOSStan))//MRF_NEW20
        {
            displayMessageBox("���� �����", MSG_ERROR);
            reversalTrans(files);
            disconnectAndClose(0);
            return FALSE;
        }

		reversalTrans(files);
		getIsoErrorMessage(errorMsg, messageSpec->responseStatus, &known);
		addTransactionErrorReport(messageSpec->transType, messageSpec->responseStatus,known);
		displayMessageBox(errorMsg, MSG_ERROR);
		disconnectAndClose(0);
		return FALSE;
	}

    //Set time with switch
    localDateTime = getDateTime();
	if (terminalCapability.OTPCapability == TRUE)//ABS:CHANGE
    {
        if (localDateTime.date != messageSpec->dateTime.date)
        {
            if (updateFileInfo(POS_TIME_FILE, &messageSpec->dateTime.date, sizeof(uint32)) != SUCCESS)
                return FALSE;
                         
            setPOSDateTime(messageSpec->dateTime.date, messageSpec->dateTime.time);
            if (!PasswordGenerator())
                return FALSE;
        }
    }
    
    difSec = dateTimeDiffSecondes(&(messageSpec->dateTime), &localDateTime);
    difSec = (difSec >= 0) ? difSec : -difSec;
    setSystemDateTime(files, &(messageSpec->dateTime), TRUE, FALSE);
    
    if ((logonFlag == TRUE) || (shaparakState != SUCCESS_TRANS))
        return FALSE;

	if (difSec >= 300)
	{
		displayMessageBox("������ ����� �� ����", MSG_ERROR);
		reversalTrans(files);
		disconnectAndClose(0);
		return FALSE;
	}

    if (!saveMessageSpec(files, messageSpec))
    {
        reversalTrans(files);
        disconnectAndClose(0);
        return FALSE;
    }

    if (!printTransactionCharity(files,messageSpec, TRUE, NO_REPRINT))
    {
        reversalTrans(files);
        disconnectAndClose(0);
        return FALSE;
    }

	fileRemove(files->customerReceiptFile);
	settlementTrans(files);//MRF_NEW16
	disconnectAndClose(0);
    
	return TRUE;
}

//HNO_CHARGE
/**
 * This function create an id base on current time.
 * @param   timeStr [input]: current time
 * @return  random id.
 */
uint32 createID(uint32 timeStr)
{
	uint32		randomID	= 0;
	int			randNom		= 0;

	randomID = timeStr + 323232;

	return randomID;
}

//HNO_DEBUG
void getCharityInstituteName(uint8* instituteName, uint8* instituteCode)
{
	int code = 0;
	code = atoi(instituteCode);

	switch (code)
	{
        case 1:
            strcpy((char*)instituteName, "�������");
            break;
        case 5:
            strcpy((char*)instituteName, "�͘");
            break;
        case 6:
            strcpy((char*)instituteName, "����Ҙ");
            break;
        case 7:
            strcpy((char*)instituteName, "����� �����");
            break;
        case 8:
            strcpy((char*)instituteName, "��� ������");
            break;
        case 9:
            strcpy((char*)instituteName, "����� �Ә� ������");
            break;
        case 10:
            strcpy((char*)instituteName, "���� ��������");
            break;
        default:
            strcpy((char*)instituteName , "������");
            break;
	}

}

