#include <string.h> 
//MRF #include <stdlib.h> 
//MRF #include <stdio.h> 
#include "N_common.h"
#include "N_utility.h"
#include "N_error.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_dateTime.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_messaging.h"
#include "N_fileManage.h" 
#include "N_terminalReport.h" 
#include "N_keyManagerWrapper.h"
#include "N_keyManager.h"
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h"
#include "N_dateTimeWrapper.h"
#include "N_display.h" 
#include "N_initialize.h"
#include "N_log.h" 

#define LEFT_MASK(a)   (a = (((uint8)a) >> 4))
#define RIGHT_MASK(a)  (a = ((a = (((uint8)a) << 4)) >> 4))

/**
 * Load security key.
 * @param  files [input]: Is Structure that include files
 * @param  fileName [input]: file name
 * @param  keyForDecryption [input]: key for Decryption
 * @param  decryptedKey [input]: decrypted key
 * @param  keyLen [input]: lenght of key
 * @see    readKey().
 * @see    decryptData_3DES().
 * @see    comportHexShow().
 * @see    decryptData().
 * @return TRUE or FALSE.
 */
uint8 loadSecurityKey(filesST* files, uint8* fileName, uint8* keyForDecryption, uint8* decryptedKey, int32 keyLen) 
{
	uint8   key[16] = {0};
    
	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**loadSecurityKey**");
    
	memset(key, 0, 16);
    
    if (!readKey(files, fileName, key, keyLen))
        return FALSE;
    
    memset(decryptedKey, 0, 16);
    
#ifdef INGENICO_5100
    decryptData(keyForDecryption, key, keyLen, decryptedKey);
#else
    decryptData_3DES(keyForDecryption, key, 16, decryptedKey);
#endif
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "key:");
    comportHexShow(key, strlen(key));
    
    showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "decryptedKey:");
    comportHexShow(decryptedKey, strlen(decryptedKey));

    return TRUE;
}



/**
 * Verify MAC for use in parse message functions.
 * @param  key [input]: current MAC key
 * @param  buffer [input]: message
 * @param  len [input]: length of message - 8
 * @param  hex [input]: HEX or not HEX?
 * @see    comportHexShow().
 * @see    createMac().
 * @see    binStrToHex().
 * @return TRUE if input MAC is TRUE and FALSE if input MAC is FALSE.
 */
int verifyMac(uint8* key, uint8* buffer, uint32 len, uint8 hex)  
{
    uint8 currentHexMac[17]     = {0};		/* create mac key result in hex form */
    uint8 currentBinMac[9]      = {0};		/* create mac key result in binary form */
    uint8 messageMac[9]         = {0};      /* message mac key */
    
    if (len < 0)
        return FALSE;

    if (hex) 
    {
        memcpy(messageMac, buffer + len, 8);
        messageMac[8] = 0;
        createMac(key, buffer, len, currentHexMac, currentBinMac);
        
        if (!memcmp(currentHexMac, messageMac, 8))
            return TRUE;
        else 
            return FALSE;
    }
    else
    {
		memcpy(messageMac, buffer + len, 8);
        messageMac[8] = 0;
        createMac(key, buffer, len, currentHexMac, currentBinMac);
        if (!memcmp(currentBinMac, messageMac, 8))
            return TRUE;
        else 
            return FALSE;
    }
}

/**
 * @param   messageSpec [input]: Is structure
 * @param   outputHex [output]: outputHex HEX form PIN
 * @param   output [output]: output binary form PIN
 * @see     encryptData_3DES().
 * @see     binStrToHex().
 * @return  outputHext and output binary.
 */
void createPin87(messageSpecST* messageSpec, uint8* outputHex, uint8* output)  
{
    uint8   code[2][8]  = {0};                                  /* data that used for encrypt & encryypt result */
    int     counter     = 0;                                    /* loop counter */
    int     index       = messageSpec->cardSpec.PANLen - 13;    /* index of encrypt function input/output argument */     
    uint8*  pin         = messageSpec->cardSpec.PIN;            /* PIN */
    uint8*  track2      = messageSpec->cardSpec.track2;         /* Track 2 of card */

    showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "pin is : %s", pin);
    showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "track2 is : %s", track2);

    code[0][0] = 4;
    code[0][1] = (pin[0] - '0') * 16 + (pin[1] - '0');
    code[0][2] = (pin[2] - '0') * 16 + (pin[3] - '0');
    code[0][3] = 255;
    code[0][4] = 255;
    code[0][5] = 255;
    code[0][6] = 255;
    code[0][7] = 255;
    code[1][0] = 0;
    code[1][1] = 0;
    code[1][2] = (track2[index]    - '0') * 16 + (track2[index + 1]  - '0');
    code[1][3] = (track2[index+2]  - '0') * 16 + (track2[index + 3]  - '0');
    code[1][4] = (track2[index+4]  - '0') * 16 + (track2[index + 5]  - '0');
    code[1][5] = (track2[index+6]  - '0') * 16 + (track2[index + 7]  - '0');
    code[1][6] = (track2[index+8]  - '0') * 16 + (track2[index + 9]  - '0');
    code[1][7] = (track2[index+10] - '0') * 16 + (track2[index + 11] - '0');
    
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "code[0] is : "); //ABS:ADD
//	comportHexShow(code[0], 20);
//    
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "code[1] is : ");
//	comportHexShow(code[1], 20);
    
    for (counter = 0; counter < 8; ++counter) 
        code[0][counter] = code[0][counter] ^ code[1][counter];
    
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "code[0]1 is : ");//ABS:ADD
//	comportHexShow(code[0], 20);
//	
//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pinKey is : ");
//	comportHexShow(messageSpec->terminalKey.pinKey, strlen(messageSpec->terminalKey.pinKey));
    
#ifdef INGENICO
		encryptData(messageSpec->terminalKey.pinKey, code[0], code[1]);
#else
	  	encryptData_3DES(messageSpec->terminalKey.pinKey, code[0], code[1]);
#endif

//	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "code[1] is : ");//ABS:ADD
//	comportHexShow(code[1], 20);
    
    binStrToHex(code[1], outputHex, 8);
    memcpy(output, code[1], 8);
}

/**
 * Calculate parity adjustment for random key/ISO 8583 V93.
 * @param   keyValue [input]: random key value
 * @return  TRUE or FALSE.
 */
uint8 parityAdjustment(char* keyValue) 
{
    int     rightMask[16]   = {1, 0, 3, 2, 5, 4, 7, 6, 9, 8, 0xb, 0xa, 0xd, 0xc, 0xf, 0xe};
    int     i               = 0;
    uint8   print[10]       = {0};
    uint8   var             = 0;

    if (keyValue != NULL)
    {
        for (i = 0; i < 8; i++)
        {
            var = keyValue[i];
            LEFT_MASK(var);
            sprintf(print, "1>>> %2x", var);       
            switch (var)
            {
                case 0x0: 
                case 0x3:     
                case 0x5:   
                case 0x6:   
                case 0x9:   
                case 0xa:      
                case 0xc:   
                case 0xf:
                    var = keyValue[i];
                    RIGHT_MASK(var);
                    sprintf(print, "2>>> %2x", var);
                    switch (var)
                    {
                        case 0x0: 
                        case 0x3:     
                        case 0x5:   
                        case 0x6:   
                        case 0x9:      
                        case 0xa:   
                        case 0xc:   
                        case 0xf:
                            keyValue[i] = (keyValue[i] & 0xf0) + 
                                           rightMask[(int)var];
                            break;
                    }
                    break;
                case 0x1: 
                case 0x2:     
                case 0x4:   
                case 0x7:   
                case 0x8:   
                case 0xb:      
                case 0xd:   
                case 0xe:
                    var = keyValue[i];
                    RIGHT_MASK(var);
                    sprintf(print, "3>>> %2x", var);
                    switch (var)
                    {
                        case 0x1: 
                        case 0x2:     
                        case 0x4:   
                        case 0x7:   
                        case 0x8:      
                        case 0xb:   
                        case 0xd:   
                        case 0xe:
                            keyValue[i] = (keyValue[i] & 0xf0) + 
                                           rightMask[(int)var];
                            break;
                  }
                  break;
                default:
                    break;
            }
        }
        return TRUE;
    }
    return FALSE;
} 

/**
 * Create PIN (field 52 in iso message) used in create message functions AND 
 * ENCRYPTED KEY (FIELD 48)/ISO 8583 V93.
 * @param   messageSpec [input]: Is structure
 * @param   encryptedKey [input]: encrypted key
 * @param   outputHex [output]: HEX form PIN
 * @param   output: [output] binary form PIN
 * @see     generateRandomKey().
 * @see     comportHexShow().
 * @see     encryptData().
 * @see     encryptData_3DES().
 * @see     binStrToHex().
 * @return  outputHEX PIN and output binary pin.
 */
void createPinAndEncryptedKey93(messageSpecST* messageSpec, uint8* output, uint8* encryptedKey)  
{
    uint8   randomKey[9]            = {0};
    uint8   code[2][8]              = {0};                                  /** data that used for encrypt & encryypt result */
    int     counter                 = 0;                                    /** loop counter */
    uint8   encryptedKeyBin[9]      = {0};    
    int     index                   = messageSpec->cardSpec.PANLen - 13;    /** index of encrypt function input/output argument */     
    uint8*  pin                     = messageSpec->cardSpec.PIN;
    uint8*  track2                  = messageSpec->cardSpec.track2;
    
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "createPin93");
    
    generateRandomKey(randomKey);

    code[0][0] = 4;
    code[0][1] = (pin[0] - '0') * 16 + (pin[1] - '0');
    code[0][2] = (pin[2] - '0') * 16 + (pin[3] - '0');
    code[0][3] = 255;
    code[0][4] = 255;
    code[0][5] = 255;
    code[0][6] = 255;
    code[0][7] = 255;
    code[1][0] = 0;
    code[1][1] = 0;
    code[1][2] = (track2[index]    - '0') * 16 + (track2[index + 1]  - '0');
    code[1][3] = (track2[index+2]  - '0') * 16 + (track2[index + 3]  - '0');
    code[1][4] = (track2[index+4]  - '0') * 16 + (track2[index + 5]  - '0');
    code[1][5] = (track2[index+6]  - '0') * 16 + (track2[index + 7]  - '0');
    code[1][6] = (track2[index+8]  - '0') * 16 + (track2[index + 9]  - '0');
    code[1][7] = (track2[index+10] - '0') * 16 + (track2[index + 11] - '0');
    
    for (counter = 0; counter < 8; ++counter) 
        code[0][counter] = code[0][counter] ^ code[1][counter];

    encryptData(randomKey, code[0], output);
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "pinBin:");
//    comportHexShow(output, 8);
     
#ifdef INGENICO
    encryptData(MASKAN_TEST_GENERAL_KEY, randomKey, encryptedKeyBin);
#else
    encryptData_3DES(MASKAN_TEST_GENERAL_KEY/*mgh_93 messageSpec->terminalKey.masterKey*/, randomKey, encryptedKeyBin);
#endif
    binStrToHex(encryptedKeyBin, encryptedKey, 8);
}


/**
 * Create Mac key.
 * @param   key [input]: Is key
 * @param   input [input]: message 
 * @param   len [input]: len of message - 8
 * @param   outputHex [output]: HEX form PIN
 * @param   output [output]: binary form PIN
 * @see     comportHexShow().
 * @see     encryptData().
 * @see     decryptData().
 * @see     binStrToHex().
 * @return  outputHEX PIN and output binary pin.
 */
void createMac(uint8* key, uint8* input, uint32 len, uint8* outputHex, uint8* output) 
{
    uint32  i                     = 0;				/** loop counter */
    uint32  j                     = 0;				/** loop counter */
    uint8   code[9]               = {0};			/** data that used for encrypt */
    uint8 	finalOutput[8 + 1]	  = {0};
    uint8	firstPartKey[8 + 1]	  = {0};
    uint8   secondPartKey[8 + 1]  = {0};

    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "**create Mac**");
       
    setKeyLenS(8);
    memcpy(firstPartKey, key, 8);
	showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "firstPartKey:");
	comportHexShow(firstPartKey, 8);
	
    while (len % 8) //pad 0 end of last block
        input[len++] = 0;

    memset(code, 0, sizeof(code));
    
    for (j = 0; j < 8; ++j) // init output
        output[j] = 0;

    for (i = 0; i < len; i += 8)
    {
        for (j = 0; j < 8; ++j) 
            code[j] = input[i+j] ^ output[j];
			
        encryptData(firstPartKey, code, output);
	}
   
    memcpy(secondPartKey, &key[8], 8);
#if defined(ICT250) || defined(IWL220)
    decryptData_3DES(secondPartKey, output, 8, finalOutput);
#else
    decryptData(secondPartKey, output, 8, finalOutput);
#endif

    memset(output, 0, sizeof(output));
    encryptData(key, finalOutput, output);
    
    setKeyLenS(16);
    binStrToHex(output, outputHex, 8);
}

/**
 * Generate random key/ISO 8583 V93.
 * @param   randomKLey [input]: random key
 * @see     getTicks().
 * @see     parityAdjustment().
 * @return  parityAdjustment().
 */
uint8 generateRandomKey(uint8* randomKLey) 
{
    uint8 counter = 0;
          
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "generateRandomKey");
     
/*
    srand(getSecond(systemDateTimeToTime()));
    
    randomKLey[counter] = rand() % (255 - 1 + 1) + 1;
    counter++;
    for (counter; counter < 8; counter++)
        randomKLey[counter] = rand() % (255 - 0 + 1) + 0;
*/
    randomKLey[counter] = getTicks() % (255 - 1 + 1) + 1;
    counter++;
    for (counter; counter < 8; counter++)
        randomKLey[counter] = getTicks() % (255 - 0 + 1) + 0;
    
    return parityAdjustment(randomKLey);
}

/**
 * Create Mac key.
 * @param   key [input]: Is key
 * @param   input [input]: message 
 * @param   len [input]: len of message - 8
 * @param   outputHex [output]: HEX form PIN
 * @param   output [output]: binary form PIN
 * @see     comportHexShow().
 * @see     encryptData().
 * @see     binStrToHex().
 * @return  outputHEX PIN and output binary pin.
 */
void createMac_old(uint8* key, uint8* input, uint32 len, uint8* outputHex, uint8* output) 
{
    uint32  i                     = 0;				/* loop counter */
    uint32  j                     = 0;				/* loop counter */
    uint8   code[9]               = {0};			/* data that used for encrypt */

    showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "create Mac");
    
    while (len % 8)
        input[len++] = 0;

    memset(code, 0, sizeof(code));
    
    for (j = 0; j < 8; ++j) // init output
        output[j] = 0;
 
//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "input is : ");
//    comportHexShow(input, len);

/*
    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "decryptedMasterKey is : ");
    comportHexShow(messageSpec->terminalKey.masterKey, 8);
*/

//    showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "decryptedMacKey is : ");
//    comportHexShow(key, 8);

    for (i = 0; i < len; i += 8)
    {
        for (j = 0; j < 8; ++j) 
            code[j] = input[i+j] ^ output[j];
        encryptData(key, code, output);
    }
   
    binStrToHex(output, outputHex, 8);
    
    showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "Mac is : %s", outputHex);
}

