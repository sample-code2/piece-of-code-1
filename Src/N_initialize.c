#include <string.h> 
//MRF #include <stdio.h>    
//MRF #include <stdlib.h> 
#include "N_common.h"
#include "N_utility.h" 
#include "N_dateTime.h"
#include "N_binaryTree.h"
#include "N_menu.h"
#include "N_scheduling.h"
#include "N_merchantSpec.h"
#include "N_cardSpec.h"
#include "N_connection.h"
#include "N_connectionWrapper.h"
//#include "N_connection.h"
#include "N_messaging.h"
#include "N_transactions.h"
#include "N_transactionReport.h"
#include "N_terminalReport.h"
#include "N_config.h"
#include "N_passManager.h"
#include "N_error.h"
//#include "N_xml_lib.h"
#include "mxml.h"
#include "N_fileManageWrapper.h"
#include "N_fileManage.h"
#include "N_terminalSpec.h" 
#include "N_POSSpecific.h" 
#include "N_initialize.h"
#include "N_displayWrapper.h" 
#include "N_display.h"
#include "N_log.h"
#include "N_printerWrapper.h"
#include "N_keyManagerWrapper.h"
#include "N_dateTimeWrapper.h"
#include "N_TMS.h"
#include "N_PCPOS.h"
#include "N_userManagement.h"

/*
* Add Item in tree menu.
* @param   menuItemList [input]: menu item list
* @param   menuItemNameEN [input]: parent node
* @param   menuItemNameEN [input]: child node
* @return  None.
*/
void addMenuItemList(menuItemST** menuItemList, enum menuItemNameEN  parent, enum menuItemNameEN child);
void fillFileInfo(serverSpecST* serverSpec, uint8 server);

/*
* Fill file information.
* @param   serverSpec [input]: management the number of switches
* @param   server [input]: the number of switch
* @return  None.
*/
void fillFileInfo(serverSpecST* serverSpec, uint8 server)
{
	sprintf(serverSpec->files.connFiles[0], "%s%d", CONN_FILE1, server);
	sprintf(serverSpec->files.connFiles[1], "%s%d", CONN_FILE2, server);
	sprintf(serverSpec->files.connFiles[2], "%s%d", CONN_FILE3, server);
	sprintf(serverSpec->files.connFiles[3], "%s%d", CONN_FILE4, server);
	sprintf(serverSpec->files.connFiles[4], "%s%d", CONN_FILE5, server);
	sprintf(serverSpec->files.connFiles[5], "%s%d", CONN_FILE6, server);
	sprintf(serverSpec->files.customerReceiptFile, "%s%d", CUSTOMER_RECEIPT_FILE, server);
	sprintf(serverSpec->files.generalConnInfoFile, "%s%d", GENERAL_CONN_INFO_FILE, server);
	sprintf(serverSpec->files.macKeyFile, "%s%d", MAC_KEY_FILE, server);
	sprintf(serverSpec->files.masterKeyFile, "%s%d", MASTER_KEY_FILE, server);
	sprintf(serverSpec->files.merchantSpecFile, "%s%d", MERCHANT_SPEC_FILE, server);
	sprintf(serverSpec->files.pinKeyFile, "%s%d", PIN_KEY_FILE, server);
	sprintf(serverSpec->files.prefixFile, "%s%d", PREFIX_FILE, server);
	sprintf(serverSpec->files.reversalReceiptFile, "%s%d", REVERSAL_RECEIPT_FILE, server);
	sprintf(serverSpec->files.reversalTransFile, "%s%d", REVERSAL_TRANS_FILE, server);
	sprintf(serverSpec->files.scheduleFile, "%s%d", SCHEDULE_FILE, server);
	sprintf(serverSpec->files.settelmentInfoFile, "%s%d", SETTLEMENT_INFO_FILE, server);
	sprintf(serverSpec->files.settingFile, "%s%d", SETTING_FILE, server);
	sprintf(serverSpec->files.versionFile, "%s%d", VERSIONS_FILE, server);
	sprintf(serverSpec->files.binFile, "%s%d", BIN_FILE, server);
	sprintf(serverSpec->files.configXml, "%d%s", server, CONFIG_XML);
	sprintf(serverSpec->files.logonInfoFile, "%s%d", LOGON_INFO_FILE, server);
	sprintf(serverSpec->files.generalKey, "%s%d", GENERAL_KEY_FILE, server);
	sprintf(serverSpec->files.versionKeyFile, "%s%d", VERSION_KEY_FILE, server);
	sprintf(serverSpec->files.kcvFile, "%s%d", KCV_FILE, server);
	sprintf(serverSpec->files.voucherKeyFile, "%s%d", VOUCHER_KEY_FILE, server);
	sprintf(serverSpec->files.ETCInfo, "%s%d", ETC_INFO_FILE, server);
	sprintf(serverSpec->files.ETCTemp, "%s%d", UNKNOWN_ETC_INFO_FILE, server);
	sprintf(serverSpec->files.loanInfo, "%s%d", LOAN_INFO_FILE, server);
	sprintf(serverSpec->files.loanTemp, "%s%d", UNKNOWN_LOAN_INFO_FILE, server);
	sprintf(serverSpec->files.rollTemp, "%s%d", ROLL_INFO_FILE, server);
	sprintf(serverSpec->files.chargeInfo, "%s%d", CHARGE_INFO_FILE, server);
	sprintf(serverSpec->files.pcPosFiles, "%s%d", PCPOS_INFO_FILE, server);//HNO_TCP
}


/**
* define and menu Items in Screen.
* @param serverSpec
*/
void defineMenuItems(serverSpecST* serverSpec)
{
	serverSpecST*   serverSpec0 = serverSpec;
	serverSpecST*   serverSpec1 = serverSpec0->next;
	serverSpecST*   serverSpec2 = serverSpec1->next;
	uint8*          connNumber = (uint8*)calloc(6, sizeof(uint8));
	uint8*          keyMenu = (uint8*)malloc(sizeof(uint8));
	uint8*          cardMenu = (uint8*)malloc(sizeof(uint8));
	uint8*          barCode = (uint8*)malloc(sizeof(uint8));
	uint8*          manually = (uint8*)malloc(sizeof(uint8));
	uint8*          onlineCharge = (uint8*)malloc(sizeof(uint8));
	uint8*          offlineCharge = (uint8*)malloc(sizeof(uint8));
	messageSpecST*	messageSpec = NULL;
	terminalSpecST  terminalCapability = getTerminalCapability();

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "defineMenuItems");

	connNumber[0] = 1;
	connNumber[1] = 2;
	connNumber[2] = 3;
	connNumber[3] = 4;
	connNumber[4] = 5;
	connNumber[5] = 6;

	//barcode reader parameters, billPay transaction
	*barCode = TRUE;
	*manually = FALSE;

	*keyMenu = TRUE;
	*cardMenu = FALSE;

	*onlineCharge = TRUE;
	*offlineCharge = FALSE;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Menu Items...");

	//menuIndex,                                                		//parentMenuIndex,               				//language   //timeOut      //align        //color   //visible    //checkPass//title                   //size    ,index,(*function)(argumentListST* arguments),argumentCount.
	/************************************ MENU KEY F1 (Main Menu) ************************************/

	setMenuItem(MENU_KEY_ROOT, 0, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ����", NORMAL_STRING, 1, NULL, 0);

	setMenuItem(MENU_ITEM_USER, MENU_KEY_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_MERCHANT, MENU_KEY_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, TRUE, "���Ԑ��", NORMAL_STRING, 1, checkMerchantMenuPassword, NULL);//forooshgah
	setMenuItem(MENU_SUPERVISOR, MENU_KEY_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, TRUE, "��������", NORMAL_STRING, 1, checkSupervisorMenuPassword, NULL);//poshtibani

	setMenuItem(MENU_ITEM_BUY, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, buyTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);//kharid
	setMenuItem(MENU_ITEM_BALANCE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, balanceTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);//mojoodi
	setMenuItem(MENU_ITEM_BILL_PAY, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), manually, messageSpec, keyMenu);//pardakhte ghabz
	setMenuItem(MENU_ITEM_BUY_CHARGE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��ю ʘ�", NORMAL_STRING, 1, NULL, 0);//kharide charge
	setMenuItem(MENU_ITEM_BUY_MULTI_CHARGE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��ю ������", NORMAL_STRING, 1, NULL, 0);//kharide charge
	//setMenuItem(MENU_ITEM_BUY_TOPUP_CHARGE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_ITEM_BUY_TOPUP_CHARGE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю ���", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu, NULL, TRUE);

	setMenuItem(MENU_ITEM_CHARITY, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_ITEM_LOAN_SERVICE, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_ITEM_ETC, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�э�� �����", NORMAL_STRING, 1, NULL, 0);


	setMenuItem(MENU_BEHZISTI_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, behzistiCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_MAHAK_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�͘", NORMAL_STRING, 1, mahakCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_KAHRIZAK_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����Ҙ", NORMAL_STRING, 1, kahrizakCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_KOMITEH_EMDAD_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �����", NORMAL_STRING, 1, komitehEmdadCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_BONYADE_MASKANE_ENGHELAB_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�Ә� ������", NORMAL_STRING, 1, bonyadeMaskaneEnghelabCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_ASHRAFOLANBIA_CHARITY, MENU_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��������", NORMAL_STRING, 1, ashrafolAnbiaCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
	//    setMenuItem (MENU_HEMMATE_JAVANAN_CHARITY                 		,MENU_ITEM_CHARITY                  			,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "��� ������"             	,NORMAL_STRING, 1, hemmateJavananCharity, 3, &(serverSpec0->files), messageSpec, keyMenu);
#ifndef INGENICO					                                                                                                                            
	if (terminalCapability.pcPosCapability)
		setMenuItem(MENU_PC_POS, MENU_ITEM_USER, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �� ������", NORMAL_STRING, 1, PCPOS, 1, &(serverSpec0->files));
#endif					                                                                                                                                                                         	
	setMenuItem(MENU_ITEM_LOAN_PAYMENT, MENU_ITEM_LOAN_SERVICE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, loanPayTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_ITEM_TRACKING_LOAN, MENU_ITEM_LOAN_SERVICE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, LoanPayTrackingTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);

	setMenuItem(MENU_ITEM_BUY_ETC, MENU_ITEM_ETC, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ������", NORMAL_STRING, 1, ETCTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);
	setMenuItem(MENU_ITEM_TRACKING_ETC, MENU_ITEM_ETC, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, ETCTrackingTrans, 3, &(serverSpec0->files), messageSpec, keyMenu);

	//    setMenuItem (MENU_BILL_PAY_BARCODE                        		,MENU_ITEM_BILL_PAY                 			,FARSI,   10,   ALIGN_CENTER, 0, FALSE,  FALSE, "��ј�����"              	,NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), barCode, messageSpec, keyMenu);
	//    setMenuItem (MENU_BILL_PAY_MANUALLY                       		,MENU_ITEM_BILL_PAY                 			,FARSI,   10,   ALIGN_CENTER, 0, FALSE,  FALSE, "���� ����"              	,NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), manually, messageSpec, keyMenu);

	/****************************SINGLE CHARGE*************************************************/
	setMenuItem(MENU_IRANCELL, MENU_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);//charge taki Irancell
	setMenuItem(MENU_MCI, MENU_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);//charge taki MCI
	setMenuItem(MENU_RIGHTEL, MENU_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);//charge taki rightel

	//    setMenuItem (MENU_IRANCELL_10000                      ,MENU_IRANCELL                      ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "10000 �����"                 ,    NORMAL_STRING, 1, buyMultiChargeIrancell10000, 5, &(serverSpec0->files), messageSpec, keyMenu , onlineCharge, TRUE);//charge Irancell 10000
	setMenuItem(MENU_IRANCELL_20000, MENU_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge Irancell 20000
	setMenuItem(MENU_IRANCELL_50000, MENU_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge Irancell 50000
	setMenuItem(MENU_IRANCELL_100000, MENU_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge Irancell 100000
	setMenuItem(MENU_IRANCELL_200000, MENU_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge Irancell 200000
	//    setMenuItem (MENU_IRANCELL_500000                     ,MENU_IRANCELL                      ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "500000 �����"                ,    NORMAL_STRING, 1, buyMultiChargeIrancell500000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge Irancell 500000

	setMenuItem(MENU_MCI_10000, MENU_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "10000 �����", NORMAL_STRING, 1, buyMultiChargeMCI10000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);// chargeBuyTrans, 3, &(serverSpec0->files), messageSpec, keyMenu );//charge taki MCI 10000
	setMenuItem(MENU_MCI_20000, MENU_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeMCI20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki MCI 20000
	setMenuItem(MENU_MCI_50000, MENU_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeMCI50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki MCI 50000
	setMenuItem(MENU_MCI_100000, MENU_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeMCI100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki MCI 100000
	setMenuItem(MENU_MCI_200000, MENU_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeMCI200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki MCI 200000
	//    setMenuItem (MENU_MULTI_MCI_500000                          ,MENU_MCI                           ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "500000 �����"                ,    NORMAL_STRING, 1, buyMultiChargeMCI500000, 5, &(serverSpec0->files), messageSpec, keyMenu , onlineCharge, TRUE);//charge taki MCI 500000

	setMenuItem(MENU_RIGHTEL_20000, MENU_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeRightel20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki rightel 20000
	setMenuItem(MENU_RIGHTEL_50000, MENU_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeRightel50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki rightel 50000
	setMenuItem(MENU_RIGHTEL_100000, MENU_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeRightel100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki rightel 100000
	setMenuItem(MENU_RIGHTEL_200000, MENU_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeRightel200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki rightel 200000
	setMenuItem(MENU_RIGHTEL_500000, MENU_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "500000 �����", NORMAL_STRING, 1, buyMultiChargeRightel500000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, TRUE);//charge taki rightel 500000

	/****************************MULTI CHARGE*************************************************/

	setMenuItem(MENU_MULTI_IRANCELL, MENU_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);//charge taki Irancell
	setMenuItem(MENU_MULTI_MCI, MENU_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);//charge taki MCI
	setMenuItem(MENU_MULTI_RIGHTEL, MENU_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);//charge taki rightel

//	setMenuItem(MENU_ITEM_BUY_TOPUP_MCI, MENU_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);//charge aani hamrahe aval
//	setMenuItem(MENU_ITEM_BUY_TOPUP_IRANCELL, MENU_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);//charge aani Irancell
//	setMenuItem(MENU_ITEM_BUY_TOPUP_RIGHTEL, MENU_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);//charge aani rightel

	//    setMenuItem (MENU_MULTI_IRANCELL_10000                      ,MENU_MULTI_IRANCELL                      ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "10000 �����"                 ,    NORMAL_STRING, 1, buyMultiChargeIrancell10000, 5, &(serverSpec0->files), messageSpec, keyMenu , onlineCharge, FALSE);//charge Irancell 10000
	setMenuItem(MENU_MULTI_IRANCELL_20000, MENU_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge Irancell 20000
	setMenuItem(MENU_MULTI_IRANCELL_50000, MENU_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge Irancell 50000
	setMenuItem(MENU_MULTI_IRANCELL_100000, MENU_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge Irancell 100000
	setMenuItem(MENU_MULTI_IRANCELL_200000, MENU_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge Irancell 200000
	//    setMenuItem (MENU_MULTI_IRANCELL_500000                     ,MENU_MULTI_IRANCELL                      ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "500000 �����"                ,    NORMAL_STRING, 1, buyMultiChargeIrancell500000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge Irancell 500000

	setMenuItem(MENU_MULTI_MCI_10000, MENU_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "10000 �����", NORMAL_STRING, 1, buyMultiChargeMCI10000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);// chargeBuyTrans, 3, &(serverSpec0->files), messageSpec, keyMenu );//charge taki MCI 10000
	setMenuItem(MENU_MULTI_MCI_20000, MENU_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeMCI20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki MCI 20000
	setMenuItem(MENU_MULTI_MCI_50000, MENU_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeMCI50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki MCI 50000
	setMenuItem(MENU_MULTI_MCI_100000, MENU_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeMCI100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki MCI 100000
	setMenuItem(MENU_MULTI_MCI_200000, MENU_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeMCI200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki MCI 200000
	//    setMenuItem (MENU_MULTI_MCI_500000                          ,MENU_MULTI_MCI                           ,FARSI,     10,        ALIGN_CENTER,        0,       TRUE,        FALSE, "500000 �����"                ,    NORMAL_STRING, 1, buyMultiChargeMCI500000, 5, &(serverSpec0->files), messageSpec, keyMenu , onlineCharge, FALSE);//charge taki MCI 500000

	setMenuItem(MENU_MULTI_RIGHTEL_20000, MENU_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeRightel20000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki rightel 20000
	setMenuItem(MENU_MULTI_RIGHTEL_50000, MENU_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeRightel50000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki rightel 50000
	setMenuItem(MENU_MULTI_RIGHTEL_100000, MENU_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeRightel100000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki rightel 100000
	setMenuItem(MENU_MULTI_RIGHTEL_200000, MENU_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeRightel200000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki rightel 200000
	setMenuItem(MENU_MULTI_RIGHTEL_500000, MENU_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "500000 �����", NORMAL_STRING, 1, buyMultiChargeRightel500000, 5, &(serverSpec0->files), messageSpec, keyMenu, onlineCharge, FALSE);//charge taki rightel 500000

	///****************************TOPUP CHARGE*************************************************/                                          	 					                                                                                                                                            
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_10000                  		,MENU_ITEM_BUY_TOPUP_MCI           		  		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MCI_10000, FALSE);  
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_20000                  		,MENU_ITEM_BUY_TOPUP_MCI           		  		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "20000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MCI_20000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_50000                  		,MENU_ITEM_BUY_TOPUP_MCI           		  		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "50000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MCI_50000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_100000                 		,MENU_ITEM_BUY_TOPUP_MCI           		  		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "100000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MCI_100000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_200000                 		,MENU_ITEM_BUY_TOPUP_MCI           		  		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "200000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MCI_200000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_MCI_OTHER_AMOUNT                   ,MENU_ITEM_BUY_TOPUP_MCI           		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "����� ���"                 ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , NULL, TRUE);
	//		                                                                                                                                                                                            
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_10000             		,MENU_ITEM_BUY_TOPUP_IRANCELL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MTN_10000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_20000             		,MENU_ITEM_BUY_TOPUP_IRANCELL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "20000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MTN_20000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_50000             		,MENU_ITEM_BUY_TOPUP_IRANCELL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "50000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MTN_50000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_100000            		,MENU_ITEM_BUY_TOPUP_IRANCELL           		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "100000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MTN_100000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_200000            		,MENU_ITEM_BUY_TOPUP_IRANCELL           		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "200000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , MTN_200000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_IRANCELL_OTHER_AMOUNT      		,MENU_ITEM_BUY_TOPUP_IRANCELL           		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "����� ���"                 ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , NULL, TRUE);

	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_10000                  		,MENU_ITEM_BUY_TOPUP_RIGHTEL          		  	,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_20000, FALSE);  
	//    
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_20000                      ,MENU_ITEM_BUY_TOPUP_RIGHTEL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "20000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_20000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_50000                      ,MENU_ITEM_BUY_TOPUP_RIGHTEL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "50000 �����"                ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_50000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_100000                     ,MENU_ITEM_BUY_TOPUP_RIGHTEL                    ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "100000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_100000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_200000                     ,MENU_ITEM_BUY_TOPUP_RIGHTEL                    ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "200000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_200000, FALSE);
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_500000             		,MENU_ITEM_BUY_TOPUP_RIGHTEL            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "500000 �����"               ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , RTL_500000, FALSE); 
	//    setMenuItem (MENU_ITEM_BUY_TOPUP_RIGHTEL_OTHER_AMOUNT               ,MENU_ITEM_BUY_TOPUP_RIGHTEL                    ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "����� ���"                 ,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, keyMenu , NULL, TRUE);


	setMenuItem(MENU_MERCHANT_SHIFT, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, checkShiftServiceAccessibility, NULL);
	setMenuItem(MENU_MERCHANT_REPORTS, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, NULL);
	setMenuItem(MENU_MERCHANT_CHANGE_PASS, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, changeMerchantMenuPassword, NULL);
	setMenuItem(MENU_MERCHANT_SETTING, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, userSetting, NULL);
	setMenuItem(MENU_MERCHANT_SERVICE_SETTING, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� �����", NORMAL_STRING, 1, setupMerchantServices, 0);//MRF_971207
	if (terminalCapability.GPRSCapability)
		setMenuItem(MENU_MERCHANT_TURN_OFF, MENU_MERCHANT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ����", NORMAL_STRING, 1, turnOffTerminal, NULL);//#MRF_971030

	setMenuItem(MENU_MERCHANT_SHIFTMANAGE, MENU_MERCHANT_SHIFT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ����", NORMAL_STRING, 1, checkAdminAccessibility, NULL);
	setMenuItem(MENU_USERS_OPEN, MENU_MERCHANT_SHIFT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��Ҙ��� ����", NORMAL_STRING, 1, activateUser, 2, &(serverSpec0->files), FALSE);
	setMenuItem(MENU_USERS_CLOSE, MENU_MERCHANT_SHIFT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ����", NORMAL_STRING, 1, deactivateUser, 2, &(serverSpec0->files), TRUE);
	//	setMenuItem(MENU_USERS_REPORT										,MENU_MERCHANT_SHIFT							,FARSI,   10, 	ALIGN_CENTER, 0, TRUE,  FALSE, "����� ����"					,NORMAL_STRING, 1, logUsersReport, 0);

	setMenuItem(MENU_SHIFTMANAGE_CREATE_USER, MENU_MERCHANT_SHIFTMANAGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �����", NORMAL_STRING, 1, addUser, NULL);
	setMenuItem(MENU_SHIFTMANAGE_DELETE_USER, MENU_MERCHANT_SHIFTMANAGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� �����", NORMAL_STRING, 1, deleteUser, NULL);
	setMenuItem(MENU_SHIFTMANAGE_REPORTS_USERS_LIST, MENU_MERCHANT_SHIFTMANAGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� �������", NORMAL_STRING, 1, printUsersList, NULL);
	setMenuItem(MENU_SHIFTMANAGE_RESET_PASS, MENU_MERCHANT_SHIFTMANAGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��� �����", NORMAL_STRING, 1, resetUserMenuPassword, NULL);

	//****************************************************************************************************************************************************************************************************************************************************************************************************************************  
	setMenuItem(MENU_REPORT_CASH, MENU_MERCHANT_REPORTS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_REPRINT_TRANSACTION, MENU_MERCHANT_REPORTS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�ǁ ���� ����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_REPORT_LIST_TRANSACTION, MENU_MERCHANT_REPORTS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ��ǘ�� ��", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_REPORT_HARDWARE, MENU_MERCHANT_REPORTS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ��� �����", NORMAL_STRING, 1, hardwareReport, NULL);
	setMenuItem(MENU_REPORT_INITIALIZE_TRANS, MENU_MERCHANT_REPORTS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �������", NORMAL_STRING, 1, transactionInitializeList, 1, &(serverSpec0->files));

	setMenuItem(MENU_REPORT_CASH_BUY, MENU_REPORT_CASH, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, merchantBoxReport, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_CASH_CHARGE, MENU_REPORT_CASH, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю", NORMAL_STRING, 1, merchantChargeReport, 1, &(serverSpec0->files));
	setMenuItem (MENU_REPORT_CASH_TOPUP                         		,MENU_REPORT_CASH                       		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "���� ��ю ���"           ,NORMAL_STRING, 1, merchantTopupReport, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_CASH_CHARITY, MENU_REPORT_CASH, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, merchantCharityReport, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_CASH_LOANPAY, MENU_REPORT_CASH, FARSI, 10, ALIGN_CENTER, 0, FALSE, FALSE, "������ ���", NORMAL_STRING, 1, merchantLOANReport, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_CASH_ETC, MENU_REPORT_CASH, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�э�� �����", NORMAL_STRING, 1, merchantETCReport, 1, &(serverSpec0->files));

	setMenuItem(MENU_REPRINT_TRANSACTION_BUY, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, reprintBuyTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPRINT_TRANSACTION_BILL, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, reprintBillTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPRINT_TRANSACTION_BUY_CHARGE, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю", NORMAL_STRING, 1, reprintChargeTransaction, 1, &(serverSpec0->files));
    setMenuItem (MENU_REPRINT_TRANSACTION_TOPUP                 		,MENU_REPRINT_TRANSACTION               		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "���� ��ю ���"           ,NORMAL_STRING, 1, reprintTopupTransaction, 1, &(serverSpec0->files ));
	setMenuItem(MENU_REPRINT_TRANSACTION_CHARITY, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, reprintCharityTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPRINT_TRANSACTION_LOAN_PAY, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, reprintLoanPayTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPRINT_TRANSACTION_ETC, MENU_REPRINT_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�э�� �����", NORMAL_STRING, 1, reprintETCTransaction, 1, &(serverSpec0->files));

	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS, MENU_REPORT_LIST_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_UNSUCCESS, MENU_REPORT_LIST_TRANSACTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, NULL, 0);

	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_BUY, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, transactionBuyList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_BILL, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, transactionBillPayList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARGE, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю", NORMAL_STRING, 1, NULL, 0);
    setMenuItem (MENU_REPORT_LIST_TRANSACTION_SUCCESS_TOPUP     		,MENU_REPORT_LIST_TRANSACTION_SUCCESS   		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "���� ��ю ���"           ,NORMAL_STRING, 1, transactionTopupList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARITY, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, transactionCharityList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_LOAN_PAY, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, transactionLoanPayList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_SUCCESS_ETC, MENU_REPORT_LIST_TRANSACTION_SUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�э�� �����", NORMAL_STRING, 1, transactionETCList, 1, &(serverSpec0->files));

	setMenuItem(MENU_REPORT_LIST_TRANSACTION_CHARGE_ABSTRACT, MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� �������", NORMAL_STRING, 1, transactionChargeList, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_CHARGE_DETAIL, MENU_REPORT_LIST_TRANSACTION_SUCCESS_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���� ��", NORMAL_STRING, 1, completeChargeReport, 1, &(serverSpec0->files));

	setMenuItem(MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_BUY, MENU_REPORT_LIST_TRANSACTION_UNSUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, unsuccessBuyTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_BILL_PAY, MENU_REPORT_LIST_TRANSACTION_UNSUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, unsuccessBillPayTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_CHARGE, MENU_REPORT_LIST_TRANSACTION_UNSUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю", NORMAL_STRING, 1, unsuccessChargeTransaction, 1, &(serverSpec0->files));
	setMenuItem(MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_CHARITY, MENU_REPORT_LIST_TRANSACTION_UNSUCCESS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, unsuccessCharityTransaction, 1, &(serverSpec0->files));
	setMenuItem (MENU_REPORT_LIST_TRANSACTION_UNSUCCESS_TOPUP   		,MENU_REPORT_LIST_TRANSACTION_UNSUCCESS 		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "���� ��ю ���"           ,NORMAL_STRING, 1, unsuccessTopupTransaction , 1, &(serverSpec0->files));	

	//    setMenuItem (MENU_LAUNCH                                    		,MENU_SUPERVISOR                        		,FARSI,   10,   ALIGN_CENTER, 0, FALSE, FALSE, "��� ������"                 ,NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_SETTING, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONFIGURATION, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, initializeTrans, 1, &(serverSpec0->files));
	if (terminalCapability.TMSCapability)
		setMenuItem(MENU_TMS, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_REPORT, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_RESET_MERCHANT_PASS, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��� ���Ԑ��", NORMAL_STRING, 1, resetMerchantMenuPasswordMenu, 0);
#ifndef INGENICO//+MRF_970829				                                                                                      
	if (terminalCapability.GPRSCapability)
		setMenuItem(MENU_LOCK_SIM_CARD, MENU_SUPERVISOR, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� PIN", NORMAL_STRING, 1, PINLock, 0);
#endif		                                                                                                                                                                                        

	setMenuItem(MENU_BANKING_SETTING, MENU_SETTING, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� ����", NORMAL_STRING, 1, setupGeneralConfig, 1, &(serverSpec0->files));
	setMenuItem(MENU_SETTING_CONNECTION, MENU_SETTING, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� ������", NORMAL_STRING, 1, checkBankConnNumberVisibility, 1, &(serverSpec0->files));
	setMenuItem(MENU_SETTING_SERVICES, MENU_SETTING, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� �����", NORMAL_STRING, 1, setupServices, 1, &(serverSpec0->files));
	setMenuItem(MENU_PRINT_KEYS, MENU_SETTING, FARSI, 10, ALIGN_CENTER, 0, FALSE, FALSE, "�ǁ ������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_SUPERVISOR_CHANGE_PASS, MENU_SETTING, FARSI, 10, ALIGN_CENTER, 0, FALSE, FALSE, "����� ��� �������", NORMAL_STRING, 1, changeSupervisorMenuPassword, NULL);

	setMenuItem(MENU_POS_CONNECTION, MENU_SETTING_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ����", NORMAL_STRING, 1, NULL, 0);
	if (terminalCapability.pcPosCapability)//+MRF_970906				                                                                                                                            
		setMenuItem(MENUE_PCPOS_CONNECTION, MENU_SETTING_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ �� ������", NORMAL_STRING, 1, setupPcPosConnection, 1, &(serverSpec0->files));

	setMenuItem(MENU_CONNECTION1, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONNECTION2, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONNECTION3, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONNECTION4, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ �����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONNECTION5, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ����", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CONNECTION6, MENU_POS_CONNECTION, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, NULL, 0);

	setMenuItem(MENU_CONN1_TYPE, MENU_CONNECTION1, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[0], &(serverSpec0->files));
	setMenuItem(MENU_CONN1_PARAM, MENU_CONNECTION1, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[0], &(serverSpec0->files));

	setMenuItem(MENU_CONN2_TYPE, MENU_CONNECTION2, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[1], &(serverSpec0->files));
	setMenuItem(MENU_CONN2_PARAM, MENU_CONNECTION2, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[1], &(serverSpec0->files));

	setMenuItem(MENU_CONN3_TYPE, MENU_CONNECTION3, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[2], &(serverSpec0->files));
	setMenuItem(MENU_CONN3_PARAM, MENU_CONNECTION3, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[2], &(serverSpec0->files));

	setMenuItem(MENU_CONN4_TYPE, MENU_CONNECTION4, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[3], &(serverSpec0->files));
	setMenuItem(MENU_CONN4_PARAM, MENU_CONNECTION4, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[3], &(serverSpec0->files));

	setMenuItem(MENU_CONN5_TYPE, MENU_CONNECTION5, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[4], &(serverSpec0->files));
	setMenuItem(MENU_CONN5_PARAM, MENU_CONNECTION5, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[4], &(serverSpec0->files));

	setMenuItem(MENU_CONN6_TYPE, MENU_CONNECTION6, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionType, 2, &connNumber[5], &(serverSpec0->files));
	setMenuItem(MENU_CONN6_PARAM, MENU_CONNECTION6, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnection, 2, &connNumber[5], &(serverSpec0->files));

	if (terminalCapability.TMSCapability)
	{
		setMenuItem(MENU_TMS_GET_SETTING, MENU_TMS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ �������", NORMAL_STRING, 1, TMSConfigManually, 1, &(serverSpec0->files));
		setMenuItem(MENU_TMS_UPDATE, MENU_TMS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�� ��������", NORMAL_STRING, 1, TMSUpdateManually, 1, &(serverSpec0->files));
		setMenuItem(MENU_TMS_SETTING, MENU_TMS, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� TMS", NORMAL_STRING, 1, NULL, 0);

		setMenuItem(MENU_TMS_TYPE, MENU_TMS_SETTING, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ������", NORMAL_STRING, 1, setupConnectionTypeTMS, NULL);
		setMenuItem(MENU_TMS_PARAM, MENU_TMS_SETTING, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, setupConnectionTMS, NULL);
	}

	setMenuItem(MENU_TRANSACTION_ERROR_LIST, MENU_REPORT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ����", NORMAL_STRING, 1, transactionsErrorList, 1, &(serverSpec0->files));
	setMenuItem(MENU_SYSTEM_ERROR_LIST, MENU_REPORT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ������", NORMAL_STRING, 1, systemErrorList, 1, &(serverSpec0->files));
	if (terminalCapability.TMSCapability)
		setMenuItem(MENU_REPORT_TMS, MENU_REPORT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� TMS", NORMAL_STRING, 1, TMSReport, NULL);
	setMenuItem(MENU_REPORT_SUPERVISOR_ACTIVITIES, MENU_REPORT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ �������", NORMAL_STRING, 1, transactionSupervisorList, 1, &(serverSpec0->files));
	setMenuItem(MENU_SETTING_REPORT, MENU_REPORT, FARSI, 10, ALIGN_CENTER, 0, FALSE, FALSE, "����� �������", NORMAL_STRING, 1, settingReport, 1, &(serverSpec0->files));

	/************************************ MENU F2 ************************************/
	setMenuItem(MENU_MANAGEMENT_ROOT, 0, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ������", NORMAL_STRING, 1, NULL, 0);

	setMenuItem(MENU_LOG, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������� �ǐ", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_SET_DATE_TIME, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ����", NORMAL_STRING, 1, setUserDateAndTime, NULL);
	setMenuItem(MENU_PRINT_SUPERVISEOR_PASS, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�.�", NORMAL_STRING, 1, printSupervisorPass, NULL);
	setMenuItem(MENU_SET_SERIAL_NUM, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �����", NORMAL_STRING, 1, NULL, 0);//serial number
#ifndef INGENICO                                                                                                                                                             	
	if (terminalCapability.GPRSCapability)//+MRF_970907		                                                                                                                                     	
		setMenuItem(MENU_PIN_UNLOCK, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��Ҙ��� PIN", NORMAL_STRING, 1, PINUnLock, NULL);//+MRF_970830
#endif						                                                                                                                                                                     	
	setMenuItem(MENU_DELETE_FILSE, MENU_MANAGEMENT_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ���� ��", NORMAL_STRING, 1, deleteFiles, 1, &(serverSpec0->files));

	setMenuItem(MENU_LOG_TYPE, MENU_LOG, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� �ǐ", NORMAL_STRING, 1, setLogType, NULL);
	setMenuItem(MENU_LOG_PORT, MENU_LOG, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ����", NORMAL_STRING, 1, setLogPort, NULL);

	setMenuItem(MENU_SET_COMFIRM_SERIAL, MENU_SET_SERIAL_NUM, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��� ����� ����", NORMAL_STRING, 1, setUserSerialNumber, NULL);
	setMenuItem(MENU_SET_RESET_SERIAL, MENU_SET_SERIAL_NUM, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� �����", NORMAL_STRING, 1, resetSerialNumber, NULL);

	/************************************ MENU CARD ************************************/

	setMenuItem(MENU_CARD_ROOT, 0, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ����", NORMAL_STRING, 1, NULL, 0);

	setMenuItem(MENU_CARD_ITEM_BUY, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����", NORMAL_STRING, 1, buyTrans, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_BALANCE, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, balanceTrans, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_BILL_PAY, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), manually, messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_BUY_CHARGE, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��ю ʘ�", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_ITEM_BUY_MULTI_CHARGE, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��ю ������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_CHARGE, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��ю ���", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, NULL, TRUE);
	setMenuItem(MENU_CARD_ITEM_CHARITY, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_ITEM_LOAN_PAYMENT, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "������ ���", NORMAL_STRING, 1, loanPayTrans, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_ETC, MENU_CARD_ROOT, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�э�� �����", NORMAL_STRING, 1, ETCTrans, 3, &(serverSpec0->files), messageSpec, cardMenu);

	//    setMenuItem (MENU_CARD_BILL_PAY_BARCODE                   		,MENU_CARD_ITEM_BILL_PAY            			,FARSI,   10,   ALIGN_CENTER, 0, FALSE, FALSE, "��ј�����"              	,NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), barCode, messageSpec, cardMenu);
	//    setMenuItem (MENU_CARD_BILL_PAY_MANUALLY                  		,MENU_CARD_ITEM_BILL_PAY            			,FARSI,   10,   ALIGN_CENTER, 0, FALSE, FALSE, "���� ����"              	,NORMAL_STRING, 1, billPayTrans, 4, &(serverSpec0->files), manually, messageSpec, cardMenu);
	/****************************SINGLE CHARGE*************************************************/
	setMenuItem(MENU_CARD_IRANCELL, MENU_CARD_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_MCI, MENU_CARD_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_RIGHTEL, MENU_CARD_ITEM_BUY_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);

	//    setMenuItem (MENU_CARD_IRANCELL_10000               				,MENU_CARD_IRANCELL                      		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"                ,NORMAL_STRING, 1, buyMultiChargeIrancell10000, 5,  &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_IRANCELL_20000, MENU_CARD_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_IRANCELL_50000, MENU_CARD_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_IRANCELL_100000, MENU_CARD_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_IRANCELL_200000, MENU_CARD_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	//    setMenuItem (MENU_CARD_IRANCELL_500000              				,MENU_CARD_IRANCELL                      		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "500000 �����"               ,NORMAL_STRING, 1, buyMultiChargeIrancell500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);

	setMenuItem(MENU_CARD_MCI_10000, MENU_CARD_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "10000 �����", NORMAL_STRING, 1, buyMultiChargeMCI10000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_MCI_20000, MENU_CARD_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeMCI20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_MCI_50000, MENU_CARD_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeMCI50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_MCI_100000, MENU_CARD_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeMCI100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_MCI_200000, MENU_CARD_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeMCI200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	//    setMenuItem (MENU_CARD_MCI_500000                   				,MENU_CARD_MCI                           		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "500000 �����"               ,NORMAL_STRING, 1, buyMultiChargeMCI500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);

	setMenuItem(MENU_CARD_RIGHTEL_20000, MENU_CARD_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeRightel20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_RIGHTEL_50000, MENU_CARD_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeRightel50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_RIGHTEL_100000, MENU_CARD_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeRightel100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_RIGHTEL_200000, MENU_CARD_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeRightel200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);
	setMenuItem(MENU_CARD_RIGHTEL_500000, MENU_CARD_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "500000 �����", NORMAL_STRING, 1, buyMultiChargeRightel500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, TRUE);

	/****************************MULTI CHARGE*************************************************/

	setMenuItem(MENU_CARD_MULTI_IRANCELL, MENU_CARD_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_MULTI_MCI, MENU_CARD_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);
	setMenuItem(MENU_CARD_MULTI_RIGHTEL, MENU_CARD_ITEM_BUY_MULTI_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);

	//    setMenuItem (MENU_CARD_MULTI_IRANCELL_10000               		,MENU_CARD_MULTI_IRANCELL                       ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"                ,NORMAL_STRING, 1, buyMultiChargeIrancell10000, 5,  &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_IRANCELL_20000, MENU_CARD_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_IRANCELL_50000, MENU_CARD_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_IRANCELL_100000, MENU_CARD_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_IRANCELL_200000, MENU_CARD_MULTI_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeIrancell200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	//    setMenuItem (MENU_CARD_MULTI_IRANCELL_500000              		,MENU_CARD_MULTI_IRANCELL                       ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "200000 �����"               ,NORMAL_STRING, 1, buyMultiChargeIrancell500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);


	setMenuItem(MENU_CARD_MULTI_MCI_10000, MENU_CARD_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "10000 �����", NORMAL_STRING, 1, buyMultiChargeMCI10000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_MCI_20000, MENU_CARD_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeMCI20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_MCI_50000, MENU_CARD_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeMCI50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_MCI_100000, MENU_CARD_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeMCI100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_MCI_200000, MENU_CARD_MULTI_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeMCI200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	//    setMenuItem (MENU_CARD_MULTI_MCI_500000                   		,MENU_CARD_MULTI_MCI                            ,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "200000 �����"               ,NORMAL_STRING, 1, buyMultiChargeMCI500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);

	setMenuItem(MENU_CARD_MULTI_RIGHTEL_20000, MENU_CARD_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, buyMultiChargeRightel20000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_RIGHTEL_50000, MENU_CARD_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, buyMultiChargeRightel50000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_RIGHTEL_100000, MENU_CARD_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, buyMultiChargeRightel100000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_RIGHTEL_200000, MENU_CARD_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, buyMultiChargeRightel200000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);
	setMenuItem(MENU_CARD_MULTI_RIGHTEL_500000, MENU_CARD_MULTI_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "500000 �����", NORMAL_STRING, 1, buyMultiChargeRightel500000, 5, &(serverSpec0->files), messageSpec, cardMenu, onlineCharge, FALSE);

	setMenuItem(MENU_CARD_ITEM_BEHZISTI_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "��������", NORMAL_STRING, 1, behzistiCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_MAHAK_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�͘", NORMAL_STRING, 1, mahakCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_KAHRIZAK_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����Ҙ", NORMAL_STRING, 1, kahrizakCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_KOMITEH_EMDAD_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� �����", NORMAL_STRING, 1, komitehEmdadCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_BONYADE_MASKANE_ENGHELAB_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�Ә� ������", NORMAL_STRING, 1, bonyadeMaskaneEnghelabCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	setMenuItem(MENU_CARD_ITEM_ASHRAFOLANBIA_CHARITY, MENU_CARD_ITEM_CHARITY, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "���� ��������", NORMAL_STRING, 1, ashrafolAnbiaCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	//    setMenuItem (MENU_CARD_ITEM_HEMMATE_JAVANAN_CHARITY               ,MENU_CARD_ITEM_CHARITY                     	,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "��� ������"             	,NORMAL_STRING, 1, hemmateJavananCharity, 3, &(serverSpec0->files), messageSpec, cardMenu);
	/****************************TOPUP CHARGE*************************************************/

	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI, MENU_CARD_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, NULL, 0);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, MENU_CARD_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�������", NORMAL_STRING, 1, NULL, 0);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, MENU_CARD_ITEM_BUY_TOPUP_CHARGE, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "�����", NORMAL_STRING, 1, NULL, 0);

	////    setMenuItem (MENU_CARDTOPUP_ITEM_BUY_TOPUP_MCI_10000             		,MENU_CARD_ITEM_BUY_TOPUP_MCI            		,FARSI,   10,   ALIGN_CENTER, 0, TRUE,  FALSE, "10000 �����"            	,NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_10000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI_20000, MENU_CARD_ITEM_BUY_TOPUP_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_20000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI_50000, MENU_CARD_ITEM_BUY_TOPUP_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_50000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI_100000, MENU_CARD_ITEM_BUY_TOPUP_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_100000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI_200000, MENU_CARD_ITEM_BUY_TOPUP_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_200000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_MCI_OTHER_AMOUNT, MENU_CARD_ITEM_BUY_TOPUP_MCI, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MCI_OTHER, TRUE);//++ABS_980728	

	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_10000, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "10000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_10000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_20000, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_20000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_50000, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_50000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_100000, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_100000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_200000, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_200000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_IRANCELL_OTHER_AMOUNT, MENU_CARD_ITEM_BUY_TOPUP_IRANCELL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, MTN_OTHER, TRUE);//++ABS_980728	

	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_20000, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "20000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_20000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_50000, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "50000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_50000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_100000, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "100000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_100000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_200000, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "200000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_200000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_500000, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "500000 �����", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_500000, FALSE);
	//setMenuItem(MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL_OTHER_AMOUNT, MENU_CARD_ITEM_BUY_TOPUP_RIGHTEL, FARSI, 10, ALIGN_CENTER, 0, TRUE, FALSE, "����� ���", NORMAL_STRING, 1, setTopupFields, 5, &(serverSpec0->files), messageSpec, cardMenu, RTL_OTHER, TRUE);//++ABS_980728	


}

/*
* Add Item in tree menu.
* @param   menuItemList [input]: menu item list
* @param   menuItemNameEN [input]: parent node
* @param   menuItemNameEN [input]: child node
* @return  None.
*/
void addMenuItemList(menuItemST** menuItemList, enum menuItemNameEN parent, enum menuItemNameEN child)
{
	menuItemST**    menuPointer = menuItemList;
	menuItemST*     menuItemValue = *menuItemList;
	if (menuItemValue == NULL)
	{
		*menuPointer = (menuItemST*)malloc(sizeof(menuItemST));
		menuItemValue = *menuPointer;
		menuItemValue->parent = parent;
		menuItemValue->child = child;
		menuItemValue->next = NULL;
	}
	else
	{
		while (menuItemValue->next != NULL)
			menuItemValue = menuItemValue->next;

		menuItemValue->next = (menuItemST*)malloc(sizeof(menuItemST));
		menuItemValue = menuItemValue->next;
		(menuItemValue)->parent = parent;
		(menuItemValue)->child = child;
		(menuItemValue)->next = NULL;
	}
}


/** ------------------------------------------------------------------------ **/
/**                                XML PART                                  **/
/** ------------------------------------------------------------------------ **/
/*
* String to token.
* @param   str [input]: string
* @param   token [input]: token
* @see     showLog().
* @return  None.
*/
void strToToken(uint8* str, uint8* token)
{
	char* tokenPtr = NULL;

	memset(token, 0, 3);
	if (str[0] == 0)
	{
		token[0] = 0;
		return;
	}

	tokenPtr = strstr(str, ".");
	if (tokenPtr == 0)
	{
		strcpy(token, str);
		str[0] = 0;
	}
	else
	{
		strncpy(token, str, strlen(str) - strlen(tokenPtr));
		strcpy(str, tokenPtr + 1);
	}
	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "TOKEN: %s", token);
}

/*
* Read integer number.
* @param   value [input]: value of number
* @param   data [input]: data
* @return  None.
*/
void readInteger(uint8* value, mxml_node_t* data)
{
	*value = (uint8)data->value.integer;
}

/*
* Read string.
* @param   str [input]: string
* @param   data [input]: data
* @param   size [input]: size of string
* @return  None.
*/
void readString(uint8* str, mxml_node_t* data, uint8 size)
{
	memset(str, 0, size);

	if (strlen(data->value.text.string) <= size)
	{
		strncat(str, data->value.text.string, size - strlen(str));
	}

	data = data->next;
	if (data != NULL && strlen(data->value.text.string) > 0)   // space in tag 
	{
		memset(str, 0, size);
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "readString", "STR is %s", str);
}

/*
* Read string with space.
* @param   str [input]: string
* @param   data [input]: data
* @param   size [input]: size of string
* @return  None.
*/
void readStringWithSpace(uint8* str, mxml_node_t* data, uint8 size)
{
	memset(str, 0, size);

	if (strlen(data->value.text.string) <= size)
	{
		strncat(str, data->value.text.string, size - strlen(str));
	}

	do
	{
		data = data->next;
		if (data != NULL && strlen(data->value.text.string) > 0)   // space in tag 
		{
			strncat(str, " ", size - strlen(str));

			strncat(str, data->value.text.string, size - strlen(str));
		}

	} while (data != NULL);
}

/*
* Check string numeric.
* @param   str [input]: string
* @param   data [input]: data
* @param   size [input]: size of string
* @return  retVal(TRUE or FALSE).
*/
uint8 checkStringNumeric(uint8* str, uint8 size, uint8 type)
{
	uint8*	tmp = NULL;
	uint8	retVal = TRUE;

	if (strlen(str) <= 0 || strlen(str) > size)
		retVal = FALSE;

	tmp = str;
	while (*tmp != '\0' && retVal != FALSE)
	{
		if (*tmp >= '0' && *tmp <= '9')
		{
			*tmp++;
		}
		else
		{
			retVal = FALSE;
			break;
		}
	}

	tmp = str;
	switch (type)
	{
	case STRING_NUMBER:
		if (*tmp == '0')
		{
			retVal = FALSE;
		}
		break;
	case STRING_PHONE:
		break;
	case STRING_PREFIX:
		if (strlen(tmp) == 0)
		{
			retVal = TRUE;
		}
		break;
	default:
		break;
	}

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "checkStringNumeric is %d", retVal);
	return retVal;
}

//***************************** Init XML Files *******************************//  

/*
* Initialize general connection information from Xml like:connectioncount,posnii,etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   generalConnInfo [input]: general connection information
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     initGeneralConnectionCountDefaults().
* @see     showLog().
* @see     initGeneralConnectionTPDUNIIDefaults().
* @see     initGeneralConnectionMessageNiiDefaults().
* @return  TRUE or FALSE.
*/
uint8 initGeneralConnectionFromXml(mxml_node_t* serverXmlHead, generalConnInfoST* generalConnInfo)
{
	mxml_node_t* data = NULL;
	uint8        dataIsCorrect = FALSE;
	uint8        buffer[512] = { 0 };
	uint8        model[20] = { 0 };//MRF_NEW18

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "connectioncount", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL)
		{
			readString(buffer, data->child, 1);
			if (checkStringNumeric(buffer, 1, STRING_NUMBER))
			{
				generalConnInfo->connectionCount = (uint8)strToInt(buffer);
				if (generalConnInfo->connectionCount <= 6 || generalConnInfo->connectionCount >= 1)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initGeneralConnectionCountDefaults(generalConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "posnii", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL)
		{
			readString(buffer, data->child, 3);
			if (checkStringNumeric(buffer, 3, STRING_NUMBER))
			{
				generalConnInfo->POSNii = strToInt(buffer);
				showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", buffer);
				if (generalConnInfo->POSNii > 0 || generalConnInfo->POSNii <= 999)
				{
					showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "nii set success");
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initGeneralConnectionTPDUNIIDefaults(generalConnInfo);


	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "swinii", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL)
		{
			readString(buffer, data->child, 3);
			if (checkStringNumeric(buffer, 3, STRING_NUMBER))
			{
				generalConnInfo->SWINii = strToInt(buffer);
				if (generalConnInfo->SWINii > 0 || generalConnInfo->SWINii <= 999)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}

	if (!dataIsCorrect)
		initGeneralConnectionMessageNiiDefaults(generalConnInfo);


	//mgh_added970528
	getDeviceModel(&model[0]); //ABS_NEW1
	if (strcmp(model, "VEGA3000") == 0)
	{
		dataIsCorrect = FALSE;
		data = mxmlFindElement(serverXmlHead, serverXmlHead, "secondswinii", NULL, NULL, MXML_DESCEND);
		if (data != NULL)
		{
			if (data->child != NULL)
			{
				readString(buffer, data->child, 3);
				if (checkStringNumeric(buffer, 3, STRING_NUMBER))
				{
					generalConnInfo->SWINii = strToInt(buffer);
					if (generalConnInfo->SWINii > 0 || generalConnInfo->SWINii <= 999)
					{
						dataIsCorrect = TRUE;
					}
				}
			}
		}

		if (!dataIsCorrect)
			initGeneralConnectionMessageNiiDefaults(generalConnInfo);
	}

	return TRUE;
}

/*
* Initialize HDLC connection information from Xml like:phone number,prefix,etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   HDLCConnInfo [input]: HDLC connection information
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     initHDLCConnectionPhoneDefaults().
* @see     initHDLCConnectionPrefixDefaults().
* @see     initHDLCConnectionRetriesDefaults().
* @see     initHDLCConnectionCommunicationTimeoutDefaults().
* @return  TRUE or FALSE.
*/
uint8 initHDLCConnectionFromXml(mxml_node_t* serverXmlHead, HDLCConnInfoST* HDLCConnInfo)
{
	mxml_node_t* data = NULL;
	uint8        dataIsCorrect = FALSE;
	uint8        buffer[512] = { 0 };

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "phone", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);
			if (checkStringNumeric(buffer, 16, STRING_PHONE))
			{
				strcpy(HDLCConnInfo->phone, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initHDLCConnectionPhoneDefaults(HDLCConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "prefix", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 6);
			if (checkStringNumeric(buffer, 6, STRING_PREFIX))
			{
				strcpy(HDLCConnInfo->prefix, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initHDLCConnectionPrefixDefaults(HDLCConnInfo);


	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "dialing", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL)
		{
			readString(buffer, data->child, 5);
			//            strToUpper(buffer);
			if (strcmp(buffer, "tone") == 0)
			{
				HDLCConnInfo->useToneDialing = TRUE;
				dataIsCorrect = TRUE;
			}
			else if (strcmp(buffer, "pulse") == 0)
			{
				dataIsCorrect = TRUE;
				HDLCConnInfo->useToneDialing = FALSE;
			}
		}
	}

	if (!dataIsCorrect)
		initHDLCConnectionUseToneDialingDefaults(HDLCConnInfo);


	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "retries", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 1);

			if (checkStringNumeric(buffer, 1, STRING_NUMBER))
			{
				HDLCConnInfo->retries = strToInt(buffer);
				if (HDLCConnInfo->retries >= 1 || HDLCConnInfo->retries <= 9)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initHDLCConnectionRetriesDefaults(HDLCConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "communicationTimeOut", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 3);

			if (checkStringNumeric(buffer, 3, STRING_NUMBER))
			{
				HDLCConnInfo->communicationTimeout = strToInt(buffer);
				if (HDLCConnInfo->communicationTimeout >= 1 ||
					HDLCConnInfo->communicationTimeout <= 180)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initHDLCConnectionCommunicationTimeoutDefaults(HDLCConnInfo);

	return TRUE;
}

/*
* Initialize LAN connection information from Xml ,for example:tcpPort,ipAddress,etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   LANConnInfo [input]: LAN connection information
* @see     showLog().
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     initLanConnectionTcpPortDefaults().
* @see     strToToken().
* @see     initLanConnectionLocalIpAddressDefaults().
* @see     initLanConnectionGatewayDefaults().
* @see     initLanConnectionNetworkMaskDefaults().
* @return  TRUE or FALSE.
*/
uint8 initLanConnectionFromXml(mxml_node_t* serverXmlHead, LANConnInfoST* LANConnInfo)
{
	mxml_node_t* 	data = NULL;
	uint8        	dataIsCorrect = FALSE;
	uint8        	buffer[512] = { 0 };
	uint8       	token[3] = { 0 };
	uint8        	i = 0;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "init LAN XML");

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "dhcp", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data != NULL");

		if (data->child != NULL)
		{
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "data->child != NULL");

			readInteger(&LANConnInfo->dhcp, data->child);
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "LANConnInfo->dhcp is : %d", LANConnInfo->dhcp);
			dataIsCorrect = TRUE;
		}
	}
	if (!dataIsCorrect)
		initLanConnectionDhcpDefaults(LANConnInfo);


	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "tcpPort", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 5);

			if (checkStringNumeric(buffer, 5, STRING_NUMBER))
			{
				LANConnInfo->tcpPort = strToInt(buffer);
				if (LANConnInfo->tcpPort >= 1 || LANConnInfo->tcpPort <= 99999)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initLanConnectionTcpPortDefaults(LANConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "ipAddress", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "IP address");
			readString(buffer, data->child, 16);
			strToToken(buffer, token);

			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					LANConnInfo->ipAddress[i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

				strToToken(buffer, token);
				i++;
			}
			LANConnInfo->ipAddress[4] = 0;
		}
	}
	if (!dataIsCorrect)
		initLanConnectionLocalIpAddressDefaults(LANConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "localIpAddress", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);

			strToToken(buffer, token);
			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					LANConnInfo->localIpAddress[i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

				strToToken(buffer, token);
				i++;
			}
			LANConnInfo->localIpAddress[4] = 0;
		}
	}
	if (!dataIsCorrect)
		initLanConnectionLocalIpAddressDefaults(LANConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "gateway", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);

			strToToken(buffer, token);
			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					LANConnInfo->gateway[i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

				strToToken(buffer, token);
				i++;
			}
			LANConnInfo->gateway[4] = 0;
		}
	}
	if (!dataIsCorrect)
		initLanConnectionGatewayDefaults(LANConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "networkMask", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);
			strToToken(buffer, token);
			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					LANConnInfo->networkMask[i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

                strToToken(buffer, token);
                i++;
            }
            LANConnInfo->networkMask[4] = 0; 
        }
    }
    if (!dataIsCorrect)
        initLanConnectionNetworkMaskDefaults(LANConnInfo);
    //+HNO_980715**************************************************
    dataIsCorrect = FALSE;

    data = mxmlFindElement(serverXmlHead, serverXmlHead, "LanURLAdrees", NULL, NULL, MXML_DESCEND);
    if (data != NULL)
    {     
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "","URL data from xml is okkkkkkkkkk");
        if (data->child != NULL && data->child->value.text.string != NULL)
        {
//            readString(buffer, data->child, 17);
			readString(buffer, data->child, strlen((const char *)data->child));
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "","buffer: %s", buffer);
            dataIsCorrect = TRUE;
        }
    }
    if (!dataIsCorrect)
        initLanConnectionDNSURLDefaults(LANConnInfo);

    return TRUE;
}

/*
* Initialize GPRS connection information from XML ,for example:tcpPort, ipAddress,etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   GPRSConnInfo [input]: GPRS connection information
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     initGPRSConnectionTcpPortDefaults().
* @see     strToToken().
* @see     initGPRSConnectionIpAddressDefaults().
* @see     initGPRSConnectionApnDefaults().
* @see     initGPRSConnectionPinDefaults().
* @return  TRUE or FALSE.
*/
uint8 initGPRSConnectionFromXml(mxml_node_t* serverXmlHead, GPRSConnInfoST* GPRSConnInfo)
{
	mxml_node_t* 	data = NULL;
	uint8        	dataIsCorrect = FALSE;
	uint8        	buffer[512] = { 0 };
	uint8       	token[3] = { 0 };
	uint8        	i = 0;

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "tcpPort", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 5);

			if (checkStringNumeric(buffer, 5, STRING_NUMBER))
			{
				GPRSConnInfo->tcpPort = strToInt(buffer);
				if (GPRSConnInfo->tcpPort >= 1 /*|| GPRSConnInfo->tcpPort <= 99999*/)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initGPRSConnectionTcpPortDefaults(GPRSConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "ipAddress", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);

			strToToken(buffer, token);
			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					GPRSConnInfo->ipAddress[(int)i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

				strToToken(buffer, token);
				i++;
			}
			GPRSConnInfo->ipAddress[4] = 0;
		}
	}
	if (!dataIsCorrect)
		initGPRSConnectionIpAddressDefaults(GPRSConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "apn", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 3);
			if (strcmp(buffer, "mtn") == 0)
			{
				strcpy(GPRSConnInfo->apn, "MTN");
				dataIsCorrect = TRUE;
			}
			else if (strcmp(buffer, "mci") == 0)
			{
				strcpy(GPRSConnInfo->apn, "MCINET");
				dataIsCorrect = TRUE;
			}
			else if (strcmp(buffer, "rightel") == 0) //MRF_GPRS
			{
				strcpy(GPRSConnInfo->apn, "RighTel");
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initGPRSConnectionApnDefaults(GPRSConnInfo);

    dataIsCorrect = FALSE;
    data = mxmlFindElement(serverXmlHead, serverXmlHead, "pin", NULL, NULL, MXML_DESCEND);
    if (data != NULL)
    {           
        if (data->child != NULL && data->child->value.text.string != NULL)
        {
            readString(buffer, data->child, 4);
            if (checkStringNumeric(buffer, 4, STRING_NUMBER))
            {
                strncpy(GPRSConnInfo->pin, buffer, 4);
                dataIsCorrect = TRUE;
            }
        }
    }
    if (!dataIsCorrect)
        initGPRSConnectionPinDefaults(GPRSConnInfo); 
    //+HNO_980721**************************************************
    dataIsCorrect = FALSE;

    data = mxmlFindElement(serverXmlHead, serverXmlHead, "GPRSURLAdrees", NULL, NULL, MXML_DESCEND);
    if (data != NULL)
    {     
        showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "","URL data from xml is okkkkkkkkkk");
        if (data->child != NULL && data->child->value.text.string != NULL)
        {
			readString(buffer, data->child, strlen((const char *)data->child));
            dataIsCorrect = TRUE;
            showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "","buffer: %s", buffer);
        }
    }
    if (!dataIsCorrect)
        initGPRSConnectionURLDefaults(GPRSConnInfo);

    return TRUE;
}

/*
* Initialize PPP connection information from XML, for example:phone, prefix, etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   PPPConnInfo [input]: PPP connection information
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     initPPPConnectionPhoneDefaults().
* @see     strToToken().
* @see     initPPPConnectionPrefixDefaults().
* @see     initPPPConnectionRetriesDefaults().
* @see     initPPPConnectionCommunicationTimeoutDefaults().
* @see     initPPPConnectionTcpPortDefaults().
* @see     initPPPConnectionIpAddressDefaults().
* @see     initPPPConnectionUserDefaults().
* @see     initPPPConnectionPasswordDefaults().
* @return  TRUE or FALSE.
*/
uint8 initPPPConnectionFromXml(mxml_node_t* serverXmlHead, PPPConnInfoST* PPPConnInfo)
{
	mxml_node_t* 	data = NULL;
	uint8        	dataIsCorrect = FALSE;
	uint8        	buffer[512] = { 0 };
	uint8       	token[3] = { 0 };
	uint8        	i = 0;

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "phone", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);
			if (checkStringNumeric(buffer, 16, STRING_PHONE))
			{
				strcpy(PPPConnInfo->HDLCConnInfo.phone, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionPhoneDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "prefix", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 6);
			if (checkStringNumeric(buffer, 6, STRING_PREFIX))
			{

				strcpy(PPPConnInfo->HDLCConnInfo.prefix, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionPrefixDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "retries", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 1);

			if (checkStringNumeric(buffer, 1, STRING_NUMBER))
			{
				PPPConnInfo->HDLCConnInfo.retries = strToInt(buffer);
				if (PPPConnInfo->HDLCConnInfo.retries >= 0 || PPPConnInfo->HDLCConnInfo.retries <= 9)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionRetriesDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "communicationTimeOut", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 3);

			if (checkStringNumeric(buffer, 3, STRING_NUMBER))
			{
				PPPConnInfo->HDLCConnInfo.communicationTimeout = strToInt(buffer);
				if (PPPConnInfo->HDLCConnInfo.communicationTimeout >= 0 ||
					PPPConnInfo->HDLCConnInfo.communicationTimeout <= 180)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionCommunicationTimeoutDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "tcpPort", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 5);

			if (checkStringNumeric(buffer, 5, STRING_NUMBER))
			{
				PPPConnInfo->tcpPort = strToInt(buffer);
				if (PPPConnInfo->tcpPort >= 1 || PPPConnInfo->tcpPort <= 99999)
				{
					dataIsCorrect = TRUE;
				}
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionTcpPortDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "ipAddress", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);

			strToToken(buffer, token);
			dataIsCorrect = TRUE;
			i = 0;
			while (token[0] != 0 && i < 4)
			{
				if (strToInt(token) >= 0 && strToInt(token) <= 255)
					PPPConnInfo->ipAddress[(int)i] = (uint8)strToInt(token);
				else
					dataIsCorrect = FALSE;

				strToToken(buffer, token);
				i++;
			}
			PPPConnInfo->ipAddress[4] = 0;
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionIpAddressDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "username", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 17);
			if (strlen(buffer) > 0)
			{
				strcpy(PPPConnInfo->PPPConfig.user, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionUserDefaults(PPPConnInfo);

	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "password", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 17);
			if (strlen(buffer) > 0)
			{
				strcpy(PPPConnInfo->PPPConfig.password, buffer);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (!dataIsCorrect)
		initPPPConnectionPasswordDefaults(PPPConnInfo);

	return TRUE;
}

/*
* Initialize connection information from XML, for example: connection type, general, etc.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   serverSpecST [input]: the number of servers
* @see     showLog().
* @see     mxmlFindElement().
* @see     initGeneralConnectionFromXml().
* @see     initGeneralConnectionDefaults().
* @see     mxmlIndexNew().
* @see     mxmlIndexEnum().
* @see     readString().
* @see     initGeneralConnectionTypeDefaults().
* @see     initHDLCConnectionFromXml().
* @see     updateFileInfo().
* @see     addSystemErrorReport().
* @see     initLanConnectionFromXml().
* @see     initPPPConnectionFromXml().
* @see     initGPRSConnectionFromXml().
* @see     initHDLCConnectionDefaults().
* @return  TRUE or FALSE.
*/
uint8 initConnectionFromXml(serverSpecST* serverSpec, mxml_node_t* serverXmlHead)
{
	generalConnInfoST       generalConnInfo = { 0 };
	LANConnInfoST           LANConnInfo = { 0 };
	PPPConnInfoST           PPPConnInfo = { 0 };
	HDLCConnInfoST          HDLCConnInfo = { 0 };
	GPRSConnInfoST          GPRSConnInfo = { 0 };
	mxml_node_t*            data = NULL;
	mxml_index_t*           choiceIndex = NULL;
	mxml_node_t*            choicecNode = NULL;

	uint16                  returnVal = 0;
	uint8                   i = 0;
	uint8                   buffer[512] = { 0 };
	uint8                   dataIsCorrect = FALSE;
	terminalSpecST          terminalCapability = getTerminalCapability(); //MRF_NEW2

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "initConnectionFromXml", "********STARTED***********");
	memset(&generalConnInfo, 0, sizeof(generalConnInfoST));
	memset(&LANConnInfo, 0, sizeof(LANConnInfoST));
	memset(&PPPConnInfo, 0, sizeof(PPPConnInfoST));
	memset(&HDLCConnInfo, 0, sizeof(HDLCConnInfoST));
	memset(&GPRSConnInfo, 0, sizeof(GPRSConnInfoST));

	data = mxmlFindElement(serverXmlHead, serverXmlHead, "connection", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		data = mxmlFindElement(serverXmlHead, serverXmlHead, "general", NULL, NULL, MXML_DESCEND);
		if (data != NULL)
		{
			initGeneralConnectionFromXml(data, &generalConnInfo);
		}
		else
			initGeneralConnectionDefaults(&generalConnInfo);

		choiceIndex = mxmlIndexNew(serverXmlHead, "choice", NULL); // index for choice
		for (i = 0; i < generalConnInfo.connectionCount; i++)
		{
			choicecNode = mxmlIndexEnum(choiceIndex);
			dataIsCorrect = FALSE;
			if (choicecNode != NULL)
			{
				data = mxmlFindElement(choicecNode, choicecNode, "connectiontype", NULL, NULL, MXML_DESCEND);

				if (terminalCapability.GPRSCapability)//MRF_NEW2
				{
					generalConnInfo.connectionsType[(int)i] = CONNECTION_GPRS;
					dataIsCorrect = TRUE;
				}

				else if (data != NULL)
				{
					if (data->child != NULL)
					{
						readString(buffer, data->child, 32);

                        if (!strcmp((char*)buffer, "hdlc"))
                        {
	                    generalConnInfo.connectionsType[(int)i] = CONNECTION_HDLC;
                        }
                        else if (!strcmp((char*)buffer, "lan") || !strcmp((char*)buffer, "ethernet"))
                        {
                            generalConnInfo.connectionsType[(int)i] = CONNECTION_LAN;
                        }
                        else if (!strcmp((char*)buffer, "ppp"))
                        {
                            generalConnInfo.connectionsType[(int)i] = CONNECTION_PPP;
                        }
                        else if (!strcmp((char*)buffer, "gprs"))//#HNO_980721 uncomment
                        {
                            generalConnInfo.connectionsType[(int)i] = CONNECTION_GPRS;
                        }
                        else
                            initGeneralConnectionTypeDefaults(&(generalConnInfo.connectionsType[i])); // get from defaults;
                        dataIsCorrect = TRUE;
                    }
                }
//                #endif
            }

			if (!dataIsCorrect)
				initGeneralConnectionTypeDefaults(&(generalConnInfo.connectionsType[i])); // get from defaults;


            //************************   get each connection *****************************************
            dataIsCorrect = FALSE;
            if (choicecNode != NULL)
            {
                dataIsCorrect = TRUE;
                switch (generalConnInfo.connectionsType[i])
                {
                    case CONNECTION_HDLC:
                        initHDLCConnectionFromXml(choicecNode, &HDLCConnInfo);
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "initHDLCConnectionFromXml");
                        returnVal = updateFileInfo(serverSpec->files.connFiles[i], &HDLCConnInfo, sizeof(HDLCConnInfoST));  
                        if (returnVal != SUCCESS) 
                        {
                            addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
                            return FALSE;
                        }
                        break;
                    case CONNECTION_LAN:
                        initLanConnectionFromXml(choicecNode, &LANConnInfo);
                        showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "uuuurl: %s", LANConnInfo.URL);
                        returnVal = updateFileInfo(serverSpec->files.connFiles[i], &LANConnInfo, sizeof(LANConnInfoST));  
                        if (returnVal != SUCCESS) 
                        {
                            addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
                            return FALSE;
                        }
                        break;
                    case CONNECTION_PPP:
                        initPPPConnectionFromXml(choicecNode, &PPPConnInfo);  
                        returnVal = updateFileInfo(serverSpec->files.connFiles[i], &PPPConnInfo, sizeof(PPPConnInfoST));  
                        if (returnVal != SUCCESS) 
                        {
                            addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
                            return FALSE;
                        }
                        break;
                    case CONNECTION_GPRS:
                        initGPRSConnectionFromXml(choicecNode, &GPRSConnInfo);
                        returnVal = updateFileInfo(serverSpec->files.connFiles[i], &GPRSConnInfo, sizeof(GPRSConnInfoST));  
                        if (returnVal != SUCCESS) 
                        {
                            addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
                            return FALSE;
                        }
                        break;
                    default:
                        initGeneralConnectionTypeDefaults(&(generalConnInfo.connectionsType[i])); // get from defaults;
                        initHDLCConnectionDefaults(&HDLCConnInfo); // default type is hdlc
                        returnVal = updateFileInfo(serverSpec->files.connFiles[i], &HDLCConnInfo, sizeof(HDLCConnInfoST));  
                        if (returnVal != SUCCESS) 
                        {
                            addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
                            return FALSE;
                        }                    
                        break;
                }
                //********************************************************************************************
            }
            if (!dataIsCorrect)
            {
                initGeneralConnectionTypeDefaults(&(generalConnInfo.connectionsType[i])); // get from defaults;
                //HNO_ADD
#ifdef GPRS_IWL
				initGPRSConnectionDefaults(&GPRSConnInfo);  // default type is hdlc
				returnVal = updateFileInfo(serverSpec->files.connFiles[0], &GPRSConnInfo, sizeof(GPRSConnInfoST));
				if (returnVal != SUCCESS)
				{
					addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
					return FALSE;
				}
#else
				initHDLCConnectionDefaults(&HDLCConnInfo); // default type is hdlc
				returnVal = updateFileInfo(serverSpec->files.connFiles[i], &HDLCConnInfo, sizeof(HDLCConnInfoST));
				if (returnVal != SUCCESS)
				{
					addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
					return FALSE;
				}
#endif
			}
		}// for loop
		mxmlIndexDelete(choiceIndex);
	}
	else // connection tag doesn't exist
	{
		// init all connection from defaults
		initGeneralConnectionDefaults(&generalConnInfo);
		initGeneralConnectionTypeDefaults(&(generalConnInfo.connectionsType[1])); // get type hdlc from defaults;
		//HNO_ADD
#ifdef GPRS_IWL
		initGPRSConnectionDefaults(&GPRSConnInfo);  // default type is hdlc
		returnVal = updateFileInfo(serverSpec->files.connFiles[0], &GPRSConnInfo, sizeof(GPRSConnInfoST));
		if (returnVal != SUCCESS)
		{
			addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
			return FALSE;
		}
#else
		initHDLCConnectionDefaults(&HDLCConnInfo);  // default type is hdlc
		returnVal = updateFileInfo(serverSpec->files.connFiles[1], &HDLCConnInfo, sizeof(HDLCConnInfoST));
		if (returnVal != SUCCESS)
		{
			addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
			return FALSE;
		}
#endif
	}

	// save general config to file 
	returnVal = updateFileInfo(serverSpec->files.generalConnInfoFile, &generalConnInfo, sizeof(generalConnInfoST));
	if (returnVal != SUCCESS)
	{
		addSystemErrorReport(returnVal, ERR_TYPE_FILE_SYSTEM);
		return FALSE;
	}
	setGeneralConnInfo(serverSpec->files.id, &generalConnInfo);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "initConnectionFromXml", "********Finised***********");
	return TRUE;
}

/*
* Initialize merchant information from XML, for example: helpPhone number.
* @param   serverXmlHead [input]: tag of XML configuration
* @param   serverSpecST [input]: the number of servers
* @see     showLog().
* @see     readMerchantSpec().
* @see     mxmlFindElement().
* @see     readString().
* @see     checkStringNumeric().
* @see     writeMerchantSpec().
* @return  None.
*/
uint8 initMerchantSpecFromXml(serverSpecST* serverSpec, mxml_node_t* serverXmlHead)
{
	mxml_node_t* data = NULL;
	uint8        dataIsCorrect = FALSE;
	uint8        buffer[512] = { 0 };
	merchantSpecST merchantSpec;

	memset(&merchantSpec, 0, sizeof(merchantSpecST));

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "initMerchantSpecFromXml", "********STARTED***********");

	readMerchantSpec(&serverSpec->files, &merchantSpec);
	dataIsCorrect = FALSE;
	data = mxmlFindElement(serverXmlHead, serverXmlHead, "helpPhone", NULL, NULL, MXML_DESCEND);
	if (data != NULL)
	{
		if (data->child != NULL && data->child->value.text.string != NULL)
		{
			readString(buffer, data->child, 16);
			if (checkStringNumeric(buffer, 16, STRING_PHONE))
			{
				strncpy(merchantSpec.helpPhone, buffer, 16);
				dataIsCorrect = TRUE;
			}
		}
	}
	if (dataIsCorrect)
		writeMerchantSpec(&serverSpec->files, merchantSpec);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "initMerchantSpecFromXml", "********FINISHED***********");

}

/*
* XMl  type of node.
* @param   node [input]: Node type like:integer, real or etc
* @see     mxmlElementGetAttr().
* @return  XML type.
*/
mxml_type_t xmlTypeCB(mxml_node_t* node)
{
	const char* type;

	type = mxmlElementGetAttr(node, "type");
	if (type == NULL)
		type = node->value.element.name;

	if (!strcmp(type, "integer"))
		return MXML_INTEGER;
	else if (!strcmp(type, "opaque"))
		return MXML_OPAQUE;
	else if (!strcmp(type, "real"))
		return MXML_REAL;
	else
		return MXML_TEXT;
}

//MRF_NEW2
void setPOSCapability(void)
{
	uint8               check = 0;
	terminalSpecST      terminalCapapility;
	serviceSpecST       POSServices;


	memset(&terminalCapapility, 0, sizeof(terminalSpecST));

	check = checkAppVersion();//+MRF_971225
	if (fileExist(TERMINAL_SPEC_FILE) != SUCCESS)
		initTerminalSpec(TRUE);
	else if (check)//+MRF_971225
		initTerminalSpec(FALSE);

	readPOSCapability();
	terminalCapapility = getTerminalCapability();

	if (fileExist(POS_SERVICE_FILE) != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "terminalInitiation", "Checking terminal");
		initPOSServices(&POSServices);
	}

	readPOSService();
	POSServices = getPOSService();
}

/*
* load initialization data from XML file to whole application.
* @param   returnedApplicationSpec [output]: returned application information
* @see     showLog().
* @see     displayMessage().
* @see     readFileInfo().
* @see     mxmlLoadString().
* @see     mxmlIndexNew().
* @see     mxmlIndexEnum().
* @see     fillFileInfo().
* @see     setServiceSchedule().
* @see     mxmlFindElement().
* @see     readString().
* @see     readInteger().
* @see     fileExist().
* @see     initConnectionFromXml().
* @see     initMerchantSpecFromXml().
* @see     mxmlIndexDelete().
* @see     mxmlDelete().
* @see     defineMenuItems().
* @see     checkServersMenusVisibility().
* @return  TRUE or FALSE.
*/
uint8 loadInitializationData(applicationSpecST** returnedApplicationSpec)
{
	int32               fileId = 0;
	mxml_node_t*        node = NULL;
	mxml_node_t*        tree = NULL;
	mxml_node_t*        data = NULL;
	mxml_index_t*       switchIndex = NULL;
	applicationSpecST*	applicationSpec = NULL;
	serverSpecST*       serverSpec = NULL;
	uint8               i = 0;
	int16               retValue = 0;
	uint8               buffer[8192] = { 0 };
	uint32              sizeOfFile = 0;
	uint8               dataIsCorrect = FALSE;
	cardTracksST        tempCardTrack;
	cardSpecST          cardSpec;
	versionKeyST        versionKeySpec;

	memset(&tempCardTrack, 0, sizeof(cardTracksST));
	memset(&cardSpec, 0, sizeof(cardSpecST));
	memset(&versionKeySpec, 0, sizeof(versionKeyST));

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "*****load Initialization Data*****");

	setPOSCapability();

	displayMessage("�� ��� ��ѐ���� ����...", DISPLAY_START_LINE + 1, ALL_LINES);
#if !defined (ICT250) && !defined(IWL220)	//HNO_ADD

	if (retValue = fileSize(CONFIG_XML, &sizeOfFile) != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ret= %d", retValue);
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fileSize");
	if (retValue = readFileInfo(CONFIG_XML, buffer, &sizeOfFile) != SUCCESS)
	{
		showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "ret= %d", retValue);
		return FALSE;
	}
#else
	readConfigFile(buffer);//HNO_ADD
#endif

	showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "strlen(buffer)= %d", strlen(buffer));

	tree = mxmlLoadString(NULL, buffer, xmlTypeCB);

	closeFile(fileId);

	if (tree == NULL)
	{
		showLog(ALL_POS, __FILE__, __LINE__, FATAL, "", "NULL tree");
		return FALSE;
	}

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "XML OK");

	//HNO_IDENT
#ifndef INGENICO //IDENT2
	applicationSpec = (applicationSpecST*)malloc(sizeof(applicationSpecST)); // alocate memory
#else
	applicationSpec = (applicationSpecST*)umalloc(sizeof(applicationSpecST));//HNO_change malloc to umalloc for ict
#endif
	*returnedApplicationSpec = applicationSpec;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Parsing xml start");

	switchIndex = mxmlIndexNew(tree, "switch", NULL);
	node = mxmlIndexEnum(switchIndex);

	do
	{
		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "memory allocation");

		if (i == 0)
		{
#ifndef INGENICO //IDENT2
			applicationSpec->serverSpec = (serverSpecST*)malloc(sizeof(serverSpecST));
#else
			applicationSpec->serverSpec = (serverSpecST*)umalloc(sizeof(serverSpecST));
#endif
			serverSpec = applicationSpec->serverSpec;
			serverSpec->next = NULL;
		}
		else
		{
#ifndef INGENICO //IDENT2
			serverSpec->next = (serverSpecST*)malloc(sizeof(serverSpecST));
#else
			serverSpec->next = (serverSpecST*)umalloc(sizeof(serverSpecST));
#endif
			serverSpec = serverSpec->next;
			serverSpec->next = NULL;
		}

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "filling schedules and file id");
		serverSpec->files.id = i;
		fillFileInfo(serverSpec, i);

		setServiceSchedule(serverSpec);
		if (i == 0)
			strcpy(serverSpec->files.generalKey, MASKAN_TEST_GENERAL_KEY);
		else
			strcpy(serverSpec->files.generalKey, MASKAN_GENERAL_KEY);

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Display tag");

		dataIsCorrect = FALSE;
		data = mxmlFindElement(node, node, "display", NULL, NULL, MXML_DESCEND);
		if (data != NULL)
		{
			if (data->child != NULL)
			{
				readString(serverSpec->files.displayLogoFile, data->child, 32);
				dataIsCorrect = TRUE;
				showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", " Display logo is %s ", serverSpec->files.displayLogoFile);
			}
		}
		if (!dataIsCorrect)
			strcpy(serverSpec->files.displayLogoFile, "");

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "printer TAG");

		dataIsCorrect = FALSE;
		data = mxmlFindElement(node, node, "printer", NULL, NULL, MXML_DESCEND);
		if (data != NULL)
		{
			if (data->child != NULL)
			{
				readString(serverSpec->files.printerLogoFile, data->child, 32);
				dataIsCorrect = TRUE;
				showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", " printer logo is %s ", serverSpec->files.printerLogoFile);
			}
		}
		if (!dataIsCorrect)
			strcpy(serverSpec->files.printerLogoFile, "");

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "visible tag");

		dataIsCorrect = FALSE;
		data = mxmlFindElement(node, node, "setvisible", NULL, NULL, MXML_DESCEND);
		if (data != NULL)
		{
			if (data->child != NULL)
			{
				readInteger(&(serverSpec->isMenuVisible), data->child);
				dataIsCorrect = TRUE;
			}
		}
		if (!dataIsCorrect)
			serverSpec->isMenuVisible = TRUE;

		if (fileExist(serverSpec->files.generalConnInfoFile) != SUCCESS)
		{
			showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "Connection files doesn't Exist");
			initConnectionFromXml(serverSpec, node);
		}
		else
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "Connection files Exist");

		showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "merchantFile");

		if (fileExist(serverSpec->files.merchantSpecFile) != SUCCESS)
		{
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "Merchant Spec file does'nt Exist");
			initMerchantSpecFromXml(serverSpec, node);
		}
		else
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "MerchantSpec files Exist");

		if (fileExist(serverSpec->files.versionKeyFile) != SUCCESS)
		{
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "version key file Not Exist");
			//if Not exist version key file message can be create
			initVersionKeySpec(&serverSpec->files, &versionKeySpec);
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "", "versionKeyFile2: %s ", serverSpec->files.versionKeyFile);

		}
		else
			showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "version key  files Exist");
		i++;

	} while ((node = mxmlIndexEnum(switchIndex)) != NULL);

	showLog(ALL_POS, __FILE__, __LINE__, DEBUG, "loadInitializationData", "Parsing xml finised");
	mxmlIndexDelete(switchIndex);
	mxmlDelete(tree);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "define menu items");
	defineMenuItems(applicationSpec->serverSpec);
	applicationSpec->menuItems = NULL;

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "fill menu structs");

	checkServersMenusVisibility(applicationSpec->serverSpec);

	//Calculate and save KCV of master key
	getKCV(MASTER_INDEX, MASKAN_TEST_GENERAL_KEY);

	showLog(ALL_POS, __FILE__, __LINE__, TRACE, "", "end of load Initialization Data");

	return TRUE;
}


